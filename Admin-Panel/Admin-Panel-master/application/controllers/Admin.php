<?php 
 
 
class Admin extends CI_Controller{
 
	function __construct(){
		parent::__construct();		
		$this->load->model('m_admin');
		$this->load->helper('url');
		  if($this->session->userdata('status') != "login"){
   		redirect(base_url("login"));
  }
  
 
	}
 
	function index(){
		$this->load->view('inc/header');        
		$this->load->view('inc/footer');
		$data['admin'] = $this->m_admin->tampil_data()->result();
		$this->load->view('admin/admin',$data);
	}
 
	function tambah(){
		$this->load->view('admin/tambah_admin');
	}
 
	function tambah_aksi(){
		$nama_user = $this->input->post('nama');
		$password = $this->input->post('password');
		$nama_lengkap = $this->input->post('namalengkap');
 
		$data = array(
			'nama_user' => $nama_user,
			'password' => md5($password),
			'nama_lengkap' => $nama_lengkap
			);
		$this->m_admin->input_data($data,'tbl_user');
		redirect('master/index');
	}
	// 	function hapus($id){
	// 	$where = array('id' => $id);
	// 	$this->m_data->hapus_data($where,'user');
	// 	redirect('master/index');
	// }
 
	// function edit($id){
	// 	$where = array('Id' => $id);
	// 	$data['user'] = $this->m_data->edit_data($where,'tbl_user')->result();
	// 	$this->load->view('admin/edit_admin',$data);
	// }


	function edit(){
	
		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('nama_user', 'nama_user', 'required');
		// $this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('nama_lengkap', 'nama_lengkap', 'required');

		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Ubah");
			redirect('admin');
		}else{
			$data=array(
				"id"=>$_POST['id'],
				"nama_user"=>$_POST['nama_user'],
				"nama_lengkap"=>$_POST['nama_lengkap']
			);
			$this->db->where('id', $_POST['id']);
			$this->db->update('tbl_user',$data);
			$this->session->set_flashdata('sukses',"Data Berhasil Di Ubah");
			redirect('admin');
		}	
	}
	
 
	
 
}