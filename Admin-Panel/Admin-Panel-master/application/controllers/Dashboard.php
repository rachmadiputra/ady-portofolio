<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

 function __construct(){
  parent::__construct();

  if($this->session->userdata('status') != "login"){
   redirect(base_url("login"));
  }
 }

	public function index()
	{
		$this->load->view('inc/header');        
		$this->load->view('inc/footer');
		$this->load->view('admin/dashboard');
	}
}
