<?php 
 
 
class Driver extends CI_Controller{
 
	function __construct(){
		parent::__construct();		
		$this->load->model('m_driver');
		$this->load->helper('url');
 		 if($this->session->userdata('status') != "login"){
  		 redirect(base_url("login"));
  }
	}
 
	function index(){
		$this->load->view('inc/header');        
		$this->load->view('inc/footer');
		$data['driver'] = $this->m_driver->tampil_driver()->result();
		$this->load->view('driver/driver',$data);
	}
 
	function tambah(){
		$this->load->view('driver/tambah_driver');
	}
 
	function tambah_driver(){
		
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('telp', 'telp', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');

 		
		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Tambah");
			redirect('driver');
		}else{
		$data = array(
		
			"nama"=>$_POST['nama'],
			"email"=>$_POST['email'],
			"telp"=>$_POST['telp'],
			"password"=>$_POST['password']
			
			);
		$this->m_driver->input_data($data,'driver');
		$this->session->set_flashdata('sukses',"Data Berhasil Di Tambah");
		redirect('driver');
	}
}
		function hapus($id){
		$where = array('id' => $id);
		$this->m_driver->hapus_data($where,'driver');
		redirect('driver');
	}

 function edit()
	{
		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('telp', 'telp', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');

		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Ubah");
			redirect('driver');
		}else{
			$data=array(
				"id"=>$_POST['id'],
				"nama"=>$_POST['nama'],
				"email"=>$_POST['email'],
				"telp"=>$_POST['telp'],
				"password"=>$_POST['password']
			);
			$this->db->where('id', $_POST['id']);
			$this->db->update('driver',$data);
			$this->session->set_flashdata('sukses',"Data Berhasil Di Ubah");
			redirect('driver');
		}
	}
 
	
 
}