<?php
class Login extends CI_Controller{

 function __construct(){
  parent::__construct();
  $this->load->model('m_login');

 }

 function index(){
  $this->load->view('login/login');
 }

 function cek_login(){
  $username = $this->input->post('username');
  $password = $this->input->post('password');
  $where = array(
   'nama_user' => $username,
   'password' => md5($password)
   );
  $cek = $this->m_login->cek_login("tbl_user",$where)->num_rows();
  if($cek > 0){

   $data_session = array(
    'nama' => $username,
    'status' => "login"
    );

   $this->session->set_userdata($data_session);

   redirect(base_url("dashboard"));

  }else{
 $this->session->set_flashdata('error',"Tolong ulangi kembali");
 redirect(base_url("login"));
  }
 }

 function logout(){
  $this->session->sess_destroy();
  redirect(base_url('login'));
 }
}
?>