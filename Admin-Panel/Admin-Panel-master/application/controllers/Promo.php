<?php 

 
class Promo extends CI_Controller{
 
function __construct(){
parent::__construct();		
$this->load->model('m_promo');
$this->load->helper('url');
  if($this->session->userdata('status') != "login"){
   redirect(base_url("login"));
  }
}
 
function index(){
$this->load->view('inc/header');        
$this->load->view('inc/footer');
$data['promo'] = $this->m_promo->tampil_driver()->result();
$this->load->view('promo/promo',$data);
}
 
function tambah_promo(){
		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('file', 'file', 'required');
		if($this->form_validation->run()==TRUE){
			$this->session->set_flashdata('error',"Data Gagal Di Tambah");
			redirect('promo');
		}else{
		$this->load->library('upload');
		$namafile = "file_".time();
		$config = array(
		'upload_path' => "./images/", 
		'allowed_types' => "gif|jpg|png|jpeg|pdf", 
		'overwrite' => TRUE,
		'max_size' => "2048000",
		'max_height' => "768",
		'max_width' => "1024",
		'file_name'	=> $namafile 
		);

		$this->upload->initialize($config);
		if($_FILES['file']['name']){
		if($this->upload->do_upload('file')){
		$gambar =  $this->upload->data();
		$data = array(
		"nama"=>$_POST['nama'],
		'url'	=> $gambar['file_name']);
		$result = $this->m_promo->insert($data);
		$this->session->set_flashdata('sukses',"Data Berhasil Di Tambah");
		redirect('promo');
		}
		}
		}
		}

	
function hapus($id){
$row = $this->m_promo->get_by_id($id);
$where = array('id' => $id);
unlink('images/'.$row->url);
$this->m_promo->hapus_data($where,'promo');
redirect('promo');
}

function edit($id)
{
		$row = $this->m_promo->get_by_id($id);
		$data = array(
		'id' => $row->id,
		'nama' => $row->nama,
		'url' => $row->url
		);
		$this->load->view('inc/header'); 
		$this->load->view('promo/edit_promo', $data);
	    $this->load->view('inc/footer');   
		
}

function update($id)
{
		$row = $this->m_promo->get_by_id($id);
		if ($_FILES AND $_FILES['filefoto']['name']) {
		$config = array(
		'upload_path' => './images/',
		'allowed_types' => 'jpeg|jpg|png',
		'max_size' => '2048',
		'max_width' => '2000',
		'max_height' => '2000'
		);
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('filefoto')) {
		$this->session->set_flashdata('error', "File terlalu besar" . $this->upload->display_errors() . "</div>");
		redirect(site_url('promo/edit/' . $row->id));
		} else {
		unlink('images/'.$row->url);
		$file = $this->upload->data();
		$id = $this->input->post('id');
		$data = array(
		'nama' => $this->input->post('nama'),
		'url' => $file['file_name'],
		);
		$this->m_promo->update($id, $data);
		}
		}
		else {
		$id = $this->input->post('id');
		$data = array(
		'nama' => $this->input->post('nama'),
		);
		$this->m_promo->update($id, $data);
		}
		$this->session->set_flashdata('sukses', "Gambar berhasil diubah");
		redirect(site_url('promo'));


}
}
