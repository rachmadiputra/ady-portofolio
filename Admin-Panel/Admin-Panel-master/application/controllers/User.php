<?php 
 
 
class User extends CI_Controller{
 
	function __construct(){
		parent::__construct();		
		$this->load->model('m_user');
		$this->load->helper('url');
 
	}
 
	function index(){
		$this->load->view('inc/header');        
		$this->load->view('inc/footer');
		$data['user'] = $this->m_user->tampil_driver()->result();
		$this->load->view('user/user',$data);
	}
 
 
	function tambah_user(){
		
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('telp', 'telp', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');

 		
		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Tambah");
			redirect('driver');
		}else{
		$data = array(
		
			"nama"=>$_POST['nama'],
			"email"=>$_POST['email'],
			"telp"=>$_POST['telp'],
			"password"=>$_POST['password']
			
			);
		$this->m_user->input_data($data,'user');
		$this->session->set_flashdata('sukses',"Data Berhasil Di Tambah");
		redirect('user');
	}
}
		function hapus($id){
		$where = array('id' => $id);
		$this->m_user->hapus_data($where,'user');
		redirect('user');
	}

 function edit()
	{
		$this->form_validation->set_rules('id', 'id', 'required');
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('telp', 'telp', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');

		if($this->form_validation->run()==FALSE){
			$this->session->set_flashdata('error',"Data Gagal Di Ubah");
			redirect('user');
		}else{
			$data=array(
				"id"=>$_POST['id'],
				"nama"=>$_POST['nama'],
				"email"=>$_POST['email'],
				"telp"=>$_POST['telp'],
				"password"=>$_POST['password']
			);
			$this->db->where('id', $_POST['id']);
			$this->db->update('user',$data);
			$this->session->set_flashdata('sukses',"Data Berhasil Di Ubah");
			redirect('user');
		}
	}
 
	
 
}