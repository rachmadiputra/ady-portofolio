<?php 

class M_driver extends CI_Model{
	function tampil_driver(){
		return $this->db->get('driver');
	}

	function input_data($data,$table){
		$this->db->insert($table,$data);
	}
		function hapus_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}
 
	function edit_data($where,$table){		
		return $this->db->get_where($table,$where);
	}
 
	function ubah($data, $id){
        $this->db->where('id',$id);
        $this->db->update('driver', $data);
        return TRUE;
    }
}