<?php 

class M_promo extends CI_Model{
	function tampil_driver(){
		return $this->db->get('promo');
	}

	function insert($data){
		//memasukkan data gambar ke database
		$this->db->insert('promo', $data);
		if ($this->db->affected_rows() > 0) {
			return true;
		}else{
			return false;
		}		
	}
		function hapus_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}
 
	function edit_data($where,$table){		
		return $this->db->get_where($table,$where);
	}
 
	function update($id,$data){
     $this->db->where('id', $id);
    $this->db->update('promo', $data);
    
}
 function get_by_id($id)
  {
    $this->db->where('id', $id);
    return $this->db->get('promo')->row();
  }

}