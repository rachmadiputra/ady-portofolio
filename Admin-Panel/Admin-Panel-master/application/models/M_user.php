<?php 

class M_user extends CI_Model{
	function tampil_driver(){
		return $this->db->get('user');
	}

	function input_data($data,$table){
		$this->db->insert($table,$data);
	}
		function hapus_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}
 
	function edit_data($where,$table){		
		return $this->db->get_where($table,$where);
	}
 
	function ubah($data, $id){
        $this->db->where('id',$id);
        $this->db->update('user', $data);
        return TRUE;
    }
}