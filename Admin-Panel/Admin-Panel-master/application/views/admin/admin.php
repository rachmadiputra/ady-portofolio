                            <section class="content">
                            <div class="container-fluid">
                            <div class="block-header">
                            <h2>
                            Data Admin
                            </h2>
                            </div>
                             <?php 
                              $data=$this->session->flashdata('sukses');
                              if($data!=""){ ?>
                              <div id="notifikasi" class="alert alert-warning"><strong>Sukses! </strong> <?=$data;?></div>
                              <?php } ?>

                              <?php 
                              $data2=$this->session->flashdata('error');
                              if($data2!=""){ ?>
                              <div id="notifikasi" class="alert alert-danger"><strong> Error! </strong> <?=$data2;?></div>
                              <?php } ?>
            <!-- Basic Examples -->
                            <div class="row clearfix">
                             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
             
                            <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                            <div class="header">
                            <h2>
                                List Data Admin
                            </h2>
                          
                           </div>
                           <div class="body">

                           <div class="table-responsive">

                           <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                       
                                  <tr>
                                  <th>No</th>
                                  <th>Nama</th>
                                  <th>Password</th>
                                  <th>Nama Lengkap</th>
                                  <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                    $no = 1;
                                    foreach($admin as $u){ 
                                    ?>
                                  
                                    <tr>
                                    <td><?php echo $no++ ?></td>
                                    <td><?php echo $u->nama_user ?></td>
                                    <td><?php echo $u->password ?></td>
                                    <td><?php echo $u->nama_lengkap ?></td>
                                    <td>
                                    <a data-toggle="modal" data-target="#modal-edit<?=$u->id;?>" >
                                        <button class="btn btn-info">Ubah</button>
                                        </a>
                                  </td>
                                  </tr>

                                  <?php } ?>
                                  </thead>
                                    
                                   </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->

                 <!-- Modal Edit -->   
            <?php $no=0; foreach($admin as $u): $id; ?>
            <div class="row">
            <div id="modal-edit<?=$u->id;?>" class="modal fade">
           <div class="modal-dialog">
           <form action="<?php echo base_url('admin/edit')?>" method="post" >
           <div class="modal-content">
           <div class="modal-header bg-primary">
           <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h4 class="modal-title">Edit Data</h4>
           </div>
           <div class="modal-body">

            <input type="hidden" readonly value="<?=$u->id;?>" name="id" class="form-control" >

            <div class="form-group">
            <label class='col-md-3'>Nama</label>
            <div class='col-md-9'><input type="text" name="nama_user" autocomplete="off" value="<?=$u->nama_user;?>" required placeholder="Masukkan Nama" class="form-control" ></div>
            </div>
             <div class="form-group">
            <label class='col-md-3'>Password</label>
            <div class='col-md-9'><input type="text" disabled name="password" autocomplete="off" value="<?=$u->password;?>" required placeholder="Masukkan Password" class="form-control" ></div>
            </div>
             <div class="form-group">
            <label class='col-md-3'>Nama Lengkap</label>
            <div class='col-md-9'><input type="text" name="nama_lengkap" autocomplete="off" value="<?=$u->nama_lengkap;?>" required placeholder="Masukkan Password" class="form-control" ></div>
          </div>
          <br>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-warning">Edit</button>
          </div>
          </form>
          </div>
          </div>
          </div>
          <?php endforeach; ?>
        </div>
        </section>

   