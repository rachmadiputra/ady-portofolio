  
<script src="<?php echo base_url(); ?>assets/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/sweetalert/dist/sweetalert.css">

<script type="text/javascript">
$(document).ready(function(){
    $('.confirm_delete').on('click', function(){
        
    var delete_url = $(this).attr('data-url');

     swal({
      title: "Apakah kamu yakin?",
      text: "Kamu akan mengapus data ini secara permanent",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Ya, hapus!",
      cancelButtonText: "Tidak, batal!",
      closeOnConfirm: false,
      closeOnCancel: false
        },
        function(isConfirm) {
          if (isConfirm) {
             window.location.href = delete_url;
            swal("Dihapus!", "Data Kamu telah terhapus.", "success");
          } else {
            swal("Batal", "Data kamu kembali aman", "error");
          }
        });

        return false
    });

});

$(document).ready(function(){
    $('.confirm_logout').on('click', function(){
        
        var logout_url = $(this).attr('data-url');

        swal({
            title: "Apakah anda yakin ingin keluar ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Keluar !",
            cancelButtonText: "Batalkan",
            closeOnConfirm: false           
        }, function(){
            window.location.href = logout_url;
        });

        return false
    });

});
</script>
 
    

    <!-- Bootstrap Notify Plugin Js -->
    <script src="<?php echo base_url() ?>assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
    <script src="<?php echo base_url() ?>assets/js/pages/ui/notifications.js"></script>

   <!--  dropzone -->
    <script src="<?php echo base_url() ?>assets/plugins/dropzone/dropzone.js"></script>

    <!-- Waves Effect Plugin Js -->
    <!-- Jquery Core Js -->
    <script src="<?php  echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php  echo base_url() ?>assets/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="<?php  echo base_url() ?>assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?php  echo base_url() ?>assets/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php  echo base_url() ?>assets/plugins/node-waves/waves.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?php  echo base_url() ?>assets/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?php  echo base_url() ?>assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?php  echo base_url() ?>assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?php  echo base_url() ?>assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?php  echo base_url() ?>assets/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?php  echo base_url() ?>assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?php  echo base_url() ?>assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?php  echo base_url() ?>assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?php  echo base_url() ?>assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Custom Js -->
    <script src="<?php  echo base_url() ?>assets/js/admin.js"></script>
    <script src="<?php  echo base_url() ?>assets/js/pages/tables/jquery-datatable.js"></script>

    <!-- Demo Js -->
    <script src="<?php  echo base_url() ?>assets/js/demo.js"></script>
  
</body>

</html>