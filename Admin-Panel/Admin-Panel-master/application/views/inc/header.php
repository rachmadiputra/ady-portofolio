<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | Bootstrap Based Admin Template - Material Design</title>
    <!-- Favicon-->
   <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.css');?>" rel="stylesheet">
     <link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url('assets/plugins/node-waves/waves.css');?>" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="<?php echo base_url('assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css');?>" rel="stylesheet">

    <!-- Animation Css -->
    <link href="<?php echo base_url('assets/plugins/animate-css/animate.css');?>" rel="stylesheet" />
    <!-- Custom Css -->
    <link href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo base_url('assets/css/themes/all-themes.css');?>" rel="stylesheet" />

    <!-- jquery js -->
    <script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.js"></script>
 <!--    dropzone -->
    <link href="<?php echo base_url(); ?>assets/plugins/dropzone/dropzone.css" rel="stylesheet">
    <!-- Multi Select Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/multi-select/css/multi-select.css" rel="stylesheet">

    <!-- Bootstrap Spinner Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/jquery-spinner/css/bootstrap-spinner.css" rel="stylesheet">

    <!-- Bootstrap Tagsinput Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">

    <!-- Bootstrap Select Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- noUISlider Css -->
    <link href="<?php echo base_url(); ?>assets/plugins/nouislider/nouislider.min.css" rel="stylesheet" /> 
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar" style="background-color: #560027;">
        <div class="container-fluid">
           
            <div class="collapse navbar-collapse" id="navbar-collapse">
         <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                  <span class="glyphicon glyphicon-user" aria-hidden="true"></span> User <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                   <div class="user-info"  style="text-align: center;"">
                <div class="image">
                    <img src="<?php echo base_url() ?>assets/images/user.png" width="70" height="70" alt="User" />
                </div>
                <div class="info-container">
                   
                    <label style="color:#000;"><?php echo $this->session->userdata("nama"); ?><label>
                </div>
            </div>
            <li style="color: #fff;">
            <a  href="#" data-url="<?php echo base_url('login/logout'); ?>" class="btn btn-prinmary confirm_logout ">Logout</a>
            </li>        
                  </ul>
                </li>
              </ul>

        </nav>
        <section>
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info"  style="text-align: center;"">
                <div class="image">
                    <img src="<?php echo base_url() ?>assets/images/user.png" width="70" height="70" alt="User" />
                </div>
                <div class="info-container">
                <label style="color:#fff;"><?php echo $this->session->userdata("status"); ?><label>
                </div>
            </div>
             <li class="header" style="background-color: #000; color: #fff; text-align: center;"><?php echo $this->session->userdata("nama"); ?> </li>
            <!-- #User Info -->
            <!-- Menu -->
             
            <div class="menu">
                <ul class="list">
                    
                    <li class="header">MENU ANJEM</li>
                     <li class=
                     <?php 
                     if ($this->uri->segment(1)=='dashboard') {
                         echo "active";
                     }
                     ?>>
                    <a href="<?php echo base_url('dashboard'); ?>">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li class=
                     <?php 
                     if ($this->uri->segment(1)=='admin') {
                         echo "active";
                     }
                     ?>>
                        <a href="<?php echo base_url('admin'); ?>">
                            <i class="material-icons">computer</i>
                            <span>Admin</span>
                        </a>
                    </li>
                    <li>
                        <a href="pages/typography.html">
                            <i class="material-icons">text_fields</i>
                            <span>Data Order</span>
                        </a>
                    </li>
                    <li>
                        <a href="pages/helper-classes.html">
                            <i class="material-icons">layers</i>
                            <span>Data Grup Order</span>
                        </a>
                    </li>
                     <li class= 
                     <?php 
                     if ($this->uri->segment(1)=='driver') {
                         echo "active";
                     }
                     ?>>
                        <a href="<?php echo base_url('driver'); ?>">
                            <i class="material-icons">home</i>
                            <span>Data Driver</span>
                        </a>
                    </li>
                      <li class= 
                      <?php 
                     if ($this->uri->segment(1)=='user') {
                         echo "active";
                     }
                     ?>>
                        <a href="<?php echo base_url('user'); ?>">
                            <i class="material-icons">person</i>
                            <span>Data User</span>
                        </a>
                    </li>
                       <li class= 
                       <?php 
                     if ($this->uri->segment(1)=='promo') {
                         echo "active";
                     }
                     ?>>
                        <a href="<?php echo base_url('promo'); ?>">
                            <i class="material-icons">rowing</i>
                            <span>Data Promo</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
        </aside>
    </section>

  