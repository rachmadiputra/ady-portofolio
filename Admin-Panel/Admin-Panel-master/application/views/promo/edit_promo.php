

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">              
                    <h2>
                    Edit Data Promo
                 
                </h2>
            </div>
                           <?php 
                          $data2=$this->session->flashdata('error');
                          if($data2!=""){ ?>
                          <div id="notifikasi" class="alert alert-danger"><strong> Error! </strong> <?=$data2;?></div>
                          <?php } ?>

          <!-- File Upload | Drag & Drop OR With Click & Choose -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                         <form action="<?php echo base_url('promo/update/'.$id)?>" method="post" enctype="multipart/form-data">
                        <div class="header">
                             <label >Nama</label>

                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="nama" value="<?=$nama;?>" id="image-upload">
                                    </div>
                                </div>

                            <h2>
                                FILE UPLOAD - CLICK & CHOOSE FILE
                               
                            </h2>
                           <span class="label label-success">Ukuran maksimal 2 MB. Format file: jpeg, jpg, dan png.</span>
                            <?php echo form_error('filefoto'); ?>
                        </div>
                        <div class="dropzone" >
                           
                                <div class="dz-message" style="background-image: url(<?php echo base_url('images/'.$url); ?>)">
                                    <div class="drag-icon-cph">
                                        <i class="material-icons">touch_app</i>
                                    </div>
                                    <h3>Choose files here or click to upload.</h3>
                                    <em>(This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)</em>
                                </div>
                                <div >
                            
                                    <input name="filefoto" class="btn btn-warning" type="file" multiple />
                                </div>
                                <br>
                                <button type="submit" class="btn btn-danger">Simpan</button>
                                <?php echo form_hidden('id', $id); ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
           
                </div>
           
    <!-- END Modal Tambah -->

      <!-- Modal Edit -->   

    <!-- #END# Exportable Table -->
        </div>
    </section>
