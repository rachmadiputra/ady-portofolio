    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                    
                <?php 
                          $data=$this->session->flashdata('sukses');
                          if($data!=""){ ?>
                          <div id="notifikasi" class="alert alert-danger"><strong>Sukses! </strong> <?=$data;?></div>
                          <?php } ?>

                          <?php 
                          $data2=$this->session->flashdata('error');
                          if($data2!=""){ ?>
                          <div id="notifikasi" class="alert alert-danger"><strong> Error! </strong> <?=$data2;?></div>
                          <?php } ?>

                <h2>
                    Data Promo
                   <!--  <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small> -->
                </h2>
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                       
                   
            <!-- #END# Basic Examples -->
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                     <?php if(isset($pesan)){
                        echo "<h5>$pesan</h5>";
                    }?>
                        <div class="header">
                            <h2>
                                List Data Promo
                            </h2>
                     
                        </div>
                     
                        <div class="body">

                            <div class="table-responsive">

                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                     <tr>
                                      <th>No</th>
                                      <th>Nama</th>
                                      <th>Url</th>
                                      <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                        $no = 1;
                                        foreach($promo as $u){ 
                                        ?>
                                      
                                        <tr>
                                        <td><?php echo $u->id ?></td>
                                        <td><?php echo $u->nama ?></td>
                                        <?php echo "<td><img src='".base_url("images/".$u->url)."' width='100' height='100'>
                                        </td>";
                                        ?>
                                        <td>
                                        <a href="<?php echo base_url('promo/edit/'.$u->id); ?>" class="btn btn-info">Edit</a>
                                        </a>
                                       <a  href="#" data-url="<?php echo site_url('promo/hapus/'.$u->id); ?>" class="btn btn-danger confirm_delete ">Hapus</a>
                                      <?php } ?>
                                    
                                    
                                    </tbody>
                                </table>
                                <a data-toggle="modal" data-target="#tambah-data" class="btn btn-primary">Tambah</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal Tambah -->
                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="tambah-data" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <h4 class="modal-title">Tambah Data</h4>
                </div>

                <form class="form-horizontal" action="<?php echo base_url('promo/tambah_promo')?>" method="post" enctype="multipart/form-data" role="form">
                    <div class="modal-body">
                            <div class="form-group">
                                <label class="col-lg-2 col-sm-2 control-label">Nama</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="nama" placeholder="Tuliskan Nama">
                                </div>                
                                 <label class="col-lg-2 col-sm-2 control-label">File Foto</label> <span class="label label-success">Ukuran maksimal 2 MB. Format file: jpeg, jpg, dan png.</span>
                                <?php echo form_error('file'); ?>
                                <!-- style:background-image is the main actor to show preview image while edit  -->
                                <div id="image-preview" >
                                 <label for="image-upload" id="image-label"></label>
                                <input type="file" name="file" id="image-upload"/>
                                </div>
                            </div>
                             
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-info"  type="submit"> Simpan&nbsp;</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal"> Batal</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal Tambah -->

      <!-- Modal Edit -->   

    <!-- #END# Exportable Table -->
        </div>
    </section>
