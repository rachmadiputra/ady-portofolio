    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <?php 
                          $data=$this->session->flashdata('sukses');
                          if($data!=""){ ?>
                          <div id="notifikasi" class="alert alert-warning"><strong>Sukses! </strong> <?=$data;?></div>
                          <?php } ?>

                          <?php 
                          $data2=$this->session->flashdata('error');
                          if($data2!=""){ ?>
                          <div id="notifikasi" class="alert alert-danger"><strong> Error! </strong> <?=$data2;?></div>
                          <?php } ?>

                <h2>
                    Data User
                   <!--  <small>Taken from <a href="https://datatables.net/" target="_blank">datatables.net</a></small> -->
                </h2>
            </div>
            <!-- Basic Examples -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                       
                   
            <!-- #END# Basic Examples -->
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                List Data User
                            </h2>
                          
                        </div>
                     
                        <div class="body">

                            <div class="table-responsive">

                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                     <tr>
                                      <th>No</th>
                                      <th>Nama</th>
                                      <th>Email</th>
                                      <th>Telp</th>
                                      <th>Password</th>
                                      <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php 
                                        $no = 1;
                                        foreach($user as $u){ 
                                        ?>
                                      
                                        <tr>
                                        <td><?php echo $u->id ?></td>
                                        <td><?php echo $u->nama ?></td>
                                        <td><?php echo $u->email ?></td>
                                        <td><?php echo $u->telp ?></td>
                                        <td><?php echo $u->password ?></td>
                                        <td>
                                        <a data-toggle="modal" data-target="#modal-edit<?=$u->id;?>" >
                                        <button class="btn btn-info">Ubah</button>
                                        </a>
                                         <a  href="#" data-url="<?php echo site_url('user/hapus/'.$u->id); ?>" class="btn btn-danger confirm_delete ">Hapus</a>
                                        </td>
                                      </tr>

                                      <?php } ?>
                                    
                                    
                                    </tbody>
                                </table>
                                <a data-toggle="modal" data-target="#tambah-data" class="btn btn-primary">Tambah</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal Tambah -->
                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="tambah-data" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <h4 class="modal-title">Tambah Data</h4>
                </div>
                <form class="form-horizontal" action="<?php echo base_url('user/tambah_user')?>" method="post" enctype="multipart/form-data" role="form">
                    <div class="modal-body">
                            <div class="form-group">
                                <label class="col-lg-2 col-sm-2 control-label">Nama</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="nama" placeholder="Tuliskan Nama">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 col-sm-2 control-label">Email</label>
                                <div class="col-lg-10">
                                    <input type="email" class="form-control" name="email" placeholder="Tuliskan Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 col-sm-2 control-label">Telephone</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="telp" placeholder="Tuliskan Telephone">
                                </div>
                            </div>
                              <div class="form-group">
                                <label class="col-lg-2 col-sm-2 control-label">Password</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" name="password" placeholder="Tuliskan Password">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-info" type="submit"> Simpan&nbsp;</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal"> Batal</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END Modal Tambah -->

      <!-- Modal Edit -->   
<?php $no=0; foreach($user as $u): $id; ?>
<div class="row">
  <div id="modal-edit<?=$u->id;?>" class="modal fade">
    <div class="modal-dialog">
      <form action="<?php echo base_url('user/edit')?>" method="post" >
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Data</h4>
        </div>
        <div class="modal-body">

          <input type="hidden" readonly value="<?=$u->id;?>" name="id" class="form-control" >

          <div class="form-group">
            <label class='col-md-3'>Nama</label>
            <div class='col-md-9'><input type="text" name="nama" autocomplete="off" value="<?=$u->nama;?>" required placeholder="Masukkan Nama" class="form-control" ></div>
          </div>
            <div class="form-group">
            <label class='col-md-3'>Email</label>
            <div class='col-md-9'><input type="text" name="email" autocomplete="off" value="<?=$u->email;?>" required placeholder="Masukkan Email" class="form-control" ></div>
          </div>
            <div class="form-group">
            <label class='col-md-3'>Telp</label>
            <div class='col-md-9'><input type="text" name="telp" autocomplete="off" value="<?=$u->telp;?>" required placeholder="Masukkan Telephone" class="form-control" ></div>
          </div>
             <div class="form-group">
            <label class='col-md-3'>Password</label>
            <div class='col-md-9'><input type="text" name="password" autocomplete="off" value="<?=$u->password;?>" required placeholder="Masukkan Password" class="form-control" ></div>
          </div>
          <br>
        </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-warning">Edit</button>
          </div>
        </form>
    </div>
  </div>
</div>
<?php endforeach; ?>
    <!-- #END# Exportable Table -->
    </div>
    </section>
