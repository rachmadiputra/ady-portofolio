<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include_once '../../../web-bin/function.php';
if (session_id() == '') sec_session_start();

if (!isset($dbgl)) {
  try {
    $dbgl = new PDO($pggl, $pguser, $pgpass);
    $dbgl->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbgl->query("SET application_name = '" . realpath(dirname(__FILE__)) . "/" . basename(__FILE__) . "/" . $_SESSION['username'] . "'");
  } catch (PDOException $e) {
    $errorMsg = 'DBGL ' . $_SESSION['username'] . ' ' . $e->getMessage() . "\nError on line " . $e->getLine() . " in " . $e->getFile();
    sendMessageBot($groupWebChatId, $errorMsg, $webSupportBot);
    // throw new pdoDbException($e);
  }
}

$data = json_decode(file_get_contents("php://input"));

$request = $data->request;

if ($request == 1) {

  try {
    $sql = "SELECT * from t_emp_pa_kompetensi";
    $stmt = $dbh->prepare($sql);
    $stmt->execute();
    //$kompetensi=$stmt->fetch();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal';
  }
  $kompetensi = [];

  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $kompetensi[] = [
      'value' => $row['nama_kompetensi'],
      'text' => $row['nama_kompetensi'] . " (" . $row['bobot_persen'] . "%)"
    ];
  };
  echo json_encode(array_values($kompetensi));
} elseif ($request == "nomor") {
  try {
    $sql = "SELECT id FROM t_emp_pa ORDER BY id DESC";
    $stmt = $dbh->prepare($sql);
    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal';
  }

  $result = $stmt->fetchColumn();
  if ($result == Null) {
    $result = 1;
  } else {
    $result = $result;
  }
  echo json_encode($result);
} elseif ($request == "savepa") {

  $nomor = $data->nomor;
  $level = $data->level;
  $username = $data->useradmin;
  $krit = $data->kriterias;
  $kriterias = json_decode($krit, true);
  try {
    $sql = 'INSERT INTO t_emp_pa(nomor_pa,target_level, create_date, create_by) VALUES (:nomor_pa,:target_level,:create_date,:create_by)';
    $stmt = $dbh->prepare($sql);
    $stmt->bindValue(':nomor_pa', $nomor);
    $stmt->bindValue(':target_level', $level);
    $stmt->bindValue(':create_date', date('Y-m-d H:i:s'));
    $stmt->bindValue(':create_by', $username);

    // execute the insert statement
    $stmt->execute();
    $lastPAinsert_id =  $dbh->lastInsertId('t_emp_pa_id_seq');
    echo $lastPAinsert_id;
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal insert PA';
  }
  try {
    $sql = 'INSERT INTO t_emp_pa_kompetensi_kriteria(pa_id,kompetensi,kriteria,bobot_nilai) VALUES(:pa_id,:kompetensi,:kriteria,:bobot_nilai)';
    $stmt = $dbh->prepare($sql);

    $idList = [];
    for ($i = 0; $i < count($kriterias); $i++) {
      $stmt->bindValue(':pa_id', $lastPAinsert_id);
      $stmt->bindValue(':kompetensi', $kriterias[$i]['kategori']);
      $stmt->bindValue(':kriteria', $kriterias[$i]['target']);
      $stmt->bindValue(':bobot_nilai', $kriterias[$i]['bobot']);
      $stmt->execute();
      $idList[] = $dbh->lastInsertId('t_emp_pa_kompetensi_kriteria_id_seq');
    }
    echo print_r($idList);
    // //echo count($krit);
    // echo "Total Array:". count($kriterias);
    // echo var_dump($kriterias);

    //print_r($kriterias);

  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal insert kriterias' . $e;
  }
} elseif ($request == "getpa") {
  $datas = [];
  $listpa = [];
  $listactive = [];

  try {
    $sql = "SELECT 
              a.id, a.nomor_pa, a.target_level, to_char(a.create_date, 'DD Month YYYY') as create_date, a.create_by, count(b.nomor_pa) as isdelete
            FROM t_emp_pa a
            LEFT JOIN t_emp_pa_periode b ON a.nomor_pa = b.nomor_pa
            GROUP BY a.id
            ORDER BY a.id DESC";
    $stmt = $dbh->prepare($sql);
    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal';
  }
  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $listpa[] = [
      'id' => $row['id'],
      'level' => $row['target_level'],
      'nomor' => $row['nomor_pa'],
      'createby' => $row['create_by'],
      'createdate' => $row['create_date'],
      'isdelete' => $row['isdelete']
    ];
  };
  $datas[] = $listpa;
  try {
    $sql1 = "SELECT 
              id, nomor_pa, to_char(periode_month, 'Month YYYY') as periode_month, to_char(dead_line, 'DD Month YYYY') as dead_line, target_level, is_active, activated_by, to_char(activated_date, 'DD Month YYYY')as activated_date 
             FROM t_emp_pa_periode
             ORDER BY periode_month::date DESC";
    $stmt1 = $dbh->prepare($sql1);
    $stmt1->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal periode';
  }
  while ($r = $stmt1->fetch(PDO::FETCH_ASSOC)) {

    if ($r['is_active'] == 1) {
      $aktif = "Aktif";
    } else {
      $aktif = "Tidak Aktif";
    }

    if ($r['target_level'] == 'GM') {
      $lvl = "General Manager";
    }
    if ($r['target_level'] == 'MGR') {
      $lvl = "Manager";
    }
    if ($r['target_level'] == 'SSPV') {
      $lvl = "Senior Supervisor";
    }
    if ($r['target_level'] == 'SPV') {
      $lvl = "Supervisor";
    }
    if ($r['target_level'] == 'STF') {
      $lvl = "Staff";
    }
    if ($r['target_level'] == 'CL') {
      $lvl = "Clerk";
    }
    $listactive[] = [
      'id' => $r['id'],
      'nomor' => $r['nomor_pa'],
      'deadline' => $r['dead_line'],
      'bulantahun' => $r['periode_month'],
      'aktif' => $aktif,
      'level' => $lvl,
      'activated_by' => $r['activated_by'],
      'activated_date' => $r['activated_date']
    ];
  };
  $datas[] = $listactive;
  //echo json_encode($listactive);
  // print_r($listactive);
  echo json_encode($datas);
} elseif ($request == "showdetailpa") {

  $paid = $data->nomor;

  try {
    $sql = "SELECT * FROM t_emp_pa_kompetensi_kriteria WHERE pa_id = $paid";
    $stmt = $dbh->prepare($sql);
    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal';
  }

  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $kriteria[] = [
      'id' => $row['id'],
      'kompetensi' => $row['kompetensi'],
      'kriteria' => $row['kriteria'],
      'bobotnilai' => $row['bobot_nilai'],
    ];
  }

  try {
    $sql = "SELECT id, nomor_pa, target_level, to_char(create_date, 'DD Month YYYY') as create_date, create_by FROM t_emp_pa WHERE id = $paid";
    $stmt = $dbh->prepare($sql);
    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal';
  }

  $row = $stmt->fetch(PDO::FETCH_ASSOC);

  $pa = array('headers' => $row, 'kompetensis' => $kriteria);
  //echo json_encode($kriteria);
  echo json_encode($pa);
} elseif ($request == 'getMateriPA') {
  // $level = $data->level;

  // if($level == 'MGR') { $lvl = "Manager"; }
  // if($level == 'SPV') { $lvl = "Supervisor"; }
  // if($level == 'STF') { $lvl = "Staff"; }
  // if($level == 'CL') { $lvl = "Clerk"; }

  try {
    //$sql="SELECT * FROM t_emp_pa WHERE target_level = '$lvl' ORDER BY create_date DESC";
    $sql = "SELECT * FROM t_emp_pa ORDER BY create_date DESC";
    $stmt = $dbh->prepare($sql);
    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal';
  }
  $data = $stmt->rowCount();

  if ($data > 0) {
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $pas[] = [
        'id' => $row['id'],
        'nomor' => $row['nomor_pa'],
        'level' => $row['target_level'],
        'create_by' => $row['create_by'],
        'create_date' => $row['create_date'],
      ];
    }
  } else {
    $pas = '';
  }
  echo json_encode($pas);
} elseif ($request == "savepaperiode") {

  $materis = $data->materis;
  $materi = json_decode($materis, true);

  $username = $data->useradmin;
  $totalRow = count($materi);
  $msg = array();
  //$today = strtotime(
  $today = date("Y-m-d H:i:s");

  $sql = "INSERT INTO t_emp_pa_periode (nomor_pa, periode_month, dead_line, target_level, is_active, activated_by, activated_date) VALUES (:nomor_pa, :periode_month, :dead_line, :target_level, :is_active, :activated_by, :activated_date)";
  $stmt = $dbh->prepare($sql);
  $idList = [];

  //for($i=0;$i < $totalRow; $i++ ){

  foreach ($materi as $m) {
    //$level = $materi[$i]['level'];
    $level = $m['level'];
    if ($level == 'MGR') {
      $lvl = "Manager";
    }
    if ($level == 'SSPV') {
      $lvl = "Senior Supervisor";
    }
    if ($level == 'SPV') {
      $lvl = "Supervisor";
    }
    if ($level == 'STF') {
      $lvl = "Staff";
    }
    if ($level == 'CL') {
      $lvl = "Clerk";
    }


    $sql1 = "SELECT * FROM t_emp_pa_periode WHERE target_level = '$level' and is_active = 1";
    $stmt1 = $dbh->prepare($sql1);
    $stmt1->execute();
    $count = $stmt1->rowCount();


    if ($count == 0) {
      try {
        $stmt->execute(array(":nomor_pa" => $m['nomateri'], ":periode_month" => $m['startmonth'], ":dead_line" => $m['enddate'], ":target_level" => $m['level'], ":is_active" => 1, ":activated_by" => $username, ":activated_date" => $today));
        $idList[] = $dbh->lastInsertId('t_emp_pa_periode_id_seq');
        $msg[] = "Penilaian untuk " . $lvl . " periode :" . $m['startmonth'] . " sudah aktif <br />";
      } catch (PDOException $e) {
        $e->getMessage();
        $msg[] = array("id" => 1, "msg" => "Mohon untuk mengisi semua kolom data");
      }
    } else {
      $msg[] = array("id" => 2, "msg" => "Penilaian untuk " . $lvl . " periode yang lalu masih aktif, mohon untuk menonaktifkan terlebih dahulu");
    }
  }
  echo json_encode($msg);
} elseif ($request == "getlistemployee") {

  $result = [];
  try {
    $sql = "SELECT a.id_num, b.id_level, g.position, a.nik, a.att_name, lower(a.emp_name) as emp_name, to_char(a.join_date, 'DD Mon YYYY') as join_date, lower(a.emp_sts) as emp_sts, a.emp_sts_notes, a.kd_level, a.kd_off, a.kd_dept, b.level, a.path_photo, d.division, e.department, f.office, c.paid, c.nilaitotal, c.indikator, c.nomorpaemp,
                (SELECT grand_total_nilai_by_indikator FROM hrm_apprasial_grand_total_penilaian WHERE id_num_target = a.id_num AND tipe_penilai = 'HRD') AS total_penilaian_hrd
                FROM hrm_employee a  
                LEFT JOIN t_emp_pa_penilaian c ON a.att_name = c.emp_userid
                LEFT JOIN hrm_level b ON a.kd_level = b.kd_level
                LEFT JOIN hrm_division d ON a.kd_div = d.kd_div
                LEFT JOIN hrm_department e ON a.kd_dept = e.kd_dept
                LEFT JOIN hrm_office f ON a.kd_off = f.kd_off
                LEFT JOIN hrm_position g ON a.kd_pos = g.kd_pos 
                WHERE a.resign_date IS NULL AND a.emp_sts != 'LEPAS'
                ORDER BY a.kd_off";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $employee = [];
    while ($d = $stmt->fetch(PDO::FETCH_ASSOC)) {

      if ($d['paid'] == NULL) {
        $paid = 0;
      } else {
        $paid = $d['paid'];
      };
      if ($d['nomorpaemp'] == NULL) {
        $nopaemp = '001';
      } else {
        $nopaemp = $d['nomorpaemp'];
      }

      $sqlx = "SELECT y.kompetensi as kriteria, z.bobot_persen, sum(x.nilai_total) as nilaistaff, sum(y.bobot_nilai*5) as nilaimax,  round(ceil(sum(x.nilai_total*100))/sum(y.bobot_nilai*5)) as persen
                FROM t_emp_pa_penilaian_detail x 
                LEFT JOIN t_emp_pa_kompetensi_kriteria y ON x.kriteria_id = y.id
                LEFT JOIN t_emp_pa_kompetensi z ON z.nama_kompetensi = y.kompetensi
                WHERE x.paid = $paid AND x.nomorpaemp = '$nopaemp'
                GROUP BY y.kompetensi, z.bobot_persen
                ";

      $stmtx = $dbh->prepare($sqlx);
      $stmtx->execute();

      $kriteria = [];
      while ($t = $stmtx->fetch(PDO::FETCH_ASSOC)) {
        $kriteria[] = $t;
      }
      //$t[] = array("karakter"=>20,"Kapasitas"=>30,"Komitmen"=>20,"Kontribusi"=>20);
      $sqly = "SELECT * FROM t_emp_pa_penilaian_kualitatif where nomorpaemp = '$nopaemp'";

      $stmty = $dbh->prepare($sqly);
      $stmty->execute();

      $attach = [];
      while ($xy = $stmty->fetch(PDO::FETCH_ASSOC)) {
        $attach[] = $xy;
      }

      $employee[] = [
        "nomorpa" => $nopaemp,
        "id_num" => $d['id_num'],
        "id_level" => $d['id_level'],
        "position" => $d['position'],
        "nik" => $d['nik'],
        "att_name" => $d['att_name'],
        "emp_name" => $d['emp_name'],
        "join_date" => $d['join_date'],
        "emp_sts" => $d['emp_sts'],
        "emp_sts_notes" => $d['emp_sts_notes'],
        "kd_level" => $d['kd_level'],
        "kd_off" => $d['kd_off'],
        "kd_dept" => $d['kd_dept'],
        "level" => $d['level'],
        "path_photo" => $d['path_photo'],
        "division" => $d['division'],
        "department" => $d['department'],
        "office" => $d['office'],
        "paid" => $d['paid'],
        "nilaitotal" => $d['nilaitotal'],
        "indikator" => $d['indikator'],
        "kompetensis" => $kriteria,
        'kualitatifs' => $attach,
        "total_penilaian_hrd" => $d['total_penilaian_hrd']
      ];
      // $employee[] = $d;

    };
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  try {
    $sql1 = "SELECT kd_off as kode, office as namaoffice FROM hrm_office WHERE kd_off <> 'DPS'";
    $stmt1 = $dbh->prepare($sql1);
    $stmt1->execute();

    $office = [];
    while ($ro = $stmt1->fetch(PDO::FETCH_ASSOC)) {
      $office[] = $ro;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  try {
    $sql3 = "SELECT kd_div as kode, division AS nama FROM hrm_division";
    $stmt3 = $dbh->prepare($sql3);
    $stmt3->execute();

    $divisi = [];
    while ($rob = $stmt3->fetch(PDO::FETCH_ASSOC)) {
      $divisi[] = $rob;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  try {
    $sql2 = "SELECT kd_dept as kode, department as namadepartemen FROM hrm_department";
    $stmt2 = $dbh->prepare($sql2);
    $stmt2->execute();

    $departemen = [];
    while ($roi = $stmt2->fetch(PDO::FETCH_ASSOC)) {
      $departemen[] = $roi;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $result[] = $office;
  $result[] = $employee;
  $result[] = $departemen;
  $result[] = $divisi;

  echo json_encode($result);
} elseif ($request == "getDetailPenilaian") {

  $nomor = $data->nomorpa;
  $datapenilaian = [];

  try {
    $sql = "SELECT a.id_num, a.nik, a.att_name, lower(a.emp_name) as emp_name, to_char(a.join_date, 'DD Mon YYYY') as join_date, lower(a.emp_sts) as emp_sts, a.emp_sts_notes, to_char(h.start_date, 'DD/MM/YYYY') as start_date, to_char(h.end_date, 'DD/MM/YYYY') as end_date,  b.level, g.position, a.kd_off, a.kd_dept, a.kd_level, a.path_photo, d.division, e.department, f.office, c.nilaitotal, c.indikator, c.penilaian_oleh, c.penilaian_date
     FROM hrm_employee a  
     LEFT JOIN t_emp_pa_penilaian c ON a.att_name = c.emp_userid
     LEFT JOIN hrm_level b ON a.kd_level = b.kd_level
     LEFT JOIN hrm_division d ON a.kd_div = d.kd_div
     LEFT JOIN hrm_department e ON a.kd_dept = e.kd_dept
     LEFT JOIN hrm_office f ON a.kd_off = f.kd_off 
     LEFT JOIN hrm_position g ON g.kd_pos = a.kd_pos
     LEFT JOIN hrm_memo h ON h.pin2 = a.pin2 AND h.memo = a.emp_sts_notes
     WHERE a.resign_date IS NULL AND c.nomorpaemp = '$nomor'";
    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $employee = [];
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $employee[] = $row;
    }
  } catch (PDPException $e) {
    $e->getMessage();
  }
  try {
    $sql1 = "SELECT y.kompetensi as kriteria, z.bobot_persen, sum(x.nilai_total) as nilaistaff, sum(y.bobot_nilai*5) as nilaimax,  round(ceil(sum(x.nilai_total*100))/sum(y.bobot_nilai*5)) as persen
            FROM t_emp_pa_penilaian_detail x 
            LEFT JOIN t_emp_pa_kompetensi_kriteria y ON x.kriteria_id = y.id
            LEFT JOIN t_emp_pa_kompetensi z ON z.nama_kompetensi = y.kompetensi
            WHERE x.nomorpaemp = '$nomor'
            GROUP BY y.kompetensi, z.bobot_persen";
    $stmt1 = $dbh->prepare($sql1);
    $stmt1->execute();

    $kompetensi = [];
    while ($ri = $stmt1->fetch(PDO::FETCH_ASSOC)) {
      $kompetensi[] = $ri;
    }
  } catch (PDPException $e) {
    $e->getMessage();
  }
  try {
    $sql2 = "SELECT b.kriteria_id, c.kompetensi, c.kriteria, c.bobot_nilai, b.nilai_staff, b.nilai_total FROM t_emp_pa_penilaian_detail b
            LEFT JOIN t_emp_pa_kompetensi_kriteria c ON c.id = b.kriteria_id
            WHERE b.nomorpaemp = '$nomor'";

    $stmt2 = $dbh->prepare($sql2);
    $stmt2->execute();

    $kompetensidetail = [];
    while ($ro = $stmt2->fetch(PDO::FETCH_ASSOC)) {
      $kompetensidetail[] = $ro;
    }
  } catch (PDPException $e) {
    $e->getMessage();
  }
  try {
    $sql3 = "SELECT * FROM t_emp_pa_penilaian_kualitatif
            WHERE nomorpaemp = '$nomor'";

    $stmt3 = $dbh->prepare($sql3);
    $stmt3->execute();

    $kualitatif = [];
    while ($rx = $stmt3->fetch(PDO::FETCH_ASSOC)) {
      $kualitatif[] = $rx;
    }
  } catch (PDPException $e) {
    $e->getMessage();
  }
  $datapenilaian[] = $employee;
  $datapenilaian[] = $kompetensi;
  $datapenilaian[] = $kompetensidetail;
  $datapenilaian[] = $kualitatif;

  echo json_encode($datapenilaian);
} elseif ($request == "nonAktifkanpa") {

  $id = $data->paid;
  $val = $data->vaktif;

  $sql = "UPDATE t_emp_pa_periode SET is_active = $val WHERE id = $id";
  $stmt = $dbh->prepare($sql);
  $stmt->execute();

  echo "Success";
} elseif ($request == "getListPenilaianWithTotalStaff") {
  $sql = "SELECT 
            a.id, a.nomor_pa, a.target_level as kd_level, 
            CASE WHEN a.target_level = 'MGR' THEN 'Manager'
                  WHEN a.target_level = 'SSPV' THEN 'Senior Supervisor'
                  WHEN a.target_level = 'SPV' THEN 'Supervisor'
                  WHEN a.target_level = 'STF' THEN 'Staff'
                  ELSE 'Clerk'
            END as level, 
            to_char(a.periode_month, 'MMYYYY') as periode, to_char(a.dead_line, 'DD Month YYYY') as deadline, a.activated_by, to_char(a.activated_date, 'DD Month YYYY') as activated_date 
                , CASE WHEN a.is_active= 1 THEN 'Aktif'
                    ELSE 'Tidak Aktif'
                END AS isactive, count(b.*) as totalstaff
          FROM t_emp_pa_periode a
          LEFT JOIN t_emp_pa_penilaian b ON a.id = b.paid
          GROUP BY a.id";

  $stmt = $dbh->prepare($sql);
  $stmt->execute();

  $list = [];
  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $list[] = $row;
  }

  echo json_encode($list);
} elseif ($request == "getlistemployeepenilaian") {
  $periode = $data->periode;
  $kd_level = $data->klevel;

  $result = [];
  try {
    $sql = "SELECT a.id_num, a.nik, a.att_name, lower(a.emp_name) as emp_name, to_char(a.join_date, 'DD Mon YYYY') as join_date, lower(a.emp_sts) as emp_sts, a.emp_sts_notes, a.kd_level, a.kd_off, a.kd_dept, b.level, a.path_photo, d.division, e.department, f.office, c.paid, c.nilaitotal, c.indikator, c.nomorpaemp
          FROM hrm_employee a  
          LEFT JOIN t_emp_pa_penilaian c ON a.att_name = c.emp_userid
          LEFT JOIN hrm_level b ON a.kd_level = b.kd_level
          LEFT JOIN hrm_division d ON a.kd_div = d.kd_div
          LEFT JOIN hrm_department e ON a.kd_dept = e.kd_dept
          LEFT JOIN hrm_office f ON a.kd_off = f.kd_off 
          WHERE a.resign_date IS NULL AND right(c.nomorpaemp, 6) = '$periode' AND a.kd_level = '$kd_level'
          ORDER BY a.kd_off";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $employee = [];
    while ($d = $stmt->fetch(PDO::FETCH_ASSOC)) {

      if ($d['paid'] == NULL) {
        $paid = 0;
      } else {
        $paid = $d['paid'];
      };

      if ($d['nomorpaemp'] == NULL) {
        $nopaemp = '001';
      } else {
        $nopaemp = $d['nomorpaemp'];
      }

      $sqlx = "SELECT y.kompetensi as kriteria, z.bobot_persen, sum(x.nilai_total) as nilaistaff, sum(y.bobot_nilai*5) as nilaimax,  round(ceil(sum(x.nilai_total*100))/sum(y.bobot_nilai*5)) as persen
              FROM t_emp_pa_penilaian_detail x 
              LEFT JOIN t_emp_pa_kompetensi_kriteria y ON x.kriteria_id = y.id
              LEFT JOIN t_emp_pa_kompetensi z ON z.nama_kompetensi = y.kompetensi
              WHERE x.paid = $paid AND x.nomorpaemp = '$nopaemp'
              GROUP BY y.kompetensi, z.bobot_persen
              ";

      $stmtx = $dbh->prepare($sqlx);
      $stmtx->execute();

      $kriteria = [];
      while ($t = $stmtx->fetch(PDO::FETCH_ASSOC)) {
        $kriteria[] = $t;
      }
      //$t[] = array("karakter"=>20,"Kapasitas"=>30,"Komitmen"=>20,"Kontribusi"=>20);
      $sqly = "SELECT * FROM t_emp_pa_penilaian_kualitatif where nomorpaemp = '$nopaemp'";

      $stmty = $dbh->prepare($sqly);
      $stmty->execute();

      $attach = [];
      while ($xy = $stmty->fetch(PDO::FETCH_ASSOC)) {
        $attach[] = $xy;
      }

      $employee[] = [
        "nomorpa" => $nopaemp,
        "id_num" => $d['id_num'],
        "nik" => $d['nik'],
        "att_name" => $d['att_name'],
        "emp_name" => $d['emp_name'],
        "join_date" => $d['join_date'],
        "emp_sts" => $d['emp_sts'],
        "emp_sts_notes" => $d['emp_sts_notes'],
        "kd_level" => $d['kd_level'],
        "kd_off" => $d['kd_off'],
        "kd_dept" => $d['kd_dept'],
        "level" => $d['level'],
        "path_photo" => $d['path_photo'],
        "division" => $d['division'],
        "department" => $d['department'],
        "office" => $d['office'],
        "paid" => $d['paid'],
        "nilaitotal" => $d['nilaitotal'],
        "indikator" => $d['indikator'],
        "kompetensis" => $kriteria,
        'kualitatifs' => $attach
      ];
      // $employee[] = $d;

    };
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  try {
    $sql1 = "SELECT kd_off as kode, office as namaoffice FROM hrm_office WHERE kd_off <> 'DPS'";
    $stmt1 = $dbh->prepare($sql1);
    $stmt1->execute();

    $office = [];
    while ($ro = $stmt1->fetch(PDO::FETCH_ASSOC)) {
      $office[] = $ro;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  try {
    $sql3 = "SELECT kd_div as kode, division AS nama FROM hrm_division";
    $stmt3 = $dbh->prepare($sql3);
    $stmt3->execute();

    $divisi = [];
    while ($rob = $stmt3->fetch(PDO::FETCH_ASSOC)) {
      $divisi[] = $rob;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  try {
    $sql2 = "SELECT kd_dept as kode, department as namadepartemen FROM hrm_department";
    $stmt2 = $dbh->prepare($sql2);
    $stmt2->execute();

    $departemen = [];
    while ($roi = $stmt2->fetch(PDO::FETCH_ASSOC)) {
      $departemen[] = $roi;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $result[] = $office;
  $result[] = $employee;
  $result[] = $departemen;
  $result[] = $divisi;

  echo json_encode($result);
} elseif ($request == "hapusPa") {

  $id = $data->paid;

  $sql = "DELETE FROM t_emp_pa WHERE id = $id";
  $stmt = $dbh->prepare($sql);
  $stmt->execute();

  echo "success delete";
} elseif ($request == "getOfficeDept") {
  $username = $data->username;
  $result = [];

  try {
    $sql1 = "SELECT id_off, kd_off as kode, office as namaoffice FROM hrm_office WHERE kd_off <> 'DPS'";
    $stmt1 = $dbh->prepare($sql1);
    $stmt1->execute();

    $office = [];
    while ($ro = $stmt1->fetch(PDO::FETCH_ASSOC)) {
      $office[] = $ro;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  try {
    $sql3 = "SELECT kd_div as kode, division AS nama FROM hrm_division";
    $stmt3 = $dbh->prepare($sql3);
    $stmt3->execute();

    $divisi = [];
    while ($rob = $stmt3->fetch(PDO::FETCH_ASSOC)) {
      $divisi[] = $rob;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  try {
    $sql2 = "SELECT id_dept, kd_dept as kode, department as namadepartemen FROM hrm_department";
    $stmt2 = $dbh->prepare($sql2);
    $stmt2->execute();

    $departemen = [];
    while ($roi = $stmt2->fetch(PDO::FETCH_ASSOC)) {
      $departemen[] = $roi;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  try {
    $sql4 = "SELECT distinct(to_char(periode_month,'YYYY-MM')) as periode_date, 
                      to_char(periode_month,'Month YYYY') as periode_name 
                FROM t_emp_pa_periode";

    $stmt4 = $dbh->prepare($sql4);
    $stmt4->execute();

    $periode = [];
    while ($rom = $stmt4->fetch(PDO::FETCH_ASSOC)) {
      $periode[] = $rom;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  try {
    $sql5 = "SELECT B.kd_off,A.office FROM hrm_office A RIGHT JOIN hrm_officeacc B 
               ON A.kd_off = B.kd_off WHERE B.username='$username' ORDER BY B.kd_off";

    $stmt5 = $dbh->prepare($sql5);
    $stmt5->execute();

    $new_off = [];
    while ($arr_off_new = $stmt5->fetch(PDO::FETCH_ASSOC)) {
      $new_off[] = $arr_off_new;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $result[] = $office;
  $result[] = $departemen;
  $result[] = $divisi;
  $result[] = $periode;
  $result[] = $new_off;

  echo json_encode($result);
} elseif ($request == 'generateReport') {

  $periode = $data->periode;
  $office = $data->office;

  if ($office == 'All') {
    $officeselect = '';
  } else {
    $officeselect = "AND d.office = '" . $office . "'";
  }

  $sql = "SELECT 
            a.id as nopa, date(a.periode_month) as bulan_periode, b.emp_userid,  to_char(a.periode_month,'Month YYYY') as periode,
            b.nilaitotal, b.indikator, date(b.penilaian_date) as penilaian_date, b.penilaian_oleh, b.nomorpaemp,
            initcap(c.emp_name) as emp_name, c.nik, to_char(c.join_date,'DD Mon YYYY') as join_date,  initcap(c.emp_sts) as emp_sts, c.emp_sts_notes, c.kd_off, c.kd_dept, c.kd_pos,
            d.office, e.department, f.level, g.position, (SELECT sum(x.nilai_total) as nilaistaff
                    FROM t_emp_pa_penilaian_detail x 
                    LEFT JOIN t_emp_pa_kompetensi_kriteria y ON x.kriteria_id = y.id
                    WHERE x.paid = a.id AND x.nomorpaemp = b.nomorpaemp AND y.kompetensi = 'Karakter'
                    GROUP BY y.kompetensi) as karakter, (SELECT sum(x.nilai_total) as nilaistaff
                    FROM t_emp_pa_penilaian_detail x 
                    LEFT JOIN t_emp_pa_kompetensi_kriteria y ON x.kriteria_id = y.id
                    WHERE x.paid = a.id AND x.nomorpaemp = b.nomorpaemp AND y.kompetensi = 'Komitmen'
                    GROUP BY y.kompetensi) as komitmen, (SELECT sum(x.nilai_total) as nilaistaff
                    FROM t_emp_pa_penilaian_detail x 
                    LEFT JOIN t_emp_pa_kompetensi_kriteria y ON x.kriteria_id = y.id
                    WHERE x.paid = a.id AND x.nomorpaemp = b.nomorpaemp AND y.kompetensi = 'Kompetensi'
                    GROUP BY y.kompetensi) as kompetensi, (SELECT sum(x.nilai_total) as nilaistaff
                    FROM t_emp_pa_penilaian_detail x 
                    LEFT JOIN t_emp_pa_kompetensi_kriteria y ON x.kriteria_id = y.id
                    WHERE x.paid = a.id AND x.nomorpaemp = b.nomorpaemp AND y.kompetensi = 'Kontribusi'
                    GROUP BY y.kompetensi) as kontribusi
            FROM t_emp_pa_periode a
            LEFT JOIN t_emp_pa_penilaian b ON a.id = b.paid
            LEFT JOIN hrm_employee c ON b.emp_userid = c.att_name
            LEFT JOIN hrm_office d ON c.kd_off = d.kd_off
            LEFT JOIN hrm_department e ON c.kd_dept = e.kd_dept
            LEFT JOIN hrm_level f ON c.kd_level = f.kd_level
            LEFT JOIN hrm_position g On c.kd_pos = g.kd_pos
            WHERE to_char(a.periode_month,'Month YYYY') = '$periode' $officeselect";
  $stmt = $dbh->prepare($sql);
  $stmt->execute();

  $list = [];
  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    // $nopaemp = $row['nomorpaemp'];
    // $paid = $row['nopa'];

    // // get kriteria nilai
    // $sqlx = "SELECT y.kompetensi as kriteria, z.bobot_persen, sum(x.nilai_total) as nilaistaff, sum(y.bobot_nilai*5) as nilaimax,  round(ceil(sum(x.nilai_total*100))/sum(y.bobot_nilai*5)) as persen
    // FROM t_emp_pa_penilaian_detail x 
    // LEFT JOIN t_emp_pa_kompetensi_kriteria y ON x.kriteria_id = y.id
    // LEFT JOIN t_emp_pa_kompetensi z ON z.nama_kompetensi = y.kompetensi
    // WHERE x.paid = $paid AND x.nomorpaemp = '$nopaemp'
    // GROUP BY y.kompetensi, z.bobot_persen
    // ";

    // $stmtx = $dbh->prepare($sqlx);
    // $stmtx->execute();

    // $kriteria = [];
    // while($t = $stmtx->fetch(PDO::FETCH_ASSOC)) {
    //     $kriteria[] = $t; 
    // }

    // $list[] = array(
    //   "bulan_periode"=>$row['bulan_periode'],
    //   "emp_userid"=>$row['emp_userid'],
    //   "periode"=>$row['periode'],
    //   "nilaitotal"=>$row['nilaitotal'],
    //   "indikator"=>$row['indikator'],
    //   "emp_name"=>$row['emp_name'],
    //   "nik"=>$row['nik'],
    //   "join_date"=>$row['join_date'],
    //   "emp_sts"=>$row['emp_sts'],
    //   "emp_sts_notes"=>$row['emp_sts_notes'],
    //   "kd_off"=>$row['kd_off'],
    //   "kd_pos"=>$row['kd_pos'],
    //   "kd_dept"=>$row['kd_dept'],
    //   "position"=>$row['position'],
    //   "level"=>$row['level'],
    //   "department"=>$row['department'],
    //   "office"=>$row['office'],
    //   "kriterias"=>$kriteria,
    // );
    $list[] = $row;
  }

  echo json_encode($list);
} elseif ($request == 'getnamastaffdanpenilai') {

  $datas = [];

  try {
    $sql1 = "SELECT a.id_num, a.nik, a.att_name, initcap(a.emp_name) as emp_name, 
          to_char(a.join_date, 'DD Mon YYYY') as join_date, lower(a.emp_sts) as emp_sts, 
          a.emp_sts_notes, a.kd_level, a.kd_off, a.kd_dept, b.level, a.path_photo, 
          d.division, e.department, f.office, c.position, initcap(y.emp_name) as penilai
          FROM hrm_employee a  
          LEFT JOIN hrm_level b ON a.kd_level = b.kd_level
          LEFT JOIN hrm_division d ON a.kd_div = d.kd_div
          LEFT JOIN hrm_department e ON a.kd_dept = e.kd_dept
          LEFT JOIN hrm_office f ON a.kd_off = f.kd_off 
          LEFT JOIN hrm_position c ON c.kd_pos = a.kd_pos
          RIGHT JOIN t_emp_pa_penilai x ON x.id_num_ternilai = a.id_num
          LEFT JOIN hrm_employee y ON x.att_name_penilai = y.att_name
          WHERE a.resign_date IS NULL
          ORDER BY a.kd_off";

    $stmt1 = $dbh->prepare($sql1);
    $stmt1->execute();

    $allstaff = [];
    while ($row = $stmt1->fetch(PDO::FETCH_ASSOC)) {
      $allstaff[] = $row;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  try {
    $sql2 = "SELECT kd_dept as kode, department as namadepartemen FROM hrm_department";
    $stmt2 = $dbh->prepare($sql2);
    $stmt2->execute();

    $departemen = [];
    while ($roi = $stmt2->fetch(PDO::FETCH_ASSOC)) {
      $departemen[] = $roi;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }
  try {
    $sql3 = "SELECT kd_off as kode, office as namaoffice FROM hrm_office WHERE kd_off <> 'DPS'";
    $stmt3 = $dbh->prepare($sql3);
    $stmt3->execute();

    $office = [];
    while ($ro = $stmt3->fetch(PDO::FETCH_ASSOC)) {
      $office[] = $ro;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $datas[] = $allstaff;
  $datas[] = $office;
  $datas[] = $departemen;

  echo json_encode($datas);
} elseif ($request == 'assignpenilai') {

  $datas = [];

  try {
    $sql4 = "SELECT initcap(a.emp_name) as emp_name, a.att_name, b.level as emp_level, c.office
             FROM hrm_employee a
             LEFT JOIN hrm_level b ON b.kd_level = a.kd_level
             LEFT JOIN hrm_office c ON c.kd_off = a.kd_off
             WHERE a.kd_level NOT IN ('KOOR','SSTF','STF','CL') AND a.resign_date IS NULL
             ORDER BY a.emp_name ASC";

    $stmt4 = $dbh->prepare($sql4);
    $stmt4->execute();

    $spvup = [];
    while ($rom = $stmt4->fetch(PDO::FETCH_ASSOC)) {
      $spvup[] = $rom;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  try {
    $sql3 = "SELECT kd_off as kode, office as namaoffice FROM hrm_office WHERE kd_off <> 'DPS'";
    $stmt3 = $dbh->prepare($sql3);
    $stmt3->execute();

    $office = [];
    while ($ro = $stmt3->fetch(PDO::FETCH_ASSOC)) {
      $office[] = $ro;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  try {
    $sql0 = "SELECT department FROM hrm_department";
    $stmt0 = $dbh->prepare($sql0);
    $stmt0->execute();

    $department = [];
    while ($rof = $stmt0->fetch(PDO::FETCH_ASSOC)) {
      $department[] = $rof;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $datas[] = $office;
  $datas[] = $spvup;
  $datas[] = $department;

  echo json_encode($datas);
} elseif ($request == 'assignpenilai2') {

  $off = $data->office;

  try {
    $sql1 = "SELECT a.id_num, a.nik, a.att_name, initcap(a.emp_name) as emp_name, 
            to_char(a.join_date, 'DD Mon YYYY') as join_date, lower(a.emp_sts) as emp_sts, 
            a.emp_sts_notes, a.kd_level, a.kd_off, a.kd_dept, b.level, a.path_photo, 
            d.division, e.department, f.office, c.position
            FROM hrm_employee a  
            LEFT JOIN hrm_level b ON a.kd_level = b.kd_level 
            LEFT JOIN hrm_division d ON a.kd_div = d.kd_div
            LEFT JOIN hrm_department e ON a.kd_dept = e.kd_dept
            LEFT JOIN hrm_office f ON a.kd_off = f.kd_off 
            LEFT JOIN hrm_position c ON c.kd_pos = a.kd_pos
            WHERE a.resign_date IS NULL AND a.id_num NOT IN (SELECT id_num_ternilai FROM t_emp_pa_penilai) AND b.kd_level NOT IN ('PRESDIR','DIR') AND f.office = '$off' 
            ORDER BY a.kd_off";

    $stmt1 = $dbh->prepare($sql1);
    $stmt1->execute();

    $allstaff = [];
    while ($row = $stmt1->fetch(PDO::FETCH_ASSOC)) {
      $allstaff[] = $row;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  //$datas[] = $allstaff;

  echo json_encode($allstaff);
} elseif ($request == 'savepenilai') {

  $att_name_penilai = $data->att_name_penilai;
  $id_num_ternilai = $data->id_num_ternilai;
  $att_name_ternilai = $data->att_name_ternilai;
  $emp_name_ternilai = $data->emp_name_ternilai;

  try {
    $sql = "INSERT INTO t_emp_pa_penilai(att_name_penilai,id_num_ternilai, att_name_ternilai, emp_name_ternilai) VALUES (:penilai,:idternilai,:ternilai,:namaternilai)";
    $stmt = $dbh->prepare($sql);

    $stmt->bindValue(':penilai', $att_name_penilai);
    $stmt->bindValue(':idternilai', $id_num_ternilai);
    $stmt->bindValue(':ternilai', $att_name_ternilai);
    $stmt->bindValue(':namaternilai', $emp_name_ternilai);

    // execute the insert statement

    $stmt->execute();
    $lastPAinsert_id =  $dbh->lastInsertId('t_emp_pa_penilai_seq');
    echo $lastPAinsert_id;
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal insert PA';
  }
} elseif ($request == 'deletePenilai') {

  $att_name = $data->att_name;

  try {

    $sql = "DELETE FROM t_emp_pa_penilai WHERE att_name_ternilai = '$att_name'";

    $stmt = $dbh->prepare($sql);

    // execute the insert statement

    $stmt->execute();
    //$lastPAinsert_id =  $dbh->lastInsertId('t_emp_pa_penilai_seq');
    echo 'OK';
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal delete penilai';
  }
} elseif ($request == 'datacabang') {

  $datas = [];

  try {
    $sql = "SELECT trim(coce_code) as coce_code FROM gl_coce WHERE coce_code NOT IN ('KP','DPS')";
    $stmt = $dbh->prepare($sql);
    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal cabang: ' . $e;
  }
  $cabang = [];
  while ($d = $stmt->fetch(PDO::FETCH_ASSOC)) {
    $cabang[] = $d;
  }
  try {
    $sqlstaff = "SELECT INITCAP(c.emp_name) as emp_name, c.kd_level, trim(c.kd_off) as kd_off, b.emp_userid, b.nilaitotal
                  FROM t_emp_pa_penilaian b
                  LEFT JOIN hrm_employee c ON b.emp_userid = c.att_name
                  ORDER BY c.kd_level DESC
                  ";
    $stmtstaff = $dbh->prepare($sqlstaff);
    $stmtstaff->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal cabang: ' . $e;
  }
  $staff = [];
  while ($d = $stmtstaff->fetch(PDO::FETCH_ASSOC)) {
    $staff[] = $d;
  }
  $datas[] = $cabang;
  $datas[] = $staff;

  echo json_encode($datas);
} elseif ($request == "hasilpenilai") {

  $datas = [];

  try {
    $sqlpenilai = "SELECT b.emp_name as nama, b.att_name, count(a.att_name_ternilai) as total_staff
                    FROM t_emp_pa_penilai a
                    LEFT JOIN hrm_employee b ON a.att_name_penilai = b.att_name
                    GROUP BY b.emp_name, b.att_name
                    ORDER BY nama
                ";
    $stmtpenilai = $dbh->prepare($sqlpenilai);
    $stmtpenilai->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal penilai: ' . $e;
  }

  while ($penilai = $stmtpenilai->fetch(PDO::FETCH_ASSOC)) {

    $sqlcountternilai = "SELECT count(emp_userid) as total_emp
                         FROM t_emp_pa_penilaian 
                         WHERE UPPER(penilaian_oleh) = :penilai
                         GROUP BY penilaian_oleh";
    $stmtcountternilai = $dbh->prepare($sqlcountternilai);
    $stmtcountternilai->bindValue(':penilai', $penilai['att_name']);

    $stmtcountternilai->execute();

    $totalnilai = $stmtcountternilai->fetchColumn();

    if ($totalnilai == false) {
      $totalternilai = 0;
    } else {
      $totalternilai = $totalnilai;
    }


    $sqldetailternilai = "SELECT INITCAP(c.emp_name) as namastaff, CASE WHEN b.emp_userid IS NULL THEN 0 ELSE 1 END as status
                          FROM t_emp_pa_penilai a
                          LEFT JOIN t_emp_pa_penilaian b ON a.att_name_ternilai = b.emp_userid
                          LEFT JOIN hrm_employee c ON a.att_name_ternilai = c.att_name
                          WHERE a.att_name_penilai = :penilai";
    $stmtdetailternilai = $dbh->prepare($sqldetailternilai);
    $stmtdetailternilai->bindValue(':penilai', $penilai['att_name']);
    $stmtdetailternilai->execute();

    $listernilai = [];

    while ($row = $stmtdetailternilai->fetch(PDO::FETCH_ASSOC)) {
      $listernilai[] = $row;
    }

    $sisa = $penilai['total_staff'] - $totalternilai;

    $datas[] = array(
      'penilai' => $penilai['nama'],
      'total_staff' => $penilai['total_staff'],
      'ternilai' => $totalternilai,
      'belumternilai' => $sisa,
      'liststaffs' => $listernilai
    );
  }

  echo json_encode($datas);
} elseif ($request == "getDataPosition") {

  $data_post = [];

  $kd_off = $data->kd_off;

  try {
    $sql_position = "SELECT kd_post, position_name, kd_post_parent FROM hrm_apprasial_position WHERE kd_off = '$kd_off' AND status_post = '1' ORDER BY id_post";

    $stmt_position = $dbh->prepare($sql_position);
    $stmt_position->execute();

    $arr_position = [];

    while ($row_postion = $stmt_position->fetch(PDO::FETCH_ASSOC)) {
      $arr_position[] = $row_postion;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $data_post[] = $arr_position;

  echo json_encode($data_post);
} elseif ($request == "checkSavePosition") {

  $data_kd_post = [];

  $kd_off = $data->kd_off;
  $kd_post = $data->kd_post;

  try {
    $sql = "SELECT kd_post FROM hrm_apprasial_position WHERE kd_post = '$kd_post' AND kd_off = '$kd_off' AND status_post = '1'";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $arr_kd_post = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $data_kd_post[] = $arr_kd_post;

  echo json_encode($data_kd_post);
} elseif ($request == "checkUpdatePosition") {

  $data_kd_post = [];

  $kd_off = $data->kd_off;
  $kd_post = $data->kd_post;
  $id_post = $data->id_post;

  try {
    $sql = "SELECT kd_post FROM hrm_apprasial_position WHERE id_post = '$id_post' AND kd_off = '$kd_off' AND status_post = '1'";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $arr_kd_post = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  try {
    $sql_count = "SELECT COUNT(*) FROM hrm_apprasial_position WHERE kd_post = '$kd_post' AND kd_off = '$kd_off' AND status_post = '1'";

    $stmt_count = $dbh->prepare($sql_count);
    $stmt_count->execute();

    $arr_count = $stmt_count->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $data_kd_post[] = $arr_kd_post;
  $data_kd_post[] = $arr_count;

  echo json_encode($data_kd_post);
} elseif ($request == "savePosition") {

  $kd_post = $data->kd_post;
  $post_name = $data->post_name;
  $kd_post_parent = $data->kd_post_parent;
  $kd_off = $data->kd_off;

  try {
    $sql = "INSERT INTO hrm_apprasial_position(kd_post,position_name,kd_post_parent,kd_off,status_post) VALUES (:kd_position,:post_name,:kd_post_parent,:kd_off,'1')";
    $stmt = $dbh->prepare($sql);

    $stmt->bindValue(':kd_position', $kd_post);
    $stmt->bindValue(':post_name', $post_name);
    $stmt->bindValue(':kd_post_parent', $kd_post_parent);
    $stmt->bindValue(':kd_off', $kd_off);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal insert Position';
  }
} elseif ($request == "getDataPositionById") {

  $data_postbyid = [];

  $kd_off = $data->kd_off;
  $id_post = $data->id_post;

  try {
    $sql_byid = "SELECT id_post, kd_post, position_name, kd_post_parent FROM hrm_apprasial_position WHERE id_post = '$id_post' AND kd_off = '$kd_off' AND status_post = '1'";

    $stmt_byid = $dbh->prepare($sql_byid);
    $stmt_byid->execute();

    $arr_postbyid = $stmt_byid->fetch(PDO::FETCH_ASSOC);

    // while($row_postbyid = $stmt_byid->fetch(PDO::FETCH_ASSOC)) {
    //   $arr_postbyid[] = $row_postbyid;
    // }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $data_postbyid[] = $arr_postbyid;

  echo json_encode($data_postbyid);
} elseif ($request == "updatePosition") {

  $id_position = $data->id_position;
  $kd_post = $data->kode_post;
  $post_name = $data->position_name;
  $kd_post_parent = $data->kode_post_parent;

  try {

    $sql_update = "UPDATE hrm_apprasial_position SET kd_post = :kd_position, position_name = :post_name, kd_post_parent = :kd_post_parent WHERE id_post = :id_position";
    $stmt = $dbh->prepare($sql_update);

    $stmt->bindValue(':id_position', $id_position);
    $stmt->bindValue(':kd_position', $kd_post);
    $stmt->bindValue(':post_name', $post_name);
    $stmt->bindValue(':kd_post_parent', $kd_post_parent);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Update Position';
  }
} elseif ($request == "checkDeletePosition") {

  $data_kd_parent = [];

  $id_position = $data->id_position;
  $kd_post = $data->kode_post;
  $kd_off = $data->kd_off;

  try {
    $sql_count = "SELECT COUNT(*) FROM hrm_apprasial_position WHERE kd_post_parent = '$kd_post' AND kd_off = '$kd_off' AND status_post = '1'";

    $stmt_count = $dbh->prepare($sql_count);
    $stmt_count->execute();

    $arr_count = $stmt_count->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  try {
    $sql_count2 = "SELECT COUNT(*) FROM hrm_apprasial_employee WHERE id_post = '$id_position' AND status_employee = '1'";

    $stmt_count2 = $dbh->prepare($sql_count2);
    $stmt_count2->execute();

    $arr_count2 = $stmt_count2->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $data_kd_parent[] = $arr_count;
  $data_kd_parent[] = $arr_count2;

  echo json_encode($data_kd_parent);
} else if ($request == "deletePosition") {

  $id_position = $data->id_position;

  try {

    $sql_delete = "UPDATE hrm_apprasial_position SET status_post = 0 WHERE id_post = :id_position";
    $stmt = $dbh->prepare($sql_delete);

    $stmt->bindValue(':id_position', $id_position);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Delete Position';
  }
} else if ($request == "getDetailEmployee") {

  $result = [];

  // $kd_off = $data->kd_off;
  $id_num = $data->id_num;

  try {
    $sql = "SELECT a.id_num, a.nik, a.att_name, lower(a.emp_name) as emp_name, to_char(a.join_date, 'DD Mon YYYY') as join_date, lower(a.emp_sts) as emp_sts, a.emp_sts_notes, a.path_photo, a.kd_off, a.kd_dept, a.kd_div, a.kd_level, b.level, b.id_level, c.office, d.department, e.position, e.kd_pos
              FROM hrm_employee a
              JOIN hrm_level b ON b.kd_level = a.kd_level
              JOIN hrm_office c ON c.kd_off = a.kd_off
              JOIN hrm_department d ON d.kd_dept = a.kd_dept
              JOIN hrm_position e ON e.kd_pos = a.kd_pos
              WHERE a.resign_date IS NULL AND a.id_num = '$id_num'";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $arr_employee = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $id_level = $arr_employee['id_level'];

  try {
    // $sql = "SELECT a.kd_indikator, to_char(a.periode_month, 'Month YYYY') as periode_month, to_char(a.expired, 'DD Month YYYY') as expired, a.status_active, a.actived_by, to_char(a.active_date, 'DD Month YYYY') as active_date, a.id_level, a.id_indikator_periode, b.level, c.id_indikator, concat(a.kd_indikator, ' - ', b.level) AS nomor_penilaian
    //         FROM hrm_apprasial_indikator_periode a
    //         JOIN hrm_level b ON a.id_level = b.id_level
    //         JOIN hrm_apprasial_indikator c ON a.kd_indikator = c.kd_indikator 
    //         WHERE a.deleted = '0' AND a.status_active = '1' AND b.id_level = '$id_level'
    //         ORDER BY a.id_indikator_periode ASC";

    $sql = "SELECT DISTINCT(kd_indikator) FROM hrm_apprasial_tab_penilaian WHERE id_level = '$id_level'";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $arr_indikator = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $result[] = $arr_employee;
  $result[] = $arr_indikator;

  echo json_encode($result);
} else if ($request == "savePenilaiExternal") {

  $id_target = $data->id_target;
  // $id_penilai = $data->id_penilai;
  // $tipe_penilai = $data->tipe_penilai;

  $create = $data->penilaiBaru;
  $penilaiBaru = json_decode($create, true);

  try {
    $sql = "INSERT INTO hrm_apprasial_penilai(id_num_penilai,id_num_target,status_penilai,type_penilai) VALUES (:id_penilai,:id_target,'1',:type_penilai)";
    $stmt = $dbh->prepare($sql);

    for ($i = 0; $i < count($penilaiBaru); $i++) {
      // try {
      //   $sql2 = "SELECT a.id_num, a.nik, a.att_name, lower(a.emp_name) as emp_name, to_char(a.join_date, 'DD Mon YYYY') as join_date, lower(a.emp_sts) as emp_sts, a.emp_sts_notes, a.path_photo, b.level, b.id_level, c.office, d.department, e.position, e.kd_pos, f.rpt_kd_pos 
      //           FROM hrm_employee a
      //           JOIN hrm_level b ON b.kd_level = a.kd_level
      //           JOIN hrm_office c ON c.kd_off = a.kd_off
      //           JOIN hrm_department d ON d.kd_dept = a.kd_dept
      //           JOIN hrm_position e ON e.kd_pos = a.kd_pos
      //           JOIN hrm_org_chart f ON f.kd_pos = a.kd_pos
      //           WHERE a.resign_date IS NULL AND a.id_num = '$id_target'";

      //   $stmt2 = $dbh->prepare($sql2);
      //   $stmt2->execute();

      //   $arr_employee1 = $stmt2->fetch(PDO::FETCH_ASSOC);

      // } catch (PDOException $e) {
      //   $e->getMessage();
      //   echo 'gagal:'.$e; 
      // }

      $id_penilai = $penilaiBaru[$i]['karyawan'];

      // try {
      //   $sql3 = "SELECT a.id_num, a.nik, a.att_name, lower(a.emp_name) as emp_name, to_char(a.join_date, 'DD Mon YYYY') as join_date, lower(a.emp_sts) as emp_sts, a.emp_sts_notes, a.path_photo, b.level, b.id_level, c.office, d.department, e.position, e.kd_pos, f.rpt_kd_pos 
      //           FROM hrm_employee a
      //           JOIN hrm_level b ON b.kd_level = a.kd_level
      //           JOIN hrm_office c ON c.kd_off = a.kd_off
      //           JOIN hrm_department d ON d.kd_dept = a.kd_dept
      //           JOIN hrm_position e ON e.kd_pos = a.kd_pos
      //           JOIN hrm_org_chart f ON f.kd_pos = a.kd_pos
      //           WHERE a.resign_date IS NULL AND a.id_num = $id_penilai";

      //   $stmt3 = $dbh->prepare($sql3);

      //   $stmt3->execute();

      //   $arr_employee2 = $stmt3->fetch(PDO::FETCH_ASSOC);

      // } catch (PDOException $e) {
      //   $e->getMessage();
      //   echo 'gagal:'.$e; 
      // }

      // print_r($arr_employee1['department']);
      // print_r($arr_employee2['department']);

      // if($arr_employee1['department'] == $arr_employee2['department']) {
      //   $tipe_penilai = 'RI';
      // } else {
      //   $tipe_penilai = 'RE';
      // }

      $stmt->bindValue(':id_penilai', $id_penilai);
      $stmt->bindValue(':id_target', $id_target);
      $stmt->bindValue(':type_penilai', 'RE');
      $stmt->execute();
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal insert Penilai';
  }
} else if ($request == "savePenilaiInternal") {

  $id_target = $data->id_target;
  // $id_penilai = $data->id_penilai;
  // $tipe_penilai = $data->tipe_penilai;

  $create = $data->penilaiBaru;
  $penilaiBaru = json_decode($create, true);

  try {
    $sql = "INSERT INTO hrm_apprasial_penilai(id_num_penilai,id_num_target,status_penilai,type_penilai) VALUES (:id_penilai,:id_target,'1',:type_penilai)";
    $stmt = $dbh->prepare($sql);

    for ($i = 0; $i < count($penilaiBaru); $i++) {
      $stmt->bindValue(':id_penilai', $penilaiBaru[$i]['karyawan']);
      $stmt->bindValue(':id_target', $id_target);
      $stmt->bindValue(':type_penilai', 'RI');
      $stmt->execute();
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal insert Penilai';
  }
} else if ($request == "savePenilaiExternal2") {

  $id_target = $data->id_target;
  $id_penilai = $data->id_penilai;
  $tipe_penilai = $data->tipe_penilai;

  try {
    $sql = "INSERT INTO hrm_apprasial_penilai(id_num_penilai,id_num_target,status_penilai,type_penilai) VALUES (:id_penilai,:id_target,'1',:type_penilai)";
    $stmt = $dbh->prepare($sql);

    $stmt->bindValue(':id_penilai', $id_penilai);
    $stmt->bindValue(':id_target', $id_target);
    $stmt->bindValue(':type_penilai', $tipe_penilai);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal insert Penilai';
  }
} else if ($request == "getPenilai") {

  $result = [];

  $id_num_target = $data->id_num;

  try {

    $sql = "SELECT a.id_num, a.nik, a.att_name, lower(a.emp_name) as emp_name, to_char(a.join_date, 'DD Mon YYYY') as join_date, lower(a.emp_sts) as emp_sts, a.emp_sts_notes, a.path_photo, b.level, c.office, d.department, e.position, hrm_apprasial_penilai.type_penilai, hrm_apprasial_penilai.id_penilai
            FROM hrm_apprasial_penilai 
            JOIN hrm_employee a ON a.id_num = hrm_apprasial_penilai.id_num_penilai
            JOIN hrm_level b ON b.kd_level = a.kd_level
            JOIN hrm_office c ON c.kd_off = a.kd_off
            JOIN hrm_department d ON d.kd_dept = a.kd_dept
            JOIN hrm_position e ON e.kd_pos = a.kd_pos
            WHERE hrm_apprasial_penilai.id_num_target = '$id_num_target'
            ORDER BY hrm_apprasial_penilai.type_penilai ASC";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $arr_penilai = [];

    while ($row_penilai = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $arr_penilai[] = $row_penilai;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $result[] = $arr_penilai;

  echo json_encode($result);
} else if ($request == "getPenilaiAI") {

  $result = [];

  $id_num_target = $data->id_num;
  $kd_div = $data->kd_div;
  $kd_dept = $data->kd_dept;
  $kd_pos = $data->kd_pos;
  $kd_off = $data->kd_off;
  $kd_level = $data->kd_level;

  if ($kd_level == 'MGR') {
    if ($kd_div == 'FAD') {
      $add_sql = "SELECT * FROM v_org_chart
                  WHERE kd_level = 'GM' 
                  AND kd_dept = 'FAD'
                  UNION ALL";
    } else {
      $add_sql = "SELECT * FROM v_org_chart
                  WHERE kd_level = 'GM' 
                  AND kd_dept = 'OBDD'
                  UNION ALL";
    }
  } else {
    $add_sql = "";
  }

  if ($kd_pos == 'HOO' || $kd_pos == 'SSS' || $kd_pos == 'HOOS') {

    try {

      $sql_rm = "SELECT regional FROM gl_coce WHERE coce_code = '$kd_off'";

      $stmt_rm = $dbh->prepare($sql_rm);
      $stmt_rm->execute();

      $arr_rm = $stmt_rm->fetch(PDO::FETCH_ASSOC);

      if ($arr_rm['regional'] == 'JATIM' || $arr_rm['regional'] == 'BALI') {
        $add_sql_rm = "SELECT * FROM v_org_chart
                        WHERE id_num = '1123'
                        AND id_num NOT IN (SELECT id_num_penilai FROM hrm_apprasial_penilai WHERE id_num_target = '$id_num_target')
                        UNION ALL";
      } else if ($arr_rm['regional'] == 'NTB' || $arr_rm['regional'] == 'NTT') {
        $add_sql_rm = "SELECT * FROM v_org_chart
                        WHERE id_num = '1085'
                        AND id_num NOT IN (SELECT id_num_penilai FROM hrm_apprasial_penilai WHERE id_num_target = '$id_num_target')
                        UNION ALL";
      } else if ($arr_rm['regional'] == 'KALTIM') {
        $add_sql_rm = "SELECT * FROM v_org_chart
                        WHERE id_num = '446'
                        AND id_num NOT IN (SELECT id_num_penilai FROM hrm_apprasial_penilai WHERE id_num_target = '$id_num_target')
                        UNION ALL";
      }
    } catch (PDOException $e) {
      $e->getMessage();
      echo 'gagal:' . $e;
    }
  } else {

    $add_sql_rm = "SELECT * FROM v_org_chart
                    WHERE kd_off = 'HO' 
                    AND kd_dept = '$kd_dept'
                    AND kd_level = 'MGR'
                    AND id_num NOT IN (SELECT id_num_penilai FROM hrm_apprasial_penilai WHERE id_num_target = '$id_num_target')
                    UNION ALL";
  }

  try {

    $sql = "SELECT * FROM v_org_chart
            WHERE kd_off = '$kd_off'
            AND emp_sts != 'LEPAS'
            AND id_num NOT IN (SELECT id_num_penilai FROM hrm_apprasial_penilai WHERE id_num_target = '$id_num_target')
            UNION ALL
            $add_sql
            $add_sql_rm
            SELECT * FROM v_org_chart
            WHERE kd_off = '$kd_off'
            AND kd_pos = 'HOO'
            AND id_num NOT IN (SELECT id_num_penilai FROM hrm_apprasial_penilai WHERE id_num_target = '$id_num_target')
            EXCEPT
            SELECT * FROM v_org_chart
            WHERE id_num = '$id_num_target'";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $arr_employee_all = [];

    while ($row_employee_all = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $arr_employee_all[] = $row_employee_all;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $result[] = $arr_employee_all;

  echo json_encode($result);
} else if ($request == "checkListPenilai") {

  $id_num_target = $data->id_num_target;
  $crete = $data->id_num_penilai;
  $id_num_penilai = json_decode($crete, true);

  print_r($id_num_penilai);

  // try {

  //   $sql = "SELECT * FROM hrm_apprasial_penilai WHERE id_num_target = '$id_num_target' AND id_num_penilai = :id_penilai AND NOT IN 
  //     (SELECT * FROM v_org_chart 
  //       JOIN hrm_employee ON v_org_chart.id_num = hrm_employee.id_num
  //     WHERE hrm_employee.id_num = '$id_num_target')";

  //   $stmt = $dbh->prepare($sql);

  //   $arr_employee_all = [];

  //   for($i=0;$i<count($id_num_penilai);$i++) {

  //     $stmt->bindValue(':id_penilai' ,$id_num_penilai[$i]['id_num']);
  //     $stmt->execute();

  //     while($row_employee_all = $stmt->fetch(PDO::FETCH_ASSOC)) {
  //       $arr_employee_all[] = $row_employee_all;
  //     }
  //   }


  // } catch (PDOException $e) {
  //   $e->getMessage();
  //   echo 'gagal:'.$e; 
  // }

  // $result[] = $arr_employee_all;

  // echo json_encode($result);

} else if ($request == "checkDeleteListPenilai") {

  $result = [];

  $id_target = $data->id_target;
  $id_penilai = $data->id_penilai;

  try {

    $sql = "SELECT * FROM hrm_apprasial_penilai WHERE id_num_penilai = '$id_penilai' AND id_num_target = '$id_target'";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_employee = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Delete Penilai';
  }

  $result[] = $arr_employee;

  echo json_encode($result);
} else if ($request == "deleteListPenilai") {

  $sql_result = [];

  $id_penilai = $data->id_penilai;

  try {

    $sql_delete = "DELETE FROM hrm_apprasial_penilai WHERE id_penilai = '$id_penilai'";
    $stmt = $dbh->prepare($sql_delete);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Delete Penilai';
  }

  $sql_result[] = $sql_delete;

  echo json_encode($sql_result);
} else if ($request == "getlistemployeeAllByKodeOff") {

  $result = [];

  $kd_off = $data->kd_off;

  try {

    $sql = "SELECT a.id_num, a.nik, a.att_name, lower(a.emp_name) as emp_name, to_char(a.join_date, 'DD Mon YYYY') as join_date, lower(a.emp_sts) as emp_sts, a.emp_sts_notes, a.path_photo, b.level, c.office, d.department, e.position_name AS position
            FROM hrm_apprasial_employee 
            JOIN hrm_employee a ON a.id_num = hrm_apprasial_employee.id_num_employee
            JOIN hrm_level b ON b.kd_level = hrm_apprasial_employee.kd_level
            JOIN hrm_office c ON c.kd_off = hrm_apprasial_employee.kd_off
            JOIN hrm_department d ON d.kd_dept = hrm_apprasial_employee.kd_dept
            JOIN hrm_apprasial_position e ON e.id_post = hrm_apprasial_employee.id_post
            WHERE hrm_apprasial_employee.kd_off = '$kd_off' AND hrm_apprasial_employee.status_employee = '1' 
            ORDER BY b.kd_level";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $arr_employee_all = [];

    while ($row_employee_all = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $arr_employee_all[] = $row_employee_all;
    }
  } catch (PDOException $e) {

    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $result[] = $arr_employee_all;

  echo json_encode($result);
} else if ($request == "getlistemployeeAll") {

  $result = [];

  $kd_off = $data->kd_off;
  $kd_dept = $data->kd_dept;
  $periode = $data->periode;

  try {

    $sql = "SELECT a.id_num, b.id_level, a.nik, a.att_name, lower(a.emp_name) as emp_name, concat(lower(a.emp_name), ' - ', office) AS name_off, to_char(a.join_date, 'DD Mon YYYY') as join_date, lower(a.emp_sts) as emp_sts, a.emp_sts_notes, a.path_photo, b.level, c.office, d.department, e.position,
            (SELECT grand_total_nilai_by_indikator FROM hrm_apprasial_grand_total_penilaian WHERE id_num_target = a.id_num AND tipe_penilai = 'HRD' AND periode = '$periode' LIMIT 1) AS total_penilaian_hrd
            FROM hrm_employee a 
            JOIN hrm_level b ON b.kd_level = a.kd_level
            JOIN hrm_office c ON c.kd_off = a.kd_off
            JOIN hrm_department d ON d.kd_dept = a.kd_dept
            JOIN hrm_position e ON e.kd_pos = a.kd_pos
            WHERE a.resign_date IS NULL AND a.emp_sts != 'LEPAS'
            ORDER BY b.kd_level";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $arr_employee_all = [];

    while ($row_employee_all = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $arr_employee_all[] = $row_employee_all;
    }
  } catch (PDOException $e) {

    $e->getMessage();
    echo 'gagal:' . $e;
  }

  try {

    $sql2 = "SELECT a.id_num, a.nik, a.att_name, lower(a.emp_name) as emp_name, concat(lower(a.emp_name), ' - ', office) AS name_off, to_char(a.join_date, 'DD Mon YYYY') as join_date, lower(a.emp_sts) as emp_sts, a.emp_sts_notes, a.path_photo, b.level, c.office, d.department, e.position
              FROM hrm_employee a 
              LEFT JOIN hrm_level b ON b.kd_level = a.kd_level
              LEFT JOIN hrm_office c ON c.kd_off = a.kd_off
              LEFT JOIN hrm_department d ON d.kd_dept = a.kd_dept
              LEFT JOIN hrm_position e ON e.kd_pos = a.kd_pos
              WHERE a.resign_date IS NULL AND a.kd_off = '$kd_off' AND a.emp_sts != 'LEPAS'
              ORDER BY b.kd_level";

    $stmt2 = $dbh->prepare($sql2);
    $stmt2->execute();

    $arr_employee_all2 = [];

    while ($row_employee_all2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
      $arr_employee_all2[] = $row_employee_all2;
    }
  } catch (PDOException $e) {

    $e->getMessage();
    echo 'gagal:' . $e;
  }

  try {

    $sql3 = "SELECT a.id_num, a.nik, a.att_name, lower(a.emp_name) as emp_name, concat(lower(a.emp_name), ' - ', office) AS name_off, to_char(a.join_date, 'DD Mon YYYY') as join_date, lower(a.emp_sts) as emp_sts, a.emp_sts_notes, a.path_photo, b.level, c.office, d.department, e.position
              FROM hrm_employee a 
              LEFT JOIN hrm_level b ON b.kd_level = a.kd_level
              LEFT JOIN hrm_office c ON c.kd_off = a.kd_off
              LEFT JOIN hrm_department d ON d.kd_dept = a.kd_dept
              LEFT JOIN hrm_position e ON e.kd_pos = a.kd_pos
              WHERE a.resign_date IS NULL AND a.kd_off = '$kd_off' AND a.kd_dept = '$kd_dept' AND a.emp_sts != 'LEPAS'
              ORDER BY b.kd_level";

    $stmt3 = $dbh->prepare($sql3);
    $stmt3->execute();

    $arr_employee_all3 = [];

    while ($row_employee_all3 = $stmt3->fetch(PDO::FETCH_ASSOC)) {
      $arr_employee_all3[] = $row_employee_all3;
    }
  } catch (PDOException $e) {

    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $result[] = $arr_employee_all;
  $result[] = $arr_employee_all2;
  $result[] = $arr_employee_all3;

  echo json_encode($result);
} else if ($request == "getlistemployeeByKodePosition") {

  $result = [];

  $kd_off = $data->kd_off;
  $id_post = $data->id_post;

  try {

    $sql = "SELECT a.id_num, a.nik, a.att_name, lower(a.emp_name) as emp_name, to_char(a.join_date, 'DD Mon YYYY') as join_date, lower(a.emp_sts) as emp_sts, a.emp_sts_notes, a.path_photo, b.level, c.office, d.department, e.position_name AS position
            FROM hrm_apprasial_employee 
            JOIN hrm_employee a ON a.id_num = hrm_apprasial_employee.id_num_employee
            JOIN hrm_level b ON b.kd_level = hrm_apprasial_employee.kd_level
            JOIN hrm_office c ON c.kd_off = hrm_apprasial_employee.kd_off
            JOIN hrm_department d ON d.kd_dept = hrm_apprasial_employee.kd_dept
            JOIN hrm_apprasial_position e ON e.id_post = hrm_apprasial_employee.id_post
            WHERE hrm_apprasial_employee.kd_off = '$kd_off' AND hrm_apprasial_employee.id_post = '$id_post' AND hrm_apprasial_employee.status_employee = '1' 
            ORDER BY b.kd_level";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $arr_employee = [];

    while ($row_employee = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $arr_employee[] = $row_employee;
    }
  } catch (PDOException $e) {

    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $result[] = $arr_employee;

  echo json_encode($result);
} else if ($request == "checkgetlistemployeeByKode") {

  $result = [];

  $kd_off = $data->kd_off;
  $kode_pos = $data->kode_pos;

  try {

    $sql = "SELECT *
            FROM hrm_apprasial_position_bawahan
            WHERE id_atasan = '0'
            AND kd_off = '$kd_off'
            AND kd_pos = '$kode_pos'";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $arr_employee = [];

    while ($row_employee = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $arr_employee[] = $row_employee;
    }
  } catch (PDOException $e) {

    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $result[] = $arr_employee;

  echo json_encode($result);
} else if ($request == "getlistemployeeByKode") {

  $result = [];

  $kd_off = $data->kd_off;
  $kode_pos = $data->kode_pos;
  $and = $data->and;

  if ($and == 0) {
    $xand = "AND a.kd_off =  '$kd_off'";
  } else {
    $xand = "";
  }

  try {

    $sql = "SELECT a.id_num, a.nik, a.att_name, lower(a.emp_name) as emp_name, to_char(a.join_date, 'DD Mon YYYY') as join_date, lower(a.emp_sts) as emp_sts, a.emp_sts_notes, a.path_photo, b.level, c.office, d.department, e.position AS position
              FROM hrm_employee a 
              JOIN hrm_level b ON b.kd_level = a.kd_level
              JOIN hrm_office c ON c.kd_off = a.kd_off
              JOIN hrm_department d ON d.kd_dept = a.kd_dept
              JOIN hrm_position e ON e.kd_pos = a.kd_pos
              WHERE a.kd_pos = '$kode_pos' AND a.resign_date IS NULL $xand
              ORDER BY b.kd_level";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $arr_employee = [];

    while ($row_employee = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $arr_employee[] = $row_employee;
    }
  } catch (PDOException $e) {

    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $result[] = $arr_employee;

  echo json_encode($result);
} else if ($request == "saveEmployee") {

  $id_post = $data->id_post;
  $kd_off = $data->kd_off;
  $id_num = $data->id_num;
  $kd_dept = $data->kd_dept;
  $kd_level = $data->kd_level;

  try {
    $sql = "INSERT INTO hrm_apprasial_employee(id_num_employee,kd_off,status_employee,kd_dept,kd_level,id_post) VALUES ('$id_num','$kd_off','1','$kd_dept','$kd_level','$id_post')";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal insert Karyawan';
  }
} else if ($request == "deleteEmployee") {

  $id_num = $data->id_num;

  try {
    $sql = "UPDATE hrm_apprasial_employee SET status_employee = 0 WHERE id_num_employee = '$id_num'";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Delete Karyawan';
  }
} else if ($request == "getPenilaiKaryawan") {

  $result = [];

  $username = $data->username;
  $today = date("Y-m-d");

  try {

    $sql = "SELECT a.kd_off, a.id_num, a.nik, a.att_name, lower(a.emp_name) as emp_name, to_char(a.join_date, 'DD Mon YYYY') as join_date, lower(a.emp_sts) as emp_sts, a.emp_sts_notes, a.path_photo, b.level, c.office, d.department, e.position, '$today' AS today
            FROM hrm_employee a 
            JOIN hrm_level b ON b.kd_level = a.kd_level
            JOIN hrm_office c ON c.kd_off = a.kd_off
            JOIN hrm_department d ON d.kd_dept = a.kd_dept
            JOIN hrm_position e ON e.kd_pos = a.kd_pos
            AND a.resign_date IS NULL AND a.pin2 = UPPER('$username')
            ORDER BY a.kd_off";

    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_employee = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Delete Penilai';
  }

  $result[] = $arr_employee;

  echo json_encode($result);
} else if ($request == "getTargetKaryawan") {

  $result = [];

  $id_num = $data->id_num;
  $periode = $data->periode;

  try {

    $sql = "SELECT a.id_num, a.nik, a.att_name, lower(a.emp_name) as emp_name, to_char(a.join_date, 'DD Mon YYYY') as join_date, lower(a.emp_sts) as emp_sts, a.emp_sts_notes, a.path_photo, b.level, b.id_level, c.office, d.department, e.position, hrm_apprasial_penilai.type_penilai, 
            (SELECT grand_total_nilai_by_indikator FROM hrm_apprasial_grand_total_penilaian WHERE id_num_target = a.id_num AND id_num_penilai = '$id_num' AND tipe_penilai != 'HRD' AND periode = '$periode' LIMIT 1) AS total_penilaian, 
            (SELECT count(grand_total_nilai_by_indikator) FROM hrm_apprasial_grand_total_penilaian_hrd WHERE id_num_target = a.id_num) AS total_penilaian_hrd
              FROM hrm_apprasial_penilai 
              JOIN hrm_employee a ON a.id_num = hrm_apprasial_penilai.id_num_target
              JOIN hrm_level b ON b.kd_level = a.kd_level
              JOIN hrm_office c ON c.kd_off = a.kd_off
              JOIN hrm_department d ON d.kd_dept = a.kd_dept
              JOIN hrm_position e ON e.kd_pos = a.kd_pos 
              WHERE hrm_apprasial_penilai.id_num_penilai = '$id_num' AND a.resign_date IS NULL";

    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_target = [];

    while ($row_target = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $arr_target[] = $row_target;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Delete Target Nilai';
  }

  $result[] = $arr_target;
  $result[] = $sql;

  echo json_encode($result);
} else if ($request == "getIndikatorPeriode") {

  $result = [];

  try {

    $sql = "SELECT a.kd_indikator, to_char(a.periode_month, 'Month YYYY') as periode_month, to_char(a.expired, 'DD Month YYYY') as expired, a.status_active, a.actived_by, to_char(a.active_date, 'DD Month YYYY') as active_date, a.id_level, a.id_indikator_periode, b.level, c.id_indikator, concat(a.kd_indikator, ' - ', b.level) AS nomor_penilaian
            FROM hrm_apprasial_indikator_periode a
            JOIN hrm_level b ON a.id_level = b.id_level
            JOIN hrm_apprasial_indikator c ON a.kd_indikator = c.kd_indikator 
            WHERE a.deleted = '0'
            ORDER BY a.id_indikator_periode ASC";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_indikator = [];

    while ($row_indikator = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $arr_indikator[] = $row_indikator;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Delete Penilai';
  }

  try {

    $sql2 = "SELECT a.kd_indikator, to_char(a.periode_month, 'Month YYYY') as periode_month, to_char(a.expired, 'DD Month YYYY') as expired, a.status_active, a.actived_by, to_char(a.active_date, 'DD Month YYYY') as active_date, a.id_level, a.id_indikator_periode, b.level, c.id_indikator, concat(a.kd_indikator, ' - ', b.level) AS nomor_penilaian
              FROM hrm_apprasial_indikator_periode a
              JOIN hrm_level b ON a.id_level = b.id_level
              JOIN hrm_apprasial_indikator c ON a.kd_indikator = c.kd_indikator 
              WHERE a.deleted = '0' AND a.status_active = '1' ORDER BY a.id_indikator_periode ASC";
    $stmt2 = $dbh->prepare($sql2);

    $stmt2->execute();

    $arr_indikator2 = [];

    while ($row_indikator2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
      $arr_indikator2[] = $row_indikator2;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Level';
  }

  $result[] = $arr_indikator;
  $result[] = $arr_indikator2;

  echo json_encode($result);
} else if ($request == "getIndikatorPeriodePenilaian") {

  $result = [];

  $id_level = $data->id_level;

  try {

    $sql = "SELECT a.kd_indikator, to_char(a.periode_month, 'Month YYYY') as periode_month, to_char(a.expired, 'DD Month YYYY') as expired, a.status_active, a.actived_by, to_char(a.active_date, 'DD Month YYYY') as active_date, a.id_level, a.id_indikator_periode, b.level, c.id_indikator, concat(a.kd_indikator, ' - ', b.level) AS nomor_penilaian
            FROM hrm_apprasial_indikator_periode a
            JOIN hrm_level b ON a.id_level = b.id_level
            JOIN hrm_apprasial_indikator c ON a.kd_indikator = c.kd_indikator 
            WHERE a.deleted = '0' AND a.status_active = '1' AND b.id_level = '$id_level'
            ORDER BY a.id_indikator_periode ASC";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_indikator = [];

    while ($row_indikator = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $arr_indikator[] = $row_indikator;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Delete Penilai';
  }

  $result[] = $arr_indikator;

  echo json_encode($result);
} else if ($request == "getIndikatorKriteria") {

  $result = [];

  $kd_indikator = $data->kd_indikator;
  $type_penilai = $data->type_penilai;
  $id_level = $data->id_level;

  try {

    $sql = "SELECT a.*
              FROM hrm_apprasial_indikator_kriteria a
              JOIN hrm_apprasial_indikator b ON a.id_indikator = b.id_indikator
              JOIN hrm_apprasial_tab_penilaian c ON a.id_indikator_kriteria = c.id_indikator_kriteria
              WHERE c.kd_indikator = '$kd_indikator' 
              AND a.deleted = '0'
              AND c.tipe_penilai = '$type_penilai'
              AND c.id_level = '$id_level'
              ORDER BY a.id_indikator_kriteria ASC";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_indikator = [];

    while ($row_indikator = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $arr_indikator[] = $row_indikator;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Delete Penilai';
  }

  $result[] = $arr_indikator;

  echo json_encode($result);
} else if ($request == "checkSaveIndikator") {

  $result = [];

  $indikator = $data->indikator;

  try {

    $sql = "SELECT COUNT(*) FROM hrm_apprasial_indikator WHERE indikator = '$indikator' AND delete_indikator = 0";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_check = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Delete Penilai';
  }

  $result[] = $arr_check;

  echo json_encode($result);
} else if ($request == "SaveIndikator") {

  $id_level = $data->id_level;
  $nopenilaian = $data->nopenilaian;
  $username = $data->username;

  $today = date("Y-m-d H:i:s");

  $result = [];

  try {
    $sql = "INSERT INTO hrm_apprasial_indikator(id_level,kd_indikator,status_indikator,delete_indikator,create_by,create_date) 
            VALUES (:id_level,:kd_indikator,'1','0',:create_by,:create_date)";
    $stmt = $dbh->prepare($sql);

    $stmt->bindValue(':id_level', $id_level);
    $stmt->bindValue(':kd_indikator', $nopenilaian);
    $stmt->bindValue(':create_by', $username);
    $stmt->bindValue(':create_date', $today);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal insert Indikator';
  }

  try {

    $sql = "SELECT id_indikator FROM hrm_apprasial_indikator ORDER BY id_indikator DESC LIMIT 1";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_indikator = [];

    $arr_indikator = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Indikator';
  }

  $result[] = $arr_indikator;

  echo json_encode($result);
} else if ($request == "getIndikatorKriteriaById") {

  $result = [];

  $kd_indikator = $data->kd_indikator;
  $id_indikator = $data->id_indikator;

  try {

    $sql = "SELECT hrm_apprasial_indikator_kriteria.*
            FROM hrm_apprasial_indikator_kriteria 
            JOIN hrm_apprasial_indikator ON hrm_apprasial_indikator_kriteria.id_indikator = hrm_apprasial_indikator.id_indikator
            WHERE hrm_apprasial_indikator.kd_indikator = '$kd_indikator' AND hrm_apprasial_indikator_kriteria.deleted = '0'
            AND id_indikator_kriteria = '$id_indikator'
            ORDER BY hrm_apprasial_indikator_kriteria.id_indikator_kriteria ASC";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_indikator = [];

    $arr_indikator = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Indikator';
  }

  $result[] = $arr_indikator;

  echo json_encode($result);
} else if ($request == "updateIndikatorById") {

  $indikator = $data->indikator;
  $persentase = $data->persentase;
  $status_indikator = $data->status_indikator;
  $id_indikator = $data->id_indikator;

  try {

    $sql_update = "UPDATE hrm_apprasial_indikator SET indikator = :indikator, persentase = :persentase, status_indikator = :status_indikator WHERE id_indikator = :id_indikator";
    $stmt = $dbh->prepare($sql_update);

    $stmt->bindValue(':indikator', $indikator);
    $stmt->bindValue(':persentase', $persentase);
    $stmt->bindValue(':status_indikator', $status_indikator);
    $stmt->bindValue(':id_indikator', $id_indikator);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Update Position';
  }
} else if ($request == "deleteIndikatorById") {

  $id_indikator = $data->id_indikator;

  try {

    $sql_delete = "UPDATE hrm_apprasial_indikator SET delete_indikator = 1 WHERE id_indikator = :id_indikator";
    $stmt = $dbh->prepare($sql_delete);

    $stmt->bindValue(':id_indikator', $id_indikator);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Delete Position';
  }
} else if ($request == "getSubIndikatorAll") {

  $result = [];

  $kd_indikator = $data->kd_indikator;

  try {

    $sql = "SELECT hrm_apprasial_sub_indikator.*, hrm_apprasial_indikator_kriteria.indikator, hrm_apprasial_indikator.kd_indikator, hrm_level.level
            FROM hrm_apprasial_sub_indikator
            JOIN hrm_apprasial_indikator_kriteria ON hrm_apprasial_indikator_kriteria.id_indikator_kriteria = hrm_apprasial_sub_indikator.id_indikator_kriteria
            JOIN hrm_apprasial_indikator ON hrm_apprasial_indikator.id_indikator = hrm_apprasial_sub_indikator.id_indikator
            JOIN hrm_level ON hrm_level.id_level = hrm_apprasial_indikator.id_level
            WHERE status_sub_indikator = '1' AND hrm_apprasial_indikator.kd_indikator = '$kd_indikator'
            ORDER BY hrm_apprasial_indikator_kriteria.id_indikator_kriteria, hrm_apprasial_sub_indikator.urutan";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_sub_indikator = [];

    while ($row_sub_indikator = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $arr_sub_indikator[] = $row_sub_indikator;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Delete Sub Indikator';
  }

  $result[] = $arr_sub_indikator;

  echo json_encode($result);
} else if ($request == "getSubIndikatorSearch") {

  $result = [];

  if (!isset($data->kd_indikator)) {
    $kd_indikator = "";
    $src_kd_indikator = "";
  } else {
    $kd_indikator = $data->kd_indikator;
    $src_kd_indikator = "AND hrm_apprasial_indikator.kd_indikator = '$kd_indikator'";
    // echo $src_kd_indikator;
  }

  if (!isset($data->id_indikator)) {
    $id_indikator = "";
    $src_id_indikator = "";
  } else {
    $id_indikator = $data->id_indikator;
    $src_id_indikator = "AND hrm_apprasial_indikator_kriteria.id_indikator_kriteria = '$id_indikator'";
    // echo $src_id_indikator;
  }

  if (!isset($data->urutan)) {
    $urutan = "";
    $src_urutan = "";
  } else {
    $urutan = $data->urutan;
    $src_urutan = "AND hrm_apprasial_sub_indikator.urutan = '$urutan'";
  }

  try {

    $sql = "SELECT hrm_apprasial_sub_indikator.*, hrm_apprasial_indikator_kriteria.indikator, hrm_apprasial_indikator.kd_indikator, hrm_level.level
            FROM hrm_apprasial_sub_indikator
            JOIN hrm_apprasial_indikator_kriteria ON hrm_apprasial_indikator_kriteria.id_indikator_kriteria = hrm_apprasial_sub_indikator.id_indikator_kriteria
            JOIN hrm_apprasial_indikator ON hrm_apprasial_indikator.id_indikator = hrm_apprasial_sub_indikator.id_indikator
            JOIN hrm_level ON hrm_level.id_level = hrm_apprasial_indikator.id_level
            WHERE status_sub_indikator = '1'
            $src_kd_indikator
            $src_id_indikator
            $src_urutan
            ORDER BY hrm_apprasial_sub_indikator.id_sub_indikator DESC";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_sub_indikator = [];

    while ($row_sub_indikator = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $arr_sub_indikator[] = $row_sub_indikator;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Delete Sub Indikator';
  }

  $result[] = $arr_sub_indikator;

  echo json_encode($result);
  // echo $sql;

} else if ($request == "saveSubIndikator") {

  $create = $data->sub_indikator_baru;
  $sub_indikator = json_decode($create, true);

  $id_level = $data->id_level;
  $id_indikator = $data->id_indikator;

  // for($i=0; $i<count($sub_indikator); $i++) {
  //   print_r($sub_indikator[$i]['indikator']);
  // }

  try {
    $sql = "INSERT INTO hrm_apprasial_sub_indikator(sub_indikator,bobot,id_indikator_kriteria,urutan,status_sub_indikator,id_level,id_indikator) 
            VALUES (:sub_indikator,:bobot,:id_indikator_kriteria,:urutan,'1',:id_level,:id_indikator)";
    $stmt = $dbh->prepare($sql);

    for ($i = 0; $i < count($sub_indikator); $i++) {

      $stmt->bindValue(':sub_indikator', $sub_indikator[$i]['sub_indikator']);
      $stmt->bindValue(':bobot', $sub_indikator[$i]['bobot']);
      $stmt->bindValue(':id_indikator_kriteria', $sub_indikator[$i]['indikator']);
      $stmt->bindValue(':urutan', $sub_indikator[$i]['no_urut']);
      $stmt->bindValue(':id_level', $id_level);
      $stmt->bindValue(':id_indikator', $id_indikator);

      $stmt->execute();
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal insert Sub Indikator';
  }
} else if ($request == "getSubIndikatorById") {

  $result = [];

  $id_sub_indikator = $data->id_sub_indikator;

  try {

    $sql = "SELECT hrm_apprasial_sub_indikator.*, hrm_apprasial_indikator_kriteria.indikator, hrm_apprasial_indikator.kd_indikator, hrm_apprasial_indikator_kriteria.max_urutan
            FROM hrm_apprasial_sub_indikator
            JOIN hrm_apprasial_indikator_kriteria ON hrm_apprasial_indikator_kriteria.id_indikator_kriteria = hrm_apprasial_sub_indikator.id_indikator_kriteria
            JOIN hrm_apprasial_indikator ON hrm_apprasial_indikator.id_indikator = hrm_apprasial_sub_indikator.id_indikator
            WHERE status_sub_indikator = '1' AND id_sub_indikator = '$id_sub_indikator'";

    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_sub_indikator = [];

    $arr_sub_indikator = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Sub Indikator';
  }

  $result[] = $arr_sub_indikator;

  echo json_encode($result);
} else if ($request == "deleteSubIndikator") {

  $id_sub_indikator = $data->id_sub_indikator;

  try {
    $sql = "UPDATE hrm_apprasial_sub_indikator SET status_sub_indikator = 0 WHERE id_sub_indikator = '$id_sub_indikator'";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Delete Karyawan';
  }
} else if ($request == "importExcel") {

  $result = [];

  $mydata = file_get_contents("https://web.pt-saa.com/hrm/gps_lat_long_customer_bali.json");

  $array = json_decode($mydata);

  // foreach ($array as $arr_data) {

  //   $lat = $arr_data->gpsLatitude;
  //   $long = $arr_data->gpsLongitude;
  //   $id = $arr_data->idToko;

  //   // echo $lat.",".$long.",".$id, "\n";

  //   $sql = "UPDATE t_card_add SET gpslat = '$lat', gpslong = '$long' WHERE tiro_code = '$id'";
  //   $stmt = $dbgl->prepare($sql);

  //   $stmt->execute();

  // }

} else if ($request == "updateSubIndikator") {

  $id_sub_indikator = $data->id_sub_indikator;
  $id_indikator = $data->id_indikator;
  $id_indikator_kriteria = $data->id_indikator_kriteria;
  $sub_indikator = $data->sub_indikator;
  $urutan = $data->urutan;
  $bobot = $data->bobot;

  try {

    $sql_update = "UPDATE hrm_apprasial_sub_indikator SET 
                    id_indikator = '$id_indikator', 
                    id_indikator_kriteria = '$id_indikator_kriteria', 
                    sub_indikator = '$sub_indikator', 
                    bobot = '$bobot', 
                    urutan = '$urutan' 
                  WHERE id_sub_indikator = '$id_sub_indikator'";
    $stmt = $dbh->prepare($sql_update);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Update Sub Indikator';
  }
} else if ($request == "getRandomFirstSubIndikator") {

  $result = [];

  $id_indikator = $data->id_indikator;

  try {

    $sql = "SELECT * FROM hrm_apprasial_sub_indikator 
            WHERE id_indikator_kriteria = '$id_indikator' AND urutan = '1' AND status_sub_indikator = '1' 
            ORDER BY random() LIMIT 1";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_sub_indikator = [];

    $arr_sub_indikator = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Delete Sub Indikator';
  }

  $result[] = $arr_sub_indikator;

  echo json_encode($result);
} else if ($request == "getDetailIndikator") {

  $result = [];

  $id_indikator_kriteria = $data->id_indikator_kriteria;
  $urutan = $data->urutan;

  try {

    $sql = "SELECT * FROM hrm_apprasial_detail_indikator_kriteria WHERE id_indikator_kriteria = '$id_indikator_kriteria' AND urutan = '$urutan'";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_detail_indikator = [];

    $arr_detail_indikator = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Indikator';
  }

  $result[] = $arr_detail_indikator;

  echo json_encode($result);
} else if ($request == "getRandomSecondSubIndikator") {

  $result = [];

  $id_indikator = $data->id_indikator;
  $urutan = $data->urutan;

  try {

    $sql = "SELECT * FROM hrm_apprasial_sub_indikator 
            WHERE id_indikator_kriteria = '$id_indikator' AND urutan = '$urutan' AND status_sub_indikator = '1' 
            ORDER BY random() LIMIT 1";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_sub_indikator = [];

    $arr_sub_indikator = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Sub Indikator';
  }

  $result[] = $arr_sub_indikator;

  echo json_encode($result);
} else if ($request == "savePenilaian") {

  $id_num_penilai = $data->id_num_penilai;
  $id_num_target = $data->id_num_target;
  $id_indikator = $data->id_indikator;
  $id_indikator_kriteria = $data->id_indikator_kriteria;
  $id_sub_indikator = $data->id_sub_indikator;
  $bobot = $data->bobot;
  $nilai = $data->nilai;
  $total_nilai = $bobot * $nilai;

  try {
    $sql = "INSERT INTO hrm_apprasial_penilaian(id_num_penilai,id_num_target,id_indikator,id_sub_indikator,bobot,nilai,total_nilai,id_indikator_kriteria) VALUES ('$id_num_penilai','$id_num_target','$id_indikator','$id_sub_indikator','$bobot','$nilai','$total_nilai','$id_indikator_kriteria')";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal insert Indikator';
  }
} else if ($request == "savePenilaianHRD") {

  $id_num_penilai = $data->id_num_penilai;
  $id_num_target = $data->id_num_target;
  $id_indikator = $data->id_indikator;
  $id_indikator_kriteria = $data->id_indikator_kriteria;
  $id_sub_indikator = $data->id_sub_indikator;
  $bobot = $data->bobot;
  $nilai = $data->nilai;
  $total_nilai = $bobot * $nilai;

  try {
    $sql = "INSERT INTO hrm_apprasial_penilaian_hrd(id_num_penilai,id_num_target,id_indikator,id_sub_indikator,bobot,nilai,total_nilai,id_indikator_kriteria) VALUES ('$id_num_penilai','$id_num_target','$id_indikator','$id_sub_indikator','$bobot','$nilai','$total_nilai','$id_indikator_kriteria')";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal insert Penilaian HRD';
  }
} else if ($request == "getNilaiTemp") {

  $id_num_penilai = $data->id_num_penilai;
  $id_num_target = $data->id_num_target;
  $id_indikator = $data->id_indikator;
  $id_indikator_kriteria = $data->id_indikator_kriteria;
  $id_sub_indikator = $data->id_sub_indikator;

  $result = [];

  try {
    $sql2 = "SELECT SUM(nilai) FROM hrm_apprasial_penilaian 
              WHERE id_num_penilai = '$id_num_penilai' 
              AND id_num_target = '$id_num_target'
              AND id_indikator = '$id_indikator'
              AND id_indikator_kriteria = '$id_indikator_kriteria'";
    $stmt2 = $dbh->prepare($sql2);

    $stmt2->execute();

    $total_nilai = $stmt2->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal insert Indikator';
  }

  $result = $total_nilai;

  echo json_encode($result);
} else if ($request == "saveTotalPenilaian") {

  $id_num_penilai = $data->id_num_penilai;
  $id_num_target = $data->id_num_target;
  $id_indikator = $data->id_indikator;
  $id_indikator_kriteria = $data->id_indikator_kriteria;

  try {

    $sql = "SELECT DISTINCT(id_indikator_kriteria) FROM hrm_apprasial_penilaian 
              WHERE id_num_target = '$id_num_target' 
              AND id_num_penilai = '$id_num_penilai' 
              AND id_indikator = '$id_indikator'";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $total_indikator = [];

    while ($row_total = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $total_indikator[] = $row_total;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Total Penilaian';
  }

  for ($i = 0; $i < count($total_indikator); $i++) {

    $kriteria = $total_indikator[$i]['id_indikator_kriteria'];

    try {

      $sql = "SELECT SUM(total_nilai) AS total_nilai 
                FROM hrm_apprasial_penilaian 
                WHERE id_num_target = '$id_num_target' 
                AND id_num_penilai = '$id_num_penilai' 
                AND id_indikator = '$id_indikator' 
                AND id_indikator_kriteria = '$kriteria'";


      $stmt = $dbh->prepare($sql);
      $stmt->execute();

      $total_kriteria = $stmt->fetch(PDO::FETCH_ASSOC);

      // print_r($total_kriteria);

      $xtotal_nilai = $total_kriteria['total_nilai'];

      try {

        $sql = "INSERT INTO 
                  hrm_apprasial_total_penilaian(id_num_penilai,id_num_target,id_indikator,id_indikator_kriteria,total_nilai_by_indikator) 
                  VALUES 
                  ('$id_num_penilai','$id_num_target','$id_indikator','$kriteria','$xtotal_nilai')";


        $stmt = $dbh->prepare($sql);
        $stmt->execute();
      } catch (PDOException $e) {
        $e->getMessage();
        echo 'gagal Show Total Penilaian';
      }
    } catch (PDOException $e) {
      $e->getMessage();
      echo 'gagal Show Total Penilaian';
    }
  }

  try {

    $sql = "SELECT SUM(total_nilai_by_indikator) FROM hrm_apprasial_total_penilaian
              WHERE id_num_target = '$id_num_target'
              AND id_num_penilai = '$id_num_penilai'
              AND id_indikator = '$id_indikator'";


    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $grand_total_kriteria = $stmt->fetch(PDO::FETCH_ASSOC);

    $xgrand_total_nilai = $grand_total_kriteria['sum'];

    try {

      $sql = "INSERT INTO 
                hrm_apprasial_grand_total_penilaian(id_num_penilai,id_num_target,id_indikator,grand_total_nilai_by_indikator) 
                VALUES 
                ('$id_num_penilai','$id_num_target','$id_indikator','$xgrand_total_nilai')";


      $stmt = $dbh->prepare($sql);
      $stmt->execute();
    } catch (PDOException $e) {
      $e->getMessage();
      echo 'gagal Show Total Penilaian';
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Total Penilaian';
  }

  $result = [];

  try {

    $sql = "SELECT a.indikator, b.total_nilai_by_indikator
              FROM hrm_apprasial_indikator_kriteria a
              JOIN hrm_apprasial_total_penilaian b
              ON a.id_indikator_kriteria = b.id_indikator_kriteria
              WHERE b.id_num_target = '$id_num_target'
              AND b.id_num_penilai = '$id_num_penilai'
              AND b.id_indikator = '$id_indikator'";


    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $view_total_indikator = [];

    while ($view_total = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $view_total_indikator[] = $view_total;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Total Penilaian By Kriteria';
  }

  $result[] = $view_total_indikator;

  echo json_encode($result);
} else if ($request == "saveTotalPenilaianHRD") {

  $id_num_penilai = $data->id_num_penilai;
  $id_num_target = $data->id_num_target;
  $id_indikator = $data->id_indikator;
  $id_indikator_kriteria = $data->id_indikator_kriteria;

  try {

    $sql = "SELECT DISTINCT(id_indikator_kriteria) FROM hrm_apprasial_penilaian_hrd 
              WHERE id_num_target = '$id_num_target' 
              AND id_num_penilai = '$id_num_penilai' 
              AND id_indikator = '$id_indikator'";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $total_indikator = [];

    while ($row_total = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $total_indikator[] = $row_total;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Total Penilaian';
  }

  for ($i = 0; $i < count($total_indikator); $i++) {

    $kriteria = $total_indikator[$i]['id_indikator_kriteria'];

    // print_r($kriteria);

    try {

      $sql = "SELECT SUM(total_nilai) AS total_nilai 
                FROM hrm_apprasial_penilaian_hrd 
                WHERE id_num_target = '$id_num_target' 
                AND id_num_penilai = '$id_num_penilai' 
                AND id_indikator = '$id_indikator' 
                AND id_indikator_kriteria = '$kriteria'";


      $stmt = $dbh->prepare($sql);
      $stmt->execute();

      $total_kriteria = $stmt->fetch(PDO::FETCH_ASSOC);

      // print_r($total_kriteria);

      $xtotal_nilai = $total_kriteria['total_nilai'];

      try {

        $sql = "INSERT INTO 
                  hrm_apprasial_total_penilaian_hrd(id_num_penilai,id_num_target,id_indikator,id_indikator_kriteria,total_nilai_by_indikator) 
                  VALUES 
                  ('$id_num_penilai','$id_num_target','$id_indikator','$kriteria','$xtotal_nilai')";


        $stmt = $dbh->prepare($sql);
        $stmt->execute();
      } catch (PDOException $e) {
        $e->getMessage();
        echo 'gagal Show Total Penilaian';
      }
    } catch (PDOException $e) {
      $e->getMessage();
      echo 'gagal Show Total Penilaian';
    }
  }

  try {

    $sql = "SELECT SUM(total_nilai_by_indikator) FROM hrm_apprasial_total_penilaian_hrd
              WHERE id_num_target = '$id_num_target'
              AND id_num_penilai = '$id_num_penilai'
              AND id_indikator = '$id_indikator'";


    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $grand_total_kriteria = $stmt->fetch(PDO::FETCH_ASSOC);

    $xgrand_total_nilai = $grand_total_kriteria['sum'];

    try {

      $sql = "INSERT INTO 
                hrm_apprasial_grand_total_penilaian_hrd(id_num_penilai,id_num_target,id_indikator,grand_total_nilai_by_indikator) 
                VALUES 
                ('$id_num_penilai','$id_num_target','$id_indikator','$xgrand_total_nilai')";


      $stmt = $dbh->prepare($sql);
      $stmt->execute();
    } catch (PDOException $e) {
      $e->getMessage();
      echo 'gagal Show Total Penilaian';
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Total Penilaian';
  }

  $result = [];

  try {

    $sql = "SELECT a.indikator, b.total_nilai_by_indikator
              FROM hrm_apprasial_indikator_kriteria a
              JOIN hrm_apprasial_total_penilaian_hrd b
              ON a.id_indikator_kriteria = b.id_indikator_kriteria
              WHERE b.id_num_target = '$id_num_target'
              AND b.id_num_penilai = '$id_num_penilai'
              AND b.id_indikator = '$id_indikator'";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $view_total_indikator = [];

    while ($view_total = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $view_total_indikator[] = $view_total;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Total Penilaian By Kriteria';
  }

  $result[] = $view_total_indikator;

  echo json_encode($result);
} else if ($request == "getFirstIndikator") {

  $result = [];

  $kd_indikator = $data->kd_indikator;
  $type_penilai = $data->type_penilai;
  $id_level = $data->id_level;

  try {

    $sql = "SELECT a.*
            FROM hrm_apprasial_indikator_kriteria a
            JOIN hrm_apprasial_indikator b ON a.id_indikator = b.id_indikator
            JOIN hrm_apprasial_tab_penilaian c ON a.id_indikator_kriteria = c.id_indikator_kriteria
            WHERE b.kd_indikator = '$kd_indikator' 
            AND a.deleted = '0'
            AND c.tipe_penilai = '$type_penilai'
            AND c.id_level = '$id_level'
            ORDER BY a.id_indikator_kriteria ASC";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_indikator = [];

    $arr_indikator = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Indikator';
  }

  $result[] = $arr_indikator;

  echo json_encode($result);
} else if ($request == "getSecondIndikator") {

  $result = [];

  $kd_indikator = $data->kd_indikator;
  $type_penilai = $data->type_penilai;
  $offset = $data->offset;
  $id_level = $data->id_level;

  try {

    $sql = "SELECT a.*
            FROM hrm_apprasial_indikator_kriteria a
            JOIN hrm_apprasial_indikator b ON a.id_indikator = b.id_indikator
            JOIN hrm_apprasial_tab_penilaian c ON a.id_indikator_kriteria = c.id_indikator_kriteria
            WHERE b.kd_indikator = '$kd_indikator' 
            AND a.deleted = '0'
            AND c.tipe_penilai = '$type_penilai'
            AND c.id_level = '$id_level'
            ORDER BY a.id_indikator_kriteria ASC OFFSET $offset ROWS FETCH NEXT 1 ROWS ONLY";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_indikator = [];

    $arr_indikator = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Indikator';
  }

  $result[] = $arr_indikator;

  echo json_encode($result);
} else if ($request == "getLevel") {

  $result = [];

  try {

    $sql = "SELECT * FROM hrm_level ORDER BY id_level ASC";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_level = [];

    while ($row_level = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $arr_level[] = $row_level;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Level';
  }

  $result[] = $arr_level;

  echo json_encode($result);
} else if ($request == "getIndikatorPeriode2") {

  $result = [];

  $kd_indikator = $data->kd_indikator;

  try {

    $sql = "SELECT a.kd_indikator, to_char(a.periode_month, 'Month YYYY') as periode_month, to_char(a.expired, 'DD Month YYYY') as expired, a.status_active, a.actived_by, to_char(a.active_date, 'DD Month YYYY') as active_date, a.id_level, a.id_indikator_periode, b.level, c.id_indikator, concat(a.kd_indikator, ' - ', b.level) AS nomor_penilaian
              FROM hrm_apprasial_indikator_periode a
              JOIN hrm_level b ON a.id_level = b.id_level
              JOIN hrm_apprasial_indikator c ON a.kd_indikator = c.kd_indikator 
              WHERE a.deleted = '0' AND a.status_active = '1' AND a.kd_indikator = '$kd_indikator' ORDER BY a.id_indikator_periode ASC";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_indikator = [];

    while ($row_indikator = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $arr_indikator[] = $row_indikator;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Level';
  }

  $result[] = $arr_indikator;

  echo json_encode($result);
} else if ($request == "getIndikatorPeriodeById") {

  $result = [];

  $id_indikator_periode = $data->id_indikator_periode;

  try {

    $sql = "SELECT a.kd_indikator, a.periode_month, a.expired, a.status_active, a.actived_by, 
                to_char(a.active_date, 'DD Month YYYY') AS active_date, a.id_level, a.id_indikator_periode, 
                a.kd_indikator, to_char(a.periode_month, 'Month YYYY') AS periode_format, to_char(a.expired, 'DD Month YYYY') AS expired_format,
                b.level, c.id_indikator 
            FROM hrm_apprasial_indikator_periode a
            JOIN hrm_level b ON a.id_level = b.id_level
            JOIN hrm_apprasial_indikator c ON a.kd_indikator = c.kd_indikator
            WHERE id_indikator_periode = '$id_indikator_periode' AND a.deleted = '0'";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_indikator = [];

    $arr_indikator = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show indikator periode';
  }

  $result[] = $arr_indikator;

  echo json_encode($result);
} else if ($request == "getPeriode") {

  $result = [];

  try {

    $sql = "SELECT to_char(periode, 'Month YYYY') as periode_date, to_char(expired, 'DD Month YYYY') as expired_date, hrm_apprasial_periode.* , hrm_level.level
            FROM hrm_apprasial_periode
            JOIN hrm_level ON hrm_apprasial_periode.id_level = hrm_level.id_level
            ORDER BY id_periode ASC";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_periode = [];

    while ($row_periode = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $arr_periode[] = $row_periode;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Periode';
  }

  $result[] = $arr_periode;

  echo json_encode($result);
} else if ($request == "generate_nomor") {

  try {
    $sql = "SELECT id_indikator_periode FROM hrm_apprasial_indikator_periode ORDER BY id_indikator_periode DESC";
    $stmt = $dbh->prepare($sql);
    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal';
  }

  $result = $stmt->fetchColumn();
  if ($result == Null) {
    $result = 1;
  } else {
    $result = $result;
  }
  echo json_encode($result);
} else if ($request == "updateIndikatorPeriode") {

  $id_indikator_periode = $data->id_indikator_periode;
  $id_level = $data->id_level;
  $expired = $data->expired;
  $periode = $data->periode;

  try {
    $sql = "UPDATE hrm_apprasial_indikator_periode SET periode_month = '$periode', expired = '$expired', id_level = '$id_level' WHERE id_indikator_periode = '$id_indikator_periode'";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal update Indikator Periode';
  }
} else if ($request == "saveIndikatorPeriode") {

  $nopenilaian = $data->nopenilaian;
  $id_level = $data->id_level;
  $expired = $data->expired;
  $periode = $data->periode;

  try {
    $sql = "INSERT INTO hrm_apprasial_indikator_periode(periode_month,expired,kd_indikator,id_level,status_active,deleted) 
            VALUES 
            (:periode_month,:expired,:kd_indikator,:id_level,'0','0')";
    $stmt = $dbh->prepare($sql);

    $stmt->bindValue(':periode_month', $periode);
    $stmt->bindValue(':expired', $expired);
    $stmt->bindValue(':kd_indikator', $nopenilaian);
    $stmt->bindValue(':id_level', $id_level);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal insert Indikator Periode';
  }
} else if ($request == "deleteIndikatorPeriode") {

  $kd_indikator = $data->kd_indikator;

  try {

    $sql_delete1 = "UPDATE hrm_apprasial_indikator_periode SET deleted = '1' WHERE kd_indikator = '$kd_indikator'";
    $stmt1 = $dbh->prepare($sql_delete1);
    $stmt1->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Delete Indikator Periode';
  }

  try {

    $sql_delete2 = "UPDATE hrm_apprasial_indikator SET delete_indikator = '1' WHERE kd_indikator = '$kd_indikator'";
    $stmt2 = $dbh->prepare($sql_delete2);
    $stmt2->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Delete Indikator Periode';
  }
} else if ($request == "checkAllStatusIndikatorPeriode") {

  $result = [];
  $id_level = $data->id_level;

  try {

    $sql = "SELECT hrm_apprasial_indikator_periode.*, hrm_level.level 
            FROM hrm_apprasial_indikator_periode 
            JOIN hrm_level ON hrm_apprasial_indikator_periode.id_level = hrm_level.id_level
            WHERE status_active = '1' AND hrm_level.id_level = '$id_level'";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_periode = [];

    while ($row_periode = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $arr_periode = $row_periode;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Periode';
  }

  $result[] = $arr_periode;

  echo json_encode($result);
} else if ($request == "checkKdIndikator") {

  $result = [];

  $nopenilaian = $data->nopenilaian;

  try {

    $sql = "SELECT kd_indikator FROM hrm_apprasial_indikator_periode WHERE kd_indikator = '$nopenilaian'";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_indikator = [];

    $arr_indikator = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Indikator';
  }

  $result[] = $arr_indikator;

  echo json_encode($result);
} else if ($request == "SaveIndikatorKriteria") {

  $id_indikator = $data->id_indikator;
  $kriteria_indikator = $data->kriteria_indikator;

  $arr_kriteria = json_decode($kriteria_indikator, true);

  try {
    $sql = 'INSERT INTO hrm_apprasial_indikator_kriteria(id_indikator,indikator,persentase,max_urutan,status_kriteria,deleted) 
            VALUES (:id_indikator,:indikator,:persentase,:max_urutan,:status_kriteria,:deleted)';
    $stmt = $dbh->prepare($sql);

    for ($i = 0; $i < count($arr_kriteria); $i++) {
      $stmt->bindValue(':id_indikator', $id_indikator);
      $stmt->bindValue(':indikator', $arr_kriteria[$i]['indikator']);
      $stmt->bindValue(':persentase', $arr_kriteria[$i]['persentase']);
      $stmt->bindValue(':max_urutan', $arr_kriteria[$i]['max_urutan']);
      $stmt->bindValue(':status_kriteria', '1');
      $stmt->bindValue(':deleted', '0');
      $stmt->execute();
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal insert indikator kriteria' . $e;
  }
} else if ($request == "UpdateStatusIndikatorPeriode") {

  $status = $data->status;
  $id_level = $data->id_level;
  $id_indikator_periode = $data->id_indikator_periode;
  $username = $data->username;
  $today = date("Y-m-d H:i:s");

  if ($status == 1) {

    try {

      $sql_check = "SELECT * FROM hrm_apprasial_indikator_periode WHERE id_level = '$id_level' AND status_active = '1'";
      $stmt_check = $dbh->prepare($sql_check);
      $stmt_check->execute();

      $arr_periode[] = $stmt_check->fetch(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
      $e->getMessage();
      echo 'gagal Show Indikator Periode';
    }

    $status_active = 1;

    $count = array_count_values(array_column($arr_periode, 'status_active'))[$status_active];

    if ($count < 1) {

      try {

        $sql_update = "UPDATE hrm_apprasial_indikator_periode SET status_active = '$status', actived_by = '$username', active_date = '$today' WHERE id_indikator_periode = '$id_indikator_periode'";
        $stmt = $dbh->prepare($sql_update);

        $stmt->execute();
      } catch (PDOException $e) {
        $e->getMessage();
        echo 'gagal Update Sub Indikator';
      }

      echo "Nomor Penilaian Berhasil Di Aktifkan";
    } else {
      echo "Nomor Penilaian yang aktif sudah ada! Mohon menonaktifkan terlebih dahulu sebelum aktifkan kembali!";
    }
  } else {

    try {

      $sql_update = "UPDATE hrm_apprasial_indikator_periode SET status_active = '$status' WHERE id_indikator_periode = '$id_indikator_periode'";
      $stmt = $dbh->prepare($sql_update);

      $stmt->execute();
    } catch (PDOException $e) {
      $e->getMessage();
      echo 'gagal Update Sub Indikator';
    }

    echo "Nomor Penilaian Berhasil Di Non Aktifkan";
  }

  // try {

  //   $sql_check = "SELECT * FROM hrm_apprasial_indikator_periode WHERE id_level = '$id_level' AND status_active = '1'";
  //   $stmt_check = $dbh->prepare($sql_check);
  //   $stmt_check->execute();

  //   $arr_periode[] = $stmt_check->fetch(PDO::FETCH_ASSOC);

  // } catch (PDOException $e) {
  //   $e->getMessage();
  //   echo 'gagal Show Indikator Periode'; 
  // }

  // $status_active = 1;

  // echo array_count_values(array_column($arr_periode, 'status_active'))[$status_active];

} else if ($request == "getDetailIndikatorPeriode") {

  $result = [];

  $kd_indikator = $data->kd_indikator;

  try {

    $sql = "SELECT hrm_apprasial_indikator_kriteria.*
            FROM hrm_apprasial_indikator_kriteria 
            JOIN hrm_apprasial_indikator ON hrm_apprasial_indikator_kriteria.id_indikator = hrm_apprasial_indikator.id_indikator
            WHERE hrm_apprasial_indikator.kd_indikator = '$kd_indikator' AND hrm_apprasial_indikator_kriteria.deleted = '0'
            ORDER BY hrm_apprasial_indikator_kriteria.id_indikator_kriteria ASC";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_detail_indikator = [];

    while ($row_detail_indikator = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $arr_detail_indikator[] = $row_detail_indikator;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Level';
  }

  $result[] = $arr_detail_indikator;

  echo json_encode($result);
} else if ($request == "checkTotalPersentaseDetailIndikator") {

  $result = [];

  $kd_indikator = $data->kd_indikator;

  try {

    $sql = "SELECT SUM(hrm_apprasial_indikator_kriteria.persentase) AS total_persentase
            FROM hrm_apprasial_indikator_kriteria 
            JOIN hrm_apprasial_indikator ON hrm_apprasial_indikator_kriteria.id_indikator = hrm_apprasial_indikator.id_indikator
            WHERE hrm_apprasial_indikator.kd_indikator = '$kd_indikator' AND hrm_apprasial_indikator_kriteria.deleted = '0'";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_indikator = [];

    $arr_indikator = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Indikator';
  }

  try {

    $sql2 = "SELECT hrm_apprasial_indikator_kriteria.*, hrm_apprasial_indikator.kd_indikator
            FROM hrm_apprasial_indikator_kriteria 
            JOIN hrm_apprasial_indikator ON hrm_apprasial_indikator_kriteria.id_indikator = hrm_apprasial_indikator.id_indikator
            WHERE hrm_apprasial_indikator.kd_indikator = '$kd_indikator' AND hrm_apprasial_indikator_kriteria.deleted = '0'";
    $stmt2 = $dbh->prepare($sql2);

    $stmt2->execute();

    $arr_indikator2 = [];

    $arr_indikator2 = $stmt2->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Indikator 2';
  }

  $result[] = $arr_indikator;
  $result[] = $arr_indikator2;

  echo json_encode($result);
} else if ($request == "checkTotalPersentaseDetailIndikatorEdit") {

  $result = [];

  $kd_indikator = $data->kd_indikator;
  $id_indikator_kriteria = $data->id_indikator_kriteria;

  try {

    $sql = "SELECT SUM(hrm_apprasial_indikator_kriteria.persentase) AS total_persentase
            FROM hrm_apprasial_indikator_kriteria 
            JOIN hrm_apprasial_indikator ON hrm_apprasial_indikator_kriteria.id_indikator = hrm_apprasial_indikator.id_indikator
            WHERE hrm_apprasial_indikator.kd_indikator = '$kd_indikator' AND hrm_apprasial_indikator_kriteria.deleted = '0'";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_indikator = [];

    $arr_indikator = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Indikator';
  }

  try {

    $sql2 = "SELECT hrm_apprasial_indikator_kriteria.*, hrm_apprasial_indikator.kd_indikator
            FROM hrm_apprasial_indikator_kriteria 
            JOIN hrm_apprasial_indikator ON hrm_apprasial_indikator_kriteria.id_indikator = hrm_apprasial_indikator.id_indikator
            WHERE hrm_apprasial_indikator.kd_indikator = '$kd_indikator' 
            AND hrm_apprasial_indikator_kriteria.id_indikator_kriteria = '$id_indikator_kriteria'
            AND hrm_apprasial_indikator_kriteria.deleted = '0'";
    $stmt2 = $dbh->prepare($sql2);

    $stmt2->execute();

    $arr_indikator2 = [];

    $arr_indikator2 = $stmt2->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Indikator 2';
  }

  $result[] = $arr_indikator;
  $result[] = $arr_indikator2;

  echo json_encode($result);
} else if ($request == "updateDetailIndikatorByKode") {

  $id_indikator_kriteria = $data->id_indikator_kriteria;
  $indikator =  $data->indikator;
  $persentase = $data->persentase;
  $max_urutan = $data->max_urutan;

  try {
    $sql = "UPDATE hrm_apprasial_indikator_kriteria SET indikator = '$indikator', persentase = '$persentase', max_urutan = '$max_urutan' 
            WHERE id_indikator_kriteria = '$id_indikator_kriteria'";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal update Indikator Kriteria';
  }
} else if ($request == "saveDetailIndikatorByKode") {

  $detail_id_indikator = $data->detail_id_indikator;
  $detail_indikator =  $data->detail_indikator;
  $detail_persentase = $data->detail_persentase;
  $detail_max_urutan = $data->detail_max_urutan;

  try {
    $sql = "INSERT INTO hrm_apprasial_indikator_kriteria(id_indikator,indikator,persentase,max_urutan,status_kriteria,deleted) 
            VALUES (:id_indikator,:indikator,:persentase,:max_urutan,'1','0')";
    $stmt = $dbh->prepare($sql);

    $stmt->bindValue(':id_indikator', $detail_id_indikator);
    $stmt->bindValue(':indikator', $detail_indikator);
    $stmt->bindValue(':persentase', $detail_persentase);
    $stmt->bindValue(':max_urutan', $detail_max_urutan);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal insert Indikator Periode';
  }
} else if ($request == "getDetailIndikatorById") {

  $request = [];

  $id_indikator_kriteria = $data->id_indikator_kriteria;

  try {

    $sql = "SELECT hrm_apprasial_indikator_kriteria.*, hrm_apprasial_indikator.kd_indikator 
            FROM hrm_apprasial_indikator_kriteria
            JOIN hrm_apprasial_indikator ON hrm_apprasial_indikator_kriteria.id_indikator = hrm_apprasial_indikator.id_indikator
            WHERE hrm_apprasial_indikator_kriteria.id_indikator_kriteria = '$id_indikator_kriteria'";

    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_kriteria = [];

    $arr_kriteria = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Kriteria';
  }

  $result[] = $arr_kriteria;

  echo json_encode($result);
} else if ($request == "getDetailIndikatorKriteria") {

  $result = [];

  $id_indikator_kriteria = $data->id_indikator_kriteria;

  try {

    $sql = "SELECT hrm_apprasial_detail_indikator_kriteria.*
            FROM hrm_apprasial_detail_indikator_kriteria
            JOIN hrm_apprasial_indikator_kriteria ON hrm_apprasial_detail_indikator_kriteria.id_indikator_kriteria = hrm_apprasial_indikator_kriteria.id_indikator_kriteria
            WHERE hrm_apprasial_detail_indikator_kriteria.id_indikator_kriteria = '$id_indikator_kriteria'
            AND hrm_apprasial_detail_indikator_kriteria.deleted = '0'";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_detail_kriteria = [];

    while ($row_detail_kriteria = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $arr_detail_kriteria[] = $row_detail_kriteria;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Kriteria';
  }

  try {

    $sql2 = "SELECT * FROM hrm_apprasial_indikator_kriteria WHERE id_indikator_kriteria = '$id_indikator_kriteria'";
    $stmt2 = $dbh->prepare($sql2);

    $stmt2->execute();

    $arr_indikator_kriteria = [];

    $arr_indikator_kriteria = $stmt2->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show indikator periode';
  }

  $urutan = [];
  for ($x = 0; $x < $arr_indikator_kriteria['max_urutan']; $x++) {
    $urutan[] = array("urutan" => $x + 1);
  }

  $result[] = $arr_detail_kriteria;
  $result[] = $arr_indikator_kriteria;
  $result[] = $urutan;

  echo json_encode($result);
} else if ($request == "deleteDetailIndikatorKriteria") {

  $id_detail_indikator_kriteria = $data->id_detail_indikator_kriteria;

  try {

    $sql_delete1 = "UPDATE hrm_apprasial_detail_indikator_kriteria SET deleted = '1' WHERE id_detail_indikator_kriteria = '$id_detail_indikator_kriteria'";
    $stmt1 = $dbh->prepare($sql_delete1);
    $stmt1->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Delete Indikator Periode';
  }
} else if ($request == "checkSaveDetailIndikatorKriteria") {

  $result = [];

  $id_indikator_kriteria = $data->id_indikator_kriteria;
  $urutan = $data->urutan;

  try {

    $sql = "SELECT * FROM hrm_apprasial_detail_indikator_kriteria WHERE id_indikator_kriteria = '$id_indikator_kriteria' AND deleted = '0'";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_detail = [];

    while ($row_detail = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $arr_detail[] = $row_detail;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Detail';
  }

  try {

    $sql2 = "SELECT * FROM hrm_apprasial_detail_indikator_kriteria WHERE id_indikator_kriteria = '$id_indikator_kriteria' AND urutan = '$urutan' AND deleted = '0'";
    $stmt2 = $dbh->prepare($sql2);

    $stmt2->execute();

    $arr_detail2 = [];

    while ($row_detail2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
      $arr_detail2[] = $row_detail2;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Detail';
  }

  $result[] = $arr_detail;
  $result[] = $arr_detail2;

  echo json_encode($result);
} else if ($request == "saveDetailIndikatorKriteria") {

  $id_indikator_kriteria = $data->id_indikator_kriteria;
  $urutan =  $data->urutan;
  $bobot = $data->bobot;

  try {
    $sql = "INSERT INTO hrm_apprasial_detail_indikator_kriteria(id_indikator_kriteria,urutan,bobot,deleted) 
            VALUES ('$id_indikator_kriteria','$urutan','$bobot','0')";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal insert Indikator Periode';
  }
} else if ($request == "getDetailIndikatorKriteriaByID") {

  $result = [];

  $id_indikator_kriteria = $data->id_indikator_kriteria;
  $id_detail_indikator_kriteria = $data->id_detail_indikator_kriteria;

  try {

    $sql = "SELECT hrm_apprasial_detail_indikator_kriteria.*
            FROM hrm_apprasial_detail_indikator_kriteria
            JOIN hrm_apprasial_indikator_kriteria ON hrm_apprasial_detail_indikator_kriteria.id_indikator_kriteria = hrm_apprasial_indikator_kriteria.id_indikator_kriteria
            WHERE hrm_apprasial_detail_indikator_kriteria.id_indikator_kriteria = '$id_indikator_kriteria'
            AND hrm_apprasial_detail_indikator_kriteria.deleted = '0'
            AND hrm_apprasial_detail_indikator_kriteria.id_detail_indikator_kriteria = '$id_detail_indikator_kriteria'";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_detail_kriteria = [];

    $arr_detail_kriteria = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Kriteria';
  }

  $result[] = $arr_detail_kriteria;

  echo json_encode($result);
} else if ($request == "checkUpdateDetailIndikatorKriteria") {

  $result = [];

  $id_indikator_kriteria = $data->id_indikator_kriteria;
  $id_detail_indikator_kriteria = $data->id_detail_indikator_kriteria;
  $urutan = $data->urutan;

  try {

    $sql2 = "SELECT * FROM hrm_apprasial_detail_indikator_kriteria WHERE id_indikator_kriteria = '$id_indikator_kriteria' AND urutan = '$urutan' AND deleted = '0'
             EXCEPT
             SELECT * FROM hrm_apprasial_detail_indikator_kriteria WHERE id_indikator_kriteria = '$id_indikator_kriteria'
             AND id_detail_indikator_kriteria = '$id_detail_indikator_kriteria' 
             AND urutan = '$urutan' AND deleted = '0'";
    $stmt2 = $dbh->prepare($sql2);

    $stmt2->execute();

    $arr_detail2 = [];

    while ($row_detail2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
      $arr_detail2[] = $row_detail2;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Detail';
  }

  $result[] = $arr_detail2;

  echo json_encode($result);
} else if ($request == "updateDetailIndikatorKriteria") {

  $id_detail_indikator_kriteria = $data->id_detail_indikator_kriteria;
  $urutan =  $data->urutan;
  $bobot = $data->bobot;

  try {
    $sql = "UPDATE hrm_apprasial_detail_indikator_kriteria SET urutan = '$urutan', bobot = '$bobot' WHERE id_detail_indikator_kriteria = '$id_detail_indikator_kriteria'";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Update Detail Indikator';
  }
} else if ($request == "getPosisi") {

  $kd_off = $data->kd_off;
  $kd_dept = $data->kd_dept;

  try {
    $sql1 = "SELECT DISTINCT(hrm_employee.kd_pos), hrm_position.id_pos, hrm_position.position
             FROM hrm_employee 
             JOIN hrm_position ON hrm_position.kd_pos = hrm_employee.kd_pos
             WHERE kd_dept = '$kd_dept'";
    $stmt1 = $dbh->prepare($sql1);
    $stmt1->execute();

    $posisi = [];
    while ($ro = $stmt1->fetch(PDO::FETCH_ASSOC)) {
      $posisi[] = $ro;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $result[] = $posisi;

  echo json_encode($result);
} else if ($request == "getPosisiBaru") {

  $kd_off = $data->kd_off;
  $kd_dept = $data->kd_dept;

  try {
    $sql1 = "SELECT DISTINCT(hrm_employee.kd_pos), hrm_position.id_pos, hrm_position.position
             FROM hrm_employee 
             JOIN hrm_position ON hrm_position.kd_pos = hrm_employee.kd_pos
             WHERE kd_dept = '$kd_dept'";
    $stmt1 = $dbh->prepare($sql1);
    $stmt1->execute();

    $posisi = [];
    while ($ro = $stmt1->fetch(PDO::FETCH_ASSOC)) {
      $posisi[] = $ro;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $result[] = $posisi;

  echo json_encode($result);
} else if ($request == "getPosisiParent") {

  $result = [];

  try {
    $sql1 = "SELECT * FROM hrm_position ORDER BY id_pos";
    $stmt1 = $dbh->prepare($sql1);
    $stmt1->execute();

    $posisi_parent = [];
    while ($ro = $stmt1->fetch(PDO::FETCH_ASSOC)) {
      $posisi_parent[] = $ro;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $result[] = $posisi_parent;

  echo json_encode($result);
} else if ($request == "getPosisiAll") {

  function createTreeView2($parent, $menu)
  {
    $html = '';
    if (isset($menu['parents'][$parent])) {
      $html .= "<ol class='tree'>";
      foreach ($menu['parents'][$parent] as $itemId) {
        if (!isset($menu['parents'][$itemId])) {
          $html .= "<li><label for='subfolder2'>";
          $html .= " <a @click='getDataKaryawanByKodePosition2(" . $menu['items'][$itemId]['id_post'] . ")'>" . $menu['items'][$itemId]['position_name'] . "</a>";
          $html .= " <a class='fa fa-trash-o' data-toggle='modal' data-target='#delete_post' @click='getDataPosition(" . $menu['items'][$itemId]['id_post'] . ")'></a>";
          $html .= "</label> <input type='checkbox' name='subfolder2'/></li>";
        }
        if (isset($menu['parents'][$itemId])) {
          $html .= "<li><label for='subfolder2'>";
          $html .= " <a @click='getDataKaryawanByKodePosition2(" . $menu['items'][$itemId]['id_post'] . ")'>" . $menu['items'][$itemId]['position_name'] . "</a>";
          $html .= " <a class='fa fa-trash-o' data-toggle='modal' data-target='#delete_post' @click='getDataPosition(" . $menu['items'][$itemId]['id_post'] . ")'></a>";
          $html .= "</label> <input type='checkbox' name='subfolder2'/> ";
          $html .= createTreeView2($itemId, $menu);
          $html .= "</li>";
        }
      }
      $html .= "</ol>";
    }
    return $html;
  }

  $menus2 = array(
    'items' => array(),
    'parents' => array()
  );

  $kd_office = $data->kd_office;

  try {
    $sql1 = "SELECT 
              hrm_apprasial_position_bawahan.id_pos AS id_post,
              hrm_apprasial_position_bawahan.kd_pos AS kd_post,
              hrm_position.position AS position_name,
              '0' AS kd_post_parent,
              hrm_apprasial_position_bawahan.kd_off
              FROM hrm_apprasial_position_bawahan
              JOIN hrm_position ON hrm_position.id_pos = hrm_apprasial_position_bawahan.id_pos
              WHERE hrm_apprasial_position_bawahan.kd_off = '$kd_office'
              AND hrm_apprasial_position_bawahan.deleted = '0'
              AND hrm_apprasial_position_bawahan.id_atasan = '0'
              UNION ALL
              SELECT 
              hrm_apprasial_position_bawahan.id_pos AS id_post,
              hrm_apprasial_position_bawahan.kd_pos AS kd_post, 
              hrm_position.position AS position_name,
              hrm_apprasial_position_atasan.kd_pos AS kd_post_parent,
              hrm_apprasial_position_bawahan.kd_off
              FROM hrm_apprasial_position_bawahan
              JOIN hrm_apprasial_position_atasan ON hrm_apprasial_position_atasan.id_atasan = hrm_apprasial_position_bawahan.id_atasan
              JOIN hrm_position ON hrm_position.id_pos = hrm_apprasial_position_bawahan.id_pos
              WHERE hrm_apprasial_position_atasan.kd_off = '$kd_office' 
              AND hrm_apprasial_position_bawahan.kd_off = '$kd_office'
              AND hrm_apprasial_position_bawahan.deleted = '0'";
    $stmt1 = $dbh->query($sql1);
    $result2 = $stmt1->fetchAll(PDO::FETCH_ASSOC);

    foreach ($result2 as $row2) {
      $menus2['items'][$row2['kd_post']] = $row2;
      $menus2['parents'][$row2['kd_post_parent']][] = $row2['kd_post'];
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  echo createTreeView2(0, $menus2);
} else if ($request == "saveMappingPosisi") {

  $kd_off = $data->kd_off;
  $id_dept = $data->id_dept;
  $kd_dept = $data->kd_dept;
  $id_pos = $data->id_pos;
  $kd_pos = $data->kd_pos;
  $id_pos_parent = $data->id_pos_parent;

  try {
    $sql = "INSERT INTO hrm_apprasial_position_bawahan(kd_off,id_dept,kd_dept,id_pos,kd_pos,deleted,id_atasan) 
            VALUES (:kd_off,:id_dept,:kd_dept,:id_post,:kd_post,'0',:id_atasan)";
    $stmt = $dbh->prepare($sql);

    $stmt->bindValue(':kd_off', $kd_off);
    $stmt->bindValue(':id_dept', $id_dept);
    $stmt->bindValue(':kd_dept', $kd_dept);
    $stmt->bindValue(':id_post', $id_pos);
    $stmt->bindValue(':kd_post', $kd_pos);
    $stmt->bindValue(':id_atasan', $id_pos_parent);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal insert Mapping Posisi bawahan';
  }

  // try {
  //   $sql = "INSERT INTO hrm_apprasial_position_atasan(kd_off,id_dept,kd_dept,id_pos,kd_pos,deleted) 
  //           VALUES (:kd_off,:id_dept,:kd_dept,:id_post,:kd_post,'0')";
  //   $stmt = $dbh->prepare($sql);

  //   $stmt->bindValue(':kd_off', $kd_off);
  //   $stmt->bindValue(':id_dept', $id_dept);
  //   $stmt->bindValue(':kd_dept', $kd_dept);
  //   $stmt->bindValue(':id_post', $id_pos);
  //   $stmt->bindValue(':kd_post', $kd_pos);

  //   $stmt->execute();

  // } catch (PDOException $e) {
  //   $e->getMessage();
  //   echo 'gagal insert Mapping Posisi Atasan'; 
  // }

} else if ($request == "getPosisiByKd") {

  $result = [];

  $kd_office = $data->kd_office;
  $kd_pos = $data->kd_pos;

  try {
    $sql1 = "SELECT COUNT(*) FROM hrm_apprasial_position_bawahan WHERE kd_off = '$kd_office' AND id_atasan = '$kd_pos'";
    $stmt1 = $dbh->prepare($sql1);
    $stmt1->execute();

    $count = [];
    while ($ro = $stmt1->fetch(PDO::FETCH_ASSOC)) {
      $count[] = $ro;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  try {
    $sql2 = "SELECT hrm_apprasial_position_bawahan.* , hrm_position.position
              FROM hrm_apprasial_position_bawahan
              JOIN hrm_position ON hrm_position.kd_pos = hrm_apprasial_position_bawahan.kd_pos
              WHERE hrm_apprasial_position_bawahan.kd_off = '$kd_office' 
              AND hrm_apprasial_position_bawahan.kd_pos = '$kd_pos'";
    $stmt2 = $dbh->prepare($sql2);
    $stmt2->execute();

    $posisi_parent = [];
    while ($ro2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
      $posisi_parent[] = $ro2;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $result[] = $count;
  $result[] = $posisi_parent;

  echo json_encode($result);
} else if ($request == "deleteMappingPosisi") {

  $kd_pos = $data->kd_pos;
  $kd_off = $data->kd_off;

  try {

    $sql_delete = "UPDATE hrm_apprasial_position_bawahan SET deleted = '1' WHERE kd_pos = '$kd_pos' AND kd_off = '$kd_off'";
    $stmt = $dbh->prepare($sql_delete);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Delete Position';
  }
} else if ($request == "getTabIndikatorPenilaian") {

  $result = [];

  try {
    $sql2 = "SELECT a.*, b.level, d.indikator 
              FROM hrm_apprasial_tab_penilaian a
              JOIN hrm_level b ON b.id_level = a.id_level
              JOIN hrm_apprasial_indikator_periode c ON c.kd_indikator = a.kd_indikator
              JOIN hrm_apprasial_indikator_kriteria d ON d.id_indikator_kriteria = a.id_indikator_kriteria
              ORDER BY a.tipe_penilai";
    $stmt2 = $dbh->prepare($sql2);
    $stmt2->execute();

    $tab_indikator = [];
    while ($ro2 = $stmt2->fetch(PDO::FETCH_ASSOC)) {
      $tab_indikator[] = $ro2;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $result[] = $tab_indikator;

  echo json_encode($result);
} else if ($request == "saveSettingIndikator") {

  $setting_baru = $data->setting_baru;
  $arr_setting_baru = json_decode($setting_baru, true);


  for ($i = 0; $i < count($arr_setting_baru); $i++) {
    // print_r($arr_setting_baru[$i]);
    for ($j = 0; $j <= count($arr_setting_baru[$i]['indikator']) - 1; $j++) {
      print_r($arr_setting_baru[$i]['indikator'][$j]);
    }
    // for($j = $i; $j <= count($arr_setting_baru[$i]['indikator']); $j++) {
    //   print_r($arr_setting_baru[$i]['indikator'])
    // }
  }
  try {
    $sql = "INSERT INTO hrm_apprasial_tab_penilaian(tipe_penilai,id_level,kd_indikator,id_indikator_kriteria) VALUES (:tipe_penilai,:id_level,:kd_indikator,:id_indikator_kriteria)";
    $stmt = $dbh->prepare($sql);

    for ($i = 0; $i < count($arr_setting_baru); $i++) {
      $stmt->bindValue(':tipe_penilai', $arr_setting_baru[$i]['tipe_penilai']);
      $stmt->bindValue(':id_level', $arr_setting_baru[$i]['level']);
      $stmt->bindValue(':kd_indikator', $arr_setting_baru[$i]['kd_indikator']);
      for ($j = 0; $j <= count($arr_setting_baru[$i]['indikator']) - 1; $j++) {
        // print_r($arr_setting_baru[$i]['indikator'][$j]);
        $stmt->bindValue(':id_indikator_kriteria', $arr_setting_baru[$i]['indikator'][$j]);
        $stmt->execute();
      }
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal insert Setting Indikator';
  }
} else if ($request == 'editSettingIndikator') {

  $result = [];

  $id_setting_indikator = $data->id_setting_indikator;

  try {

    $sql = "SELECT * FROM hrm_apprasial_tab_penilaian WHERE id_tab = '$id_setting_indikator'";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_setting_indikator = [];

    $arr_setting_indikator = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Indikator';
  }

  $result[] = $arr_setting_indikator;

  echo json_encode($result);
} else if ($request == 'updateSettingIndikator') {

  $tipe_penilai = $data->v_type_penilai;
  $id_level = $data->v_level;
  $kd_indikator = $data->v_nomor_penilaian;
  $id_indikator_kriteria = $data->v_id_indikator;
  $id_tab = $data->id_tab;

  try {

    $sql = "UPDATE hrm_apprasial_tab_penilaian SET tipe_penilai = '$tipe_penilai', id_level = '$id_level', kd_indikator = '$kd_indikator', id_indikator_kriteria = '$id_indikator_kriteria' WHERE id_tab = '$id_tab'";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Update Setting Indikator';
  }

  echo $sql;
} else if ($request == 'deleteSettingIndikator') {

  $id_tab = $data->id_tab;

  try {

    $sql = "DELETE FROM hrm_apprasial_tab_penilaian WHERE id_tab = '$id_tab'";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Delete Setting Indikator';
  }
} else if ($request == "saveTotalPenilaianBaru") {

  $result = [];

  $id_num_penilai = $data->id_num_penilai;
  $id_num_target = $data->id_num_target;
  $id_indikator = $data->id_indikator;
  $id_indikator_kriteria = $data->id_indikator_kriteria;
  $type_penilai = $data->type_penilai;
  $kd_indikator = $data->kd_indikator;

  try {

    $sql = "SELECT SUM(total_nilai) FROM hrm_apprasial_penilaian 
              WHERE id_num_target = '$id_num_target' 
              AND id_num_penilai = '$id_num_penilai' 
              AND id_indikator = '$id_indikator'
              AND id_indikator_kriteria = '$id_indikator_kriteria'";

    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $total_indikator = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Total Penilaian';
  }

  $total = $total_indikator['sum'];

  try {

    $sql = "INSERT INTO 
              hrm_apprasial_total_penilaian(id_num_penilai,id_num_target,id_indikator,id_indikator_kriteria,total_nilai_by_indikator) 
              VALUES 
              ('$id_num_penilai','$id_num_target','$id_indikator','$id_indikator_kriteria','$total')";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Total Penilaian';
  }

  try {

    $sql = "SELECT a.*, d.total_nilai_by_indikator
            FROM hrm_apprasial_indikator_kriteria a
            JOIN hrm_apprasial_indikator b ON a.id_indikator = b.id_indikator
            JOIN hrm_apprasial_tab_penilaian c ON a.id_indikator_kriteria = c.id_indikator_kriteria
            JOIN hrm_apprasial_total_penilaian d ON a.id_indikator_kriteria = d.id_indikator_kriteria
            WHERE b.kd_indikator = '$kd_indikator' 
            AND a.deleted = '0'
            AND c.tipe_penilai = '$type_penilai'
            AND d.id_num_target = '$id_num_target'
            AND d.id_num_penilai = '$id_num_penilai'
            AND d.id_indikator = '$id_indikator'
            ORDER BY a.id_indikator_kriteria ASC";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    while ($temp_indikator = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $indikator_total[] = $temp_indikator;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Total Penilaian';
  }

  $result[] = $indikator_total;

  echo json_encode($result);
} else if ($request == "saveGrandTotalPenilaianBaru") {

  $result = [];

  $id_num_penilai = $data->id_num_penilai;
  $id_num_target = $data->id_num_target;
  $id_indikator = $data->id_indikator;
  $id_indikator_kriteria = $data->id_indikator_kriteria;

  try {

    $sql = "SELECT SUM(total_nilai_by_indikator) FROM hrm_apprasial_total_penilaian 
              WHERE id_num_target = '$id_num_target' 
              AND id_num_penilai = '$id_num_penilai' 
              AND id_indikator = '$id_indikator'";

    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $grand_total_indikator = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Total Penilaian';
  }

  $grand_total = $grand_total_indikator['sum'];

  try {

    $sql = "INSERT INTO hrm_apprasial_grand_total_penilaian(id_num_penilai,id_num_target,id_indikator,grand_total_nilai_by_indikator)
              VALUES ('$id_num_penilai','$id_num_target','$id_indikator','$grand_total')";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Total Penilaian';
  }

  $result[] = $grand_total;

  echo json_encode($result);
} else if ($request == "savePenilaiAtasanBawahan") {

  $id_target = $data->id_target;

  $create = $data->penilaiBaru;
  $penilaiBaru = json_decode($create, true);

  try {
    $sql = "INSERT INTO hrm_apprasial_penilai(id_num_penilai,id_num_target,status_penilai,type_penilai) VALUES (:id_penilai,:id_target,'1',:type_penilai)";
    $stmt = $dbh->prepare($sql);

    for ($i = 0; $i < count($penilaiBaru); $i++) {

      $id_penilai = $penilaiBaru[$i]['karyawan'];

      $stmt->bindValue(':id_penilai', $id_penilai);
      $stmt->bindValue(':id_target', $id_target);
      $stmt->bindValue(':type_penilai', $penilaiBaru[$i]['tipe_penilai']);
      $stmt->execute();
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal insert Penilai';
  }
} else if ($request == "getAllPertanyaan") {

  $result = [];
  $max_urutan = [];

  $kd_indikator = $data->kd_indikator;
  $type_penilai = $data->type_penilai;
  $id_level = $data->id_level;

  $max_urutan = [];
  $xarr_indikator = [];
  $arr = [];
  $arr_sub = [];
  $all_arr = [];

  $indikator = [];
  $id_indikator = '';

  try {

    $sql = "SELECT a.*
            FROM hrm_apprasial_indikator_kriteria a
            JOIN hrm_apprasial_indikator b ON a.id_indikator = b.id_indikator and tipe_indikator = 'pk'
            JOIN hrm_apprasial_tab_penilaian c ON a.id_indikator_kriteria = c.id_indikator_kriteria
            WHERE c.kd_indikator = '$kd_indikator' 
            AND a.deleted = '0'
            AND c.tipe_penilai = '$type_penilai'
            AND c.id_level = '$id_level'
            ORDER BY a.id_indikator_kriteria ASC";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $arr_indikator = [];

    while ($row_indikator = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $arr_indikator[] = $row_indikator;
      // $arr_indikator[] = array($row_indikator);
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Delete Penilai';
  }

  // print_r($arr);

  try {

    for ($i = 0; $i < count($arr_indikator); $i++) {
      $xarr_indikator = $arr_indikator[$i]['id_indikator_kriteria'];

      for ($k = 0; $k < $arr_indikator[$i]['max_urutan']; $k++) {
        $urutan = $k + 1;

        $sql = "SELECT a.*, b.indikator, b.max_urutan FROM hrm_apprasial_sub_indikator a
                JOIN hrm_apprasial_indikator_kriteria b ON a.id_indikator_kriteria = b.id_indikator_kriteria
                WHERE a.id_indikator_kriteria = '$xarr_indikator' 
                AND a.urutan = '$urutan' 
                AND a.status_sub_indikator = '1' 
                ORDER BY random() LIMIT 1";
        $stmt = $dbh->prepare($sql);

        $stmt->execute();

        while ($row_sub = $stmt->fetch(PDO::FETCH_ASSOC)) {
          $arr[] = $row_sub;
        }
      }
      // $arr_sub[] = array(
      //   "id_indikator_kriteria" => $arr_indikator[$i]['id_indikator_kriteria'],
      //   "indikator" => $arr_indikator[$i]['indikator'],
      //   "pertanyaan" => $arr
      // );
      // $arr = [];
      $indikator[] = array(
        "id_indikator" => $arr_indikator[$i]['id_indikator'],
        "id_indikator_kriteria" => $arr_indikator[$i]['id_indikator_kriteria']
      );
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Delete Sub Indikator';
  }

  $result[] = $arr;
  $result[] = $indikator;
  $result[] = $type_penilai;

  echo json_encode($result);
} else if ($request == "savePenilaianNew") {

  $result = [];
  $arr_total = [];

  $id_num_penilai = $data->id_num_penilai;
  $id_num_target = $data->id_num_target;
  $type_penilai = $data->type_penilai;
  $periode = $data->periode;

  $penilaian = $data->penilaian;
  $arr_penilaian = json_decode($penilaian, true);

  $detail_indikator = $data->detail_indikator;
  $arr_detail_indikator = json_decode($detail_indikator, true);

  echo json_encode($arr_penilaian);

  try {

    $sql = "INSERT INTO 
              hrm_apprasial_penilaian(id_num_penilai,
              id_num_target,
              id_indikator,
              id_sub_indikator,
              bobot,
              nilai,
              total_nilai,
              id_indikator_kriteria,
              tipe_penilai,
              periode) 
            VALUES 
              (:id_num_penilai,
                :id_num_target,
                :id_indikator,
                :id_sub_indikator,
                :bobot,
                :nilai,
                :total_nilai,
                :id_indikator_kriteria,
                :type_penilai,
                :periode)";

    $stmt = $dbh->prepare($sql);

    for ($i = 0; $i < count($arr_penilaian); $i++) {
      $stmt->bindValue(':id_num_penilai', $id_num_penilai);
      $stmt->bindValue(':id_num_target', $id_num_target);
      $stmt->bindValue(':id_indikator', $arr_penilaian[$i]['id_indikator']);
      $stmt->bindValue(':id_sub_indikator', $arr_penilaian[$i]['id_sub_indikator']);
      $stmt->bindValue(':bobot', $arr_penilaian[$i]['bobot']);
      $stmt->bindValue(':nilai', $arr_penilaian[$i]['nilai']);
      $stmt->bindValue(':total_nilai', $arr_penilaian[$i]['total']);
      $stmt->bindValue(':id_indikator_kriteria', $arr_penilaian[$i]['id_indikator_kriteria']);
      $stmt->bindValue(':type_penilai', $type_penilai);
      $stmt->bindValue(':periode', $periode);
      $stmt->execute();
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Total Penilaian';
  }

  try {

    for ($i = 0; $i < count($arr_detail_indikator); $i++) {

      $xid_indikator = $arr_detail_indikator[$i]['id_indikator'];
      $xid_indikator_kriteria = $arr_detail_indikator[$i]['id_indikator_kriteria'];


      $sql = "SELECT SUM(total_nilai) FROM hrm_apprasial_penilaian 
              WHERE id_num_target = '$id_num_target' 
              AND id_num_penilai = '$id_num_penilai' 
              AND id_indikator = '$xid_indikator'
              AND id_indikator_kriteria = '$xid_indikator_kriteria'
              AND tipe_penilai = '$type_penilai'
              AND periode = '$periode'";

      $stmt = $dbh->prepare($sql);
      $stmt->execute();

      $arr_total = $stmt->fetch(PDO::FETCH_ASSOC);
      $xtotal_nilai = $arr_total['sum'];

      try {

        $sql = "INSERT INTO 
                  hrm_apprasial_total_penilaian(id_num_penilai,id_num_target,id_indikator,id_indikator_kriteria,total_nilai_by_indikator,tipe_penilai,periode) 
                  VALUES 
                  ('$id_num_penilai','$id_num_target','$xid_indikator','$xid_indikator_kriteria','$xtotal_nilai','$type_penilai','$periode')";

        $stmt = $dbh->prepare($sql);
        $stmt->execute();
      } catch (PDOException $e) {
        $e->getMessage();
        echo 'gagal Save Total Penilaian';
      }

      $id_indikator = $arr_detail_indikator[0]['id_indikator'];
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Total Penilaian';
  }

  try {

    $sql = "SELECT SUM(total_nilai_by_indikator) FROM hrm_apprasial_total_penilaian 
              WHERE id_num_target = '$id_num_target' 
              AND id_num_penilai = '$id_num_penilai' 
              AND id_indikator = '$id_indikator'
              AND tipe_penilai = '$type_penilai'
              AND periode = '$periode'";

    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $grand_total_indikator = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Total Penilaian';
  }

  $xgrand_total_indikator = $grand_total_indikator['sum'];

  try {

    $sql = "INSERT INTO hrm_apprasial_grand_total_penilaian(id_num_penilai,id_num_target,id_indikator,grand_total_nilai_by_indikator,tipe_penilai,periode)
              VALUES ('$id_num_penilai','$id_num_target','$id_indikator','$xgrand_total_indikator','$type_penilai','$periode')";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Total Penilaian';
  }

  // $result[] = $grand_total;

  // echo json_encode($result);

} else if ($request == "laporanGrandTotalPenilaian") {

  $result = [];

  $id_num_target = $data->id_num_target;
  $periode = $data->periode;

  $arr_indikator = [];
  $arr_grand_total = [];
  $arr_total_nilai = [];
  $arr = [];

  $arr_total = [];
  $arr_nilai = [];

  $total_atasan = 0;

  $total_kompetensi = 0;
  $total_karakter = 0;
  $total_kontribusi = 0;
  $total_komitmen = 0;

  $nilai_kompetensi = 0;
  $nilai_karakter = 0;
  $nilai_kontribusi = 0;
  $nilai_komitmen = 0;

  $jumlah_komitmen = 0;

  $grand_total_nilai = 0;

  $xhasil = [];
  $grand_total = [];

  $total_penilai = 0;

  try {

    // $sql = "SELECT c.office,
    //           d.department,
    //           b.emp_name, 
    //           e.position,
    //           a.tipe_penilai AS type_penilai,
    //           a.grand_total_nilai_by_indikator,
    //           a.id_num_penilai,
    //           f.id_indikator
    //           FROM hrm_apprasial_grand_total_penilaian a
    //           JOIN hrm_employee b ON a.id_num_penilai = b.id_num
    //           JOIN hrm_office c ON b.kd_off = c.kd_off
    //           JOIN hrm_department d ON b.kd_dept = d.kd_dept
    //           JOIN hrm_position e ON b.kd_pos = e.kd_pos
    //           JOIN hrm_apprasial_indikator f ON a.id_indikator = f.id_indikator
    //           WHERE a.id_num_target = '$id_num_target'";

    // $sql = "SELECT c.office,
    //             d.department,
    //             lower(a.emp_name) as emp_name,
    //             e.position,
    //             hrm_apprasial_penilai.type_penilai,
    //             hrm_apprasial_penilai.id_num_penilai,
    //             (SELECT DISTINCT(id_indikator) FROM hrm_apprasial_grand_total_penilaian WHERE id_num_target = '$id_num_target') AS id_indikator,
    //             (SELECT grand_total_nilai_by_indikator FROM hrm_apprasial_grand_total_penilaian WHERE id_num_target = '$id_num_target' AND id_num_penilai = hrm_apprasial_penilai.id_num_penilai AND tipe_penilai != 'HRD') AS grand_total_nilai_by_indikator
    //         FROM hrm_apprasial_penilai 
    //         JOIN hrm_employee a ON a.id_num = hrm_apprasial_penilai.id_num_penilai
    //         JOIN hrm_level b ON b.kd_level = a.kd_level
    //         JOIN hrm_office c ON c.kd_off = a.kd_off
    //         JOIN hrm_department d ON d.kd_dept = a.kd_dept
    //         JOIN hrm_position e ON e.kd_pos = a.kd_pos
    //         WHERE hrm_apprasial_penilai.id_num_target = '$id_num_target'
    //         ORDER BY hrm_apprasial_penilai.type_penilai ASC";

    $sql = "SELECT * FROM (SELECT c.office,
              d.department,
              lower(a.emp_name) as emp_name,
              e.position,
              hrm_apprasial_penilai.type_penilai,
              hrm_apprasial_penilai.id_num_penilai,
              (SELECT DISTINCT(a.id_indikator)
              FROM hrm_apprasial_indikator_kriteria a
              JOIN hrm_apprasial_tab_penilaian c ON a.id_indikator_kriteria = c.id_indikator_kriteria
              WHERE a.deleted = '0'
              AND c.tipe_penilai = hrm_apprasial_penilai.type_penilai
              AND c.id_level = (SELECT id_level FROM hrm_employee a LEFT JOIN hrm_level b ON a.kd_level = b.kd_level WHERE a.id_num = '$id_num_target')) AS id_indikator,
              (SELECT grand_total_nilai_by_indikator FROM hrm_apprasial_grand_total_penilaian WHERE id_num_target = '$id_num_target' AND id_num_penilai = hrm_apprasial_penilai.id_num_penilai AND tipe_penilai != 'HRD' AND periode = '$periode' LIMIT 1) AS grand_total_nilai_by_indikator
              FROM hrm_apprasial_penilai 
              JOIN hrm_employee a ON a.id_num = hrm_apprasial_penilai.id_num_penilai
              JOIN hrm_level b ON b.kd_level = a.kd_level
              JOIN hrm_office c ON c.kd_off = a.kd_off
              JOIN hrm_department d ON d.kd_dept = a.kd_dept
              JOIN hrm_position e ON e.kd_pos = a.kd_pos
              WHERE hrm_apprasial_penilai.id_num_target = '$id_num_target'
              ORDER BY hrm_apprasial_penilai.type_penilai) AS penilai
              UNION ALL
              SELECT c.office,
              d.department,
              b.emp_name, 
              e.position,
              a.tipe_penilai AS type_penilai,
              a.id_num_penilai,
              f.id_indikator,
              a.grand_total_nilai_by_indikator
              FROM hrm_apprasial_grand_total_penilaian a
              JOIN hrm_employee b ON a.id_num_penilai = b.id_num
              JOIN hrm_office c ON b.kd_off = c.kd_off
              JOIN hrm_department d ON b.kd_dept = d.kd_dept
              JOIN hrm_position e ON b.kd_pos = e.kd_pos
              JOIN hrm_apprasial_indikator f ON a.id_indikator = f.id_indikator
              WHERE a.id_num_target = '$id_num_target' AND a.tipe_penilai = 'HRD' AND periode = '$periode'";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    while ($row_grand_total = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $arr_grand_total[] = $row_grand_total;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Total Penilaian';
  }

  for ($i = 0; $i < count($arr_grand_total); $i++) {
    $id_indiaktor = $arr_grand_total[$i]['id_indikator'];
    $id_num_penilai = $arr_grand_total[$i]['id_num_penilai'];
    $type_penilai = $arr_grand_total[$i]['type_penilai'];

    try {
      $sql = "SELECT a.indikator, a.id_indikator_kriteria,
              (SELECT tipe_penilai FROM hrm_apprasial_total_penilaian
                WHERE id_indikator = '$id_indiaktor'
                AND id_num_target = '$id_num_target'
                AND id_num_penilai = '$id_num_penilai'
                AND periode = '$periode'
                AND id_indikator_kriteria = a.id_indikator_kriteria) AS type_penilai,
              (SELECT total_nilai_by_indikator FROM hrm_apprasial_total_penilaian
                WHERE id_indikator = '$id_indiaktor'
                AND id_num_target = '$id_num_target'
                AND id_num_penilai = '$id_num_penilai'
                AND periode = '$periode'
                AND tipe_penilai = '$type_penilai'
                AND id_indikator_kriteria = a.id_indikator_kriteria) AS total_nilai
              FROM hrm_apprasial_indikator_kriteria a
              JOIN hrm_apprasial_indikator b ON a.id_indikator = b.id_indikator
              WHERE a.id_indikator = '$id_indiaktor' 
              AND a.status_kriteria = '1' 
              AND a.deleted = '0'
              ORDER BY a.indikator";
      $stmt = $dbh->prepare($sql);

      $stmt->execute();

      while ($row_sub = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $arr_indikator[] = $row_sub;
      }
    } catch (PDOException $e) {
      $e->getMessage();
      echo 'gagal Delete Sub Indikator' . $e . "/n" . $sql;
    }

    $hasil[] = array(
      "office" => $arr_grand_total[$i]['office'],
      "department" => $arr_grand_total[$i]['department'],
      "emp_name" => $arr_grand_total[$i]['emp_name'],
      "position" => $arr_grand_total[$i]['position'],
      "type_penilai" => $arr_grand_total[$i]['type_penilai'],
      "grand_total_nilai_by_indikator" => $arr_grand_total[$i]['grand_total_nilai_by_indikator'],
      "id_num_penilai" => $arr_grand_total[$i]['id_num_penilai'],
      "id_indikator" => $arr_grand_total[$i]['id_indikator'],
      "indikator" => $arr_indikator
    );
    $arr_indikator = [];
  }

  // $total_penilai = count($hasil);


  for ($i = 0; $i < count($hasil); $i++) {

    if ($hasil[$i]['type_penilai'] == 'Atasan') {
      $total_atasan += count($hasil[$i]['type_penilai']);
    }

    if ($hasil[$i]['type_penilai'] != 'HRD') {
      $total_penilai += count($hasil[$i]['type_penilai']);
    }

    for ($j = 0; $j < count($hasil[$i]['indikator']); $j++) {

      if ($hasil[$i]['indikator'][$j]['indikator'] == 'Kompetensi') {
        $total_kompetensi += $hasil[$i]['indikator'][$j]['total_nilai'];
      }

      if ($hasil[$i]['indikator'][$j]['indikator'] == 'Karakter') {
        $total_karakter += $hasil[$i]['indikator'][$j]['total_nilai'];
      }

      if ($hasil[$i]['indikator'][$j]['indikator'] == 'Kontribusi') {
        $total_kontribusi += $hasil[$i]['indikator'][$j]['total_nilai'];
      }

      if ($hasil[$i]['indikator'][$j]['indikator'] == 'Komitmen') {
        $jumlah_komitmen += count($hasil[$i]['indikator'][$j]['total_nilai']);
      }

      if ($hasil[$i]['indikator'][$j]['indikator'] == 'Komitmen') {
        $total_komitmen += $hasil[$i]['indikator'][$j]['total_nilai'];
      }
    }
  }

  $nilai_karakter = $total_karakter / $total_penilai;
  $nilai_kompetensi = $total_kompetensi / $total_penilai;

  if ($total_atasan > 0) {
    $nilai_kontribusi = $total_kontribusi / $total_atasan;
  } else {
    $nilai_kontribusi = 0;
  }

  if ($nilai_komitmen > 0) {
    $nilai_komitmen = $total_komitmen / $jumlah_komitmen;
  } else {
    $nilai_komitmen = 0;
  }

  $grand_total_nilai = round($nilai_karakter) + round($nilai_kompetensi) + round($nilai_kontribusi) + round($total_komitmen);

  $arr_total = array(
    "total_karakter" => $total_karakter,
    "total_kompetensi" => $total_kompetensi,
    "total_kontribusi" => $total_kontribusi,
    "total_komitmen" => $total_komitmen
  );

  $arr_nilai = array(
    "nilai_kompetensi" => round($nilai_kompetensi),
    "nilai_karakter" => round($nilai_karakter),
    "nilai_kontribusi" => round($nilai_kontribusi),
    "nilai_komitmen" => round($total_komitmen)
  );

  $nilai = round($grand_total_nilai);
  $keterangan = '';
  $xketerangan = '';
  $warna = '';

  switch ($nilai) {
    case in_array($nilai, range(0, 300)):
      $keterangan = "D";
      $warna = 'red';
      break;
    case in_array($nilai, range(301, 320)):
      $keterangan = "C-";
      $warna = 'yellow';
      break;
    case in_array($nilai, range(321, 345)):
      $keterangan = "C+";
      $warna = '#ffff66';
      break;
    case in_array($nilai, range(346, 365)):
      $keterangan = "B-";
      $warna = 'green';
      break;
    case in_array($nilai, range(366, 385)):
      $keterangan = "B+";
      $warna = 'purple';
      break;
    case in_array($nilai, range(386, 435)):
      $keterangan = "A";
      $warna = 'aqua';
      break;
    case in_array($nilai, range(436, 500)):
      $keterangan = "A+";
      $warna = 'blue';
      break;
    default:
      $keterangan = "";
      $warna = "";
  }

  try {
    // $sql = "SELECT (SELECT COUNT(DISTINCT(id_num_penilai)) FROM hrm_apprasial_penilaian WHERE id_num_target = '$id_num_target' AND tipe_penilai != 'HRD') AS sudah_nilai,
    //           (SELECT COUNT(id_num_penilai) FROM hrm_apprasial_penilai WHERE id_num_target = '$id_num_target') AS total_penilai
    //           FROM hrm_employee
    //           WHERE id_num = '$id_num_target'";

    $sql = "SELECT (SELECT COUNT(DISTINCT(id_num_penilai)) FROM hrm_apprasial_penilaian WHERE id_num_target = '$id_num_target' AND tipe_penilai != 'HRD' AND periode = '$periode' AND id_num_penilai IN (SELECT id_num_penilai FROM hrm_apprasial_penilai WHERE id_num_target = '$id_num_target')) AS sudah_nilai,
                   (SELECT COUNT(id_num_penilai) FROM hrm_apprasial_penilai WHERE id_num_target = '$id_num_target') AS total_penilai
            FROM hrm_employee
            WHERE id_num = '$id_num_target'";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $employee = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  if ($employee['sudah_nilai'] == $employee['total_penilai']) {
    $xketerangan = $keterangan;
  } else {
    $xketerangan = '-';
  }

  $grand_total = array(
    "total" => $arr_total,
    "nilai" => $arr_nilai,
    "total_nilai" => $nilai,
    "keterangan" => $xketerangan,
    "warna" => $warna
  );

  $result[] = $hasil;
  $result[] = $grand_total;

  echo json_encode($result);
} else if ($request == "laporanTotalPnilaian") {

  $result = [];

  $id_num_target = $data->id_num_target;

  try {

    $sql = "SELECT indikator FROM hrm_apprasial_indikator_kriteria 
            WHERE id_indikator = '33'
            AND status_kriteria = '1'
            AND deleted = '0'";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $arr_grand_total = [];

    while ($row_grand_total = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $arr_grand_total[] = $row_grand_total;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Total Penilaian';
  }

  try {

    $sql = "SELECT a.indikator, b.total_nilai_by_indikator
            FROM hrm_apprasial_indikator_kriteria a
            JOIN hrm_apprasial_total_penilaian b ON a.id_indikator = b.id_indikator AND a.id_indikator_kriteria = b.id_indikator_kriteria
            WHERE a.id_indikator = '33' 
            AND a.status_kriteria = '1' 
            AND a.deleted = '0'
            AND b.id_num_target = '$id_num_target'";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $arr_total_nilai = [];

    while ($row_total_nilai = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $arr_total_nilai[] = $row_total_nilai;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Total Penilaian';
  }

  $result[] = $arr_grand_total;
  $result[] = $arr_total_nilai;

  echo json_encode($result);
} else if ($request == "savePenilaianNewHRD") {

  $result = [];
  $arr_total = [];

  $id_num_penilai = $data->id_num_penilai;
  $id_num_target = $data->id_num_target;
  $type_penilai = $data->type_penilai;
  $periode = $data->periode;

  $penilaian = $data->penilaian;
  $arr_penilaian = json_decode($penilaian, true);

  $detail_indikator = $data->detail_indikator;
  $arr_detail_indikator = json_decode($detail_indikator, true);

  try {

    $sql = "INSERT INTO 
              hrm_apprasial_penilaian(id_num_penilai,
              id_num_target,
              id_indikator,
              id_sub_indikator,
              bobot,
              nilai,
              total_nilai,
              id_indikator_kriteria,
              tipe_penilai,
              periode) 
            VALUES 
              (:id_num_penilai,
                :id_num_target,
                :id_indikator,
                :id_sub_indikator,
                :bobot,
                :nilai,
                :total_nilai,
                :id_indikator_kriteria,
                :type_penilai,
                :periode)";

    $stmt = $dbh->prepare($sql);

    for ($i = 0; $i < count($arr_penilaian); $i++) {
      $stmt->bindValue(':id_num_penilai', $id_num_penilai);
      $stmt->bindValue(':id_num_target', $id_num_target);
      $stmt->bindValue(':id_indikator', $arr_penilaian[$i]['id_indikator']);
      $stmt->bindValue(':id_sub_indikator', $arr_penilaian[$i]['id_sub_indikator']);
      $stmt->bindValue(':bobot', $arr_penilaian[$i]['bobot']);
      $stmt->bindValue(':nilai', $arr_penilaian[$i]['nilai']);
      $stmt->bindValue(':total_nilai', $arr_penilaian[$i]['total']);
      $stmt->bindValue(':id_indikator_kriteria', $arr_penilaian[$i]['id_indikator_kriteria']);
      $stmt->bindValue(':type_penilai', $type_penilai);
      $stmt->bindValue(':periode', $periode);
      $stmt->execute();
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Total Penilaian';
  }

  try {

    for ($i = 0; $i < count($arr_detail_indikator); $i++) {

      $xid_indikator = $arr_detail_indikator[$i]['id_indikator'];
      $xid_indikator_kriteria = $arr_detail_indikator[$i]['id_indikator_kriteria'];


      $sql = "SELECT SUM(total_nilai) FROM hrm_apprasial_penilaian 
              WHERE id_num_target = '$id_num_target' 
              AND id_num_penilai = '$id_num_penilai' 
              AND id_indikator = '$xid_indikator'
              AND id_indikator_kriteria = '$xid_indikator_kriteria'
              AND tipe_penilai = '$type_penilai'
              AND periode = '$periode'";

      $stmt = $dbh->prepare($sql);
      $stmt->execute();

      $arr_total = $stmt->fetch(PDO::FETCH_ASSOC);
      $xtotal_nilai = $arr_total['sum'];

      try {

        $sql = "INSERT INTO 
                  hrm_apprasial_total_penilaian(id_num_penilai,id_num_target,id_indikator,id_indikator_kriteria,total_nilai_by_indikator,tipe_penilai,periode) 
                  VALUES 
                  ('$id_num_penilai','$id_num_target','$xid_indikator','$xid_indikator_kriteria','$xtotal_nilai','$type_penilai','$periode')";

        $stmt = $dbh->prepare($sql);
        $stmt->execute();
      } catch (PDOException $e) {
        $e->getMessage();
        echo 'gagal Save Total Penilaian';
      }

      $id_indikator = $arr_detail_indikator[0]['id_indikator'];
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Total Penilaian';
  }

  try {

    $sql = "SELECT SUM(total_nilai_by_indikator) FROM hrm_apprasial_total_penilaian 
              WHERE id_num_target = '$id_num_target' 
              AND id_num_penilai = '$id_num_penilai' 
              AND id_indikator = '$id_indikator'
              AND tipe_penilai = '$type_penilai'
              AND periode = '$periode'";

    $stmt = $dbh->prepare($sql);

    $stmt->execute();

    $grand_total_indikator = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Total Penilaian';
  }

  $xgrand_total_indikator = $grand_total_indikator['sum'];

  try {

    $sql = "INSERT INTO hrm_apprasial_grand_total_penilaian(id_num_penilai,id_num_target,id_indikator,grand_total_nilai_by_indikator,tipe_penilai,periode)
            VALUES ('$id_num_penilai','$id_num_target','$id_indikator','$xgrand_total_indikator','$type_penilai','$periode')";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Show Total Penilaian';
  }

  // $result[] = $grand_total;

  // echo json_encode($result);

} else if ($request == "getlistemployeePenilaianNew") {

  $result = [];
  $periode = $data->periode;

  try {
    $sql = "SELECT a.id_num, b.id_level, g.position, a.nik, a.att_name, lower(a.emp_name) as emp_name, to_char(a.join_date, 'DD Mon YYYY') as join_date, lower(a.emp_sts) as emp_sts, a.emp_sts_notes, a.kd_level, a.kd_off, a.kd_dept, b.level, a.path_photo, d.division, e.department, f.office,
            (SELECT grand_total_nilai_by_indikator FROM hrm_apprasial_grand_total_penilaian WHERE id_num_target = a.id_num AND tipe_penilai = 'HRD' AND periode = '$periode' LIMIT 1) AS total_penilaian_hrd,
            CONCAT((SELECT COUNT(DISTINCT(id_num_penilai)) FROM hrm_apprasial_penilaian WHERE id_num_target = a.id_num AND tipe_penilai != 'HRD' AND periode = '$periode' AND id_num_penilai IN (SELECT id_num_penilai FROM hrm_apprasial_penilai WHERE id_num_target = a.id_num)), '/', (SELECT COUNT(id_num_penilai) FROM hrm_apprasial_penilai WHERE id_num_target = a.id_num)) AS total_penilai,
            (SELECT COUNT(DISTINCT(id_num_penilai)) FROM hrm_apprasial_penilaian WHERE id_num_target = a.id_num AND tipe_penilai = 'HRD' AND periode = '$periode') AS total_penilai_hrd
            FROM hrm_employee a  
            LEFT JOIN hrm_level b ON a.kd_level = b.kd_level
            LEFT JOIN hrm_division d ON a.kd_div = d.kd_div
            LEFT JOIN hrm_department e ON a.kd_dept = e.kd_dept
            LEFT JOIN hrm_office f ON a.kd_off = f.kd_off
            LEFT JOIN hrm_position g ON a.kd_pos = g.kd_pos 
            WHERE a.resign_date IS NULL
            ORDER BY a.kd_off";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $employee = [];
    while ($d = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $employee[] = $d;
    };
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $result[] = $employee;

  echo json_encode($result);

} else if ($request == "deletelaporanTotalPnilaian") {

  $id_num_penilai = $data->id_num_penilai;
  $id_num_target = $data->id_num_target;
  $id_indikator = $data->id_indikator;
  $tipe_penilai = $data->tipe_penilai;

  try {

    $sql = "DELETE FROM hrm_apprasial_penilaian WHERE id_num_target = '$id_num_target' AND id_num_penilai = '$id_num_penilai' AND id_indikator = '$id_indikator' AND tipe_penilai = '$tipe_penilai'";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo $e;
  }

  try {

    $sql = "DELETE FROM hrm_apprasial_total_penilaian WHERE id_num_target = '$id_num_target' AND id_num_penilai = '$id_num_penilai' AND id_indikator = '$id_indikator' AND tipe_penilai = '$tipe_penilai'";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo $e;
  }

  try {

    $sql = "DELETE FROM hrm_apprasial_grand_total_penilaian WHERE id_num_target = '$id_num_target' AND id_num_penilai = '$id_num_penilai' AND id_indikator = '$id_indikator' AND tipe_penilai = '$tipe_penilai'";
    $stmt = $dbh->prepare($sql);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo $e;
  }
} else if ($request == "exportExcelPasanganPenilai") {

  // $id_num_target = $data->id_num;

  $tgl_now = date("Y-m-d");
  //$thn_now = date("Y");
  $thn_now  = date("Y");
  $random   = rand();
  $filename = 'laporanPasanganPenilaiHRD' . $random . date('d-m-Y') . date('H:i:sa') . '[' . $_SESSION['username'] . $random . '].xls';

  header("Content-type: application/octet-stream");
  header("Content-Disposition: attachment; filename=$filename"); //ganti nama sesuai keperluan
  header("Pragma: no-cache");
  header("Expires: 0");

  require_once '../../lib/PHPExcel.php';

  $styleThinBlackBorderOutline = array(
    'borders' => array(
      'outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN,
        'color' => array('argb' => 'FF000000'),
      ),
    ),
  );

  $styleHeader = array(
    'font'      => array(
      'bold' => true,
    ),
    'fill'      => array(
      'type'  => PHPExcel_Style_Fill::FILL_SOLID,
      'color' => array('argb' => '00CCCCCC'),
    ),
    'alignment' => array(
      'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
  );

  $styleBody = array(
    'font'      => array(
      'bold' => true,
    ),
    'alignment' => array(
      'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
  );

  $styleTotal = array(
    'font' => array(
      'bold' => true,
    ),
    'fill' => array(
      'type'  => PHPExcel_Style_Fill::FILL_SOLID,
      'color' => array('rgb' => 'e8e8e8'),
    ),
  );

  function changeColor($param)
  {
    $style = array(
      'fill' => array(
        'type'  => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('argb' => $param),
      ),
    );

    return $style;
  }

  $white       = changeColor('FFFFFF');
  $yellow      = changeColor('FFFF99');
  $purple      = changeColor('EBB5F7');
  $orangelight = changeColor('FFC107');
  $brown       = changeColor('DDDDDD');
  $aqua        = changeColor('00D9D9');
  $red         = changeColor('FF5465');
  $green       = changeColor('C8F983');
  $greenLight  = changeColor('33FF77');
  $nila        = changeColor('97F4AA');
  $orange      = changeColor('00FF8E8E');

  $objPHPExcel = new PHPExcel();

  $objPHPExcel->getProperties()->setCreator($_SESSION['username'] . $random)
    ->setLastModifiedBy($_SESSION['username'])
    ->setTitle("Laporan Pasangan Penilai")
    ->setSubject("Laporan Pasangan Penilai")
    ->setDescription("Laporan Pasangan Penilai")
    ->setKeywords("Laporan Pasangan Penilai")
    ->setCategory("HUMAN RESOURCE");

  $sheet = $objPHPExcel->getActiveSheet();
  $sheet->setTitle('Pasangan Penilai' . $random);

  $sheet->getHeaderFooter()->setOddHeader('&L' . 'Pasangan Penilai' . $random . '&RPrinted on &D');
  $sheet->getHeaderFooter()->setOddFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');

  $sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
  $sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

  $sheet->setCellValue('C1', 'PT. SAKA AGUNG ABADI');
  $sheet->setCellValue('C2', 'Laporan Pasangan Penilai');
  $sheet->setCellValue('C4', "Tanggal " . date('m-d-Y'));
  $sheet->getStyle('C1:C4')->getFont()->setBold(true);
  $sheet->getStyle('C1')->getFont()->setSize(18);

  $objDrawing = new PHPExcel_Worksheet_Drawing();
  $objDrawing->setName('logo');
  $objDrawing->setDescription('logo');
  $objDrawing->setPath('../../img/Logo_SAA.png');
  $objDrawing->setWidth(40);
  $objDrawing->setResizeProportional(false);
  $objDrawing->setCoordinates('B1');
  $objDrawing->setOffsetX(5);
  $objDrawing->setOffsetY(5);
  $objDrawing->setRotation(0);
  $objDrawing->setHeight(45);
  $objDrawing->setWorksheet($sheet);

  $i = 5;

  $sheet->setCellValue('A' . $i, 'Lokasi');
  $sheet->setCellValue('B' . $i, 'Nama Karyawan');
  $sheet->setCellValue('C' . $i, 'NIK');
  $sheet->setCellValue('D' . $i, 'Department');
  $sheet->setCellValue('E' . $i, 'Position');
  $sheet->setCellValue('F' . $i, 'Level Penilai');
  $sheet->setCellValue('G' . $i, 'Tipe Penilai');
  $sheet->setCellValue('H' . $i, 'Join Date');

  $sheet->getStyle('A' . $i . ':H' . $i)->applyFromArray($styleHeader);

  $sheet->getColumnDimension('A')->setWidth(3);
  $sheet->getColumnDimension('B')->setWidth(5);
  $sheet->getColumnDimension('C')->setWidth(10);
  $sheet->getColumnDimension('D')->setWidth(10);
  $sheet->getColumnDimension('E')->setWidth(10);
  $sheet->getColumnDimension('F')->setWidth(15);
  $sheet->getColumnDimension('G')->setWidth(10);
  $sheet->getColumnDimension('H')->setWidth(40);

  $objPHPExcel->setActiveSheetIndex(0);

  require_once '../../lib/PHPExcel/IOFactory.php';

  $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
  $objWriter->setPreCalculateFormulas();
  $objWriter->save($filename);
  echo json_encode($filename);
} else if ($request == "getJumlahPenilai") {

  $result = [];

  $id_num_target = $data->id_num_target;

  try {
    $sql = "SELECT (SELECT COUNT(DISTINCT(id_num_penilai)) FROM hrm_apprasial_penilaian WHERE id_num_target = '$id_num_target' AND tipe_penilai != 'HRD' AND id_num_penilai IN (SELECT id_num_penilai FROM hrm_apprasial_penilai WHERE id_num_target = '$id_num_target')) AS sudah_nilai,
                   (SELECT COUNT(id_num_penilai) FROM hrm_apprasial_penilai WHERE id_num_target = '$id_num_target') AS total_penilai
            FROM hrm_employee
            WHERE id_num = '$id_num_target'";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $employee = $stmt->fetch(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $result[] = $employee;

  echo json_encode($result);
} else if ($request == "saveAllGrandTotal") {

  $id_num_target = $data->id_num_target;
  $periode = $data->periode;

  $dt_all_grand_total = $data->all_grand_total;
  $arr_all_grand_total = json_decode($dt_all_grand_total, true);

  // print_r($arr_all_grand_total['nilai']);

  try {

    $sql = "DELETE FROM hrm_apprasial_all_grand_total_penilaian WHERE id_num_target = '$id_num_target'";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'Gagal Delete : ' . $e;
  }

  try {

    $sql = "INSERT INTO 
              hrm_apprasial_all_grand_total_penilaian(id_num_target,
              nilai_karakter,
              nilai_komitmen,
              nilai_kompetensi,
              nilai_kontribusi,
              grand_total_nilai,
              keterangan,
              periode) 
            VALUES 
              (:id_num_target,
                :nilai_karakter,
                :nilai_komitmen,
                :nilai_kompetensi,
                :nilai_kontribusi,
                :grand_total_nilai,
                :keterangan,
                :periode)";

    $stmt = $dbh->prepare($sql);

    $stmt->bindValue(':id_num_target', $id_num_target);
    $stmt->bindValue(':nilai_karakter', $arr_all_grand_total['nilai']['nilai_karakter']);
    $stmt->bindValue(':nilai_komitmen', $arr_all_grand_total['nilai']['nilai_komitmen']);
    $stmt->bindValue(':nilai_kompetensi', $arr_all_grand_total['nilai']['nilai_kompetensi']);
    $stmt->bindValue(':nilai_kontribusi', $arr_all_grand_total['nilai']['nilai_kontribusi']);
    $stmt->bindValue(':grand_total_nilai', $arr_all_grand_total['total_nilai']);
    $stmt->bindValue(':keterangan', $arr_all_grand_total['keterangan']);
    $stmt->bindValue(':periode', $periode);
    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'Gagal Insert : ' . $e;
  }
} else if ($request == "getlistemployeePenilaianAll") {

  $result = [];

  $office = $data->office;
  $dept = $data->dept;
  $periode = $data->periode;

  if ($office != "") {
    $and_office = "AND a.kd_off = '$office'";
  } else {
    $and_office = "";
  }

  if ($dept != "") {
    $and_dept = "AND a.kd_dept = '$dept'";
  } else {
    $and_dept = "";
  }

  try {
    $sql = "SELECT a.id_num, b.id_level, g.position, a.nik, a.att_name, initcap(a.emp_name) as emp_name, to_char(a.join_date, 'DD Mon YYYY') as join_date, initcap(a.emp_sts) as emp_sts, a.emp_sts_notes, a.kd_level, a.kd_off, a.kd_dept, b.level, a.path_photo, d.division, e.department, f.office, h.*, 
            CASE 
                WHEN h.grand_total_nilai is null THEN 0
                ELSE h.grand_total_nilai
            END AS grand_total
            FROM hrm_employee a  
            LEFT JOIN hrm_level b ON a.kd_level = b.kd_level
            LEFT JOIN hrm_division d ON a.kd_div = d.kd_div
            LEFT JOIN hrm_department e ON a.kd_dept = e.kd_dept
            LEFT JOIN hrm_office f ON a.kd_off = f.kd_off
            LEFT JOIN hrm_position g ON a.kd_pos = g.kd_pos
            LEFT JOIN hrm_apprasial_all_grand_total_penilaian h ON a.id_num = h.id_num_target
            WHERE a.resign_date IS NULL AND a.emp_sts != 'LEPAS' $and_office $and_dept
            AND h.periode = '$periode'
            ORDER BY a.kd_off, grand_total DESC";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $employee = [];
    while ($d = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $employee[] = $d;
    };
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $result[] = $employee;

  echo json_encode($result);
} else if ($request == "getRapotPersonal") {

  $result = [];
  $hasil = [];
  $arrHasil = [];

  $arr_sub_indikator = [];
  $arr_nilai = [];

  $id_num_target = $data->id_num;
  $periode = $data->periode;

  $row_nilai = [];

  try {

    $sql = "SELECT b.sub_indikator, a.bobot, a.total_nilai, a.id_sub_indikator, a.id_num_penilai 
                FROM hrm_apprasial_penilaian a
                JOIN hrm_apprasial_sub_indikator b ON a.id_sub_indikator = b.id_sub_indikator
                WHERE a.id_num_target = '$id_num_target' and periode = '$periode'
                ORDER BY a.tipe_penilai ASC";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    while ($row_sub = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $arr_sub_indikator[] = $row_sub;
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal : ' . $e;
  }

  for ($i = 0; $i < count($arr_sub_indikator); $i++) {
    $id_sub_indiaktor = $arr_sub_indikator[$i]['id_sub_indikator'];
    $xid_num_penilai = $arr_sub_indikator[$i]['id_num_penilai'];

    // if($xid_num_penilai != null || $xid_num_penilai != "") {
    //   echo $xid_num_penilai;
    //   echo "\n";
    // } else {
    //   echo "Kosong";
    //   echo "\n";
    // }

    try {
      $sql2 = "SELECT (SELECT nilai FROM hrm_apprasial_penilaian WHERE id_num_target = '$id_num_target' AND id_sub_indikator = '$id_sub_indiaktor' AND tipe_penilai = 'Atasan' AND id_num_penilai = '$xid_num_penilai' and periode = '$periode') AS Atasan,
                    (SELECT nilai FROM hrm_apprasial_penilaian WHERE id_num_target = '$id_num_target' AND id_sub_indikator = '$id_sub_indiaktor' AND tipe_penilai = 'Bawahan' AND id_num_penilai = '$xid_num_penilai' and periode = '$periode') AS Bawahan,
                    (SELECT nilai FROM hrm_apprasial_penilaian WHERE id_num_target = '$id_num_target' AND id_sub_indikator = '$id_sub_indiaktor' AND tipe_penilai = 'RI' AND id_num_penilai = '$xid_num_penilai' and periode = '$periode') AS RI,
                    (SELECT nilai FROM hrm_apprasial_penilaian WHERE id_num_target = '$id_num_target' AND id_sub_indikator = '$id_sub_indiaktor' AND tipe_penilai = 'RE' AND id_num_penilai = '$xid_num_penilai' and periode = '$periode') AS RE
                    FROM hrm_apprasial_penilaian
                    WHERE id_num_target = '$id_num_target' AND id_sub_indikator = '$id_sub_indiaktor' AND id_num_penilai = '$xid_num_penilai' and periode = '$periode'";

      $stmt2 = $dbh->prepare($sql2);
      $stmt2->execute();

      while ($row_nilai = $stmt2->fetch(PDO::FETCH_ASSOC)) {
        $arr_nilai[] = $row_nilai;
      }
    } catch (PDOException $e) {
      $e->getMessage();
      echo 'gagal : ' . $e;
    }

    $hasil[] = array(
      "id_sub_indikator" => $arr_sub_indikator[$i]['id_sub_indikator'],
      "sub_indikator" => $arr_sub_indikator[$i]['sub_indikator'],
      "bobot" => $arr_sub_indikator[$i]['bobot'],
      "total_nilai" => $arr_sub_indikator[$i]['total_nilai'],
      "nilai" => $arr_nilai
    );
    $arr_nilai = [];
  }

  for ($k = 0; $k < count($hasil); $k++) {

    $arrHasil[] = array(
      "id_sub_indikator" => $hasil[$k]['id_sub_indikator'],
      "sub_indikator" => $hasil[$k]['sub_indikator'],
      "bobot" => $hasil[$k]['bobot'],
      "total_nilai" => $hasil[$k]['total_nilai'],
      "atasan" => ($hasil[$k]['nilai'][0]['atasan'] == null) ? 0 : $hasil[$k]['nilai'][0]['atasan'],
      "bawahan" => ($hasil[$k]['nilai'][0]['bawahan'] == null) ? 0 : $hasil[$k]['nilai'][0]['bawahan'],
      "ri" => ($hasil[$k]['nilai'][0]['ri'] == null) ? 0 : $hasil[$k]['nilai'][0]['ri'],
      "re" => ($hasil[$k]['nilai'][0]['re'] == null) ? 0 : $hasil[$k]['nilai'][0]['re']
    );
  }

  $result[] = $arrHasil;

  echo json_encode($result);
} else if ($request == "getJadwalAll") {

  try {
    $sql = "SELECT id_setting, kd_off, to_char(startdate, 'DD Month YYYY') as startdate, to_char(enddate, 'DD Month YYYY') as enddate FROM hrm_apprasial_setting_jadwal";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $jadwal = [];
    while ($d = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $jadwal[] = $d;
    };
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $result[] = $jadwal;

  echo json_encode($result);
} else if ($request == "getSession") {

  $result = [];

  $kd_off = $data->kd_off;
  $today = $data->today;

  try {
    $sql = "SELECT * 
              FROM hrm_apprasial_setting_jadwal
              WHERE kd_off = '$kd_off'
              AND '$today' BETWEEN startdate AND enddate";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $session = [];
    while ($d = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $session[] = $d;
    };
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $result[] = $session;

  echo json_encode($result);
} else if ($request == "saveJadwal") {

  $data_jadwal = $data->jadwal;
  $arr_jadwal = json_decode($data_jadwal, true);

  try {
    $sql = "INSERT INTO hrm_apprasial_setting_jadwal(kd_off,startdate,enddate,status) 
            VALUES (:kd_off,:startdate,:enddate,'1')";
    $stmt = $dbh->prepare($sql);

    for ($i = 0; $i < count($arr_jadwal); $i++) {

      $stmt->bindValue(':kd_off', $arr_jadwal[$i]['cabang']);
      $stmt->bindValue(':startdate', $arr_jadwal[$i]['tgl_awal']);
      $stmt->bindValue(':enddate', $arr_jadwal[$i]['tgl_akhir']);

      $stmt->execute();
    }
  } catch (PDOException $e) {
    $e->getMessage();
    echo $e;
  }
} else if ($request == "deleteDataJadwal") {

  $id_setting = $data->id_setting;

  try {

    $sql_delete = "DELETE FROM hrm_apprasial_setting_jadwal WHERE id_setting = '$id_setting'";
    $stmt = $dbh->prepare($sql_delete);

    $stmt->execute();
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal Delete Position';
  }
} else if ($request == "detailRapot") {

  $result = [];

  $id_num_target = $data->id_num_target;
  $id_num_penilai = $data->id_num_penilai;
  $periode = $data->periode;

  try {
    $sql = "SELECT d.emp_name, b.indikator, c.sub_indikator, a.tipe_penilai, a.bobot, a.nilai, a.total_nilai
              FROM hrm_apprasial_penilaian a
              JOIN hrm_apprasial_indikator_kriteria b ON a.id_indikator_kriteria = b.id_indikator_kriteria
              JOIN hrm_apprasial_sub_indikator c ON a.id_sub_indikator = c.id_sub_indikator
              JOIN hrm_employee d ON a.id_num_penilai = d.id_num
              WHERE a.id_num_target = '$id_num_target'
              AND a.id_num_penilai = '$id_num_penilai'
              AND a.periode = '$periode'";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $detail = [];
    while ($d = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $detail[] = $d;
    };
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $result[] = $detail;

  echo json_encode($result);
} else if ($request == "getPasanganPenilaiByCabang") {

  $result = [];

  $office = $data->office;
  $dept = $data->dept;

  if ($office != "") {
    $and_office = "AND a.kd_off = '$office'";
  } else {
    $and_office = "";
  }

  if ($dept != "") {
    $and_dept = "AND a.kd_dept = '$dept'";
  } else {
    $and_dept = "";
  }

  try {
    $sql = "SELECT a.id_num, lower(a.emp_name) as emp_name, b.level, c.office, d.department, e.position
              FROM hrm_employee a
              JOIN hrm_level b ON b.kd_level = a.kd_level
              JOIN hrm_office c ON c.kd_off = a.kd_off
              JOIN hrm_department d ON d.kd_dept = a.kd_dept
              JOIN hrm_position e ON e.kd_pos = a.kd_pos
              WHERE a.resign_date IS NULL 
              $and_office
              $and_dept
              AND a.emp_sts != 'LEPAS'
              AND a.resign_date IS NULL";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $employee = [];
    while ($d = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $employee[] = $d;
    };
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  for ($i = 0; $i < count($employee); $i++) {
    $id_num_penilai = $employee[$i]['id_num'];

    try {
      $sql = "SELECT a.id_num, a.nik, a.att_name, lower(a.emp_name) as emp_name, to_char(a.join_date, 'DD Mon YYYY') as join_date, lower(a.emp_sts) as emp_sts, a.emp_sts_notes, a.path_photo, b.level, b.id_level, c.office, d.department, e.position, hrm_apprasial_penilai.type_penilai
                FROM hrm_apprasial_penilai 
                JOIN hrm_employee a ON a.id_num = hrm_apprasial_penilai.id_num_penilai
                JOIN hrm_level b ON b.kd_level = a.kd_level
                JOIN hrm_office c ON c.kd_off = a.kd_off
                JOIN hrm_department d ON d.kd_dept = a.kd_dept
                JOIN hrm_position e ON e.kd_pos = a.kd_pos 
                WHERE hrm_apprasial_penilai.id_num_target = '$id_num_penilai' AND a.resign_date IS NULL
                ORDER BY hrm_apprasial_penilai.type_penilai";

      $stmt = $dbh->prepare($sql);
      $stmt->execute();

      $penilai = [];
      while ($d = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $penilai[] = $d;
      };
    } catch (PDOException $e) {
      $e->getMessage();
      echo 'gagal:' . $e;
    }

    $hasil[] = array(
      "emp_name" => $employee[$i]['emp_name'],
      "level" => $employee[$i]['level'],
      "office" => $employee[$i]['office'],
      "department" => $employee[$i]['department'],
      "position" => $employee[$i]['position'],
      "target_nilai" => $penilai
    );
    $penilai = [];
  }

  $result[] = $hasil;

  echo json_encode($result);
} else if ($request == "getPasanganPenilaiByCabang2") {

  $result = [];

  $office = $data->office;
  $dept = $data->dept;
  $periode = $data->periode;

  if ($office != "") {
    $and_office = "AND a.kd_off = '$office'";
  } else {
    $and_office = "";
  }

  if ($dept != "") {
    $and_dept = "AND a.kd_dept = '$dept'";
  } else {
    $and_dept = "";
  }

  try {
    $sql = "SELECT a.id_num, lower(a.emp_name) as emp_name, b.level, c.office, d.department, e.position
              FROM hrm_employee a
              JOIN hrm_level b ON b.kd_level = a.kd_level
              JOIN hrm_office c ON c.kd_off = a.kd_off
              JOIN hrm_department d ON d.kd_dept = a.kd_dept
              JOIN hrm_position e ON e.kd_pos = a.kd_pos
              WHERE a.resign_date IS NULL 
              $and_office
              $and_dept
              AND a.emp_sts != 'LEPAS'
              AND a.resign_date IS NULL";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $employee = [];
    while ($d = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $employee[] = $d;
    };
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  for ($i = 0; $i < count($employee); $i++) {
    $id_num_penilai = $employee[$i]['id_num'];

    try {
      $sql = "SELECT a.id_num, a.nik, a.att_name, lower(a.emp_name) as emp_name, to_char(a.join_date, 'DD Mon YYYY') as join_date, lower(a.emp_sts) as emp_sts, a.emp_sts_notes, a.path_photo, b.level, b.id_level, c.office, d.department, e.position, hrm_apprasial_penilai.type_penilai,
              (SELECT COUNT(DISTINCT(id_num_penilai)) FROM hrm_apprasial_penilaian WHERE id_num_target = '$id_num_penilai' AND id_num_penilai = a.id_num AND periode = '$periode') AS sudah_nilai
              FROM hrm_apprasial_penilai 
              JOIN hrm_employee a ON a.id_num = hrm_apprasial_penilai.id_num_penilai
              JOIN hrm_level b ON b.kd_level = a.kd_level
              JOIN hrm_office c ON c.kd_off = a.kd_off
              JOIN hrm_department d ON d.kd_dept = a.kd_dept
              JOIN hrm_position e ON e.kd_pos = a.kd_pos 
              WHERE hrm_apprasial_penilai.id_num_target = '$id_num_penilai' AND a.resign_date IS NULL
              ORDER BY hrm_apprasial_penilai.type_penilai";

      $stmt = $dbh->prepare($sql);
      $stmt->execute();

      $penilai = [];
      while ($d = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $penilai[] = $d;
      };
    } catch (PDOException $e) {
      $e->getMessage();
      echo 'gagal:' . $e;
    }

    $hasil[] = array(
      "emp_name" => $employee[$i]['emp_name'],
      "level" => $employee[$i]['level'],
      "office" => $employee[$i]['office'],
      "department" => $employee[$i]['department'],
      "position" => $employee[$i]['position'],
      "target_nilai" => $penilai
    );
    $penilai = [];
  }

  $result[] = $hasil;

  echo json_encode($result);
} else if ($request == "updateAllLaporanPenilaian") {

  $dt_all_grand_total = $data->employee_all;
  $arr_all_grand_total = json_decode($dt_all_grand_total, true);


  for ($i = 0; $i < count($arr_all_grand_total); $i++) {

    $hasil[] = array(
      "id_num_target" => $arr_all_grand_total[$i]['id_num']
    );
  }

  $result[] = $hasil;

  echo json_encode($result);
} else if ($request == "getDetailUsername") {

  $result = [];

  $username = strtoupper($data->username);

  try {
    $sql = "SELECT * FROM hrm_employee WHERE att_name = '$username'";

    $stmt = $dbh->prepare($sql);
    $stmt->execute();

    $session = [];
    while ($d = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $session[] = $d;
    };
  } catch (PDOException $e) {
    $e->getMessage();
    echo 'gagal:' . $e;
  }

  $result[] = $session;

  echo json_encode($result);
} else if ($request == "getDatesFromRange") {

  // $start = date('Y-m-d');

  $today = new DateTime('today');
  $start_day = $today->modify('-2 years');
  $start = $start_day->format('Y-m-d');

  $end_day = $today->modify('+10 years');
  $end = $end_day->format('Y-m-d');

  $format = 'Y';

  $array = array();
          
  $interval = new DateInterval('P1Y');

  $realEnd = new DateTime($end);
  $realEnd->add($interval);

  $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

  foreach($period as $date) {                 
      $array[] = array(
          'tahun' => $date->format($format)
      );
  }

  echo json_encode($array);

}
