const EventBus = new Vue();

Vue.component('newpenilaian', {
    template: `<div><transition name="fade">
                <div style="transition-duration: 4s;" class="nav-tabs-custom" v-if="newappraisal">
                    <div class="tab-content" style="border-top: 1px solid #eee;">
                        <h3 style="margin-bottom: -10px;margin-top: 0px;"><i class="fa fa-address-book"></i> Buat Penilaian baru</h3><button class="btn btn-primary btn-sm pull-right" onClick="document.location.reload(true)" style="margin-top:-15px">Kembali ke Hal Utama</button>
                        <hr />
                        <div class="tab-pane fade in active" id="personal">
                        <form id="form" @submit.prevent="saveAppraisal" method="post"  class="form-horizontal">
                            <div class="row">
                                <div class="col-md-6">
                                        <!-- Text input-->
                                        <div class="form-group">
                                        <label class="col-md-3 control-label" for="nomor">Nomor Penilaian</label>
                                        <div class="col-md-3">
                                        <input id="nomor" name="nomor" type="text" placeholder="Nomor " v-model="nopenilaian" class="form-control" readonly>

                                        </div>
                                        </div>
                                </div>
                                <div class="col-md-6">
                                        <!-- Select Basic -->
                                        <div class="form-group">
                                        <label class="col-md-3 control-label" for="level">Level Karyawan</label>
                                        <div class="col-md-3">
                                            <select id="level" name="level" class="form-control" v-model="levelkaryawan" required>
                                            <option value="Manager">Manager</option>
                                            <option value="Senior Supervisor">Senior Supervisor</option>
                                            <option value="Supervisor">Supervisor</option>
                                            <option value="Staff">Staff</option>
                                            <option value="Clerk">Clerk</option>
                                            </select>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <p>&nbsp;</p>
                                <table class="table table-sm">
                                    <thead class="thead-light">
                                        <tr>
                                            <th width="5%" style="background-color: blue;color: white">#</th>
                                            <th width="15%" style="background-color: blue;color: white">Kategori</th>
                                            <th style="background-color: blue;color: white">Target Penilaian</th>
                                            <th width="10%" style="background-color: blue;color: white">Bobot Nilai</th>


                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(appraisal, k) in appraisals" :key="k">
                                            <td scope="row" class="trashIconContainer">
                                                <button class="btn btn-warning btn-sm"  @click="deleteRow(k, appraisal)"><i class="fa fa-trash"></i></button>
                                            </td>
                                            <td>
                                                <select id="kompetensikategori" name="kompetensikategori" v-model="appraisal.kategori" class="form-control" required>
                                                    <option v-for="(cat_option, i) in cat_options" :key="i" :value="cat_option.value">{{ cat_option.text }}

                                                    </option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control" type="text" name="isi" v-model="appraisal.target" placeholder="Masukkan Target Kompetensi" autocomplete="off" required/>
                                            </td>
                                            <td>
                                                <input class="form-control text-right" type="number" min="0" step=".01" v-model="appraisal.bobot" @change="calculateLineTotal(appraisal)"
                                                />
                                                <input readonly class="form-control text-right" type="hidden" min="0" step=".01" v-model="appraisal.line_total" required/>
                                            </td>
                                        </tr>

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td><button type='button' class="btn btn-info btn-sm" :disabled="totalBobot > 100" @click="addNewRow">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </td>
                                            <td></td>
                                            <td class="text-right">Total Bobot</td>
                                            <td class="text-right">{{ totalBobot }}</td>

                                        </tr>

                                    </tfoot>
                                </table>
                                <div class="row">
                                    <div class="col-md-9"></div>
                                    <div class="col-md-3">
                                        <transition name="fade">
                                            <div style="transition-duration: 4s;" class="alert alert-danger text-center" role="alert" v-if="totalBobot > 100">
                                                    <strong>Warning!</strong> {{ error }}.
                                            </div>
                                        </transition>
                                    </div>
                                </div>
                                <button class="btn btn-primary text-right" :disabled="totalBobot > 100">Submit Indikator</button>
                        </form>
                        </div>
                    </div>

                </div>

            </transition></div>`,
    props: ['username'],
    data: function () {
        return {
            levelkaryawan: '',
            totalBobot: 0,
            nomorchar: "HRD",
            nomorID: '',
            appraisals: [{
                kategori: '',
                target: '',
                bobot: '',
                line_total: 0
            }],
            error: '',
            cat_options: [],
            newForm: '',
            newappraisal: true,
            uname: this.username,
        }
    },
    methods: {
        showList() {
            this.$emit('closedList');
        },
        saveAppraisal: function () {

            //console.log(this.levelkaryawan, this.nopenilaian);
            //console.log(JSON.stringify(this.appraisals));
            axios.post('_component/data_penilaian_2.php', {
                request: "savepa",
                useradmin: this.uname,
                level: this.levelkaryawan,
                nomor: this.nopenilaian,
                kriterias: JSON.stringify(this.appraisals)
            }).then(response => {
                document.location.reload(true);
            }).catch(error => {
                console.log(error);
            });


        },
        calculateTotal() {
            var total;
            total = this.appraisals.reduce(function (sum, product) {
                var lineTotal = parseFloat(product.line_total);
                if (!isNaN(lineTotal)) {
                    return sum + lineTotal;
                }
            }, 0);
            this.totalBobot = total;
            if (this.totalBobot > 100) {
                this.error = 'Total Bobot Maksimal = 100!';
            }
            if (!this.error) {
                return true;
            }
        },
        calculateLineTotal(appraisal) {
            var total = parseFloat(appraisal.bobot);
            if (!isNaN(total)) {
                appraisal.line_total = total;
            }
            this.calculateTotal();
        },
        deleteRow(index, appraisal) {
            var idx = this.appraisals.indexOf(appraisal);
            console.log(idx, index);
            if (idx > -1) {
                this.appraisals.splice(idx, 1);
            }
            this.calculateTotal();
        },
        addNewRow() {
            this.appraisals.push({
                kategori: '',
                target: '',
                bobot: '',
                line_total: 0
            });
        },
        kompetensi() {
            axios.post('_component/data_penilaian_2.php', {
                request: 1
            })
                .then(response => {
                    this.cat_options = response.data;
                    //console.log(this.cat_options);
                })
                .catch(function (error) {
                    console.log(error);
                });

        },
        nomor() {
            axios.post('_component/data_penilaian_2.php', {
                request: "nomor"
            }).then(response => {
                this.nomorID = response.data;
            }).catch(error => {
                console.log(error);
            });
        },
    },
    created() {
        this.nomor();
        this.kompetensi();
    },
    computed: {
        nopenilaian() {
            var thn = new Date().getFullYear();
            var countNomor = (' ' + this.nomorID).length - 1;
            //console.log(countNomor, thn);

            if (countNomor < 2) {
                zero = "000";
            } else if (countNomor < 3) {
                zero = "00";
            } else {
                zero = "0";
            }
            return nopenilaian = this.nomorchar + thn + zero + this.nomorID;
        }
    }

});

Vue.component('listpenilaian', {
    template: `<div>
                    <h3 style="margin-bottom: 20px;"><i class="fa fa-address-card-o"></i> Status Penilaian Karyawan</h3>
                    <table class="table table-sm table-condensed table-bordered table-hover">
                        <thead>
                            <tr>
                                <th width="12%">No. Materi</th>
                                <th>Level Karyawan</th>
                                <th>Periode</th>
                                <th>Batas Waktu Maksimal</th>
                                <th>Diaktifkan Oleh</th>
                                <th>Status</th>
                                <th width="12%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-if="noPAAktif">
                                <td colspan="7" class="text-center"><h5> Belum ada penilaian yang aktif </h5>
                                </td>
                            </tr>

                            <tr v-for="(s, index) in status" :key="index">
                                <td>{{s.nomor}}</td>
                                <td>{{s.level}}</td>
                                <td>{{s.bulantahun}}</td>
                                <td>{{s.deadline}}</td>
                                <td><span style="text-transform:capitalize">{{s.activated_by}}</span> [ {{s.activated_date}} ]</td>
                                <td>{{s.aktif}}</td>
                                <td><button class="btn btn-danger btn-sm" @click="nonaktifpa(s, index)" v-if="s.aktif == 'Aktif'">Non Aktifkan</button><button class="btn btn-warning btn-sm" @click="aktifkanpa(s, index)" v-else>Aktifkan Kembali</button></td>
                            </tr>
                        </tbody>
                    </table>
                    <p>&nbsp;</p>
                    <h3 style="margin-bottom: 20px;"><i class="fa fa-address-book"></i> Daftar materi Penilaian</h3>
                    <table class="table table-sm table-condensed table-hover table-bordered">
                        <thead class="thead-light">
                            <tr>
                                <th width="12%">No. Materi </th>
                                <th>Untuk Level Karyawan</th>
                                <th>Tanggal Pembuatan</th>
                                <th>Dibuat Oleh</th>
                                <th width="12%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(pa, x) in pas" :key="x">
                                <td>{{ pa.nomor }}</td>
                                <td>{{ pa.level }}</td>
                                <td>{{ pa.createdate }}</td>
                                <td style="text-transform: capitalize">{{ pa.createby }}</td>
                                <td><button class="btn btn-primary btn-sm" @click="showpa(pa.id)">Lihat</button> <button class="btn btn-danger btn-sm" @click="deletepa(pa.id)" v-if="pa.isdelete == 0"><i class="fa fa-trash"></i></button></td>
                            </tr>
                        </tbody>
                    </table>
                    <div v-if="isConfirm || isAlert">
                        <transition name="modal">
                            <div class="modal-mask">
                                <div class="modal-wrapper">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header bg-blue">
                                            <h3 class="modal-title"><i class="fa fa-exclamation-triangle"></i> Informasi</h3>
                                        </div>
                                        <div class="modal-body">
                                            <h5>{{modalMsg}}</h5>
                                        </div>
                                        <div class="modal-footer" v-if="isConfirm">
                                            <button type="button" class="btn btn-default" @click="isConfirm = false">Tidak</button>
                                            <button type="button" class="btn btn-primary" @click="submitAction(paId,isActive,isAction)">Ya</button>
                                        </div>
                                        <div class="modal-footer" v-if="isAlert">
                                            <button type="button" class="btn btn-default" @click="isAlert = false">Tutup</button>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </transition>
                    </div>
                </div>
                `,
    data: function () {
        return {
            pas: [],
            status: [],
            currentPage: 1,
            pageCount: 0,
            isConfirm: false,
            isAlert: false,
            modalMsg: '',
            paId: '',
            isActive: '',
            isAction: '',
            noPAAktif: false,
        }
    },
    methods: {
        showpa(id) {
            this.$emit('viewedpa', id);
        },
        submitAction(paId, isActive, isAction) {

            let paid = paId;

            if (isAction == 1) {
                var action = { request: "nonAktifkanpa", paid: paid, vaktif: isActive };

            } else {
                var action = { request: "hapusPa", paid: paid };
            }
            //console.log(action);
            axios.post('_component/data_penilaian_2.php', action).then(response => {
                console.log(response.data);
                this.isConfirm = false;
                document.location.reload(true);
            }).catch(error => {
                console.log(error);
            });

        },
        activepa(id) {
            this.$emit('activedpa', id);
        },
        nonaktifpa(s) {
            this.isAction = 1;
            this.isConfirm = true;
            this.paId = s.id;
            this.isActive = 0;
            this.modalMsg = "Apakah anda yakin untuk menonaktifkan penilaian karyawan?";
        },
        aktifkanpa(s) {
            //let active = this.status.find(obj => obj.level === s.level);
            let active = this.status.filter(function (v, i) {
                return (v["level"] == s.level && v.aktif == "Aktif")
            })

            if (active.length == 1) {
                this.isAlert = true;
                this.modalMsg = "Penilaian untuk staff yang aktif sudah ada! Mohon menonaktifkan terlebih dahulu sebelum aktifkan kembali!";

            } else {
                this.isAction = 1;
                this.isConfirm = true;
                this.paId = s.id;

                this.isActive = 1;
                this.modalMsg = "Apakah anda yakin untuk mengaktifkan kembali penilaian karyawan ini?";
            }
        },
        deletepa(id) {
            this.isAction = 0;
            this.isConfirm = true;
            this.paId = id;
            this.modalMsg = "Apakah anda yakin untuk menghapus materi penilaian ini?";
        },
        getPA() {
            axios.post('_component/data_penilaian_2.php', {
                request: "getpa"
            }).then(response => {
                this.pas = response.data[0];
                this.status = response.data[1];

                if (this.status.length > 0) {
                    this.noPAAktif = false;
                } else {
                    this.noPAAktif = true;
                }

                console.log(this.pas);
                console.log(this.status);
            }).catch(error => {
                console.log(error);
            });
        }

    },
    created() {
        this.getPA();
    }
});

Vue.component('showpa', {
    template: `
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-10">
                            <h4 style="margin-bottom: 10px;">Materi PA (<span style="font-style: italic">Performance Appraisal</span>)</h4>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-success" style="margin-top: 14px;" onClick="document.location.reload(true)">Kembali ke Daftar PA</button>
                        </div>
                    </div>
                    <p>&nbsp;</p>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Nomor: {{nomor}}</th>
                                <th>Untuk Level:  {{target}}</th>
                                <th>Dibuat oleh : {{dibuat}}</th>
                            </tr>
                            <tr>
                                <th colspan="3">&nbsp;</th>
                            </tr>
                            <tr>
                                <th>KRITERIA</th>
                                <th>TARGET PENCAPAIAN</th>
                                <th>BOBOT</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(detail,k) in details" :key="k">
                                <td :class="getClass(detail.kompetensi)">{{detail.kompetensi}}</td>
                                <td>{{detail.kriteria}}</td>
                                <td>{{detail.bobotnilai}}</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2" style="text-align: right;">BOBOT TOTAL</td>
                                <td>{{total}}</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            `,
    props: ['paid'],
    data() {
        return {
            details: [],
            nomor: '',
            target: '',
            dibuat: '',
            nopa: this.paid
        }
    },
    methods: {
        showdetailpa(id) {
            axios.post('_component/data_penilaian_2.php', {
                request: "showdetailpa",
                nomor: id,
            }).then(response => {
                this.details = response.data['kompetensis'];
                this.nomor = response.data['headers']['nomor_pa'];
                this.target = response.data['headers']['target_level'];
                this.dibuat = response.data['headers']['create_by'] + ' ' + response.data['headers']['create_date']
                console.log(response.data);
            }).catch(error => {
                console.log(error);
            });
        },
        getClass(x) {
            var bgClass;

            if (x === 'Karakter') {
                bgClass = 'bg-primary';
            } else if (x == 'Kontribusi') {
                bgClass = 'bg-info';
            } else if (x == 'Komitmen') {
                bgClass = 'bg-success';
            } else {
                bgClass = 'bg-warning';
            }
            return bgClass;
        }
    },
    computed: {
        total() {
            return this.details.reduce(function (sum, detail) {
                return sum + detail.bobotnilai
            }, 0)
        },
    },
    created() {
        this.showdetailpa(this.nopa);
    }

});


Vue.component('aktifasipa', {
    template: `<div style="background-color: #dddddd;border-radius: 5px;padding: 25px 40px;margin-bottom: 50px;">
                    <div class="alert alert-warning text-center fade in" role="alert" v-if="errorid > 0" >
                        {{ errorMsg }}
                    </div>
                    <form @submit.prevent="aktifkanMateri">
                    <h4>Aktifasi Penilaian</h4>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th width="5%" style="border: 0px solid #ddd;">&nbsp;</th>
                                <th style="border: 0px solid #ddd;">Level</th>
                                <th style="border: 0px solid #ddd;">Materi Penilaian</th>
                                <th style="border: 0px solid #ddd;">Bulan Penilaian</th>
                                <th style="border: 0px solid #ddd;">Batas Akhir</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(materi, k) in materis" :key="k">
                                <td style="border: 0px solid #ddd;">
                                    <button class="btn btn-warning btn-sm" type="button" @click="deleteRow(k, materi)"><i class="fa fa-trash"></i></button>
                                </td>
                                <td style="padding-right:20px; border: 0px solid #ddd;">
                                    <select class="form-control" v-model="materi.level">
                                        <option value="">-- Pilih Level --</option>
                                        <option value="GM">General Manager</option>
                                        <option value="MGR">Manager</option>
                                        <option value="SSPV">Senior Supervisor</option>
                                        <option value="SPV">Supervisor</option>
                                        <option value="STF">Staff</option>
                                        <option value="CL">Clerk</option>
                                    </select>
                                </td>
                                <td style="padding-right:20px; border: 0px solid #ddd;">
                                    <select class="form-control" v-model="materi.nomateri">
                                        <option value="" selected>-- Pilih Materi --</option>
                                        <option v-for="materipa in materipas" :key="materipa.id" :value="materipa.nomor">{{ materipa.nomor }} - Materi {{materipa.level}}</option>
                                    </select>
                                </td>
                                <td style="padding-right:20px; border: 0px solid #ddd;">
                                    <vuejs-datepicker
                                        v-model="materi.startmonth"
                                        :value="Date"
                                        :format="DatePickerFormata"
                                        :disabledDates="disabledDates"
                                        :bootstrap-styling="true"
                                        :placeholder="holdera"
                                        :minimum-view="minv"
                                        >
                                    </vuejs-datepicker>
                                </td>
                                <td style="padding-right:20px; border: 0px solid #ddd;">
                                    <vuejs-datepicker
                                        v-model="materi.enddate"
                                        :format="DatePickerFormatb"
                                        :disabledDates="disabledDates"
                                        :bootstrap-styling="true"
                                        :placeholder="holderb"
                                        >

                                    </vuejs-datepicker>
                                </td>

                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td style="border: 0px solid #ddd;">
                                    <button type='button' class="btn btn-info btn-sm" @click="addNewRow">
                                        <i class="fa fa-plus"></i>
                                     </button>
                                </td>
                                <td colspan="4" style="border: 0px solid #ddd;">&nbsp;</td>
                            </tr>
                        </tfoot>
                    </table>
                    <button class="btn btn-primary text-right">Aktifkan Penilaian</button>
                    </form>
                </div>`,
    props: ['username'],
    components: {
        vuejsDatepicker
    },
    data() {
        return {
            minv: 'month',
            errorid: 0,
            errorMsg: '',
            materis: [{
                level: '',
                startmonth: '',
                enddate: '',
                nomateri: '',
            }],
            monthNames: [
                { value: "1", text: "January" },
                { value: "2", text: "February" },
                { value: "3", text: "March" },
                { value: "4", text: "April" },
                { value: "5", text: "May" },
                { value: "6", text: "June" },
                { value: "7", text: "July" },
                { value: "8", text: "August" },
                { value: "9", text: "September" },
                { value: "10", text: "October" },
                { value: "11", text: "November" },
                { value: "12", text: "December" }],
            materipas: [],
            selectedLevel: '',
            materiAda: '',
            uname: this.username,
            activatedMsg: '',
            holdera: "-- Pilih Bulan --",
            holderb: "-- Pilih Tanggal --",
            DatePickerFormata: 'MMMM yyyy',
            DatePickerFormatb: 'dd MMMM yyyy',
            disabledDates: {
                to: new Date(Date.now() - 8640000)
            },
            startmonth: '',
            enddate: ''

        }
    },
    methods: {
        aktifkanMateri() {

            console.log(JSON.stringify(this.materis));

            axios.post('_component/data_penilaian_2.php', {
                request: "savepaperiode",
                materis: JSON.stringify(this.materis),
                useradmin: this.uname
            }).then(response => {
                console.log(response.data);
                this.activatedMsg = response.data;
                if (response.data[0].id > 0) {
                    this.errorid = response.data[0].id;
                    this.errorMsg = response.data[0].msg;
                } else {
                    document.location.reload(true)
                }
            }).catch(error => {
                console.log(error);
            });
        },
        deleteRow(index, materi) {
            var idx = this.materis.indexOf(materi);
            console.log(idx, index);
            if (idx > -1) {
                this.materis.splice(idx, 1);
            }

        },
        addNewRow() {
            this.materis.push({
                level: '',
                startmonth: '',
                enddate: '',
                nomateri: ''
            });
        },
        getMateriPa() {
            axios.post('_component/data_penilaian_2.php', {
                request: "getMateriPA"
            }).then(response => {
                this.materipas = response.data;
                console.log(response.data);
            }).catch(error => {
                console.log(error);
            });
        },
        selectMateri(nomateri, $event) {
            var levelid = event.target.value;
            var k = index;
            axios.post('_component/data_penilaian_2.php', {
                request: "getMateriPA",
                level: levelid
            }).then(response => {
                if (response.data.length > 0) {
                    this.materiAda = response.data.length;
                    this.materipas = response.data;
                } else {
                    this.materiAda = 0;
                }
                console.log(this.materiAda);
                console.log(this.materipas);

            }).catch(error => {
                console.log(error);
            });

        },
        customFormatter(date) {
            return moment(date).format('MMMM Do YYYY, h:mm:ss a');
        },
        callFunction() {

            var currentDate = new Date();
            // console.log(currentDate);

            var currentDateWithFormat = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
            // console.log(currentDateWithFormat);

            var bulanini = currentDate.getMonth();
            var tahunini = currentDate.getFullYear();
            // console.log(bulanini);
            var tahun = [];
            for (i = 0; i < 2; i++) {
                tahun.push(tahunini + i);
            }
            this.optionsYear = tahun;
            this.selectedYear = tahunini;
            this.selectedMonth = bulanini + 1;
            // console.log(tahun);
        },
    },
    beforeDestroy() {

    },
    created() {
        this.getMateriPa();
        this.callFunction();
    }

});

Vue.component('lihatnilai', {
    template: `
        <div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <input type="text" v-model="searchName" placeholder="Cari Nama detail_karyawan..." class="form-control">
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="pilih" class="col-sm-4 control-label">Kantor</label>
                        <div class="col-sm-8">
                            <select class="form-control" v-model="selectedOffice">
                                    <option value="" selected>- ALL -</option>
                                    <option v-for="(office,k) in offices" :key="k" :value="office.namaoffice">{{office.namaoffice}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="pilih" class="col-sm-4 control-label">Departemen</label>
                        <div class="col-sm-8">
                            <select class="form-control" v-model="selectedDepartemen">
                                <option value="">- ALL -</option>
                                <option v-for="(departemen,x) in departemens" :key="x" :value="departemen.namadepartemen">{{departemen.namadepartemen}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <p style="font-size: 2.0em;font-weight: 600;text-align: right;">#TotalKaryawan : {{filterResult}}</p>
                </div>

            </div>
            <p>&nbsp;</p>
            <div class="row">
                <div class="col-md-12">
                <table class="table table-shopping table-striped">
                    <thead>
                        <tr>
                            <th class="text-center"></th>
                            <th></th>
                            <th class="text-right">Lokasi</th>
                            <th class="text-right">Indikator Penilaian</th>
                            <th class="text-right">Hasil Penilaian</th>
                            <th class="text-right">Total Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr v-if="loading">
                        <td colspan="6">
                            <div class="lds-facebook"><div></div><div></div><div></div></div>
                        </td>
                    </tr>
                    <template v-for="p,index in filterItems" v-if="index >= pagestart && index <= pageend">
                    <tr v-on:click="clickDetail(p)"">
                        <td>
                            <div class="img-container" style="padding-left: 20px;">
                                <img :src="'https://web.pt-saa.com/img/hrm/emp/'+ p.path_photo" alt="Photo" class="img-rounded" v-if="p.path_photo != null" />
                                <img src="https://web.pt-saa.com/img/avatar/avatar.png" alt="Photo Belum Ada" class="img-rounde" v-else>
                            </div>
                        </td>
                        <td class="td-product">
                            <dl class="dl-horizontal">
                                <dt>No Penilaian</dt><dd><span class="label label-default">{{p.nomorpa}}</span></dd>
                                <dt>Nama</dt><dd style="text-transform: capitalize;">{{p.emp_name}}</dd>
                                <dt>Jabatan</dt><dd>{{p.level}}</dd>
                                <dt>N.I.K</dt><dd>{{p.nik}}</dd>
                                <dt>Tgl Bergabung</dt><dd>{{p.join_date}}</dd>
                                <dt>Status</dt><dd><span style="text-transform: capitalize;">{{p.emp_sts}}</span> <span v-if="p.emp_sts != 'tetap'">({{p.emp_sts_notes}})</span></dd>

                            </dl>

                        </td>

                        <td class="td-price">
                            <span class="label label-default">{{p.office}}</span><br />
                            {{p.department}}<br />
                            <span style="text-transform: capitalize;">{{p.division}}</span>
                        </td>
                        <td class="td-price text-center" >
                           <span class="label label-default" v-if="p.indikator == null">Belum Dinilai</span>
                           <span class="label label-primary" v-if="p.indikator == 'Sangat Memuaskan'">Sangat Memuaskan</span>
                           <span class="label label-info" v-if="p.indikator == 'Memuaskan'">Memuaskan</span>
                           <span class="label label-success" v-if="p.indikator == 'Cukup Memuaskan'">Cukup Memuaskan</span>
                           <span class="label label-warning" v-if="p.indikator == 'Kurang Memuaskan'">Sangat Memuaskan</span>
                           <span class="label label-danger" v-if="p.indikator == 'Tidak Memuaskan'">Sangat Memuaskan</span>
                        </td>
                        <td class="td-quantity text-center">
                            <div v-if="p.paid == null">Belum Ada</div>
                          <div v-else>
                            <div class="progress" v-for="(k,x) in p.kompetensis" :key="x">
                                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="k.nilaistaff" aria-valuemin="0" aria-valuemax="k.nilaimax" :style="{ width: k.persen+'%' }" v-if="k.kriteria == 'Karakter'">
                                    <span class="show">{{k.kriteria}} ({{k.bobot_persen}}%) : {{k.nilaistaff}}</span>
                                </div>
                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="k.nilaistaff" aria-valuemin="0" aria-valuemax="k.nilaimax" :style="{ width: k.persen+'%' }" v-if="k.kriteria == 'Komitmen'">
                                    <span class="show">{{k.kriteria}} ({{k.bobot_persen}}%) : {{k.nilaistaff}}</span>
                                </div>
                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="k.nilaistaff" aria-valuemin="0" aria-valuemax="k.nilaimax" :style="{ width: k.persen+'%' }" v-if="k.kriteria == 'Kontribusi'">
                                    <span class="show">{{k.kriteria}} ({{k.bobot_persen}}%) : {{k.nilaistaff}}</span>
                                </div>
                                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="k.nilaistaff" aria-valuemin="0" aria-valuemax="k.nilaimax" :style="{ width: k.persen+'%' }" v-if="k.kriteria == 'Kompetensi'">
                                    <span class="show">{{k.kriteria}} ({{k.bobot_persen}}%) : {{k.nilaistaff}}</span>
                                </div>

                            </div>
                        </div>
                        </td>
                        <td class="td-number-total">
                          <span v-if="p.nilaitotal == null">0</span>
                          <span v-else>{{p.nilaitotal}}</span></td>
                    </tr>
                    </template>
                    <tr>
                        <td colspan="6">

                        </td>
                    </tr>

                    </tbody>
                </table>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <nav aria-label="Page navigation" class="text-center">
                    <ul class="pagination">
                        <li><a @click="pagination(pagecurrent - 1)"><i class="fa fa-chevron-left"></i></a></li>
                        <li v-for="index in pagipages" @click="pagination(index)" :class="{active:index == pagecurrent}"><a>{{index}}</a></li>
                        <li><a @click="pagination(pagecurrent + 1)"><i class="fa fa-chevron-right"></i></a></li>
                    </ul>
                    </nav>
                </div>
            </div>
        </div>
      `,
    props: ['periode', 'kdlevel', 'userlevel'],
    data() {
        return {
            periodenilai: this.periode,
            kd_level: this.kdlevel,
            levelstaff: this.userlevel,
            loading: false,
            employees: [],
            pagecurrent: 1,
            pagestart: 0,
            pageend: 25,
            itemsperpage: 15,
            offices: [],
            departemens: [],
            selectedOffice: '',
            selectedDepartemen: '',
            selectedDivisi: '',
            filterResult: 0,
            searchName: '',
            opened: [],
            isWarning: false,
        }
    },
    methods: {
        clickDetail(p) {
            console.log("clickDeltail Nomor: " + p.nomorpa);
            if (p.nomorpa == '001') {
                this.isWarning = true;
            } else {
                this.$emit('viewpenilaian', p.nomorpa);
            }
        },
        getListEmp() {
            this.loading = true;

            axios.post('_component/data_penilaian_2.php', {
                request: "getlistemployeepenilaian",
                periode: this.periode,
                klevel: this.kd_level

            }).then(response => {
                this.loading = false;
                this.offices = response.data[0];
                this.employees = response.data[1];
                this.departemens = response.data[2];
                //this.divisis = response.data[3];

                this.filterResult = this.employees.length;
                console.log(this.employees);
                console.log(response.data);

            }).catch(error => {
                this.loading = false;
                console.log(error);
            });
        },
        pagination: function pagination(index) {
            //This calculates the range of data to show depending on the selected page
            if (index > 0 && index <= this.pagipages) {
                this.pagecurrent = index;
                this.pagestart = (this.itemsperpage * index) - this.itemsperpage;
                this.pageend = (this.itemsperpage * index);
            }
        },
    },
    computed: {

        filterItems: function filterItems() {

            let filterOffice = this.selectedOffice, filterDepartemen = this.selectedDepartemen, filterName = this.searchName

            let result = this.employees.filter(function (e) {

                let filtered = true

                if (filterName && filterName.length > 0) {
                    filtered = e.emp_name.includes(filterName)
                }
                if (filtered) {
                    if (filterOffice && filterOffice.length > 0) {
                        filtered = e.office == filterOffice
                    }
                    if (filterDepartemen && filterDepartemen.length > 0) {
                        filtered = e.department == filterDepartemen
                    }
                }
                return filtered;


            });
            console.log(filterName);
            this.filterResult = result.length;

            return result;
        },
        pagipages: function pagipage() {
            //This calculates the amount of pages
            return Math.ceil(this.filterItems.length / this.itemsperpage);

        },
        indikatorClass() {
            console.log(this.k.persen);
            // if(this.k.kriteria == 'Karakter') {
            //     this.indikatorClass = "progress-bar progress-bar-success";
            // }
            // if(this.k.kriteria == 'Komitmen') {
            //     this.indikatorClass = "progress-bar progress-bar-warning";
            // }
            // if(this.k.kriteria == 'Kontribusi') {
            //     this.indikatorClass = "progress-bar progress-bar-danger";
            // }
            // if(this.k.kriteria == 'Kompentensi') {
            //     this.indikatorClass = "progress-bar progress-bar-info";
            // }

            // return this.indikatorClass;
        }
    },
    watch: {
        filterItems: function () {
            this.pagecurrent = 1;
            this.pagestart = 0;
            this.pageend = this.itemsperpage;
        },

    },
    created() {
        this.getListEmp();
    },
});

Vue.component('viewpenilaian', {
    template: ` <div>
                   <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-primary pull-right" @click="kembaliDaftar">Kembali ke Daftar</button>&nbsp;&nbsp;&nbsp;
                        </div>
                    </div>
                    <div class="row" v-if="isLoading">
                        <div class="col-md-12">
                            <div class="lds-facebook"><div></div><div></div><div></div></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-sm-6 col-xs-6">

                            <h4>Informasi Karyawan</h4>
                            <hr />
                            <div class="box" style="border-top: 0px;" >
                                <div class="box-body box-profile" v-for="(emp,index) in emps" :key="index">
                                    <img class="profile-user-img img-responsive img-rounded" :src="'https://web.pt-saa.com/img/hrm/emp/'+ emp.path_photo"  alt="User profile picture" v-if="emp.path_photo != null">
                                    <img class="profile-user-img img-responsive img-rounded" src="https://web.pt-saa.com/img/avatar/avatar.png" alt="Photo Belum Ada" v-else>
                                    <h3 class="profile-username text-center" style="text-transform: capitalize">{{emp.emp_name}}</h3>

                                    <p class="text-muted text-center">{{emp.position}}</p>

                                    <ul class="list-group list-group-unbordered">
                                        <li class="list-group-item">
                                            <b>N.I.K</b> <a class="pull-right">{{emp.nik}}</a>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Departemen</b> <a class="pull-right">{{emp.department}}</a>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Tgl Bergabung</b> <a class="pull-right">{{emp.join_date}}</a>
                                        </li>
                                        <li class="list-group-item">
                                            <b>Status</b> <a class="pull-right" style="text-transform: capitalize;">{{emp.emp_sts}}</span> <span v-if="emp.emp_sts != 'tetap'">{{emp.emp_sts_notes}}  ({{emp.start_date}} - {{emp.end_date}})</span></a>
                                        </li>
                                        <li class="list-group-item">
                                            <h4>Hasil Penilaian</h4>
                                        </li>
                                        <div  v-for="(k,x) in komps" :key="x">
                                        <li class="list-group-item" v-if="k.kriteria == 'Karakter'">
                                            <b>{{k.kriteria}}</b> <a class="pull-right">{{k.nilaistaff}}</a>
                                        </li>
                                        <li class="list-group-item" v-if="k.kriteria == 'Komitmen'">
                                            <b>{{k.kriteria}}</b> <a class="pull-right">{{k.nilaistaff}}</a>
                                        </li>
                                        <li class="list-group-item" v-if="k.kriteria == 'Kontribusi'">
                                            <b>{{k.kriteria}}</b> <a class="pull-right">{{k.nilaistaff}}</a>
                                        </li>
                                        <li class="list-group-item" v-if="k.kriteria == 'Kompetensi'">
                                            <b>{{k.kriteria}}</b> <a class="pull-right">{{k.nilaistaff}}</a>
                                        </li>
                                        </div>

                                    </ul>
                                    <div class="hide progress" v-for="(k,x) in komps" :key="x">
                                        <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="k.nilaistaff" aria-valuemin="0" aria-valuemax="k.nilaimax" :style="{ width: k.persen+'%' }" v-if="k.kriteria == 'Karakter'" >
                                            <span class="show">{{k.kriteria}} : {{k.nilaistaff}}</span>
                                        </div>
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="k.nilaistaff" aria-valuemin="0" aria-valuemax="k.nilaimax" :style="{ width: k.persen+'%' }" v-if="k.kriteria == 'Komitmen'">
                                            <span class="show">{{k.kriteria}} ({{k.bobot_persen}}%) : {{k.nilaistaff}}</span>
                                        </div>
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="k.nilaistaff" aria-valuemin="0" aria-valuemax="k.nilaimax" :style="{ width: k.persen+'%' }" v-if="k.kriteria == 'Kontribusi'">
                                            <span class="show">{{k.kriteria}} ({{k.bobot_persen}}%) : {{k.nilaistaff}}</span>
                                        </div>
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="k.nilaistaff" aria-valuemin="0" aria-valuemax="k.nilaimax" :style="{ width: k.persen+'%' }" v-if="k.kriteria == 'Kompetensi'">
                                            <span class="show">{{k.kriteria}} ({{k.bobot_persen}}%) : {{k.nilaistaff}}</span>
                                        </div>

                                    </div>


                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                            <div class="row">
                            <div class="col-md-12">
                                <ul class="timeline" v-for="(kua,index) in kualis" :key="index">
                                    <!-- timeline item -->
                                    <li  >
                                        <!-- timeline icon -->
                                        <i class="fa fa-thumbs-o-up bg-blue"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header"><a href="#">Kekuatan</a></h3>

                                            <div class="timeline-body">
                                                {{kua.kekuatan}}
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <!-- timeline icon -->
                                        <i class="fa fa-exclamation-triangle bg-yellow"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header"><a href="#">Kelemahan</a></h3>

                                            <div class="timeline-body">
                                                {{kua.kelemahan}}
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <!-- timeline icon -->
                                        <i class="fa fa-shield bg-green"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header"><a href="#">Pelatihan</a></h3>

                                            <div class="timeline-body">
                                                {{kua.pelatihan}}
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <!-- timeline icon -->
                                        <i class="fa fa-paper-plane-o bg-red"></i>
                                        <div class="timeline-item">
                                            <h3 class="timeline-header"><a href="#">Pengembangan</a></h3>

                                            <div class="timeline-body">
                                                {{kua.pengembangan}}
                                            </div>
                                        </div>
                                    </li>

                                    <!-- END timeline item -->



                                </ul>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-7 col-sm-6 col-xs-6">
                            <h4>Hasil Penilaian</h4>
                            <hr />
                             
                              <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kriteria</th>
                                        <th>Deskripsi</th>
                                        <th>Bobot</th>
                                        <th>Nilai Atasan</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(krit,z) in krits" :key="z">
                                        <td>{{z+1}}.</td>
                                        <td>{{krit.kompetensi}}</td>
                                        <td>{{krit.kriteria}}</td>
                                        <td class="text-center">{{krit.bobot_nilai}}</td>
                                        <td class="text-center">{{krit.nilai_staff}}</td>
                                        <td class="text-right">{{krit.nilai_total}}</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="2">TOTAL NILAI </td>
                                        <td colspan="4" class="text-right"><strong>{{total}}<strong></td>
                                    </tr>
                                    <tr  v-for="(e,dex) in emps" :key="dex">
                                        <td colspan="2">HASIL PENILAIAN </td>
                                        <td colspan="4" class="text-right"><strong>{{e.indikator}}</strong></td>
                                </tfoot>
                              </table>
                        </div>
                    </div>
               </div>`,
    props: ['nomorpa'],
    data() {
        return {
            nomorpaemployee: this.nomorpa,
            emps: [],
            komps: [],
            krits: [],
            kualis: [],
            isLoading: false,
        }
    },
    methods: {
        kembaliDaftar() {
            this.$emit('kembalikedaftar');
        },
        downloadReport() {
            var doc = new jsPDF('landscape');
            // doc.autoTable({
            //     html: '#tblreport',
            //     tableWidth: 'wrap',
            //     styles: {cellPadding: 2, fontSize: 8, overflow: 'ellipsize', cellWidth: 'wrap'},
            //     columnStyles: {text: {cellWidth: 'auto'}},
            // });
            var totalPagesExp = "{total_pages_count_string}";
            var judul = "Laporan  Penilaian No. " + this.nomorpa
            doc.autoTable({
                "html": "#headprint",
                "styles": { "fontSize": 8, "halign": "justify" },
                //"styles": { "overflow": "linebreak", "cellWidth": "wrap", "rowPageBreak": "avoid", "halign": "justify", "fontSize": "8", "lineColor": "100", "lineWidth": ".25" },
                //"columnStyles": { "No": { "halign": "left" }, "Kantor": { "halign": "left", "cellWidth": "auto" }, "Dept": { "halign": "left"}, "Nama": { "halign": "left", "cellWidth": "warp" }, "Level": {  "halign": "left", "cellWidth": "auto" }, "Posisi": {  "halign": "left", "cellWidth": "auto" }, "NIK": {  "halign": "left", "cellWidth": "auto" }, "Tgl Bergabung": {  "halign": "left", "cellWidth": "auto" }, "Status": {  "halign": "left", "cellWidth": "auto" }, "Note": {  "halign": "left", "cellWidth": "warp" }, "Penilaian": { "halign": "left", "cellWidth": "auto" }, "Total Nilai": { "halign": "left", "cellWidth": "auto" }, "Kategori": { "halign": "left", "cellWidth": "auto" } },
                "theme": "striped",
                "tableWidth": "auto",
                "showHead": "everyPage",
                "showFoot": "everyPage",
                "tableLineWidth": 0,
                "tableLineColor": 200,
                "margin": { "top": 20 },
                "rowPageBreak": "avoid",

                didDrawPage: function (data) {
                    doc.setFontSize(14);
                    doc.setTextColor(40);
                    doc.setFontStyle('normal');
                    doc.text(judul, data.settings.margin.left + 0, 14);

                    // Footer
                    var str = "Halaman " + doc.internal.getNumberOfPages()
                    // Total page number plugin only available in jspdf v1.0+
                    if (typeof doc.putTotalPages === 'function') {
                        str = str + " dari " + totalPagesExp;
                    }
                    doc.setFontSize(10);

                    // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                    var pageSize = doc.internal.pageSize;
                    var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();
                    doc.text(str, data.settings.margin.left, pageHeight - 8);

                }
            });
            //     tableWidth: 'auto',
            //     styles: {cellPadding: 2, fontSize: 8, overflow: 'linebreak', cellWidth: 'wrap'},
            //     columnStyles: {nama: { cellWidth: 'wrap'},note: { cellWidth: 'wrap'} },

            //     // head: headRows(),
            //     // body: bodyRows(10),
            //     didDrawPage: function (data) {
            //         // Header
            //         doc.setFontSize(14);
            //         doc.setTextColor(40);
            //         doc.setFontStyle('normal');
            //         doc.text("Laporan Penilaian Karyawan", data.settings.margin.left + 0, 14);

            //         // Footer
            //         var str = "Halaman " + doc.internal.getNumberOfPages()
            //         // Total page number plugin only available in jspdf v1.0+
            //         if (typeof doc.putTotalPages === 'function') {
            //             str = str + " dari " + totalPagesExp;
            //         }
            //         doc.setFontSize(10);

            //         // jsPDF 1.4+ uses getWidth, <1.4 uses .width
            //         var pageSize = doc.internal.pageSize;
            //         var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();
            //         doc.text(str, data.settings.margin.left, pageHeight - 10);
            //     },
            //     margin: {top: 20}
            // });

            // Total page number plugin only available in jspdf v1.0+
            if (typeof doc.putTotalPages === 'function') {
                doc.putTotalPages(totalPagesExp);
            }

            doc.save('LaporanPenilaianNo_' + this.nomorpa + '.pdf');
        },
        getDatas() {
            this.isLoading = true;
            axios.post('_component/data_penilaian_2.php', {
                request: "getDetailPenilaian",
                nomorpa: this.nomorpaemployee
            }).then(response => {

                this.isLoading = false;
                this.emps = response.data[0];
                this.komps = response.data[1];
                this.krits = response.data[2];
                this.kualis = response.data[3];

                console.log(response.data);

            }).catch(error => {
                this.isLoading = false;
                console.log(error);
            });
        },

    },
    computed: {
        total() {
            return this.krits.reduce(function (sum, nilai) {
                return sum + nilai.nilai_total
            }, 0)
        },
        periode() {
            let bulan = this.nomorpaemployee.substring(this.nomorpaemployee.length - 6).substring(0, 2);
            let tahun = this.nomorpaemployee.substring(this.nomorpaemployee.length - 4);

            let bulanName = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

            let namaBulan = bulanName[parseInt(bulan) - 1];
            let result = namaBulan + ' ' + tahun;

            return result;
        }
    },
    created() {
        this.getDatas();
        console.log(this.nomorpaemployee);
    }

});

Vue.component('notify', {
    template: `
        <transition name="modal">
            <div class="modal-mask">
                <div class="modal-wrapper">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-blue">
                            <h3 class="modal-title"><i class="fa fa-exclamation-triangle"></i> Informasi</h3>
                        </div>
                        <div class="modal-body">
                            <p>{{}}</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" @click="isWarning = false">Tutup</button>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </transition>
    `,
    props: ['messages'],
    data() {
        return {
            isShow: false,
            contentMsg: this.messages
        }
    },
    methods: {
        showTrigger(msg) {
            this.isShow = true;
        }
    }
})

Vue.component('lihatpenilaian', {
    template: `
        <transition name="modal">
            <div>
            <h4><i class="fa fa-address-card-o"></i> Daftar Penilaian Karyawan</h4>
            <p>&nbsp;</p>
            <table class="table table-sm table-bordered table-hover">
                <thead>
                    <tr>
                        <th>No. Materi </th>
                        <th>Level Karyawan</th>
                        <th>Periode</th>
                        <th>Batas Waktu Terakhir</th>
                        <th>Diaktifkan Oleh</th>
                        <th>Diaktifkan Tanggal</th>
                        <th>Status Penilaian</th>
                        <th>Total Ternilai</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-if="isLoading">
                        <td colspan="9">
                            <div class="lds-facebook"><div></div><div></div><div></div></div>
                        </td>
                    </tr>
                    <tr v-for="s in nilais" :key="s.id">
                        <td>{{s.nomor_pa}}</td>
                        <td>{{s.level}}</td>
                        <td>{{s.periode}}</td>
                        <td>{{s.deadline}}</td>
                        <td><span style="text-transform:capitalize">{{s.activated_by}}</span></td>
                        <td>{{s.activated_date}}</td>
                        <td>{{s.isactive}}</td>
                        <td class="text-center">{{s.totalstaff}}</td>
                        <td><button class="btn btn-primary btn-sm" @click="lihatDaftar(s)" v-if="s.totalstaff > 0">Lihat Detail</button><span v-else>&nbsp;</span></td>
                    </tr>
                </tbody>
            </table>
            <p>&nbsp;</p>
            </div>
        </transition>
    `,
    data() {
        return {
            nilais: [],
            isLoading: false
        }
    },
    methods: {
        lihatDaftar(s) {
            this.$emit("detail", s.periode, s.kd_level, s.level);
        },
        getDatas() {
            this.isLoading = true;
            axios.post('_component/data_penilaian_2.php', {
                request: "getListPenilaianWithTotalStaff"

            }).then(response => {
                this.isLoading = false;
                this.nilais = response.data;

                console.log(response.data);

            }).catch(error => {
                this.isLoading = false;
                console.log(error);
            });
        },
    },
    mounted() {
        this.getDatas();
    }
});

Vue.component('Laporan', {
    template: `
            <div>
                <div class="row" style="margin-bottom: 30px;" v-if="!isShowRpt">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="pilih" class="col-sm-4 control-label">Periode</label>
                            <div class="col-sm-8">
                                <select class="form-control" v-model="selectedPeriode">
                                    <option value="">- Pilih Periode -</option>
                                    <option v-for="(periode,x) in periodes" :key="x" :value="periode.periode_name">{{periode.periode_name}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="pilih" class="col-sm-4 control-label">Kantor</label>
                            <div class="col-sm-8">
                                <select class="form-control" v-model="selectedOffice">
                                        <option value="" selected>- Pilih Kantor -</option>
                                        <option value="All">Semua Cabang</option>
                                        <option v-for="(office,k) in offices" :key="k" :value="office.namaoffice">{{office.namaoffice}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <button class="btn btn-primary" @click="generateReport">Tampilkan</button>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 30px;" v-if="isShowRpt">

                <div class="col-md-12">
                        <button class="btn btn-danger" @click="downloadReport()"><i class="fa fa-download"></i> Download PDF</button>
                </div>
                </div>
                <div class="row" id="content" v-if="isShowRpt">
                    <div class="col-md-12">
                        <div style="border: 1px solid #666;padding: 0px 30px 20px 30px;border-radius: 8px;">
                            <h3>Laporan Penilaian Karyawan Periode {{selectedPeriode}}</h3>
                            <table class="table table-condensed table-striped table-bordered" id="tblreport">
                                <thead>
                                    <tr>
                                        <th width="3%">No</th>
                                        <th width="3%">Ktr</th>
                                        <th width="3%">Dept</th>
                                        <th width="15%">Nama</th>
                                        <th>Lvl</th>
                                        <th>Posisi</th>
                                        <th width="12%">NIK</th>
                                        <th>Tgl Bergabung</th>
                                        <th>Status</th>
                                        <th>Note</th>
                                        <th>Karakter</th>
                                        <th>Kompetensi</th>
                                        <th>Komitmen</th>
                                        <th>Kontribusi</th>
                                        <th>Total Nilai</th>
                                        <th width="10%">Kategori</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(rpt, index) in reports" :key="index">
                                        <td class="text-center">{{index + 1}}.</td>
                                        <td>{{rpt.kd_off}}</td>
                                        <td>{{rpt.kd_dept}}</td>
                                        <td>{{rpt.emp_name}}</td>
                                        <td>{{rpt.level}}</td>
                                        <td>{{rpt.position}}</td>
                                        <td>{{rpt.nik}}</td>
                                        <td>{{rpt.join_date}}</td>
                                        <td>{{rpt.emp_sts}}</td>
                                        <td>{{rpt.emp_sts_notes}}</td>
                                        <td class="text-center">{{rpt.karakter}}</td>
                                        <td class="text-center">{{rpt.kompetensi}}</td>
                                        <td class="text-center">{{rpt.komitmen}}</td>
                                        <td class="text-center">{{rpt.kontribusi}}</td>
                                        <td class="text-right">{{rpt.nilaitotal}}</td>
                                        <td>{{rpt.indikator}}</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            `,
    data() {
        return {
            isSearch: false,
            offices: [],
            departemens: [],
            divisis: [],
            periodes: [],
            selectedOffice: '',
            selectedDepartemen: '',
            selectedDivisi: '',
            selectedPeriode: '',
            reports: [],
            isShowRpt: false
        }
    },
    methods: {
        downloadReport() {
            var doc = new jsPDF('landscape');
            // doc.autoTable({
            //     html: '#tblreport',
            //     tableWidth: 'wrap',
            //     styles: {cellPadding: 2, fontSize: 8, overflow: 'ellipsize', cellWidth: 'wrap'},
            //     columnStyles: {text: {cellWidth: 'auto'}},
            // });
            var totalPagesExp = "{total_pages_count_string}";
            var judul = "Laporan Penilaian Karyawan " + this.selectedOffice + " Periode: " + this.selectedPeriode;
            doc.autoTable({
                "html": "#tblreport",
                "styles": { "fontSize": 8, "halign": "justify" },
                //"styles": { "overflow": "linebreak", "cellWidth": "wrap", "rowPageBreak": "avoid", "halign": "justify", "fontSize": "8", "lineColor": "100", "lineWidth": ".25" },
                "columnStyles": { "No": { "halign": "left" }, "Kantor": { "halign": "left", "cellWidth": "auto" }, "Dept": { "halign": "left" }, "Nama": { "halign": "left", "cellWidth": "warp" }, "Level": { "halign": "left", "cellWidth": "auto" }, "Posisi": { "halign": "left", "cellWidth": "auto" }, "NIK": { "halign": "left", "cellWidth": "auto" }, "Tgl Bergabung": { "halign": "left", "cellWidth": "auto" }, "Status": { "halign": "left", "cellWidth": "auto" }, "Note": { "halign": "left", "cellWidth": "warp" }, "Penilaian": { "halign": "left", "cellWidth": "auto" }, "Total Nilai": { "halign": "left", "cellWidth": "auto" }, "Kategori": { "halign": "left", "cellWidth": "auto" } },
                "theme": "striped",
                "tableWidth": "auto",
                "showHead": "everyPage",
                "showFoot": "everyPage",
                "tableLineWidth": 0,
                "tableLineColor": 200,
                "margin": { "top": 20 },
                "rowPageBreak": "avoid",

                didDrawPage: function (data) {
                    doc.setFontSize(14);
                    doc.setTextColor(40);
                    doc.setFontStyle('normal');
                    doc.text(judul, data.settings.margin.left + 0, 14);

                    // Footer
                    var str = "Halaman " + doc.internal.getNumberOfPages()
                    // Total page number plugin only available in jspdf v1.0+
                    if (typeof doc.putTotalPages === 'function') {
                        str = str + " dari " + totalPagesExp;
                    }
                    doc.setFontSize(10);

                    // jsPDF 1.4+ uses getWidth, <1.4 uses .width
                    var pageSize = doc.internal.pageSize;
                    var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();
                    doc.text(str, data.settings.margin.left, pageHeight - 8);

                }
            });
            //     tableWidth: 'auto',
            //     styles: {cellPadding: 2, fontSize: 8, overflow: 'linebreak', cellWidth: 'wrap'},
            //     columnStyles: {nama: { cellWidth: 'wrap'},note: { cellWidth: 'wrap'} },

            //     // head: headRows(),
            //     // body: bodyRows(10),
            //     didDrawPage: function (data) {
            //         // Header
            //         doc.setFontSize(14);
            //         doc.setTextColor(40);
            //         doc.setFontStyle('normal');
            //         doc.text("Laporan Penilaian Karyawan", data.settings.margin.left + 0, 14);

            //         // Footer
            //         var str = "Halaman " + doc.internal.getNumberOfPages()
            //         // Total page number plugin only available in jspdf v1.0+
            //         if (typeof doc.putTotalPages === 'function') {
            //             str = str + " dari " + totalPagesExp;
            //         }
            //         doc.setFontSize(10);

            //         // jsPDF 1.4+ uses getWidth, <1.4 uses .width
            //         var pageSize = doc.internal.pageSize;
            //         var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();
            //         doc.text(str, data.settings.margin.left, pageHeight - 10);
            //     },
            //     margin: {top: 20}
            // });

            // Total page number plugin only available in jspdf v1.0+
            if (typeof doc.putTotalPages === 'function') {
                doc.putTotalPages(totalPagesExp);
            }

            doc.save('LaporanPenilaian' + this.selectedOffice + '.pdf');
        },
        getData() {

            axios.post('_component/data_penilaian_2.php', {
                request: "getOfficeDept",
            }).then(response => {
                this.offices = response.data[0];
                this.departemens = response.data[1];
                this.divisis = response.data[2];
                this.periodes = response.data[3];
                console.log(response.data);
            }).catch(error => {
                this.loading = false;
                console.log(error);
            });
        },
        generateReport() {
            let period = this.selectedPeriode;
            let office = this.selectedOffice;

            axios.post('_component/data_penilaian_2.php', {
                request: "generateReport",
                periode: period,
                office: office
            }).then(response => {
                this.isShowRpt = true;
                this.reports = response.data;
                console.log(response.data);
            }).catch(error => {
                this.isShowRpt = false;
                console.log(error);
            });
        }
    },
    computed: {

    },
    created() {
        this.getData();
    }
});

Vue.component('pengaturanpenilai', {
    template: `
        <div>
            <div class="row">
                <div class="col-md-8">
                    <h4>Daftar Karyawan dengan Penilai</h4>
                </div>
                <div class="col-md-4">
                <button class="btn btn-info" @click="checkpenilai"><i class="fa fa-tachometer"></i> Check Penilai</button>
                    <button class="btn btn-info" @click="buataturpenilai"><i class="fa fa-list"></i> Assign Penilai</button>
                </div>
            </div>
            <p>&nbsp;</p>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="pilih" class="col-sm-4 control-label">Nama Karyawan</label>
                        <div class="col-sm-8">
                            <input type="text" v-model="search" class="form-control" placeholder="Nama Karyawan">
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="pilih" class="col-sm-4 control-label">Kantor</label>
                        <div class="col-sm-8">
                            <select class="form-control" v-model="selectedOffice">
                                    <option value="" selected>- ALL -</option>
                                    <option v-for="(office,k) in offices" :key="k" :value="office.namaoffice">{{office.namaoffice}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="pilih" class="col-sm-4 control-label">Departemen</label>
                        <div class="col-sm-8">
                            <select class="form-control" v-model="selectedDepartemen">
                                <option value="">- ALL -</option>
                                <option v-for="(departemen,x) in departemens" :key="x" :value="departemen.namadepartemen">{{departemen.namadepartemen}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <span style="font-size: 1.5em;font-weight: 600;margin-right: 20px;">#Total : {{filterResult}} Karyawan</span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table" style="margin-top: 20px;">
                        <thead>
                            <tr>
                                <th style="background-color:#f5f5f5;">Lokasi</th>
                                <th style="background-color:#f5f5f5;text-transform: capitalize">Nama</th>
                                <th style="background-color:#f5f5f5;">NIK</th>
                                <th style="background-color:#f5f5f5;">Departemen</th>
                                <th style="background-color:#f5f5f5;">Posisi</th>
                                <th style="background-color:#f5f5f5;">Level</th>
                                <th style="background-color:#f5f5f5;">Join Date</th>
                                <th style="background-color:#f5f5f5;">Penilai</th>
                                <th style="background-color:#f5f5f5;">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-if="isLoading">
                                <td colspan="9">
                                    <div class="lds-facebook"><div></div><div></div><div></div></div>
                                </td>
                            </tr>
                            <template v-for="staff,index in filterStaffs" v-if="index >= pagestart && index <= pageend">
                            <tr>
                                <td>{{ staff.office }}</td>
                                <td>{{ staff.emp_name }}</td>
                                <td>{{ staff.nik }}</td>
                                <td>{{ staff.department }}</td>
                                <td>{{ staff.position }}</td>
                                <td>{{ staff.level }}</td>
                                <td>{{ staff.join_date }}</td>
                                <td class="text-center">
                                    {{ staff.penilai }}
                                </td>
                                <td><button class="btn btn-danger btn-sm" @click="delPenilai(staff.att_name)"><i class="fa fa-trash-o"></i></button></td>
                            </tr>
                            </template>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <nav aria-label="Page navigation" class="text-center">
                    <ul class="pagination">
                        <li><a @click="pagination(pagecurrent - 1)"><i class="fa fa-chevron-left"></i></a></li>
                        <li v-for="index in pagipages" @click="pagination(index)" :class="{active:index == pagecurrent}"><a>{{index}}</a></li>
                        <li><a @click="pagination(pagecurrent + 1)"><i class="fa fa-chevron-right"></i></a></li>
                    </ul>
                    </nav>
                </div>
            </div>
        </div>
            `,
    data() {
        return {
            isOpen: false,
            search: '',
            selectedOffice: '',
            selectedDepartemen: '',
            isLoading: false,
            departemens: [],
            offices: [],
            filterResult: 0,
            staffs: [],
            pagecurrent: 1,
            pagestart: 0,
            pageend: 25,
            itemsperpage: 15,
        }
    },
    methods: {
        checkpenilai() {
            this.$emit('klikcheckpenilai');
        },
        buataturpenilai() {
            this.$emit('assignpenilai');
        },
        delPenilai(login) {
            axios.post('_component/data_penilaian_2.php', {
                request: "deletePenilai",
                att_name: login,
            }).then(response => {
                console.log(response.data)
                document.location.reload(true)
            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
            console.log(login);
        },
        getDatas() {
            this.isLoading = true;
            axios.post('_component/data_penilaian_2.php', {
                request: "getnamastaffdanpenilai"
            }).then(response => {
                this.isLoading = false;
                this.staffs = response.data[0];
                this.offices = response.data[1];
                this.departemens = response.data[2];

                //console.log(response.data);

                console.log(this.staffs);
                console.log(this.offices);
                console.log(this.departemens);

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        pagination: function pagination(index) {
            //This calculates the range of data to show depending on the selected page
            if (index > 0 && index <= this.pagipages) {
                this.pagecurrent = index;
                this.pagestart = (this.itemsperpage * index) - this.itemsperpage;
                this.pageend = (this.itemsperpage * index);
            }
        },
    },
    computed: {
        filterStaffs() {
            let filterOffice = this.selectedOffice, filterDepartemen = this.selectedDepartemen, filterName = this.search;

            let result = this.staffs.filter(function (e) {
                let filtered = true

                if (filterName && filterName.length > 0) {
                    filtered = e.emp_name.includes(filterName)
                }
                if (filtered) {
                    if (filterOffice && filterOffice.length > 0) {
                        filtered = e.office == filterOffice
                    }
                    if (filterDepartemen && filterDepartemen.length > 0) {
                        filtered = e.department == filterDepartemen
                    }
                }
                return filtered;

            });
            //console.log(filterName);
            this.filterResult = result.length;

            return result;
        },
        pagipages: function pagipage() {
            //This calculates the amount of pages
            return Math.ceil(this.filterStaffs.length / this.itemsperpage);

        },
    },
    created() {
        this.getDatas();
    },
    watch: {
        filterStaffs: function () {
            this.pagecurrent = 1;
            this.pagestart = 0;
            this.pageend = this.itemsperpage;
        },
    }
});

Vue.component('buatpengaturanpenilai', {
    template: `
        <div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="pilih" class="col-sm-4 control-label">Kantor</label>
                        <div class="col-sm-8">
                            <select class="form-control" @change="getDataStaff()" v-model="selectedOffice">
                                    <option value="" selected>- ALL -</option>
                                    <option v-for="(office,k) in offices" :key="k" :value="office.namaoffice">{{office.namaoffice}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="pilih" class="col-sm-4 control-label">Departemen</label>
                        <div class="col-sm-8">
                            <select class="form-control" v-model="selectedDepartemen" v-if="isSelectedOffice" @change="filterDept()">
                                <option value="">- ALL -</option>
                                <option v-for="(departemen,x) in departemens" :key="x" :value="departemen.department">{{departemen.department}}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                </div>
                <div class="col-md-3">
                    <span style="font-size: 1.5em;font-weight: 600;margin-right: 20px;">#Total : {{filterResult}} Karyawan</span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table" style="margin-top: 20px;">
                        <thead>
                            <tr>
                                <th style="background-color:#f5f5f5;">Lokasi</th>
                                <th style="background-color:#f5f5f5;">Nama</th>
                                <th style="background-color:#f5f5f5;">NIK</th>
                                <th style="background-color:#f5f5f5;">Departemen</th>
                                <th style="background-color:#f5f5f5;">Posisi</th>
                                <th style="background-color:#f5f5f5;">Level</th>
                                <th style="background-color:#f5f5f5;">Join Date</th>
                                <th style="background-color:#f5f5f5;">Penilai</th>
                                <th style="background-color:#f5f5f5;">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-if="isLoading">
                                <td colspan="9">
                                    <div class="lds-facebook"><div></div><div></div><div></div></div>
                                </td>
                            </tr>
                            <tr v-for="(staff,xindex) in staffs" :key="xindex">
                                <td>{{ staff.office }}</td>
                                <td>{{ staff.emp_name }}</td>
                                <td>{{ staff.nik }}</td>
                                <td>{{ staff.department }}</td>
                                <td>{{ staff.position }}</td>
                                <td>{{ staff.level }}</td>
                                <td>{{ staff.join_date }}</td>
                                <td>
                                    <select class="form-control" v-model="staff.penilaiid">
                                        <option value="">-- Pilih Penilai --</option>
                                        <option v-for="approver,i in approvers" :key="i" :value="approver.att_name">{{ approver.emp_name}}</option>
                                    </select>
                                </td>
                                <td>
                                    <button class="btn btn-primary btn-sm" @click.prevent="savePenilai(staff.penilaiid,staff.att_name,staff.id_num,staff.emp_name,xindex)"><i class="fa fa-check"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
            `,
    data() {
        return {
            isSelectedOffice: false,
            selectedOffice: '',
            selectedDepartemen: '',
            selectedLevel: '',
            isLoading: false,
            approvers: [],
            departemens: [],
            offices: [],
            levels: [],
            filterResult: 0,
            staffs: [],
            penilaiid: ''
        }
    },
    methods: {

        savePenilai(loginPenilai, loginStaff, staffId, staffName, index) {

            axios.post('_component/data_penilaian_2.php', {
                request: "savepenilai",
                att_name_penilai: loginPenilai,
                id_num_ternilai: staffId,
                att_name_ternilai: loginStaff,
                emp_name_ternilai: staffName

            }).then(response => {
                this.isLoading = false;
                this.staffs.splice(index, 1);
                this.filterResult = this.staffs.length;

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });


            console.log('penilaiid: ' + loginPenilai);
            console.log('userid: ' + staffId);
            console.log('loginstaff: ' + loginStaff);
            console.log('nama staff: ' + staffName);

            console.log('index no: ' + index);
            console.log('records:' + this.staffs.length);
            console.log(this.staffs);
        },
        getDataPenilai() {
            axios.post('_component/data_penilaian_2.php', {
                request: "assignpenilai"
            }).then(response => {

                this.offices = response.data[0];
                this.approvers = response.data[1];
                this.departemens = response.data[2];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        filterDept() {
            let dept = this.selectedDepartemen;
            let result = this.staffs.filter(function (e) {
                return e.department.includes(dept);
            });
            // console.log(dept);
            // console.log(result);
            this.staffs = result;

        },
        getDataStaff() {

            let cabang = this.selectedOffice;

            this.isLoading = true;
            this.isSelectedOffice = true;

            axios.post('_component/data_penilaian_2.php', {
                request: "assignpenilai2",
                office: cabang

            }).then(response => {

                this.isLoading = false;
                this.filterResult = response.data.length;
                this.staffs = response.data;

                console.log(response.data);

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        }
    },
    computed: {

    },
    created() {
        this.getDataPenilai()
    },
});

Vue.component('autocomplete', {
    template: `
        <div>
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="pilih" class="col-sm-3 control-label">Pilih Penilai</label>
                        <div class="col-sm-9">
                            <div class="autocomplete">
                                <input
                                type="text"
                                @input="onChange"
                                v-model="search"
                                @keydown.down="onArrowDown"
                                @keydown.up="onArrowUp"
                                @keydown.enter="onEnter"
                                class="form-control select2"
                                />
                                <ul
                                id="autocomplete-results"
                                v-show="isOpen"
                                class="autocomplete-results"
                                >
                                <li
                                    class="loading"
                                    v-if="isLoading"
                                >
                                    Loading results...
                                </li>
                                <li
                                    v-else
                                    v-for="(result, i) in results"
                                    :key="i"
                                    @click="setResult(result)"
                                    class="autocomplete-result"
                                    :class="{ 'is-active': i === arrowCounter }"
                                >
                                    {{ result }}
                                </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control" readonly="readonly">
                </div>

            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th>Lokasi</th>
                                <th>Nama</th>
                                <th>NIK</th>
                                <th>Departemen</th>
                                <th>Posisi</th>
                                <th>Level</th>
                                <th>Join Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(staff,index) in staffs" :key="index">
                                <td><input type="checkbox"></td>
                                <td>{{ staff.office }}</td>
                                <td>{{ staff.emp_name }}</td>
                                <td>{{ staff.nik }}</td>
                                <td>{{ staff.department }}</td>
                                <td>{{ staff.position }}</td>
                                <td>{{ staff.level }}</td>
                                <td>{{ staff.join_date }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <nav aria-label="Page navigation" class="text-center">
                    <ul class="pagination">
                        <li><a @click="pagination(pagecurrent - 1)"><i class="fa fa-chevron-left"></i></a></li>
                        <li v-for="index in pagipages" @click="pagination(index)" :class="{active:index == pagecurrent}"><a>{{index}}</a></li>
                        <li><a @click="pagination(pagecurrent + 1)"><i class="fa fa-chevron-right"></i></a></li>
                    </ul>
                    </nav>
                </div>
            </div>
        </div>
            `,
    data() {
        return {
            isOpen: false,
            results: [],
            search: '',
            isLoading: false,
            arrowCounter: 0,
            items: [],
            departemen: '',
            office: '',
            staffs: [],
        }
    },
    methods: {
        getDatas() {
            axios.post('_component/data_penilaian_2.php', {
                request: "getsupervisorup"
            }).then(response => {
                this.items = response.data[0];
                this.staffs = response.data[1];

                console.log(response.data);

                console.log(this.items);
                console.log(this.staffs);
            }).catch(error => {
                console.log(error);
            });
        },
        onChange() {
            this.filterResults();
            this.isOpen = true;
        },
        filterResults() {
            // first uncapitalize all the things
            this.results = this.items.filter((item) => {
                return item.emp_name.toLowerCase().indexOf(this.search.toLowerCase()) > -1;
            }).map(function (item) {
                return item.emp_name;
            });

        },
        setResult(result) {
            this.search = result;
            this.isOpen = false;
        },
        onArrowDown(evt) {
            if (this.arrowCounter < this.results.length) {
                this.arrowCounter = this.arrowCounter + 1;
            }
        },
        onArrowUp() {
            if (this.arrowCounter > 0) {
                this.arrowCounter = this.arrowCounter - 1;
            }
        },
        onEnter() {
            this.search = this.results[this.arrowCounter];
            this.isOpen = false;
            this.arrowCounter = -1;
            this.getListStaff(this.search);
        },
        handleClickOutside(evt) {
            if (!this.$el.contains(evt.target)) {
                this.isOpen = false;
                this.arrowCounter = -1;
            }
        },
    },
    created() {
        this.getDatas();
    },
    watch: {
        items: function (val, oldValue) {
            // actually compare them
            if (val.length !== oldValue.length) {
                this.results = val;
                this.isLoading = false;
            }
            // console.log(this.results);
        },
    },

    mounted() {
        document.addEventListener('click', this.handleClickOutside);
    },
    destroyed() {
        document.removeEventListener('click', this.handleClickOutside);
    }
});

Vue.component('select2', {
    template: `
        <div class="dropdown" v-if="options">

            <!-- Dropdown Input -->
            <input class="dropdown-input"
            :name="name"
            @focus="showOptions()"
            @blur="exit()"
            @keyup="keyMonitor"
            v-model="searchFilter"
            :disabled="disabled"
            :placeholder="placeholder" />

            <!-- Dropdown Menu -->
            <div class="dropdown-content"
            v-show="optionsShown">
            <div
                class="dropdown-item"
                @mousedown="selectOption(option)"
                v-for="(option, index) in filteredOptions"
                :key="index">
                {{ option.name || option.id || '-' }}
            </div>
            </div>
        </div>
    `,
    data() {
        return {
            selected: {},
            optionsShown: false,
            searchFilter: '',
            name: 'penilai',
            options: [],
            placeholder: 'Pilih Penilai',
            disabled: false,
            maxItem: 6
        }
    },
    created() {
        //this.$emit('selected', this.selected);
    },
    computed: {
        filteredOptions() {
            const filtered = [];
            const regOption = new RegExp(this.searchFilter, 'ig');
            for (const option of this.options) {
                if (this.searchFilter.length < 1 || option.name.match(regOption)) {
                    if (filtered.length < this.maxItem) filtered.push(option);
                }
            }
            return filtered;
        }
    },
    methods: {
        selectOption(option) {
            this.selected = option;
            this.optionsShown = false;
            this.searchFilter = this.selected.name;
            //this.$emit('selected', this.selected);
        },
        showOptions() {
            if (!this.disabled) {
                this.searchFilter = '';
                this.optionsShown = true;
            }
        },
        exit() {
            if (!this.selected.id) {
                this.selected = {};
                this.searchFilter = '';
            } else {
                this.searchFilter = this.selected.name;
            }
            this.$emit('selected', this.selected);
            this.optionsShown = false;
        },
        // Selecting when pressing Enter
        keyMonitor: function (event) {
            if (event.key === "Enter" && this.filteredOptions[0])
                this.selectOption(this.filteredOptions[0]);
        }
    },
    watch: {
        searchFilter() {
            if (this.filteredOptions.length === 0) {
                this.selected = {};
            } else {
                this.selected = this.filteredOptions[0];
            }
            this.$emit('filter', this.searchFilter);
        }
    }
});

Vue.component('chart', {
    template: `<div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header" style="border-bottom: 1px Solid #CCC">
                            <h4>Grafik Penilaian Karyawan</h4>
                            <hr />
                            <div class="form-group">
                                <label class="col-md-2 col-sm-12 col-xs-12">Pilih Cabang</label>
                                <div class="col-md-2 col-sm-12 col-xs-12">
                                    <select class="form-control" v-model="selectedCabang" @change="keyUpdate();dataChart(selectedCabang);">
                                        <option value="">ALL</option>
                                        <option v-for="cab in cabangs" :value="cab.coce_code">{{cab.coce_code}}</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <b>Total Karyawan Ternilai : #{{ totalstaffternilai }}</b>
                                </div>
                            </div>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                        </div><!-- end box header -->

                        <div class="box-body table-responsive">
                            <div class="row">
                                <div class="col-md-8 col-xs-12 col-sm-12">
                                    <penilaian :datas="series" :cabang="selectedCabang" :key="componentKey" />
                                </div>
                                <div class="col-md-4 col-xs-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                        <br />
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Cabang</th><th @click="sort('emp_name')">Nama Karyawan</th><th @click="sort('kd_level')">Posisi</th><th @click="sort('nilaitotal')">Nilai</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <template v-for="d,index in filterStaff" v-if="index >= pagestart && index <= pageend">
                                            <tr>
                                                <td><div class="label label-primary">{{d.kd_off}}</div></td><td>{{d.emp_name}}</td><td>{{d.kd_level}}</td><td>{{d.nilaitotal}}</td>
                                            </tr>
                                            </template>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td colspan="4">&nbsp;</td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                        <nav aria-label="Page navigation" class="text-center">
                                            <ul class="pagination pagination-sm no-margin">
                                                <li><a @click="pagination(pagecurrent - 1)"><i class="fa fa-chevron-left"></i></a></li>
                                                <li v-for="index in pagipages" @click="pagination(index)" :class="{active:index == pagecurrent}"><a>{{index}}</a></li>
                                                <li><a @click="pagination(pagecurrent + 1)"><i class="fa fa-chevron-right"></i></a></li>
                                            </ul>
                                        </nav> 
                                        </div>
                                    </div>  
                                    
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row -->`,
    data() {
        return {
            isLoading: false,
            selectedCabang: '',
            cabangs: [],
            dataCharts: [],
            dataStaffs: [],
            pagecurrent: 1,
            pagestart: 0,
            pageend: 0,
            itemsperpage: 10,
            componentKey: 0,
            series: [],
            currentSort: 'name',
            currentSortDir: 'asc'
        }
    },
    methods: {
        keyUpdate() {
            this.componentKey += 1;
            //this.series = this.dataChart(cbg);
        },
        getDataCabang() {
            axios.post('_component/data_penilaian_2.php', {
                request: "datacabang"
            }).then(response => {
                this.cabangs = response.data[0];
                this.dataStaffs = response.data[1];
                console.log(response.data);
            }).catch(error => {
                console.log(error);
            });
        },
        sort: function (s) {
            if (s === this.currentSort) {
                this.currentSortDir = this.currentSortDir === 'asc' ? 'desc' : 'asc';
            }
            this.currentSort = s;
        },
        pagination: function pagination(index) {
            //This calculates the range of data to show depending on the selected page
            if (index > 0 && index <= this.pagipages) {
                this.pagecurrent = index;
                this.pagestart = (this.itemsperpage * index) - this.itemsperpage;
                this.pageend = (this.itemsperpage * index);
            }
        },
        dataChart(cbg) {

            if (cbg == 'ALL') {
                filterCbg = '';
            } else {
                filterCbg = cbg;
            }

            let result = this.dataStaffs.filter(function (e) {

                let filtered = true

                if (filtered) {
                    if (filterCbg && filterCbg.length > 0) {
                        filtered = e.kd_off == filterCbg;
                    }
                }
                return filtered
            });

            const position = [... new Set(result.map(x => x.kd_level))];

            let dataSeries = [];
            let color = '';

            let i = 0;
            let j = 0;

            for (i = 0; i < position.length; i++) {

                if (position[i] == "MGR") {
                    color = 'rgba(128, 0, 128, 0.5)';
                    seq = 7;
                } else if (position[i] == "SSPV") {
                    color = 'rgba(255, 0, 0, 0.5)';
                    seq = 6;
                } else if (position[i] == "SPV") {
                    color = 'rgba(255, 0, 0, 0.9)';
                    seq = 5;
                } else if (position[i] == "SSTF") {
                    color = 'rgba(0, 0, 255, 0.8)';
                    seq = 4;
                } else if (position[i] == "STF") {
                    color = 'rgba(0, 0, 255, 1)';
                    seq = 3;
                } else if (position[i] == "KOOR") {
                    color = 'rgba(128, 128, 128, 0.6)';
                    seq = 2;
                } else if (position[i] == "CL") {
                    color = 'rgba(128, 128, 128, 0.5)';
                    seq = 1;
                }


                let res = result.filter(data => (data.kd_level == position[i]));
                let nilais = [];
                //let pernilais = [];
                let x = 0;
                for (j = 0; j < res.length; j++) {
                    x = j + 2;
                    nilais.push([res[j].nilaitotal, seq]);
                }

                dataSeries.push({ 'name': position[i], 'color': color, 'data': nilais });

                // console.log('position '+i+' : '+ position[i]);
                // console.log('colors'+i+' : '+ color);
                // console.log('Result filter pos '+i +' : '+JSON.stringify(res));
                // console.log('count # emp pos '+ res.length);
                // console.log('nilai : '+nilais);

            }
            //console.log('chartdatas: '+ JSON.stringify(dataSeries,null,4));

            //return dataSeries;

            this.series = dataSeries;

        }

    },
    beforeDestroy() {

    },
    created() {
        this.getDataCabang();
    },
    computed: {
        filterStaff: function filterStaff() {

            const filterCbg = this.selectedCabang;

            let result = this.dataStaffs.filter(function (e) {

                let filtered = true

                if (filtered) {
                    if (filterCbg && filterCbg.length > 0) {
                        filtered = e.kd_off == filterCbg;
                    }
                }
                return filtered
            });

            return result.sort((a, b) => {
                let modifier = 1;
                if (this.currentSortDir === 'desc') modifier = -1;
                if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
                if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
                return 0;
            });
        },
        totalstaffternilai() {
            return this.filterStaff.length;
        },
        pagipages: function pagipage() {
            return Math.ceil(this.filterStaff.length / this.itemsperpage);
        },
    },
    watch: {
        filterStaff: function () {
            this.pagecurrent = 1;
            this.pagestart = 0;
            this.pageend = this.itemsperpage;
        },
    }
});

Vue.component('penilaian', {
    template: `<vue-highcharts :options="options" :Highcharts="Highcharts" ref="lineCharts"></vue-highcharts>`,
    props: ['datas', 'cabang'],
    components: {
        VueHighcharts: VueHighcharts.default
    },
    data() {
        return {
            dataMgr: [],
            dataStf: [],
            dataSstf: [],
            dataCl: [],
            dataSpv: [],
            dataSspv: [],
            Highcharts: Highcharts,
            options: {
                chart: {
                    type: 'scatter',
                    zoomType: 'xy'
                },
                title: {
                    text: 'Hasil Penilaian Karyawan Cabang : ' + this.cabang,
                    align: 'left'
                },
                subtitle: {
                    text: 'Periode penilaian : DES 2019',
                    align: 'left'
                },
                xAxis: {
                    title: {
                        enabled: true,
                        text: 'Nilai Karyawan'
                    },
                    //   startOnTick: true,
                    //   endOnTick: true,
                    //   showLastLabel: true
                    max: 500,
                    min: 100,
                    plotBands: [{
                        color: 'rgba(255, 0, 0, 0.5)', // Color value
                        from: 100, // Start of the plot band
                        to: 160 // End of the plot band
                    },
                    {
                        color: 'rgba(255, 255, 0, 0.5)', // Color value
                        from: 161, // Start of the plot band
                        to: 250 // End of the plot band
                    },
                    {
                        color: 'rgba(0, 128, 0, 0.5)', // Color value
                        from: 251, // Start of the plot band
                        to: 350 // End of the plot band
                    },
                    {
                        color: 'rgba(0, 0, 255, 0.5)', // Color value
                        from: 350, // Start of the plot band
                        to: 440 // End of the plot band
                    },
                    {
                        color: 'rgba(2, 117, 216, 0.5)', // Color value
                        from: 441, // Start of the plot band
                        to: 500 // End of the plot band
                    }],
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                    max: 8,
                    min: 0,
                },
                legend: {
                    layout: 'vertical',
                    align: 'left',
                    verticalAlign: 'top',
                    x: 55,
                    y: 45,
                    floating: true,
                    backgroundColor: Highcharts.defaultOptions.chart.backgroundColor,
                    borderWidth: 1
                },
                plotOptions: {
                    scatter: {
                        marker: {
                            symbol: 'circle',
                            radius: 8,
                        },
                        states: {
                        },
                        tooltip: {
                            enabled: false,
                            headerFormat: 'LEVEL: <b>{series.name}</b><br>',
                            pointFormat: 'NILAI: {point.x}'
                        }
                    }
                },
            }
        }
    },
    computed: {
        kodecabang() {
            if (this.cabang.lenght > 0) {
                const result = this.cabang;
            } else {
                const result = 'ALL';
            }
            return result;
        }
    },
    created() {
        this.options.series = this.datas;
    }
});

Vue.component('checkpenilai', {
    template: `
            <div class="row">
                <div class="col-md-12">
                    <h4>Hasil Penilaian Penilai</h4>
                    <table class="table table-condensed table-striped">
                        <thead>
                            <tr>
                                <th>Penilai</th><th class="text-center">#Dinilai</th><th class="text-center">#Ternilai</th><th class="text-center">#Belum</th>
                            </tr>
                        </thead>
                        <tbody>
                        <template v-for="(penilai,index) in penilaiList">
                            <tr @click="toggle(index)" :class="{ opened: opened.includes(index) }" role="button">
                                <td>{{penilai.penilai}}</td><td class="text-center"><span class="badge badge-info">{{penilai.total_staff}}</span></td><td class="text-center"><span class="badge badge-success">{{penilai.ternilai}}</span></td><td class="text-center"><span class="badge badge-danger">{{penilai.belumternilai}}</span></td>
                            </tr>
                            <tr v-if="opened.includes(index)">
                                <td colspan="4">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Nama Staff</th><th>Status Penilaian</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="p in penilai.liststaffs">
                                                <td>{{p.namastaff}}</td><td v-if="p.status == 1"><i class="fa fa-check"></i></td><td v-else><i class="fa fa-close"></i></td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </td>       
                            </tr>
                        </template>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4">&nbsp;</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            `,
    data() {
        return {
            penilaiList: [],
            opened: [],
        }
    },
    methods: {
        getDatas() {
            axios.post('_component/data_penilaian_2.php', {
                request: "hasilpenilai"
            }).then(response => {
                this.penilaiList = response.data;
                console.log(this.penilaiList);
            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        toggle(id) {
            const index = this.opened.indexOf(id);
            if (index > -1) {
                this.opened.splice(index, 1)
            } else {
                this.opened.push(id)
            }
        }
    },
    created() {
        this.getDatas();
    }
});

Vue.component('organisasi', {
    template:
        `
    <div class="col-md-6">
        <form @submit.prevent="savePosition()">
            <div class="form-group">
                <label>Kode Posisi</label>
                <input type="kd_post" v-model="kd_post" required class="form-control" required>
            </div>
            <div class="form-group">
                <label>Nama Posisi</label>
                <input type="post_name" v-model="post_name" required class="form-control" required>
            </div>
            <div class="form-group">
                <label>Parent</label>
                <select class="form-control select2" required v-model="kd_post_parent" required>
                    <option value="0">None</option>
                    <option v-for="(arr_postion,k) in position" :key="k" :value="arr_postion.kd_post">{{arr_postion.position_name}}</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
    `,
    props: ['kd_off'],
    data() {
        return {
            isOpen: false,
            isLoading: false,
            position: [],
            kd_post: '',
            check_kd: '',
            post_name: '',
            kd_post_parent: '',
            deskripsi: ''
        }
    },
    mounted: function () {
        this.getDataPosition();
    },
    created() {

    },
    methods: {
        getDataPosition() {
            kd_office = this.kd_off;

            this.isLoading = true;
            axios.post('_component/data_penilaian_2.php', {
                request: "getDataPosition",
                kd_off: kd_office
            }).then(response => {
                this.isLoading = false;

                this.position = response.data[0];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        savePosition() {
            let kode_post = this.kd_post, position_name = this.post_name, kode_post_parent = this.kd_post_parent, kd_office = this.kd_off;

            axios.post('_component/data_penilaian_2.php', {
                request: "checkSavePosition",
                kd_post: kode_post,
                kd_off: kd_office,
            }).then(response => {
                this.isLoading = false;

                this.check_kd = response.data[0].kd_post;

                if (kode_post == this.check_kd) {
                    alert("Kode Posisi Sudah Digunakan");
                } else {
                    axios.post('_component/data_penilaian_2.php', {
                        request: "savePosition",
                        kd_post: kode_post,
                        post_name: position_name,
                        kd_post_parent: kode_post_parent,
                        kd_off: kd_office,

                    }).then(response => {
                        this.isLoading = false;

                        alert('Data Saved');
                        window.location.href = "/hrm/penilaiankaryawanadmin_a1?kd_off=" + kd_office;

                    }).catch(error => {
                        console.log(error);
                        this.isLoading = false;
                    });
                }

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        }
    },
    computed: {

    },
    watch: {

    }
});

Vue.component('paginate', VuejsPaginate);

Vue.component('organisasi_new', {

    template:
        `
    <div>
        <h4>Manage Organisasi New</h4>
        </br>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <v-select style="text-transform: capitalize" label="namaoffice" :options="office" v-model="v_office" @input="getPositionAll" require></v-select>
                    </div>
                </div>
            </div>
        </div>
        </br></br>
        <div class="row">
            <div class="col-md-2">
                <button type="button" data-toggle='modal' data-target='#add_mapping_posisi' class="btn btn-primary">Tambah Posisi</button>
            </div>
        </div>
        <div class="modal fade" id="add_mapping_posisi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <form @submit.prevent="">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title font-weight-bold" id="exampleModalLabel">Tambah Posisi</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div id="position-modal" class="modal-body">
                            <div class="form-group">
                                <label>Office</label>
                                <v-select style="text-transform: capitalize" label="namaoffice" :options="office" v-model="v_office" require></v-select>
                            </div>
                            <div class="form-group">
                                <label>Department</label>
                                <v-select style="text-transform: capitalize" label="namadepartemen" :options="department" v-model="v_department" require></v-select>
                            </div>
                            <div class="form-group">
                                <label>Position</label>
                                <v-select style="text-transform: capitalize" label="position" :options="position" v-model="v_posisi" require></v-select>
                            </div>
                            <div class="form-group">
                                <label>Parent</label>
                                <v-select style="text-transform: capitalize" label="position" :options="position_parent" v-model="v_posisi_parent" require></v-select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    `,
    data() {
        return {
            isOpen: false,
            isLoading: false,
            office: [],
            department: [],
            position: [],
            position_parent: [],
            position_all: [],
            v_office: '',
            v_department: '',
            v_posisi: '',
            v_posisi_parent: ''
        }
    },
    methods: {
        getOfficeDept() {
            axios.post('_component/data_penilaian_2.php', {
                request: "getOfficeDept",
            }).then(response => {
                this.isLoading = false;

                this.office = response.data[0];
                this.department = response.data[1];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getPosition() {
            axios.post('_component/data_penilaian_2.php', {
                request: "getPosisi",
            }).then(response => {
                this.isLoading = false;

                this.position = response.data[0];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getPosisiParent() {
            axios.post('_component/data_penilaian_2.php', {
                request: "getPosisiParent",
            }).then(response => {
                this.isLoading = false;

                this.position_parent = response.data[0];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getPositionAll() {
            this.isLoading = true;

            let kd_office = this.v_office.kode;

            axios.post('_component/data_penilaian_2.php', {
                request: "getPosisiAll",
                kd_office: kd_office,
            }).then(response => {
                this.isLoading = false;

                this.position_all = response.data[0];
                console.log(this.position_all);

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        }
    },
    created() {
        this.getOfficeDept();
        this.getPosition();
        this.getPosisiParent();
    }

});

Vue.component('penilai', {
    template: `
    <div><transition name="fade">
        <div style="transition-duration: 0.15s;" class="nav-tabs-custom" v-if="isOpen">
            <div class="tab-content" style="border-top: 1px solid #eee;">
                <h3 style="margin-bottom: -10px;margin-top: 0px;">Penilai Karyawan</h3>
                <hr />
                <div class="row">
                    <div class="lds-facebook" v-if="isLoading"><div></div><div></div><div></div></div>
                    
                    <div class="col-md-12">
                        <div class="box-tools pull-right">
                            
                        </div>
                        <v-client-table :data="employees" :columns="columns" :options="options">
                            <button type="button" class="btn btn-xs btn-info" slot="action" slot-scope="props" target="_blank" @click="detail_karyawan(props.row)">DETAIL KARYAWAN</button>
                        </v-client-table>
                    </div>
                </div>
            </div>
        </div>
    </transition></div>
    `,
    data() {
        return {
            isOpen: true,
            isLoading: false,
            employees: [],
            columns: ['office', 'emp_name', 'nik', 'department', 'level', 'join_date', 'action'],
            options: {
                sortable: ['office', 'emp_name', 'nik', 'department', 'level', 'join_date'],
                headings: {
                    office: 'Lokasi',
                    emp_name: 'Nama Karyawan',
                    nik: 'NIK',
                    department: 'Departemen',
                    level: 'Level',
                    join_date: 'Join Date',
                    action: '#'
                },
                templates: {
                    emp_name: 'emp_component',
                    // action: 'action_component'
                },
                customFilters: ['office', 'emp_name']
            },
            isShow: true
        }
    },
    methods: {

        getDataKaryawan() {
            this.isLoading = true;

            axios.post('_component/data_penilaian_2.php', {
                request: "getlistemployeeAll",
                kd_off: '',
                kd_dept: ''
            }).then(response => {
                this.isLoading = false;
                console.log(response.data)
                // this.offices = response.data[0];
                this.employees = response.data[0];
                // this.departemens = response.data[2];
                // this.divisions = response.data[3];


            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        detail_karyawan(val) {
            EventBus.$emit('detail_karyawan', val, this.isShow);
            this.isOpen = false
        },

    },
    created() {
        this.getDataKaryawan();
        EventBus.$on('refresh', (penilai) => {
            this.getDataKaryawan();
            this.isOpen = penilai
        })
        EventBus.$on('close_pasangan_penilai', (close) => {
            this.isOpen = close;
        })
    }
});

Vue.component('emp_component', {
    props: ["data", "index", "column"],
    template: `
      <span style="text-transform: capitalize">{{ data.emp_name }}</span>`,
});

Vue.component('detail_karyawan', {
    template: `
    <div><transition name="fade">
        <div style="transition-duration: 1s;" class="nav-tabs-custom" v-if="isShow">
            <div class="tab-content" style="border-top: 1px solid #eee;">
                <h3 style="margin-bottom: -10px;margin-top: 0px;"> Detail Karyawan</h3>

                <div class="btn-toolbar pull-right" role="toolbar" aria-label="Toolbar with button groups">
                    <div class="btn-group mr-2" role="group" aria-label="Second group">
                        <button type="button" class="btn btn-success btn-sm pull-right" id="exportExcel" @click="tableToExcel2" style="margin-top: -20px;"><i class="fa fa-download"></i></button>
                    </div>
                    <div class="btn-group mr-2" role="group" aria-label="Second group">
                        <button class="btn btn-info btn-sm pull-right" @click="empty_atasan_bawahan" style="margin-top:-20px" v-if="v_btn_add_atasan_bawahan"><i class="fa fa-plus"></i></button>
                    </div>
                </div>

                <hr />
                <div class="row">
                    <div class="col-md-12" v-if="v_form_add_atasan_bawahan" style="margin-top: -4em;">
                        <form id="form" class="form-horizontal" style="background-color: rgb(221, 221, 221); border-radius: 5px; padding: 25px 40px; margin-bottom: 50px; margin-top: 3em;">
                            <div class="col-md-12">
                                <div class="col-sm-4 pull-right">
                                    <button type="button" @click="close_sub_indikator" class="btn btn-danger pull-right">X</button>
                                </div>
                            </div>
                            <table class="table table-sm">
                                <thead class="thead-light">
                                    <tr>
                                    <th>Cabang</th>
                                    <th>Nama Karyawan</th>
                                    <th>Tipe Penilai</th>
                                    <th>#</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <tr v-for="(penilai, k) in penilaiBaruAtasanBawahan" :key="k">
                                    <td>
                                        <v-select 
                                            name="office"
                                            :options="office"
                                            :reduce="office=>office.kd_off"
                                            label="office" 
                                            v-model="penilai.office" 
                                            placeholder="Pilih Kantor Cabang"
                                            @input="getEmployeeByCode(penilai.office)">
                                        </v-select>
                                    </td>
                                    <td>
                                        <v-select 
                                            style="text-transform: capitalize;"
                                            name="karyawan"
                                            :options="karyawan"
                                            :reduce="karyawan=>karyawan.id_num"
                                            label="name_off" 
                                            v-model="penilai.karyawan" 
                                            placeholder="Pilih Karyawan">
                                        </v-select>
                                    </td>
                                    <td>
                                        <v-select 
                                            style="text-transform: capitalize;"
                                            :options="opt_penilai" 
                                            v-model="penilai.tipe_penilai" 
                                            placeholder="Pilih Tipe Penilai">
                                        </v-select>
                                    </td>
                                    <td scope="row" class="trashIconContainer">
                                        <button class="btn btn-warning btn-sm"  @click="deleteRowAtasanBawahan(k, penilai)"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td>
                                            <button type='button' class="btn btn-info btn-sm" @click="addNewRowAtasanBawahan">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="row" style="margin-top: -4.2em;">
                                <button style="margin-right: 2em;" type='button' class="btn btn-info btn-sm pull-right" 
                                @click="savePenilaiAtasanBawahan()">
                                    SIMPAN
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="row" ref="tablePasangan">
                        <div class="col-md-12">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td rowspan="5" style="border: 1px Solid #ddd;">
                                        <img :src="'https://web.pt-saa.com/img/hrm/emp/'+ detail_karyawan.path_photo" alt="Photo" style="width: 100px;" class="img-rounded" v-if="detail_karyawan.path_photo != null" />
                                        <img src="https://web.pt-saa.com/img/avatar/avatar.png" alt="Photo Belum Ada" class="img-rounded" style="width: 100px;" v-else>
                                        </td>
                                        <td style="width: 17%;background-color: #f5f5f5;border: 1px Solid #ddd;">Nama</td><td style="width: 36%;border: 1px Solid #ddd; text-transform:capitalize;">{{detail_karyawan.emp_name}}</td><td style="width: 17%;background-color: #f5f5f5;border: 1px Solid #ddd;">NIK</td><td style="width: 30%;border: 1px Solid #ddd;">{{detail_karyawan.nik}}</td>
                                    </tr>
                                    <tr>
                                        <td style="background-color: #f5f5f5;border: 1px Solid #ddd;">Department</td><td style="border: 1px Solid #ddd;">{{detail_karyawan.department}}</td><td style="background-color: #f5f5f5;border: 1px Solid #ddd;">Office</td><td style="border: 1px Solid #ddd;">{{detail_karyawan.office}}</td>
                                    </tr>
                                    <tr>
                                        <td style="background-color: #f5f5f5;border: 1px Solid #ddd;">Position Name</td><td style="border: 1px Solid #ddd;">{{detail_karyawan.position}}</td><td style="background-color: #f5f5f5;border: 1px Solid #ddd;">Level</td><td style="border: 1px Solid #ddd;">{{detail_karyawan.level}}</td>
                                    </tr>
                                    <tr>
                                        <td style="background-color: #f5f5f5;border: 1px Solid #ddd;">Status</td><td style="border: 1px Solid #ddd; text-transform:capitalize;">{{detail_karyawan.emp_sts}}</td><td style="background-color: #f5f5f5;border: 1px Solid #ddd;">Join Date</td><td style="border: 1px Solid #ddd;">{{detail_karyawan.join_date}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="lds-facebook" v-if="isLoading"><div></div><div></div><div></div></div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                    
                                        <div class="col-md-6">
                                            <h4><strong>{{ judul }}</strong></h4>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="row pull-right">
                                                <div class="col-md-3">
                                                    <button type="button" @click="btn_internal" class="btn btn-sm btn-primary"><span class="fa fa-plus"></span> Atasan / Bawahan</button>
                                                </div>
                                                <div class="col-md-3">
                                                    <button type="button" @click="btn_rekanan_internal" class="btn btn-sm btn-primary"><span class="fa fa-plus"></span> Rekanan Internal</button>
                                                </div>
                                                <div class="col-md-3">
                                                    <button type="button" @click="btn_external" class="btn btn-sm btn-primary"><span class="fa fa-plus"></span> Rekanan Eksternal</button>
                                                </div>
                                                <div class="col-md-3">
                                                    <button type="button" @click="btn_all" class="btn btn-sm btn-primary">Lihat Penilai</button>
                                                </div>
                                            </div>
                                        </div>

                                        <hr />

                                        <div class="col-md-12" v-if="view_all_penilai">
                                            <v-client-table :data="penilai" :columns="columnAll" :options="optionsAll">
                                                <button type="button" class="btn btn-danger fa fa-trash-o" slot="action_all" @click="deletePenilai(props.row.id_penilai)" slot-scope="props"></button>
                                            </v-client-table>
                                        </div>

                                        <div class="col-md-12" v-if="view_internal">
                                            <v-client-table :data="penilai_ai" :columns="columnIn" :options="optionsIn"></v-client-table>
                                        </div>

                                        <div class="col-md-12" v-if="view_rekanan_internal">
                                            <table class="table table-sm">
                                                <thead class="thead-light">
                                                    <tr>
                                                    <th>Cabang</th>
                                                    <th>Nama Karyawan</th>
                                                    <th>#</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <tr v-for="(penilai, k) in penilaiBaruByDept" :key="k">
                                                    <td>
                                                        <v-select 
                                                            name="office"
                                                            :options="office"
                                                            :reduce="office=>office.kd_off"
                                                            label="office" 
                                                            v-model="penilai.office" 
                                                            placeholder="Pilih Kantor Cabang"
                                                            @input="getEmployeeByDept(penilai.office)">
                                                        </v-select>
                                                    </td>
                                                    <td>
                                                        <v-select 
                                                            style="text-transform: capitalize;"
                                                            name="karyawan"
                                                            :options="karyawan"
                                                            :reduce="karyawan=>karyawan.id_num"
                                                            label="name_off" 
                                                            v-model="penilai.karyawan" 
                                                            placeholder="Pilih Karyawan">
                                                        </v-select>
                                                    </td>
                                                    <td scope="row" class="trashIconContainer">
                                                        <button class="btn btn-warning btn-sm"  @click="deleteRowDept(k, penilai)"><i class="fa fa-trash"></i></button>
                                                    </td>
                                                </tr>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td>
                                                            <button type='button' class="btn btn-info btn-sm" @click="addNewRowDept" >
                                                                <i class="fa fa-plus"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <button type="button" id="save" class="btn btn-primary text-left" @click="savePenilaiInternal()">Simpan</button>
                                        </div>

                                        <div class="col-md-12" v-if="view_external">
                                            <table class="table table-sm">
                                                <thead class="thead-light">
                                                    <tr>
                                                    <th>Cabang</th>
                                                    <th>Nama Karyawan</th>
                                                    <th>Tipe Penilai</th>
                                                    <th>#</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <tr v-for="(penilai, k) in penilaiBaru" :key="k">
                                                    <td>
                                                        <v-select 
                                                            name="office"
                                                            :options="office"
                                                            :reduce="office=>office.kd_off"
                                                            label="office" 
                                                            v-model="penilai.office" 
                                                            placeholder="Pilih Kantor Cabang"
                                                            @input="getEmployeeByCode(penilai.office)">
                                                        </v-select>
                                                    </td>
                                                    <td>
                                                        <v-select 
                                                            style="text-transform: capitalize;"
                                                            name="karyawan"
                                                            :options="karyawan"
                                                            :reduce="karyawan=>karyawan.id_num"
                                                            label="name_off" 
                                                            v-model="penilai.karyawan" 
                                                            placeholder="Pilih Karyawan">
                                                        </v-select>
                                                    </td>
                                                    <td scope="row" class="trashIconContainer">
                                                        <button class="btn btn-warning btn-sm"  @click="deleteRow(k, penilai)"><i class="fa fa-trash"></i></button>
                                                    </td>
                                                </tr>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td>
                                                            <button type='button' class="btn btn-info btn-sm" @click="addNewRow">
                                                                <i class="fa fa-plus"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <button type="button" id="save" class="btn btn-primary text-left" @click="savePenilai()">Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </transition></div>
    `,
    // props: ['id_num'],
    data() {
        return {
            counter_internal: false,
            v_btn_add_atasan_bawahan: true,
            v_form_add_atasan_bawahan: false,
            judul: "List Penilaian Karyawan",
            view_all_penilai: true,
            view_internal: false,
            view_external: false,
            view_rekanan_internal: false,
            isShow: false,
            isOpen: false,
            isLoading: false,
            detail_karyawan: [],
            karyawan: [],
            karyawan_by_dept: [],
            penilai: [],
            id_num_penilai: '',
            checkPenilai: [],
            checkDataPenilai: false,
            id_atasan_target: '',
            kd_pos: '',
            id_pos: '',
            tipe_penilai: [],
            v_tipe_penilai: '',
            office: [],
            kd_off: [],
            data_detail: [],
            penilai_ai: [],
            columnAll: ['office', 'emp_name', 'nik', 'department', 'position', 'level', 'type_penilai', 'join_date', 'action_all'],
            optionsAll: {
                templates: {
                    emp_name: 'emp_component'
                },
                headings: {
                    office: 'Lokasi',
                    emp_name: 'Nama Karyawan',
                    nik: 'NIK',
                    department: 'Departemen',
                    level: 'Level Penilai',
                    type_penilai: 'Tipe Penilai',
                    join_date: 'Join Date',
                    action_all: '#'
                },
                sortable: ['office', 'emp_name', 'nik', 'department', 'position', 'level', 'type_penilai', 'join_date']
            },
            columnIn: ['office', 'emp_name', 'nik', 'department', 'position', 'level', 'join_date', 'action_in'],
            optionsIn: {
                templates: {
                    emp_name: 'emp_component',
                    action_in: 'action_internal_component'
                },
                headings: {
                    office: 'Lokasi',
                    emp_name: 'Nama Karyawan',
                    nik: 'NIK',
                    department: 'Departemen',
                    position: 'Posisi',
                    level: 'Level',
                    join_date: 'Join Date',
                    action_in: '#'
                },
                sortable: ['office', 'emp_name', 'nik', 'department', 'position', 'level', 'join_date']
            },
            penilaiBaru: [{
                office: '',
                karyawan: '',
            }],
            opt_penilai: [
                'Atasan',
                'Bawahan',
            ],
            penilaiBaruByDept: [{
                office: '',
                karyawan: ''
            }],
            penilaiBaruAtasanBawahan: [{
                office: '',
                karyawan: '',
                tipe_penilai: ''
            }],
        }
    },
    created() {
        EventBus.$on('close_detail_karyawan', (close) => {
            this.isShow = close;
        });
        EventBus.$on('detail_karyawan', (val, isShow) => {
            this.data_detail = val
            this.isShow = isShow
            this.getEmployeeAll();
            this.getOfficeDept();
            this.getDetailEmployee();
        })
        EventBus.$on('tutup_detail_karyawan', (detail_karyawan) => {
            this.isShow = detail_karyawan
        })
        EventBus.$on('updatePenilai2', (id, status) => {
            this.savePenilai2(id, status)
            this.getPenilaiEmployeeAI();

        })
        EventBus.$on('refresh_update_internal', (data_detail) => {
            console.log(data_detail)
            console.log("ON")
            this.penilaiBaruByDept.push({
                office: '',
                karyawan: ''
            });
            this.penilaiBaru.push({
                office: '',
                karyawan: ''
            });
            // this.data_detail = data_detail
        })
    },
    methods: {
        empty_atasan_bawahan() {
            this.v_btn_add_atasan_bawahan = false;
            this.v_form_add_atasan_bawahan = true;
            this.penilaiBaruAtasanBawahan.length = 0;
        },
        close_sub_indikator() {
            this.v_btn_add_atasan_bawahan = true;
            this.v_form_add_atasan_bawahan = false;
            this.penilaiBaruAtasanBawahan.length = 0;
        },
        btn_internal() {
            this.getPenilaiEmployeeAI();
            this.view_all_penilai = false;
            this.view_internal = true;
            this.view_external = false;
            this.view_rekanan_internal = false;
            this.judul = "List Penilai Rekanan Department Internal"
        },
        btn_external() {
            this.view_all_penilai = false;
            this.view_internal = false;
            this.view_external = true;
            this.view_rekanan_internal = false;
            this.judul = "Tambah Penilai Rekanan Eksternal"
        },
        btn_all() {
            this.view_all_penilai = true;
            this.view_internal = false;
            this.view_external = false;
            this.view_rekanan_internal = false;
            this.judul = "List Semua Penilai"
        },
        btn_rekanan_internal() {
            this.view_all_penilai = false;
            this.view_internal = false;
            this.view_external = false;
            this.view_rekanan_internal = true;
            this.judul = "Tambah Penilai Rekanan Internal"
        },
        deleteRow(index, penilai) {
            var idx = this.penilaiBaru.indexOf(penilai);
            if (idx > -1) {
                this.penilaiBaru.splice(idx, 1);
            }
        },
        deleteRowDept(index, penilai) {
            var idx = this.penilaiBaruByDept.indexOf(penilai);
            if (idx > -1) {
                this.penilaiBaruByDept.splice(idx, 1);
            }
        },
        deleteRowAtasanBawahan(index, penilai) {
            var idx = this.penilaiBaruAtasanBawahan.indexOf(penilai);
            if (idx > -1) {
                this.penilaiBaruAtasanBawahan.splice(idx, 1);
            }
        },
        addNewRow() {
            this.penilaiBaru.push({
                office: '',
                karyawan: ''
            });
        },
        addNewRowDept() {
            this.penilaiBaruByDept.push({
                office: '',
                karyawan: ''
            });
        },
        addNewRowAtasanBawahan() {
            this.penilaiBaruAtasanBawahan.push({
                office: '',
                karyawan: '',
                tipe_penilai: ''
            });
        },
        getDetailEmployee() {
            this.isLoading = true;
            // id_num = this.id_num;

            axios.post('_component/data_penilaian_2.php', {
                request: "getDetailEmployee",
                kd_off: this.data_detail.kd_off,
                id_num: this.data_detail.id_num,
            }).then(response => {
                this.isLoading = false;

                this.detail_karyawan = response.data[0];
                console.log(this.detail_karyawan);

                this.id_atasan_target = this.detail_karyawan.id_atasan;
                this.kd_pos = this.detail_karyawan.kd_pos;
                this.id_pos = this.detail_karyawan.id_pos;

                this.getPenilaiEmployee(this.id_atasan_target, this.id_pos, this.kd_pos);
                this.getPenilaiEmployeeAI();

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getPenilaiEmployee(id_atasan, id_pos, kd_pos) {
            this.isLoading = true;
            // id_num = this.id_num;

            axios.post('_component/data_penilaian_2.php', {
                request: "getPenilai",
                id_num: this.data_detail.id_num,
            }).then(response => {
                this.isLoading = false;

                this.penilai = response.data[0];
                console.log(this.penilai);

                if (this.penilai.length > 0) {
                    this.checkDataPenilai = false;
                } else {
                    this.checkDataPenilai = true;
                }

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getEmployeeAll() {
            this.isLoading = true;

            axios.post('_component/data_penilaian_2.php', {
                request: "getlistemployeeAll",
                kd_off: "",
                kd_dept: "",
            }).then(response => {
                this.isLoading = false;

                this.karyawan = response.data[0];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getEmployeeByKdOff(kd_off) {
            this.isLoading = true;

            axios.post('_component/data_penilaian_2.php', {
                request: "getlistemployeeAll",
                kd_off: kd_off,
                kd_dept: "",
            }).then(response => {
                this.isLoading = false;
                this.karyawan = response.data[1];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getEmployeeAllByDept(kd_off) {
            this.isLoading = true;

            axios.post('_component/data_penilaian_2.php', {
                request: "getlistemployeeAll",
                kd_off: kd_off,
                kd_dept: this.detail_karyawan.kd_dept
            }).then(response => {
                this.isLoading = false;

                this.karyawan_by_dept = response.data[2];
                console.log(this.karyawan_by_dept);
                console.log(this.detail_karyawan.kd_dept);

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getPenilaiEmployeeAI() {
            this.isLoading = true;

            axios.post('_component/data_penilaian_2.php', {
                request: "getPenilaiAI",
                kd_level: this.detail_karyawan.kd_level,
                kd_div: this.detail_karyawan.kd_div,
                kd_dept: this.detail_karyawan.kd_dept,
                kd_pos: this.detail_karyawan.kd_pos,
                kd_off: this.detail_karyawan.kd_off,
                id_num: this.detail_karyawan.id_num
            }).then(response => {
                this.isLoading = false;

                this.penilai_ai = response.data[0];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        async savePenilai() {
            let id_num_target = this.data_detail.id_num, id_num_penilai = this.id_num_penilai;

            await axios.post('_component/data_penilaian_2.php', {
                request: "savePenilaiExternal",
                id_target: id_num_target,
                penilaiBaru: JSON.stringify(this.penilaiBaru)
            }).then(response => {
                this.isLoading = false;
                swal.fire({
                    title: "Berhasil!",
                    text: "Penilai Rekanan External berhasil disimpan!",
                    icon: "success",
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        EventBus.$emit('refresh_update_internal', this.data_detail)
                    }
                });

                this.penilaiBaruByDept.length = 0
                this.penilaiBaruByDept = []
                this.penilaiBaru.length = 0
                this.penilaiBaru = []
                this.getPenilaiEmployee();

                // alert('Data Berhasil Ditambah');
                // this.getPenilaiEmployee();

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        async savePenilaiInternal() {
            let id_num_target = this.data_detail.id_num, id_num_penilai = this.id_num_penilai;

            await axios.post('_component/data_penilaian_2.php', {
                request: "savePenilaiInternal",
                id_target: id_num_target,
                penilaiBaru: JSON.stringify(this.penilaiBaruByDept)
            }).then(response => {
                this.isLoading = false;
                swal.fire({
                    title: "Berhasil!",
                    text: "Penilai Rekanan Internal berhasil disimpan!",
                    icon: "success",
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        EventBus.$emit('refresh_update_internal', this.data_detail)
                    }
                });
                this.penilaiBaruByDept.length = 0
                this.penilaiBaruByDept = []
                this.penilaiBaru.length = 0
                this.penilaiBaru = []
                this.getPenilaiEmployee();

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        savePenilai2(id, status) {
            let id_num_target = this.data_detail.id_num,
                id_num_penilai = id,
                tipe_penilai = status;

            // console.log(this.data_detail.id_num)
            // console.log(id_num_penilai)
            // console.log(tipe_penilai)

            axios.post('_component/data_penilaian_2.php', {
                request: "savePenilaiExternal2",
                id_target: id_num_target,
                id_penilai: id_num_penilai,
                tipe_penilai: tipe_penilai,
            }).then(response => {
                this.isLoading = false;
                swal.fire({
                    title: "Berhasil!",
                    text: "Data Penilai berhasil ditambahkan!",
                    icon: "success",
                })

                this.getPenilaiEmployee();
                this.getPenilaiEmployeeAI();

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        deletePenilai(id_penilai) {
            // let id_num_target = this.data_detail.id_num, id_num_penilai = id;

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You want to delete this data?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    axios.post('_component/data_penilaian_2.php', {
                        request: "deleteListPenilai",
                        id_penilai: id_penilai,
                    }).then(response => {
                        swal.fire({
                            title: "Terhapus!",
                            text: "Data Penilai berhasil dihapus!",
                            icon: "success",
                        })
                        this.getPenilaiEmployee();

                    }).catch(error => {
                        console.log(error);
                        this.isLoading = false;
                    });
                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your data is safe :)',
                        'error'
                    )
                }
            })

            // if (confirm("Do you really want to delete?")) {

            //     axios.post('_component/data_penilaian_2.php', {
            //         request: "deleteListPenilai",
            //         id_target: id_num_target,
            //         id_penilai: id_num_penilai,
            //     }).then(response => {

            //         this.isLoading = false;
            //         this.getPenilaiEmployee();

            //     }).catch(error => {
            //         console.log(error);
            //         this.isLoading = false;
            //     });
            // }
            // }
        },
        getOfficeDept() {
            axios.post('_component/data_penilaian_2.php', {
                request: "getOfficeDept",
                username: 'suardana',
            }).then(response => {
                this.isLoading = false;

                this.office = response.data[4];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getEmployeeByCode(val) {
            if (val != null) {
                this.getEmployeeByKdOff(val);
            } else {
                this.getEmployeeAll();
            }
        },
        getEmployeeByDept(val) {
            if (val != null || val == '') {
                this.getEmployeeAllByDept(val);
            } else {
                this.getEmployeeAll();
            }

            // <v-select 
            //     style="text-transform: capitalize;"
            //     name="karyawan_by_dept"
            //     :options="karyawan_by_dept"
            //     :reduce="karyawan_by_dept=>karyawan_by_dept.id_num"
            //     label="name_off" 
            //     v-model="penilai.karyawan" 
            //     placeholder="Pilih Karyawan">
            // </v-select>
        },
        savePenilaiAtasanBawahan() {
            let id_num_target = this.data_detail.id_num
            // console.log(this.penilaiBaruAtasanBawahan)

            axios.post('_component/data_penilaian_2.php', {
                request: "savePenilaiAtasanBawahan",
                id_target: id_num_target,
                penilaiBaru: JSON.stringify(this.penilaiBaruAtasanBawahan)
            }).then(response => {
                this.isLoading = false;
                swal.fire({
                    title: "Berhasil!",
                    text: "Penilai berhasil disimpan!",
                    icon: "success",
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        EventBus.$emit('refresh_update_internal', this.data_detail)
                        this.penilaiBaruAtasanBawahan.length = 0
                        this.penilaiBaruAtasanBawahan = []
                    }
                });
                this.getPenilaiEmployee();

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        tableToExcel2() {
            let id_num_target = this.data_detail.id_num;
            var data = {
                id_num_target
            };
            $.post("_component/pasangan_penilai_excel.php",
                JSON.stringify(data),
                function (response) {
                    window.location.href = JSON.parse(response)
                }
            )
            // axios.post('_component/data_penilaian_2.php', {
            //     request: "exportExcelPasanganPenilai",
            //     id_num: this.data_detail.id_num,
            //     responseType: 'arraybuffer',
            // }).then(response => {

            //     const url = window.URL.createObjectURL(new Blob([response.data]));
            //     const link = document.createElement('a');
            //     link.href = url;
            //     link.setAttribute('download', 'file.xls'); //or any other extension
            //     document.body.appendChild(link);
            //     link.click();

            // }).catch(error => {
            //     console.log(error);
            //     this.isLoading = false;
            // });
        }
    },
    computed: {
        nama_karyawan() {
            return this.detail_karyawan.emp_name
        }
    }
});

Vue.component('action_internal_component', {
    props: ["data"],
    template: `
        <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
            <div class="btn-group mr-2" role="group" aria-label="First group">
                <button type="button" class="btn btn-xs btn-info" @click="update(data.id_num, atasan)">ATASAN</button>
            </div>
            <div class="btn-group mr-2" role="group" aria-label="Second group">
                <button type="button" class="btn btn-xs btn-info" @click="update(data.id_num, bawahan)">BAWAHAN</button>
            </div>
        </div>
    `,
    data() {
        return {
            atasan: "Atasan",
            bawahan: "Bawahan"
        }
    },
    methods: {
        update(id, status) {
            console.log(this.data)
            console.log(id, status)
            EventBus.$emit('updatePenilai2', id, status)
        },
    }
})

Vue.component('penilaian_karyawan', {
    template:
        `
    <div class="row">
        <div class="col-md-12">
            <h4>Penilaian Karyawan</h4>
            </br>
            <table class="table table-sm table-condensed table-bordered table-hover">
                <thead>
                    <tr v-if="isLoading">
                        <td colspan="9">
                            <div class="lds-facebook"><div></div><div></div><div></div></div>
                        </td>
                    </tr>
                    <tr>
                        <th scope="col">Lokasi</th>
                        <th scope="col">Nama</th>
                        <th scope="col">NIK</th>
                        <th scope="col">Department</th>
                        <th scope="col">Position Name</th>
                        <th scope="col">Level</th>
                        <th scope="col">Status</th>
                        <th scope="col">Join Date</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <template v-for="arr_target in target_karyawan">
                        <tr @click="nilai_karyawan(arr_target.id_num,arr_target.id_level)">
                            <td>{{ arr_target.office }}</td>
                            <td style="text-transform: capitalize">{{ arr_target.emp_name }}</td>
                            <td>{{ arr_target.nik }}</td>
                            <td>{{ arr_target.department }}</td>
                            <td>{{ arr_target.position }}</td>
                            <td>{{ arr_target.level }}</td>
                            <td>{{ arr_target.xstatus }}</td>
                            <td>{{ arr_target.join_date }}</td>
                        </tr>
                    </template>
                </tbody>
            </table>
        </div>
    </div>
    `,
    props: ['username'],
    data() {
        return {
            isOpen: false,
            isLoading: false,
            penilai_karyawan: [],
            target_karyawan: [],
        }
    },
    created() {
        this.getPenilaiKaryawan();
        this.getTargetKaryawan();
    },
    mounted: function () {

    },
    methods: {
        getPenilaiKaryawan() {
            let username = this.username;

            axios.post('_component/data_penilaian_2.php', {
                request: "getPenilaiKaryawan",
                username: username,
            }).then(response => {
                this.isLoading = false;

                this.penilai_karyawan = response.data[0];
                this.getTargetKaryawan();

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getTargetKaryawan() {
            let id_num = this.penilai_karyawan.id_num;
            let kd_off = this.penilai_karyawan.kd_off;
            let kd_dept = this.penilai_karyawan.kd_dept;

            console.log(id_num);

            axios.post('_component/data_penilaian_2.php', {
                request: "getTargetKaryawan",
                id_num: id_num
            }).then(response => {
                this.isLoading = false;

                this.target_karyawan = response.data[0];
                console.log(this.target_karyawan);

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });

        },
        nilai_karyawan(id_target, id_level) {
            this.$emit('view_nilai_karyawan', [id_target, id_level]);
        }
    }
});

Vue.component('nilai_employee', {
    template:
        `
    <div>
        <h4>Nilai Karyawan</h4>
        </br>
        <div class="row">
            <div class="col-md-12" v-if="blank">
            </div>
            <div class="col-lg-12" v-if="penilaian">
                <div class="panel with-nav-tabs panel-default">
                    <div class="panel-heading">
                        <ul class="nav nav-pills nav-fill">
                            <template v-for="arr_indikator in tab_indikator">
                                <li class="nav-item col-md-2" v-bind:class="{'aktif':(v_indikator === arr_indikator.indikator)}"><a class="nav-link text-center" v-bind:class="{'text-aktif':(v_indikator === arr_indikator.indikator)}">{{ arr_indikator.indikator }}</a></li>
                            </template>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        </br>
        <div class="row">
            <div class="col-md-9" v-if="blank">
            </div>
            <div class="col-md-9" v-if="start">
                <div class="row">
                    <div class="col-md-6">
                        <h3>{{v_indikator}}</h3>
                    </div>
                    <div class="col-md-6">
                        <h3 class="pull-right">00:00</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align: center;">
                        <h4>Klik Mulai Untuk memulai Penilaian</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align: center;">
                        <button type="button" class="btn btn-primary btn-md" @click="start_penilaian">Mulai</button>
                    </div>
                </div>
            </div>
            <div class="col-md-9" v-if="penilaian">
                <div class="row">
                    <div class="col-md-6">
                        <h3>{{v_indikator}}</h3>
                    </div>
                    <div class="col-md-6">
                        <h3 class="pull-right">{{ prettyTime | prettify }}</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="text-align: center;">
                        <h3>{{ subIndikator.sub_indikator }}</h3>
                    </div>
                </div>
                </br>
                <div class="row">
                    <div class="col-md-2" style="margin-left: 110px;">
                        <label class="btn btn-default btn-circle btn-lg">
                            <input type="radio" name="answer1" v-model="v_nilai" value="1" class="custom-control-input">
                            <i class="" checked>1</i>
                        </label>
                        </br>
                        <label>Sangat Kurang</label>
                    </div>
                    <div class="col-md-2">
                        <label class="btn btn-default btn-circle btn-lg">
                            <input type="radio" name="answer1" v-model="v_nilai" value="2" class="custom-control-input">
                            <i class="" checked>2</i>
                        </label>
                        </br>
                        <label>Kurang</label>
                    </div>
                    <div class="col-md-2">
                        <label class="btn btn-default btn-circle btn-lg">
                            <input type="radio" name="answer1" v-model="v_nilai" value="3" class="custom-control-input">
                            <i class="" checked>3</i>
                        </label>
                        </br>
                        <label>Baik</label>
                    </div>
                    <div class="col-md-2">
                        <label class="btn btn-default btn-circle btn-lg">
                            <input type="radio" name="answer1" v-model="v_nilai" value="4" class="custom-control-input">
                            <i class="" checked>4</i>
                        </label>
                        </br>
                        <label>Sangat Baik</label>
                    </div>
                    <div class="col-md-2">
                        <label class="btn btn-default btn-circle btn-lg">
                            <input type="radio" name="answer1" v-model="v_nilai" value="5" class="custom-control-input">
                            <i class="" checked>5</i>
                        </label>
                        </br>
                        <label>Excellent</label>
                    </div>
                </div>
                </br>
                </br>
                <div class="row">
                    <div class="col-md-6">
                        <button type="button" class="btn btn-danger btn-md" @click="stop_penilaian">Stop</button>
                    </div>
                    <div class="col-md-6" v-if="btn_berikutnya">
                        <button type="button" class="btn btn-primary btn-md pull-right" @click="next_sub_indikator">Berikutnya</button>
                    </div>
                    <div class="col-md-6" v-if="btn_selesai">
                        <button type="button" class="btn btn-primary btn-md pull-right" @click="next_sub_indikator">Selesai</button>
                    </div>
                </div>
            </div>
            <div class="col-md-3" v-if="photo">
                <img :src="'https://web.pt-saa.com/img/hrm/emp/'+ employee.path_photo" alt="Photo" style="width: 150px; margin-left: 75px;" class="img-rounded text-center" v-if="employee.path_photo != null" />
                <img src="https://web.pt-saa.com/img/avatar/avatar.png" alt="Photo Belum Ada" class="img-rounded text-center" style="width: 150px; margin-left: 75px;" v-else>
                <h4 class="text-center" style="text-transform:capitalize;">{{employee.emp_name}}</h4>
                <h5 class="text-center">{{employee.position}}</h5>
                <h5 class="text-center">{{employee.nik}}</h5>
            </div>
        </div>
    </div>

    `,
    props: ['id_target', 'username'],
    data() {
        return {
            isOpen: false,
            isLoading: false,
            blank: true,
            start: false,
            penilaian: false,
            photo: false,
            employee: [],
            indikator: [],
            tab_indikator: [],
            v_total_indikator: '',
            v_index_indikator: 0,
            penilai_karyawan: [],
            v_indikator: '',
            v_id_indikator: '',
            isRunning: false,
            minutes: 0,
            secondes: 0,
            time: 300,
            timer: null,
            subIndikator: [],
            v_urutan: 0,
            v_max_urutan: 0,
            btn_berikutnya: true,
            btn_selesai: false,
            v_sub_indikator: '',
            v_id_sub_indikator: '',
            v_nilai: '',
            v_kd_indikator: '',
        }
    },
    filters: {
        prettify: function (value) {
            let data = value.split(':')
            let minutes = data[0]
            let secondes = data[1]
            if (minutes < 10) {
                minutes = "0" + minutes
            }
            if (secondes < 10) {
                secondes = "0" + secondes
            }
            return minutes + ":" + secondes
        }
    },
    computed: {
        prettyTime() {
            let time = this.time / 60
            let minutes = parseInt(time)
            let secondes = Math.round((time - minutes) * 60)
            return minutes + ":" + secondes
        }
    },
    created() {
        this.getDetailEmployee();
        this.getIndikatorPeriode();
        this.getFirstIndikator();
        this.getPenilaiKaryawan();
    },
    methods: {
        getPenilaiKaryawan() {
            let username = this.username;

            axios.post('_component/data_penilaian_2.php', {
                request: "getPenilaiKaryawan",
                username: username,
            }).then(response => {
                this.isLoading = false;

                this.penilai_karyawan = response.data[0];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getDetailEmployee() {
            axios.post('_component/data_penilaian_2.php', {
                request: "getDetailEmployee",
                id_num: this.id_target[0],
            }).then(response => {
                this.isLoading = false;

                this.employee = response.data[0];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getIndikatorPeriode() {
            axios.post('_component/data_penilaian_2.php', {
                request: "getIndikatorPeriodePenilaian",
                id_level: this.id_target[1],
            }).then(response => {
                this.isLoading = false;

                this.v_kd_indikator = response.data[0][0];
                this.getIndikatorKriteria(this.v_kd_indikator.kd_indikator);
                this.getFirstIndikator(this.v_kd_indikator.kd_indikator);

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getIndikatorKriteria(kd) {
            axios.post('_component/data_penilaian_2.php', {
                request: "getIndikatorKriteria",
                kd_indikator: kd
            }).then(response => {
                this.isLoading = false;

                this.v_total_indikator = response.data[0].length;
                this.tab_indikator = response.data[0];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getFirstIndikator(kd) {
            axios.post('_component/data_penilaian_2.php', {
                request: "getFirstIndikator",
                kd_indikator: kd,
            }).then(response => {
                this.isLoading = false;

                this.indikator = response.data[0];
                this.v_index_indikator = 0;

                this.v_indikator = this.indikator.indikator;

                this.blank = false;
                this.start = true;
                this.penilaian = false;
                this.photo = true;

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getSecondIndikator(kd_indikator, offset) {
            axios.post('_component/data_penilaian_2.php', {
                request: "getSecondIndikator",
                kd_indikator: kd_indikator,
                offset: offset,
            }).then(response => {
                this.isLoading = false;

                this.indikator = response.data[0];
                this.v_indikator = this.indikator.indikator;

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        get_indikator(id, x_indikator) {
            this.v_indikator = x_indikator;
            this.v_id_indikator = id;

            this.isRunning = false
            clearInterval(this.timer)
            this.timer = null

            this.time = 300
            this.secondes = 0
            this.minutes = 0

            this.blank = false;
            this.start = true;
            this.penilaian = false;
            this.photo = true;
        },
        start_penilaian() {
            this.isRunning = true;
            if (!this.timer) {
                this.timer = setInterval(() => {
                    if (this.time > 0) {
                        this.time--
                    } else {
                        clearInterval(this.timer)
                        this.stop_penilaian()
                    }
                }, 1000)
            }

            this.getRandomFirstSubIndikator(this.indikator.id_indikator_kriteria);

            this.blank = false;
            this.start = false;
            this.penilaian = true;
            this.photo = true;
        },
        stop_penilaian() {
            this.isRunning = false
            clearInterval(this.timer)
            this.timer = null

            this.time = 300
            this.secondes = 0
            this.minutes = 0

            this.blank = false;
            this.start = true;
            this.penilaian = false;
            this.photo = true;
        },
        getRandomFirstSubIndikator(id) {
            axios.post('_component/data_penilaian_2.php', {
                request: "getRandomFirstSubIndikator",
                id_indikator: id,
            }).then(response => {
                this.isLoading = false;

                this.subIndikator = response.data[0];
                this.v_urutan = 1;

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        next_sub_indikator() {
            this.getRandomSecondSubIndikator(this.indikator.id_indikator_kriteria, this.v_kd_indikator.kd_indikator);
        },
        async getRandomSecondSubIndikator(id_indikator, kd_indikator) {

            if (this.v_nilai == "") {
                alert("Pilih Nilai Terlebih Dahulu");
            } else {

                axios.post('_component/data_penilaian_2.php', {
                    request: "savePenilaian",
                    id_num_penilai: this.penilai_karyawan.id_num,
                    id_num_target: this.id_target[1],
                    id_indikator: this.subIndikator.id_indikator,
                    id_sub_indikator: this.subIndikator.id_sub_indikator,
                    bobot: this.subIndikator.bobot,
                    nilai: this.v_nilai,
                }).then(response => {
                    this.isLoading = false;
                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });

                console.log(this.penilai_karyawan.id_num);
                console.log(this.id_target[1]),
                    console.log(this.subIndikator.id_indikator);
                console.log(this.subIndikator.id_sub_indikator);
                console.log(this.subIndikator.bobot);
                console.log(this.v_nilai);

                this.v_urutan = this.v_urutan + 1;

                await axios.post('_component/data_penilaian_2.php', {
                    request: "getIndikatorKriteriaById",
                    id_indikator: id_indikator,
                    kd_indikator: kd_indikator,
                }).then(response => {
                    this.isLoading = false;

                    this.v_max_urutan = response.data[0];

                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });

                if (this.v_urutan == this.v_max_urutan.max_urutan) {
                    this.btn_berikutnya = false;
                    this.btn_selesai = true;
                } else {
                    this.btn_berikutnya = true;
                    this.btn_selesai = false;
                }

                if (this.v_urutan <= this.v_max_urutan.max_urutan) {

                    axios.post('_component/data_penilaian_2.php', {
                        request: "getRandomSecondSubIndikator",
                        urutan: this.v_urutan,
                        id_indikator: id_indikator,
                    }).then(response => {
                        this.isLoading = false;

                        this.subIndikator = response.data[0];

                        this.v_id_sub_indikator = '';

                    }).catch(error => {
                        console.log(error);
                        this.isLoading = false;
                    });
                } else {
                    this.v_max_urutan = 0;
                    this.v_urutan = 0;

                    this.v_index_indikator = this.v_index_indikator + 1;
                    this.getSecondIndikator(kd_indikator, this.v_index_indikator);

                    if (this.v_index_indikator < this.v_total_indikator) {
                        this.blank = false;
                        this.start = false;
                        this.penilaian = true;
                        this.photo = true;

                        this.isRunning = true;
                        if (!this.timer) {
                            this.timer = setInterval(() => {
                                if (this.time > 0) {
                                    this.time--
                                } else {
                                    clearInterval(this.timer)
                                    this.stop_penilaian()
                                }
                            }, 1000)
                        }

                    } else {
                        this.blank = true;
                        this.start = false;
                        this.penilaian = false;
                        this.photo = true;
                    }
                }

            }
        },
    }

});

// LIST INDIKATOR START
Vue.component('list_indikator', {
    template: `
    <div><transition name="fade">
        <div style="transition-duration: 0.15s;" class="nav-tabs-custom" v-if="isOpen">
            <div class="tab-content" style="border-top: 1px solid #eee;">
                <h3 style="margin-bottom: -10px;margin-top: 0px;"> Materi Penilaian</h3>
                <button class="btn btn-info btn-sm pull-right" @click="btn_add_indikator" style="margin-top:-20px"><i class="fa fa-plus"></i></button>
                <hr />
                <div class="row">
                    <div class="col-md-12" v-if="view_add_indikator_periode">
                        <div style="background-color: rgb(221, 221, 221); border-radius: 5px; padding: 25px 40px; margin-bottom: 50px;">
                            <form id="form" @submit.prevent="saveIndikatorPeriode" method="post" class="form-horizontal">
                                <div class="row">
                                    <div class="col-sm-4 pull-right">
                                        <button type="button" @click="btn_close_add_indikator" class="btn btn-danger pull-right">X</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Nomor Penilaian</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <input type="text" class="form-control" v-model="nopenilaian" required readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Level</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <v-select style="text-transform: capitalize" label="level" :options="level" v-model="v_level" require></v-select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </br>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Periode</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <vuejs-datepicker
                                                    v-model="v_periode"
                                                    :value="Date"
                                                    :format="DatePickerFormata"
                                                    :disabledDates="disabledDates"
                                                    :bootstrap-styling="true"
                                                    :placeholder="holdera"
                                                    :minimum-view="minv">
                                                </vuejs-datepicker>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Expired</label>
                                            </div>
                                            <div class="col-sm-6">
                                                <vuejs-datepicker
                                                    v-model="v_expired"
                                                    :format="DatePickerFormatb"
                                                    :disabledDates="disabledDates"
                                                    :bootstrap-styling="true"
                                                    :placeholder="holderb">
                                                </vuejs-datepicker>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </br>
                                <table class="table table-sm">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>#</th>
                                            <th>Indikator</th>
                                            <th>Persentase</th>
                                            <th>Maksimal Pertanyaan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(arr_indikator, k) in data_indikator" :key="k">
                                            <td scope="row" class="trashIconContainer">
                                                <button class="btn btn-warning btn-sm"  @click="deleteRow(k,arr_indikator)"><i class="fa fa-trash"></i></button>
                                            </td>
                                            <td>
                                                <input class="form-control" type="text" v-model="arr_indikator.indikator" placeholder="Masukkan Indikator" required/>
                                            </td>
                                            <td>
                                                <input class="form-control" type="number" v-model="arr_indikator.persentase" @change="calculateLineTotal(arr_indikator)" placeholder="Masukkan Persentase" required/>
                                            </td>
                                            <td>
                                                <input class="form-control" type="number" v-model="arr_indikator.max_urutan" placeholder="Masukkan Maksimal Pertanyaan" required/>
                                            </td>
                                            <td>
                                                <input readonly class="form-control text-right" type="hidden" min="0" step=".01" v-model="arr_indikator.line_total" required/>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td>
                                                <button type='button' class="btn btn-info btn-sm" :disabled="totalBobot > 100" @click="addNewRow">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                                <td></td>
                                                <td></td>
                                                <td class="text-right">Total Bobot &nbsp;&nbsp; {{ totalBobot }}</td>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <div class="row">
                                    <div class="col-md-9"></div>
                                    <div class="col-md-3">
                                        <transition name="fade">
                                            <div style="transition-duration: 4s;" class="alert alert-danger text-center" role="alert" v-if="totalBobot > 100">
                                                    <strong>Warning!</strong> {{ error }}.
                                            </div>
                                        </transition>
                                    </div>
                                </div>
                                <button class="btn btn-primary text-right" :disabled="totalBobot > 100" type="submit">Simpan</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="lds-facebook" v-if="isLoading"><div></div><div></div><div></div></div>
                        <v-client-table :data="indikator_periode" :columns="columns" :options="options"></v-client-table>
                    </div>
                </div>
            </div>
        </div>
    </transition></div>
    `,
    props: ['username'],
    components: {
        vuejsDatepicker
    },
    data() {
        return {
            columns: ['kd_indikator', 'level', 'periode_month', 'expired',
                'status_active', 'actived_by', 'active_date', 'action_indikator'],
            options: {
                templates: {
                    action_indikator: "action_indikator_component",
                    status_active: "status_indikator_component"
                },
                headings: {
                    kd_indikator: "No. Penilaian",
                    level: "Level",
                    periode_month: "Periode",
                    expired: "Expired",
                    status_active: "Status",
                    actived_by: "Active By",
                    active_date: "Active Date",
                    action_indikator: "#",
                },
                texts: {
                    count: ''
                },
                sortable: ['kd_indikator', 'level', 'periode_month', 'expired',
                    'status_active', 'actived_by', 'active_date',]
            },
            isOpen: true,
            isLoading: false,
            indikator_periode: [],
            totalBobot: 0,
            error: '',
            data_indikator: [{
                indikator: '',
                persentase: '',
                max_urutan: '',
                line_total: 0,
            }],
            level: [],
            v_indikator: '',
            v_persentase: '',
            v_max_urutan: '',
            v_kode_indikator: '',
            v_level: '',
            v_id_level: '',
            v_periode: '',
            v_expired: '',
            v_id_indikator_periode: '',
            view_add_indikator_periode: false,
            btn_add_indikator_periode: true,
            view_edit_indikator_periode: false,
            view_setting_indikator_detail: false,
            nomorchar: "HRD",
            nomorID: '',
            holdera: "-- Pilih Bulan --",
            holderb: "-- Pilih Tanggal --",
            DatePickerFormata: 'MMMM yyyy',
            DatePickerFormatb: 'dd MMMM yyyy',
            disabledDates: {
                to: new Date(Date.now() - 8640000)
            },
            startmonth: '',
            enddate: '',
            minv: 'month',
            monthNames: [
                { value: "1", text: "January" },
                { value: "2", text: "February" },
                { value: "3", text: "March" },
                { value: "4", text: "April" },
                { value: "5", text: "May" },
                { value: "6", text: "June" },
                { value: "7", text: "July" },
                { value: "8", text: "August" },
                { value: "9", text: "September" },
                { value: "10", text: "October" },
                { value: "11", text: "November" },
                { value: "12", text: "December" }
            ],
            v_change: '',
            view_kd_indikator: '',
            view_periode: '',
            view_expired: '',
            v_detail_indikator_periode: [],
            v_detail_indikator_kriteria: [],
            v_detail_id_indikator: '',
            v_detail_kd_indikator: '',
            v_detail_indikator: '',
            v_detail_persentase: '',
            v_detail_max_urutan: '',
            v_detail_id_indikator_kriteria: '',
            v_total_persentase: '',
            view_id_indikator_kriteria: '',
            view_indikator_kriteria: '',
            view_indikator_max_urutan: '',
            v_kriteria_id_detail_indikator_kriteria: '',
            v_kriteria_urutan: '',
            v_kriteria_bobot: '',
        }
    },
    created() {
        this.getIndikatorPeriode();
        this.getLevel();
        this.nomor();
        this.callFunction();
        EventBus.$on('open_list_indikator', () => {
            this.isOpen = true;
        })
        EventBus.$on("refreshListIndikator", (list_indikator) => {
            this.isOpen = list_indikator
        })
        EventBus.$on("list_indikator", (status, id_level, id_indikator_periode) => {
            this.change_status_indikator_periode(status, id_level, id_indikator_periode)
        })
        EventBus.$on('tutup_list_indikator', (closed) => {
            this.isOpen = closed
        })
        EventBus.$on('close_materi_penilaian', () => {
            this.isOpen = false
        });
        // EventBus.$on('close_laporan', (close) => {
        //     this.isOpen = close;
        // });
        // EventBus.$on('close_laporan_detail', (close) => {
        //     this.isOpen = close;
        // });
    },
    methods: {
        empty_form() {
            this.v_id_indikator_periode = "";
            this.v_level = "";
            this.v_id_level = "";
            this.v_periode = "";
            this.v_expired = "";
            this.v_change = "";
        },
        btn_add_indikator() {
            this.btn_add_indikator_periode = false;
            this.view_edit_indikator_periode = false;
            this.view_setting_indikator_detail = false;
            this.view_add_indikator_periode = true;

            this.empty_form();
        },
        btn_detail_indikator(id, kd_indikator) {
            this.view_add_indikator_periode = false;
            this.btn_add_indikator_periode = false;
            this.view_setting_indikator_detail = false;
            this.view_edit_indikator_periode = true;

            this.getIndikatorPeriodeByID(id);
            this.getDetailIndikatorPeriode(kd_indikator);
            // console.log("masuk")
        },
        btn_setting_detail_indikator(id) {
            this.view_add_indikator_periode = false;
            this.btn_add_indikator_periode = false;
            this.view_edit_indikator_periode = false;
            this.view_setting_indikator_detail = true;

            this.getDetailIndikatorKriteria(id);
        },
        btn_close_add_indikator() {
            this.view_add_indikator_periode = false;
            this.view_edit_indikator_periode = false;
            this.view_setting_indikator_detail = false;
            this.btn_add_indikator_periode = true;
        },
        btn_close_edit_indikator() {
            this.view_add_indikator_periode = false;
            this.view_edit_indikator_periode = false;
            this.view_setting_indikator_detail = false;
            this.btn_add_indikator_periode = true;
        },
        btn_detail_pertanyaan(kd) {
            this.$emit('view_sub_indikator', [kd]);
        },
        addNewRow() {
            this.data_indikator.push({
                indikator: '',
                persentase: '',
                max_urutan: '',
                line_total: 0
            });
        },
        deleteRow(index, data_indikator) {
            var idx = this.data_indikator.indexOf(data_indikator);
            if (idx > -1) {
                this.data_indikator.splice(idx, 1);
            }
            this.calculateTotal();
        },
        calculateTotal() {
            var total;
            total = this.data_indikator.reduce(function (sum, product) {
                var lineTotal = parseFloat(product.line_total);
                if (!isNaN(lineTotal)) {
                    return sum + lineTotal;
                }
            }, 0);
            this.totalBobot = total;
            if (this.totalBobot > 100) {
                this.error = 'Total Persentase Maksimal = 100 %';
            }
            if (!this.error) {
                return true;
            }
        },
        calculateLineTotal(data_indikator) {
            var total = parseFloat(data_indikator.persentase);
            if (!isNaN(total)) {
                data_indikator.line_total = total;
            }
            this.calculateTotal();
        },
        async saveIndikatorPeriode() {

            let check_kd = '';
            let id_indikator = '';

            let username = this.username;
            let nopenilaian = this.nopenilaian;
            let id_level = this.v_level.id_level;
            let periode = this.v_periode;
            let expired = this.v_expired;

            await axios.post('_component/data_penilaian_2.php', {
                request: "checkKdIndikator",
                nopenilaian: nopenilaian,
            }).then(response => {
                this.isLoading = false;

                check_kd = response.data[0];

                if (check_kd.kd_indikator == null) {

                    axios.post('_component/data_penilaian_2.php', {
                        request: "saveIndikatorPeriode",
                        nopenilaian: nopenilaian,
                        id_level: id_level,
                        periode: periode,
                        expired: expired
                    }).then(response => {
                        this.isLoading = false;
                    }).catch(error => {
                        console.log(error);
                        this.isLoading = false;
                    });

                    axios.post('_component/data_penilaian_2.php', {
                        request: "SaveIndikator",
                        username: username,
                        nopenilaian: nopenilaian,
                        id_level: id_level
                    }).then(response => {
                        this.isLoading = false;

                        id_indikator = response.data[0].id_indikator;

                        axios.post('_component/data_penilaian_2.php', {
                            request: "SaveIndikatorKriteria",
                            id_indikator: id_indikator,
                            kriteria_indikator: JSON.stringify(this.data_indikator),
                        }).then(response => {
                            this.isLoading = false;

                            console.log(id_indikator);
                            alert("Data Penilaian Berhasil Ditambah")
                            document.location.reload(true);

                        }).catch(error => {
                            console.log(error);
                            this.isLoading = false;
                        });

                    }).catch(error => {
                        console.log(error);
                        this.isLoading = false;
                    });

                } else {
                    alert("Nomor Penilaian Sudah Terpakai");
                }

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        async change_status_indikator_periode(status, id_level, id_indikator_periode) {
            console.log(status + "," + id_level + "," + id_indikator_periode);
            let lenght_indikator_periode = '';
            let level = '';

            if (status == 1) {

                axios.post('_component/data_penilaian_2.php', {
                    request: "checkAllStatusIndikatorPeriode",
                    id_level: id_level,
                    id_indikator_periode: id_indikator_periode,
                }).then(response => {
                    this.isLoading = false;

                    lenght_indikator_periode = response.data[0].length;
                    level = response.data[0].level;
                    console.log(response.data[0].level);

                    if (lenght_indikator_periode < 1) {
                        axios.post('_component/data_penilaian_2.php', {
                            request: "UpdateStatusIndikatorPeriode",
                            status: status,
                            id_level: id_level,
                            id_indikator_periode: id_indikator_periode,
                            username: this.username,
                        }).then(response => {
                            this.isLoading = false;

                            alert("Nomor Penilaian Berhasil Di Aktifkan");
                            this.getIndikatorPeriode();

                        }).catch(error => {
                            console.log(error);
                            this.isLoading = false;
                        });
                    } else {
                        alert("Nomor Penilaian " + level + " yang aktif sudah ada! Mohon menonaktifkan terlebih dahulu sebelum aktifkan kembali!");
                    }


                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });

            } else {

                axios.post('_component/data_penilaian_2.php', {
                    request: "UpdateStatusIndikatorPeriode",
                    status: status,
                    id_level: id_level,
                    id_indikator_periode: id_indikator_periode,
                    username: this.username,
                }).then(response => {
                    this.isLoading = false;

                    alert("Nomor Penilaian Berhasil Di Non Aktifkan");
                    this.getIndikatorPeriode();

                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });

            }
        },
        getLevel() {
            level: [],

                axios.post('_component/data_penilaian_2.php', {
                    request: "getLevel",
                }).then(response => {
                    this.isLoading = false;

                    this.level = response.data[0];

                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });
        },
        getIndikatorPeriode() {
            this.isLoading = true;

            axios.post('_component/data_penilaian_2.php', {
                request: "getIndikatorPeriode",
            }).then(response => {
                this.isLoading = false;

                this.indikator_periode = response.data[0];
                // console.log(this.indikator_periode);

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        saveIndikator() {
            let id_level = this.v_level.id_level;
            let indikator = this.v_indikator;
            let persentase = this.v_persentase;
            let max_urutan = this.v_max_urutan;

            axios.post('_component/data_penilaian_2.php', {
                request: "SaveIndikator",
                username: this.username,
                id_level: id_level,
                indikator: indikator,
                persentase: persentase,
                max_urutan: max_urutan,
            }).then(response => {
                this.isLoading = false;

                alert("Data Indikator Berhasil Di Tambah");
                $('#add_indikator').modal('hide');
                this.v_indikator = '';
                this.v_persentase = '';
                this.v_max_urutan = '';
                this.getIndikator();

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getIndikatorPeriodeByID(id) {
            axios.post('_component/data_penilaian_2.php', {
                request: "getIndikatorPeriodeById",
                id_indikator_periode: id,
            }).then(response => {
                this.isLoading = false;

                this.v_id_indikator_periode = response.data[0].id_indikator_periode;
                this.v_level = response.data[0].level;
                this.v_id_level = response.data[0].id_level;
                this.v_periode = response.data[0].periode_month;
                this.v_expired = response.data[0].expired;

                this.view_kd_indikator = response.data[0].kd_indikator;
                this.view_periode = response.data[0].periode_format;
                this.view_expired = response.data[0].expired_format;

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        updateIndikatorPeriode() {
            let id_level = '';

            if (this.v_change == 1) {
                id_level = this.v_level.id_level;
                this.v_change = '';
            } else {
                id_level = this.v_id_level;
                this.v_change = '';
            }

            axios.post('_component/data_penilaian_2.php', {
                request: "updateIndikatorPeriode",
                id_indikator_periode: this.v_id_indikator_periode,
                id_level: id_level,
                periode: this.v_periode,
                expired: this.v_expired
            }).then(response => {
                this.isLoading = false;

                alert("Data Berhasil Di Update");
                $('#edit_indikator_periode').modal('hide');

                this.v_id_indikator_periode = "";
                this.v_level = "";
                this.v_id_level = "";
                this.v_periode = "";
                this.v_expired = "";
                this.v_change = '';

                this.getIndikatorPeriode();

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        deleteIndikatorPeriode(kd) {
            let detail_lenght = '';

            if (confirm("Do you really want to delete?")) {

                axios.post('_component/data_penilaian_2.php', {
                    request: "getDetailIndikatorPeriode",
                    kd_indikator: kd,
                }).then(response => {
                    this.isLoading = false;

                    detail_lenght = response.data[0].length;

                    if (detail_lenght < 1) {
                        console.log(detail_lenght + " bisa boss");
                        axios.post('_component/data_penilaian_2.php', {
                            request: "deleteIndikatorPeriode",
                            kd_indikator: kd,
                        }).then(response => {
                            this.isLoading = false;
                            alert("Data Berhasil Di Hapus");
                            this.getIndikatorPeriode();
                        }).catch(error => {
                            console.log(error);
                            this.isLoading = false;
                        });
                    } else {
                        alert("Terdapat Data Detail Pada Nomor Penilaian, Nomor Penilaian Gagal Di Hapus ");
                    }

                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });

            }

        },
        getDetailIndikatorPeriode(kd_indikator) {
            axios.post('_component/data_penilaian_2.php', {
                request: "getDetailIndikatorPeriode",
                kd_indikator: kd_indikator,
            }).then(response => {
                this.isLoading = false;

                this.v_detail_indikator_periode = response.data[0];
                this.v_lenght_detail_indikator_periode = this.v_detail_indikator_periode.length;

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        checkSaveDetailIndikator(kd) {

            this.v_detail_indikator = '';
            this.v_detail_persentase = '';
            this.v_detail_max_urutan = '';

            let kd_indikator = kd;

            console.log(kd_indikator);

            axios.post('_component/data_penilaian_2.php', {
                request: "checkTotalPersentaseDetailIndikator",
                kd_indikator: kd,
            }).then(response => {
                this.isLoading = false;

                this.v_total_persentase = response.data[0].total_persentase;
                this.v_detail_id_indikator = response.data[1].id_indikator;
                this.v_detail_kd_indikator = response.data[1].kd_indikator;

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        saveDetailIndikator() {

            total_persentase = parseInt(this.v_total_persentase) + parseInt(this.v_detail_persentase);

            let detail_id_indikator = this.v_detail_id_indikator;
            let detail_kd_indikator = this.v_detail_kd_indikator;
            let detail_indikator = this.v_detail_indikator;
            let detail_persentase = this.v_detail_persentase;
            let detail_max_urutan = this.v_detail_max_urutan;

            if (total_persentase <= 100) {
                axios.post('_component/data_penilaian_2.php', {
                    request: "saveDetailIndikatorByKode",
                    detail_id_indikator: detail_id_indikator,
                    detail_indikator: detail_indikator,
                    detail_persentase: detail_persentase,
                    detail_max_urutan: detail_max_urutan
                }).then(response => {
                    this.isLoading = false;

                    $('#add_detail_indikator').modal('hide');

                    this.view_add_indikator_periode = false;
                    this.btn_add_indikator_periode = false;
                    this.view_setting_indikator_detail = false;
                    this.view_edit_indikator_periode = true;

                    this.getIndikatorPeriode();
                    this.getDetailIndikatorPeriode(detail_kd_indikator);

                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });
            } else {
                alert("Gagal Menambahkan Indikator, Total Persentase Diatas 100 %");
            }

        },
        async editDetailIndikator(id) {
            console.log(id);

            await axios.post('_component/data_penilaian_2.php', {
                request: "getDetailIndikatorById",
                id_indikator_kriteria: id,
            }).then(response => {
                this.isLoading = false;

                this.v_detail_indikator = response.data[0].indikator;
                this.v_detail_persentase = response.data[0].persentase;
                this.v_detail_max_urutan = response.data[0].max_urutan;
                this.v_detail_id_indikator_kriteria = response.data[0].id_indikator_kriteria;
                this.v_detail_kd_indikator = response.data[0].kd_indikator;

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });

            await axios.post('_component/data_penilaian_2.php', {
                request: "checkTotalPersentaseDetailIndikatorEdit",
                kd_indikator: this.v_detail_kd_indikator,
                id_indikator_kriteria: this.v_detail_id_indikator_kriteria,
            }).then(response => {
                this.isLoading = false;

                this.v_total_persentase = response.data;

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        updateDetailIndikator() {

            let total_1 = '';
            let total_2 = '';
            let temp_persentase = '';
            let total_persentase = '';

            total_1 = parseInt(this.v_total_persentase[0].total_persentase);
            total_2 = parseInt(this.v_total_persentase[1].persentase);

            temp_persentase = total_1 - total_2;
            total_persentase = temp_persentase + parseInt(this.v_detail_persentase);

            if (total_persentase <= 100) {
                axios.post('_component/data_penilaian_2.php', {
                    request: "updateDetailIndikatorByKode",
                    id_indikator_kriteria: this.v_detail_id_indikator_kriteria,
                    indikator: this.v_detail_indikator,
                    persentase: this.v_detail_persentase,
                    max_urutan: this.v_detail_max_urutan
                }).then(response => {
                    this.isLoading = false;

                    $('#edit_detail_indikator').modal('hide');

                    this.view_add_indikator_periode = false;
                    this.btn_add_indikator_periode = false;
                    this.view_setting_indikator_detail = false;
                    this.view_edit_indikator_periode = true;

                    this.getIndikatorPeriode();
                    this.getDetailIndikatorPeriode(this.v_detail_kd_indikator);

                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });
            } else {
                alert("Gagal Menambahkan Indikator, Total Persentase Diatas 100 %");
            }

        },
        deleteDetailIndikator(id) {
            let detail_kriteria_lenght = [];

            axios.post('_component/data_penilaian_2.php', {
                request: "getDetailIndikatorById",
                id_indikator_kriteria: id,
            }).then(response => {
                this.isLoading = false;

                this.v_detail_indikator = response.data[0].indikator;
                this.v_detail_persentase = response.data[0].persentase;
                this.v_detail_max_urutan = response.data[0].max_urutan;
                this.v_detail_id_indikator_kriteria = response.data[0].id_indikator_kriteria;
                this.v_detail_kd_indikator = response.data[0].kd_indikator;

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });

            if (confirm("Do you really want to delete?")) {
                axios.post('_component/data_penilaian_2.php', {
                    request: "getDetailIndikatorKriteria",
                    id_indikator_kriteria: id,
                }).then(response => {
                    this.isLoading = false;

                    detail_kriteria_lenght = response.data[0].length;

                    console.log(detail_kriteria_lenght);

                    if (detail_kriteria_lenght < 1) {
                        axios.post('_component/data_penilaian_2.php', {
                            request: "deleteDetailIndikatorKriteria",
                            id_indikator_kriteria: id,
                        }).then(response => {
                            this.isLoading = false;
                            alert("Data Berhasil Di Hapus");

                            this.view_add_indikator_periode = false;
                            this.btn_add_indikator_periode = false;
                            this.view_setting_indikator_detail = false;
                            this.view_edit_indikator_periode = true;

                            this.getIndikatorPeriode();
                            this.getDetailIndikatorPeriode(this.v_detail_kd_indikator);
                        }).catch(error => {
                            console.log(error);
                            this.isLoading = false;
                        });
                    } else {
                        alert("Terdapat Data Detail Pada Nomor Penilaian, Nomor Penilaian Gagal Di Hapus ");
                    }

                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });
            }
        },
        empty_data_detail_indikator_kriteria() {
            this.v_kriteria_urutan = '';
            this.v_kriteria_bobot = '';
        },
        getDetailIndikatorKriteria(id) {
            axios.post('_component/data_penilaian_2.php', {
                request: "getDetailIndikatorKriteria",
                id_indikator_kriteria: id,
            }).then(response => {
                this.isLoading = false;

                this.v_detail_indikator_kriteria = response.data[0];
                this.view_id_indikator_kriteria = response.data[1].id_indikator_kriteria;
                this.view_indikator_kriteria = response.data[1].indikator;
                this.view_indikator_max_urutan = response.data[1].max_urutan;

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        saveDetailIndikatorKriteria() {

            axios.post('_component/data_penilaian_2.php', {
                request: "checkSaveDetailIndikatorKriteria",
                id_indikator_kriteria: this.view_id_indikator_kriteria,
                urutan: this.v_kriteria_urutan,
            }).then(response => {
                this.isLoading = false;

                max_urutan = this.view_indikator_max_urutan;
                total_row = response.data[0].length;
                row_with_urutan = response.data[1].length;

                if (total_row < max_urutan) {
                    if (row_with_urutan == 0) {

                        axios.post('_component/data_penilaian_2.php', {
                            request: "saveDetailIndikatorKriteria",
                            id_indikator_kriteria: this.view_id_indikator_kriteria,
                            urutan: this.v_kriteria_urutan,
                            bobot: this.v_kriteria_bobot,
                        }).then(response => {
                            this.isLoading = false;

                            $('#add_detail_indikator_kriteria').modal('hide');

                            alert("Data Berhasil Ditambah");

                            this.view_add_indikator_periode = false;
                            this.btn_add_indikator_periode = false;
                            this.view_edit_indikator_periode = false;
                            this.view_setting_indikator_detail = true;

                            this.getDetailIndikatorKriteria(this.view_id_indikator_kriteria);

                        }).catch(error => {
                            console.log(error);
                            this.isLoading = false;
                        });

                    } else {
                        alert("Data Dengan Urutan " + this.v_kriteria_urutan + " Sudah Ada");
                    }
                } else {
                    alert("Data Sudah Maksimal Sesuai Maksimal Urutan");
                }

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        editDetailIndikatorKriteria(id) {

            axios.post('_component/data_penilaian_2.php', {
                request: "getDetailIndikatorKriteriaByID",
                id_indikator_kriteria: this.view_id_indikator_kriteria,
                id_detail_indikator_kriteria: id,
            }).then(response => {
                this.isLoading = false;

                this.v_kriteria_id_detail_indikator_kriteria = response.data[0].id_detail_indikator_kriteria;
                this.v_kriteria_urutan = response.data[0].urutan;
                this.v_kriteria_bobot = response.data[0].bobot;

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });

        },
        updateDetailIndikatorKriteria() {

            axios.post('_component/data_penilaian_2.php', {
                request: "checkUpdateDetailIndikatorKriteria",
                id_indikator_kriteria: this.view_id_indikator_kriteria,
                id_detail_indikator_kriteria: this.v_kriteria_id_detail_indikator_kriteria,
                urutan: this.v_kriteria_urutan,
            }).then(response => {
                this.isLoading = false;

                row_with_urutan = response.data[0].length;

                if (row_with_urutan == 0) {

                    axios.post('_component/data_penilaian_2.php', {
                        request: "updateDetailIndikatorKriteria",
                        id_detail_indikator_kriteria: this.v_kriteria_id_detail_indikator_kriteria,
                        urutan: this.v_kriteria_urutan,
                        bobot: this.v_kriteria_bobot,
                    }).then(response => {
                        this.isLoading = false;

                        $('#edit_detail_indikator_kriteria').modal('hide');

                        alert("Data Berhasil Di Update");

                        this.view_add_indikator_periode = false;
                        this.btn_add_indikator_periode = false;
                        this.view_edit_indikator_periode = false;
                        this.view_setting_indikator_detail = true;

                        this.getDetailIndikatorKriteria(this.view_id_indikator_kriteria);

                    }).catch(error => {
                        console.log(error);
                        this.isLoading = false;
                    });

                } else {
                    alert("Data Dengan Urutan " + this.v_kriteria_urutan + " Sudah Ada");
                }

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });

        },
        deleteDetailIndikatorKriteria(id) {

            if (confirm("Do you really want to delete?")) {

                axios.post('_component/data_penilaian_2.php', {
                    request: "deleteDetailIndikatorKriteria",
                    id_detail_indikator_kriteria: id
                }).then(response => {
                    this.isLoading = false;

                    alert("Data Berhasil Di Hapus");

                    this.view_add_indikator_periode = false;
                    this.btn_add_indikator_periode = false;
                    this.view_edit_indikator_periode = false;
                    this.view_setting_indikator_detail = true;

                    this.getDetailIndikatorKriteria(this.view_id_indikator_kriteria);

                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });

            }
        },
        changeLevel() {
            this.v_change = 1;
        },
        nomor() {
            axios.post('_component/data_penilaian_2.php', {
                request: "generate_nomor"
            }).then(response => {
                this.nomorID = response.data;
            }).catch(error => {
                console.log(error);
            });
        },
        customFormatter(date) {
            return moment(date, 'YYYY-MM-Do h:mm:ss a');
        },
        callFunction() {

            var currentDate = new Date();
            // console.log(currentDate);

            var currentDateWithFormat = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
            // console.log(currentDateWithFormat);

            var bulanini = currentDate.getMonth();
            var tahunini = currentDate.getFullYear();
            // console.log(bulanini);
            var tahun = [];
            for (i = 0; i < 2; i++) {
                tahun.push(tahunini + i);
            }
            this.optionsYear = tahun;
            this.selectedYear = tahunini;
            this.selectedMonth = bulanini + 1;
            // console.log(tahun);
        },
    },
    computed: {
        nopenilaian() {
            var thn = new Date().getFullYear();
            var countNomor = (' ' + this.nomorID).length - 1;
            //console.log(countNomor, thn);

            if (countNomor < 2) {
                zero = "000";
            } else if (countNomor < 3) {
                zero = "00";
            } else {
                zero = "0";
            }
            return nopenilaian = this.nomorchar + thn + zero + this.nomorID;
        }
    }

});

Vue.component('status_indikator_component', {
    props: ["data", "columns", "index"],
    template: `
        <span v-if="data.status_active === '1'" style="color: green; font-weight: bold;">Aktif</span>
        <span v-else style="color: red; font-weight: bold;">Tidak Aktif</span>
    `
})

Vue.component('action_indikator_component', {
    props: ["data", "index", "columns"],
    template: `
        <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
            <div class="btn-group mr-2" role="group" aria-label="First group">
                <button type="button" @click="detail_indikator(data.id_indikator_periode, data.kd_indikator)" class="btn btn-sm btn-primary">Detail</button>
            </div>
            <div class="btn-group mr-2" role="group" aria-label="Second group">
                <button id="gantung" type="button" @click="btn_detail_pertanyaan(data.kd_indikator)" class="btn btn-sm btn-primary" :disabled="data.status_active == 0">Lihat Pertanyaan</button>
            </div>
            <div class="btn-group mr-2" role="group" aria-label="Third group">
                <button type="button" v-if="data.status_active === '1'" @click="changeStatus(0, data.id_level, data.id_indikator_periode)" class="btn btn-sm btn-danger"><a class="fa fa-close" style="color: white;"></a></button>
                <button type="button" v-else @click="changeStatus(1, data.id_level, data.id_indikator_periode)" class="btn btn-sm btn-success"><a class="fa fa-check" style="color: white;"></a></button>
            </div>
        </div>
    `,
    data() {
        return {
            closed: false,
            open: true
        }
    },
    methods: {
        detail_indikator(id, kd_indikator) {
            EventBus.$emit("detail_indikator_component", this.open, id, kd_indikator)
        },
        changeStatus(status, id_level, id_indikator_periode) {
            EventBus.$emit("list_indikator", status, id_level, id_indikator_periode)
        },
        btn_detail_pertanyaan(kd) {
            EventBus.$emit('sub_list_indikator', kd, this.open);
            EventBus.$emit('tutup_list_indikator', this.closed);
        },
    }
})

// DETAIL INDIKATOR 
Vue.component("detail_indikator_component", {
    template: `
    <div><transition name="fade">
        <div style="transition-duration: 0.15s;" class="nav-tabs-custom" v-if="isShow">
            <div class="tab-content" style="border-top: 1px solid #eee;">
                <h3 style="margin-bottom: -10px;margin-top: 0px;"> Detail Indikator</h3>
                <hr />
                <div class="row">
                    <div class="col-md-12">
                        <div style="background-color: rgb(221, 221, 221); border-radius: 5px; padding: 25px 40px; margin-bottom: 50px;">
                            <div class="row">
                                <div class="col-sm-4 pull-right">
                                    <button type="button" @click="btn_close_edit_indikator" class="btn btn-danger pull-right">X</button>
                                </div>
                            </div>
                            </br>
                            <table class="table table-condensed table-bordered table-hover">
                                <thead>
                                    <tr v-if="isLoading">
                                        <td colspan="9">
                                            <div class="lds-facebook"><div></div><div></div><div></div></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="col" colspan="5">Detail Nomor Penilaian</th>
                                    </tr>
                                    <tr>
                                        <th scope="col" colspan="5">
                                            <button type="button" 
                                            data-toggle='modal'  
                                            data-target='#edit_indikator_periode' 
                                            class="btn btn-primary pull-right">Edit Level</button>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th scope="col">Nomor Penilaian : {{ view_kd_indikator }}</th>
                                        <th scope="col">Level : {{ v_level }}</th>
                                        <th scope="col">Periode : {{ view_periode }}</th>
                                        <th scope="col"colspan="2">Expired : {{ view_expired }}</th>
                                    </tr>
                                    </br></br>
                                    <tr>
                                        <th scope="col" colspan="5">
                                            <button type="button" data-toggle='modal' data-target='#add_detail_indikator' @click="checkSaveDetailIndikator(view_kd_indikator)" class="btn btn-primary pull-right">Tambah Indikator</button>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th scope="col">No.</th>
                                        <th scope="col">Indikator</th>
                                        <th scope="col">Persentase %</th>
                                        <th scope="col">Maksimal Pertanyaan</th>
                                        <th scope="col"></th>
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                    <tr v-for="arr_detail,index in v_detail_indikator_periode">
                                        <td scope="col">{{ index+1 }}</td>
                                        <td scope="col">{{ arr_detail.indikator }}</td>
                                        <td scope="col">{{ arr_detail.persentase }}</td>
                                        <td scope="col">{{ arr_detail.max_urutan }}</td>
                                        <td scope="col">
                                        <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                                            <div class="btn-group mr-2" role="group" aria-label="First group">
                                                <button type="button" class="btn btn-warning btn-xs" data-toggle='modal' data-target='#edit_detail_indikator' @click="editDetailIndikator(arr_detail.id_indikator_kriteria)"><i class="fa fa-pencil"></i></button>
                                            </div>
                                            <div class="btn-group mr-2" role="group" aria-label="First group">
                                                <button type="button" class="btn btn-danger btn-xs" @click="deleteDetailIndikator(arr_detail.id_indikator_kriteria)"><i class="fa fa-trash"></i></button>
                                            </div>
                                        </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                    <div class="modal fade" id="edit_indikator_periode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <form @submit.prevent="updateIndikatorPeriode">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title font-weight-bold" id="exampleModalLabel">Edit Pembuat Indikator</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div id="position-modal" class="modal-body">
                                        <div class="form-group">
                                            <label>Level</label>
                                            <v-select style="text-transform: capitalize" label="level" :options="level" v-model="v_level" @input="changeLevel" require></v-select>
                                        </div>
                                        <div class="form-group">
                                            <label>Periode</label>
                                            <vuejs-datepicker
                                                v-model="v_periode"
                                                :value="Date"
                                                :format="DatePickerFormata"
                                                :disabledDates="disabledDates"
                                                :bootstrap-styling="true"
                                                :placeholder="holdera"
                                                :minimum-view="minv">
                                            </vuejs-datepicker>
                                        </div>
                                        <div class="form-group">
                                            <label>Expired</label>
                                            <vuejs-datepicker
                                                v-model="v_expired"
                                                :format="DatePickerFormatb"
                                                :disabledDates="disabledDates"
                                                :bootstrap-styling="true"
                                                :placeholder="holderb">
                                            </vuejs-datepicker>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="modal fade" id="add_detail_indikator" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <form @submit.prevent="saveDetailIndikator">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title font-weight-bold" id="exampleModalLabel">Tambah Detail Indikator</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div id="position-modal" class="modal-body">
                                        <div class="form-group">
                                            <label>Indikator</label>
                                            <input type="text" v-model="v_detail_indikator" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Persentase</label>
                                            <input type="text" v-model="v_detail_persentase" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Maksimal Pertanyaan</label>
                                            <input type="text" v-model="v_detail_max_urutan" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="modal fade" id="edit_detail_indikator" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <form @submit.prevent="updateDetailIndikator">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title font-weight-bold" id="exampleModalLabel">Edit Detail Indikator</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div id="position-modal" class="modal-body">
                                        <div class="form-group">
                                            <label>Indikator</label>
                                            <input type="text" v-model="v_detail_indikator" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Persentase</label>
                                            <input type="text" v-model="v_detail_persentase" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Maksimal Pertanyaan</label>
                                            <input type="text" v-model="v_detail_max_urutan" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </transition></div>
    `,
    created() {
        EventBus.$on("detail_indikator_component", (open, id, kd_indikator) => {
            this.isShow = open
            // console.log(id)
            // console.log(kd_indikator)
            this.getLevel()
            this.id_indikator = id
            this.getIndikatorPeriodeByID(id);
            this.getDetailIndikatorPeriode(kd_indikator);
        })
        EventBus.$on("detail_indikator_component", (list_indikator) => {
            this.isShow = list_indikator
        })
    },
    components: {
        vuejsDatepicker
    },
    data() {
        return {
            holdera: "-- Pilih Bulan --",
            holderb: "-- Pilih Tanggal --",
            DatePickerFormata: 'MMMM yyyy',
            DatePickerFormatb: 'dd MMMM yyyy',
            disabledDates: {
                to: new Date(Date.now() - 8640000)
            },
            startmonth: '',
            enddate: '',
            minv: 'month',
            monthNames: [
                { value: "1", text: "January" },
                { value: "2", text: "February" },
                { value: "3", text: "March" },
                { value: "4", text: "April" },
                { value: "5", text: "May" },
                { value: "6", text: "June" },
                { value: "7", text: "July" },
                { value: "8", text: "August" },
                { value: "9", text: "September" },
                { value: "10", text: "October" },
                { value: "11", text: "November" },
                { value: "12", text: "December" }
            ],
            id_indikator: 0,
            v_change: '',
            isShow: false,
            isLoading: false,
            v_indikator: '',
            v_persentase: '',
            v_max_urutan: '',
            v_kode_indikator: '',
            v_level: '',
            v_id_level: '',
            v_periode: '',
            v_expired: '',
            v_id_indikator_periode: '',
            v_change: '',
            view_kd_indikator: '',
            view_periode: '',
            view_expired: '',
            v_detail_indikator_periode: [],
            v_detail_indikator_kriteria: [],
            v_detail_id_indikator: '',
            v_detail_kd_indikator: '',
            v_detail_indikator: '',
            v_detail_persentase: '',
            v_detail_max_urutan: '',
            v_detail_id_indikator_kriteria: '',
            v_total_persentase: '',
            view_id_indikator_kriteria: '',
            view_indikator_kriteria: '',
            view_indikator_max_urutan: '',
            v_kriteria_id_detail_indikator_kriteria: '',
            v_kriteria_urutan: '',
            v_kriteria_bobot: '',
            level: [],
        }
    },
    methods: {
        getLevel() {

            axios.post('_component/data_penilaian_2.php', {
                request: "getLevel",
            }).then(response => {
                this.isLoading = false;

                this.level = response.data[0];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        changeLevel() {
            this.v_change = 1;
        },
        btn_close_edit_indikator() {
            this.isShow = false;
        },
        getIndikatorPeriodeByID(id) {
            axios.post('_component/data_penilaian_2.php', {
                request: "getIndikatorPeriodeById",
                id_indikator_periode: id,
            }).then(response => {
                this.isLoading = false;
                // console.log(response.data)
                this.v_id_indikator_periode = response.data[0].id_indikator_periode;
                this.v_level = response.data[0].level;
                this.v_id_level = response.data[0].id_level;
                this.v_periode = response.data[0].periode_month;
                this.v_expired = response.data[0].expired;

                this.view_kd_indikator = response.data[0].kd_indikator;
                this.view_periode = response.data[0].periode_format;
                this.view_expired = response.data[0].expired_format;

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getDetailIndikatorPeriode(kd_indikator) {
            axios.post('_component/data_penilaian_2.php', {
                request: "getDetailIndikatorPeriode",
                kd_indikator: kd_indikator,
            }).then(response => {
                this.isLoading = false;

                this.v_detail_indikator_periode = response.data[0];
                this.v_lenght_detail_indikator_periode = this.v_detail_indikator_periode.length;

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        checkSaveDetailIndikator(kd) {

            this.v_detail_indikator = '';
            this.v_detail_persentase = '';
            this.v_detail_max_urutan = '';

            let kd_indikator = kd;

            console.log(kd_indikator);

            axios.post('_component/data_penilaian_2.php', {
                request: "checkTotalPersentaseDetailIndikator",
                kd_indikator: kd,
            }).then(response => {
                this.isLoading = false;

                this.v_total_persentase = response.data[0].total_persentase;
                this.v_detail_id_indikator = response.data[1].id_indikator;
                this.v_detail_kd_indikator = response.data[1].kd_indikator;

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        saveDetailIndikator() {

            total_persentase = parseInt(this.v_total_persentase) + parseInt(this.v_detail_persentase);

            let detail_id_indikator = this.v_detail_id_indikator;
            let detail_kd_indikator = this.v_detail_kd_indikator;
            let detail_indikator = this.v_detail_indikator;
            let detail_persentase = this.v_detail_persentase;
            let detail_max_urutan = this.v_detail_max_urutan;

            if (total_persentase <= 100) {
                axios.post('_component/data_penilaian_2.php', {
                    request: "saveDetailIndikatorByKode",
                    detail_id_indikator: detail_id_indikator,
                    detail_indikator: detail_indikator,
                    detail_persentase: detail_persentase,
                    detail_max_urutan: detail_max_urutan
                }).then(response => {
                    this.isLoading = false;

                    $('#add_detail_indikator').modal('hide');

                    this.view_add_indikator_periode = false;
                    this.btn_add_indikator_periode = false;
                    this.view_setting_indikator_detail = false;
                    this.view_edit_indikator_periode = true;

                    this.getIndikatorPeriode();
                    this.getDetailIndikatorPeriode(detail_kd_indikator);

                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });
            } else {
                alert("Gagal Menambahkan Indikator, Total Persentase Diatas 100 %");
            }

        },
        async editDetailIndikator(id) {
            console.log(id);

            await axios.post('_component/data_penilaian_2.php', {
                request: "getDetailIndikatorById",
                id_indikator_kriteria: id,
            }).then(response => {
                this.isLoading = false;

                this.v_detail_indikator = response.data[0].indikator;
                this.v_detail_persentase = response.data[0].persentase;
                this.v_detail_max_urutan = response.data[0].max_urutan;
                this.v_detail_id_indikator_kriteria = response.data[0].id_indikator_kriteria;
                this.v_detail_kd_indikator = response.data[0].kd_indikator;

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });

            await axios.post('_component/data_penilaian_2.php', {
                request: "checkTotalPersentaseDetailIndikatorEdit",
                kd_indikator: this.v_detail_kd_indikator,
                id_indikator_kriteria: this.v_detail_id_indikator_kriteria,
            }).then(response => {
                this.isLoading = false;

                this.v_total_persentase = response.data;

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        updateDetailIndikator() {

            let total_1 = '';
            let total_2 = '';
            let temp_persentase = '';
            let total_persentase = '';

            total_1 = parseInt(this.v_total_persentase[0].total_persentase);
            total_2 = parseInt(this.v_total_persentase[1].persentase);

            temp_persentase = total_1 - total_2;
            total_persentase = temp_persentase + parseInt(this.v_detail_persentase);

            if (total_persentase <= 100) {
                axios.post('_component/data_penilaian_2.php', {
                    request: "updateDetailIndikatorByKode",
                    id_indikator_kriteria: this.v_detail_id_indikator_kriteria,
                    indikator: this.v_detail_indikator,
                    persentase: this.v_detail_persentase,
                    max_urutan: this.v_detail_max_urutan
                }).then(response => {
                    this.isLoading = false;

                    $('#edit_detail_indikator').modal('hide');

                    this.view_add_indikator_periode = false;
                    this.btn_add_indikator_periode = false;
                    this.view_setting_indikator_detail = false;
                    this.view_edit_indikator_periode = true;

                    this.getIndikatorPeriode();
                    this.getDetailIndikatorPeriode(this.v_detail_kd_indikator);

                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });
            } else {
                alert("Gagal Menambahkan Indikator, Total Persentase Diatas 100 %");
            }

        },
        deleteDetailIndikator(id) {
            let detail_kriteria_lenght = [];

            axios.post('_component/data_penilaian_2.php', {
                request: "getDetailIndikatorById",
                id_indikator_kriteria: id,
            }).then(response => {
                this.isLoading = false;

                this.v_detail_indikator = response.data[0].indikator;
                this.v_detail_persentase = response.data[0].persentase;
                this.v_detail_max_urutan = response.data[0].max_urutan;
                this.v_detail_id_indikator_kriteria = response.data[0].id_indikator_kriteria;
                this.v_detail_kd_indikator = response.data[0].kd_indikator;

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });

            if (confirm("Do you really want to delete?")) {
                axios.post('_component/data_penilaian_2.php', {
                    request: "getDetailIndikatorKriteria",
                    id_indikator_kriteria: id,
                }).then(response => {
                    this.isLoading = false;

                    detail_kriteria_lenght = response.data[0].length;

                    console.log(detail_kriteria_lenght);

                    if (detail_kriteria_lenght < 1) {
                        axios.post('_component/data_penilaian_2.php', {
                            request: "deleteDetailIndikatorKriteria",
                            id_indikator_kriteria: id,
                        }).then(response => {
                            this.isLoading = false;
                            alert("Data Berhasil Di Hapus");

                            this.view_add_indikator_periode = false;
                            this.btn_add_indikator_periode = false;
                            this.view_setting_indikator_detail = false;
                            this.view_edit_indikator_periode = true;

                            this.getIndikatorPeriode();
                            this.getDetailIndikatorPeriode(this.v_detail_kd_indikator);
                        }).catch(error => {
                            console.log(error);
                            this.isLoading = false;
                        });
                    } else {
                        alert("Terdapat Data Detail Pada Nomor Penilaian, Nomor Penilaian Gagal Di Hapus ");
                    }

                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });
            }
        },
        editDetailIndikatorKriteria(id) {

            axios.post('_component/data_penilaian_2.php', {
                request: "getDetailIndikatorKriteriaByID",
                id_indikator_kriteria: this.view_id_indikator_kriteria,
                id_detail_indikator_kriteria: id,
            }).then(response => {
                this.isLoading = false;

                this.v_kriteria_id_detail_indikator_kriteria = response.data[0].id_detail_indikator_kriteria;
                this.v_kriteria_urutan = response.data[0].urutan;
                this.v_kriteria_bobot = response.data[0].bobot;

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });

        },
        getDetailIndikatorKriteria(id) {
            axios.post('_component/data_penilaian_2.php', {
                request: "getDetailIndikatorKriteria",
                id_indikator_kriteria: id,
            }).then(response => {
                this.isLoading = false;
                console.log(response.data)
                this.v_detail_indikator_kriteria = response.data[0];
                this.view_id_indikator_kriteria = response.data[1].id_indikator_kriteria;
                this.view_indikator_kriteria = response.data[1].indikator;
                this.view_indikator_max_urutan = response.data[1].max_urutan;

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        updateIndikatorPeriode() {
            let id_level = '';

            if (this.v_change == 1) {
                id_level = this.v_level.id_level;
                this.v_change = '';
            } else {
                id_level = this.v_id_level;
                this.v_change = '';
            }

            axios.post('_component/data_penilaian_2.php', {
                request: "updateIndikatorPeriode",
                id_indikator_periode: this.v_id_indikator_periode,
                id_level: id_level,
                periode: this.v_periode,
                expired: this.v_expired
            }).then(response => {
                this.isLoading = false;

                alert("Data Berhasil Di Update");
                $('#edit_indikator_periode').modal('hide');

                this.v_id_indikator_periode = "";
                this.v_level = "";
                this.v_id_level = "";
                this.v_periode = "";
                this.v_expired = "";
                this.v_change = '';

                this.getIndikatorPeriode();

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
    }
})


// SUB LIST PERTANYAAN
Vue.component('sub_list_indikator', {
    template: `
    <div><transition name="fade">
        <div style="transition-duration: 0.1s;" class="nav-tabs-custom" v-if="isShow">
            <div class="tab-content" style="border-top: 1px solid #eee;">
                <h3 style="margin-bottom: -10px;margin-top: 0px;"> Detail Pertanyaan</h3>
                <button v-if="v_btn_add_sub_indikator" style="margin-top: -20px;" class='pull-right btn btn-primary' @click='empty_sub_indikator'><i class="fa fa-plus"></i></button>
                <hr />
                <div class="row">
                    <div class="col-md-12" style="margin-bottom: 2em; margin-top: -1em;">
                        <h4>Materi Pertanyaan Nomor <strong>{{xkd_indikator}}</strong></h4>
                    </div>

                    <div class="col-md-12" v-if="v_form_add_sub_indikator" style="margin-top: -4em;">
                        <form id="form" class="form-horizontal" style="background-color: rgb(221, 221, 221); border-radius: 5px; padding: 25px 40px; margin-bottom: 50px; margin-top: 3em;">
                            <div class="col-md-12">
                                <div class="col-sm-4 pull-right">
                                    <button type="button" @click="close_sub_indikator" class="btn btn-danger pull-right">X</button>
                                </div>
                            </div>
                            <table class="table table-sm">
                                <thead class="thead-light">
                                    <tr>
                                        <th>#</th>
                                        <th>Indikator</th>
                                        <th>Pertanyaan</th>
                                        <th>No. Urut Pertanyaan</th>
                                        <th>Bobot</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(arr_sub_indikator, k) in data_sub_indikator" :key="k">
                                        <td scope="row" class="trashIconContainer">
                                            <button class="btn btn-warning btn-sm"  @click="deleteRow(k,arr_sub_indikator)"><i class="fa fa-trash"></i></button>
                                        </td>
                                        <td>
                                            <v-select 
                                            style="text-transform: capitalize;" 
                                            label="indikator" 
                                            :options="view_indikator" 
                                            :reduce = "view_indikator=>view_indikator.id_indikator_kriteria"
                                            v-model="arr_sub_indikator.indikator" 
                                            @input="changeDetailIndikator(arr_sub_indikator.indikator)" 
                                            require></v-select>
                                        </td>
                                        <td>
                                            <input class="form-control" type="text" v-model="arr_sub_indikator.sub_indikator" required/>
                                        </td>
                                        <td>
                                            <v-select 
                                            style="text-transform: capitalize;" 
                                            label="urutan" 
                                            :reduce = "v_pertanyaan=>v_pertanyaan.urutan"
                                            :options="v_pertanyaan" 
                                            v-model="arr_sub_indikator.no_urut" 
                                            @input="changeUrutan" require></v-select>
                                        </td>
                                        <td>
                                            <input class="form-control" type="number" v-model="arr_sub_indikator.bobot" required/>
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td>
                                            <button type='button' class="btn btn-info btn-sm" @click="addNewRow">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                            <td></td>
                                            <td></td>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="row" style="margin-top: -4.2em;">
                                <button style="margin-right: 2em;" type='button' class="btn btn-info btn-sm pull-right" 
                                @click="saveSubIndikator">
                                    SIMPAN
                                </button>
                            </div>
                        </form>
                    </div>

                    <div class="lds-facebook" v-if="isLoading"><div></div><div></div><div></div></div>
                    <div class="col-md-12">
                        <v-client-table :data="sub_indikator" :columns="columns" :options="options"></v-client-table>
                    </div>

                    
                </div>
            </div>
        </div>
    </transition></div>
    `,
    // props: ['kd_indikator'],
    data() {
        return {
            isShow: false,
            isOpen: false,
            isLoading: false,
            columns: ['kd_indikator', 'indikator', 'sub_indikator', 'level', 'urutan', 'bobot', 'action_sub_indikator'],
            options: {
                templates: {
                    action_sub_indikator: 'button_sub_indikator'
                },
                headings: {
                    kd_indikator: 'No. Penilaian',
                    indikator: 'Indikator',
                    sub_indikator: 'Sub Indikator',
                    level: 'Level',
                    urutan: 'Urutan',
                    bobot: 'Bobot',
                    action_sub_indikator: '#'
                },
                sortable: ['kd_indikator', 'indikator', 'level', 'urutan', 'bobot']
            },
            v_nomor_penilaian: '',
            v_indikator: '',
            v_detail_indikator: '',
            v_sub_indikator: '',
            view_nomor_penilaian: [],
            view_indikator: [],
            view_detail_indikator: [],
            sub_indikator: [],
            v_urutan: '',
            v_pertanyaan: [],
            detail_indikator_kriteria: [],
            v_id_level: '',
            v_id_indikator: '',
            v_id_indikator_kriteria: '',
            detail_sub_indikator: [],
            v_max_urutan: '',
            v_id_sub_indikator: '',
            v_change_nomor_penilaian: 0,
            v_change_detail_indikator: 0,
            v_change_urutan: 0,
            v_bobot: '',
            xkd_indikator: '',
            v_btn_add_sub_indikator: true,
            v_form_add_sub_indikator: false,
            data_sub_indikator: [{
                indikator: '',
                sub_indikator: '',
                no_urut: '',
                bobot: 0,
            }],

        }
    },
    created() {
        EventBus.$on('update_sub_indikator', (isShow, kd) => {
            console.log(kd)
            this.xkd_indikator = kd
            this.isShow = isShow
            this.getSubIndikator()
            this.getIndikator();
            this.getNomorPenialain();
        })
        EventBus.$on('sub_list_indikator', (kd, open) => {
            this.xkd_indikator = kd
            this.isShow = open
            this.getSubIndikator()
            this.getIndikator();
            this.getNomorPenialain();
        })
        EventBus.$on('sub_list_indikator', (sub_list_indikator) => {
            this.isShow = sub_list_indikator
        })
        EventBus.$on('tutupSubListIndikator', (sub_list_indikator) => {
            this.isShow = sub_list_indikator
        })
    },
    methods: {
        empty_sub_indikator() {
            this.v_nomor_penilaian = '';
            this.v_indikator = '';
            this.v_sub_indikator = '';
            this.v_detail_indikator = '';
            this.v_bobot = '';
            this.v_btn_add_sub_indikator = false;
            this.v_form_add_sub_indikator = true;
        },
        close_sub_indikator() {
            this.v_btn_add_sub_indikator = true;
            this.v_form_add_sub_indikator = false;
        },
        addNewRow() {
            this.data_sub_indikator.push({
                indikator: '',
                sub_indikator: '',
                no_urut: '',
                bobot: 0,
            });
        },
        deleteRow(index, data_sub_indikator) {
            var idx = this.data_sub_indikator.indexOf(data_sub_indikator);
            if (idx > -1) {
                this.data_sub_indikator.splice(idx, 1);
            }
        },
        getSubIndikator() {
            this.isLoading = true;

            axios.post('_component/data_penilaian_2.php', {
                request: "getSubIndikatorAll",
                kd_indikator: this.xkd_indikator,
            }).then(response => {
                this.isLoading = false;
                // console.log(response.data)
                this.sub_indikator = response.data[0];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getNomorPenialain() {
            this.isLoading = true;

            axios.post('_component/data_penilaian_2.php', {
                request: "getIndikatorPeriode2",
                kd_indikator: this.xkd_indikator,
            }).then(response => {
                this.isLoading = false;

                this.view_nomor_penilaian = response.data[0];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getIndikator() {
            this.isLoading = true
            axios.post('_component/data_penilaian_2.php', {
                request: "getDetailIndikatorPeriode",
                kd_indikator: this.xkd_indikator,
            }).then(response => {
                this.isLoading = false;

                this.view_indikator = response.data[0];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getDetailIndikator(id_indikator) {

            axios.post('_component/data_penilaian_2.php', {
                request: "getDetailIndikatorKriteria",
                id_indikator_kriteria: id_indikator,
            }).then(response => {
                this.isLoading = false;

                this.view_detail_indikator = response.data[0];
                this.v_pertanyaan = response.data[2];
                // this.data_sub_indikator.id_indikator_kriteria 

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        changeDetailIndikator(id) {
            if (this.view_indikator != null) {
                this.getDetailIndikator(id);
            } else {
                this.view_indikator = [];
            }
            this.v_change_detail_indikator = 1;
        },
        changeUrutan() {
            if (this.v_detail_indikator != null) {
                this.v_urutan = this.v_detail_indikator.urutan;
            } else {
                this.v_urutan = "";
                this.v_detail_indikator = [];
            }
            this.v_change_urutan = 1;
        },
        saveSubIndikator() {

            axios.post('_component/data_penilaian_2.php', {
                request: "saveSubIndikator",
                sub_indikator_baru: JSON.stringify(this.data_sub_indikator),
                id_level: this.view_nomor_penilaian[0].id_level,
                id_indikator: this.view_nomor_penilaian[0].id_indikator,
            }).then(response => {
                this.isLoading = false;

                this.getSubIndikator();
                this.data_sub_indikator = []

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        }
    }

});

Vue.component("button_sub_indikator", {
    props: ["data", "index", "columns"],
    template: `
    <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
        <div class="btn-group mr-2" role="group" aria-label="First group">
            <button type="button" class="btn btn-xs btn-warning" @click="edit_sub_indikator(data.id_sub_indikator, data.kd_indikator)"><i class="fa fa-pencil"></i></button>
        </div>
        <div class="btn-group mr-2" role="group" aria-label="Second group">
            <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash" @click="hapus_sub_indikator(data.id_sub_indikator, data.kd_indikator)"></i></button>
        </div>
    </div>
    `,
    methods: {
        edit_sub_indikator(id, kd_indikator) {
            // console.log(kd_indikator)
            EventBus.$emit('edit_sub_indikator_component', id, kd_indikator)
        },
        hapus_sub_indikator(id, kd_indikator) {
            EventBus.$emit('hapus_sub_indikator_component', id, kd_indikator)
        }
    }
})

Vue.component('edit_sub_indikator_component', {
    template: `
    <div class="modal fade" id="edit_sub_indikator_component" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <form>
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title font-weight-bold" id="exampleModalLabel">Edit Pertanyaan</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div id="position-modal" class="modal-body">
                        <div class="form-group">
                            <label>Indikator</label>
                            <v-select style="text-transform: capitalize" label="indikator" :options="view_indikator" v-model="v_indikator" @input="changeDetailIndikator" require disabled></v-select>
                        </div>
                        <div class="form-group">
                            <label>Sub Indikator</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" v-model="v_sub_indikator"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Pertanyaan</label>
                            <v-select style="text-transform: capitalize" label="urutan" :options="v_pertanyaan" v-model="v_detail_indikator" @input="changeUrutan" require></v-select>
                        </div>
                        <div class="form-group">
                            <label>Bobot</label>
                            <input type="number" class="form-control" v-model="v_bobot">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" @click="updateSubIndikator(v_nomor_penilaian)" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    `,
    created() {
        EventBus.$on('edit_sub_indikator_component', (id, kd_indikator) => {
            // console.log(kd_indikator)
            this.xkd_indikator = kd_indikator
            this.edit_sub_indikator(id)
            this.openModal()
        })
    },
    data() {
        return {
            v_nomor_penilaian: '',
            v_indikator: '',
            v_detail_indikator: '',
            v_sub_indikator: '',
            view_nomor_penilaian: [],
            view_indikator: [],
            view_detail_indikator: [],
            sub_indikator: [],
            v_urutan: '',
            v_pertanyaan: [],
            detail_indikator_kriteria: [],
            v_id_level: '',
            v_id_indikator: '',
            v_id_indikator_kriteria: '',
            detail_sub_indikator: [],
            v_max_urutan: '',
            v_id_sub_indikator: '',
            v_change_nomor_penilaian: 0,
            v_change_detail_indikator: 0,
            v_change_urutan: 0,
            v_bobot: '',
            xkd_indikator: '',
        }
    },
    methods: {
        edit_sub_indikator(id) {
            axios.post('_component/data_penilaian_2.php', {
                request: "getSubIndikatorById",
                id_sub_indikator: id,
            }).then(response => {
                this.isLoading = false;

                this.detail_sub_indikator = response.data[0];

                // console.log(this.detail_sub_indikator);

                this.v_nomor_penilaian = this.detail_sub_indikator.kd_indikator;
                this.v_indikator = this.detail_sub_indikator.indikator;
                this.v_sub_indikator = this.detail_sub_indikator.sub_indikator;
                this.v_detail_indikator = this.detail_sub_indikator.urutan;

                this.v_id_sub_indikator = this.detail_sub_indikator.id_sub_indikator;
                this.v_id_indikator = this.detail_sub_indikator.id_indikator;
                this.v_id_indikator_kriteria = this.detail_sub_indikator.id_indikator_kriteria;
                this.v_bobot = this.detail_sub_indikator.bobot;
                this.v_max_urutan = this.detail_sub_indikator.max_urutan;

                this.getIndikator(this.v_nomor_penilaian);
                this.getDetailIndikator(this.v_id_indikator_kriteria);
                // console.log(this.v_id_indikator_kriteria)
                // console.log(this.v_nomor_penilaian)


            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        updateSubIndikator(kd) {
            let id_indikator = this.v_id_indikator;
            let id_indikator_kriteria = this.v_id_indikator_kriteria;
            let urutan = '';
            let sub_indikator = this.v_sub_indikator;
            let id_sub_indikator = this.v_id_sub_indikator;
            let bobot = this.v_bobot;

            if (this.v_change_urutan == 1) {
                urutan = this.v_detail_indikator.urutan;
                this.v_change_urutan = '';
            } else {
                urutan = this.v_detail_indikator;
                this.v_change_urutan = '';
            }

            axios.post('_component/data_penilaian_2.php', {
                request: "updateSubIndikator",
                id_sub_indikator: id_sub_indikator,
                id_indikator: id_indikator,
                id_indikator_kriteria: id_indikator_kriteria,
                sub_indikator: sub_indikator,
                urutan: urutan,
                bobot: bobot,
            }).then(response => {
                swal.fire({
                    title: "Updated!",
                    text: "Pertanyaan berhasil diperbarui!",
                    icon: "success",
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        let isShow = true
                        EventBus.$emit('update_sub_indikator', isShow, kd)
                        $('#edit_sub_indikator_component').modal('hide');


                        id_indikator = '';
                        id_indikator_kriteria = '';
                        urutan = '';

                        this.v_change_nomor_penilaian = '';
                        this.v_change_detail_indikator = '';
                        this.v_change_urutan = '';
                    }
                });
            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });

        },
        openModal() {
            $("#edit_sub_indikator_component").modal("show");
        },
        changeDetailIndikator() {
            if (this.v_indikator != null) {
                this.getDetailIndikator(this.v_indikator.id_indikator_kriteria);
            } else {
                this.v_indikator = [];
            }
            this.v_change_detail_indikator = 1;
        },
        changeUrutan() {
            if (this.v_detail_indikator != null) {
                this.v_urutan = this.v_detail_indikator.urutan;
            } else {
                this.v_urutan = "";
                this.v_detail_indikator = [];
            }
            this.v_change_urutan = 1;
        },
        getIndikator(id) {
            axios.post('_component/data_penilaian_2.php', {
                request: "getDetailIndikatorPeriode",
                kd_indikator: id,
            }).then(response => {
                this.isLoading = false;
                // console.log(response.data)
                // console.log(this.xkd_indikator)
                this.view_indikator = response.data[0];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getDetailIndikator(id_indikator) {

            axios.post('_component/data_penilaian_2.php', {
                request: "getDetailIndikatorKriteria",
                id_indikator_kriteria: id_indikator,
            }).then(response => {
                this.isLoading = false;
                // console.log(response.data)
                this.view_detail_indikator = response.data[0];
                this.v_pertanyaan = response.data[2];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
    }
})

Vue.component('hapus_sub_indikator_component', {
    props: ["data", "username"],
    template: `<br>`,
    created() {
        EventBus.$on('hapus_sub_indikator_component', (id, kd_indikator) => {
            this.deleteFunc(id, kd_indikator)
        })
    },
    methods: {
        deleteFunc(id, kd_indikator) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You want to delete this data?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    axios.post('_component/data_penilaian_2.php', {
                        request: "deleteSubIndikator",
                        id_sub_indikator: id,
                    }).then(response => {

                        this.isLoading = false;

                        swal.fire({
                            title: "Terhapus!",
                            text: "Data berhasil di hapus!",
                            icon: "success",
                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                // location.reload();
                                console.log(kd_indikator);
                                EventBus.$emit('sub-list-indikator', isShow = true)
                            }
                        });

                    }).catch(error => {
                        console.log(error);
                        this.isLoading = false;
                    });
                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your data is safe :)',
                        'error'
                    )
                }
            })
        }
    }
})

Vue.component('list_periode', {

    template:
        `
        <div>
            <div class="row">
                <div class="col-md-12">
                    <h4>Manage Periode</h4>
                </div>
            </div>
            </br></br>
            <div class="row">
                <div class="col-md-12">
                    <button type="button" data-toggle='modal' data-target='#add_periode' @click="" class="btn btn-primary pull-right">Tambah Periode</button>
                </div>
            </div>
            </br>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-condensed table-bordered table-hover">
                        <thead>
                            <tr v-if="isLoading">
                                <td colspan="9">
                                    <div class="lds-facebook"><div></div><div></div><div></div></div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="col">No.</th>
                                <th scope="col">Level</th>
                                <th scope="col">Periode</th>
                                <th scope="col">Expired</th>
                                <th scope="col">Status</th>
                                <th scope="col" colspan="2"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <template v-for="arr_periode,index in periode">
                                <tr>
                                    <td>{{ index+1 }}</td>
                                    <td>{{ arr_periode.level }}</td>
                                    <td>{{ arr_periode.periode_date }}</td>
                                    <td>{{ arr_periode.expired_date }}</td>
                                    <td>{{ arr_periode.status }}</td>
                                    <td><button class='pull-right btn btn-primary fa fa-edit' data-toggle='modal' data-target='#edit_periode' @click=''></button></td>
                                    <td><button class='btn btn-danger fa fa-trash-o' data-toggle='modal' @click=''></button></td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    `,
    data() {
        return {
            isOpen: false,
            isLoading: false,
            periode: [],
        }
    },
    created() {
        this.getPeriode();
    },
    methods: {
        getPeriode() {
            this.isLoading = true;
            axios.post('_component/data_penilaian_2.php', {
                request: "getPeriode",
            }).then(response => {
                this.isLoading = false;

                this.periode = response.data[0];
                console.log(this.periode)

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
    }

});

Vue.component('hrd_setting', {
    template: `
    <div><transition name="fade">
        <div style="transition-duration: 0.1s;" class="nav-tabs-custom" v-if="isOpen">
            <div class="tab-content" style="margin-top: -2.5em;border-top: 1px solid #eee;">
                <h3 style="margin-bottom: -10px;margin-top: 0px;"> Pengaturan Indikator Penilaian</h3>
                <button v-if="v_btn_add_sub_indikator" style="margin-top: -20px;" class='pull-right btn btn-primary' @click='empty_sub_indikator'><i class="fa fa-plus"></i></button>
                <hr />
                <div class="row">

                    <div class="col-md-12" v-if="v_form_add_sub_indikator" style="margin-top: -4em;">
                        <form id="form" class="form-horizontal" style="background-color: rgb(221, 221, 221); border-radius: 5px; padding: 25px 40px; margin-bottom: 50px; margin-top: 3em;">
                            <div class="col-md-12">
                                <div class="col-sm-4 pull-right">
                                    <button type="button" @click="close_sub_indikator" class="btn btn-danger pull-right">X</button>
                                </div>
                            </div>
                            <table class="table table-sm">
                                <thead class="thead-light">
                                    <tr>
                                        <th>#</th>
                                        <th>Tipe Penilai</th>
                                        <th>Level</th>
                                        <th>No. Penilaian</th>
                                        <th>Indikator</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(setting, k) in pengaturan_indikator" :key="k">
                                        <td scope="row" class="trashIconContainer">
                                            <button class="btn btn-warning btn-sm" @click="deleteRow(k,setting)"><i class="fa fa-trash"></i></button>
                                        </td>
                                        <td>
                                            <v-select 
                                            v-model="setting.tipe_penilai"
                                            :options="opt_penilai"
                                            placeholder="Pilih Tipe Penilai" require></v-select>
                                        </td>
                                        <td>
                                            <v-select 
                                            style="text-transform: capitalize;" 
                                            v-model="setting.level"
                                            :options="level" 
                                            :reduce="level=>level.id_level"
                                            label="level"
                                            placeholder="Pilih Level" require></v-select>
                                        </td>
                                        <td>
                                            <v-select 
                                            style="text-transform: capitalize;" 
                                            v-model="setting.kd_indikator"
                                            :options="indikator_periode" 
                                            :reduce="indikator_periode=>indikator_periode.kd_indikator"
                                            label="nomor_penilaian"
                                            placeholder="Pilih No. Penilaian"
                                            @input="getDetailIndikatorPeriode(setting.kd_indikator)" require></v-select>
                                        </td>
                                        <td>
                                            <v-select multiple
                                            style="text-transform: capitalize;" 
                                            v-model="setting.indikator"
                                            :options="sub_indikator" 
                                            :reduce="sub_indikator=>sub_indikator.id_indikator_kriteria"
                                            label="indikator" 
                                            placeholder="Pilih Indikator" require></v-select>
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td>
                                            <button type='button' class="btn btn-info btn-sm" @click="addNewRow()">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                            <td></td>
                                            <td></td>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="row" style="margin-top: -4.2em;">
                                <button style="margin-right: 2em;" type='button' class="btn btn-info btn-sm pull-right" 
                                @click="saveSettingIndikator">
                                    SIMPAN
                                </button>
                            </div>
                        </form>
                    </div>

                    <div class="lds-facebook" v-if="isLoading"><div></div><div></div><div></div></div>
                    <div class="col-md-12">
                        <v-client-table :data="tab_indikator" :columns="columns" :options="options"></v-client-table>
                    </div>

                    
                </div>
            </div>
        </div>
    </transition></div>
    `,
    data() {
        return {
            isOpen: true,
            isLoading: false,
            tab_indikator: [],
            level: [],
            opt_penilai: ['Atasan', 'Bawahan', 'RE', 'RI', 'HRD'],
            indikator_periode: [],
            sub_indikator: [],
            isShow: false,
            kd_indikator: '',
            columns: ['tipe_penilai', 'level', 'kd_indikator', 'indikator', 'action_hrd'],
            options: {
                templates: {
                    action_hrd: 'action_hrd'
                },
                headings: {
                    kd_indikator: 'No. Penilaian',
                    indikator: 'Indikator',
                    level: 'Level Target',
                    tipe_penilai: 'Tipe Penilai',
                    action_hrd: ''
                },
                sortable: ['kd_indikator', 'indikator', 'level', 'tipe_penilai']
            },
            v_btn_add_sub_indikator: true,
            v_form_add_sub_indikator: false,
            pengaturan_indikator: [{
                tipe_penilai: '',
                level: '',
                kd_indikator: '',
                indikator: [],
            }],

        }
    },
    created() {
        this.getIndikatorPeriode();
        this.getTabIndikatorPenilaian();
        this.getLevel();
        EventBus.$on('open_hrd_setting', () => {
            this.isOpen = true
        })
        EventBus.$on('refresh_update', () => {
            this.getIndikatorPeriode();
            this.getTabIndikatorPenilaian();
            this.getLevel();
        })
        EventBus.$on('close_pengaturan_hrd', (close) => {
            this.isOpen = close
        })
        // EventBus.$on('close_laporan', (close) => {
        //     this.isOpen = close;
        // });
        // EventBus.$on('close_laporan_detail', (close) => {
        //     this.isOpen = close;
        // });
    },
    methods: {
        empty_sub_indikator() {
            this.v_btn_add_sub_indikator = false;
            this.v_form_add_sub_indikator = true;
            this.pengaturan_indikator.length = 0;
        },
        close_sub_indikator() {
            this.v_btn_add_sub_indikator = true;
            this.v_form_add_sub_indikator = false;
        },
        addNewRow() {
            this.pengaturan_indikator.push({
                tipe_penilai: '',
                level: '',
                kd_indikator: '',
                indikator: [],
            });
        },
        deleteRow(index, setting) {
            var idx = this.pengaturan_indikator.indexOf(setting);
            if (idx > -1) {
                this.pengaturan_indikator.splice(idx, 1);
            }
        },
        getTabIndikatorPenilaian() {
            this.isLoading = true;
            axios.post('_component/data_penilaian_2.php', {
                request: "getTabIndikatorPenilaian",
            }).then(response => {
                this.isLoading = false;

                this.tab_indikator = response.data[0];
                console.log(response.data)

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getIndikatorPeriode() {
            this.isLoading = true;

            axios.post('_component/data_penilaian_2.php', {
                request: "getIndikatorPeriode",
            }).then(response => {
                this.isLoading = false;

                this.indikator_periode = response.data[0];
                console.log(this.indikator_periode)

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getLevel() {

            axios.post('_component/data_penilaian_2.php', {
                request: "getLevel",
            }).then(response => {
                this.isLoading = false;

                this.level = response.data[0];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getDetailIndikatorPeriode(kd_indikator) {
            axios.post('_component/data_penilaian_2.php', {
                request: "getDetailIndikatorPeriode",
                kd_indikator: kd_indikator,
            }).then(response => {
                this.isLoading = false;

                this.sub_indikator = response.data[0];
                console.log(this.sub_indikator)

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        saveSettingIndikator() {
            // console.log(JSON.stringify(this.pengaturan_indikator))
            axios.post('_component/data_penilaian_2.php', {
                request: "saveSettingIndikator",
                setting_baru: JSON.stringify(this.pengaturan_indikator),
            }).then(response => {
                this.isLoading = false;

                console.log(response.data)

                swal.fire({
                    title: "Berhasil!",
                    text: "Pengaturan Indikator Penilaian berhasil disimpan!",
                    icon: "success"
                })

                this.close_sub_indikator()
                this.getTabIndikatorPenilaian()
            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        }
    }
});

Vue.component("action_hrd", {
    props: ["data", "index", "columns"],
    template: `
    <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
        <div class="btn-group mr-2" role="group" aria-label="Second group">
            <button type="button" class="btn btn-xs btn-danger" @click="hapus_hrd_setting(data.id_tab)"><i class="fa fa-trash"></i></button>
        </div>
    </div>
    `,
    methods: {
        // edit_hrd_setting() {
        //     <div class="btn-group mr-2" role="group" aria-label="First group">
        //     <button type="button" class="btn btn-xs btn-warning" @click="edit_hrd_setting()"><i class="fa fa-pencil"></i></button>
        // </div>
        //     console.log(this.data)
        //     EventBus.$emit('open_edit', this.data)
        // },
        hapus_hrd_setting(id) {
            EventBus.$emit('hapus_hrd_setting', id);
        }
    }
})

Vue.component("edit_hrd_setting", {
    template: `
    <div class="modal fade" id="edit_hrd_setting" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <form>
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title font-weight-bold" id="exampleModalLabel">Edit Pengaturan Indikator Penilaian</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div id="position-modal" class="modal-body">
                        <div class="form-group">
                            <label>Tipe Penilai</label>
                            <v-select v-model="v_type_penilai" :options="opt_penilai" :key="v_type_penilai"></v-select>
                        </div>
                        <div class="form-group">
                            <label>Level</label>
                            <v-select label="level" :options="level" v-model="v_level" :reduce="level=>level.id_level" :key="v_level"></v-select>
                        </div>
                        <div class="form-group">
                            <label>No. Penilaian</label>
                            <v-select v-model="v_nomor_penilaian" :options="indikator_periode" 
                            :reduce="indikator_periode=>indikator_periode.kd_indikator" :key="v_nomor_penilaian"
                            label="nomor_penilaian" @input="getDetailIndikatorPeriode(v_nomor_penilaian)"></v-select>
                        </div>
                        <div class="form-group">
                            <label>Indikator</label>
                            <v-select v-model="v_id_indikator_kriteria" :key="v_id_indikator_kriteria" :options="sub_indikator" :reduce="sub_indikator=>sub_indikator.id_indikator_kriteria"
                            label="indikator"></v-select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" @click="updateSettingIndikator" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    `,
    data() {
        return {
            arr_edit_setting: [],
            id_tab: '',
            v_type_penilai: '',
            v_level: '',
            v_nomor_penilaian: '',
            v_id_indikator_kriteria: '',
            indikator_periode: [],
            level: [],
            opt_penilai: ['Atasan', 'Bawahan', 'RE', 'RI', 'HRD'],
            sub_indikator: [],
            v_change_level: 0
        }
    },
    created() {
        EventBus.$on('open_edit', (data) => {
            this.getLevel()
            this.getIndikatorPeriode()
            // this.arr_edit_setting = data
            this.v_type_penilai = data.tipe_penilai
            this.v_level = data.level
            this.v_nomor_penilaian = data.kd_indikator
            this.v_id_indikator_kriteria = data.indikator
            this.id_tab = data.id_tab
            console.log(this.arr_edit_setting)
            this.openModal()
        })
    },
    methods: {
        openModal() {
            $("#edit_hrd_setting").modal("show");
        },
        getIndikatorPeriode() {
            this.isLoading = true;

            axios.post('_component/data_penilaian_2.php', {
                request: "getIndikatorPeriode",
            }).then(response => {
                this.isLoading = false;

                this.indikator_periode = response.data[0];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getLevel() {

            axios.post('_component/data_penilaian_2.php', {
                request: "getLevel",
            }).then(response => {
                this.isLoading = false;

                this.level = response.data[0];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getDetailIndikatorPeriode(kd_indikator) {
            axios.post('_component/data_penilaian_2.php', {
                request: "getDetailIndikatorPeriode",
                kd_indikator: kd_indikator,
            }).then(response => {
                this.isLoading = false;

                this.sub_indikator = response.data[0];
                console.log(this.sub_indikator)

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        updateSettingIndikator() {
            // console.log(JSON.stringify(this.arr_edit_setting))
            axios.post('_component/data_penilaian_2.php', {
                request: "updateSettingIndikator",
                arr_data_setting: JSON.stringify(this.arr_edit_setting),
                v_type_penilai: this.v_type_penilai,
                v_level: this.v_level,
                v_nomor_penilaian: this.v_nomor_penilaian,
                v_id_indikator: this.v_id_indikator_kriteria,
                id_tab: this.id_tab

            }).then(response => {
                this.isLoading = false;

                swal.fire({
                    title: "Berhasil!",
                    text: "Pengaturan Indikator Penilaian berhasil diperbaharui!",
                    icon: "success",
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        $("#edit_hrd_setting").modal("hide");
                        EventBus.$emit('refresh_update')
                    }
                });


                // this.getTabIndikatorPenilaian()

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });

        },
        // getTabIndikatorPenilaian() {
        //     this.isLoading = true;
        //     axios.post('_component/data_penilaian_2.php', {
        //         request: "getTabIndikatorPenilaian",
        //     }).then(response => {
        //         this.isLoading = false;

        //         this.tab_indikator = response.data[0];
        //         console.log(response.data)

        //     }).catch(error => {
        //         console.log(error);
        //         this.isLoading = false;
        //     });
        // },

    }
})

Vue.component('hapus_hrd_setting', {
    template: `<br>`,
    created() {
        EventBus.$on('hapus_hrd_setting', (id) => {
            this.deleteFunc(id)
        })
    },
    methods: {
        deleteFunc(id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You want to delete this data?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    axios.post('_component/data_penilaian_2.php', {
                        request: "deleteSettingIndikator",
                        id_tab: id,
                    }).then(response => {

                        this.isLoading = false;

                        swal.fire({
                            title: "Terhapus!",
                            text: "Data berhasil di hapus!",
                            icon: "success",
                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                // location.reload();
                                // console.log(kd_indikator);
                                EventBus.$emit('refresh_update')
                            }
                        });

                    }).catch(error => {
                        console.log(error);
                        this.isLoading = false;
                    });
                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your data is safe :)',
                        'error'
                    )
                }
            })
        }
    }
})


Vue.component('penilaian_karyawan_hrd', {
    template: `
    <div><transition name="fade">
            <div class="lds-facebook" v-if="isLoading"><div></div><div></div><div></div></div>
            <div style="transition-duration: 0.15s;" class="nav-tabs-custom" v-if="isOpen">
                <div class="tab-content" style="margin-top: -4em;">
                    <h3 style="margin-bottom: -10px;margin-top: 0px;"><i class="fa fa-users"></i> Penilaian Karyawan HRD</h3>
                    </br>
                    <div class="row">
                        <div class="col-sm-1">
                            <h4>Periode</h4>
                        </div>
                        <div class="col-sm-2">
                            <v-select style="text-transform: capitalize" label="tahun" :options="periode" v-model="thn_periode" :reduce="periode=>periode.tahun"></v-select>
                        </div>
                        <div class="col-sm-9" style="text-align: end;">
                            <button class="btn btn-primary btn-sm" @click="getTargetKaryawan"><i class="fa fa-refresh"></i></button>
                        </div>
                    </div>
                    </br>
                    <div class="row">
                        <div class="col-md-12">
                            <v-client-table :data="target_karyawan" :columns="columns" :options="options">  
                            </v-client-table>
                        </div>
                    </div>
                </div>
            </div>
    </transition></div>`,
    props: ['username'],
    data() {
        return {
            isOpen: false,
            isLoading: false,
            penilai_karyawan: [],
            indikator_karyawan: [],
            target_karyawan: [],
            columns: ['office', 'emp_name', 'nik', 'department', 'position', 'level',
                'join_date', 'total_penilaian_hrd', 'action_penilaian_karyawan_hrd'],
            options: {
                sortable: ['office', 'emp_name', 'nik', 'department', 'position', 'level', 'join_date', 'total_penilaian_hrd'],
                headings: {
                    office: "Lokasi",
                    emp_name: "Nama Karyawan",
                    nik: "NIK",
                    department: "Departemen",
                    position: "Posisi",
                    level: "Level",
                    join_date: "Join Date",
                    total_penilaian_hrd: "Total Nilai HRD",
                    action_penilaian_karyawan_hrd: "#"
                },
                templates: {
                    emp_name: "emp_name-component",
                    action_penilaian_karyawan_hrd: "action_penilaian_karyawan_hrd"
                }
            },
            thn_periode: '',
            periode: []
        }
    },
    created() {
        this.getDateNow();
        this.getperiode();
        // this.getPenilaiKaryawan();
        // this.getTargetKaryawan();
        EventBus.$on('open_penilai_hrd', (open) => {
            this.isOpen = open
            this.getPenilaiKaryawan();
            this.getTargetKaryawan();
        })
        EventBus.$on('close_penilai_hrd', (close) => {
            this.isOpen = close
            this.isOpen = false
        })
        EventBus.$on('table_penilaian', () => {
            this.isOpen = open
            this.getPenilaiKaryawan();
            this.getTargetKaryawan();
        });
        // EventBus.$on('refresh_penilai_hrd', () => {
        //     this.getPenilaiKaryawan();
        //     this.getTargetKaryawan();
        // })
    },
    methods: {
        getDateNow() {
            const today = new Date();
			const date = today.getFullYear();

            this.thn_periode = date;
        },
        getperiode() {
            axios.post('_component/data_penilaian_2.php', {
                request: "getDatesFromRange",
            }).then(response => {
                this.periode = response.data;
                console.log(this.periode);
            }).catch(error => {
                console.log(error);
            });
        },
        getPenilaiKaryawan() {
            let username = this.username;
            this.isLoading = true
            axios.post('_component/data_penilaian_2.php', {
                request: "getPenilaiKaryawan",
                username: username,
            }).then(response => {
                this.isLoading = false;
                this.penilai_karyawan = response.data[0];
            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getTargetKaryawan() {
            this.isLoading = true
            axios.post('_component/data_penilaian_2.php', {
                request: "getlistemployeeAll",
                kd_off: '',
                kd_dept: '',
                periode: this.thn_periode
            }).then(response => {
                this.isLoading = false;
                this.target_karyawan = response.data[0];
            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });

        },
        nilai_karyawan(id_target, id_level) {
            this.$emit('view_nilai_karyawan', [id_target, id_level]);
        }
    }
});

Vue.component('emp_name-component', {
    props: ["data", "index", "column"],
    template: `<span style="text-transform:capitalize;">{{ data.emp_name }}</span>`
})

Vue.component('action_penilaian_karyawan_hrd', {
    props: ["data", "index", "column"],
    template: `
    <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
        <div class="btn-group mr-2" role="group" aria-label="First group"">
            <button v-if="data.total_penilaian_hrd == null" type="button" @click="penilai(data.id_num, data.id_level, 'HRD')" class="btn btn-sm btn-info">Nilai </button>
            <button v-else type="button" class="btn btn-sm btn-success" :disabled="data.total_penilaian_hrd !== null"> Ternilai <i class="fa fa-check"></i> </button>
        </div>
    </div>
    `,
    data() {
        return {
            open: true,
        };
    },
    methods: {
        penilai(id_target, id_level, type_penilai) {
            // EventBus.$emit("open_modal_nilai_employee", id_target, id_level, type_penilai)
            EventBus.$emit('open_tata_cara', this.open, id_target, id_level, type_penilai);
        },
    },
    created() {
        this.getDateNow();
    }
})

// TEMPLATE BARU
let interval = null;

Vue.component('penilaian', {
    template: `
		<div>
			<transition name="modal fade">
				<div class="modal-mask" v-if="open">
					<div class="modal-wrapper" style="margin-top: 0px;">
						<div class="modal-dialog" role="document" style="margin-top: 0px;">
							<div class="modal-content">
								<div class="modal-header">
									<slot name="header"></slot>
								</div>
								<div class="modal-body">
                                    <slot name="body"></slot>
								</div>
								<div class="modal-footer">
									<slot name="footer"></slot>
								</div>
							</div>
						</div>
					</div>
				</div>
			</transition>
		</div>
		`,
    data() {
        return {
            open: false,
            id_target: 0,
            tipe_penilai: '',
            id_level: 0,
            thn_periode: ''
        }
    },
    created() {
        EventBus.$on('open_modal_nilai_employee', (open, id_target, id_level, type_penilai) => {
            console.log(open)
            this.open = open;
            this.id_target = id_target;
            this.tipe_penilai = type_penilai;
            this.id_level = id_level;
            this.$emit('test', { target: this.id_target, penilai: this.tipe_penilai, level: this.id_level })
        });
        EventBus.$on('close_peniliain', (close) => {
            this.open = close;
        })
    },
})

Vue.component('progressBar', {
    template: `
		<div class="progress">
			<div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar" :style="barStyle">
			{{ percent }}
			</div>
		</div>
	`,
    props: {
        min: { type: Number, default: 0 },
        max: { type: Number, default: 1 },
        value: { type: Number, default: 0 },
    },
    computed: {
        barStyle() {
            const frac = Math.min(1, Math.max(0, (this.value - this.min) / (this.max - this.min) || 0));
            return { width: frac * 100 + '%' };
        },
        percent() {
            const frac = Math.min(1, Math.max(0, (this.value - this.min) / (this.max - this.min) || 0));
            const total = Math.round(frac * 100)
            return total + ' %'
        }
    },
})

Vue.component('indikator', {
    props: ["indikators", "v_indikator"],
    template: `
		<div class="indikator-box">
			<ul class="indikator">
				<li v-for="(dt,i) in indikators" :class="{'active':(v_indikator === dt.indikator)}">{{ dt.indikator }}</li>
			</ul>
		</div>
    `,
})

Vue.component('completeTimer', {
    template: `
		<ul class="vuejs-countdown">
			<li v-if="days > 0">
				<p class="digit">{{ days | twoDigits }}</p>
				<p class="text">{{ days > 1 ? 'days' : 'day' }}</p>
			</li>
			<li>
				<p class="digit">{{ hours | twoDigits }}</p>
				<p class="text">{{ hours > 1 ? 'hours' : 'hour' }}</p>
			</li>
			<li>
				<p class="digit">{{ minutes | twoDigits }}</p>
				<p class="text">min</p>
			</li>
			<li>
				<p class="digit">{{ seconds | twoDigits }}</p>
				<p class="text">Sec</p>
			</li>
		</ul>
	`,
    props: {
        deadline: {
            type: String
        },
        end: {
            type: String
        },
        stop: {
            type: Boolean
        }
    },
    data() {
        return {
            now: Math.trunc((new Date()).getTime() / 1000),
            date: null,
            diff: 0
        }
    },
    created() {
        if (!this.deadline && !this.end) {
            throw new Error("Missing props 'deadline' or 'end'");
        }
        let endTime = this.deadline ? this.deadline : this.end;
        this.date = Math.trunc(Date.parse(endTime.replace(/-/g, "/")) / 1000);
        if (!this.date) {
            throw new Error("Invalid props value, correct the 'deadline' or 'end'");
        }
        interval = setInterval(() => {
            this.now = Math.trunc((new Date()).getTime() / 1000);
        }, 1000);
    },
    computed: {
        seconds() {
            return Math.trunc(this.diff) % 60
        },
        minutes() {
            return Math.trunc(this.diff / 60) % 60
        },
        hours() {
            return Math.trunc(this.diff / 60 / 60) % 24
        },
        days() {
            return Math.trunc(this.diff / 60 / 60 / 24)
        }
    },
    watch: {
        now(value) {
            this.diff = this.date - this.now;
            if (this.diff <= 0 || this.stop) {
                this.diff = 0;
                // Remove interval
                clearInterval(interval);
            }
        }
    },
    filters: {
        twoDigits(value) {
            if (value.toString().length <= 1) {
                return '0' + value.toString()
            }
            return value.toString()
        }
    },
    destroyed() {
        clearInterval(interval);
    }
})

Vue.component('timer', {
    template: `
		<div class="timer">
			{{ timer | minutesAndSeconds }}
		</div>
	`,
    props: {
        time: Number
    },
    data() {
        return {
            timer: this.time
        }
    },
    filters: {
        minutesAndSeconds(value) {
            var minutes = Math.floor(parseInt(value, 10) / 60)
            var seconds = parseInt(value, 10) - minutes * 60
            return `${minutes}:${seconds}`
        }
    },
    mounted() {
        setInterval(() => {
            this.timer -= 1
        }, 1000)
    },
    methods: {
        startcounter() {
            if (this.time > 0) {

            }
            this.timer = this.time

        },
    },

})

Vue.component('employeeCard', {
    props: ["employees", "total"],
    template: `
		<div class="card">
            <img style="margin-left: 4.4em;
            margin-right: 4.4em;" :src="'https://web.pt-saa.com/img/hrm/emp/'+ employees.path_photo" alt="Photo" class="card-img-top" v-if="employees.path_photo != null" />
			<div class="card-body">
			<h4 class="card-title" style="text-align: center;text-transform: capitalize;">{{employees.emp_name}}</h4>
			<h5 class="card-text" style="text-align: center;">{{employees.position}}</h5>
			<h6 class="card-text" style="text-align: center;">{{employees.nik}}</h6>
			<a href="#" style="margin-left: 4.5em;" class="btn btn-default btn-flat btn-md">Total Nilai: {{total}}</a>
			</div>
		</div>
	`
})

Vue.component('questions', {
    template: `
		<div class="card-pertanyaan">
            <div class="row">
                <transition name="slide-fade">
                    <div class="col-md-12" style="transition-duration: 100s;" v-show="show">
                        <p class="pertanyaan"><i class="fa fa-quote-left" aria-hidden="true"></i> {{ pertanyaan }}</p>
                    </div>
                </transition>
			</div>
			<div class="row">
				<div class="col-md-12">
					<slot :selectedvalue="nilaipilihan"></slot>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<p class="nilaitotal">Nilai: <span class="label label-info">{{totalNilai}}</span></p>
				</div>
			</div>
		</div>
	`,
    props: ['step', 'nilai', 'pertanyaan', 'bobot', 'show'],
    data() {
        return {

        };
    },
    computed: {
        totalNilai() {
            const total = this.nilai * this.bobot
            return total
        }
    },
    methods: {
        nilaipilihan({ val }) {
            this.nilai = val
        }
    },
    created() {

    }
})

Vue.component('nilaiIndikator', {
    template: `
		<div>
			<ul class="nilaiindikator">
				<li 
				<li 
				v-for="item in items"
				:key="item.id"
				:class="item.class"
				@click="set_active_id(item.id)"
				>
					<p class="no">{{ item.id }}</p>
					<p style="font-size: 10px;text-align: center;line-height: 1em;">
						{{item.ket}}
					</p>
				</li>
			</ul>
		</div>
	`,
    props: {
        items: Array
    },
    data() {
        return {
            previous_active_id: 1
        }
    },
    methods: {
        set_active_id(id) {
            // if (this.previous_active_id === id) return //no need to go further
            this.items.find(item => item.id === this.previous_active_id).class = '' //remove the active class from old active li
            this.items.find(item => item.id === id).class = 'active' //set active class to new li
            this.previous_active_id = id //store the new active li id
            // console.log(id)
            this.$emit('selectedvalue', { val: id })
        },

    },
    created() {
        console.log(this.nilais)
    }
})

Vue.component('btnPenilaian', {
    template: `
		<button :class="classbtn" @click.prevent="cancel()" v-if="condition">{{nama}}</button>
		<button :class="classbtn" @click.prevent="close()" v-else>{{nama}}</button>
	`,
    props: {
        classbtn: String,
        nama: String,
        nilai: Number,
        condition: Boolean
    },
    methods: {
        cancel() {
            // this.step = parseInt(this.step) + 1;
            this.$emit('cancel')
        },
        close() {
            this.$emit('close')
        }
    }
})

Vue.component('test', {
    template: `
	<div>
        <transition name="modal fade">
            <div class="modal-mask" v-if="show">
                <div class="modal-wrapper" style="margin-top: 0px;">
                <div class="modal-dialog" role="document" style="margin-top: 0px;">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: #3087bd; height: 85px;">
                            <div class="row">
                                <div class="col-sm-3">
                                    <i class="fa fa-exclamation-triangle pull-right" style="font-size: xxx-large; color:white; padding-top: 10px;"></i>
                                </div>
                                <div class="col-sm-8">
                                    <h3 class="modal-title" style="text-align: left;font-size: xx-large; font-family: system-ui; font-weight: bold; color: white;">TATA CARA PENILAIAN KARYAWAN</h3>
                                    <h5 style="margin-top: 0px;padding-top: 0px; color: white; text-align: left; font-size: 13px;">Isilah kolom Nilai Staff dengan nilai 1 - 5. Adapun penjelasan angka penilaian adalah seperti berikut:</h5>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body" style="background: white; padding-top: 1px;">
                            <table class="table" style="margin-bottom: -10px; margin-top:0px;">
                                <thead>
                                    <tr>
                                        <th width="20%" class="text-center">Nilai Staff</th>
                                        <th>Keterangan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center" style="padding-bottom: 0px;">
                                            <span style="font-size: 25px;font-weight: bold; font-family: fantasy;">1</span><br />
                                            <span style="font-size: 14px; font-weight: bold;">KURANG</span></br>
                                            <span style="font-size: 14px; font-weight: bold;">SEKALI</span>
                                        </td>
                                        <td width="80%" style="font-size: 1em">
                                            <p style="font-size: 18px;">Kinerja karyawan menunjukan kekurangan yang signifikan. pada level ini tidak dapat dibiarkan, karena menimbulkan hambatan dalam operasional.</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center" style="padding-bottom: 0px;">
                                            <span style="font-size: 25px;font-weight: bold; font-family: fantasy;">2</span><br />
                                            <span style="font-size: 14px; font-weight: bold;">KURANG</span>

                                        </td>
                                        <td width="80%" style="font-size: 1em">
                                            <p style="font-size: 18px;">Kinerja karyawan tidak memenuhi level yang diharapkan, memerlukan pengembangan untuk meningkat ke tingkat yang sesuai harapan</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center" style="padding-bottom: 0px;">
                                            <span style="font-size: 25px;font-weight: bold; font-family: fantasy;">3</span><br />
                                            <span style="font-size: 14px; font-weight: bold;">BAIK</span>
                                        </td>
                                        <td width="80%" style="font-size: 1em">
                                            <p style="font-size: 18px;">Karyawan kompeten pada tingkat kinerja yang diharapkan. kinerja karyawan baik, berhasil menyelesaikan tujuan dan tantangan yang diuraikan selama periode penilaian.</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center" style="padding-bottom: 0px;">
                                            <span style="font-size: 25px;font-weight: bold; font-family: fantasy;">4</span><br />
                                            <span style="font-size: 14px; font-weight: bold;">BAIK</span></br>
                                            <span style="font-size: 14px; font-weight: bold;">SEKALI</span>
                                        </td>
                                        <td width="80%" style="font-size: 1em padding-bottom: 0px;">
                                            <p style="font-size: 18px;">
                                                Kinerja keseluruhan tugas dan tanggung jawab melebihi standar. 
                                                Berkonstribusi pada pencapaian misi, tujuan, dan obyektif.
                                                Pada nilai ini menunjukkan kinerja keseluruhan karyawan
                                                berada pada level yang melebihi harapan yang dinyatakan
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                            <span style="font-size: 25px;font-weight: bold; font-family: fantasy;">5</span><br />
                                            <span style="font-size: 14px; font-weight: bold;">ISTIMEWA</span>
                                        </td>
                                        <td width="80%" style="font-size: 1em">
                                            <p style="font-size: 18px;">
                                                Kinerja secara signifikan melebihi standar harapan, dan diakui oleh banyak team,
                                                rekan internal dan rekan exsternal, membawa dampak positif terahadap organisa
                                                Karyawan merancang dan mengimplementasimentasikan perbaikan
                                                yang menghasilkan peningkatan / kerberhasilan organisasi
                                            </p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger btn-lg" @click="close()">Nanti Saja</button>
                            <button type="button" class="btn btn-danger btn-lg" @click="start()">Mengerti & Nilai</button>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </transition> 
	</div>    
    `,
    data() {
        return {
            show: false,
            id_target: 0,
            id_level: 0,
            type_penilai: '',
            thn_periode: ''
        };
    },
    created() {
        EventBus.$on("open_tata_cara", (open, id_target, id_level, type_penilai) => {
            this.show = open;
            this.id_target = id_target;
            this.id_level = id_level;
            this.type_penilai = type_penilai;
        });
        // console.log(this.show)
    },
    methods: {
        close() {
            if (this.show == false) {
                return this.show = true;
            } else {
                return this.show = false;
            }
        },
        start() {
            const open = true;
            EventBus.$emit("open_modal_nilai_employee", open, this.id_target, this.id_level, this.type_penilai)
            this.show = false;
        }
    }
})

Vue.component('nilaiWizard', {
    template: `
			<div>
				<div v-if="step === 1">
				
					<button @click.prevent="next()" class="btn btn-primary btn-lg" :disabled="nilai === 0">Berikutnya</button>
				
				</div>

                <div v-if="step > 1 && step < totalsteps">
                
				
					<button @click.prevent="prev()" class="btn btn-primary btn-lg">Sebelumnya </button>
					<button @click.prevent="next()" class="btn btn-primary btn-lg" :disabled="nilai === 0">Berikutnya </button>
				
				</div>
				<div v-if="step == totalsteps">
					<button @click.prevent="prev()" class="btn btn-primary btn-lg">Sebelumnya </button>
					<button @click.prevent="save()" class="btn btn-warning btn-lg" :disabled="nilai === 0">Simpan Penilaian </button>
				</div>

			</div>
	`,
    props: {
        totalsteps: Number,
        startstep: Number,
        nilai: Number,
    },
    data() {
        return {
            step: this.startstep,
            thn_periode: '',
        }
    },
    methods: {
        prev() {
            this.step = parseInt(this.step) - 1;
            this.$emit('wizard', { stepping: this.step, action: 1 })
            // console.log('step:' + this.step + ', prev: ' + this.step)
        },
        next() {
            this.step = parseInt(this.step) + 1;
            this.$emit('wizard', { stepping: this.step, action: 2 })
            // console.log('step:' + this.step + ', next: ' + this.step)
        },
        save() {
            this.step = parseInt(this.step) + 1;
            this.$emit('wizard', { stepping: this.step, action: 3 })
        }
    }
});

Vue.component('thanks_card', {
    template: `
    <div class="card" style="margin-left: 40px;">
        <h1 class="display-3">Terima Kasih</h1>
        <hr>
    </div>
    `
})

Vue.mixin({
    data() {
        return {
            action: '../../hrm/_component/orgChartData_2.php',
        }
    }
});

Vue.component('struktur-org', {
    template: `
	<div><transition name="fade">
        <div style="transition-duration: 0.2s;" class="nav-tabs-custom" v-if="isShow">
            <div class="tab-content" style="border-top: 1px solid #eee;">
                <h3 style="margin-bottom: -10px;margin-top: 0px;"><i class="fa fa-table"></i> STRUKTUR ORGANISASI</h3>
                <button class="btn btn-info btn-sm pull-right" @click="openStrukturBaru" style="margin-top:-20px"><i class="fa fa-plus"></i></button>
                <hr />
				<div>
					<div class="row">
						<div class="col-md-12">
							<v-client-table :data="data" :columns="columns" :options="options" @click="sort('dept')"></v-client-table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</transition></div>
	`,
    data() {
        return {
            data: [],
            currentSort: 'dept',
            currentSortDir: 'asc',
            isShow: true,
            columns: ['dept', 'div', 'pos', 'level', 'rpt_pos', 'action'],
            options: {
                templates: {
                    action: "action-component"
                },
                headings: {
                    dept: "Departemen",
                    div: "Divisi",
                    pos: "Posisi",
                    level: "Level",
                    rpt_pos: "Report To",
                    action: "#",
                },
                sortable: ['dept', 'div', 'pos', 'level', 'rpt_pos']
            }
        }
    },
    methods: {
        getData() {
            axios.post(this.action, { action: "getData" })
                .then(response => {
                    this.data = response.data;
                    // console.log(this.data)
                });
        },
        sort: function (s) {
            //if s == current sort, reverse
            if (s === this.currentSort) {
                this.currentSortDir = this.currentSortDir === 'asc' ? 'desc' : 'asc';
            }
            this.currentSort = s;
        },
        openStrukturBaru() {
            EventBus.$emit('struktur-baru', this.isShow)
        }

    },
    computed: {
        sortedOrg: function () {
            return this.data.sort((a, b) => {
                let modifier = 1;
                if (this.currentSortDir === 'desc') modifier = -1;
                if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
                if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
                return 0;
            });
        },
    },
    created() {
        this.getData()
        EventBus.$on('struktur-org', (struktur_org) => {
            // console.log(detail_karyawan)
            this.isShow = struktur_org
        })
        EventBus.$on('struktur-org', (isShow) => {
            this.isShow = isShow
            this.getData()
        })
        EventBus.$on('struktur-org', (isShow) => {
            this.isShow = isShow
            this.getData()
        })
        EventBus.$on('struktur-org', (isShow) => {
            this.isShow = isShow
            this.getData()
        })
    }
})

Vue.component('struktur-baru', {
    props: ["username"],
    template: `
	<div><transition name="fade">
        <div style="transition-duration: 0.2s;" class="nav-tabs-custom" v-if="isShow">
            <div class="tab-content" style="border-top: 1px solid #eee;">
                <h3 style="margin-bottom: -10px;margin-top: 0px;"><i class="fa fa-table"></i> Struktur Baru</h3>
                <hr />
                <div>
					<div class="row">
						<div class="col-md-12">
							<table class="table table-hover table-striped">
								<thead>
									<tr>
										<th>Departement</th>
										<th>Divisi</th>
										<th>Posisi</th>
										<th>Level</th>
										<th>Report To</th>
										<th>&nbsp;</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="(item, i) in formData.infos" :key="i">
										<td>
											<v-select 
												v-model="formData.infos[i].dept" 
												:options="department" 
												label="dept"
												:reduce="dept => dept.kd_dept"
												@input="getDiv(formData.infos[i].dept)">
											</v-select>
										</td>
										<td>
											<v-select 
												v-model="formData.infos[i].div" 
												:options="division"
												label="division" 
												:reduce="division => division.kd_div"
												@input="getPos(formData.infos[i].div)">
												
											</v-select>
										</td>
										<td>
											<v-select 
												v-model="formData.infos[i].pos" 
												:options="position" 
												label="position"
												:reduce="position => position.kd_pos"
												@input="getLevel(formData.infos[i].pos)">
											</v-select>
										</td>
										<td>
											<v-select 
												v-model="formData.infos[i].level" 
												:options="level"
												label="level"
												:reduce="level => level.kd_level">
											</v-select>
										</td>
										<td>
											<v-select
												ref="atasan"
												v-model="formData.infos[i].rpt_to"
												:options="atasan"
												:reduce="position => position.kd_pos"
												label="pos" 
												@input="getDetail(formData.infos[i].rpt_to)">
											</v-select>
										</td>
										<td>
											<button 
												class="btn btn-sm btn-danger"
												@click="removeItem(i)">
												<i class="fa fa-minus"></i>
											</button>
											<button 
												class="btn btn-sm btn-primary"
												@click="addItem()">
												<i class="fa fa-plus"></i>
											</button>
										</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="6">&nbsp;</td>
									</tr>
								</tfoot>
							</table>
							<div style="margin-top: -4em;" class="pull-right">
								<button style="margin-top: 10px;" type="button" class="btn btn-sm btn-info" @click="saveOrgChart()">SIMPAN</button>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
    </transition></div>
	`,
    data() {
        return {
            isShow: false,
            // id: vdp_translation_id.js,
            loading: false,
            department: [],
            division: [],
            position: [],
            level: [],
            atasan: [],
            formData: {
                infos: [
                    {
                        dept: '',
                        div: '',
                        pos: '',
                        level: '',
                        rpt_to: '',
                        // rpt_kd_dept: '',
                        // rpt_kd_level: ''
                    }
                ],
            }
        }
    },
    // validations: {
    //     formData: {
    //         infos: {
    //             $each: {
    //                 dept: { required },
    //                 div: { required },
    //                 pos: { required },
    //                 level: { required },
    //                 rpt_to: { required }
    //             }
    //         }
    //     }
    // },
    methods: {
        addItem() {
            this.formData.infos.push({
                dept: '',
                div: '',
                pos: '',
                level: '',
                rpt_to: '',
                // rpt_kd_dept: '',
                // rpt_kd_level: ''
            });
        },
        removeItem(i) {
            let infosLength = this.formData.infos.length;
            if (infosLength === 1) {
                return
            } else {
                this.$nextTick(() => {
                    this.formData.infos.splice(i, 1);
                });
            }
        },
        getNotes() {
            let frmData = { ...this.formData }

            frmData.action = 'getNotes';

            axios.post(this.action, frmData)
                .then(response => {
                    this.data = response.data;
                    // console.log(this.data)
                });
        },
        getDept() {

            axios.post(this.action, { action: "getDept" })
                .then(response => {
                    // console.log(response.data)
                    this.department = response.data
                });
        },
        getDiv(val) {

            axios.post(this.action, { action: "getDiv", kd_dept: val })
                .then(response => {
                    // console.log(response.data)
                    this.division = response.data
                });
        },
        getPos(val) {

            axios.post(this.action, { action: "getPos", kd_div: val })
                .then(response => {
                    console.log(response.data)
                    this.position = response.data
                });
        },
        getLevel(val) {

            axios.post(this.action, { action: "getLevel", kd_pos: val })
                .then(response => {
                    this.level = response.data
                });
        },
        getAtasan() {

            axios.post(this.action, { action: "getAtasan" })
                .then(response => {
                    this.atasan = response.data
                });
        },

        saveOrgChart() {
            axios.post(this.action, { action: "saveOrgChart", form: JSON.stringify(this.formData.infos), username: this.username })
                .then(response => {
                    console.log(response.data)
                    swal.fire({
                        title: "Berhasil!",
                        text: "Struktur Baru berhasil disimpan!",
                        icon: "success"
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            this.isShow = false
                            EventBus.$emit('struktur-org', this.isShow = true)
                        }
                    });
                });
        },

        getDetail(val) {
            console.log(val)
            // axios.post(this.action, { action: "getDetail", kd_pos: val })
            // 	.then(response => {
            // 		console.log(response.data)
            // 		for (var i = 0; i < this.formData.infos[i].length; i++) {
            // 			console.log(this.formData.infos[i])
            // 			this.formData.infos[i].rpt_kd_level = response.data[0].kd_level
            // 			this.formData.infos[i].rpt_kd_dept = response.data[0].kd_dept
            // 		}
            // 	});
        }
    },
    created() {
        EventBus.$on('struktur-baru', (isShow) => {
            this.isShow = isShow
            this.getDept()
            // this.getDiv()
            // this.getPos()
            // this.getLevel()
            this.getAtasan()
            this.getNotes()
        })
        EventBus.$on('struktur-baru', (struktur_org) => {
            // console.log(detail_karyawan)
            this.isShow = struktur_org
        })
    }
})

Vue.component('action-component', {
    props: ["data", "index", "columns"],
    template: `
    <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
        <div class="btn-group mr-2" role="group" aria-label="Second group">
            <button type="button" class="btn btn-xs btn-warning" @click="edit()"><i class="fa fa-edit"></i></button>
        </div>
        <div class="btn-group mr-2" role="group" aria-label="Second group">
            <button type="button" class="btn btn-xs btn-danger" @click="deleteButton()"><i class="fa fa-trash"></i></button>
        </div>
    </div>
    `,
    data() {
        return {
            atasan: []
        }
    },
    methods: {
        edit() {
            axios.post(this.action, { action: "getAtasan" })
                .then(response => {
                    this.atasan = response.data
                    EventBus.$emit('edit-component', this.data, this.atasan);
                });
        },
        deleteButton() {
            EventBus.$emit('hapus_component', this.data.id);
        }
    }
})

Vue.component('edit-component', {
    props: ["username"],
    template: `
	<div class="modal fade in" id="edit-component" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header bg-aqua">
					<h3 class="modal-title">Update Organisasi</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<form>
								<div class="form-row">
									<div class="form-group col-md-6">
										<label for="">Departemen</label>
										<input type="text" class="form-control" v-model="detail.dept" readonly>
									</div>
								
									<div class="form-group col-md-6">
										<label for="">Divisi</label>
										<input type="text" class="form-control" v-model="detail.div" readonly>
									</div>
									
									<div class="form-group col-md-4">
										<label for="">Posisi</label>
										<input type="text" class="form-control" v-model="detail.pos" readonly>
									</div>
		
									<div class="form-group col-md-4">
										<label for="">Level</label>
										<input type="text" class="form-control" v-model="detail.level" readonly>
									</div>
		
									<div class="form-group col-md-4">
										<label for="">Report To</label>
										<v-select
											ref="atasan"
											v-model="rpt_kd_pos"
											:options="atasan"
                                            :reduce="position => position.kd_pos" 
                                            :key="rpt_kd_pos"
											label="pos" 
											@input="getDetail(rpt_kd_pos)">
										</v-select>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-sm btn-info" @click="updateChart()">SIMPAN</button>
				</div>
			</div>
		</div>
	</div>
	`,
    data() {
        return {
            atasan: [],
            detail: [],
            rpt_kd_pos: '',
            rpt_kd_level: '',
            rpt_kd_dept: ''
        }
    },
    created() {
        this.atasan = []
        EventBus.$on('edit-component', (data, atasan) => {
            this.atasan = atasan
            this.detail = data
            console.log(this.detail)
            this.clearModel()
            this.openModal()
        })
    },
    methods: {
        clearModel() {
            this.rpt_kd_pos = ''
            this.rpt_kd_level = ''
            this.rpt_kd_dept = ''
        },
        openModal() {
            $("#edit-component").modal("show");
        },
        getDetail(val) {
            console.log(val)
            axios.post(this.action, { action: "getDetail", kd_pos: val })
                .then(response => {
                    console.log(response.data)
                    this.rpt_kd_level = response.data[0].kd_level
                    this.rpt_kd_dept = response.data[0].kd_dept
                });
        },
        updateChart() {
            axios.post(this.action, {
                action: "updateChart",
                rpt_kd_pos: this.rpt_kd_pos,
                rpt_kd_level: this.rpt_kd_level,
                rpt_kd_dept: this.rpt_kd_dept,
                id: this.detail.id,
                username: this.username
            })
                .then(response => {
                    this.rpt_kd_pos = ''
                    this.rpt_kd_level = ''
                    this.rpt_kd_dept = ''
                    swal.fire({
                        title: "Berhasil!",
                        text: "Struktur Organisasi berhasil diperbarui!",
                        icon: "success",
                        button: "info"
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            // location.reload();
                            EventBus.$emit('struktur-org', isShow = true)
                            $("#edit-component").modal("hide")
                            this.atasan = [];
                        }
                    });
                });
        }
    },
})

Vue.component('hapus_component', {
    props: ["data", "username"],
    template: `<br>`,
    data() {
        return {
            id: 0
        }
    },
    created() {
        EventBus.$on('hapus_component', (data) => {
            this.id = data
            console.log(this.id)
            this.deleteFunc()
        })
    },
    methods: {
        deleteFunc() {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You want to delete this data?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    axios.post(this.action, { action: "deleteData", id: this.id })
                        .then(response => {
                            swal.fire({
                                title: "Terhapus!",
                                text: "Data berhasil di hapus!",
                                icon: "success",
                            }).then(function (isConfirm) {
                                if (isConfirm) {
                                    // location.reload();
                                    EventBus.$emit('struktur-org', isShow = true)
                                }
                            });
                        }).catch(error => {
                            console.log(error)
                        })
                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your data is safe :)',
                        'error'
                    )
                }
            })
        }
    }
})

Vue.component('laporan_component', {
    template: `
    <div>
        <transition name="fade">
            <div style="transition-duration: 0.15s;" class="nav-tabs-custom" v-if="isOpen">
                <div class="tab-content" style="border-top: 1px solid #eee; margin-top: -4em;">
                    <h3 style="margin-bottom: -10px;margin-top: 0px;">Laporan Penilaian Karyawan</h3>
                    <div class="row">
                        <div class="col-sm-1">
                            <h4>Periode</h4>
                        </div>
                        <div class="col-sm-2">
                            <v-select style="text-transform: capitalize" label="tahun" :options="periode" v-model="thn_periode" :reduce="periode=>periode.tahun"></v-select>
                        </div>
                        <div class="col-sm-9" style="text-align: end;">
                            <button class="btn btn-primary btn-sm" @click="refreshAll"><i class="fa fa-refresh"></i></button>
                        </div>
                    </div>
                    </br>
                    <div class="row">
                        <div class="lds-facebook" v-if="isLoading"><div></div><div></div><div></div></div>
                        <div class="col-md-12">
                            <v-client-table :data="employees" :columns="columns" :options="options">
                                <button type="button" class="btn btn-xs btn-success" slot="action" slot-scope="props" target="_blank" @click="detail_laporan(props.row)">DETAIL PENILAIAN</button>
                            </v-client-table>
                        </div>
                    </div>
                </div>
            </div>
        </transition>
    </div>
    `,
    data() {
        return {
            isOpen: true,
            isLoading: false,
            employees: [],
            new_employee:[],
            hasil_employee: [],
            pricieList: [],
            xtotal_nilai: [],
            arr_xtotal_nilai: [],
            columns: ['office', 'emp_name', 'nik', 'department', 'level', 'total_penilai', 'total_penilai_hrd', 'action'],
            options: {
                sortable: [],
                headings: {
                    office: 'Lokasi',
                    emp_name: 'Nama Karyawan',
                    nik: 'NIK',
                    department: 'Departemen',
                    level: 'Level',
                    total_penilai: 'Jumlah Penilai',
                    total_penilai_hrd: 'Jumlah Penilai HRD',
                    action: '#'
                },
                templates: {
                    emp_name: 'emp_component',
                    // action: 'action_component'
                },
                customFilters: ['office', 'emp_name']
            },
            isShow: true,
            thn_periode: '',
            periode: []
        }
    },
    methods: {
        getDateNow() {
            const today = new Date();
			const date = today.getFullYear();

            this.thn_periode = date;
        },
        getperiode() {
            axios.post('_component/data_penilaian_2.php', {
                request: "getDatesFromRange",
            }).then(response => {
                this.periode = response.data;
                console.log(this.periode);
            }).catch(error => {
                console.log(error);
            });
        },
        getDataKaryawan() {
            this.isLoading = true;

            axios.post('_component/data_penilaian_2.php', {
                request: "getlistemployeePenilaianNew",
                periode: this.thn_periode
            }).then(response => {
                this.isLoading = false;
                this.employees = response.data[0];
            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        detail_laporan(val) {
            const open = true
            EventBus.$emit('open_detail_laporan', open, val, this.thn_periode);
            this.isOpen = false;
        },
        refreshAll() {
            this.getDataKaryawan();
            this.new_employee = this.employees;

            axios.post('_component/data_penilaian_2.php', {
                request: "updateAllLaporanPenilaian",
                employee_all: JSON.stringify(this.new_employee)
            }).then(response => {
                // this.isLoading = false;
                this.pricieList = response.data[0];

                for (let index = 0; index < this.pricieList.length; index++) {

                    const id_num_target_all = this.pricieList[index];

                    axios.post('_component/data_penilaian_2.php', {
                        request: "laporanGrandTotalPenilaian",
                        id_num_target: id_num_target_all.id_num_target,
                        periode: this.thn_periode
                    }).then(response => {
    
                        this.isLoading = false;
                        this.xtotal_nilai = response.data[1];

                        axios.post('../../hrm/_component/data_penilaian_2.php', {
                            request: "saveAllGrandTotal",
                            id_num_target: id_num_target_all.id_num_target,
                            all_grand_total: JSON.stringify(this.xtotal_nilai),
                            periode: this.thn_periode
                        }).then(response => {
                            
                            // console.log(this.xtotal_nilai);
            
                        }).catch(error => {
                            console.log(error);
                            this.isLoading = false;
                        });
    
                    }).catch(error => {
                        console.log(error);
                        this.isLoading = false;
                    });
                }

            }).catch(error => {
                console.log(error);
            });
        }
    },
    created() {
        this.getDateNow();
        this.getperiode();
        this.getDataKaryawan();
        EventBus.$on('open_laporan', (open) => {
            this.getDataKaryawan();
            this.isOpen = open;
        });
        EventBus.$on('close_laporan', (close) => {
            this.isOpen = close;
        })
    }
});

Vue.component('detail_laporan', {
    template: `
    <div>
        <transition name="fade">
            <div style="transition-duration: 0.15s;" class="nav-tabs-custom" v-if="isOpen">
                <div class="tab-content" style="border-top: 1px solid #eee; margin-top: -4em;">
                    <h3 style="margin-bottom: -10px;margin-top: 0px;">Detail Penilaian Karyawan</h3>
                    <div class="btn-toolbar pull-left" role="toolbar" aria-label="Toolbar with button groups" style="margin-bottom: 20px;">
                        <div class="btn-group mr-2" role="group" aria-label="First group" style="font-size: 18px;">
                            Jumlah Penilai : 
                        </div>
                        <div class="btn-group mr-2" role="group" aria-label="Second group" style="font-size: 18px;">
                            {{sudahNilai}} / {{jumlahNilai}} 
                        </div>
                    </div>
                    <div class="btn-toolbar pull-right" role="toolbar" aria-label="Toolbar with button groups" style="margin-bottom: 20px;">
                        <div class="btn-group mr-2" role="group" aria-label="First group">
                            <button class="btn btn-primary btn-sm pull-right" @click="refresh(detail_data.id_num)"><i class="fa fa-refresh"></i></button>
                        </div>
                        <div class="btn-group mr-2" role="group" aria-label="Second group">
                            <button class="btn btn-success btn-sm pull-right" @click="tableToExcel"><i class="fa fa-download"></i></button>
                        </div>
                        <div class="btn-group" role="group" aria-label="Third group">
                            <button class="btn btn-success btn-sm pull-right" @click="reportPersonal(detail_data.id_num)"> Report </button>
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>Periode : ({{periode}})</h4>
                        </div>
                    </div>
                    <div class="row" ref="table">
                        <div class="lds-facebook" v-if="isLoading"><div></div><div></div><div></div></div>

                        <div class="col-md-12">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td rowspan="5" style="border: 1px Solid #ddd;">
                                        <img :src="'https://web.pt-saa.com/img/hrm/emp/'+ detail.path_photo" alt="Photo" style="width: 100px;" class="img-rounded" v-if="detail.path_photo != null" />
                                        <img src="https://web.pt-saa.com/img/avatar/avatar.png" alt="Photo Belum Ada" class="img-rounded" style="width: 100px;" v-else>
                                        </td>
                                        <td style="width: 17%;background-color: #f5f5f5;border: 1px Solid #ddd;">Nama</td><td style="width: 36%;border: 1px Solid #ddd; text-transform:capitalize;">{{detail.emp_name}}</td><td style="width: 17%;background-color: #f5f5f5;border: 1px Solid #ddd;">NIK</td><td style="width: 30%;border: 1px Solid #ddd;">{{detail.nik}}</td>
                                    </tr>
                                    <tr>
                                        <td style="background-color: #f5f5f5;border: 1px Solid #ddd;">Department</td><td style="border: 1px Solid #ddd;">{{detail.department}}</td><td style="background-color: #f5f5f5;border: 1px Solid #ddd;">Office</td><td style="border: 1px Solid #ddd;">{{detail.office}}</td>
                                    </tr>
                                    <tr>
                                        <td style="background-color: #f5f5f5;border: 1px Solid #ddd;">Position Name</td><td style="border: 1px Solid #ddd;">{{detail.position}}</td><td style="background-color: #f5f5f5;border: 1px Solid #ddd;">Level</td><td style="border: 1px Solid #ddd;">{{detail.level}}</td>
                                    </tr>
                                    <tr>
                                        <td style="background-color: #f5f5f5;border: 1px Solid #ddd;">Status</td><td style="border: 1px Solid #ddd; text-transform:capitalize;">{{detail.emp_sts}}</td><td style="background-color: #f5f5f5;border: 1px Solid #ddd;">Join Date</td><td style="border: 1px Solid #ddd;">{{detail.join_date}}</td>
                                    </tr>
                                </tbody>
                            </table>

                            <table class="table table-stripped" v-if="isShowTable">
                                <thead>
                                    <tr>
                                        <th scope="col">Wilayah</th>
                                        <th scope="col">Dept</th>
                                        <th scope="col">Nama Penilai</th>
                                        <th scope="col">Posisi</th>
                                        <th scope="col">Tipe Penilai</th>
                                        <th scope="col" v-for="(data, index) in headings">{{ data.indikator }}</th>  
                                        <th scope="col">Total Nilai</th>
                                        <th scope="col" colspan="2">#</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(data, index) in penilaian" style="text-transform: uppercase;">
                                        <td>{{ data.office}}</td>
                                        <td>{{ data.department }}</td>
                                        <td>{{ data.emp_name }}</td>
                                        <td>{{ data.position }}</td>
                                        <td>{{ data.type_penilai }}</td>
                                        <td scope="col" v-for="(nilai, index) in data.indikator">
                                            <div v-if="nilai.total_nilai != null">{{ nilai.total_nilai }}</div>
                                            <div v-else >0</div>
                                        </td>  
                                        <td>{{ data.grand_total_nilai_by_indikator }}</td>
                                        <td><button type="button" class="btn btn-xs btn-danger" @click="deletePenilaian(data.id_num_penilai,data.id_indikator,data.type_penilai)"> <i class="fa fa-trash"></i> </button></td>
                                        <td><button type="button" class="btn btn-xs btn-success" @click="detailRapot(data.id_num_penilai)"> <i class="fa fa-drivers-license"></i> </button></td>
                                    </tr>
                                    <tr style="text-transform: uppercase;">
                                        <td colspan="5"></td>
                                        <td style="font-weight: BOLD;">Karakter</td>  
                                        <td style="font-weight: BOLD;">Komitmen</td>
                                        <td style="font-weight: BOLD;">Kompetensi</td>
                                        <td style="font-weight: BOLD;">Kontribusi</td>
                                        </td></td>
                                    </tr>
                                    <tr style="text-transform: uppercase;">
                                        <td colspan="4"></td>
                                        <td style="font-weight: BOLD;">Total Penilaian</td>
                                        <td style="font-weight: BOLD;">{{ xtotal_nilai.total.total_karakter }}</td>
                                        <td style="font-weight: BOLD;">{{ xtotal_nilai.total.total_komitmen }}</td>
                                        <td style="font-weight: BOLD;">{{ xtotal_nilai.total.total_kompetensi }}</td>
                                        <td style="font-weight: BOLD;">{{ xtotal_nilai.total.total_kontribusi }}</td>
                                        </td></td>
                                    </tr>
                                    <tr style="text-transform: uppercase;">
                                        <td colspan="4"></td>
                                        <td style="font-weight: BOLD;">Total Nilai</td>
                                        <td style="font-weight: BOLD;">{{ xtotal_nilai.nilai.nilai_karakter }}</td>
                                        <td style="font-weight: BOLD;">{{ xtotal_nilai.nilai.nilai_komitmen }}</td>
                                        <td style="font-weight: BOLD;">{{ xtotal_nilai.nilai.nilai_kompetensi }}</td>
                                        <td style="font-weight: BOLD;">{{ xtotal_nilai.nilai.nilai_kontribusi }}</td>
                                        </td></td>
                                    </tr>
                                    </br>
                                    </br>
                                    <tr style="text-transform: uppercase;">
                                        <td colspan="4"></td>
                                        <td colspan="2" style="font-weight: BOLD;">Grand Total Nilai</td>
                                        <td colspan="1" style="font-weight: BOLD;">{{ xtotal_nilai.total_nilai }}</td>
                                        <td colspan="1" style="font-weight: BOLD;">Keterangan</td>
                                        <td colspan="2" v-bind:style="{ color: color, fontWeight:'800' }" v-if="sudahNilai == jumlahNilai">{{ xtotal_nilai.keterangan }}</td>
                                        <td colspan="2" style="font-weight: BOLD;" v-else>-</td>
                                    </tr>
                                </tbody>
                            </table>
                            <rapot_personal_detail></rapot_personal_detail>
                        </div>
                    </div>
                </div>
            </div>
        </transition>
    </div>
    `,
    data() {
        return {
            uri: 'data:application/vnd.ms-excel;base64,',
            template: '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
            base64: function (s) { return window.btoa(unescape(encodeURIComponent(s))) },
            format: function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) },
            isOpen: false,
            isLoading: false,
            isShowTable: true,
            isShowDetail: false,
            isShowTableGrandTotal: false,
            detail: [],
            // columns: ['cabang', 'dept', 'emp_name', 'tipe_penilai', 'karakter', 'kompetensi', 'komitmen', 'kontribusi', 'grand_total_nilai_by_indikator'],
            // options: {
            //     sortable: ['cabang'],
            //     headings: {
            //         cabang: 'Cabang',
            //         department: 'Department',
            //         emp_name: 'Nama Penilai',
            //         tipe_penilai: 'Tipe Penilai',
            //         karakter: 'Karakter',
            //         kompetensi: 'Kompetensi',
            //         komitmen: 'Komitmen',
            //         kontribusi: 'Kontribusi',
            //         grand_total_nilai_by_indikator: 'Total Nilai'
            //     },
            //     templates: {
            //         emp_name: 'emp_component',
            //     },
            // },
            isShow: true,
            xid_num_target: '',
            penilaian: [],
            indikator: [],
            detail_data: [],
            total_nilai: [],
            kompetensi: [],
            headings: [],
            xtotal_nilai: [],
            jumlahNilai: '',
            sudahNilai: '',
            saveAll: [],
            open: true,
            detail: [],
            color: '',
            periode: ''
        }
    },
    created() {
        EventBus.$on('open_detail_laporan', (open, val, periode) => {
            this.periode = periode;
            this.getDetailEmployee(val.id_num, open);
            this.detail_data = val;
            this.xid_num_target = val.id_num;
        });

        EventBus.$on('close_laporan_detail', (close) => {
            this.isOpen = close;
        });

        EventBus.$on('close_rapot_personal', () => {
            this.isOpen = true;
            this.isShowTable = true;
        });
    },
    methods: {
        refresh(val) {
            let open = 'open';
            this.getDetailEmployee(val, open);

            axios.post('../../hrm/_component/data_penilaian_2.php', {
                request: "saveAllGrandTotal",
                id_num_target: val,
                all_grand_total: JSON.stringify(this.xtotal_nilai),
                periode: this.periode
            }).then(response => {
                
                console.log(this.xtotal_nilai);

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getDetailEmployee(id_num, open) {
            this.isLoading = true;

            console.log(this.periode);

            axios.post('_component/data_penilaian_2.php', {
                request: "getDetailEmployee",
                id_num: id_num,
            }).then(response => {
                this.isLoading = false;
                this.detail = response.data[0];

                axios.post('_component/data_penilaian_2.php', {
                    request: "laporanGrandTotalPenilaian",
                    id_num_target: id_num,
                    periode: this.periode
                }).then(response => {
                    this.isLoading = false;
                    this.xtotal_nilai = response.data[1];
                    this.penilaian = response.data[0];
                    // this.indikator = response.data[1];
                    // this.kompetensi = response.data[2];
                    this.headings = response.data[0][0].indikator;
                    this.isOpen = open;
                    // console.log(this.penilaian);
                    // console.log(this.xtotal_nilai);
                    // <v-client-table :data="penilaian" :columns="columns" :options="options">
                    //             <button type="button" class="btn btn-xs btn-info" slot="action" slot-scope="props" target="_blank" @click="">DETAIL PENILAIAN</button>
                    //         </v-client-table>
                    this.color = this.xtotal_nilai.warna;
                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });

                axios.post('_component/data_penilaian_2.php', {
                    request: "getJumlahPenilai",
                    id_num_target: id_num,
                    periode: this.periode
                }).then(response => {
                    this.isLoading = false;

                    this.sudahNilai = response.data[0].sudah_nilai;
                    this.jumlahNilai = response.data[0].total_penilai;
                    console.log(this.sudahNilai + " / " + this.jumlahNilai)

                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });

                // axios.post('_component/data_penilaian_2.php', {
                //     request: "laporanTotalPnilaian",
                //     id_num_target: id_num,
                // }).then(response => {
                //     this.isLoading = false;
                //     this.indikator = response.data[0];
                //     this.total_nilai = response.data[1];

                //     console.log(this.total_nilai);

                // }).catch(error => {
                //     console.log(error);
                //     this.isLoading = false;
                // });

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        deletePenilaian(id_penilai, id_indikator, tipe_penilai) {

            console.log(id_penilai + "," + id_indikator + "," + tipe_penilai + "," + this.xid_num_target)

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You want to delete this data?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {

                    axios.post('_component/data_penilaian_2.php', {
                        request: "deletelaporanTotalPnilaian",
                        id_num_penilai: id_penilai,
                        id_num_target: this.xid_num_target,
                        id_indikator: id_indikator,
                        tipe_penilai: tipe_penilai
                    }).then(response => {
                        this.isLoading = false;

                        // this.refresh(this.xid_num_target);
                        axios.post('_component/data_penilaian_2.php', {
                            request: "laporanGrandTotalPenilaian",
                            id_num_target: this.xid_num_target,
                        }).then(response => {

                            this.saveAll = response.data[1];
                            // console.log(this.saveAll);								
                            axios.post('_component/data_penilaian_2.php', {
                                request: "saveAllGrandTotal",
                                id_num_target: this.xid_num_target,
                                all_grand_total: JSON.stringify(this.saveAll),
                            }).then(response => {

                                this.refresh(this.xid_num_target);

                            }).catch(error => {
                                console.log(error);
                                this.isLoading = false;
                            });

                        }).catch(error => {
                            console.log(error);
                            this.isLoading = false;
                        });

                    }).catch(error => {
                        console.log(error);
                        this.isLoading = false;
                    });

                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your data is safe :)',
                        'error'
                    )
                }
            })
        },
        detailRapot(id_penilai) {
            EventBus.$emit('open_rapot_personal_detail', id_penilai, this.xid_num_target);
            this.isShowTable = false;
            this.isShowTableGrandTotal = true;
        },
        // tableToExcel(table, name){
        //     if (!table.nodeType) table = this.$refs.table
        //     var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
        //     window.location.href = this.uri + this.base64(this.format(this.template, ctx))
        // }
        tableToExcel() {
            let id_num_target = this.xid_num_target;
            let xsudahNilai = this.sudahNilai;
            let xjumlahNilai = this.jumlahNilai;
            let xperiode = this.periode;
            var data = {
                id_num_target,
                xsudahNilai,
                xjumlahNilai,
                xperiode
            };
            $.post("_component/hasil_penilaian_excel.php",
                JSON.stringify(data),
                function (response) {
                    window.location.href = JSON.parse(response)
                }
            )
        },
        reportExcel() {
            let id_num_target = this.xid_num_target;
            var data = {
                id_num_target
            };
            $.post("_component/rapot_penilaian_excel.php",
                JSON.stringify(data),
                function (response) {
                    window.location.href = JSON.parse(response)
                }
            )
        },
        reportPersonal(id_num) {
            EventBus.$emit('open_rapot_personal', id_num);
            this.isShowTable = false;
            this.isShowTableGrandTotal = true;
        }
    },
    computed: {
        nama_karyawan() {
            return this.detail.emp_name
        }
    }
});

Vue.component('repot_personal', {
    template: 
    `
        <div>
            <div v-if="isOpen">
                <div class="btn-toolbar pull-right" role="toolbar" aria-label="Toolbar with button groups" style="margin-bottom: 20px;">
                    <div class="btn-group mr-2" role="group" aria-label="Second group">
                        <button class="btn btn-success btn-sm pull-right" @click="reportExcel"><i class="fa fa-download"></i></button>
                    </div>
                    <div class="btn-group" role="group" aria-label="Third group">
                        <button class="btn btn-danger btn-sm pull-right" @click="closeRapot">X</button>
                    </div>
                </div>
                <table class="table table-stripped">
                    <thead>
                        <tr>
                            <th scope="col">Sub Indikator</th>
                            <th scope="col">Bobot</th>
                            <th scope="col">Atasan</th>
                            <th scope="col">Bawahan</th>
                            <th scope="col">RI</th>  
                            <th scope="col">RE</th>
                            <th scope="col">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="(data, index) in dt_rapot_personal" style="text-transform: uppercase;">
                            <td>{{ data.sub_indikator}}</td>
                            <td>{{ data.bobot }}</td>
                            <td>{{ data.atasan }}</td>
                            <td>{{ data.bawahan }}</td>
                            <td>{{ data.ri }}</td>
                            <td>{{ data.re }}</td>
                            <td>{{ data.total_nilai }}</td>
                        </tr>
                        </br>
                        </br>
                        <tr style="text-transform: uppercase;">
                            <td colspan="2"></td>
                            <td style="font-weight: BOLD;">Karakter</td>  
                            <td style="font-weight: BOLD;">Komitmen</td>
                            <td style="font-weight: BOLD;">Kompetensi</td>
                            <td style="font-weight: BOLD;">Kontribusi</td>
                            </td></td>
                        </tr>
                        <tr style="text-transform: uppercase;">
                            <td colspan="1"></td>
                            <td style="font-weight: BOLD;">Total Penilaian</td>
                            <td style="font-weight: BOLD;">{{ xtotal_nilai.total.total_karakter }}</td>
                            <td style="font-weight: BOLD;">{{ xtotal_nilai.total.total_komitmen }}</td>
                            <td style="font-weight: BOLD;">{{ xtotal_nilai.total.total_kompetensi }}</td>
                            <td style="font-weight: BOLD;">{{ xtotal_nilai.total.total_kontribusi }}</td>
                            </td></td>
                        </tr>
                        <tr style="text-transform: uppercase;">
                            <td colspan="1"></td>
                            <td style="font-weight: BOLD;">Total Nilai</td>
                            <td style="font-weight: BOLD;">{{ xtotal_nilai.nilai.nilai_karakter }}</td>
                            <td style="font-weight: BOLD;">{{ xtotal_nilai.nilai.nilai_komitmen }}</td>
                            <td style="font-weight: BOLD;">{{ xtotal_nilai.nilai.nilai_kompetensi }}</td>
                            <td style="font-weight: BOLD;">{{ xtotal_nilai.nilai.nilai_kontribusi }}</td>
                            </td></td>
                        </tr>
                        </br>
                        </br>
                        <tr style="text-transform: uppercase;">
                            <td colspan="1"></td>
                            <td colspan="2" style="font-weight: BOLD;">Grand Total Nilai</td>
                            <td colspan="1" style="font-weight: BOLD;">{{ xtotal_nilai.total_nilai }}</td>
                            <td colspan="1" style="font-weight: BOLD;">Keterangan</td>
                            <td colspan="2" style="font-weight: BOLD;" v-if="sudahNilai == jumlahNilai">{{ xtotal_nilai.keterangan }}</td>
                            <td colspan="2" style="font-weight: BOLD;" v-else>-</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    `,
    data() {
        return {
            isOpen: false,
            id_num: '',
            dt_rapot_personal: [],
            xtotal_nilai: [],
            sudahNilai: 0,
            jumlahNilai: 0

        }
    },
    methods: {
        getRapotPersonal(id_num) {
            let thn_periode;
            thn_periode = '';

            const today = new Date();
            const tahun = today.getFullYear();

            thn_periode = tahun;

            console.log(id_num);
            axios.post('_component/data_penilaian_2.php', {
                request: "getRapotPersonal",
                id_num: id_num,
                periode: thn_periode
            }).then(response => {
                this.isLoading = false;
                this.dt_rapot_personal = response.data[0];

                console.log(response.data);
                this.isOpen = true;

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getDetailEmployee(id_num) {
            let thn_periode;
            thn_periode = '';

            const today = new Date();
            const tahun = today.getFullYear();

            thn_periode = tahun;

            axios.post('_component/data_penilaian_2.php', {
                request: "laporanGrandTotalPenilaian",
                id_num_target: id_num,
                periode: thn_periode
            }).then(response => {

                this.xtotal_nilai = response.data[1];

                axios.post('_component/data_penilaian_2.php', {
                    request: "getJumlahPenilai",
                    id_num_target: id_num,
                }).then(response => {
    
                    this.sudahNilai = response.data[0].sudah_nilai;
                    this.jumlahNilai = response.data[0].total_penilai;
    
                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });
            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        reportExcel() {
            let id_num_target = this.id_num;
            var data = {
                id_num_target
            };
            $.post("_component/rapot_penilaian_excel.php",
                JSON.stringify(data),
                function (response) {
                    window.location.href = JSON.parse(response)
                }
            )
        },
        closeRapot() {
            this.isOpen = false;
            EventBus.$emit('close_rapot_personal');
        }
    },
    created() {
        EventBus.$on('open_rapot_personal', (id_num) => {
            this.id_num = id_num;
            this.getDetailEmployee(id_num);
            this.getRapotPersonal(id_num);
        })
    }
});

Vue.component('rapot_personal_detail', {

    template: 
    `
        <div v-if="isOpen">
            <div class="btn-toolbar pull-right" role="toolbar" aria-label="Toolbar with button groups" style="margin-bottom: 20px;">
                <div class="btn-group mr-2" role="group" aria-label="First group">
                    <button class="btn btn-danger btn-sm pull-right" @click="closeListDetail">Kembali Ke List</button>
                </div>
            </div>
            <table class="table table-stripped">
                <thead>
                    <tr>
                        <th scope="col">Nama Penilai</th>
                        <th scope="col">Tipe Penilai</th>
                        <th scope="col">Indikator</th>
                        <th scope="col">Sub Indikator</th>
                        <th scope="col">Nilai</th>
                        <th scope="col">Bobot</th>
                        <th scope="col">Total Nilai</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(data, index) in detail" style="text-transform: uppercase;">
                        <td>{{ data.emp_name}}</td>
                        <td>{{ data.tipe_penilai }}</td>
                        <td>{{ data.indikator }}</td>
                        <td>{{ data.sub_indikator }}</td>
                        <td>{{ data.nilai }}</td>
                        <td>{{ data.bobot }}</td>
                        <td>{{ data.total_nilai }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    `,
    data() {
        return {
            isOpen: false,
            detail: []
        }
    },
    methods: {
        getRapotDetail(id_num_penilai,id_target) {

            let thn_periode;
            thn_periode = '';

            const today = new Date();
            const tahun = today.getFullYear();

            thn_periode = tahun;

            axios.post('_component/data_penilaian_2.php', {
                request: "detailRapot",
                id_num_penilai: id_num_penilai,
                id_num_target: id_target,
                periode: thn_periode
            }).then(response => {
                this.isLoading = false;

                this.detail = response.data[0];
                console.log(this.detail);

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        closeListDetail() {
            this.isOpen = false;
            EventBus.$emit('close_rapot_personal');
        }
    },
    created() {
        EventBus.$on('open_rapot_personal_detail', (id_num_penilai,id_target) => {
            this.isOpen = true;
            this.getRapotDetail(id_num_penilai,id_target);
            console.log(id_target+","+id_num_penilai);
        })
    }

});

Vue.component('laporan_all', {
    template: `
    <div>
        <transition name="fade">
            <div style="transition-duration: 0.15s;" class="nav-tabs-custom" v-if="isOpen">
                <div class="tab-content" style="border-top: 1px solid #eee; margin-top: -4em;">
                    <h3 style="margin-bottom: -10px;margin-top: 0px;">Laporan Karyawan Per Cabang</h3>
                    <hr />
                    <div class="row">
                        <div class="col-md-1">
                            <label class="control-label text-center">Periode</label>
                        </div>
                        <div class="col-md-2">
                            <v-select style="text-transform: capitalize" label="tahun" :options="periode" v-model="thn_periode" :reduce="periode=>periode.tahun"></v-select>
                        </div>
                        <div class="col-md-1">
                            <label class="control-label text-center">Cabang</label>
                        </div>
                        <div class="col-md-2">
                            <v-select style="text-transform: capitalize" label="office" :options="office" v-model="v_office" :reduce="office=>office.kd_off"></v-select>
                        </div>
                        <div class="col-md-1">
                            <label class="control-label text-center">Department</label>
                        </div>
                        <div class="col-md-3">
                            <v-select style="text-transform: capitalize" label="namadepartemen" :options="dept" v-model="v_dept" :reduce="dept=>dept.kode"></v-select>
                        </div>
                        <div class="col-md-1">
                            <button class='pull-right btn btn-primary fa fa-search' @click='showData(v_office,v_dept)'></button>
                        </div>
                        <div class="col-md-1">
                            <button class='btn btn-success fa fa-download' @click='download(v_office,v_dept)'></button>
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="lds-facebook" v-if="isLoading"><div></div><div></div><div></div></div>
                        <div class="col-md-12" style="text-transform: capitalize;" v-if="isShow">
                            <v-client-table :data="employees" :columns="columns" :options="options"></v-client-table>
                        </div>
                    </div>
                </div>
            </div>
        </transition>
    </div>
    `,
    data() {
        return {
            isOpen: true,
            isLoading: false,
            isShow: false,
            employees: [],
            columns: ['nik', 'emp_name', 'office', 'department', 'position', 'level',
                'emp_sts', 'join_date',
                'nilai_karakter', 'nilai_komitmen', 'nilai_kompetensi', 'nilai_kontribusi',
                'grand_total', 'keterangan'],
            options: {
                sortable: [],
                headings: {
                    nik: 'NIK',
                    office: 'Cabang',
                    department: 'Department',
                    emp_name: 'Nama',
                    level: 'Level',
                    position: 'Posisi',
                    join_date: 'Tgl Bergabung',
                    emp_sts: 'Status',
                    nilai_karakter: 'Karakter',
                    nilai_komitmen: 'Komitmen',
                    nilai_kompetensi: 'Kompetensi',
                    nilai_kontribusi: 'Kontribusi',
                    grand_total: 'Total',
                    keterangan: 'Keterangan Nilai'
                },
                templates: {
                    emp_name: 'emp_component',
                    // action: 'action_component'
                },
                customFilters: ['office', 'emp_name']
            },
            isShow: true,
            office: [],
            dept: [],
            v_office: '',
            v_dept: '',
            thn_periode: '',
            periode: []
        }
    },
    methods: {
        getDateNow() {
            const today = new Date();
			const date = today.getFullYear();

            this.thn_periode = date;
        },
        getperiode() {
            axios.post('_component/data_penilaian_2.php', {
                request: "getDatesFromRange",
            }).then(response => {
                this.periode = response.data;
            }).catch(error => {
                console.log(error);
            });
        },
        getDataKaryawan(office, dept) {
            this.isLoading = true;

            axios.post('_component/data_penilaian_2.php', {
                request: "getlistemployeePenilaianAll",
                office: office,
                dept: dept,
                periode: this.thn_periode
            }).then(response => {
                this.isLoading = false;
                this.employees = response.data[0];
            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getOfficeDept() {
            this.isLoading = true;

            axios.post('_component/data_penilaian_2.php', {
                request: "getOfficeDept",
                username: 'suardana'
            }).then(response => {
                this.isLoading = false;

                this.office = response.data[4];
                this.dept = response.data[1];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        showData(office, dept) {
            let xoffice = '';
            let xdept = '';

            if (this.v_office != null) {
                xoffice = office
            } else {
                xoffice = "";
            }

            if (this.v_dept != null) {
                xdept = dept
            } else {
                xdept = "";
            }
            // console.log("office : "+xoffice+" , "+"dept : "+xdept);
            this.isShow = true;
            this.getDataKaryawan(xoffice, xdept);
        },
        download(office, dept) {
            let xoffice = '';
            let xdept = '';

            if (this.v_office != null) {
                xoffice = office
            } else {
                xoffice = "";
            }

            if (this.v_dept != null) {
                xdept = dept
            } else {
                xdept = "";
            }

            var data = {
                xoffice,
                xdept
            };
            $.post("_component/laporan_penilaian_excel.php",
                JSON.stringify(data),
                function (response) {
                    window.location.href = JSON.parse(response)
                }
            )
        }
    },
    created() {
        this.getDateNow();
        this.getperiode();
        this.getOfficeDept();
        EventBus.$on('open_laporan', (open) => {
            this.getOfficeDept();
            this.isOpen = open;
        });
        EventBus.$on('close_laporan', (close) => {
            this.isOpen = close;
        });
    }
});

Vue.component('session_personal', {
    template: 
    `
    <div>
        <transition name="fade">
            <div style="transition-duration: 0.15s;" class="nav-tabs-custom">
                <div class="tab-content" style="border-top: 1px solid #eee; margin-top: -4em;">
                    <h3 style="margin-bottom: -10px;margin-top: 0px;">Pengaturan Jadwal</h3>
                    <div class="btn-toolbar pull-right" role="toolbar" aria-label="Toolbar with button groups" style="margin-bottom: 20px;">
                        <div class="btn-group mr-2" role="group" aria-label="First group">
                            <button class="btn btn-primary btn-sm pull-right" @click="open_add_jadwal">Tambah Jadwal</button>
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="lds-facebook" v-if="isLoading"><div></div><div></div><div></div></div>
                        <div class="col-md-12" v-if="isShowJadwal">
                            <form id="form" class="form-horizontal" style="background-color: rgb(221, 221, 221); border-radius: 5px; padding: 25px 40px; margin-bottom: 50px; margin-top: 3em;">
                                <div class="col-md-12">
                                    <div class="col-sm-4 pull-right">
                                        <button type="button" @click="close_add_jadwal" class="btn btn-danger pull-right">X</button>
                                    </div>
                                </div>
                                <table class="table table-sm">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>#</th>
                                            <th>Cabang</th>
                                            <th>Tanggal Mulai</th>
                                            <th>Tanggal Akhir</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(arr_data_jadwal, k) in data_jadwal" :key="k">
                                            <td scope="row" class="trashIconContainer">
                                                <button class="btn btn-warning btn-sm"  @click="deleteRow(k,arr_data_jadwal)"><i class="fa fa-trash"></i></button>
                                            </td>
                                            <td>
                                                <v-select style="text-transform: capitalize" label="office" :options="office" v-model="arr_data_jadwal.cabang" :reduce="office=>office.kd_off"></v-select>
                                            </td>
                                            <td>
                                                <vuejs-datepicker
                                                    v-model="arr_data_jadwal.tgl_awal"
                                                    :format="DatePickerFormatb"
                                                    :disabledDates="disabledDates"
                                                    :bootstrap-styling="true"
                                                    :placeholder="holderb">
                                                </vuejs-datepicker>
                                            </td>
                                            <td>
                                                <vuejs-datepicker
                                                    v-model="arr_data_jadwal.tgl_akhir"
                                                    :format="DatePickerFormatb"
                                                    :disabledDates="disabledDates"
                                                    :bootstrap-styling="true"
                                                    :placeholder="holderb">
                                                </vuejs-datepicker>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td>
                                                <button type='button' class="btn btn-info btn-sm" @click="addNewRow">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                                <td></td>
                                                <td></td>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <div class="row" style="margin-top: -4.2em;">
                                    <button style="margin-right: 2em;" type='button' class="btn btn-info btn-sm pull-right" 
                                    @click="saveJadwalData">
                                        SIMPAN
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-12">
                            <v-client-table :data="jadwal" :columns="columns" :options="options"></v-client-table>
                        </div>
                        <div class="col-md-12">
                            <delete_data_jadwal></delete_data_jadwal>
                        </div>
                    </div>
                </div>
            </div>
        </transition>
    </div>
    `,
    components: {
        vuejsDatepicker
    },
    data() {
        return {
            isLoading: false,
            isShowJadwal: false,
            jadwal: [],
            columns: ['kd_off', 'startdate', 'enddate','action_jadwal'],
            options: {
                sortable: [],
                headings: {
                    kd_off: 'Cabang',
                    startdate: 'Start Date',
                    enddate: 'End Date',
                    action_jadwal: ''
                },
                templates: {
                    action_jadwal: 'action_data_jadwal'
                }
            },
            data_jadwal: [{
                cabang: '',
                tgl_awal: '',
                tgl_akhir: ''
            }],
            holdera: "-- Pilih Bulan --",
            holderb: "-- Pilih Tanggal --",
            DatePickerFormata: 'MMMM yyyy',
            DatePickerFormatb: 'dd MMMM yyyy',
            disabledDates: {
                to: new Date(Date.now() - 8640000)
            },
            startmonth: '',
            enddate: '',
            minv: 'month',
            monthNames: [
                { value: "1", text: "January" },
                { value: "2", text: "February" },
                { value: "3", text: "March" },
                { value: "4", text: "April" },
                { value: "5", text: "May" },
                { value: "6", text: "June" },
                { value: "7", text: "July" },
                { value: "8", text: "August" },
                { value: "9", text: "September" },
                { value: "10", text: "October" },
                { value: "11", text: "November" },
                { value: "12", text: "December" }
            ],
            v_startdate: '',
            v_enddate: '',
            office: [],
            v_office: ''
        }
    },
    methods: {
        addNewRow() {
            this.data_jadwal.push({
                cabang: '',
                tgl_awal: '',
                tgl_akhir: ''
            })
        },
        deleteRow(index, data_jadwal) {
            var idx = this.data_jadwal.indexOf(data_jadwal);
            if (idx > -1) {
                this.data_jadwal.splice(idx, 1);
            }
        },
        open_add_jadwal() {
            this.isShowJadwal = true;
        },
        close_add_jadwal() {
            this.isShowJadwal = false;
        },
        saveJadwalData() {
            axios.post('_component/data_penilaian_2.php', {
                request: "saveJadwal",
                jadwal: JSON.stringify(this.data_jadwal)
            }).then(response => {
                this.getDataJadwal();
                this.isShowJadwal = false;
            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getDataJadwal() {
            this.isLoading = true;

            axios.post('_component/data_penilaian_2.php', {
                request: "getJadwalAll",
            }).then(response => {
                this.isLoading = false;
                this.jadwal = response.data[0];
                console.log(this.jadwal);
            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getOfficeDept() {
            axios.post('_component/data_penilaian_2.php', {
                request: "getOfficeDept",
                username: 'suardana'
            }).then(response => {

                this.office = response.data[4];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
    },
    created() {
        this.getDataJadwal();
        this.getOfficeDept();
        EventBus.$on('session_personal', () => {
            this.getDataJadwal();
        })
    }
});

Vue.component('action_data_jadwal', {
    props: ["data", "index", "columns"],
    // <div class="btn-group mr-2" role="group" aria-label="First group">
    //     <button type="button" @click="" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></i></button>
    // </div>
    template: `
        <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
            <div class="btn-group mr-2" role="group" aria-label="Second group">
                <button type="button" @click="deleteJadwal(data.id_setting)" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
            </div>
        </div>
    `,
    methods: {
        deleteJadwal(id) {
            EventBus.$emit('delete_data_jadwal', id)
        }
    }
});

Vue.component('delete_data_jadwal', {
    template: `<br>`,
    created() {
        EventBus.$on('delete_data_jadwal', (id) => {
            this.deleteFunc(id)
        })
    },
    methods: {
        deleteFunc(id) {
            console.log(id);
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })
            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You want to delete this data?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    axios.post('_component/data_penilaian_2.php', {
                        request: "deleteDataJadwal",
                        id_setting: id,
                    }).then(response => {

                        this.isLoading = false;

                        swal.fire({
                            title: "Terhapus!",
                            text: "Data berhasil di hapus!",
                            icon: "success",
                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                EventBus.$emit('session_personal')
                            }
                        });

                    }).catch(error => {
                        console.log(error);
                        this.isLoading = false;
                    });
                } else if (
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your data is safe :)',
                        'error'
                    )
                }
            })
        }
    }
});

Vue.component('laporan_all_pasangan_penilai', {
    template: 
    `
        <div>
            <transition name="fade">
                <div style="transition-duration: 0.15s;" class="nav-tabs-custom">
                    <div class="tab-content" style="border-top: 1px solid #eee; margin-top: -4em;">
                        <h3 style="margin-bottom: -10px;margin-top: 0px;">Laporan Pasangan Penilai Per Cabang</h3>
                        <hr />
                        <div class="row">
                            <div class="col-md-2">
                                <label class="control-label text-center">Cabang</label>
                            </div>
                            <div class="col-md-3">
                                <v-select style="text-transform: capitalize" label="office" :options="office" v-model="v_office" :reduce="office=>office.kd_off"></v-select>
                            </div>
                            <div class="col-md-1">
                                <label class="control-label text-center">Department</label>
                            </div>
                            <div class="col-md-4">
                                <v-select style="text-transform: capitalize" label="namadepartemen" :options="dept" v-model="v_dept" :reduce="dept=>dept.kode"></v-select>
                            </div>
                            <div class="col-md-1">
                                <button class='pull-right btn btn-primary fa fa-search' @click='showData(v_office,v_dept)'></button>
                            </div>
                            <div class="col-md-1">
                                <button class='btn btn-success fa fa-download' @click='download(v_office,v_dept)'></button>
                            </div>
                        </div>
                        <hr />
                        <div class="row" v-if="tableOpen">
                            <div class="lds-facebook" v-if="isLoading"><div></div><div></div><div></div></div>
                            <div class="col-md-12" style="text-transform: capitalize;" v-if="isShow">
                                <v-client-table :data="employees" :columns="columns" :options="options"></v-client-table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="lds-facebook" v-if="isLoading"><div></div><div></div><div></div></div>
                            <table class="table table-stripped table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">No.</th>
                                        <th scope="col">Nama</th>
                                        <th scope="col">Level</th>
                                        <th scope="col">Cabang</th>
                                        <th scope="col">Department</th>
                                        <th scope="col">Posisi</th>  
                                        <th scope="col">Penilai</th>
                                        <th scope="col">Tipe Penilai</th>
                                        <th scope="col">Posisi Penilai</th>
                                    </tr>
                                </thead>
                                <tbody v-for="(data, index) in employees" v-if="index >= pagestart && index <= pageend">
                                    <tr style="text-transform: capitalize;">
                                        <td>{{ index+1 }}</td>
                                        <td>{{ data.emp_name }}</td>
                                        <td>{{ data.level }}</td>
                                        <td>{{ data.office }}</td>
                                        <td>{{ data.department }}</td>
                                        <td>{{ data.position }}</td>
                                    </tr>
                                    <tr v-for="(target, index) in data.target_nilai" style="text-transform: capitalize;">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>{{target.emp_name}}</td>
                                        <td>{{target.type_penilai}}</td>
                                        <td>{{target.position}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <template>
                                    <paginate
                                    :page-count="pagipages"
                                    :page-range="3"
                                    :margin-pages="2"
                                    :click-handler="clickCallback"
                                    :prev-text="'Prev'"
                                    :next-text="'Next'"
                                    :container-class="'pagination'"
                                    :page-class="'page-item'">
                                    </paginate>
                                </template>
                            </div>
                        </div>
                    </div>
                </div>
            </transition>
        </div>
    `,
    data() {
        return {
            isOpen: true,
            tableOpen: false,
            isLoading: false,
            isShow: false,
            employees: [],
            columns: ['emp_name', 'level', 'office', 'department', 'position'],
            options: {
                sortable: [],
                headings: {
                    emp_name: 'Nama Penilai',
                    level: 'Level',
                    office: 'Cabang',
                    department: 'Department',
                    position: 'Posisi'
                },
                customFilters: ['office', 'emp_name']
            },
            office: [],
            dept: [],
            v_office: '',
            v_dept: '',
            pagecurrent: 1,
            pagestart: 0,
            pageend: 25,
            itemsperpage: 15,
        }
    },
    methods: {
        clickCallback(pageNum) {
            this.pagination(pageNum);
        },
        getPasanganPenilai(office, dept) {
            this.isLoading = true;

            axios.post('_component/data_penilaian_2.php', {
                request: "getPasanganPenilaiByCabang",
                office: office,
                dept: dept,
            }).then(response => {
                this.isLoading = false;
                this.employees = response.data[0];
                console.log(this.employees);
            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getOfficeDept() {
            this.isLoading = true;

            axios.post('_component/data_penilaian_2.php', {
                request: "getOfficeDept",
                username: 'suardana'
            }).then(response => {
                this.isLoading = false;

                this.office = response.data[4];
                this.dept = response.data[1];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        showData(office, dept) {
            let xoffice = '';
            let xdept = '';

            if (this.v_office != null) {
                xoffice = office
            } else {
                xoffice = "";
            }

            if (this.v_dept != null) {
                xdept = dept
            } else {
                xdept = "";
            }
            // console.log("office : "+xoffice+" , "+"dept : "+xdept);
            this.isShow = true;
            this.getPasanganPenilai(xoffice,xdept);
        },
        download(office, dept) {
            let xoffice = '';
            let xdept = '';

            if (this.v_office != null) {
                xoffice = office
            } else {
                xoffice = "";
            }

            if (this.v_dept != null) {
                xdept = dept
            } else {
                xdept = "";
            }

            var data = {
                xoffice,
                xdept
            };
            $.post("_component/laporan_pasangan_all.php",
                JSON.stringify(data),
                function (response) {
                    window.location.href = JSON.parse(response)
                }
            )
        },
        pagination: function pagination(index) {
            //This calculates the range of data to show depending on the selected page
            if (index > 0 && index <= this.pagipages) {
                this.pagecurrent = index;
                this.pagestart = (this.itemsperpage * index) - this.itemsperpage;
                this.pageend = (this.itemsperpage * index);
            }
        },
    },
    created() {
        this.getOfficeDept();
        EventBus.$on('open_laporan_by_cabang', (open) => {
            this.getDataKaryawan();
            this.isOpen = open;
        });
    },
    computed: {
        pagipages: function pagipage() {
            //This calculates the amount of pages
            return Math.ceil(this.employees.length / this.itemsperpage);
        }
    }
});

Vue.component('laporan_all_by_cabang', {
    template: 
    `
        <div>
            <transition name="fade">
                <div style="transition-duration: 0.15s;" class="nav-tabs-custom">
                    <div class="tab-content" style="border-top: 1px solid #eee; margin-top: -4em;">
                        <h3 style="margin-bottom: -10px;margin-top: 0px;">Laporan Penilaian Per Cabang</h3>
                        <hr />
                        <div class="row">
                            <div class="col-md-1">
                                <label class="control-label text-center">Periode</label>
                            </div>
                            <div class="col-md-2">
                                <v-select style="text-transform: capitalize" label="tahun" :options="periode" v-model="thn_periode" :reduce="periode=>periode.tahun"></v-select>
                            </div>
                            <div class="col-md-1">
                                <label class="control-label text-center">Cabang</label>
                            </div>
                            <div class="col-md-2">
                                <v-select style="text-transform: capitalize" label="office" :options="office" v-model="v_office" :reduce="office=>office.kd_off"></v-select>
                            </div>
                            <div class="col-md-1">
                                <label class="control-label text-center">Department</label>
                            </div>
                            <div class="col-md-3">
                                <v-select style="text-transform: capitalize" label="namadepartemen" :options="dept" v-model="v_dept" :reduce="dept=>dept.kode"></v-select>
                            </div>
                            <div class="col-md-1">
                                <button class='pull-right btn btn-primary fa fa-search' @click='showData(v_office,v_dept)'></button>
                            </div>
                            <div class="col-md-1">
                                <button class='btn btn-success fa fa-download' @click='download(v_office,v_dept)'></button>
                            </div>
                        </div>
                        <hr />
                        <div class="row" v-if="tableOpen">
                            <div class="lds-facebook" v-if="isLoading"><div></div><div></div><div></div></div>
                            <div class="col-md-12" style="text-transform: capitalize;" v-if="isShow">
                                <v-client-table :data="employees" :columns="columns" :options="options"></v-client-table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="lds-facebook" v-if="isLoading"><div></div><div></div><div></div></div>
                            <table class="table table-stripped table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">No.</th>
                                        <th scope="col">Nama</th>
                                        <th scope="col">Level</th>
                                        <th scope="col">Cabang</th>
                                        <th scope="col">Department</th>
                                        <th scope="col">Posisi</th>  
                                        <th scope="col">Penilai</th>
                                        <th scope="col">Tipe Penilai</th>
                                        <th scope="col">Posisi Penilai</th>
                                        <th scope="col">Status Penilaian</th>
                                    </tr>
                                </thead>
                                <tbody v-for="(data, index) in employees" v-if="index >= pagestart && index <= pageend">
                                    <tr style="text-transform: capitalize;">
                                        <td>{{ index+1 }}</td>
                                        <td>{{ data.emp_name }}</td>
                                        <td>{{ data.level }}</td>
                                        <td>{{ data.office }}</td>
                                        <td>{{ data.department }}</td>
                                        <td>{{ data.position }}</td>
                                    </tr>
                                    <tr v-for="(target, index) in data.target_nilai" style="text-transform: capitalize;">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>{{target.emp_name}}</td>
                                        <td>{{target.type_penilai}}</td>
                                        <td>{{target.position}}</td>
                                        <td v-if="target.sudah_nilai === 1" style="color: green; font-weight: bold;">Sudah</td>
                                        <td v-else style="color: red; font-weight: bold;">Belum</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <template>
                                    <paginate
                                    :page-count="pagipages"
                                    :page-range="3"
                                    :margin-pages="2"
                                    :click-handler="clickCallback"
                                    :prev-text="'Prev'"
                                    :next-text="'Next'"
                                    :container-class="'pagination'"
                                    :page-class="'page-item'">
                                    </paginate>
                                </template>
                            </div>
                        </div>
                    </div>
                </div>
            </transition>
        </div>
    `,
    data() {
        return {
            isOpen: true,
            tableOpen: false,
            isLoading: false,
            isShow: false,
            employees: [],
            columns: ['emp_name', 'level', 'office', 'department', 'position'],
            options: {
                sortable: [],
                headings: {
                    emp_name: 'Nama Penilai',
                    level: 'Level',
                    office: 'Cabang',
                    department: 'Department',
                    position: 'Posisi'
                },
                customFilters: ['office', 'emp_name']
            },
            office: [],
            dept: [],
            v_office: '',
            v_dept: '',
            pagecurrent: 1,
            pagestart: 0,
            pageend: 25,
            itemsperpage: 15,
            thn_periode: '',
            periode: []
        }
    },
    methods: {
        clickCallback(pageNum) {
            this.pagination(pageNum);
        },
        getDateNow() {
            const today = new Date();
			const date = today.getFullYear();

            this.thn_periode = date;
        },
        getperiode() {
            axios.post('_component/data_penilaian_2.php', {
                request: "getDatesFromRange",
            }).then(response => {
                this.periode = response.data;
            }).catch(error => {
                console.log(error);
            });
        },
        getPasanganPenilai(office, dept) {
            this.isLoading = true;

            axios.post('_component/data_penilaian_2.php', {
                request: "getPasanganPenilaiByCabang2",
                office: office,
                dept: dept,
                periode: this.thn_periode
            }).then(response => {
                this.isLoading = false;
                this.employees = response.data[0];
                console.log(this.employees);
            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getOfficeDept() {
            this.isLoading = true;

            axios.post('_component/data_penilaian_2.php', {
                request: "getOfficeDept",
                username: 'suardana'
            }).then(response => {
                this.isLoading = false;

                this.office = response.data[4];
                this.dept = response.data[1];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        showData(office, dept) {
            let xoffice = '';
            let xdept = '';

            if (this.v_office != null) {
                xoffice = office
            } else {
                xoffice = "";
            }

            if (this.v_dept != null) {
                xdept = dept
            } else {
                xdept = "";
            }
            // console.log("office : "+xoffice+" , "+"dept : "+xdept);
            this.isShow = true;
            this.getPasanganPenilai(xoffice,xdept);
        },
        download(office, dept) {
            let xoffice = '';
            let xdept = '';

            if (this.v_office != null) {
                xoffice = office
            } else {
                xoffice = "";
            }

            if (this.v_dept != null) {
                xdept = dept
            } else {
                xdept = "";
            }

            var data = {
                xoffice,
                xdept
            };
            $.post("_component/laporan_pasangan_all.php",
                JSON.stringify(data),
                function (response) {
                    window.location.href = JSON.parse(response)
                }
            )
        },
        pagination: function pagination(index) {
            //This calculates the range of data to show depending on the selected page
            if (index > 0 && index <= this.pagipages) {
                this.pagecurrent = index;
                this.pagestart = (this.itemsperpage * index) - this.itemsperpage;
                this.pageend = (this.itemsperpage * index);
            }
        },
    },
    created() {
        this.getDateNow();
        this.getperiode();
        this.getOfficeDept();
        EventBus.$on('open_laporan_by_cabang', (open) => {
            this.getDataKaryawan();
            this.isOpen = open;
        });
    },
    computed: {
        pagipages: function pagipage() {
            //This calculates the amount of pages
            return Math.ceil(this.employees.length / this.itemsperpage);
        }
    }
});
