<?
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(-1);

$menu_code = 'penilaian';
$accGroup = get_acc_group($menu_code, $dbh);

if ($accGroup['acc_menu']) :

  $menu_desc = $accGroup['menu_desc'];
  $acc_new = $accGroup['acc_new'];
  $acc_edit = $accGroup['acc_edit'];
  $acc_delete = $accGroup['acc_delete'];
  $acc_save = $accGroup['acc_save'];
  $acc_download = $accGroup['acc_download'];
  $cabang_awl = isset($_POST['cabang_awl']) ? $_POST['cabang_awl'] : NULL;
  $dept_awl = isset($_POST['dept_awl']) ? $_POST['dept_awl'] : NULL;
  $div_awl = isset($_POST['div_awl']) ? $_POST['div_awl'] : NULL;
  $pos_awl = isset($_POST['pos_awl']) ? $_POST['pos_awl'] : NULL;
  $date1 = isset($_POST['date1']) ? $_POST['date1'] : date('n');
  $date2 = isset($_POST['date2']) ? $_POST['date2'] : '12';

  $tgl_now = date("Y-m-d");

?>
  <link href="https://fonts.googleapis.com/css2?family=Cabin&display=swap" rel="stylesheet">
  <style type="text/css">
    table#tabledata th {
      background-color: #E8E8E8 !important;
    }

    .navbar-inverse {
      border-color: #ddd;
    }

    .navbar-inverse .navbar-nav>li>a {
      border: 1px #cccccc solid;
      color: #333333;
    }

    .navbar-inverse .navbar-nav>li>a:hover {
      background-color: #cccccc;
    }

    .lds-facebook div {
      display: inline-block;
      position: absolute;
      left: 6px;
      width: 13px;
      background: rgba(39, 34, 229, 0.3);
      animation: lds-facebook 1.2s cubic-bezier(0, 0.5, 0.5, 1) infinite;
    }

    .lds-facebook div:nth-child(1) {
      left: 6px;
      animation-delay: -0.24s;
    }

    .lds-facebook div:nth-child(2) {
      left: 26px;
      animation-delay: -0.12s;
    }

    .lds-facebook div:nth-child(3) {
      left: 45px;
      animation-delay: 0;
    }

    @keyframes lds-facebook {
      0% {
        top: 6px;
        height: 61px;
      }

      50%,
      100% {
        top: 19px;
        height: 36px;
      }
    }

    .img-staff {
      width: 120px;
    }

    .table-shopping>thead>tr>th {
      color: #333;
      font-size: 1em;
      font-weight: 600
    }

    .table-shopping>tbody>tr>td {
      font-size: 1 em;
      padding: 15px 5px 15px 5px;
    }

    .table-shopping>tbody>tr>td b {
      display: block;
      margin-bottom: 5px
    }

    .table-shopping .td-number,
    .table-shopping .td-price,
    .table-shopping .td-total {
      font-size: 1 em;
      font-weight: 300;
      min-width: 130px;
      text-align: right
    }

    .table-shopping .td-number-total {
      font-size: 1.4em;
      font-weight: 600;
      text-align: right;
      padding-top: 1.5em;
    }

    .table-shopping .td-number small,
    .table-shopping .td-price small,
    .table-shopping .td-total small {
      margin-right: 3px
    }

    .table-shopping .td-product {
      min-width: 170px;
      padding-left: 10px;
      font-size: 1 em;
      font-weight: 300;
    }

    .table-shopping .td-product strong {
      color: #403d39;
      font-size: 1em;
      font-weight: 600
    }

    .table-shopping .td-number,
    .table-shopping .td-total {
      color: #403d39;
      font-weight: 600;
      font-size: 1.0em;
    }

    .table-shopping .td-quantity {
      min-width: 200px
    }

    .table-shopping .td-quantity .btn-group {
      margin-left: 10px
    }

    .table-shopping .img-container {
      border-radius: 6px;
      display: block;
      height: 90px;
      overflow: hidden;
      width: 80px
    }

    .table-shopping .img-container img {
      width: 100%
    }

    .table-shopping .tr-actions>td {
      border-top: 0
    }

    .table-shopping>tbody>tr opened {
      background-color: #ddd;
    }

    .card-img-top {
      width: 50%;
    }

    .modal-mask {
      position: fixed;
      z-index: 9998;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background-color: rgba(0, 0, 0, .8);
      display: table;
      transition: opacity .3s ease;
    }

    .modal-wrapper {
      display: table-cell;
      vertical-align: middle;
      top: 8%;
    }

    .modal-dialog {
      width: 95%;
      border: 5px solid #000;
      -webkit-border-radius: 10px !important;
      -moz-border-radius: 10px !important;
      border-radius: 10px !important;
    }

    .modal-content {
      background: transparent;

    }

    .modal-header {
      background-color: #eee;
      -webkit-border-radius: 10px 10px 0px 0px !important;
      -moz-border-radius: 10px 10px 0px 0px !important;
      border-radius: 10px 10px 0px 0px !important;
      border: 0px;
    }

    .modal-body {
      background-color: #eee;
      border: 0px;
    }

    .modal-footer {
      border: 0px;
      background-color: #ddd;
      -webkit-border-radius: 0px 0px 10px 10px !important;
      -moz-border-radius: 0px 0px 10px 10px !important;
      border-radius: 0px 0px 10px 10px !important;
    }

    .progress {
      -webkit-border-radius: 5px !important;
      -moz-border-radius: 5px !important;
      border-radius: 5px !important;
      border: 1px solid #ccc;
      height: 40px;
    }

    .progress-bar {
      font-size: 20px;
      font-weight: bold;
      padding: 10px 10px 10px 10px;
    }

    .indikator-box {
      margin: 8px 0 0 0;
    }

    ul.indikator {
      list-style: none;
    }

    ul.indikator li {
      margin-right: 20px;
      border: 2px dashed #ccc;
      border-radius: 4px;
      display: inline;
      padding: 7px 20px;
      text-align: center;
      text-transform: uppercase;
      font-size: 20px;
      color: #ccc;
    }

    ul.indikator li.active {
      border-color: #0275d8;
      color: #fff;
      background-color: #0275d8;
      font-weight: bold;
      -webkit-box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
      -moz-box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
      box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
    }

    .timer {
      margin-left: 10px;
      margin-top: 0px;
      border: 2px solid #d9534f;
      border-radius: 4px;
      padding: 3px 10px;
      text-align: center;
      text-transform: uppercase;
      font-size: 28px;
      font-weight: bold;
      color: #FFF;
      width: 70%;
      background-color: #d9534f;
      -webkit-box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
      -moz-box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
      box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
    }

    .card {
      border-radius: 5px;
      background-color: #fff;
      padding: 20px 25px;
      width: 90%;
      text-align: left;
      -webkit-box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
      -moz-box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
      box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
    }

    .card_atas {
      border-radius: 5px;
      background-color: #fff;
      padding: 20px 25px;
      width: 90%;
      text-align: left;
      -webkit-box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
      -moz-box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
      box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
    }

    .show_atas {
      display: none;
    }

    .card-pertanyaan {
      margin-left: 4%;
      border-radius: 5px;
      background-color: #fff;
      padding: 20px 25px;
      width: 95%;
      -webkit-box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
      -moz-box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
      box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
    }

    .switch {
      position: relative;
      display: inline-block;
      width: 60px;
      height: 34px;
    }

    .switch input {
      opacity: 0;
      width: 0;
      height: 0;
    }

    .slider {
      position: absolute;
      cursor: pointer;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: #ccc;
      -webkit-transition: .4s;
      transition: .4s;
    }

    .slider:before {
      position: absolute;
      content: "";
      height: 26px;
      width: 26px;
      left: 4px;
      bottom: 4px;
      background-color: white;
      -webkit-transition: .4s;
      transition: .4s;
    }

    input:checked+.slider {
      background-color: #2196F3;
    }

    input:focus+.slider {
      box-shadow: 0 0 1px #2196F3;
    }

    input:checked+.slider:before {
      -webkit-transform: translateX(26px);
      -ms-transform: translateX(26px);
      transform: translateX(26px);
    }

    .pertanyaan {
      font-family: 'cabin', san-serif;
      font-size: 28px;
      text-align: center;
      font-weight: bold;
      padding: 8px 18px;
    }

    .pertanyaan:first-letter {
      text-transform: capitalize;
    }

    ul.nilaiindikator {
      margin-top: 20px;
      list-style: none;
    }

    ul.nilaiindikator li {
      width: 15%;
      border-radius: 5px;
      border: 1px dotted #999;
      margin-right: 20px;
      padding: 5px 10px;
      display: inline-block;
    }

    ul.nilaiindikator li:hover {
      background-color: #5bc0de;
      color: #fff;
      border: 1px solid #666;
    }

    ul.nilaiindikator li.active {
      background-color: #5bc0de;
      color: #fff;
      border: 1px solid #666;
    }

    ul.nilaiindikator li p.no {
      font-size: 30px;
      text-align: center;
      font-weight: bold;
    }

    p.nilaitotal {
      font-size: 18px;
      padding: 12px 10px 5px 10px;
      margin-right: 50px;
      text-align: right;
      font-weight: bold;
    }

    .slide-fade-enter-active {
      transition: all 1s cubic-bezier(0.3, 0.3, 0.3, 0.3);
    }

    .slide-fade-leave-active {
      transition: all 1s cubic-bezier(1.0, 0.5, 0.8, 1.0);
    }

    .slide-fade-enter,
    .slide-fade-leave-to

    /* .slide-fade-leave-active di bawah versi 2.1.8 */
      {
      transform: translateX(10px);
      opacity: 0;
    }

    @media only screen and (min-width: 600px) and (max-width: 800px) {
      .show_atas {
        display: block;
      }

      .modal-dialog {
        width: 95% !important;
        border: 5px solid #000 !important;
        -webkit-border-radius: 10px !important;
        -moz-border-radius: 10px !important;
        border-radius: 10px !important;
      }

      .timer {
        margin-left: 80%;
        margin-top: 0px;
        border: 2px solid #d9534f;
        border-radius: 4px;
        padding: 3px 10px;
        text-align: center;
        text-transform: uppercase;
        font-size: 28px;
        font-weight: bold;
        color: #FFF;
        width: 20%;
        background-color: #d9534f;
        -webkit-box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
        -moz-box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
        box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
      }

      .card {
        display: none;
        margin-top: 10px;
        margin-left: 180px;
        border-radius: 5px;
        background-color: #fff;
        padding: 20px 25px;
        width: 40%;
        text-align: left;
        -webkit-box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
        -moz-box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
        box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
      }

      .card_atas {
        display: block;
        margin-bottom: 10px;
        margin-left: 180px;
        border-radius: 5px;
        background-color: #fff;
        padding: 20px 25px;
        width: 40%;
        text-align: left;
        -webkit-box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
        -moz-box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
        box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
      }
    }

    @media only screen and (min-width: 800px) and (max-width: 1000px) {

      .show_atas {
        display: block;
      }

      .timer {
        margin-left: 80%;
        margin-top: 0px;
        border: 2px solid #d9534f;
        border-radius: 4px;
        padding: 3px 10px;
        text-align: center;
        text-transform: uppercase;
        font-size: 28px;
        font-weight: bold;
        color: #FFF;
        width: 20%;
        background-color: #d9534f;
        -webkit-box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
        -moz-box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
        box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
      }

      .card {
        display: none;
        margin-top: 3%;
        margin-left: 30%;
        border-radius: 5px;
        background-color: #fff;
        padding: 20px 25px;
        width: 40%;
        text-align: left;
        -webkit-box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
        -moz-box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
        box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
      }

      .card_atas {
        display: block;
        margin-bottom: 3%;
        margin-left: 30%;
        border-radius: 5px;
        background-color: #fff;
        padding: 20px 25px;
        width: 40%;
        text-align: left;
        -webkit-box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
        -moz-box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
        box-shadow: 9px 10px 14px -4px rgba(153, 150, 153, 1);
      }

    }
  </style>
  <aside class="content-wrapper" id="app">
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <?php print_r($_SESSION['hrm_pin']); ?>
          <penilaian_karyawan :username="username"></penilaian_karyawan>
        </div>
        <div class="col-md-12">
          <test></test>
        </div>
        <div class="col-md-12">
          <penilaian @test="test2" @resetvalue="resetValue">
            <template v-slot:header>
              <progress-bar :value="progressvalue" v-if="question"></progress-bar>
              <div class="row">
                <div class="col-md-10">
                  <indikator :indikators="tab_indikator" :v_indikator="v_indikator" v-if="question"></indikator>
                </div>
                <div class="col-md-2">
                  <timer :time="timer" v-if="question"></timer>
                </div>
            </template>
            <template v-slot:body>
              <div class="row">
                <div class="col-md-4 show_atas">
                  <employee-Card-Atas :employees="employees" :total="totalnilai">
                    </employee-card>
                </div>
                <div class="col-md-8">
                  <questions :show="show" :pertanyaan="subIndikator" :step="currentStep" :nilai="nilai" :bobot="bobot" :urutan="urutanSubIndikator" v-if="question">
                    <nilai-indikator :prev_id="indexing" :items="items" @selectedvalue="nilaipilihan"></nilai-indikator>
                  </questions>
                  <thanks_card v-if="thankyou"></thanks_card>
                </div>
                <div class="col-md-4">
                  <employee-card :employees="employees" :total="totalnilai"></employee-card>
                </div>
              </div>
              <p>&nbsp;</p>
            </template>
            <template v-slot:footer>
              <div class="row">
                <div class="col-md-8">
                  <nilai-wizard :nilai="nilai" :totalsteps="totalsteps" :startstep="currentStep" @wizard="wizardstep"></nilai-wizard>
                </div>
                <div class="col-md-4">
                  <btn-penilaian :condition="question" :classbtn="btnCancelClass" :nama="btnCancel" @cancel="cancel" v-if="question"></btn-penilaian>
                  <btn-penilaian :classbtn="btnInfoClass" :nama="btnInfo" v-else @close="tutup"></btn-penilaian>
                </div>
              </div>

            </template>
          </penilaian>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </section><!-- /.content -->
  </aside><!-- /.content-wrapper -->


  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  <script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>
  <script src="../2.4.0/plugins/slimScroll/jquery.slimscroll.js"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <script src="../cdn/vue/vue-table-2.js"></script>

  <script src="https://cdn.jsdelivr.net/npm/lodash@4.17.15/lodash.min.js"></script>
  <script src="component/penilaiankaryawan_new_b.js"></script>
  <script>
    Vue.use(VueTables.ClientTable);
    // Vue.config.devtools = false;
    // Vue.config.productionTip = false;
    // Vue.config.silent = true;
    var app = new Vue({
      el: '#app',
      data: {
        show: true,
        thankyou: false,
        question: true,
        username: '<?php echo $_SESSION['hrm_pin'] ?>',
        items: [{
            id: 1,
            ket: 'Kurang Sekali',
            class: ''
          },
          {
            id: 2,
            ket: 'Kurang',
            class: ''
          },
          {
            id: 3,
            ket: 'Baik',
            class: ''
          },
          {
            id: 4,
            ket: 'Baik Sekali',
            class: ''
          },
          {
            id: 5,
            ket: 'Istimewa',
            class: ''
          },
        ],
        timer: 900,
        btnPrev: 'Kembali',
        btnPrevClass: 'btn btn-primary btn-lg',
        btnNext: 'Lanjutkan',
        btnNextClass: 'btn btn-primary btn-lg',
        btnCancel: 'Tutup, saya akan nilai ulang',
        btnCancelClass: 'btn btn-danger btn-lg',
        btnInfo: 'Selesai',
        btnInfoClass: 'btn btn-info btn-lg',
        totalsteps: 0,
        currentStep: 1,
        nilai: 0,
        progressvalue: 0,
        allProgress: 0,
        bobot: 0,
        employees: [],
        tab_indikator: [],
        setPertanyaan: [],
        v_indikator: '',
        indexing: 0,
        subIndikator: '',
        urutanSubIndikator: '',
        max_urutan: '',
        totalnilai: 0,
        penilaian_arr: [],
        index_nilai: [],
        id_num_target: '',
        type_penilai: '',
        id_level: '',
        penilai_karyawan: [],
        detail_indikator: [],
        xtype_penilai: '',
        saveAll: []
      },
      watch: {
        // show(value) {
        // 	if(value === true) {
        // 		return this.show = false
        // 	} else {
        // 		return this.show = true
        // 	}
        // }
        currentStep(value) {
          if (value === this.totalsteps) {
            return this.progressvalue = 100;
          }
        },
        totalsteps(value) {
          if (value === this.currentStep) {
            return this.progressvalue = 100;
          }
        }
      },
      methods: {
        resetValue() {
          this.timer = 900;
          this.totalsteps = 0;
          this.currentStep = 1;
          this.nilai = 0;
          this.progressvalue = 0;
          this.allProgress = 0;
          this.bobot = 0;
          this.employees = [];
          this.tab_indikator = [];
          this.setPertanyaan = [];
          this.v_indikator = '';
          this.indexing = 0;
          this.subIndikator = '';
          this.urutanSubIndikator = '';
          this.max_urutan = '';
          this.totalnilai = 0;
          this.penilaian_arr = [];
          this.index_nilai = [];
          this.items = [{
              id: 1,
              ket: 'Kurang Sekali',
              class: ''
            },
            {
              id: 2,
              ket: 'Kurang',
              class: ''
            },
            {
              id: 3,
              ket: 'Baik',
              class: ''
            },
            {
              id: 4,
              ket: 'Baik Sekali',
              class: ''
            },
            {
              id: 5,
              ket: 'Istimewa',
              class: ''
            },
          ];
          this.id_num_target = '';
          this.type_penilai = '';
          this.id_level = '';
          this.penilai_karyawan = [];
          this.detail_indikator = [];
          this.xtype_penilai = '';
          this.saveAll = []
          this.$forceUpdate();
        },
        tutup() {
          this.resetValue();
          EventBus.$emit('close_peniliain');
          EventBus.$emit('table_penilaian');
        },
        cancel() {
          this.resetValue();
          EventBus.$emit('close_peniliain');
          EventBus.$emit('table_penilaian');
          // EventBus.$emit('cancel');
        },
        wizardstep({
          stepping,
          action,
          total
        }) {
          if (action == 1) {
            const lastNilai = this.index_nilai[this.index_nilai.length - 1];
            // console.log(lastNilai);
            this.totalnilai = this.totalnilai - lastNilai;
            this.currentStep = stepping;
            // if(this.indexing == 1) {
            // 	console.log('masuk index berapa' + this.indexing)
            // 	this.progressvalue = this.allProgress * 0;
            // } else {
            // 	this.progressvalue = this.allProgress *  this.currentStep;
            // }

            this.indexing--
            this.progressvalue = this.allProgress * this.indexing;

            this.v_indikator = this.set_pertanyaan[this.indexing].indikator

            this.subIndikator = this.set_pertanyaan[this.indexing].sub_indikator
            this.urutanSubIndikator = this.set_pertanyaan[this.indexing].urutan

            this.bobot = this.set_pertanyaan[this.indexing].bobot

            this.index_nilai.splice(-1, 1);

            this.penilaian_arr.splice(-1, 1);
          } else if (action == 2) {
            this.totalnilai += this.nilai * this.bobot;
            this.currentStep = stepping;
            // if(this.indexing == 0) {
            // 	// console.log('masuk index berapa' + this.indexing)
            // 	this.progressvalue = this.allProgress *  1;
            // } else {
            // 	// console.log('masuk index berapa' + this.indexing)
            // 	this.progressvalue = this.allProgress *  (this.currentStep - this.indexing);
            // }
            this.index_nilai.push(this.nilai * this.bobot);

            this.set_pertanyaan[this.indexing].nilai = this.nilai;
            this.set_pertanyaan[this.indexing].total = this.nilai * this.bobot;

            this.penilaian_arr.push(this.set_pertanyaan[this.indexing]);

            this.indexing++
            // step == totalsteps
            this.progressvalue = this.allProgress * this.indexing;

            this.v_indikator = this.set_pertanyaan[this.indexing].indikator

            this.subIndikator = this.set_pertanyaan[this.indexing].sub_indikator
            this.urutanSubIndikator = this.set_pertanyaan[this.indexing].urutan

            this.bobot = this.set_pertanyaan[this.indexing].bobot;

            this.nilai = 0;

            this.items = [{
                id: 1,
                ket: 'Kurang Sekali',
                class: ''
              },
              {
                id: 2,
                ket: 'Kurang',
                class: ''
              },
              {
                id: 3,
                ket: 'Baik',
                class: ''
              },
              {
                id: 4,
                ket: 'Baik Sekali',
                class: ''
              },
              {
                id: 5,
                ket: 'Istimewa',
                class: ''
              },
            ];
            console.log(this.nilai);

          } else if (action == 3) {
            this.totalnilai += this.nilai * this.bobot;
            this.currentStep = stepping;
            this.progressvalue = total;
            this.index_nilai.push(this.nilai * this.bobot);

            this.set_pertanyaan[this.indexing].nilai = this.nilai;
            this.set_pertanyaan[this.indexing].total = this.nilai * this.bobot;

            this.penilaian_arr.push(this.set_pertanyaan[this.indexing]);

            this.thankyou = true;
            this.question = false;

            // console.log("SAVE DISINI");
            // const close = false
            // EventBus.$emit('close_peniliain', close)

            let thn_periode;
            thn_periode = '';

            const today = new Date();
            const tahun = today.getFullYear();

            thn_periode = tahun;

            axios.post('../../hrm/_component/data_penilaian_2.php', {
              request: "savePenilaianNew",
              id_num_penilai: this.penilai_karyawan.id_num,
              id_num_target: this.id_num_target,
              penilaian: JSON.stringify(this.penilaian_arr),
              detail_indikator: JSON.stringify(this.detail_indikator),
              type_penilai: this.xtype_penilai,
              periode: thn_periode
            }).then(response => {

              axios.post('../../hrm/_component/data_penilaian_2.php', {
                request: "laporanGrandTotalPenilaian",
                id_num_target: this.id_num_target,
                periode: thn_periode
              }).then(response => {

                this.saveAll = response.data[1];
                axios.post('../../hrm/_component/data_penilaian_2.php', {
                  request: "saveAllGrandTotal",
                  id_num_target: this.id_num_target,
                  all_grand_total: JSON.stringify(this.saveAll),
                  periode: thn_periode
                }).then(response => {

                  console.log(this.saveAll);

                }).catch(error => {
                  console.log(error);
                  this.isLoading = false;
                });

              }).catch(error => {
                console.log(error);
                this.isLoading = false;
              });


            }).catch(error => {
              console.log(error);
              this.isLoading = false;
            });
          }
        },
        nilaipilihan({
          val
        }) {
          this.nilai = val
        },
        test2(data) {
          this.id_num_target = data.target;
          this.type_penilai = data.penilai;
          this.id_level = data.level;
          this.getIndikatorPeriode(data.level)
          this.getDetailEmployee(data.target, data.penilai, data.level)
          this.getPenilaiKaryawan();
        },
        getDetailEmployee(data, penilai, level) {
          axios.post('../../hrm/_component/data_penilaian_2.php', {
            request: "getDetailEmployee",
            id_num: data,
          }).then(response => {
            // console.log(response.data)
            // this.isLoading = false;

            this.employees = response.data[0];
            // this.indikator_karyawan = response.data[1];

            this.getIndikatorKriteria(response.data[1].kd_indikator, penilai, level);
            this.getPertanyaanAll(response.data[1].kd_indikator, penilai, level);

          }).catch(error => {
            console.log(error);
            this.isLoading = false;
          });
        },
        getIndikatorPeriode(level) {
          axios.post('../../hrm/_component/data_penilaian_2.php', {
            request: "getIndikatorPeriodePenilaian",
            id_level: level,
          }).then(response => {
            // this.isLoading = false;
            // console.log(response.data)
            // this.v_kd_indikator = response.data[0][0];
            // console.log(this.v_kd_indikator)
          }).catch(error => {
            console.log(error);
            this.isLoading = false;
          });
        },
        getPenilaiKaryawan() {
          let username = this.username;

          axios.post('../../hrm/_component/data_penilaian_2.php', {
            request: "getPenilaiKaryawan",
            username: this.username,
          }).then(response => {
            this.isLoading = false;

            this.penilai_karyawan = response.data[0];

          }).catch(error => {
            console.log(error);
            this.isLoading = false;
          });
        },
        getIndikatorKriteria(kd, type_penilai, level) {
          console.log(`${kd} ${type_penilai} ${level}`)
          axios.post('../../hrm/_component/data_penilaian_2.php', {
            request: "getIndikatorKriteria",
            kd_indikator: kd,
            type_penilai: type_penilai,
            id_level: level
          }).then(response => {
            this.isLoading = false;

            // this.v_total_indikator = response.data[0].length;
            this.tab_indikator = response.data[0];
            // console.log(response.data[0]);
            // this.getFirstIndikator(kd, type_penilai);

          }).catch(error => {
            console.log(error);
            this.isLoading = false;
          });
        },
        getPertanyaanAll(kd, type_penilai, level) {
          console.log(kd + "," + type_penilai)
          axios.post('../../hrm/_component/data_penilaian_2.php', {
            request: "getAllPertanyaan",
            kd_indikator: kd,
            type_penilai: type_penilai,
            id_level: level
          }).then(response => {
            this.isLoading = false;
            // this.show = !this.show; 

            this.set_pertanyaan = response.data[0]
            this.detail_indikator = response.data[1]
            this.xtype_penilai = response.data[2]

            this.totalsteps = response.data[0].length;
            this.allProgress = (100 / this.totalsteps) / 100;
            this.indexing = 0

            this.v_indikator = response.data[0][this.indexing].indikator
            this.bobot = response.data[0][this.indexing].bobot
            this.subIndikator = this.set_pertanyaan[this.indexing].sub_indikator
            this.urutanSubIndikator = this.set_pertanyaan[this.indexing].urutan

            console.log(response.data);

          }).catch(error => {
            console.log(error);
            this.isLoading = false;
          });
        }
      },
    });

    document.title = "<? echo $menu_desc; ?>";
  </script>


<? $dbh = null;
else :
  header('location:..');
endif;
?>