const EventBus = new Vue();


Vue.component('penilaian_karyawan', {
    template: `
    <div><transition name="fade">
            <div style="transition-duration: 0.15s;" class="nav-tabs-custom" v-if="">
                <div class="tab-content" style="border-top: 1px solid #eee;">
                    <h3 style="margin-bottom: -10px;margin-top: 0px;"><i class="fa fa-users"></i> Penilai Karyawan</h3>
                    <hr />
                    <div class="row">
                        <div class="lds-facebook" v-if="isLoading"><div></div><div></div><div></div></div>

                        <div class="col-md-12" v-if="isSession">
                            <v-client-table :data="target_karyawan" :columns="columns" :options="options">
                                <button type="button" class="btn btn-xs btn-info" slot="action" slot-scope="props" target="_blank" @click="detail_karyawan(props.row)">DETAIL KARYAWAN</button>
                            </v-client-table>
                        </div>

                        <div class="col-md-12" v-if="isWarning">
                            <h4>Sesi Penilaian Anda Belum Aktif</h4>
                        </div>
                    </div>
                </div>
            </div>
    </transition></div>`,
    props: ['username'],
    data() {
        return {
            isOpen: true,
            isLoading: false,
            isSession: false,
            isWarning: false,
            penilai_karyawan: [],
            indikator_karyawan: [],
            target_karyawan: [],
            data_session: [],
            columns: ['office', 'emp_name', 'nik', 'department', 'position',
                'level', 'total_penilaian', 'action_penilaian_karyawan'],
            options: {
                sortable: ['office', 'department', 'position'],
                headings: {
                    office: "Lokasi",
                    emp_name: "Nama Karyawan",
                    nik: "NIK",
                    department: "Departemen",
                    position: "Posisi",
                    level: "Level Target",
                    total_penilaian: "Total Nilai",
                    action_penilaian_karyawan: "#"
                },
                templates: {
                    emp_name: "emp_name-component",
                    action_penilaian_karyawan: "action_penilaian_karyawan"
                }
            }
        }
    },
    created() {
        this.getPenilaiKaryawan();
        EventBus.$on('refreshTablePenilai', () => {
            this.getPenilaiKaryawan();
        })
        EventBus.$on('table_penilaian', () => {
            this.getPenilaiKaryawan();
        });
        // this.getTargetKaryawan();
    },
    methods: {
        getPenilaiKaryawan() {
            let username = this.username;

            axios.post('../../hrm/_component/data_penilaian_2.php', {
                request: "getPenilaiKaryawan",
                username: username,
            }).then(response => {
                this.isLoading = false;

                // console.log(response.data)
                this.penilai_karyawan = response.data[0];

                this.getSession();

                this.getTargetKaryawan();

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getSession() {
            let today = this.penilai_karyawan.today;
            let kd_off = this.penilai_karyawan.kd_off;

            axios.post('../../hrm/_component/data_penilaian_2.php', {
                request: "getSession",
                today: today,
                kd_off: kd_off
            }).then(response => {
                this.isLoading = false;

                this.data_session = response.data[0];
                if(this.data_session.length > 0) {
                    this.isSession = true;
                    this.isWarning = false;
                } else {
                    this.isSession = false;
                    this.isWarning = true;
                }
                console.log(this.data_session);
                console.log(today+","+kd_off);

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getTargetKaryawan() {
            let id_num = this.penilai_karyawan.id_num;
            let kd_off = this.penilai_karyawan.kd_off;
            let kd_dept = this.penilai_karyawan.kd_dept;

            // console.log(id_num);

            let thn_periode;
            thn_periode = "";

            const today = new Date();
			const date = today.getFullYear();

            thn_periode = date;

            axios.post('../../hrm/_component/data_penilaian_2.php', {
                request: "getTargetKaryawan",
                id_num: id_num,
                periode: thn_periode
            }).then(response => {
                this.isLoading = false;

                // console.log(response.data);
                this.target_karyawan = response.data[0];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });

        },
        nilai_karyawan(id_target, id_level) {
            this.$emit('view_nilai_karyawan', [id_target, id_level]);
        }
    }
});

Vue.component('emp_name-component', {
    props: ["data", "index", "column"],
    template: `<span style="text-transform:capitalize;">{{ data.emp_name }}</span>`
})

Vue.component('action_penilaian_karyawan', {
    props: ["data", "index", "column"],
    template: `
    <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
        <div class="btn-group mr-2" role="group" aria-label="First group">
            <button v-if="data.total_penilaian == null" type="button" @click="penilai(data.id_num, data.id_level, data.type_penilai)" class="btn btn-sm btn-primary">Nilai </button>
            <button v-else type="button" class="btn btn-sm btn-success" :disabled="data.total_penilaian !== null"><i class="fa fa-check"></i> </button>
        </div>
    </div>
    `,
    data() {
        return {
            open: true,
        }
    },
    methods: {
        penilai(id_target, id_level, type_penilai) {
            // EventBus.$emit("open_modal_nilai_employee", this.open, id_target, id_level, type_penilai)
            EventBus.$emit('open_tata_cara', id_target, id_level, type_penilai);
        },
    }
})

Vue.component('nilai_employee', {
    template: `
    <div class="modal fade" id="nilai_employee" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-mask">
            <div class="modal-wrapper" style="margin-top: 0px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" v-if="isAlert"><i class="fa fa-exclamation-triangle" v-if="isAlert"></i> Tata Cara Penilaian Karyawan</h4>
                        <div class="progress" v-if="isAlert == false">
                            <div class="progress-bar progress-bar-primary progress-bar-striped active" 
                            role="progressbar" aria-valuenow="0"
                            aria-valuemin="0" aria-valuemax="0" style="width:0%">
                            {{ progressVal }}%
                            </div>
                        </div>
                        <div class="row" v-if="penilaian">
                            <div class="col-md-10">
                                <div class="indikator-box">
                                    <ul class="indikator">
                                        <li v-for="arr_indikator in tab_indikator" 
                                            :class="{'active':(v_indikator === arr_indikator.indikator)}">
                                            {{ arr_indikator.indikator }}
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-2 pull-right">
                                <div class="timer"><slot>{{ prettyTime | prettify }}</slot></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12" v-if="isAlert">
                                <h5 style="margin-top: 0px;padding-top: 0px;" v-if="isAlert">Isilah kolom Nilai Staff dengan nilai 1 - 5. Adapun penjelasan angka penilaian adalah seperti berikut:</h5>
                                <table class="table" style="margin-bottom: -10px;">
                                    <thead>
                                        <tr>
                                            <th width="20%">Nilai Staff</th>
                                            <th>Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center">
                                                <span class="badge bg-red">1</span><br />
                                                <span style="font-size: 1em"><u>TIDAK MEMUASKAN</u><br />Buruk</span>
                                            </td>
                                            <td width="80%" style="font-size: 1em; text-align: justify;">
                                                <p>Karyawan menunjukkan kekurangan yang signifikan dalam perilaku profesional atau pengetahuan pekerjaan atau kinerja. Kinerja pada level ini tidak dapat dibiarkan terus menerus karena menimbulkan hambatan dalam operasional.</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <span class="badge bg-yellow">2</span><br />
                                                <span style="font-size: 1em"><u>TIDAK SESUAI HARAPAN</u><br />Dibawah rata-rata</span>
            
                                            </td>
                                            <td width="80%" style="font-size: 1em; text-align: justify;">
                                                <p>Kinerja karyawan tidak memenuhi level yang diharapkan atau karyawan masih memerlukan
                                                pelatihan / pembelajaran dan kinerja harus ditingkatkan untuk memenuhi harapan.
                                                Karyawan di level ini adalah memerlukan pengembangan untuk meningkat ke tingkat yang sesuai harapan
                                                
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <span class="badge bg-green">3</span><br />
                                                <span style="font-size: 1em"><u>TARGET KERJA TERPENUHI</u><br />Rata-rata</span>
                                            </td>
                                            <td width="80%" style="font-size: 1em; text-align: justify;">
                                                <p>Karyawan kompeten pada tingkat kinerja yang diharapkan dan dihargai
                                                kontribusi kepada organisasi. 
                                                Kinerja konsisten dengan apa yang secara rutin diharapkan
                                                karyawan dalam posisi terkait, dan karyawan telah berhasil menyelesaikan tujuan dan
                                                tantangan yang diuraikan selama periode penilaian. 
                                                Kinerja baik, tidak rata-rata, dan
                                                individu dianggap sebagai pekerja yang stabil dan terampil.
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <span class="badge bg-light-blue">4</span><br />
                                                <span style="font-size: 1em"><u>MELEBIHI TARGET KERJA</u><br />Baik</span>
                                            </td>
                                            <td width="80%" style="font-size: 1em; text-align: justify;">
                                                <p>Kinerja keseluruhan tugas dan tanggung jawab melebihi standar kinerja yang diberikan untuk posisi tersebut. Kinerja secara teratur berkontribusi pada pencapaian
                                                misi, tujuan, dan obyektif. 
                                                Pada nilai ini menunjukkan kinerja keseluruhan
                                                karyawan berada pada level yang secara konsisten melebihi harapan yang dinyatakan.
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                <span class="badge bg-blue">5</span><br />
                                                <span style="font-size: 1em"><u>MEMBAWA PERUBAHAN</u><br />Sangat Baik</span>
                                            </td>
                                            <td width="80%" style="font-size: 1em; text-align: justify;">
                                                <p>Kinerja secara signifikan melebihi standar dan harapan. Kinerja pada level ini adalah
                                                diakui oleh banyak team, rekan internal dan rekan eksternal, membawa dampak positif terhadap organisasi.
                                                Karyawan merancang dan mengimplementasikan perbaikan yang menghasilkan peningkatan / keberhasilan organisasi
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>    
                            </div>
                        
                            <div class="col-md-8" v-if="penilaian">
                                <div class="card-pertanyaan">
                                    
                                    <div class="row">
                                        <transition name="slide-fade">
                                            <div class="col-md-12" style="text-align: center; transition-duration: 1s;" v-if="show">
                                                <p class="pertanyaan"><i class="fa fa-quote-left" aria-hidden="true"></i> {{ subIndikator.sub_indikator }}</p>
                                            </div>
                                        </transition>
                                    </div>
                                    

                                    </br>


                                    <div class="row" style="margin-top: -9px;"> 
                                        <div id="select1">
                                            <div class="col-md-2" style="margin-left: 45px;">
                                                <label class="btn btn-default btn-lg" @click="nilaiInd(1)" style="border: 1px dotted #999;">
                                                    <input type="radio" name="answer1" class="custom-control-input" style="display:none;">
                                                    <p class="q_no">1</p>
                                                </label>
                                                </br>
                                                <p style="font-size: 10px;text-align: center;line-height: 1em; padding-right:22px; padding-top:10px;">Tidak Memuaskan</p>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="btn btn-default btn-lg" @click="nilaiInd(2)" style="border: 1px dotted #999;">
                                                    <input type="radio" name="answer1" class="custom-control-input" style="display:none;">
                                                    <p class="q_no">2</p>
                                                </label>
                                                </br>
                                                <p style="font-size: 10px;text-align: center;line-height: 1em; padding-right:22px; padding-top:10px;">Tidak Sesuai Harapan</p>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="btn btn-default btn-lg" @click="nilaiInd(3)" style="border: 1px dotted #999;">
                                                    <input type="radio" name="answer1" class="custom-control-input" style="display:none;">
                                                    <p class="q_no">3</p>
                                                </label>
                                                </br>
                                                <p style="font-size: 10px;text-align: center;line-height: 1em; padding-right:22px; padding-top:10px;">Target Kerja Terpenuhi</p>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="btn btn-default btn-lg" @click="nilaiInd(4)" style="border: 1px dotted #999;">
                                                    <input type="radio" name="answer1" class="custom-control-input" style="display:none;">
                                                    <p class="q_no">4</p>
                                                </label>
                                                </br>
                                                <p style="font-size: 10px;text-align: center;line-height: 1em; padding-right:22px; padding-top:10px;">Melebihi Target Kerja</p>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="btn btn-default btn-lg" @click="nilaiInd(5)" style="border: 1px dotted #999;">
                                                    <input type="radio" name="answer1" class="custom-control-input" style="display:none;">
                                                    <p class="q_no">5</p>
                                                </label>
                                                </br>
                                                <p style="font-size: 10px;text-align: center;line-height: 1em; padding-right:22px; padding-top:10px;">Membawa Perubahan</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <p class="nilaitotal">Nilai: <span class="label label-info">{{ v_nilai_temp}}</span></p>
                                        </div>
                                    </div>
                                    </br>
                                    </br>
                                    
                                </div>
                            </div>
                            <div class="col-md-4" v-if="photo">
                                <div class="card">
                                    <img :src="'https://web.pt-saa.com/img/hrm/emp/'+ employee.path_photo" alt="Photo" class="card-img-top" v-if="employee.path_photo != null" />
                                    <img src="https://web.pt-saa.com/img/avatar/avatar.png" alt="Photo Belum Ada" class="card-img-top" v-else>
                                    <div class="card-body">
                                        <h5 class="widget-user-username" style="font-size: 1.2em;font-weight: 500; text-transform:capitalize; text-align: center;">{{employee.emp_name}}</h5>
                                        <h6 class="widget-user-desc" style="text-align: center;">{{employee.position}}</h6>
                                        <h6 class="widget-user-desc" style="text-align: center;">{{employee.nik}}</h6>
                                        <a href="#" class="btn btn-info btn-md" style="margin-left: 4.7em;">Total Nilai: <span>10</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger btn-lg" @click="start_penilaian()" v-if="isAlert">Saya Mengerti & Akan menilai</button>
                        <button type="button" class="btn btn-default btn-lg" @click="refreshTablePenilai" v-if="isAlert">Nanti saja</button>
                        <button type="button" class="btn btn-danger btn-lg" @click="stop_penilaian" v-if="isAlert == false">Stop</button>
                        <button type="button" class="btn btn-primary btn-lg pull-right" @click="next_sub_indikator" v-if="btn_berikutnya && isAlert == false">Next</button>
                        <button type="button" class="btn btn-primary btn-lg pull-right" @click="" v-if="isAlert == false">Prev</button>
                        <button type="button" class="btn btn-info btn-lg pull-right" @click="simpan_lanjut(indexing++)" v-if="btn_selesai"><i class="fa fa-arrow-right">Simpan & Lanjut</i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    `,
    props: ['id_target', 'username'],
    data() {
        return {
            isAlert: true,
            total_penilaian: false,
            show: true,
            isOpen: false,
            isLoading: false,
            blank: true,
            start: false,
            penilaian: false,
            photo: false,
            judul_indikator: false,
            waktu_mulai: false,
            employee: [],
            indikator: [],
            tab_indikator: [],
            v_total_indikator: '',
            v_index_indikator: 0,
            penilai_karyawan: [],
            v_indikator: '',
            v_id_indikator: '',
            isRunning: false,
            minutes: 0,
            secondes: 0,
            time: 300,
            timer: null,
            subIndikator: [],
            v_urutan: 0,
            v_max_urutan: 0,
            btn_berikutnya: true,
            btn_selesai: false,
            v_sub_indikator: '',
            v_id_sub_indikator: '',
            v_nilai: '',
            v_kd_indikator: '',
            id_num: 0,
            id_level: 0,
            type_penilai: '',
            v_nilai_temp: 0,
            data_penilai: [],
            columns: ['indikator', 'total_nilai_by_indikator'],
            options: {
                headings: {
                    indikator: "Indikator",
                    total_nilai_by_indikator: "Total Nilai"
                },
                pagination: false,
                filterable: false,
                texts: {
                    count: ''
                },
                // sortable: false
            },
            urutan: '',
            xnext: 0,
            total_indikator: [],
            progressVal: 0,
            nilais: [
                { ket: 'Tidak Memuaskan' },
                { ket: 'Tidak Sesuai Harapan' },
                { ket: 'Target Kerja Terpenuhi' },
                { ket: 'Melebihi Target Kerja' },
                { ket: 'Membawa Perubahan' },
            ],
            total_pertanyaan: 0,
            set_pertanyaan: [],
            index_pertanyaan: 0,
            indexing: 0,
            simpan_hasil: []
        }
    },
    filters: {
        prettify: function (value) {
            let data = value.split(':')
            let minutes = data[0]
            let secondes = data[1]
            if (minutes < 10) {
                minutes = "0" + minutes
            }
            if (secondes < 10) {
                secondes = "0" + secondes
            }
            return minutes + ":" + secondes
        },
    },
    computed: {
        prettyTime() {
            let time = this.time / 60
            let minutes = parseInt(time)
            let secondes = Math.round((time - minutes) * 60)
            return minutes + ":" + secondes
        },
        computedNumber() {
            this.v_nilai_temp = Number(this.v_nilai)
            return this.v_nilai_temp
        },
    },
    created() {
        EventBus.$on("open_modal_nilai_employee", (id_target, id_level, type_penilai) => {
            this.id_level = id_level
            this.id_num = id_target
            this.type_penilai = type_penilai
            this.getDetailEmployee();
            this.getPenilaiKaryawan();
            this.getIndikatorPeriode();
            this.openModal()

        })
    },
    methods: {
        nilaiInd(val) {
            console.log(val)
            this.v_nilai = val;
            return this.v_nilai_temp = this.v_nilai;
        },
        refreshTablePenilai() {
            this.start = false;
            this.penilaian = false;
            this.photo = false;
            this.isAlert = true;
            $("#nilai_employee").modal("hide");
            EventBus.$emit('refreshTablePenilai')
        },
        openModal() {
            $("#nilai_employee").modal("show");
        },
        getPenilaiKaryawan() {
            let username = this.username;

            axios.post('_component/data_penilaian_2.php', {
                request: "getPenilaiKaryawan",
                username: username,
            }).then(response => {
                this.isLoading = false;

                // console.log(response.data)
                this.penilai_karyawan = response.data[0];

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getDetailEmployee() {
            axios.post('_component/data_penilaian_2.php', {
                request: "getDetailEmployee",
                id_num: this.id_num,
            }).then(response => {
                this.isLoading = false;

                this.employee = response.data[0];
                this.indikator_karyawan = response.data[1];

                this.getIndikatorKriteria(this.indikator_karyawan.kd_indikator, this.type_penilai);

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getIndikatorPeriode() {
            axios.post('_component/data_penilaian_2.php', {
                request: "getIndikatorPeriodePenilaian",
                id_level: this.id_level,
            }).then(response => {
                this.isLoading = false;

                this.v_kd_indikator = response.data[0][0];
            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        getIndikatorKriteria(kd, type_penilai) {
            axios.post('_component/data_penilaian_2.php', {
                request: "getIndikatorKriteria",
                kd_indikator: kd,
                type_penilai: type_penilai,
                id_level: this.id_level
            }).then(response => {
                this.isLoading = false;

                this.v_total_indikator = response.data[0].length;
                this.tab_indikator = response.data[0];
                console.log(response.data[0]);
                // this.getFirstIndikator(kd, type_penilai);

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },

        get_indikator(id, x_indikator) {
            this.v_indikator = x_indikator;
            this.v_id_indikator = id;
            console.log("masuk ke 2")


            this.isRunning = false
            clearInterval(this.timer)
            this.timer = null

            this.time = 300
            this.secondes = 0
            this.minutes = 0

            this.blank = false;
            this.start = true;
            this.penilaian = false;
            this.photo = true;
        },
        start_penilaian() {
            this.isAlert = false
            this.isRunning = true;
            this.judul_indikator = true;
            this.waktu_indikator = true;
            if (!this.timer) {
                this.timer = setInterval(() => {
                    if (this.time > 0) {
                        this.time--
                    } else {
                        clearInterval(this.timer)
                        this.stop_penilaian()
                    }
                }, 1000)
            }
            // this.getRandomFirstSubIndikator(this.indikator.id_indikator_kriteria);
            this.getPertanyaanAll(this.indikator_karyawan.kd_indikator, this.type_penilai)

            this.blank = false;
            this.start = false;
            this.penilaian = true;
            this.photo = true;
        },
        stop_penilaian() {
            this.isAlert = true
            this.isRunning = false
            clearInterval(this.timer)
            this.timer = null

            this.time = 300
            this.secondes = 0
            this.minutes = 0

            this.blank = false;
            this.start = true;
            this.penilaian = false;
            this.photo = false;
        },
        getPertanyaanAll(kd, type_penilai) {
            console.log(kd + "," + type_penilai + "," + this.id_level);
            axios.post('_component/data_penilaian_2.php', {
                request: "getAllPertanyaan",
                kd_indikator: kd,
                type_penilai: type_penilai,
                id_level: this.id_level
            }).then(response => {
                this.isLoading = false;

                console.log(response.data[0][1])

                this.v_indikator = response.data[0][0].indikator

                this.indexing = 0

                this.set_pertanyaan = response.data[0]
                this.subIndikator = this.set_pertanyaan[0].pertanyaan[this.index_pertanyaan]
                this.total_pertanyaan = response.data[0][0].pertanyaan.length
                this.index_pertanyaan = 0
                // console.log(this.subIndikator)
                // console.log(this.total_pertanyaan)

            }).catch(error => {
                console.log(error);
                this.isLoading = false;
            });
        },
        next_sub_indikator() {
            // this.show = false
            this.getSecondPertanyaan();
        },
        async simpan_lanjut() {
            // if (this.v_nilai == "") {
            //     alert("Pilih Nilai Terlebih Dahulu");
            // } else {
            //     await swal.fire({
            //         title: "Berhasil!",
            //         text: `Penilaian ${this.v_indikator} berhasil disimpan!`,
            //         icon: "success",
            //     })
            // }
            // this.set_pertanyaan = response.data[0]
            console.log(this.simpan_hasil)

            this.index_pertanyaan = 0
            this.subIndikator = this.set_pertanyaan[this.indexing].pertanyaan[this.index_pertanyaan]
            console.log(this.set_pertanyaan[this.indexing])
            this.v_indikator = this.set_pertanyaan[this.indexing].indikator
            this.total_pertanyaan = this.set_pertanyaan[this.indexing].pertanyaan.length
            await this.next_sub_indikator()
        },
        async getSecondPertanyaan() {
            this.index_pertanyaan++;
            this.simpan_hasil.push(this.subIndikator)
            this.subIndikator = this.set_pertanyaan[this.indexing].pertanyaan[this.index_pertanyaan];
            // this.show = true

            if (this.total_pertanyaan == this.index_pertanyaan) {
                if (!this.subIndikator) {
                    debugger
                    console.log("Indikator ini sudah selesai. Tampilkan button selesai dan tampilkan DIV kosong untuk memberikan informasi nilai akhir dari indikator ini.")
                    this.btn_berikutnya = false;
                    this.btn_selesai = true;
                    console.log(this.simpan_hasil)
                }
            } else {
                this.btn_berikutnya = true;
                this.btn_selesai = false;
                this.show = true
                console.log("masuk ke pertanyaan ke 2")
            }

            console.log(`${this.total_pertanyaan} - ${this.index_pertanyaan}`)
            // if (this.total_pertanyaan == this.index_pertanyaan) {
            //     debugger
            //     console.log("Indikator ini sudah selesai. Tampilkan button selesai dan tampilkan DIV kosong untuk memberikan informasi nilai akhir dari indikator ini.")
            //     this.btn_berikutnya = false;
            //     this.btn_selesai = true;
            //     console.log(this.simpan_hasil)
            // } else {
            //     this.btn_berikutnya = true;
            //     this.btn_selesai = false;
            //     this.show = true
            //     console.log("masuk ke pertanyaan ke 2")
            // }
        }
    }

});

// TEMPLATE BARU
let interval = null;

Vue.component('penilaian', {
    template: `
		<div class="modal fade" id="penilaian_employee" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" style="overflow-y: auto;">
            <div class="modal-dialog" style="margin-top: 0px;">
                <div class="modal-content">
                    <div class="modal-header">
                        <slot name="header"></slot>
                    </div>
                    <div class="modal-body">
                        <slot name="body"></slot>
                    </div>
                    <div class="modal-footer">
                        <slot name="footer"></slot>
                    </div>
                </div>
            </div>
		</div>
		`,
    data() {
        return {
            open: false,
            id_target: 0,
            tipe_penilai: '',
            id_level: 0
        }
    },
    created() {
        EventBus.$on('open_modal_nilai_employee', (id_target, id_level, type_penilai) => {
            this.id_target = id_target;
            this.tipe_penilai = type_penilai;
            this.id_level = id_level;
            this.$emit('resetvalue');
            this.$emit('test', { target: this.id_target, penilai: this.tipe_penilai, level: this.id_level });
            this.openModal();
        });
        EventBus.$on('close_peniliain', () => {
            this.$emit('resetvalue');
            this.closeModal();
            location.reload(0);
        });
    },
    methods: {
        openModal() {
            $("#penilaian_employee").modal("show");
        },
        closeModal() {
            $("#penilaian_employee").modal("hide");
        },
    }
})

Vue.component('progressBar', {
    template: `
		<div class="progress progress_bar">
			<div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar" :style="barStyle">
			
	{{ percent }}

			</div>
		</div>
	`,
    props: {
        min: { type: Number, default: 0 },
        max: { type: Number, default: 1 },
        value: { type: Number, default: 0 },
    },
    computed: {
        barStyle() {
            const frac = Math.min(1, Math.max(0, (this.value - this.min) / (this.max - this.min) || 0));
            return { width: frac * 100 + '%' };
        },
        percent() {
            const frac = Math.min(1, Math.max(0, (this.value - this.min) / (this.max - this.min) || 0));
            const total = Math.round(frac * 100)
            return total + ' %'
        }
    },
})

Vue.component('indikator', {
    props: ["indikators", "v_indikator"],
    template: `
		<div class="indikator-box">
			<ul class="indikator">
				<li v-for="(dt,i) in indikators" :class="{'active':(v_indikator === dt.indikator)}">{{ dt.indikator }}</li>
			</ul>
		</div>
    `,
})

Vue.component('completeTimer', {
    template: `
		<ul class="vuejs-countdown">
			<li v-if="days > 0">
				<p class="digit">{{ days | twoDigits }}</p>
				<p class="text">{{ days > 1 ? 'days' : 'day' }}</p>
			</li>
			<li>
				<p class="digit">{{ hours | twoDigits }}</p>
				<p class="text">{{ hours > 1 ? 'hours' : 'hour' }}</p>
			</li>
			<li>
				<p class="digit">{{ minutes | twoDigits }}</p>
				<p class="text">min</p>
			</li>
			<li>
				<p class="digit">{{ seconds | twoDigits }}</p>
				<p class="text">Sec</p>
			</li>
		</ul>
	`,
    props: {
        deadline: {
            type: String
        },
        end: {
            type: String
        },
        stop: {
            type: Boolean
        }
    },
    data() {
        return {
            now: Math.trunc((new Date()).getTime() / 1000),
            date: null,
            diff: 0
        }
    },
    created() {
        if (!this.deadline && !this.end) {
            throw new Error("Missing props 'deadline' or 'end'");
        }
        let endTime = this.deadline ? this.deadline : this.end;
        this.date = Math.trunc(Date.parse(endTime.replace(/-/g, "/")) / 1000);
        if (!this.date) {
            throw new Error("Invalid props value, correct the 'deadline' or 'end'");
        }
        interval = setInterval(() => {
            this.now = Math.trunc((new Date()).getTime() / 1000);
        }, 1000);
    },
    computed: {
        seconds() {
            return Math.trunc(this.diff) % 60
        },
        minutes() {
            return Math.trunc(this.diff / 60) % 60
        },
        hours() {
            return Math.trunc(this.diff / 60 / 60) % 24
        },
        days() {
            return Math.trunc(this.diff / 60 / 60 / 24)
        }
    },
    watch: {
        now(value) {
            this.diff = this.date - this.now;
            if (this.diff <= 0 || this.stop) {
                this.diff = 0;
                // Remove interval
                clearInterval(interval);
            }
        }
    },
    filters: {
        twoDigits(value) {
            if (value.toString().length <= 1) {
                return '0' + value.toString()
            }
            return value.toString()
        }
    },
    destroyed() {
        clearInterval(interval);
    }
})

Vue.component('timer', {
    template: `
		<div class="timer">
			{{ timer | minutesAndSeconds }}
		</div>
	`,
    props: {
        time: Number
    },
    data() {
        return {
            timer: this.time
        }
    },
    filters: {
        minutesAndSeconds(value) {
            var minutes = Math.floor(parseInt(value, 10) / 60)
            var seconds = parseInt(value, 10) - minutes * 60
            return `${minutes}:${seconds}`
        }
    },
    mounted() {
        setInterval(() => {
            this.timer -= 1
        }, 1000)
    },
    methods: {
        startcounter() {
            if (this.time > 0) {

            }
            this.timer = this.time

        },
    },

})

Vue.component('employeeCard', {
    props: ["employees", "total"],
    template: `
		<div class="card">
            <img style="margin-left: 4.4em;
            margin-right: 4.4em;" :src="'https://web.pt-saa.com/img/hrm/emp/'+ employees.path_photo" alt="Photo" class="card-img-top" v-if="employees.path_photo != null" />
			<div class="card-body">
			<h4 class="card-title" style="text-align: center;text-transform: capitalize;">{{employees.emp_name}}</h4>
			<h5 class="card-text" style="text-align: center;">{{employees.position}}</h5>
			<h6 class="card-text" style="text-align: center;">{{employees.nik}}</h6>
			<a href="#" style="margin-left: 4.9em;" class="btn btn-default btn-flat btn-md">Total Nilai: {{total}}</a>
			</div>
		</div>
	`
})

Vue.component('employeeCardAtas', {
    props: ["employees", "total"],
    template: `
		<div class="card_atas">
            <img style="margin-left: 4.4em;
            margin-right: 4.4em;" :src="'https://web.pt-saa.com/img/hrm/emp/'+ employees.path_photo" alt="Photo" class="card-img-top" v-if="employees.path_photo != null" />
			<div class="card-body">
			<h4 class="card-title" style="text-align: center;text-transform: capitalize;">{{employees.emp_name}}</h4>
			<h5 class="card-text" style="text-align: center;">{{employees.position}}</h5>
			<h6 class="card-text" style="text-align: center;">{{employees.nik}}</h6>
			<a href="#" style="margin-left: 4.9em;" class="btn btn-default btn-flat btn-md">Total Nilai: {{total}}</a>
			</div>
		</div>
	`
})

Vue.component('questions', {
    template: `
		<div class="card-pertanyaan">
            <div class="row">
                <transition name="slide-fade">
                    <div class="col-md-12" style="transition-duration: 1s;" v-if="show">
                        <p class="pertanyaan"> {{ pertanyaan }} </p>
                    </div>
                </transition>
			</div>
			<div class="row">
				<div class="col-md-12">
					<slot :selectedvalue="nilaipilihan"></slot>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<p class="nilaitotal">Nilai: <span class="label label-info">{{totalNilai}}</span></p>
				</div>
			</div>
		</div>
	`,
    props: ['step', 'nilai', 'pertanyaan', 'bobot', 'urutan', 'show'],
    data() {
        return {

        };
    },
    computed: {
        totalNilai() {
            const total = this.nilai * this.bobot
            return total
        }
    },
    methods: {
        nilaipilihan({ val }) {
            this.nilai = val
        }
    },
    created() {

    }
})

Vue.component('nilaiIndikator', {
    template: `
		<div>
			<ul class="nilaiindikator">
				<li 
				<li 
				v-for="item in items"
				:class="item.class"
				@click="set_active_id(item.id)"
				>
					<p class="no">{{ item.id }}</p>
					<p style="font-size: 10px;text-align: center;line-height: 1em;">
						{{item.ket}}
					</p>
				</li>
			</ul>
		</div>
	`,
    props: {
        items: Array,
        prev_id: Number
    },
    data() {
        return {
            previous_active_id: 1
        }
    },
    methods: {
        set_active_id(id) {
            // if (this.previous_active_id === id) return //no need to go further
            this.items.find(item => item.id === this.previous_active_id).class = '' //remove the active class from old active li
            this.items.find(item => item.id === id).class = 'active' //set active class to new li

            this.previous_active_id = id //store the new active li id

            this.$emit('selectedvalue', { val: id })
        },

    },
})

Vue.component('btnPenilaian', {
    template: `
    
		<button :class="classbtn" @click="cancel()" v-if="condition">{{nama}}</button>
		<button :class="classbtn" @click="close()" v-else>{{nama}}</button>
	`,
    props: {
        classbtn: String,
        nama: String,
        nilai: Number,
        condition: Boolean
    },
    methods: {
        cancel() {
            // this.step = parseInt(this.step) + 1;
            this.$emit('cancel')
        },
        close() {
            this.$emit('close')
        }
    }
})

Vue.component('test', {
    template: `
	<div class="modal fade" id="nilai_employee" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" style="margin-top: 0px; width: 95%;">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #3087bd; height: 100px;">
                    <div class="row">
                        <div class="col-sm-3">
                            <i class="fa fa-exclamation-triangle pull-right" style="font-size: xxx-large; color:white; padding-top: 10px;"></i>
                        </div>
                        <div class="col-sm-8">
                            <h3 class="modal-title" style="text-align: left;font-size: xx-large; font-family: system-ui; font-weight: bold; color: white;">TATA CARA PENILAIAN KARYAWAN</h3>
                            <h5 style="margin-top: 0px;padding-top: 0px; color: white; text-align: left; font-size: 13px;">Isilah kolom Nilai Staff dengan nilai 1 - 5. Adapun penjelasan angka penilaian adalah seperti berikut:</h5>
                        </div>
                    </div>
                </div>
                <div class="modal-body" style="background: white; padding-top: 1px;">
                    <table class="table" style="margin-bottom: -10px; margin-top:0px;">
                        <thead>
                            <tr>
                                <th width="20%" class="text-center">Nilai Staff</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center" style="padding-bottom: 0px;">
                                    <span style="font-size: 25px;font-weight: bold; font-family: fantasy;">1</span><br />
                                    <span style="font-size: 14px; font-weight: bold;">KURANG</span></br>
                                    <span style="font-size: 14px; font-weight: bold;">SEKALI</span>
                                </td>
                                <td width="80%" style="font-size: 1em">
                                    <p style="font-size: 18px;">Kinerja karyawan menunjukan kekurangan yang signifikan. Pada level ini tidak dapat dibiarkan, karena menimbulkan hambatan dalam operasional.</p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center" style="padding-bottom: 0px;">
                                    <span style="font-size: 25px;font-weight: bold; font-family: fantasy;">2</span><br />
                                    <span style="font-size: 14px; font-weight: bold;">KURANG</span>

                                </td>
                                <td width="80%" style="font-size: 1em">
                                    <p style="font-size: 18px;">Kinerja karyawan tidak memenuhi level yang diharapkan, memerlukan pengembangan untuk meningkat ke tingkat yang sesuai harapan</p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center" style="padding-bottom: 0px;">
                                    <span style="font-size: 25px;font-weight: bold; font-family: fantasy;">3</span><br />
                                    <span style="font-size: 14px; font-weight: bold;">BAIK</span>
                                </td>
                                <td width="80%" style="font-size: 1em">
                                    <p style="font-size: 18px;">Karyawan kompeten pada tingkat kinerja yang diharapkan. Kinerja karyawan baik, berhasil menyelesaikan tujuan dan tantangan yang diuraikan selama periode penilaian.</p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center" style="padding-bottom: 0px;">
                                    <span style="font-size: 25px;font-weight: bold; font-family: fantasy;">4</span><br />
                                    <span style="font-size: 14px; font-weight: bold;">BAIK</span></br>
                                    <span style="font-size: 14px; font-weight: bold;">SEKALI</span>
                                </td>
                                <td width="80%" style="font-size: 1em; padding-bottom: 0px;">
                                    <p style="font-size: 18px;">
                                        Kinerja keseluruhan tugas dan tanggung jawab melebihi standar. 
                                        Berkonstribusi pada pencapaian misi, tujuan, dan obyektif.
                                        Pada nilai ini menunjukkan kinerja keseluruhan karyawan
                                        berada pada level yang melebihi harapan yang dinyatakan
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">
                                    <span style="font-size: 25px;font-weight: bold; font-family: fantasy;">5</span><br />
                                    <span style="font-size: 14px; font-weight: bold;">ISTIMEWA</span>
                                </td>
                                <td width="80%" style="font-size: 1em">
                                    <p style="font-size: 18px;">
                                        Kinerja secara signifikan melebihi standar harapan dan diakui oleh banyak team,
                                        rekan internal dan rekan exsternal, membawa dampak positif terahadap organisasi
                                        Karyawan merancang dan mengimplementasimentasikan perbaikan
                                        yang menghasilkan peningkatan / kerberhasilan organisasi
                                    </p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-lg" @click="close()">Nanti Saja</button>
                    <button type="button" class="btn btn-danger btn-lg" @click="start()">Mengerti & Nilai</button>
                </div>
            </div>
        </div>
	</div>    
    `,
    data() {
        return {
            show: false,
            id_target: 0,
            id_level: 0,
            type_penilai: ''
        };
    },
    created() {
        EventBus.$on("open_tata_cara", (id_target, id_level, type_penilai) => {
            // this.show = open;
            this.id_target = id_target;
            this.id_level = id_level;
            this.type_penilai = type_penilai;
            this.openModal();
        });
        // console.log(this.show)
    },
    methods: {
        close() {
            this.closeModal();
            // if (this.show == false) {
            //     return this.show = true;
            // } else {
            //     return this.show = false;
            // }
        },
        closeModal() {
            $("#nilai_employee").modal("hide");
        },
        start() {
            EventBus.$emit("open_modal_nilai_employee", this.id_target, this.id_level, this.type_penilai)
            this.closeModal();
            // this.show = false;
        },
        openModal() {
            $("#nilai_employee").modal("show");
        },
    }
})

Vue.component('nilaiWizard', {
    template: `
			<div style="text-align: left;">
				<div v-if="step === 1">
				
					<button @click.prevent="next()" class="btn btn-primary btn-lg" :disabled="nilai === 0">Berikutnya</button>
				
				</div>

                <div v-if="step > 1 && step < totalsteps">
                
				
					<button @click.prevent="prev()" class="btn btn-primary btn-lg">Sebelumnya </button>
					<button @click.prevent="next()" class="btn btn-primary btn-lg" :disabled="nilai === 0">Berikutnya</button>
				
				</div>
				<div v-if="step == totalsteps">
					<button @click.prevent="prev()" class="btn btn-primary btn-lg">Sebelumnya</button>
					<button @click.prevent="save()" class="btn btn-warning btn-lg" :disabled="nilai === 0">Simpan Penilaian </button>
				</div>

			</div>
	`,
    props: {
        totalsteps: Number,
        startstep: Number,
        nilai: Number,
    },
    data() {
        return {
            step: this.startstep
        }
    },
    methods: {
        prev() {
            this.step = parseInt(this.step) - 1;
            this.$emit('wizard', { stepping: this.step, action: 1 })
            // console.log('step:' + this.step + ', prev: ' + this.step)
        },
        next() {
            this.step = parseInt(this.step) + 1;
            this.$emit('wizard', { stepping: this.step, action: 2 })
            // console.log('step:' + this.step + ', next: ' + this.step)
        },
        save() {
            this.step = parseInt(this.step) + 1;
            this.total = 100;
            this.$emit('wizard', { stepping: this.step, action: 3, total: this.total })
        }
    }
});

Vue.component('thanks_card', {
    template: `
    <div class="card" style="margin-left: 40px;">
        <h1 class="display-3">Terima Kasih</h1>
        <hr>
    </div>
    `
})