    
<?

$menu_code = 'hrpenilaian';
$accGroup = get_acc_group($menu_code, $dbh);

if ( $accGroup['acc_menu'] ) :

    $menu_desc = $accGroup['menu_desc'];
    $acc_new = $accGroup['acc_new'];
    $acc_edit = $accGroup['acc_edit'];
    $acc_save = $accGroup['acc_save'];
    $acc_delete = $accGroup['acc_delete'];
    $acc_download = $accGroup['acc_download'];


    $cabang_awl = isset($_POST['cabang_awl']) ? $_POST['cabang_awl'] : NULL;
    $dept_awl = isset($_POST['dept_awl']) ? $_POST['dept_awl'] : NULL;
    $div_awl = isset($_POST['div_awl']) ? $_POST['div_awl'] : NULL;
    $pos_awl = isset($_POST['pos_awl']) ? $_POST['pos_awl'] : NULL;
    $cari = isset($_POST['cari']) ? $_POST['cari'] : NULL;
    $stts = isset($_POST['stts']) ? $_POST['stts'] : NULL;

    $tgl_now = date("Y-m-d");
?>

<style type="text/css">
    .slide-fade-enter-active {
    transition: all 10s cubic-bezier(0.3, 0.3, 0.3, 0.3);
    }
    .slide-fade-leave-active {
    transition: all 5s cubic-bezier(1.0, 0.5, 0.8, 1.0);
    }
    .slide-fade-enter, .slide-fade-leave-to
    /* .slide-fade-leave-active di bawah versi 2.1.8 */ {
    transform: translateX(10px);
    opacity: 0;
    }


    .btn-circle {
        width: 30px;
        height: 30px;
        text-align: center;
        padding: 6px 0;
        font-size: 12px;
        line-height: 1.428571429;
        border-radius: 15px;
    }

    .btn-circle.btn-lg {
        width: 90px;
        height: 70px;
        padding: 23px 23px;
        font-size: 20px;
        line-height: 1.33;
        border-radius: 45px;
    }
    .progress {
        position: relative;
    }

    .progress span {
        position: absolute;
        display: block;
        width: 100%;
        color: black;
        text-align: left;
        padding-left: 8px;
    }
    .table-shopping>thead>tr>th {
        color:#666666;
        font-size: 1 em;
        font-weight:300
    }
    .table-shopping>tbody>tr>td {
        font-size: 1 em;
        padding:10px 5px 0px 5px;
    }
    .table-shopping>tbody>tr>td b {
        display:block;
        margin-bottom:5px
    }
    .table-shopping .td-number,.table-shopping .td-price,.table-shopping .td-total{
        font-size:1 em;font-weight:300;min-width:130px;text-align:right
    }
    .table-shopping .td-number-total {
        font-size: 1.4em;font-weight:600;text-align: right;
    }
    .table-shopping .td-number small,.table-shopping .td-price small,.table-shopping .td-total small{
        margin-right:3px
    }
    .table-shopping .td-product {
        min-width:170px;
        padding-left:10px;
        font-size:1 em;
        font-weight:300;
    }
    .table-shopping .td-product strong {
        color:#403d39;font-size:1em;font-weight:600
    }
    .table-shopping .td-number,.table-shopping .td-total{
        color:#403d39;
        font-weight:600;
        font-size:1.0em;
    }
    .table-shopping .td-quantity {
        min-width:200px
    }
    .table-shopping .td-quantity .btn-group {
        margin-left:10px
    }
    .table-shopping .img-container {
        border-radius:6px;display:block;height:100px;overflow:hidden;width:100px
    }
    .table-shopping .img-container img {
        width:100%
    }
    .table-shopping .tr-actions>td {
        border-top:0
    }
    .table-shopping>tbody>tr opened {
        background-color: #ddd;
    }
    
    table#tabledata th {
        background-color: #E8E8E8 !important;
    }
    .navbar-inverse {
        border-color: #ddd;
    }
    .navbar-inverse .navbar-nav>li>a {
        border: 1px #cccccc solid;
        color: #333333;
    }
    .navbar-inverse .navbar-nav>li>a:hover {
        background-color: #cccccc;
    }
    .fade-enter,
    .fade-leave-to { opacity: 0; }
    .fade-enter-active,
    .fade-leave-active { transition: 0.5s; }

    .slide-enter {
    opacity: 0;
    transform: scale3d(2, 0.5, 1) translate3d(400px, 0, 0);
    }

    .slide-enter-to { transform: scale3d(1, 1, 1); }
    .slide-enter-active,
    .slide-leave-active { transition: 0.5s cubic-bezier(0.68, -0.55, 0.265, 1.55); }
    .slide-leave { transform: scale3d(1, 1, 1); }

    .slide-leave-to {
    opacity: 0;
    transform: scale3d(2, 0.5, 1) translate3d(-400px, 0, 0);
    }

    .rotate-enter { transform: perspective(500px) rotate3d(0, 1, 0, 90deg); }
    .rotate-enter-active,
    .rotate-leave-active { transition: 0.5s; }
    .rotate-leave-to { transform: perspective(500px) rotate3d(0, 1, 0, -90deg); }

    .lds-facebook {
        display: inline-block;
        position: relative;
        left: 48%;
        text-align: center;
        width: 64px;
        height: 74px;
    }
    .lds-facebook div {
        display: inline-block;
        position: absolute;
        left: 6px;
        width: 13px;
        background: rgba(39, 34, 229, 0.3);
        animation: lds-facebook 1.2s cubic-bezier(0, 0.5, 0.5, 1) infinite;
    }
    .lds-facebook div:nth-child(1) {
        left: 6px;
        animation-delay: -0.24s;
    }
    .lds-facebook div:nth-child(2) {
        left: 26px;
        animation-delay: -0.12s;
    }
    .lds-facebook div:nth-child(3) {
        left: 45px;
        animation-delay: 0;
    }
    @keyframes lds-facebook {
        0% {
            top: 6px;
            height: 61px;
        }
        50%, 100% {
            top: 19px;
            height: 36px;
        }
    }
    /* .modal-mask {
        position: fixed;
        z-index: 9998;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, .5);
        display: table;
        transition: opacity .3s ease;
    }

    .modal-wrapper {
        display: table-cell;
        vertical-align: middle;
    } */
    .autocomplete {
        position: relative;
        font-size: 12px;
    }

    .autocomplete-results {
        padding: 0;
        margin: 0;
        border: 1px solid #eeeeee;
        height: 120px;
        overflow: auto;
        width: 100%;
        background-color: #f5f5f5;
        z-index: 1000;
    }

    .autocomplete-result {
        list-style: none;
        text-align: left;
        padding: 5px 10px;
        cursor: pointer;
    }

    .autocomplete-result.is-active,
    .autocomplete-result:hover {
        background-color: #4AAE9B;
        color: white;
    }
    @media print {
        @page {
            size: 330mm 427mm;
            margin: 14mm;
        }
        .container {
            width: 1170px;
        }
        .make-grid(sm);
    }
    .styled-select select {
        -moz-appearance:none;
        -webkit-appearance:none;
        appearance:none;
    }

    .card-img-top {
        width: 50%;
    }
    .modal-mask {
        position: fixed;
        z-index: 9998;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, .8);
        display: table;
        transition: opacity .3s ease;
    }
    .modal-wrapper {
        display: table-cell;
        vertical-align: middle;
        top: 8%;
    }
    .modal-dialog {
        width:1050px;
        border: 5px solid #000;
        -webkit-border-radius: 10px !important;
        -moz-border-radius: 10px !important;
        border-radius: 10px !important; 
    }
    .modal-content  {
        background: transparent;
        
    }
    .modal-header { 
        background-color: #eee;
        -webkit-border-radius: 10px 10px 0px 0px !important;
        -moz-border-radius: 10px 10px 0px 0px !important;
        border-radius: 10px 10px 0px 0px !important; 
        border: 0px;
    }
    .modal-body {
        background-color: #eee;
        border: 0px;
    }
    .modal-footer {
        border: 0px;
        background-color: #ddd;
        -webkit-border-radius: 0px 0px 10px 10px !important;
        -moz-border-radius: 0px 0px 10px 10px !important;
        border-radius: 0px 0px 10px 10px !important; 
    }
    .progress {
        -webkit-border-radius: 5px !important;
        -moz-border-radius: 5px !important;
        border-radius: 5px !important;
        border: 1px solid #ccc;
        height: 40px;
    }
    .progress-bar {
        font-size: 20px;
        font-weight: bold;
        padding: 10px 10px 10px 10px;
    }

    .indikator-box {
        margin: 8px 0 0 0;
    }
    ul.indikator {
        list-style: none;
    }
    ul.indikator li {
        margin-right: 20px;
        border: 2px dashed #ccc;
        border-radius: 4px;
        display: inline;
        padding: 7px 20px;
        text-align: center;
        text-transform: uppercase;
        font-size: 20px;
        color: #ccc;
    }
    ul.indikator li.active {
        border-color: #0275d8;
        color: #fff;
        background-color: #0275d8;
        font-weight: bold;
        -webkit-box-shadow: 9px 10px 14px -4px rgba(153,150,153,1);
        -moz-box-shadow: 9px 10px 14px -4px rgba(153,150,153,1);
        box-shadow: 9px 10px 14px -4px rgba(153,150,153,1);
    }
    .timer {
        margin-left: 10px;
        margin-top: 0px;
        border: 2px solid #d9534f;
        border-radius: 4px;
        padding: 3px 10px;
        text-align: center;
        text-transform: uppercase;
        font-size: 28px;
        font-weight: bold;
        color: #FFF;
        width: 70%;
        background-color: #d9534f;
        -webkit-box-shadow: 9px 10px 14px -4px rgba(153,150,153,1);
        -moz-box-shadow: 9px 10px 14px -4px rgba(153,150,153,1);
        box-shadow: 9px 10px 14px -4px rgba(153,150,153,1);
    }
    .card {
        border-radius: 5px;
        background-color: #fff;
        padding: 20px 25px;
        width: 90%;
        text-align: left;
        -webkit-box-shadow: 9px 10px 14px -4px rgba(153,150,153,1);
        -moz-box-shadow: 9px 10px 14px -4px rgba(153,150,153,1);
        box-shadow: 9px 10px 14px -4px rgba(153,150,153,1);
    }
    .card-pertanyaan {
        margin-left: 4%;
        border-radius: 5px;
        background-color: #fff;
        padding: 20px 25px;
        width: 95%;
        -webkit-box-shadow: 9px 10px 14px -4px rgba(153,150,153,1);
        -moz-box-shadow: 9px 10px 14px -4px rgba(153,150,153,1);
        box-shadow: 9px 10px 14px -4px rgba(153,150,153,1);
    }
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    .switch input { 
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    .pertanyaan {
        font-family:  'cabin', san-serif;
        font-size: 28px;
        text-align: center;
        font-weight: bold;
        padding: 8px 18px;
        text-transform: capitalize;
    }
    ul.nilaiindikator {
        margin-top: 20px;
        list-style: none;
    }
    ul.nilaiindikator li {
        width: 15%;
        border-radius: 5px;
        border: 1px dotted #999;
        margin-right: 20px;
        padding: 5px 10px;
        display: inline-block;
    }
    ul.nilaiindikator li:hover {
        background-color: #5bc0de;
        color: #fff;
        border: 1px solid #666;
    }
    ul.nilaiindikator li.active {
        background-color: #5bc0de;
        color: #fff;
        border: 1px solid #666;
    }
    ul.nilaiindikator li p.no {
        font-size: 30px;
        text-align: center;
        font-weight: bold;
    }
    
    p.nilaitotal {
        font-size: 18px;
        padding: 12px 10px 5px 10px;
        margin-right: 50px;
        text-align: right;
        font-weight: bold;
    }

    .slide-fade-enter-active {
    transition: all 1000s cubic-bezier(0.3, 0.3, 0.3, 0.3);
    }
    .slide-fade-leave-active {
    transition: all 1000s cubic-bezier(1.0, 0.5, 0.8, 1.0);
    }
    .slide-fade-enter, .slide-fade-leave-to
    /* .slide-fade-leave-active di bawah versi 2.1.8 */ {
    transform: translateX(10px);
    opacity: 1;
    }
</style>

<style type="text/css">
    ol.tree {
        padding: 0 0 0 50px;
        width: 500px;
    }
    li { 
        position: relative;
        list-style: none;
    }      
    li input {
        position: absolute;
        left: 0;
        margin-left: 0;
        opacity: 0;
        z-index: 2;
        cursor: pointer;
        height: 1em;
        width: 1em;
        top: 0;
    }
    li input + ol {
        background: url(../img/hrm/toggle-small-expand.png) 40px 0 no-repeat;
        margin: -1.600em 0px 8px -44px; 
        height: 1em;
    }
    li input + ol > li { 
        display: none; 
        margin-left: -14px !important; 
        padding-left: 1px; 
    }
    li label {
        background: url(../img/hrm/folder.png) 15px 5px no-repeat;
        cursor: pointer;
        display: block;
        padding-left: 37px;
    }
    li input:checked + ol {
        background: url(../img/hrm/toggle-small.png) 40px 5px no-repeat;
        margin: -1.96em 0 0 -44px; 
        padding: 1.563em 0 0 80px;
        height: auto;
    }
    li input:checked + ol > li { 
        display: block; 
        margin: 8px 0px 0px 0.125em;
    }
    li input:checked + ol > li:last-child { 
        margin: 8px 0 0.063em;
    }
</style>

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.js"></script>

<script src="https://unpkg.com/vuejs-datepicker"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="../2.4.0/plugins/slimScroll/jquery.slimscroll.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
<script src="https://unpkg.com/jspdf-autotable"></script>
<script src="https://cdn.jsdelivr.net/npm/vue2-highcharts@1.2.4/dist/vue-highcharts.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://web.pt-saa.com/cdn/vue-select/moment.min.js"></script>
<script src="https://web.pt-saa.com/cdn/vue-select/moment-with-locales.min.js"></script>
<script src="../cdn/vue/vue-table-2.js"></script>


<script src="https://unpkg.com/vue-select@3.0.0"></script>
<link rel="stylesheet" href="https://unpkg.com/vue-select@3.0.0/dist/vue-select.css">

<script src="https://unpkg.com/vuejs-paginate@0.9.0"></script>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.0/dist/sweetalert2.min.css">

<aside class="content-wrapper" id="app">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#" style="font-weight: bold;text-shadow: 1px 0px 1px #fff;"> PENILAIAN KARYAWAN</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse" v-if="access">
            <ul class="nav navbar-nav navbar-right">
                <!-- <li><a href="#" id="organisasi_new" @click="view_organisasi_new">Organisasi</a></li> -->
                <!-- <li><a href="#" id="penilai" @click="view_penilai">Pasangan Penilai</a></li> -->
                <li class="dropdown" >
                    <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Pasangan Penilai <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#" @click="view_penilai">Personal</a></li>
                        <li><a href="#" @click="view_penilai_byCabang">Per Cabang</a></li>
                    </ul>
                </li>
                <li><a href="#" id="indikator" @click="view_indikator">Materi Penilaian</a></li>
                <li class="dropdown" >
                    <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> HRD <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#" id="indikator" @click="view_hrd_setting">Pengaturan HRD</a></li>
                        <li><a href="#" id="indikator" @click="view_penilai_hrd">Penilaian HRD</a></li>
                    </ul>
                </li>
                <!-- <li><a href="#" id="sub_indikator" @click="view_sub_indikator">Sub Indikator</a></li> -->
                <li class="dropdown" >
                    <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Laporan <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#" @click="laporan">Laporan Personal</a></li>
                        <li><a href="#" @click="laporanCabang">Laporan Per Cabang</a></li>
                        <li><a href="#" @click="laporanAll">Laporan Semua</a></li>
                    </ul>
                </li>
                <li><a href="#" id="indikator" @click="setting_jadwal">Pengaturan Jadwal</a></li>

            <!-- <li>Indikator</li>
            <li>Sub Indikator</li>
            <li>Laporan</li> -->
            </ul>
        </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>

    <section class="content">
        <div class="row" v-if="access">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                    <? /* <div v-if="organisasi">
                        <h4>Manage Organisasi</h4>
                        </br>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <?php 
                                            $kd_off = "";
                                            if (isset($_POST['kd_off'])) {
                                                $kd_off = $_POST['kd_off'];
                                            } else if(isset($_GET['kd_off'])) {
                                                $kd_off = $_GET['kd_off'];
                                            }
                                        ?>
                                        <form id="myform" method="post">
                                            <select name="kd_off" class="styled-select form-control select2" id="office" onChange='change()'>
                                                <option value=""></option>
                                                <?
                                                $sql = "SELECT kd_off, office FROM hrm_office WHERE kd_off <> 'DPS'";
                                                $stmt = $dbh->query($sql);
                                                foreach ($stmt as $row) { ?>
                                                    <option <? if ($kd_off == $row['kd_off']) echo "selected"; ?> value="<?= $row['kd_off'] ?>"> <?= $row['kd_off'] ?> - <?= $row['office'] ?> </option>
                                                <? }
                                                ?>
                                            </select>
                                        </form>

                                        <!-- <button onclick='postId(10)'>Test</button> -->
                                    </div>
                                </div>
                                </br>
                                <?php
                                    function createTreeView($parent, $menu) {
                                        $html = "";
                                        if (isset($menu['parents'][$parent])) {
                                            $html .= "<ol class='tree'>";
                                            foreach ($menu['parents'][$parent] as $itemId) {
                                                if(!isset($menu['parents'][$itemId])) {
                                                    $html .= "<li><label for='subfolder2'>";
                                                    $html .= " <a v-on:click='getDataKaryawanByKodePosition(`".$menu['items'][$itemId]['id_post']."`)'>".$menu['items'][$itemId]['position_name']."</a>";
                                                    $html .= " <a class='fa fa-edit' data-toggle='modal' data-target='#edit_post' @click='getDataPosition(".$menu['items'][$itemId]['id_post'].")'></a>";
                                                    $html .= " <a class='fa fa-trash-o' data-toggle='modal' data-target='#delete_post' @click='getDataPosition(".$menu['items'][$itemId]['id_post'].")'></a>";
                                                    $html .= "</label> <input type='checkbox' name='subfolder2'/></li>";
                                                }
                                                if(isset($menu['parents'][$itemId])) {
                                                    $html .= "<li><label for='subfolder2'>";
                                                    $html .= " <a v-on:click='getDataKaryawanByKodePosition(`".$menu['items'][$itemId]['id_post']."`)'>".$menu['items'][$itemId]['position_name']."</a>";
                                                    $html .= " <a class='fa fa-edit' data-toggle='modal' data-target='#edit_post'  @click='getDataPosition(".$menu['items'][$itemId]['id_post'].")'></a>";
                                                    $html .= " <a class='fa fa-trash-o' data-toggle='modal' data-target='#delete_post' @click='getDataPosition(".$menu['items'][$itemId]['id_post'].")'></a>";
                                                    $html .= "</label> <input type='checkbox' name='subfolder2'/> ";
                                                    $html .= createTreeView($itemId, $menu);
                                                    $html .= "</li>";
                                                }
                                            }
                                            $html .= "</ol>";
                                        }
                                        return $html;
                                    }

                                    $sql = "SELECT * FROM hrm_apprasial_position WHERE kd_off = '$kd_off' AND status_post = '1' ORDER BY id_post";
                                    $stmt = $dbh->query($sql);
                                    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

                                    $menus = array(
                                        'items' => array(),
                                        'parents' => array()
                                    );

                                    foreach ($result as $row) {
                                        $menus['items'][$row['kd_post']] = $row;
                                        $menus['parents'][$row['kd_post_parent']][] = $row['kd_post'];
                                    }
                                    echo createTreeView(0, $menus);
                                ?>
                            </div>
                            <Organisasi :kd_off="kd_off"></Organisasi>
                        </div>
                        </br></br>

                        </br>
                        <div class="row">
                            <div class="col-md-8">
                                <h4>List Karyawan <strong>{{ xOffice }}</strong> Posisi <strong>{{ xPosition }}</strong></h4>
                            </div>
                            <div class="col-md-4">
                                <button type="button" data-toggle='modal' data-target='#add_karyawan' class="btn btn-primary pull-right">Tambah Karyawan</button>
                            </div>
                        </div>
                        </br>

                        <table class="table">
                            <thead>
                                <tr v-if="isLoading">
                                    <td colspan="9">
                                        <div class="lds-facebook"><div></div><div></div><div></div></div>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="col">Lokasi</th>
                                    <th scope="col">Nama</th>
                                    <th scope="col">NIK</th>
                                    <th scope="col">Department</th>
                                    <th scope="col">Position Name</th>
                                    <th scope="col">Level</th>
                                    <th scope="col">Join Date</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <template v-for="arr_employee in employee">
                                <tr>
                                    <td>{{ arr_employee.office }}</td>
                                    <td style="text-transform: capitalize">{{ arr_employee.emp_name }}</td>
                                    <td>{{ arr_employee.nik }}</td>
                                    <td>{{ arr_employee.department }}</td>
                                    <td>{{ arr_employee.position }}</td>
                                    <td>{{ arr_employee.level }}</td>
                                    <td>{{ arr_employee.join_date }}</td>
                                    <td><button class='btn btn-danger fa fa-trash-o' data-toggle='modal' data-target='#delete_penilai' @click='deleteEmployee(arr_employee.id_num)'></button></td>
                                </tr>
                                </template>
                            </tbody>
                        </table>

                        <div class="modal fade" id="edit_post" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <form @submit.prevent="updateDataPosition">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title font-weight-bold" id="exampleModalLabel">Edit Position</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div id="position-modal" class="modal-body">
                                            <div class="form-group">
                                                <label>Kode Posisi</label>
                                                <input type="kd_post" v-model="kd_post" class="form-control" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Nama Posisi</label>
                                                <input type="post_name" v-model="post_name" class="form-control" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Parent</label>
                                                <select class="form-control" v-model="kd_post_parent" required>
                                                    <option value="0">None</option>
                                                    <option v-for="(arr_postion,k) in position" :key="k" :value="arr_postion.kd_post">{{arr_postion.position_name}}</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="modal fade" id="delete_post" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title font-weight-bold" id="exampleModalLabel">Delete Position</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div id="position-modal" class="modal-body">
                                        <h5>Are You Sure Want To Delete <a class="font-weight-bold">{{ post_name }}</a> ? </h5>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-danger" @click.prevent='DeleteDataPosition'>Delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="add_karyawan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <form @submit.prevent="SaveEmployee">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title font-weight-bold" id="exampleModalLabel">Tambah Karyawan</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div id="position-modal" class="modal-body">
                                            <div class="form-group">
                                                <label>Karyawan</label>
                                                <v-select style="text-transform: capitalize" label="emp_name" :options="karyawan" v-model="id_num_employee" require></v-select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div v-if="organisasi_new">
                        <h4>Manage Organisasi</h4>
                        </br>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <!-- <v-select style="text-transform: capitalize" label="office" :options="office" v-model="v_office" @input="getPositionAll" require></v-select> -->
                                        <?php 
                                            $username = $_SESSION['username'];
                                            $kd_off = "";
                                            $office = "";
                                            if (isset($_POST['kd_off'])) {
                                                $kd_off = $_POST['kd_off'];
                                            } else if(isset($_GET['kd_off'])) {
                                                $kd_off = $_GET['kd_off'];
                                            }

                                            if (isset($_POST['office'])) {
                                                $office = $_POST['office'];
                                            } else if(isset($_GET['office'])) {
                                                $office = $_GET['office'];
                                            }
                                        ?>
                                        <form id="form_posisi" method="post">
                                            <select name="kd_off" class="styled-select form-control select2" id="office" onChange='getPosisiByKdOff()'>
                                                <option value=""></option>
                                                <?
                                                $sql = "SELECT B.kd_off,A.office FROM hrm_office A RIGHT JOIN hrm_officeacc B 
                                                        ON A.kd_off = B.kd_off WHERE B.username='$username' ORDER BY B.kd_off";
                                                $stmt = $dbh->query($sql);
                                                foreach ($stmt as $row) { ?>
                                                    <option <? if ($kd_off == $row['kd_off']) echo "selected"; ?> value="<?= $row['kd_off'] ?>"> <?= $row['kd_off'] ?> - <?= $row['office'] ?> </option>
                                                <? } ?>
                                            </select>
                                        </form>
                                    </div>
                                </div>
                                </br></br>
                                <div class="row">
                                    <div class="col-md-2">
                                        <button type="button" data-toggle='modal' data-target='#add_mapping_posisi' class="btn btn-primary" @click="empty_add_struktur">Tambah Struktur</button>
                                    </div>
                                </div>
                                </br></br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <!-- <div v-html="position_all"></div> -->
                                <?php
                                    function createTreeView2($parent, $menu) {
                                        $html = "";
                                        if (isset($menu['parents'][$parent])) {
                                            $html .= "<ol class='tree'>";
                                            foreach ($menu['parents'][$parent] as $itemId) {
                                                if(!isset($menu['parents'][$itemId])) {
                                                    $html .= "<li><label for='subfolder2'>";
                                                    $html .= " <a v-on:click='getDataKaryawanByKodePosition2(`".$menu['items'][$itemId]['kd_pos']."`,`".$menu['items'][$itemId]['kd_off']."`)'>".$menu['items'][$itemId]['position_name']."</a>";
                                                    $html .= " <a class='fa fa-trash-o' data-toggle='modal' data-target='#delete_post' @click='getMappingPosisiByKd(`".$menu['items'][$itemId]['kd_pos']."`)'></a>";
                                                    $html .= "</label> <input type='checkbox' name='subfolder2'/></li>";
                                                }
                                                if(isset($menu['parents'][$itemId])) {
                                                    $html .= "<li><label for='subfolder2'>";
                                                    $html .= " <a v-on:click='getDataKaryawanByKodePosition2(`".$menu['items'][$itemId]['kd_pos']."`,`".$menu['items'][$itemId]['kd_off']."`)'>".$menu['items'][$itemId]['position_name']."</a>";
                                                    $html .= " <a class='fa fa-trash-o' data-toggle='modal' data-target='#delete_post' @click='getMappingPosisiByKd(`".$menu['items'][$itemId]['kd_pos']."`)'></a>";
                                                    $html .= "</label> <input type='checkbox' name='subfolder2'/> ";
                                                    $html .= createTreeView2($itemId, $menu);
                                                    $html .= "</li>";
                                                }
                                            }
                                            $html .= "</ol>";
                                        }
                                        return $html;
                                    }

                                    $sql2 = "SELECT 
                                                hrm_apprasial_position_bawahan.id_pos AS id_post,
                                                hrm_apprasial_position_bawahan.kd_pos AS kd_pos,
                                                hrm_position.position AS position_name,
                                                '0' AS kd_post_parent,
                                                hrm_apprasial_position_bawahan.kd_off
                                                FROM hrm_apprasial_position_bawahan
                                                JOIN hrm_position ON hrm_position.id_pos = hrm_apprasial_position_bawahan.id_pos
                                                WHERE hrm_apprasial_position_bawahan.id_atasan = '0'
                                                AND hrm_apprasial_position_bawahan.kd_off = '$kd_off'
                                                UNION ALL
                                                SELECT 
                                                a.id_pos AS id_post,
                                                a.kd_pos AS kd_pos,
                                                b.position AS position_name,
                                                c.kd_pos AS kd_post_parent,
                                                a.kd_off
                                                FROM hrm_apprasial_position_bawahan a
                                                LEFT JOIN hrm_position b ON b.id_pos = a.id_pos
                                                LEFT JOIN hrm_position c ON c.id_pos = a.id_atasan
                                                WHERE a.kd_off = '$kd_off'
                                                AND a.id_atasan <> '0'
                                                AND a.deleted = '0'
                                                ORDER BY id_post";
                                    $stmt2 = $dbh->query($sql2);
                                    $result2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);

                                    $menus2 = array(
                                        'items' => array(),
                                        'parents' => array()
                                    );

                                    foreach ($result2 as $row2) {
                                        $menus2['items'][$row2['kd_pos']] = $row2;
                                        $menus2['parents'][$row2['kd_post_parent']][] = $row2['kd_pos'];
                                    }
                                    echo createTreeView2(0, $menus2);
                                ?>
                            </div>
                        </div>
                        </br></br>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table">
                                    <thead>
                                        <tr v-if="isLoading">
                                            <td colspan="9">
                                                <div class="lds-facebook"><div></div><div></div><div></div></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="col">No.</th>
                                            <th scope="col">Lokasi</th>
                                            <th scope="col">Nama</th>
                                            <th scope="col">NIK</th>
                                            <th scope="col">Department</th>
                                            <th scope="col">Position Name</th>
                                            <th scope="col">Level</th>
                                            <th scope="col">Join Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <template v-for="arr_employee,index in employee2">
                                            <tr>
                                                <td>{{ index+1 }}</td>
                                                <td>{{ arr_employee.office }}</td>
                                                <td style="text-transform: capitalize">{{ arr_employee.emp_name }}</td>
                                                <td>{{ arr_employee.nik }}</td>
                                                <td>{{ arr_employee.department }}</td>
                                                <td>{{ arr_employee.position }}</td>
                                                <td>{{ arr_employee.level }}</td>
                                                <td>{{ arr_employee.join_date }}</td>
                                            </tr>
                                        </template>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </br></br>
                        <div class="modal fade" id="add_mapping_posisi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <form @submit.prevent="saveMappingPosisi">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title font-weight-bold" id="exampleModalLabel">Tambah Struktur</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div id="position-modal" class="modal-body">
                                            <div class="form-group">
                                                <label>Office</label>
                                                <input type="text" class="form-control" value="<?= $kd_off; ?>" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label>Departemen</label>
                                                <v-select style="text-transform: capitalize" label="namadepartemen" :options="department" v-model="v_department" @input="getPosition" require></v-select>
                                            </div>
                                            <div class="form-group">
                                                <label>Posisi</label>
                                                <v-select style="text-transform: capitalize" label="position" :options="position" v-model="v_posisi" require></v-select>
                                            </div>
                                            <div class="form-group">
                                                <label>Atasan</label>
                                                <v-select style="text-transform: capitalize" label="position" :options="position_parent" v-model="v_posisi_parent" require></v-select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="modal fade" id="delete_post" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title font-weight-bold" id="exampleModalLabel">Hapus Struktur</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div id="position-modal" class="modal-body">
                                        <h5>Are You Sure Want To Delete <a class="font-weight-bold">{{ post_name }}</a> ? </h5>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-danger" @click="deleteMappingPosisi">Delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="confirm_post" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title font-weight-bold" id="exampleModalLabel">Gagal Hapus</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div id="position-modal" class="modal-body">
                                        <h5>Kode Posisi Memiliki Bawahan</h5>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> */ ?>
                    <!-- <organisasi_new v-if="organisasi_new"></organisasi_new> -->
                    <struktur-baru :username = "username"></struktur-baru>
                    <struktur-org v-if="struktur_org"></struktur-org>
                    <edit-component :username = "username"></edit-component>
                    <hapus_component></hapus_component>

                    <!-- PASANGAN PENILAI -->
                    <penilai v-if="penilai" @view_detail_karyawan="v_detail_karyawan"></penilai>
                    <detail_karyawan></detail_karyawan>

                    <list_periode v-if="list_periode"></list_periode>

                    <!-- INDIKATOR DAN SUB INDIKATOR -->
                    <detail_indikator_component></detail_indikator_component>
                    <list_indikator v-if="list_indikator" :username="username" @view_sub_indikator="view_sub_indikator"></list_indikator>
                    <sub_list_indikator></sub_list_indikator>
                    <edit_sub_indikator_component></edit_sub_indikator_component>
                    <hapus_sub_indikator_component></hapus_sub_indikator_component>
                    <!-- <sub_list_indikator v-if="sub_list_indikator" :kd_indikator="kd_indikator"></sub_list_indikator> -->

                    <!-- HRD SETTING -->
                    <hrd_setting v-if="hrd_setting"></hrd_setting>
                    <!-- <edit_hrd_setting></edit_hrd_setting> -->
                    <hapus_hrd_setting></hapus_hrd_setting>

                    <!-- PENILAIAN HRD -->
                    <penilaian_karyawan_hrd :username="username"></penilaian_karyawan_hrd>
                    <div class="col-md-12">
                        <test></test>
                    </div>
                    <penilaian @test="test2">
                        <template v-slot:header>
                            <progress-bar :value="progressvalue" v-if="question"></progress-bar>
                            <div class="row">
                                <div class="col-md-10">
                                    <indikator :indikators="tab_indikator" :v_indikator="v_indikator" v-if="question"></indikator>
                                </div>
                                <div class="col-md-2">
                                    <timer :time="timer" v-if="question"></timer>
                                </div>
                        </template>
                        <template v-slot:body>
                            <div class="row">
                                <div class="col-md-8">
                                    <questions :show="show" :pertanyaan="subIndikator" :step="currentStep" :nilai="nilai" :bobot="bobot" v-if="question">
                                        <nilai-indikator :items="items" @selectedvalue="nilaipilihan"></nilai-indikator>
                                    </questions>
                                    <thanks_card v-if="thankyou"></thanks_card>
                                </div>
                                <div class="col-md-4">
                                    <employee-card :employees="employees" :total="totalnilai"></employee-card>
                                </div>
                            </div>
                            <p>&nbsp;</p>
                        </template>
                        <template v-slot:footer>
                            <div class="row">
                                <div class="col-md-8">
                                    <nilai-wizard :nilai="nilai" :totalsteps="totalsteps" :startstep="currentStep" @wizard="wizardstep"></nilai-wizard>
                                </div>
                                <div class="col-md-4">
                                    <btn-penilaian :condition="question" :classbtn="btnCancelClass" :nama="btnCancel" @cancel="cancel" v-if="question" ></btn-penilaian>
                                    <btn-penilaian :classbtn="btnInfoClass" :nama="btnInfo" v-else @close="tutup"></btn-penilaian>
                                </div>
                            </div>
                            
                        </template>
                    </penilaian>


                    <!-- <nilai_employee_hrd :username="username"></nilai_employee_hrd> -->

                    <!-- LAPORAN -->
                    <laporan_component v-if="lihatlaporan"></laporan_component>
                    <laporan_all v-if="lihatlaporanAll"></laporan_all>
                    <detail_laporan></detail_laporan>
                    <repot_personal></repot_personal>
                    <session_personal v-if="jadwal"></session_personal>
                    <laporan_all_pasangan_penilai v-if="penilai_byCabang"></laporan_all_pasangan_penilai>
                    <laporan_all_by_cabang v-if="lihatlaporanCabang"></laporan_all_by_cabang>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row" v-if="non_access">
            <div class="col-md-12">
                <h4>Anda Tidak Memiliki Akses Untuk Halaman Ini</h4>
            </div>
        </div>
    </section><!-- /.content -->
</aside><!-- /.content-wrapper -->

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.0/dist/sweetalert2.all.min.js"></script>
<script src="_component/penilaian_component_2.js"></script>
<script type="text/javascript">
    Vue.use(VueTables.ClientTable);
    Vue.component('v-select', VueSelect.VueSelect);

    $('#exportExcel').on('click', function() {
    	console.log('hello world');
    });

    function change(){
        document.getElementById("myform").submit();
    }

    function change2(){
        document.getElementById("myform2").submit();
    }
    
    function getPosisiByKdOff() {
        document.getElementById("form_posisi").submit();
    }
    
    var app = new Vue({
        el: '#app',
        data: {
            isLoading: false,
            struktur_org:false,
            aktif: false,
            newPenilaian: false,
            listedPenilaian: false,
            lihathasilpenilaian: false,
            showedpa: false,
            aktifasi: false,
            lihatnilai: false,
            lihatlaporan: true,
            lihatlaporanAll: false,
            lihatlaporanCabang: false,
            jadwal: false,
            organisasi: false,
            organisasi_new: false,
            list_periode: false,
            list_indikator: false,
            sub_list_indikator: false,
            hrd_setting: false,
            detail_karyawan: false,
            penilai: false,
            penilai_byCabang: false,
            aturpenilai: false,
            buatpengaturanpenilai: false,
            isChart: false,
            paid: 0,
            panomor: '',
            viewnilai: false,
            kodelevel:'',
            periode: '',
            level:'',
            username: '<?php echo $_SESSION['username'] ?>',
            kd_off: '<?php echo $kd_off; ?>',
            id_num: '',
            id_periode_indikator: '',
            position:[],
            position_by_id:[],
            count_position_by_kd: [],
            position_by_kd: [],
            employee: [],
            employee2: [],
            karyawan: [],
            xOffice: '',
            xPosition: '',
            id_num_employee: '',
            id_post: '',
            kd_post: '',
            xid_post: '',
            post_name: '',
            kd_post_parent: '',
            check_kd: [],
            isCheckPenilai: false,
            office: [],
            department: [],
            position: [],
            position_parent: [],
            position_all: "",
            v_office: '',
            v_department: '',
            v_posisi: '',
            v_posisi_parent: '',
            posisi_new: false,
            data_posisi_new: [],
            v_posisi_new: '',
            atasan_new: false,
            kd_indikator: '',
            show: true,
            thankyou: false,
            question: true,
            xusername: '<?php echo $_SESSION['hrm_pin'] ?>',
            items: [
                { id: 1, ket: 'Kurang Sekali', class: ''},
                { id: 2, ket: 'Kurang', class: ''},
                { id: 3, ket: 'Baik', class: ''},
                { id: 4, ket: 'Baik Sekali', class: ''},
                { id: 5, ket: 'Istimewa', class: ''},
            ],
            timer: 900,
            btnPrev: 'Kembali',
            btnPrevClass: 'btn btn-primary btn-lg',
            btnNext: 'Lanjutkan',
            btnNextClass: 'btn btn-primary btn-lg',
            btnCancel: 'Tutup, saya akan nilai ulang',
            btnCancelClass: 'btn btn-danger btn-lg',
            btnInfo: 'Selesai',
            btnInfoClass: 'btn btn-info btn-lg',
            totalsteps: 0,
            currentStep: 1,
            nilai: 0,
            progressvalue: 0,
            allProgress: 0,
            bobot: 0,
            employees: [],
            tab_indikator: [],
            setPertanyaan: [],
            v_indikator: '',
            indexing: 0,
            subIndikator: '',
            max_urutan: '',
            totalnilai: 0,
            penilaian_arr: [],
            index_nilai: [],
            id_num_target: '',
            type_penilai: '',
            id_level: '',
            penilai_karyawan: [],
            detail_indikator: [],
            xtype_penilai: '',
            saveAll: [],
            detail_username: [],
            access: false,
            non_access: true
        },
        watch: {
            // show(value) {
            // 	if(value === true) {
            // 		return this.show = false
            // 	} else {
            // 		return this.show = true
            // 	}
            // }
            currentStep(value) {
                if(value === this.totalsteps) {
                    return this.progressvalue = 100;
                }
            },
            totalsteps(value) {
                if(value === this.currentStep) {
                    return this.progressvalue = 100;
                }
            }
        },
        methods: {
            closedAktifasi() {
                this.aktifasi = false;
            },
            forceRerender() {
                this.componentKey += 1;  
            },
            createNewPenilaian() {
                this.newPenilaian = true;
                this.showedpa = false;
                this.listedPenilaian = false;
                this.aktifasi= false;
                this.lihatnilai = false;
                this.viewnilai = false;
                this.lihathasilpenilaian = false;
                this.lihatlaporan = false;
                this.lihatlaporanAll = false;
                this.lihatlaporanCabang = false;
                this.jadwal = false;
                this.organisasi = false;
                this.organisasi_new = false;
                this.detail_karyawan = false;
                this.penilai = false;
                this.penilai_byCabang = false;
                this.aturpenilai= false;
                this.buatpengaturanpenilai= false;
                this.isChart = false;
                this.isCheckPenilai = false;
            },
            aktifasiPenilaian() {
                this.aktifasi = true;
                this.newPenilaian = false;
                this.showedpa = false,
                this.listedPenilaian = true;
                this.lihatnilai = false;
                this.viewnilai = false;
                this.lihathasilpenilaian = false;
                this.lihatlaporan = false;
                this.lihatlaporanAll = false;
                this.lihatlaporanCabang = false;
                this.jadwal = false;
                this.organisasi = false;
                this.organisasi_new = false;
                this.detail_karyawan = false;
                this.penilai = false;
                this.penilai_byCabang = false;
                this.aturpenilai= false;
                this.buatpengaturanpenilai= false;
                this.isChart = false;
                this.isCheckPenilai = false;
            },
            closedlisted() {
                this.listedPenilaian = false;
            },
            closedNewPenilaian() {
                this.newPenilaian = false;
            },
            refreshList() {
                this.listedPenilaian = true;
                this.componentKey += 1; 
            },
            viewpa(id) {
                this.showedpa = true;
                this.listedPenilaian = false;
                this.newPenilaian = false;
                this.lihatnilai = false;
                this.viewnilai = false;
                this.paid = id;
                this.lihathasilpenilaian = false;
                this.lihatlaporan = false;
                this.lihatlaporanAll = false;
                this.lihatlaporanCabang = false;
                this.jadwal = false;
                this.organisasi = false;
                this.organisasi_new = false;
                this.detail_karyawan = false;
                this.penilai = false;
                this.penilai_byCabang = false;
                this.aturpenilai= false;
                this.buatpengaturanpenilai= false;
                this.isChart = false;
                this.isCheckPenilai = false;
            },
            aktifkanpa(id) {
                this.panomor= id;
                this.aktifasi = true;
                this.showedpa = false;
                this.listedPenilaian = false;
                this.newPenilaian = false;
                this.lihatnilai = false;
                this.viewnilai = false;
                this.lihathasilpenilaian = false;
                this.lihatlaporan = false;
                this.lihatlaporanAll = false;
                this.lihatlaporanCabang = false;
                this.jadwal = false;
                this.organisasi = false;
                this.organisasi_new = false;
                this.detail_karyawan = false;
                this.penilai = false;
                this.penilai_byCabang = false;
                this.aturpenilai= false;
                this.isChart = false;
                this.isCheckPenilai = false;
                this.buatpengaturanpenilai= false;
                let element = this.$refs.modal.$el
                $(element).modal('show')
            },
            lihatpenilaian() {
                this.lihatnilai = false;
                this.lihathasilpenilaian = true;
                this.showedpa = false;
                this.newPenilaian = false;
                this.listedPenilaian = false;
                this.aktifasi = false;
                this.viewnilai = false;
                this.lihatlaporan = false;
                this.lihatlaporanAll = false;
                this.lihatlaporanCabang = false;
                this.jadwal = false;
                this.organisasi = false;
                this.organisasi_new = false;
                this.detail_karyawan = false;
                this.penilai = false;
                this.penilai_byCabang = false;
                this.aturpenilai= false;
                this.buatpengaturanpenilai= false;
                this.isChart = false;
                this.isCheckPenilai = false;
            },
            viewpenilaian(nomor) {
                this.lihatnilai = false;
                this.showedpa = false;
                this.newPenilaian = false;
                this.listedPenilaian = false;
                this.aktifasi = false;
                this.viewnilai = true;
                this.lihathasilpenilaian = false;
                this.lihatlaporan = false;
                this.lihatlaporanAll = false;
                this.lihatlaporanCabang = false;
                this.jadwal = false;
                this.organisasi = false;
                this.organisasi_new = false;
                this.detail_karyawan = false;
                this.penilai = false;
                this.penilai_byCabang = false;
                this.panomor = nomor;
                this.aturpenilai= false;
                this.buatpengaturanpenilai= false;
                this.isChart = false;
                this.isCheckPenilai = false;
            },
            closedDetail() {
                this.lihatnilai = true;
                this.showedpa = false;
                this.newPenilaian = false;
                this.listedPenilaian = false;
                this.aktifasi = false;
                this.viewnilai = false;
                this.lihathasilpenilaian = false;
                this.lihatlaporan = false;
                this.lihatlaporanAll = false;
                this.lihatlaporanCabang = false;
                this.jadwal = false;
                this.organisasi = false;
                this.organisasi_new = false;
                this.detail_karyawan = false;
                this.penilai = false;
                this.penilai_byCabang = false;
                this.aturpenilai= false;
                this.buatpengaturanpenilai= false;
                this.isChart = false;
                this.isCheckPenilai = false;
            },
            viewdetail(periode,kodelevel,level) {
                this.kodelevel = kodelevel;
                this.periode = periode;
                this.level = level;
                this.lihatnilai = true;
                this.showedpa = false;
                this.newPenilaian = false;
                this.listedPenilaian = false;
                this.aktifasi = false;
                this.viewnilai = false;
                this.lihathasilpenilaian = false;
                this.lihatlaporan = false;
                this.lihatlaporanAll = false;
                this.lihatlaporanCabang = false;
                this.jadwal = false;
                this.organisasi = false;
                this.organisasi_new = false;
                this.detail_karyawan = false;
                this.penilai = false;
                this.penilai_byCabang = false;
                this.aturpenilai= false;
                this.buatpengaturanpenilai= false;
                this.isChart = false;
                this.isCheckPenilai = false;
            },
            laporan() {
                this.lihatnilai = false;
                this.showedpa = false;
                this.newPenilaian = false;
                this.listedPenilaian = false;
                this.aktifasi = false;
                this.viewnilai = false;
                this.lihathasilpenilaian = false;
                this.lihatlaporan = true;
                this.lihatlaporanAll = false;
                this.lihatlaporanCabang = false;
                this.jadwal = false;
                this.organisasi = false;
                this.organisasi_new = false;
                this.detail_karyawan = false;
                this.aturpenilai= false;
                this.penilai = false;
                this.penilai_byCabang = false;
                this.buatpengaturanpenilai= false;
                this.isChart = false;
                this.isCheckPenilai = false;
                const open = true;
                const close = false;
                EventBus.$emit('open_laporan', open);
                EventBus.$emit('close_pengaturan_hrd', close);
                EventBus.$emit('close_materi_penilaian', close);
                EventBus.$emit('close_pasangan_penilai', close);
                EventBus.$emit('close_detail_karyawan', close);
                EventBus.$emit('close_penilai_hrd', close);
                EventBus.$emit('close_laporan_detail', close);
            },
            laporanAll() {
                this.lihatnilai = false;
                this.showedpa = false;
                this.newPenilaian = false;
                this.listedPenilaian = false;
                this.aktifasi = false;
                this.viewnilai = false;
                this.lihathasilpenilaian = false;
                this.lihatlaporan = false;
                this.lihatlaporanAll = true;
                this.lihatlaporanCabang = false;
                this.jadwal = false;
                this.organisasi = false;
                this.organisasi_new = false;
                this.detail_karyawan = false;
                this.aturpenilai= false;
                this.penilai = false;
                this.penilai_byCabang = false;
                this.buatpengaturanpenilai= false;
                this.isChart = false;
                this.isCheckPenilai = false;
                EventBus.$emit('close_materi_penilaian');
            },
            laporanCabang() {
                this.lihatnilai = false;
                this.showedpa = false;
                this.newPenilaian = false;
                this.listedPenilaian = false;
                this.aktifasi = false;
                this.viewnilai = false;
                this.lihathasilpenilaian = false;
                this.lihatlaporan = false;
                this.lihatlaporanAll = false;
                this.lihatlaporanCabang = true;
                this.jadwal = false;
                this.organisasi = false;
                this.organisasi_new = false;
                this.detail_karyawan = false;
                this.aturpenilai= false;
                this.penilai = false;
                this.penilai_byCabang = false;
                this.buatpengaturanpenilai= false;
                this.isChart = false;
                this.isCheckPenilai = false;
                EventBus.$emit('close_materi_penilaian');
            },
            setting_jadwal() {
                this.lihatnilai = false;
                this.showedpa = false;
                this.newPenilaian = false;
                this.listedPenilaian = false;
                this.aktifasi = false;
                this.viewnilai = false;
                this.lihathasilpenilaian = false;
                this.lihatlaporan = false;
                this.lihatlaporanAll = false;
                this.lihatlaporanCabang = false;
                this.jadwal = true;
                this.organisasi = false;
                this.organisasi_new = false;
                this.detail_karyawan = false;
                this.aturpenilai= false;
                this.penilai = false;
                this.penilai_byCabang = false;
                this.buatpengaturanpenilai= false;
                this.isChart = false;
                this.isCheckPenilai = false;
                var close = false;
                EventBus.$emit('close_materi_penilaian');
                EventBus.$emit('close_laporan_detail', close);
            },
            view_organisasi() {
                this.lihatnilai = false;
                this.showedpa = false;
                this.newPenilaian = false;
                this.listedPenilaian = false;
                this.aktifasi = false;
                this.viewnilai = false;
                this.lihathasilpenilaian = false;
                this.lihatlaporan = false;
                this.lihatlaporanAll = false;
                this.lihatlaporanCabang = false;
                this.jadwal = false;
                this.organisasi = true;
                this.organisasi_new = false;
                this.detail_karyawan = false;
                this.penilai = false;
                this.penilai_byCabang = false;
                this.list_periode = false;
                this.list_indikator = false;
                this.sub_list_indikator = false;
                this.hrd_setting = false;
                this.aturpenilai= false;
                this.buatpengaturanpenilai= false;
                this.isChart = false;
                this.isCheckPenilai = false;
            },
            view_organisasi_new() {
                this.struktur_org = true
                this.lihatnilai = false;
                this.showedpa = false;
                this.newPenilaian = false;
                this.listedPenilaian = false;
                this.aktifasi = false;
                this.viewnilai = false;
                this.lihathasilpenilaian = false;
                this.lihatlaporan = false;
                this.lihatlaporanAll = false;
                this.lihatlaporanCabang = false;
                this.jadwal = false;
                this.organisasi = false;
                this.organisasi_new = false;
                this.detail_karyawan = false;
                this.penilai = false;
                this.penilai_byCabang = false;
                this.list_periode = false;
                this.list_indikator = false;
                this.sub_list_indikator = false;
                this.hrd_setting = false;
                this.aturpenilai= false;
                this.buatpengaturanpenilai= false;
                this.isChart = false;
                this.isCheckPenilai = false;
                // EventBus.$emit('detail_karyawan', this.detail_karyawan = false)
                EventBus.$emit('sub_list_indikator', this.sub_list_indikator = false)
                EventBus.$emit('detail_indikator_component', this.list_indikator = false)
            },
            view_penilai() {
                this.lihatnilai = false;
                this.showedpa = false;
                this.newPenilaian = false;
                this.listedPenilaian = false;
                this.aktifasi = false;
                this.viewnilai = false;
                this.lihathasilpenilaian = false;
                this.lihatlaporan = false;
                this.lihatlaporanAll = false;
                this.lihatlaporanCabang = false;
                this.jadwal = false;
                this.organisasi = false;
                this.organisasi_new = false;
                this.detail_karyawan = false;
                this.penilai = true;
                this.penilai_byCabang = false;
                this.list_periode = false;
                this.list_indikator = false;
                this.sub_list_indikator = false;
                this.hrd_setting = false;
                this.aturpenilai= false;
                this.buatpengaturanpenilai= false;
                this.isChart = false;
                this.isCheckPenilai = false;
                this.struktur_org = false
                const close = false;
                // EventBus.$emit('struktur-org', this.struktur_org = false)
                // EventBus.$emit('struktur-baru', this.struktur_org = false)
                EventBus.$emit('sub_list_indikator', this.sub_list_indikator = false)
                EventBus.$emit('detail_indikator_component', this.list_indikator = false)
                EventBus.$emit('refresh', this.penilai)
                EventBus.$emit('tutup_detail_karyawan', this.detail_karyawan)
                EventBus.$emit('close_penilai_hrd', close);
                EventBus.$emit('close_laporan', close);
                EventBus.$emit('close_laporan_detail', close);
            },
            view_penilai_byCabang() {
                this.lihatnilai = false;
                this.showedpa = false;
                this.newPenilaian = false;
                this.listedPenilaian = false;
                this.aktifasi = false;
                this.viewnilai = false;
                this.lihathasilpenilaian = false;
                this.lihatlaporan = false;
                this.lihatlaporanAll = false;
                this.lihatlaporanCabang = false;
                this.jadwal = false;
                this.organisasi = false;
                this.organisasi_new = false;
                this.detail_karyawan = true;
                this.penilai = false;
                this.penilai_byCabang = true;
                this.list_periode = false;
                this.list_indikator = false;
                this.sub_list_indikator = false;
                this.hrd_setting = false;
                this.aturpenilai= false;
                this.buatpengaturanpenilai = false;
                this.isChart = false;
                this.isCheckPenilai = false;
                const open = true;
                const close = false;
                EventBus.$emit('open_laporan_by_cabang', open);
            },
            v_detail_karyawan(id) {
                this.lihatnilai = false;
                this.showedpa = false;
                this.newPenilaian = false;
                this.listedPenilaian = false;
                this.aktifasi = false;
                this.viewnilai = false;
                this.lihathasilpenilaian = false;
                this.lihatlaporan = false;
                this.lihatlaporanAll = false;
                this.lihatlaporanCabang = false;
                this.jadwal = false;
                this.organisasi = false;
                this.organisasi_new = false;
                this.detail_karyawan = true;
                this.id_num = id;
                this.penilai = false;
                this.penilai_byCabang = false;
                this.list_periode = false;
                this.list_indikator = false;
                this.sub_list_indikator = false;
                this.hrd_setting = false;
                this.aturpenilai= false;
                this.buatpengaturanpenilai = false;
                this.isChart = false;
                this.isCheckPenilai = false;
                EventBus.$emit('close_penilai_hrd', close);
                EventBus.$emit('close_laporan', close);
                EventBus.$emit('close_laporan_detail', close);
            },
            view_periode() {
                this.lihatnilai = false;
                this.showedpa = false;
                this.newPenilaian = false;
                this.listedPenilaian = false;
                this.aktifasi = false;
                this.viewnilai = false;
                this.lihathasilpenilaian = false;
                this.lihatlaporan = false;
                this.lihatlaporanAll = false;
                this.lihatlaporanCabang = false;
                this.jadwal = false;
                this.organisasi = false;
                this.organisasi_new = false;
                this.detail_karyawan = false;
                this.penilai = false;
                this.penilai_byCabang = false;
                this.list_periode = true;
                this.list_indikator = false;
                this.sub_list_indikator = false;
                this.hrd_setting = false;
                this.aturpenilai= false;
                this.buatpengaturanpenilai = false;
                this.isChart = false;
                this.isCheckPenilai = false;
            },
            view_indikator(id) {
                this.lihatnilai = false;
                this.showedpa = false;
                this.newPenilaian = false;
                this.listedPenilaian = false;
                this.aktifasi = false;
                this.viewnilai = false;
                this.lihathasilpenilaian = false;
                this.lihatlaporan = false;
                this.lihatlaporanAll = false;
                this.lihatlaporanCabang = false;
                this.jadwal = false;
                this.organisasi = false;
                this.organisasi_new = false;
                this.detail_karyawan = false;
                this.penilai = false;
                this.penilai_byCabang = false;
                this.id_periode_indikator = id;
                this.list_periode = false;
                this.list_indikator = true;
                this.sub_list_indikator = false;
                this.hrd_setting = false;
                this.aturpenilai= false;
                this.buatpengaturanpenilai = false;
                this.isChart = false;
                this.isCheckPenilai = false;
                const close = false
                EventBus.$emit('refreshListIndikator', this.list_indikator = true)
                EventBus.$emit('tutup_detail_karyawan', this.detail_karyawan = false)
                EventBus.$emit('tutupSubListIndikator', this.sub_list_indikator)
                EventBus.$emit('close_penilai_hrd', close)
                EventBus.$emit('close_laporan', close);
                EventBus.$emit('close_laporan_detail', close);
                EventBus.$emit('open_list_indikator');
                // EventBus.$emit('struktur-org', this.struktur_org = false)
                // EventBus.$emit('struktur-baru', this.struktur_org = false)
            },
            view_sub_indikator(kd) {
                this.lihatnilai = false;
                this.showedpa = false;
                this.newPenilaian = false;
                this.listedPenilaian = false;
                this.aktifasi = false;
                this.viewnilai = false;
                this.lihathasilpenilaian = false;
                this.lihatlaporan = false;
                this.lihatlaporanAll = false;
                this.lihatlaporanCabang = false;
                this.jadwal = false;
                this.organisasi = false;
                this.organisasi_new = false;
                this.detail_karyawan = false;
                this.penilai = false;
                this.penilai_byCabang = false;
                this.list_periode = false;
                this.list_indikator = false;
                this.sub_list_indikator = true;
                this.kd_indikator = kd;
                this.hrd_setting = false;
                this.aturpenilai= false;
                this.buatpengaturanpenilai = false;
                this.isChart = false;
                this.isCheckPenilai = false;
            },
            view_hrd_setting(){
                this.lihatnilai = false;
                this.showedpa = false;
                this.newPenilaian = false;
                this.listedPenilaian = false;
                this.aktifasi = false;
                this.viewnilai = false;
                this.lihathasilpenilaian = false;
                this.lihatlaporan = false;
                this.lihatlaporanAll = false;
                this.lihatlaporanCabang = false;
                this.jadwal = false;
                this.organisasi = false;
                this.organisasi_new = false;
                this.detail_karyawan = false;
                this.penilai = false;
                this.penilai_byCabang = false;
                this.list_periode = false;
                this.list_indikator = false;
                this.sub_list_indikator = false;
                this.hrd_setting = true;
                this.aturpenilai= false;
                this.buatpengaturanpenilai = false;
                this.isChart = false;
                this.isCheckPenilai = false;
                const close = false;
                const open = true;
                // EventBus.$emit('op')
                EventBus.$emit('open_hrd_setting');
                EventBus.$emit('close_detail_karyawan', close);
                EventBus.$emit('close_laporan', close);
                EventBus.$emit('close_laporan_detail', close);
                EventBus.$emit('close_penilai_hrd', close);
            },
            view_penilai_hrd() {
                const open = true;
                const close = false;
                EventBus.$emit('open_penilai_hrd', open);
                EventBus.$emit('close_pengaturan_hrd', close);
                EventBus.$emit('close_materi_penilaian', close);
                EventBus.$emit('close_pasangan_penilai', close);
                EventBus.$emit('close_detail_karyawan', close);
                EventBus.$emit('close_laporan', close);
                EventBus.$emit('close_laporan_detail', close);
            },
            pengaturanPenilai() {
                this.lihatnilai = false;
                this.showedpa = false;
                this.newPenilaian = false;
                this.listedPenilaian = false;
                this.aktifasi = false;
                this.viewnilai = false;
                this.lihathasilpenilaian = false;
                this.lihatlaporan = false;
                this.lihatlaporanAll = false;
                this.lihatlaporanCabang = false;
                this.jadwal = false;
                this.organisasi = false;
                this.organisasi_new = false;
                this.detail_karyawan = false;
                this.penilai = false;
                this.penilai_byCabang = false;
                this.aturpenilai= true;
                this.buatpengaturanpenilai= false;
                this.isChart = false;
                this.isCheckPenilai = false;
            },
            buataturanpenilai() {
                this.lihatnilai = false;
                this.showedpa = false;
                this.newPenilaian = false;
                this.listedPenilaian = false;
                this.aktifasi = false;
                this.viewnilai = false;
                this.lihathasilpenilaian = false;
                this.lihatlaporan = false;
                this.lihatlaporanAll = false;
                this.lihatlaporanCabang = false;
                this.jadwal = false;
                this.organisasi = false;
                this.organisasi_new = false;
                this.detail_karyawan = false;
                this.penilai = false;
                this.penilai_byCabang = false;
                this.aturpenilai= false;
                this.buatpengaturanpenilai= true;
                this.isChart = false;
                this.isCheckPenilai = false;
            },
            grafikpenilaian() {
                this.lihatnilai = false;
                this.showedpa = false;
                this.newPenilaian = false;
                this.listedPenilaian = false;
                this.aktifasi = false;
                this.viewnilai = false;
                this.lihathasilpenilaian = false;
                this.lihatlaporan = false;
                this.lihatlaporanAll = false;
                this.lihatlaporanCabang = false;
                this.jadwal = false;
                this.organisasi = false;
                this.organisasi_new = false;
                this.detail_karyawan = false;
                this.penilai = false;
                this.penilai_byCabang = false;
                this.aturpenilai= false;
                this.buatpengaturanpenilai= false;
                this.isChart = true;
                this.isCheckPenilai = false;
            },
            checkhasilpenilai() {
                this.lihatnilai = false;
                this.showedpa = false;
                this.newPenilaian = false;
                this.listedPenilaian = false;
                this.aktifasi = false;
                this.viewnilai = false;
                this.lihathasilpenilaian = false;
                this.lihatlaporan = false;
                this.lihatlaporanAll = false;
                this.lihatlaporanCabang = false;
                this.jadwal = false;
                this.organisasi = false;
                this.organisasi_new = false;
                this.detail_karyawan = false;
                this.penilai = false;
                this.penilai_byCabang = false;
                this.aturpenilai= false;
                this.buatpengaturanpenilai= false;
                this.isChart = false;
                this.isCheckPenilai = true;
            },
            getDataPosition: function(id) {
                this.isLoading = true;

                axios.post('_component/data_penilaian_2.php', {
                    request: "getDataPositionById",
                    id_post: id,
                    kd_off: this.kd_off
                }).then(response => {
                    this.isLoading = false;

                    this.id_post = response.data[0].id_post;
                    this.kd_post = response.data[0].kd_post;
                    this.post_name = response.data[0].position_name;
                    this.kd_post_parent = response.data[0].kd_post_parent;

                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });

                axios.post('_component/data_penilaian_2.php', {
                    request: "getDataPosition",
                    kd_off: this.kd_off
                }).then(response => {
                    this.isLoading = false;

                    this.position = response.data[0];
                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });
            },
            updateDataPosition: function() {
                let id_position = this.id_post, kode_post = this.kd_post, position_name = this.post_name, kode_post_parent = this.kd_post_parent, kd_office = this.kd_off;
                let arr_count = "";

                axios.post('_component/data_penilaian_2.php', {
                    request: "checkUpdatePosition",
                    kd_post: kode_post,
                    kd_off: kd_office,
                    id_post: id_position,
                }).then(response => {
                    this.isLoading = false;

                    this.check_kd = response.data[0];
                    arr_count = response.data[1];

                    if (kode_post == this.check_kd['kd_post']) {
                        this.executeUpdatePosition();
                    } else if (kode_post != this.check_kd['kd_post'] && arr_count['count'] < 1) {
                        this.executeUpdatePosition();
                    } else {
                        alert("Kode Sudah Digunakan");
                    }

                }).catch (error => {
                    console.log(error);
                    this.isLoading = false;
                });
            },
            executeUpdatePosition: function () {
                let id_position = this.id_post, kode_post = this.kd_post, position_name = this.post_name, kode_post_parent = this.kd_post_parent, kd_office = this.kd_off;
                let arr_count = "";

                axios.post('_component/data_penilaian_2.php', {
                    request: "updatePosition",
                    id_position: id_position,
                    kode_post: kode_post, 
                    position_name: position_name,
                    kode_post_parent: kode_post_parent
                }).then(response => {
                    this.isLoading = false;

                    alert('Data Updated');
                    window.location.href = "/hrm/penilaiankaryawanadmin_a1?kd_off="+kd_office;

                }).catch (error => {
                    console.log(error);
                    this.isLoading = false;
                });

            },
            DeleteDataPosition: function() {
                let id_position = this.id_post, kd_post = this.kd_post, kd_off = this.kd_off;
                let count_parent = "";

                axios.post('_component/data_penilaian_2.php', {
                    request: "checkDeletePosition",
                    id_position: id_position,
                    kode_post: kd_post,
                    kd_off: kd_off,
                }).then(response => {
                    this.isLoading = false;

                    count_parent = response.data[0].count;
                    count_employee = response.data[1].count;

                    if (count_parent > 0) {
                        alert('Kode Posisi Memiliki Bawahan');
                    } else {
                        if(count_employee > 0) {
                            alert('Kode Posisi Memiliki Karyawan');
                        } else {
                            this.deletePosition();
                        }
                    }

                }).catch (error => {
                    console.log(error);
                    this.isLoading = false;
                });
            },
            deletePosition: function () {
                let id_position = this.id_post, kd_office = this.kd_off;

                axios.post('_component/data_penilaian_2.php', {
                    request: "deletePosition",
                    id_position: id_position
                }).then(response => {
                    this.isLoading = false;

                    alert("Hapus Data Posisi Berhasil");
                    window.location.href = "/hrm/penilaiankaryawanadmin_a1?kd_off="+kd_office;


                }).catch (error => {
                    console.log(error);
                    this.isLoading = false;
                });
            },
            getDataKaryawanByKodePosition(id_posisi) {
                let kd_office = this.kd_off, id_position = id_posisi;
                this.xid_post = id_posisi;

                this.isLoading = true;

                console.log(kd_office+","+id_position);

                if (id_position) {

                    axios.post('_component/data_penilaian_2.php', {
                        request: "getlistemployeeByKodePosition",
                        kd_off: kd_office,
                        id_post: id_position
                    }).then(response => {
                        this.isLoading = false;

                        this.employee = response.data[0];

                        if (this.employee.length > 0) {
                            this.xOffice = this.employee[0].office;
                            this.xPosition = this.employee[0].position_name;
                        } else {
                            this.xOffice = "";
                            this.xPosition = "";
                        }

                    }).catch(error => {
                        console.log(error);
                        this.isLoading = false;
                    });

                } else {

                    axios.post('_component/data_penilaian_2.php', {
                        request: "getlistemployeeAllByKodeOff",
                        kd_off: kd_office,
                    }).then(response => {
                        this.isLoading = false;

                        this.employee = response.data[0];

                    }).catch(error => {
                        console.log(error);
                        this.isLoading = false;
                    });

                }

            },
            getDataKaryawanByKodePosition2(kd_pos,kd_off) {
                let kd_office = kd_off, kode_pos = kd_pos;
                let lengt_employee;
                let and = '';

                axios.post('_component/data_penilaian_2.php', {
                    request: "checkgetlistemployeeByKode",
                    kd_off: kd_off,
                    kode_pos: kd_pos
                }).then(response => {
                    this.isLoading = false;

                    lengt_employee = response.data[0].length;
                    if (lengt_employee > 0) {
                        and = 1;
                    } else {
                        and = 0;
                    }

                    axios.post('_component/data_penilaian_2.php', {
                        request: "getlistemployeeByKode",
                        kd_off: kd_office,
                        kode_pos: kd_pos,
                        and: and
                    }).then(response => {
                        this.isLoading = false;

                        this.employee2 = response.data[0];

                    }).catch(error => {
                        console.log(error);
                        this.isLoading = false;
                    });

                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });
            },
            getEmployee() {
                this.isLoading = true;

                axios.post('_component/data_penilaian_2.php', {
                    request: "getlistemployee",
                }).then(response => {
                    this.isLoading = false;
                    
                    this.karyawan = response.data[1];
        
                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });
            },
            SaveEmployee() {
                let kode_office = this.kd_off, id_position = this.xid_post, id_num = this.id_num_employee.id_num;
                let kode_dept = this.id_num_employee.kd_dept, kode_level = this.id_num_employee.kd_level;

                console.log(kode_office+","+id_position);

                if (id_position && id_num) {
                    
                    axios.post('_component/data_penilaian_2.php', {
                        request: "saveEmployee",
                        id_post: id_position,
                        kd_off: kode_office,
                        id_num: id_num,
                        kd_dept: kode_dept,
                        kd_level: kode_level
                    }).then(response => {
                        this.isLoading = false;
                        alert("Data Berhasil Disimpan");
                        $('#add_karyawan').modal('hide');
                        this.getDataKaryawanByKodePosition(id_position);
                    }).catch (error => {
                        console.log(error);
                        this.isLoading = false;
                    });
                
                } else {
                    alert("Data Karyawan / Position Kosong");
                }
            },
            deleteEmployee(id) {
                let kode_position = this.kd_post, id_position = this.xid_post;

                if(confirm("Do you really want to delete?")){

                    axios.post('_component/data_penilaian_2.php', {
                        request: "deleteEmployee",
                        id_num: id,
                    }).then(response => {
                        this.isLoading = false;
                        alert("Data Berhasil Di Delete");
                        this.getDataKaryawanByKodePosition(id_position);
                    }).catch (error => {
                        console.log(error);
                        this.isLoading = false;
                    });

                }

            },
            getOfficeDept() {
                axios.post('_component/data_penilaian_2.php', {
                    request: "getOfficeDept",
                    username: this.username,
                }).then(response => {
                    this.isLoading = false;

                    this.office = response.data[4];
                    this.department = response.data[1];
        
                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });
            },
            getPosition() {
                let kd_dept = '';
                if(this.v_department != null) {
                    kd_dept = this.v_department.kode;
                } else {
                    kd_dept = '';
                }

                axios.post('_component/data_penilaian_2.php', {
                    request: "getPosisi",
                    kd_off: this.kd_off,
                    kd_dept: kd_dept
                }).then(response => {
                    this.isLoading = false;

                    this.position = response.data[0];

                    this.getPosisiParent();
        
                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });
            },
            getPosisiParent() {
                axios.post('_component/data_penilaian_2.php', {
                    request: "getPosisiParent",
                }).then(response => {
                    this.isLoading = false;

                    this.position_parent = response.data[0];
                    this.position_parent.push(
                        {
                            "id_pos": "0",
                            "kd_pos": "0",
                            "position": "NONE",
                            "description": "0",
                        }
                    );
                    console.log(this.position_parent);
        
                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });
            },
            getPositionAll() {
                this.isLoading = true;

                let kd_office = "";

                if(this.v_office == null){
                    kd_office = "";
                } else {
                    kd_office = this.v_office.kd_off;
                }
                
                axios.post('_component/data_penilaian_2.php', {
                    request: "getPosisiAll",
                    kd_office: kd_office,
                }).then(response => {
                    this.isLoading = false;

                    this.position_all = response.data;
        
                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });
            },
            saveMappingPosisi() {

                let kd_off = this.kd_off;
                let id_dept = this.v_department.id_dept;
                let kd_dept = this.v_department.kode;
                let id_pos = this.v_posisi.id_pos;
                let kd_pos = this.v_posisi.kd_pos;
                let id_pos_parent = this.v_posisi_parent.id_pos;

                axios.post('_component/data_penilaian_2.php', {
                    request: "saveMappingPosisi",
                    kd_off: kd_off,
                    id_dept: id_dept,
                    kd_dept: kd_dept,
                    id_pos: id_pos,
                    kd_pos: kd_pos,
                    id_pos_parent: id_pos_parent,
                }).then(response => {
                    this.isLoading = false;

                    $("#add_mapping_posisi").modal('hide');

                    this.getPositionAll();
                    this.getOfficeDept();
                    this.getPosition();
                    this.getPosisiParent();

                    // this.v_office = '';
                    this.v_department = '';
                    this.v_posisi = '';
                    this.v_posisi_parent = '';

                    location.reload();

                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });
            },
            getMappingPosisiByKd(kode_posisi) {
                let kd_pos = kode_posisi; 
                let kd_off = this.kd_off;

                axios.post('_component/data_penilaian_2.php', {
                    request: "getPosisiByKd",
                    kd_pos: kd_pos,
                    kd_office: kd_off
                }).then(response => {
                    this.isLoading = false;

                    this.count_position_by_kd = response.data[0][0];
                    this.position_by_kd = response.data[1][0];

                    this.post_name = this.position_by_kd.position;
        
                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });
            },
            deleteMappingPosisi() {

                if (this.count_position_by_kd.count > 0) {
                    $('#delete_post').modal('hide');
                    $('#confirm_post').modal('show');
                } else {
                    axios.post('_component/data_penilaian_2.php', {
                        request: "deleteMappingPosisi",
                        kd_pos: this.position_by_kd.kd_pos,
                        kd_off: this.position_by_kd.kd_off,
                    }).then(response => {
                        this.isLoading = false;

                        location.reload();
            
                    }).catch(error => {
                        console.log(error);
                        this.isLoading = false;
                    });   
                }
            },
            empty_add_struktur() {
                this.v_department = '';
                this.v_posisi = '';
                this.v_posisi_parent = '';

                this.posisi_new = false;
            },
            tutup() {
                console.log("SELESAI TUTUP");
                this.thankyou = false;
                this.question = true;
                this.totalsteps= 0;
                this.currentStep= 1;
                this.nilai= 1;
                this.progressvalue= 0;
                this.allProgress= 0;
                this.bobot= 0;
                this.employees= [];
                this.tab_indikator= [];
                this.setPertanyaan= [];
                this.v_indikator= '';
                this.indexing= 0;
                this.subIndikator= '';
                this.max_urutan= '';
                this.totalnilai= 0;
                this.penilaian_arr= [];
                this.index_nilai= [];
                this.items= [
                { id: 1, ket: 'Tidak Memuaskan', class: ''},
                { id: 2, ket: 'Tidak Sesuai Harapan', class: ''},
                { id: 3, ket: 'Target Kerja Terpenuhi', class: ''},
                { id: 4, ket: 'Melebihi Target Kerja', class: ''},
                { id: 5, ket: 'Membawa Perubahan', class: ''},
                ];
                const close = false;
                const open = true;
                EventBus.$emit('close_peniliain', close);
                EventBus.$emit('table_penilaian');
            },
            cancel() {
                this.thankyou = false;
                this.question = true;
                this.totalsteps= 0;
                this.currentStep= 1;
                this.nilai= 1;
                this.progressvalue= 0;
                this.allProgress= 0;
                this.bobot= 0;
                this.employees= [];
                this.tab_indikator= [];
                this.setPertanyaan= [];
                this.v_indikator= '';
                this.indexing= 0;
                this.subIndikator= '';
                this.max_urutan= '';
                this.totalnilai= 0;
                this.penilaian_arr= [];
                this.index_nilai= [];
                this.items= [
                { id: 1, ket: 'Tidak Memuaskan', class: ''},
                { id: 2, ket: 'Tidak Sesuai Harapan', class: ''},
                { id: 3, ket: 'Target Kerja Terpenuhi', class: ''},
                { id: 4, ket: 'Melebihi Target Kerja', class: ''},
                { id: 5, ket: 'Membawa Perubahan', class: ''},
                ];
                const close = false;
                EventBus.$emit('close_peniliain', close);
            },
            wizardstep({stepping,action}) {
                if(action == 1) {
                    const lastNilai = this.index_nilai[this.index_nilai.length - 1];
                    // console.log(lastNilai);
                    this.totalnilai = this.totalnilai - lastNilai;
                    this.currentStep = stepping;
                    
                    this.indexing--
                    this.progressvalue = this.allProgress *  this.indexing;
                    
                    this.v_indikator = this.set_pertanyaan[this.indexing].indikator
                    
                    this.subIndikator = this.set_pertanyaan[this.indexing].sub_indikator
                    this.bobot = this.set_pertanyaan[this.indexing].bobot
                    
                    this.index_nilai.splice(-1, 1);
                    // console.table(this.index_nilai);

                    this.penilaian_arr.splice(-1, 1);
                    // console.table(this.penilaian_arr);

                    this.items = [
                        { id: 1, ket: 'Kurang Sekali', class: ''},
                        { id: 2, ket: 'Kurang', class: ''},
                        { id: 3, ket: 'Baik', class: ''},
                        { id: 4, ket: 'Baik Sekali', class: ''},
                        { id: 5, ket: 'Istimewa', class: ''},
                    ];

                } else if(action == 2) {
                    this.show = !this.show; 
                    // console.log(`${this.show} show atas`);

                    this.totalnilai += this.nilai * this.bobot;
                    this.currentStep = stepping;
                    this.index_nilai.push(this.nilai*this.bobot);
                    
                    this.set_pertanyaan[this.indexing].nilai = this.nilai;
                    this.set_pertanyaan[this.indexing].total = this.nilai*this.bobot;
                    
                    this.penilaian_arr.push(this.set_pertanyaan[this.indexing]);
                    
                    this.indexing++
                    this.progressvalue = this.allProgress *  this.indexing;
                    
                    this.v_indikator = this.set_pertanyaan[this.indexing].indikator
                    
                    this.subIndikator = this.set_pertanyaan[this.indexing].sub_indikator

                    this.bobot = this.set_pertanyaan[this.indexing].bobot;
                    
                    // console.log('masuk pertanyaan 2')

                    this.show = true;

                    this.items= [
                        { id: 1, ket: 'Kurang Sekali', class: ''},
                        { id: 2, ket: 'Kurang', class: ''},
                        { id: 3, ket: 'Baik', class: ''},
                        { id: 4, ket: 'Baik Sekali', class: ''},
                        { id: 5, ket: 'Istimewa', class: ''},
                    ];
                    this.nilai = 0;
                } else if (action == 3) {
                    this.totalnilai += this.nilai * this.bobot;
                    this.currentStep = stepping;
                    this.index_nilai.push(this.nilai*this.bobot);
                    
                    this.set_pertanyaan[this.indexing].nilai = this.nilai;
                    this.set_pertanyaan[this.indexing].total = this.nilai*this.bobot;
                    
                    this.penilaian_arr.push(this.set_pertanyaan[this.indexing]);

                    this.thankyou = true;
                    this.question = false;
                    
                    console.log("SAVE DISINI");

                    let thn_periode;
                    thn_periode = '';

                    const today = new Date();
                    const tahun = today.getFullYear();

                    thn_periode = tahun;
                    
                    axios.post('_component/data_penilaian_2.php', {
                        request: "savePenilaianNewHRD",
                        id_num_penilai: this.penilai_karyawan.id_num,
                        id_num_target: this.id_num_target,
                        penilaian: JSON.stringify(this.penilaian_arr),
                        detail_indikator: JSON.stringify(this.detail_indikator),
                        type_penilai: this.xtype_penilai,
                        periode: thn_periode
                    }).then(response => {
                        
                        axios.post('_component/data_penilaian_2.php', {
								request: "laporanGrandTotalPenilaian",
								id_num_target: this.id_num_target,
                                periode: thn_periode
                        }).then(response => {

                            this.saveAll = response.data[1];								
                            axios.post('_component/data_penilaian_2.php', {
                                request: "saveAllGrandTotal",
                                id_num_target: this.id_num_target,
                                all_grand_total: JSON.stringify(this.saveAll),
                                periode: thn_periode
                            }).then(response => {
                                
                                console.log(this.saveAll);

                            }).catch(error => {
                                console.log(error);
                                this.isLoading = false;
                            });

                        }).catch(error => {
                            console.log(error);
                            this.isLoading = false;
                        });	

                    }).catch(error => {
                        console.log(error);
                        this.isLoading = false;
                    });	
                }
            },
            nilaipilihan({val}) {
                this.nilai = val
            },
            test2(data) {
                this.id_num_target = data.target;
                this.type_penilai = data.penilai;
                this.id_level = data.level;
                console.log("masuk pertanyaan 2")
                this.getDetailEmployee(data.target, data.penilai, data.level)
                this.getIndikatorPeriode(data.level)
                this.getPenilaiKaryawan();
            },
            getDetailEmployee(data, penilai, level) {
                axios.post('_component/data_penilaian_2.php', {
                    request: "getDetailEmployee",
                    id_num: data,
                }).then(response => {
                    // console.log(response.data)
                    // this.isLoading = false;

                    this.employees = response.data[0];
                    // this.indikator_karyawan = response.data[1];

                    this.getIndikatorKriteria(response.data[1].kd_indikator, penilai, level);
                    this.getPertanyaanAll(response.data[1].kd_indikator, penilai, level);

                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });
            },
            getIndikatorPeriode(level) {
                axios.post('_component/data_penilaian_2.php', {
                    request: "getIndikatorPeriodePenilaian",
                    id_level: level,
                }).then(response => {
                    // this.isLoading = false;
                    // console.log(response.data)
                    // this.v_kd_indikator = response.data[0][0];
                    // console.log(this.v_kd_indikator)
                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });
            },
            getPenilaiKaryawan() {
                let username = this.xusername;

                axios.post('_component/data_penilaian_2.php', {
                    request: "getPenilaiKaryawan",
                    username: username,
                }).then(response => {
                    this.isLoading = false;

                    this.penilai_karyawan = response.data[0];

                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });
            },
            getIndikatorKriteria(kd, type_penilai, level) {
                console.log(`${kd} ${type_penilai} ${level}`)
                axios.post('_component/data_penilaian_2.php', {
                    request: "getIndikatorKriteria",
                    kd_indikator: kd,
                    type_penilai: type_penilai,
                    id_level: level
                }).then(response => {
                    this.isLoading = false;

                    // this.v_total_indikator = response.data[0].length;
                    this.tab_indikator = response.data[0];
                    // console.log(response.data[0]);
                    // this.getFirstIndikator(kd, type_penilai);

                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });
            },
            getPertanyaanAll(kd, type_penilai, level) {
                console.log(kd + "," + type_penilai + "," +level)
                axios.post('_component/data_penilaian_2.php', {
                    request: "getAllPertanyaan",
                    kd_indikator: kd,
                    type_penilai: type_penilai,
                    id_level: level
                }).then(response => {
                    this.isLoading = false;
                    // this.show = !this.show; 
                    
                    this.set_pertanyaan = response.data[0]
                    this.detail_indikator = response.data[1]
                    this.xtype_penilai = response.data[2]
                    
                    this.totalsteps = response.data[0].length;
                    this.allProgress = (100 / this.totalsteps) / 100;
                    this.indexing = 0

                    this.v_indikator = response.data[0][this.indexing].indikator;
                    this.bobot = response.data[0][this.indexing].bobot;
                    this.subIndikator = this.set_pertanyaan[this.indexing].sub_indikator;

                    console.log(response.data);

                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });
            },
            checkAccess() {
                let username = this.username;

                axios.post('_component/data_penilaian_2.php', {
                    request: "getDetailUsername",
                    username: username,
                }).then(response => {
                    this.isLoading = false;

                    this.detail_username = response.data[0];

                    // console.log(this.detail_username[0].kd_div);

                    if(this.detail_username[0].kd_div == 'HR') {
                        console.log(0);
                        this.access = true;
                        this.non_access = false;
                    } else {
                        if(this.detail_username[0].att_name == 'RACHMADI' || this.detail_username[0].att_name == 'ROMY' || this.detail_username[0].att_name == 'JULI') {
                            console.log(1);
                            this.access = true;
                            this.non_access = false;
                        } else {
                            console.log(2);
                            this.access = false;
                            this.non_access = true;
                        }
                    }

                }).catch(error => {
                    console.log(error);
                    this.isLoading = false;
                });
            }
        },
        mounted: function () {
            this.getDataKaryawanByKodePosition();
        },
        beforeDestroy () {
            if (this.timer !== void 0) {
                clearTimeout(this.timer)
                this.$q.loading.hide()
            }
        },
        created() {
            this.checkAccess();
            this.getEmployee();
            this.getOfficeDept();
        }
    });
    document.title = "<? echo $menu_desc; ?>";
</script>


<?  $dbh = null;

else : 
header('location:..');
endif; 
?>