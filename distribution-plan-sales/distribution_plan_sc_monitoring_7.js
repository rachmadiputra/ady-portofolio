const EventBus = new Vue();

Vue.use(window.vuelidate.default);
const { required } = window.validators;

// Vue.use(VueToast, {position: 'top'});
Vue.use(Toasted, {
	duration: 3000,
	iconPack: 'custom-class',
	theme: 'toasted-primary',
	action: {
		text: 'OK',
		onClick: (e, toastObject) => {
			toastObject.goAway(0);
		}
	},
});

Vue.mixin({
	data() {
		return {
			action: '../../sales/service/distribution_plan_sc_monitoring_dt_7.php',
			doPage: '../../inventory/invtranout_dist_bak',
			doPage2: '../../inventory/invtranout_bak',
			actionFeeSales: '../../feesales/actionSalesMemoDp.php',
			// actionFeeSales: '../../feesales/new_actionFeeSales.php',
		}
	}
});

Vue.component('daftar_so_component', {

	template: `
	<div>
		<div class="row">
			<div class="col-md-12">
				<div class="lds-facebook" v-if="loading"><div></div><div></div><div></div></div>
				<tabs_distribution_plan_component>
					<tab_distribution_plan_component v-for="(dt,i) in tabs" :key="i" :name="dt.name" :icon="dt.icon" :selected="i === 0 ? 'true' : ''">
						<div v-if="i == 0">
							<daf_so_kirim_comp :data_so="listSO_kirim" :data_sm="listSM_kirim" :username="username" :loc="loc"></daf_so_kirim_comp>
						</div>
						<div v-if="i == 2">
							<daf_so_rektur_component :data="listSO_rektur" :username="username" :loc="loc"></daf_so_rektur_component>
						</div>
					</tab_distribution_plan_component>
				</tabs_distribution_plan_component>
			</div>
		</div>
	</div>
	`,
	components: {
		vuejsDatepicker
	},
	data() {
		return {
			loading: false,
			tabs: [
				{
					name: 'KIRIM',
					icon: '<i class="fa fa-print"></i>',
					counter: 0,
				},
				{
					name: 'MONITORING',
					icon: '<i class="fa fa-print"></i>',
					counter: 1,
				},
				{
					name: 'RETUR',
					icon: '<i class="fa fa-print"></i>',
					counter: 2,
				},
			],
			listSO_kirim: [],
			listSM_kirim: [],
			listSO_kirim_pengalihan: [],
			listSO_ambil: [],
			listSO_ambil_pengalihan: [],
			listSO_rektur: [],
			listOfficeCode: [],
			v_tgl_perintah_kirim: '',
			officeCode: [],
			checkedSO: [],
			v_office: '',
			confirmPrice_kirim: [],
			confirmPrice_ambil: [],
			pilih_harga_kirim: '',
			pilih_harga_ambil: '',
			arr_price_kirim: [{
				data_so: [],
				price: []
			}],
			new_arr_price_kirim: [{
				data_so: []
			}],
			new_arr_price_ambil: [{
				data_so: []
			}],
			arr_price_ambil: [{
				data_so: [],
				price: []
			}],
			val_price: [],
			v_search_so: "",
			listSO_kirim_less: [],
			listSO_kirim_more: [],
			listSO_kirim_sna: [],
			totalMuatan: 0,
			totalHarga: 0
		}
	},
	props: ["username", "where", "loc"],
	methods: {
		getDataSOKirim() {
			this.loading = true;

			axios.post(this.action, {
				case: 'getDataSOKirim',
				username: this.username,
				where: this.where
			}).then(response => {
				this.loading = false;
				if (response.data != null) {
					this.listSO_kirim = response.data;
				} else {
					this.listSO_kirim = [];
				}
			});
		},
		getDataSMKirim() {
			// this.loading = true;

			axios.post(this.action, {
				case: 'getDataSMKirim',
				username: this.username,
				where: this.where
			}).then(response => {
				if (response.data != null) {
					this.listSM_kirim = response.data;
					console.log(this.listSM_kirim);
				} else {
					this.listSM_kirim = [];
				}
			});
		},
		getDataSOKirimNew() {
			this.loading = true;

			axios.post(this.action, {
				case: 'getDataSOKirimNew',
				username: this.username,
				where: this.where
			}).then(response => {
				this.loading = false;
				this.listSO_kirim_less = response.data.less;
				this.listSO_kirim_more = response.data.more;
				console.log(this.listSO_kirim_less)
				console.log(this.listSO_kirim_more)
			});
		},
		getDataSOKirimSna() {
			this.loading = true;

			axios.post(this.action, {
				case: 'getDataSOKirimSna',
				username: this.username,
				where: this.where
			}).then(response => {
				this.loading = false;
				this.listSO_kirim_sna = response.data;
				console.log(this.listSO_kirim_sna);
			});
		},
		getDataSOAmbil() {
			this.new_arr_price_ambil[0].data_so = [];
			this.confirmPrice_ambil = [];

			axios.post(this.action, {
				case: 'getDataSOAmbil',
				username: this.username,
				where: this.where
			}).then(response => {
				if (response.data != null) {
					this.listSO_ambil = response.data;
				} else {
					this.listSO_ambil = [];
				}
			});
		},
		getDataSORektur() {
			axios.post(this.action, {
				case: 'getDataSORektur',
				username: this.username,
				where: this.where,
				pin2: this.pin2
			}).then(response => {
				if (response.data != null) {
					this.listSO_rektur = response.data[0];
				} else {
					this.listSO_rektur = [];
				}
			});
		},
		add_SO() {
			axios.post(this.action, {
				case: 'saveDistributionPlan',
				tgl_perintah: this.v_tgl_perintah,
				username: this.username,
				dt_so: this.checkedSO,
			}).then(response => {
				console.log(response.data);
			});
		},
		simpanHargaKirim() {
			for (let i = 0; i < this.confirmPrice_kirim.length; i++) {
				this.new_arr_price_kirim[0].data_so.push(this.confirmPrice_kirim[i].data_so);
			}

			axios.post(this.action, {
				case: 'simpanDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
				dt_price: this.new_arr_price_kirim,
				totalMuatan: this.totalMuatan,
			}).then(response => {
				this.new_arr_price_kirim[0].data_so = [];
				this.checkedSO = [];

				let rspn = response.data;
				if (rspn.success === 1) {
					this.getDataSOKirimNew();
					// this.getDataSOAmbil();
					// console.log(response.data);
					this.$toasted.success(rspn.message, { duration: 5000 });
				} else {
					this.getDataSOKirimNew();
					// this.getDataSOAmbil();
					// console.log(response.data);
					this.$toasted.error(rspn.message, { duration: 5000 });
				}
			});
		},
		simpanHargaKirim2() {
			for (let i = 0; i < this.confirmPrice_kirim.length; i++) {
				this.new_arr_price_kirim[0].data_so.push(this.confirmPrice_kirim[i].data_so);
			}

			axios.post(this.action, {
				case: 'simpanDistributionPlanKirimLess',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
				dt_price: this.new_arr_price_kirim,
				totalMuatan: this.totalMuatan,
				totalharga: this.totalHarga,
			}).then(response => {
				this.new_arr_price_kirim[0].data_so = [];
				this.checkedSO = [];

				let rspn = response.data;
				if (rspn.success === 1) {
					this.getDataSOKirimNew();
					// this.getDataSOKirim();
					// this.getDataSOAmbil();
					// console.log(response.data);
					this.$toasted.success(rspn.message, { duration: 5000 });
				} else {
					this.getDataSOKirimNew();
					// this.getDataSOAmbil();
					console.log(response.data);
					this.$toasted.error(rspn.message, { duration: 5000 });
				}
			});
		},
		simpanHargaKirimPengalihan() {
			axios.post(this.action, {
				case: 'simpanDistributionPlanKirimPengalihan',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO
			}).then(response => {
				// location.reload();

				let rspn = response.data;
				if (rspn.success === 1) {
					this.getDataSOKirim();
					this.getDataSOAmbil();
					console.log(response.data);
					this.$toasted.success(rspn.message, { duration: 5000 });
				} else {
					this.getDataSOKirim();
					this.getDataSOAmbil();
					console.log(response.data);
					this.$toasted.error(rspn.message, { duration: 5000 });
				}
			});
		},
		simpanHargaAmbil() {
			// if (this.val_price == 'price_kirim_so') {
			// 	for (let i = 0; i < this.confirmPrice_ambil.length; i++) {
			// 		this.arr_price_ambil[0].data_so.push(this.confirmPrice_ambil[i].data_so);
			// 		this.arr_price_ambil[0].price.push(this.confirmPrice_ambil[i].price_so);
			// 	}
			// } else {
			// 	for (let i = 0; i < this.confirmPrice_ambil.length; i++) {
			// 		this.arr_price_ambil[0].data_so.push(this.confirmPrice_ambil[i].data_so);
			// 		this.arr_price_ambil[0].price.push(this.confirmPrice_ambil[i].price_sales);
			// 	}
			// }

			for (let i = 0; i < this.confirmPrice_ambil.length; i++) {
				this.new_arr_price_ambil[0].data_so.push(this.confirmPrice_ambil[i].data_so);
			}
			// console.log(this.new_arr_price_ambil);

			axios.post(this.action, {
				case: 'simpanDistributionPlanAmbil',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
				dt_price: this.new_arr_price_ambil
			}).then(response => {
				// location.reload();

				this.new_arr_price_ambil[0].data_so = [];
				// this.new_arr_price_ambil[0].price = [];
				this.checkedSO = [];

				let rspn = response.data;
				if (rspn.success === 1) {
					this.getDataSOKirim();
					this.getDataSOAmbil();
					console.log(response.data);
					this.$toasted.success(rspn.message, { duration: 5000 });
				} else {
					this.getDataSOKirim();
					this.getDataSOAmbil();
					console.log(response.data);
					this.$toasted.error(rspn.message, { duration: 5000 });
				}
			});
		},
		simpanHargaAmbilPengalihan() {
			axios.post(this.action, {
				case: 'simpanDistributionPlanAmbilPengalihan',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO
			}).then(response => {
				// location.reload();

				let rspn = response.data;
				if (rspn.success === 1) {
					this.getDataSOKirim();
					this.getDataSOAmbil();
					console.log(response.data);
					this.$toasted.success(rspn.message, { duration: 5000 });
				} else {
					this.getDataSOKirim();
					this.getDataSOAmbil();
					console.log(response.data);
					this.$toasted.error(rspn.message, { duration: 5000 });
				}
			});
		}
	},
	created() {
		this.getDataSOKirim();
		this.getDataSMKirim();
		// this.getDataSOKirimNew();
		// this.getDataSOAmbil();
		// this.getDataSORektur();
		// this.getDataSOKirimSna();

		EventBus.$on('refresh_so_sc', () => {
			this.getDataSOKirim();
			this.getDataSMKirim();
			// this.getDataSOKirimNew();
		});
		EventBus.$on('addItemSO', (data) => {
			this.checkedSO = data;
		});

		EventBus.$on('confirmPrice_kirim', (price_data, tgl_perintah, username, v_office, checkedSO, totalMuatan) => {

			this.confirmPrice_kirim = price_data;
			this.v_tgl_perintah_kirim = tgl_perintah;
			this.username = username;
			this.v_office = v_office;
			this.checkedSO = checkedSO;
			this.totalMuatan = totalMuatan;
			console.log(totalMuatan);


			// $('#modal_confirmPrice_kirim').modal('show');
			this.simpanHargaKirim();
		});

		EventBus.$on('confirmPrice_kirim_less', (price_data, tgl_perintah, username, v_office, checkedSO, totalMuatan, totalHarga) => {

			this.confirmPrice_kirim = price_data;
			this.v_tgl_perintah_kirim = tgl_perintah;
			this.username = username;
			this.v_office = v_office;
			this.checkedSO = checkedSO;
			this.totalMuatan = totalMuatan;
			this.totalHarga = totalHarga;

			// $('#modal_confirmPrice_kirim').modal('show');
			// this.simpanHargaKirim2();
		});

		EventBus.$on('confirmPrice_kirim_pengalihan', (price_data, tgl_perintah, username, v_office, checkedSO) => {

			this.confirmPrice_kirim = price_data;
			this.v_tgl_perintah_kirim = tgl_perintah;
			this.username = username;
			this.v_office = v_office;
			this.checkedSO = checkedSO;

			$('#modal_confirmPrice_kirim_pengalihan').modal('show');
		});

		EventBus.$on('confirmPrice_ambil', (price_data, tgl_perintah, username, v_office, checkedSO) => {

			this.confirmPrice_ambil = price_data;
			this.v_tgl_perintah_kirim = tgl_perintah;
			this.username = username;
			this.v_office = v_office;
			this.checkedSO = checkedSO;

			// $('#modal_confirmPrice_ambil').modal('show');
			this.simpanHargaAmbil();
		});

		EventBus.$on('confirmPrice_ambil_pengalihan', (price_data, tgl_perintah, username, v_office, checkedSO) => {

			this.confirmPrice_ambil = price_data;
			this.v_tgl_perintah_kirim = tgl_perintah;
			this.username = username;
			this.v_office = v_office;
			this.checkedSO = checkedSO;

			$('#modal_confirmPrice_ambil_pengalihan').modal('show');
		});

		EventBus.$on('refreshData', () => {
			this.getDataSOKirimNew();
			// this.getDataSOAmbil();
			// this.getDataSORektur();
			this.getDataSOKirimSna();
		});
	},
	watch: {
		pilih_harga_kirim(value) {
			this.val_price = value;
		}
	}

});

Vue.component('daf_so_kirim_comp', {


	props: ['data_so', 'data_sm', 'username', 'loc'],
	template: `
	<div>		
		<div class="row">
			<div class="col-md-12">
				<div class="lds-facebook" v-if="loading"><div></div><div></div><div></div></div>
				<tabs_distribution_plan_component>
					<tab_distribution_plan_component v-for="(dt,i) in tabs" :key="i" :name="dt.name" :icon="dt.icon" :selected="i === 0 ? 'true' : ''">
						<div v-if="i == 0">
							
						</div>
						<div v-if="i == 1">
							<daf_so_kirim_comp_sm :data="data_sm" :username="username" :loc="loc"></daf_so_kirim_comp_sm>
						</div>
					</tab_distribution_plan_component>
				</tabs_distribution_plan_component>
			</div>
		</div>
	</div>
	`,
	data() {
		return {
			loading: false,
			tabs: [
				{
					name: 'SO',
					icon: '<i class="fa fa-print"></i>',
					counter: 0,
				},
				{
					name: 'SM',
					icon: '<i class="fa fa-print"></i>',
					counter: 1,
				},
			],
		}
	},
	created() {

	}

});

Vue.component('daf_so_kirim_comp_so', {

	template: `
	<div>
		<div class="row">
			<div class="col-sm-10">
				<button type="button" class="btn btn-xm btn-success" @click="getDataSOKirim"><i class="fa fa-refresh"></i></button>
			</div>
			<div class="col-sm-2">
				<label>Cari</label>
				<input type="text" class="form-control" placeholder="No. SO" v-model="v_search_so">
			</div>
		</div>
		</br>
		<div class="row">
			<div class="lds-facebook" v-if="loading"><div></div><div></div><div></div></div>
			<div class="col-md-12" style="text-transform: capitalize;">
				<table class="table table-hover table-responsive table-striped" style="font-size: 12px;">
					<thead>
						<tr>
							<th class="active">Comp</th>
							<th class="active">Tipe</th>
							<th class="active">Ledger</th>
							<th class="active">Customer</th>
							<th v-for="column in new_columns" @click="activeColumn = column" class="active">
								{{ colTitles[column.name] }} 
								<span @click="column.order = column.order * (-1), sortBy(column.name)" class="arrow" :class="column.order > 0 ? 'asc' : 'dsc'" ></span>
							</th>
							<th class="active">Alamat</th>
							<th class="active">Keterangan</th>
							<th class="active">Sales</th>
							<th class="active">#</th>
						</tr>
					</thead>
					<tbody>
						<tr v-for="(dt,i) in filterListST" :key="i"  v-if="i >= pagestart && i < pageend" style="text-transform: uppercase;"
							:style="color(dt.tipe)">
							<td>{{ dt.code_comp }}</td>
							<td>{{ dt.tipe }}</td>
							<td>{{ dt.code }}</td>
							<td style="width: 10%;">{{ dt.ledgername }}</td>
							<td style="width: 10%;">{{ dt.tanggal }} {{ dt.jaminput }}</td>
							<td style="width: 10%;">{{ dt.delivery_time }}</td>
							<td>{{ dt.so_no }}</td>
							<td style="width: 13%;">{{ dt.item_code }}</td>
							<td>{{ dt.alamat_toko }}</td>
							<td>{{ dt.keterangan }}</td>
							<td>{{ dt.sales_code }}</td>
							<td><button type="button" class="btn btn-xs btn-info" @click="buatSM(dt)">BUAT SM</button></td>
						</tr>
					</tbody>
				</table>
			</div>
			</br></br></br>
			<div class="col-md-12" style="text-transform: capitalize; text-align: center;">
			<paginate 
				:pagecount="totalpages"
				:containerclass="'pagination pagination-sm'"
				:clickhandler="pagination"
				:prevtext="prevButtonIcon"
				:nexttext="nextButtonIcon">
			</paginate>
			</div>
		</div>
		<div class="modal fade bd-example-modal-lg" id="modal_buat_sm" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<div class="row">
							<div class="col-sm-11">
								<h3 class="modal-title">BUAT SM</h3>
							</div>
							<div class="col-sm-1" style="text-align: end;">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
						</div>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-3" style="text-align: start;">
								<label class="control-label text-center">Tanggal Order</label>
							</div>
							<div class="col-md-3" style="text-align: start;">
								<label class="control-label text-center">Nomor</label>
							</div>
							<!-- <div class="col-md-3" style="text-align: start;">
								<label class="control-label text-center">Tanggal Kirim</label>
							</div> -->
							<div class="col-md-3" style="text-align: start;">
								<label class="control-label text-center">NIK</label>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<vuejs-datepicker
									v-model="v_tgl_order"
									:format="DatePickerFormatb"
									:bootstrap-styling="true"
									:disabledDates="disabledDates"
									:placeholder="holderb"
									@input="getNomor()"
									required
									style="width: 290px;">
								</vuejs-datepicker>
							</div>
							<div class="col-md-3">
								<input type="text" class="form-control" v-model="nomorSM" disabled>
							</div>
							<!-- <div class="col-md-3">
								<vuejs-datepicker
									v-model="v_tgl_perintah_kirim"
									:format="DatePickerFormatb"
									:disabledDates="disabledDates"
									:bootstrap-styling="true"
									:placeholder="holderb"
									required
									style="width: 290px;">
								</vuejs-datepicker>
							</div> -->
							<div class="col-md-3">
								<input type="text" class="form-control" v-model="nik" disabled>
							</div>
						</div>
						</br>
						<div class="row">
							<div class="col-md-12" style="text-transform: capitalize;">
								<table class="table table-hover table-responsive table-striped">
									<thead>
										<tr>
											<th class="active">NO. LEDGER</th>
											<th class="active">NAMA TOKO</th>
											<th class="active">NO. SM</th>
											<th class="active">NO. SO</th>
											<th class="active">ITEM</th>
											<th class="active">TAMBAH/KURANG QTY</th>
										</tr>
									</thead>
									<tbody v-for="(dt,i) in pilihSO">
										<tr v-for="(dta,i) in dt.detail">
											<td>{{ dta.code }}</td>
											<td>{{ dta.name }}</td>
											<td v-if="dta.type === 'SM'">{{ dta.no }}</td>
											<td v-else></td>
											<td>{{ dta.so_no }}</td>
											<td>{{ dta.item_code }}</td>
											<td>
												<input type="number" class="form-control" v-model="dta.item_qty2">
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3" style="text-align: start;">
								<label class="control-label text-center">Keterangan</label>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<input type="text" class="form-control" v-model="keterangan">
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger btn-sm" @click="closeModalSM()">Batal</button>
						<button type="button" class="btn btn-primary btn-sm" @click="saveSM" :disabled="isActive">Simpan</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	`,
	components: {
		vuejsDatepicker
	},
	props: {
		username: {
			required: true
		},
		loc: {
			required: true
		},
		where: {
			required: true
		},
		acc_delete: {
			required: true
		},
	},
	data() {
		return {
			loading: false,
			v_tgl_perintah_kirim: '',
			officeCode: [],
			current_kdOff: [],
			checkedSO: [],
			pilihSO: [],
			v_office: this.loc,
			v_office_current: this.loc,
			columns: ['type', 'code', 'ledgername', 'tanggal', 'sm_no', 'so_no',
				'item_code', 'alamat_toko', 'keterangan', 'jaminput', 'code_comp', 'sales_code', 'status_so', 'dept_code', 'pilih'],
			pagestart: 0,
			pagecurrent: 1,
			itemsperpage: 100,
			pageend: 100,
			prevButtonIcon: '<i class="fa fa-chevron-left"></i>',
			nextButtonIcon: '<i class="fa fa-chevron-right"></i>',
			currentSort: 'name',
			currentSortDir: 'asc',
			activeColumn: {},
			new_columns: [
				{ name: 'tanggal', order: 1 },
				{ name: 'delivery', order: 1 },
				{ name: 'so_no', order: 1 },
				{ name: 'item_code', order: 1 },
			],
			colTitles: {
				'tanggal': 'Tgl. SO',
				'delivery': 'Tgl. Kirim',
				'sm_no': 'No. SM',
				'so_no': 'No. SO',
				'item_code': 'Item & QTY',
			},
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 8640000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
			v_search_so: '',
			styleObject: {
				background: 'none',
			},
			totalMuatan: Math.round(0),
			listSO_kirim: [],
			v_tgl_order: '',
			nomorSM: '',
			orderNo: '',
			keterangan: '',
			items: [{
				itemCode: '',
				itemName: '',
				itemQty: '',
				itemUnit: ''
			}],
			checkedSM: [],
			confirmPrice_kirim: [],
			new_arr_price_kirim: [{
				data_so: []
			}],
			isActive: true,
			nik: ''
		}
	},
	methods: {
		closeModalSM() {
			$("#modal_buat_sm").modal("hide");
		},
		getDataSOKirim() {
			this.checkedSM = [];
			this.checkedSO = [];
			this.pilihSO = [];
			this.loading = true;

			axios.post(this.action, {
				case: 'getDataSOKirim',
				username: this.username,
				where: this.where
			}).then(response => {
				this.loading = false;
				if (response.data != null) {
					this.listSO_kirim = response.data;
				} else {
					this.listSO_kirim = [];
				}
			});
		},
		getDataSMKirimByNo(sm_no) {
			axios.post(this.action, {
				case: 'getDataSMKirimByNo',
				username: this.username,
				where: this.where,
				sm_no: sm_no,
			}).then(response => {
				this.checkedSM = response.data;
				// console.log(this.checkedSM);

				// const result = this.checkedSM.filter(rslt => rslt.no === sm_no);
				// console.log(result)

				// if (result.ton >= 10) {
				// 	alert('Tekan OK utk melanjutkan');
				// }

				if (this.checkedSM[0].ton >= 10) {

					axios.post(this.action, {
						case: 'saveDistributionPlanKirim',
						tgl_perintah: this.v_tgl_order,
						username: this.username,
						office: this.v_office,
						dt_so: this.checkedSM,
					}).then(response => {
						// console.log(response.data);
						EventBus.$emit('confirmPrice_kirim_less_new', response.data, this.v_tgl_order, this.username, this.v_office, this.checkedSM, this.checkedSM[0].ton);
						this.checkedSM = [];
						this.checkedSO = [];
						this.pilihSO = [];
					});

				} else {
					this.$toasted.success('Data Berhasil Di Simpan', { duration: 5000 });
				}
			});
		},
		getOfficeCode() {
			axios.post(this.action, {
				case: 'getOfficeCode',
				username: 'rachmadi'
			}).then(response => {
				this.officeCode = response.data.dt_kd_offifce[0];
			});
		},
		getNomor() {
			axios.post(this.action, {
				case: 'getLastNoSM',
				username: this.username,
				shift: this.pilihSO[0].code_comp.toLowerCase(),
				typeForm: 'titipan',
				orderDate: this.v_tgl_order,
				salesCode: 'OFFICE',
				coceCode: this.pilihSO[0].cabang
			}).then(response => {
				console.log(response.data);
				this.nomorSM = response.data.noText;
				this.orderNo = response.data.lastNo;
			});
		},
		buatSM(dt) {
			this.isActive = false;
			this.pilihSO = [];
			this.pilihSO.push(dt);

			this.nik = '';

			this.getNomor();
			this.nik = this.pilihSO[0].idno1;

			console.log(this.pilihSO);
			console.log(this.nik);

			$("#modal_buat_sm").modal("show");
		},
		simpanHargaKirim_new() {
			this.new_arr_price_kirim[0].data_so = [];

			for (let i = 0; i < this.confirmPrice_kirim.length; i++) {
				this.new_arr_price_kirim[0].data_so.push(this.confirmPrice_kirim[i].data_so);
			}

			axios.post(this.action, {
				case: 'simpanDistributionPlanKirimLess',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
				dt_price: this.new_arr_price_kirim,
				totalMuatan: this.totalMuatan,
				totalharga: this.totalHarga,
			}).then(response => {
				this.new_arr_price_kirim[0].data_so = [];
				this.checkedSO = [];
				this.confirmPrice_kirim = [];

				let rspn = response.data;
				if (rspn.success === 1) {
					console.log(response.data);
					this.$toasted.success(rspn.message, { duration: 5000 });
				} else {
					console.log(response.data);
					this.$toasted.error(rspn.message, { duration: 5000 });
				}
			});
		},
		saveSM() {
			$("#modal_buat_sm").modal("hide");

			this.items = [];

			for (let i = 0; i < this.pilihSO[0].detail.length; i++) {
				this.items.push({
					'itemCode': this.pilihSO[0].detail[i].item_code.trim(),
					'itemName': this.pilihSO[0].detail[i].item_name,
					'itemQty': this.pilihSO[0].detail[i].item_qty2,
					'itemPrice': parseInt(this.pilihSO[0].detail[i].price_amt),
				});
			}

			axios.post(this.actionFeeSales, {
				case: 'saveData',
				cabang: this.pilihSO[0].cabang,
				shift: this.pilihSO[0].code_comp.toLowerCase(),
				orderDate: this.v_tgl_order,
				salesFormNo: this.nomorSM,
				lastNo: this.orderNo,
				orderStat: 'I',
				ledgerCode: this.pilihSO[0].code,
				ledgerName: this.pilihSO[0].ledgerName,
				keterangan: this.keterangan,
				insertUser: this.username,
				insertDate: '',
				updateUser: '',
				updateDate: '',
				longitude: '',
				latitude: '',
				longitudeBrowser: '',
				latitudeBrowser: '',
				items: this.items,
				eventCode: 'SAVE',
				accDelete: this.acc_delete,
				top: this.pilihSO[0].term,
				pemberiOrder: 'konsumen',
				typeForm: 'titipan',
				soNo: this.pilihSO[0].so_no,
				menuCode: 'salesfrm',
				type: '',
				nikNpwp: this.pilihSO[0].idno1,
				kp_code: this.pilihSO[0].kp_code,
				freightType: 'kirim',
				menu_code: 'salesfrm',
			}).then(response => {
				console.log(response.data);
				this.isActive = true;
				this.getDataSMKirimByNo(this.nomorSM);
				this.getDataSOKirim();
			});
		},
		pagination(index) {
			if (index >= 0 && index <= this.totalpages) {
				this.pagecurrent = index;
				this.pagestart = (this.itemsperpage * index) - this.itemsperpage;
				this.pageend = (this.itemsperpage * index);
			}
		},
		sortBy(column) {
			if (column === this.currentSort) {
				this.currentSortDir = this.currentSortDir === 'asc' ? 'desc' : 'asc';
			}
			this.currentSort = column;
		},
		diffDay(dateSo) {
			var dateNow = new Date().getTime();
			var dateSo = new Date(dateSo).getTime();
			var diffDay = parseInt((dateNow - dateSo) / (24 * 3600 * 1000));
			return diffDay;
		},
		getNow() {
			const today = new Date();
			const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
			const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			const dateTime = date + ' ' + time;
			this.v_tgl_perintah_kirim = dateTime;
			this.v_tgl_order = dateTime;
		},
		send_mail_approve(data) {
			// console.log(data);
			this.loading = true;
			axios.post(this.action, {
				case: 'send_mail_approve',
				dt_so: data,
			}).then(response => {
				console.log(response.data);
				this.loading = false;
				this.$toasted.success(response.data, { duration: 5000 });
				// console.log("satu cabang");
				this.checkedSO = [];
			});
		},
		color(tgl) {
			var dateNow = new Date().getTime();
			var dateSo = new Date(tgl).getTime();
			var diffDay = parseInt((dateNow - dateSo) / (24 * 3600 * 1000));

			// if (diffDay >= 3 && diffDay < 5) {
			// 	return 'color: orange';
			// } else if (diffDay >= 5) {
			// 	return 'color: red';
			// } else {
			// 	return 'color: black';
			// }

			if (tgl == 'L') {
				return 'color: black';
			} else {
				return 'color: red';
			}
		},
	},
	created() {
		this.getOfficeCode();
		this.getDataSOKirim();
		this.getNow();
		EventBus.$on('resetCheckBox', () => {
			this.checkedSO = [];
		});
		EventBus.$on('refreshModal', () => {
			// alert('hi');
			EventBus.$emit('refreshSNA');
		});
		EventBus.$on('confirmPrice_kirim_less_new', (price_data, tgl_perintah, username, v_office, checkedSO, totalMuatan, totalHarga) => {

			this.confirmPrice_kirim = price_data;
			this.v_tgl_perintah_kirim = tgl_perintah;
			this.username = username;
			this.v_office = v_office;
			this.checkedSO = checkedSO;
			this.totalMuatan = totalMuatan;
			this.totalHarga = totalHarga;

			this.simpanHargaKirim_new();
		});
	},
	computed: {
		filterListST() {
			let filterST = this.v_search_so.toUpperCase();

			let result = this.listSO_kirim.filter(function (e) {
				let filtered = true
				if (filterST && filterST.length > 0) {
					filtered = e.so_no.includes(filterST)
				}
				return filtered;

			}).sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});

			if (result.length == 0) {
				let res = this.listSO_kirim.filter(function (e) {
					let filtered = true
					if (filterST && filterST.length > 0) {
						filtered = e.ledgername.includes(filterST)
					}
					return filtered;

				}).sort((a, b) => {
					let modifier = 1;
					if (this.currentSortDir === 'desc') modifier = -1;
					if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
					if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
					return 0;
				});
				return res;
			} else {
				return result;
			}
		},
		totalpages() {
			//This calculates the amount of pages
			return Math.ceil(this.filterListST.length / this.itemsperpage);
		},
		sortedData: function () {
			return this.listSO_kirim.sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});
		},
		muatan() {
			let total = 0;
			this.checkedSO.forEach(so => {
				// console.log(so.ton);
				total += so.ton;
			});
			this.totalMuatan = Math.round(total);
			return total;
		}
	},
	watch: {
		data: function () {
			this.pagecurrent = 1;
			this.pagestart = 0;
			this.pageend = this.itemsperpage;
		},
	}

});

Vue.component('daftar_sm_component', {

	template: `
	<div>
		<div class="row">
			<div class="col-md-12">
				<div class="lds-facebook" v-if="loading"><div></div><div></div><div></div></div>
				<tabs_distribution_plan_component>
					<tab_distribution_plan_component v-for="(dt,i) in tabs" :key="i" :name="dt.name" :icon="dt.icon" :selected="i === 0 ? 'true' : ''">
						<!-- <div v-if="i == 0">
							
						</div> -->
						<!-- <div v-if="i == 0">
							
						</div> -->
					</tab_distribution_plan_component>
				</tabs_distribution_plan_component>
			</div>
		</div>
	</div>
	`,
	props: {
		username: {
			required: true
		},
		loc: {
			required: true
		},
		where: {
			required: true
		},
	},
	data() {
		return {
			loading: false,
			tabs: [
				// {
				// 	name: 'SM (10 TON)',
				// 	icon: '<i class="fa fa-print"></i>',
				// 	counter: 0,
				// },
				{
					name: 'SM',
					icon: '<i class="fa fa-print"></i>',
					counter: 0,
				},
			],
			listSM_kirim: [],
			listSM_kirim_less: [],
			v_tgl_perintah_kirim: '',
			officeCode: [],
			current_kdOff: [],
			checkedSO: [],
			v_office: this.loc,
			v_office_current: this.loc,
			columns: ['type', 'code', 'ledgername', 'tanggal', 'sm_no', 'so_no',
				'item_code', 'alamat_toko', 'keterangan', 'jaminput', 'code_comp', 'sales_code', 'status_so', 'dept_code', 'pilih'],
			pagestart: 0,
			pagecurrent: 1,
			itemsperpage: 100,
			pageend: 100,
			prevButtonIcon: '<i class="fa fa-chevron-left"></i>',
			nextButtonIcon: '<i class="fa fa-chevron-right"></i>',
			currentSort: 'name',
			currentSortDir: 'asc',
			activeColumn: {},
			new_columns: [
				{ name: 'tanggal', order: 1 },
				{ name: 'delivery', order: 1 },
				{ name: 'sm_no', order: 1 },
				{ name: 'so_no', order: 1 },
				{ name: 'item_code', order: 1 },
			],
			colTitles: {
				'tanggal': 'Tgl. SO / SM',
				'delivery': 'Tgl. Kirim',
				'sm_no': 'No. SM',
				'so_no': 'No. SO',
				'item_code': 'Item & QTY',
			},
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 8640000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
			v_search_so: '',
			styleObject: {
				background: 'none',
			},
			totalMuatan: Math.round(0),
			total_ton: 0,
			confirmPrice_kirim: [],
			new_arr_price_kirim: [{
				data_so: []
			}],
		}
	},
	methods: {
		getDataSMKirim() {
			this.loading = true;

			axios.post(this.action, {
				case: 'getDataSMKirim',
				username: this.username,
				where: this.where
			}).then(response => {
				if (response.data != null) {
					this.loading = false;
					this.listSM_kirim = response.data;
					this.listSM_kirim_less = response.data;
					console.log(response.data)
				} else {
					this.listSM_kirim = [];
				}
			});
		},
	},
	created() {
		this.getDataSMKirim();
	}
});

// Vue.component('daf_so_kirim_comp_sm_more', {

// 	template: `
// 	<div>
// 		<!-- <div class="row">
// 			<div class="col-md-2" style="text-align: start;">
// 				<label class="control-label text-center">Lokasi Ambil</label>
// 			</div>
// 			<div class="col-md-2" style="text-align: start;">
// 				<label class="control-label text-center">Tanggal Kirim</label>
// 			</div>
// 		</div> -->
// 		<!-- <div class="row">
// 			<div class="col-md-2">
// 				<v-select style="text-transform: capitalize" label="kd_off" :options="officeCode" v-model="v_office" :reduce="officeCode=>officeCode.kd_off"></v-select>
// 			</div>
// 			<div class="col-md-2">
// 				<vuejs-datepicker
// 					v-model="v_tgl_perintah_kirim"
// 					:format="DatePickerFormatb"
// 					:disabledDates="disabledDates"
// 					:bootstrap-styling="true"
// 					:placeholder="holderb"
// 					required
// 					style="width: 290px;">
// 				</vuejs-datepicker>
// 			</div>
// 			<div class="col-md-1">
// 				<button type="button" class="btn btn-xm btn-primary" @click="add_SO" :disabled="totalMuatan < 0">Simpan</button>
// 			</div>
// 			<div class="col-md-1">
// 				<button type="button" class="btn btn-xm btn-success" @click="load"><i class="fa fa-refresh"></i></button>
// 			</div>
// 		</div> -->
// 		<div class="lds-facebook" v-if="loading"><div></div><div></div><div></div></div>
// 		<div class="row">
// 			<div class="col-md-2" style="text-align: start;">
// 				<label class="control-label text-center">Tanggal Kirim</label>
// 			</div>
// 		</div>
// 		<div class="row">
// 			<div class="col-md-2">
// 				<vuejs-datepicker
// 					v-model="v_tgl_perintah_kirim"
// 					:format="DatePickerFormatb"
// 					:disabledDates="disabledDates"
// 					:bootstrap-styling="true"
// 					:placeholder="holderb"
// 					required
// 					style="width: 290px;">
// 				</vuejs-datepicker>
// 			</div>
// 			<div class="col-sm-1" style="">
// 				<button type="button" class="btn btn-xm btn-primary" @click="add_SO" :disabled="totalMuatan < 0">Simpan</button>
// 			</div>
// 			<div class="col-sm-7" style="">
// 				<button type="button" class="btn btn-xm btn-success" @click="load"><i class="fa fa-refresh"></i></button>
// 			</div>
// 			<div class="col-sm-2" style="">
// 				<label>Cari</label>
// 				<input type="text" class="form-control" placeholder="No. SO" v-model="v_search_so">
// 			</div>
// 		</div>
// 		</br>
// 		<div class="row">
// 			<div class="col-sm-2" style="text-align: left;">
// 				<h4>TOTAL MUATAN: {{ Math.round(muatan) }} TON</h4>
// 			</div>
// 		</div>
// 		<div class="row">
// 			<div class="col-md-12" style="text-transform: capitalize;">
// 				<table class="table table-hover table-responsive table-striped" style="font-size: 12px;">
// 					<thead>
// 						<tr>
// 							<th class="active">#</th>
// 							<th class="active">Comp</th>
// 							<th class="active">Ledger</th>
// 							<th class="active">Customer</th>
// 							<th v-for="column in new_columns" @click="activeColumn = column" class="active">
// 								{{ colTitles[column.name] }} 
// 								<span @click="column.order = column.order * (-1), sortBy(column.name)" class="arrow" :class="column.order > 0 ? 'asc' : 'dsc'" ></span>
// 							</th>
// 							<th class="active">Alamat</th>
// 							<th class="active">Keterangan</th>
// 							<th class="active">Sales</th>
// 							<th class="active">Ton</th>
// 						</tr>
// 					</thead>
// 					<tbody>
// 						<tr v-for="(dt,i) in filterListST" :key="i"  v-if="i >= pagestart && i < pageend" style="text-transform: uppercase;"
// 							:style="color(dt.tanggal)">
// 							<td>
// 								<label class="form-checkbox">
// 									<input type="checkbox" :value="dt" v-model="checkedSO">
// 									<i class="form-icon"></i>
// 								</label>
// 							</td>
// 							<td>{{ dt.code_comp }}</td>
// 							<td>{{ dt.code }}</td>
// 							<td style="width: 10%;">{{ dt.ledgername }}</td>
// 							<td style="width: 10%;">{{ dt.tanggal }} {{ dt.jaminput }}</td>
// 							<td style="width: 10%;">{{ dt.delivery_time }}</td>
// 							<td style="width: 5%;">{{ dt.sm_no }}</td>
// 							<td>{{ dt.so_no }}</td>
// 							<td style="width: 13%;">{{ dt.item_code }}</td>
// 							<td>{{ dt.alamat_toko }}</td>
// 							<td>{{ dt.keterangan }}</td>
// 							<td>{{ dt.sales_code }}</td>
// 							<td>{{ dt.ton }}</td>
// 						</tr>
// 					</tbody>
// 				</table>
// 			</div>
// 			</br></br></br>
// 			<div class="col-md-12" style="text-transform: capitalize; text-align: center;">
// 			<paginate 
// 				:pagecount="totalpages"
// 				:containerclass="'pagination pagination-sm'"
// 				:clickhandler="pagination"
// 				:prevtext="prevButtonIcon"
// 				:nexttext="nextButtonIcon">
// 			</paginate>
// 			</div>
// 		</div>
// 	</div>
// 	`,
// 	components: {
// 		vuejsDatepicker
// 	},
// 	props: {
// 		username: {
// 			required: true
// 		},
// 		loc: {
// 			required: true
// 		},
// 		where: {
// 			required: true
// 		},
// 		listSM_kirim: {
// 			type: Array,
// 			required: true
// 		}
// 	},
// 	data() {
// 		return {
// 			loading: false,
// 			v_tgl_perintah_kirim: '',
// 			officeCode: [],
// 			current_kdOff: [],
// 			checkedSO: [],
// 			v_office: this.loc,
// 			v_office_current: this.loc,
// 			columns: ['type', 'code', 'ledgername', 'tanggal', 'sm_no', 'so_no',
// 				'item_code', 'alamat_toko', 'keterangan', 'jaminput', 'code_comp', 'sales_code', 'status_so', 'dept_code', 'pilih'],
// 			pagestart: 0,
// 			pagecurrent: 1,
// 			itemsperpage: 100,
// 			pageend: 100,
// 			prevButtonIcon: '<i class="fa fa-chevron-left"></i>',
// 			nextButtonIcon: '<i class="fa fa-chevron-right"></i>',
// 			currentSort: 'name',
// 			currentSortDir: 'asc',
// 			activeColumn: {},
// 			new_columns: [
// 				{ name: 'tanggal', order: 1 },
// 				{ name: 'delivery', order: 1 },
// 				{ name: 'sm_no', order: 1 },
// 				{ name: 'so_no', order: 1 },
// 				{ name: 'item_code', order: 1 },
// 			],
// 			colTitles: {
// 				'tanggal': 'Tgl. SO / SM',
// 				'delivery': 'Tgl. Kirim',
// 				'sm_no': 'No. SM',
// 				'so_no': 'No. SO',
// 				'item_code': 'Item & QTY',
// 			},
// 			holdera: "-- Pilih Bulan --",
// 			holderb: "-- Pilih Tanggal --",
// 			DatePickerFormata: 'MMMM yyyy',
// 			DatePickerFormatb: 'dd MMMM yyyy',
// 			disabledDates: {
// 				to: new Date(Date.now() - 8640000)
// 			},
// 			startmonth: '',
// 			enddate: '',
// 			minv: 'month',
// 			monthNames: [
// 				{ value: "1", text: "January" },
// 				{ value: "2", text: "February" },
// 				{ value: "3", text: "March" },
// 				{ value: "4", text: "April" },
// 				{ value: "5", text: "May" },
// 				{ value: "6", text: "June" },
// 				{ value: "7", text: "July" },
// 				{ value: "8", text: "August" },
// 				{ value: "9", text: "September" },
// 				{ value: "10", text: "October" },
// 				{ value: "11", text: "November" },
// 				{ value: "12", text: "December" }
// 			],
// 			v_search_so: '',
// 			styleObject: {
// 				background: 'none',
// 			},
// 			totalMuatan: Math.round(0),
// 			total_ton: 0,
// 			confirmPrice_kirim: [],
// 			new_arr_price_kirim: [{
// 				data_so: []
// 			}],
// 		}
// 	},
// 	methods: {
// 		closeModalSM() {
// 			$("#modal_buat_sm").modal("hide");
// 		},
// 		getOfficeCode() {
// 			axios.post(this.action, {
// 				case: 'getOfficeCode',
// 				username: 'rachmadi'
// 			}).then(response => {
// 				this.officeCode = response.data.dt_kd_offifce[0];
// 			});
// 		},
// 		add_SO() {
// 			if (this.totalMuatan >= 5 && this.totalMuatan < 10 && this.totalMuatan !== 0 || this.totalMuatan < 4) {
// 				let userdata = {
// 					listSo: this.checkedSO,
// 					totalMuatan: this.totalMuatan,
// 					loc: this.loc,
// 					username: this.username,
// 					tglPerintahKirim: this.v_tgl_perintah_kirim,
// 					vOffice: this.v_office
// 				};
// 				EventBus.$emit('modalBiayaTambahan', userdata);
// 			} else if (this.totalMuatan < 5 && this.totalMuatan >= 4) {
// 				this.saveSO();
// 			} else if (this.totalMuatan >= 10) {
// 				this.saveSO();
// 			} else {
// 				swal.fire({
// 					title: "Gagal Simpan SO",
// 					text: "Silahkan Pilih SO Terlebih Dahulu",
// 					icon: "error",
// 				}).then(function (isConfirm) {
// 					if (isConfirm) {
// 						console.log('gagal boss');
// 					}
// 				});
// 			}
// 		},
// 		saveSO() {
// 			axios.post(this.action, {
// 				case: 'saveDistributionPlanKirim',
// 				tgl_perintah: this.v_tgl_perintah_kirim,
// 				username: this.username,
// 				office: this.v_office,
// 				dt_so: this.checkedSO,
// 			}).then(response => {
// 				// console.log(response.data);
// 				// EventBus.$emit('confirmPrice_kirim_less', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO, this.totalMuatan);
// 				this.checkedSO = [];
// 			});
// 		},
// 		pagination(index) {
// 			if (index >= 0 && index <= this.totalpages) {
// 				this.pagecurrent = index;
// 				this.pagestart = (this.itemsperpage * index) - this.itemsperpage;
// 				this.pageend = (this.itemsperpage * index);
// 			}
// 		},
// 		sortBy(column) {
// 			if (column === this.currentSort) {
// 				this.currentSortDir = this.currentSortDir === 'asc' ? 'desc' : 'asc';
// 			}
// 			this.currentSort = column;
// 		},
// 		diffDay(dateSo) {
// 			var dateNow = new Date().getTime();
// 			var dateSo = new Date(dateSo).getTime();
// 			var diffDay = parseInt((dateNow - dateSo) / (24 * 3600 * 1000));
// 			return diffDay;
// 		},
// 		getNow() {
// 			const today = new Date();
// 			const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
// 			const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
// 			const dateTime = date + ' ' + time;
// 			this.v_tgl_perintah_kirim = dateTime;
// 		},
// 		send_mail_approve(data) {
// 			// console.log(data);
// 			this.loading = true;
// 			axios.post(this.action, {
// 				case: 'send_mail_approve',
// 				dt_so: data,
// 			}).then(response => {
// 				console.log(response.data);
// 				this.loading = false;
// 				this.$toasted.success(response.data, { duration: 5000 });
// 				// console.log("satu cabang");
// 				this.checkedSO = [];
// 			});
// 		},
// 		load() {

// 		},
// 		color(tgl) {
// 			var dateNow = new Date().getTime();
// 			var dateSo = new Date(tgl).getTime();
// 			var diffDay = parseInt((dateNow - dateSo) / (24 * 3600 * 1000));

// 			if (diffDay >= 3 && diffDay < 5) {
// 				return 'color: orange';
// 			} else if (diffDay >= 5) {
// 				return 'color: red';
// 			} else {
// 				return 'color: black';
// 			}
// 		},
// 		deleteSM(data) {
// 			const swalWithBootstrapButtons = Swal.mixin({
// 				customClass: {
// 					confirmButton: 'btn btn-success',
// 					cancelButton: 'btn btn-danger'
// 				},
// 				buttonsStyling: false
// 			})

// 			swalWithBootstrapButtons.fire({
// 				title: 'Are you sure?',
// 				text: "You want to delete this data?",
// 				icon: 'warning',
// 				showCancelButton: true,
// 				confirmButtonText: 'Yes, delete it!',
// 				cancelButtonText: 'No, cancel!',
// 				reverseButtons: true
// 			}).then((result) => {
// 				if (result.value) {
// 					axios.post(this.action, {
// 						case: 'deletedSM',
// 						dt_so: data,
// 					}).then(response => {
// 						// console.log(response.data);
// 						this.$toasted.success(response.data.message, { duration: 5000 });
// 						this.load();
// 					});
// 				} else if (
// 					result.dismiss === Swal.DismissReason.cancel
// 				) {
// 					swalWithBootstrapButtons.fire(
// 						'Cancelled',
// 						'Your data is safe :)',
// 						'error'
// 					)
// 				}
// 			})
// 		},
// 		simpanHargaKirim2() {
// 			for (let i = 0; i < this.confirmPrice_kirim.length; i++) {
// 				this.new_arr_price_kirim[0].data_so.push(this.confirmPrice_kirim[i].data_so);
// 			}

// 			axios.post(this.action, {
// 				case: 'simpanDistributionPlanKirimLess',
// 				tgl_perintah: this.v_tgl_perintah_kirim,
// 				username: this.username,
// 				office: this.v_office,
// 				dt_so: this.checkedSO,
// 				dt_price: this.new_arr_price_kirim,
// 				totalMuatan: this.totalMuatan,
// 				totalharga: this.totalHarga,
// 			}).then(response => {
// 				this.new_arr_price_kirim[0].data_so = [];
// 				this.checkedSO = [];

// 				let rspn = response.data;
// 				if (rspn.success === 1) {
// 					this.$toasted.success(rspn.message, { duration: 5000 });
// 				} else {
// 					console.log(response.data);
// 					this.$toasted.error(rspn.message, { duration: 5000 });
// 				}
// 			});
// 		},
// 	},
// 	created() {
// 		this.getOfficeCode();
// 		this.getNow();
// 		EventBus.$on('resetCheckBox', () => {
// 			this.checkedSO = [];
// 		});
// 		EventBus.$on('refreshModal', () => {
// 			// alert('hi');
// 			EventBus.$emit('refreshSNA');
// 		});

// 		// EventBus.$on('confirmPrice_kirim_less', (price_data, tgl_perintah, username, v_office, checkedSO, totalMuatan, totalHarga) => {

// 		// 	this.confirmPrice_kirim = price_data;
// 		// 	this.v_tgl_perintah_kirim = tgl_perintah;
// 		// 	this.username = username;
// 		// 	this.v_office = v_office;
// 		// 	this.checkedSO = checkedSO;
// 		// 	this.totalMuatan = totalMuatan;
// 		// 	this.totalHarga = totalHarga;

// 		// 	this.simpanHargaKirim2();
// 		// });
// 	},
// 	computed: {
// 		filterListST() {
// 			let filterST = this.v_search_so.toUpperCase();

// 			let result = this.listSM_kirim.filter(function (e) {
// 				let filtered = true
// 				if (filterST && filterST.length > 0) {
// 					filtered = e.so_no.includes(filterST)
// 				}
// 				return filtered;

// 			}).sort((a, b) => {
// 				let modifier = 1;
// 				if (this.currentSortDir === 'desc') modifier = -1;
// 				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
// 				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
// 				return 0;
// 			});

// 			if (result.length == 0) {
// 				let res = this.listSM_kirim.filter(function (e) {
// 					let filtered = true
// 					if (filterST && filterST.length > 0) {
// 						filtered = e.ledgername.includes(filterST)
// 					}
// 					return filtered;

// 				}).sort((a, b) => {
// 					let modifier = 1;
// 					if (this.currentSortDir === 'desc') modifier = -1;
// 					if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
// 					if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
// 					return 0;
// 				});
// 				return res;
// 			} else {
// 				return result;
// 			}
// 		},
// 		totalpages() {
// 			//This calculates the amount of pages
// 			return Math.ceil(this.filterListST.length / this.itemsperpage);
// 		},
// 		sortedData: function () {
// 			return this.listSM_kirim.sort((a, b) => {
// 				let modifier = 1;
// 				if (this.currentSortDir === 'desc') modifier = -1;
// 				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
// 				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
// 				return 0;
// 			});
// 		},
// 		muatan() {
// 			let total = 0;
// 			this.checkedSO.forEach(so => {
// 				// console.log(so);
// 				total += so.ton;
// 			});
// 			this.totalMuatan = Math.round(total);
// 			return total;
// 		}
// 	},
// 	watch: {
// 		data: function () {
// 			this.pagecurrent = 1;
// 			this.pagestart = 0;
// 			this.pageend = this.itemsperpage;
// 		},
// 	}

// });

Vue.component('daf_so_kirim_comp_sm_less', {

	template: `
	<div>
		<!-- <div class="row">
			<div class="col-md-2" style="text-align: start;">
				<label class="control-label text-center">Lokasi Ambil</label>
			</div>
			<div class="col-md-2" style="text-align: start;">
				<label class="control-label text-center">Tanggal Kirim</label>
			</div>
		</div> -->
		<!-- <div class="row">
			<div class="col-md-2">
				<v-select style="text-transform: capitalize" label="kd_off" :options="officeCode" v-model="v_office" :reduce="officeCode=>officeCode.kd_off"></v-select>
			</div>
			<div class="col-md-2">
				<vuejs-datepicker
					v-model="v_tgl_perintah_kirim"
					:format="DatePickerFormatb"
					:disabledDates="disabledDates"
					:bootstrap-styling="true"
					:placeholder="holderb"
					required
					style="width: 290px;">
				</vuejs-datepicker>
			</div>
			<div class="col-md-1">
				<button type="button" class="btn btn-xm btn-primary" @click="add_SO" :disabled="isActive">Simpan</button>
			</div>
			<div class="col-md-1">
				<button type="button" class="btn btn-xm btn-success" @click="load"><i class="fa fa-refresh"></i></button>
			</div>
		</div> -->
		<div class="lds-facebook" v-if="loading"><div></div><div></div><div></div></div>
		<div class="row">
			<div class="col-md-2" style="text-align: start;">
				<label class="control-label text-center">Tanggal Kirim</label>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<vuejs-datepicker
					v-model="v_tgl_perintah_kirim"
					:format="DatePickerFormatb"
					:disabledDates="disabledDates"
					:bootstrap-styling="true"
					:placeholder="holderb"
					required
					style="width: 290px;">
				</vuejs-datepicker>
			</div>
			<div class="col-sm-1" style="">
				<button type="button" class="btn btn-xm btn-primary" @click="add_SO" :disabled="isActive">Simpan</button>
			</div>
			<div class="col-sm-7" style="">
				<button type="button" class="btn btn-xm btn-success" @click="load"><i class="fa fa-refresh"></i></button>
			</div>
			<div class="col-sm-2" style="">
				<label>Cari</label>
				<input type="text" class="form-control" placeholder="No. SO" v-model="v_search_so">
			</div>
		</div>
		</br>
		<div class="row">
			<div class="col-sm-2" style="text-align: left;">
				<h4>TOTAL MUATAN: {{ Math.round(muatan) }} TON</h4>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" style="text-transform: capitalize;">
				<table class="table table-hover table-responsive table-striped" style="font-size: 12px;">
					<thead>
						<tr>
							<th class="active">#</th>
							<th class="active">Comp</th>
							<th class="active">Ledger</th>
							<th class="active">Customer</th>
							<th v-for="column in new_columns" @click="activeColumn = column" class="active">
								{{ colTitles[column.name] }} 
								<span @click="column.order = column.order * (-1), sortBy(column.name)" class="arrow" :class="column.order > 0 ? 'asc' : 'dsc'" ></span>
							</th>
							<th class="active">Alamat</th>
							<th class="active">Keterangan</th>
							<th class="active">Sales</th>
							<th class="active">Ton</th>
						</tr>
					</thead>
					<tbody>
						<tr v-for="(dt,i) in filterListST" :key="i"  v-if="i >= pagestart && i < pageend" style="text-transform: uppercase;"
							:style="color(dt.tanggal)">
							<td>
								<label class="form-checkbox">
									<input type="checkbox" :value="dt" v-model="pilihSO">
									<i class="form-icon"></i>
								</label>
							</td>
							<td>{{ dt.code_comp }}</td>
							<td>{{ dt.code }}</td>
							<td style="width: 10%;">{{ dt.ledgername }}</td>
							<td style="width: 10%;">{{ dt.tanggal }} {{ dt.jaminput }}</td>
							<td style="width: 10%;">{{ dt.delivery_time }}</td>
							<td style="width: 5%;">{{ dt.sm_no }}</td>
							<td>{{ dt.so_no }}</td>
							<td style="width: 13%;">{{ dt.item_code }}</td>
							<td>{{ dt.alamat_toko }}</td>
							<td>{{ dt.keterangan }}</td>
							<td>{{ dt.sales_code }}</td>
							<td>{{ dt.ton }}</td>
						</tr>
					</tbody>
				</table>
			</div>
			</br></br></br>
			<div class="col-md-12" style="text-transform: capitalize; text-align: center;">
			<paginate 
				:pagecount="totalpages"
				:containerclass="'pagination pagination-sm'"
				:clickhandler="pagination"
				:prevtext="prevButtonIcon"
				:nexttext="nextButtonIcon">
			</paginate>
			</div>
		</div>
		<div class="row">
			<modalBiayaTambahanNew></modalBiayaTambahanNew>
		</div>
	</div>
	`,
	components: {
		vuejsDatepicker
	},
	props: {
		username: {
			required: true
		},
		loc: {
			required: true
		},
		where: {
			required: true
		}
	},
	data() {
		return {
			loading: false,
			v_tgl_perintah_kirim: '',
			officeCode: [],
			current_kdOff: [],
			checkedSO: [],
			pilihSO: [],
			v_office: this.loc,
			v_office_current: this.loc,
			columns: ['type', 'code', 'ledgername', 'tanggal', 'sm_no', 'so_no',
				'item_code', 'alamat_toko', 'keterangan', 'jaminput', 'code_comp', 'sales_code', 'status_so', 'dept_code', 'pilih'],
			pagestart: 0,
			pagecurrent: 1,
			itemsperpage: 100,
			pageend: 100,
			prevButtonIcon: '<i class="fa fa-chevron-left"></i>',
			nextButtonIcon: '<i class="fa fa-chevron-right"></i>',
			currentSort: 'name',
			currentSortDir: 'asc',
			activeColumn: {},
			new_columns: [
				{ name: 'tanggal', order: 1 },
				{ name: 'delivery', order: 1 },
				{ name: 'sm_no', order: 1 },
				{ name: 'so_no', order: 1 },
				{ name: 'item_code', order: 1 },
			],
			colTitles: {
				'tanggal': 'Tgl. SO / SM',
				'delivery': 'Tgl. Kirim',
				'sm_no': 'No. SM',
				'so_no': 'No. SO',
				'item_code': 'Item & QTY',
			},
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 8640000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
			v_search_so: '',
			styleObject: {
				background: 'none',
			},
			totalMuatan: Math.round(0),
			total_ton: 0,
			confirmPrice_kirim: [],
			new_arr_price_kirim: [{
				data_so: []
			}],
			listSM_kirim_less: [],
			isActive: true
		}
	},
	methods: {
		closeModalSM() {
			$("#modal_buat_sm").modal("hide");
		},
		getDataSMKirim() {
			this.checkedSO = [];
			this.pilihSO = [];
			this.loading = true;
			this.isActive = false;

			axios.post(this.action, {
				case: 'getDataSMKirim',
				username: this.username,
				where: this.where
			}).then(response => {
				if (response.data != null) {
					this.loading = false;
					this.listSM_kirim_less = response.data;
					console.log(response.data)
				} else {
					this.listSM_kirim = [];
				}
			});
		},
		getOfficeCode() {
			axios.post(this.action, {
				case: 'getOfficeCode',
				username: 'rachmadi'
			}).then(response => {
				this.officeCode = response.data.dt_kd_offifce[0];
			});
		},
		add_SO() {
			if (this.totalMuatan >= 5 && this.totalMuatan < 10 && this.totalMuatan !== 0 || this.totalMuatan < 4) {
				let userdata = {
					listSo: this.pilihSO,
					totalMuatan: this.totalMuatan,
					loc: this.loc,
					username: this.username,
					tglPerintahKirim: this.v_tgl_perintah_kirim,
					vOffice: this.v_office
				};
				EventBus.$emit('modalBiayaTambahanNew', userdata);
				userdata = [];
				this.pilihSO = [];
			} else if (this.totalMuatan < 5 && this.totalMuatan >= 4) {
				this.saveSO();
			} else if (this.totalMuatan >= 10) {
				this.saveSO();
			} else {
				swal.fire({
					title: "Gagal Simpan SO",
					text: "Silahkan Pilih SO Terlebih Dahulu",
					icon: "error",
				}).then(function (isConfirm) {
					if (isConfirm) {
						console.log('gagal boss');
					}
				});
			}
		},
		saveSO() {
			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.pilihSO,
			}).then(response => {
				// console.log(response.data);
				EventBus.$emit('confirmPrice_kirim_less_new3', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.pilihSO, this.totalMuatan);
				this.pilihSO = [];
				this.isActive = true;
				this.getDataSMKirim();
			});
		},
		pagination(index) {
			if (index >= 0 && index <= this.totalpages) {
				this.pagecurrent = index;
				this.pagestart = (this.itemsperpage * index) - this.itemsperpage;
				this.pageend = (this.itemsperpage * index);
			}
		},
		sortBy(column) {
			if (column === this.currentSort) {
				this.currentSortDir = this.currentSortDir === 'asc' ? 'desc' : 'asc';
			}
			this.currentSort = column;
		},
		diffDay(dateSo) {
			var dateNow = new Date().getTime();
			var dateSo = new Date(dateSo).getTime();
			var diffDay = parseInt((dateNow - dateSo) / (24 * 3600 * 1000));
			return diffDay;
		},
		getNow() {
			const today = new Date();
			const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
			const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			const dateTime = date + ' ' + time;
			this.v_tgl_perintah_kirim = dateTime;
		},
		send_mail_approve(data) {
			// console.log(data);
			this.loading = true;
			axios.post(this.action, {
				case: 'send_mail_approve',
				dt_so: data,
			}).then(response => {
				console.log(response.data);
				this.loading = false;
				this.$toasted.success(response.data, { duration: 5000 });
				// console.log("satu cabang");
				this.checkedSO = [];
			});
		},
		load() {
			this.getDataSMKirim();
		},
		color(tgl) {
			var dateNow = new Date().getTime();
			var dateSo = new Date(tgl).getTime();
			var diffDay = parseInt((dateNow - dateSo) / (24 * 3600 * 1000));

			if (diffDay >= 3 && diffDay < 5) {
				return 'color: orange';
			} else if (diffDay >= 5) {
				return 'color: red';
			} else {
				return 'color: black';
			}
		},
		deleteSM(data) {
			const swalWithBootstrapButtons = Swal.mixin({
				customClass: {
					confirmButton: 'btn btn-success',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})

			swalWithBootstrapButtons.fire({
				title: 'Are you sure?',
				text: "You want to delete this data?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					axios.post(this.action, {
						case: 'deletedSM',
						dt_so: data,
					}).then(response => {
						// console.log(response.data);
						this.$toasted.success(response.data.message, { duration: 5000 });
						this.load();
					});
				} else if (
					result.dismiss === Swal.DismissReason.cancel
				) {
					swalWithBootstrapButtons.fire(
						'Cancelled',
						'Your data is safe :)',
						'error'
					)
				}
			})
		},
		simpanHargaKirim3() {
			this.new_arr_price_kirim[0].data_so = [];

			for (let i = 0; i < this.confirmPrice_kirim.length; i++) {
				this.new_arr_price_kirim[0].data_so.push(this.confirmPrice_kirim[i].data_so);
			}

			axios.post(this.action, {
				case: 'simpanDistributionPlanKirimLess',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
				dt_price: this.new_arr_price_kirim,
				totalMuatan: this.totalMuatan,
				totalharga: this.totalHarga,
			}).then(response => {
				this.new_arr_price_kirim[0].data_so = [];
				this.checkedSO = [];
				this.confirmPrice_kirim = [];

				let rspn = response.data;
				if (rspn.success === 1) {
					console.log(response.data);
					this.$toasted.success(rspn.message, { duration: 5000 });
				} else {
					console.log(response.data);
					this.$toasted.error(rspn.message, { duration: 5000 });
				}
			});
		},
	},
	created() {
		this.getDataSMKirim();
		this.getOfficeCode();
		this.getNow();
		EventBus.$on('resetCheckBox', () => {
			this.pilihSO = [];
		});
		EventBus.$on('refreshModal', () => {
			// alert('hi');
			EventBus.$emit('refreshSNA');
		});

		EventBus.$on('confirmPrice_kirim_less_new3', (price_data, tgl_perintah, username, v_office, checkedSO, totalMuatan, totalHarga) => {

			this.confirmPrice_kirim = price_data;
			this.v_tgl_perintah_kirim = tgl_perintah;
			this.username = username;
			this.v_office = v_office;
			this.checkedSO = checkedSO;
			this.totalMuatan = totalMuatan;
			this.totalHarga = totalHarga;

			this.simpanHargaKirim3();
		});

		EventBus.$on('refreshSM', () => {
			this.pilihSO = [];
			this.getDataSMKirim();
		});
	},
	computed: {
		filterListST() {
			let filterST = this.v_search_so.toUpperCase();

			let result = this.listSM_kirim_less.filter(function (e) {
				let filtered = true
				if (filterST && filterST.length > 0) {
					filtered = e.so_no.includes(filterST)
				}
				return filtered;

			}).sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});

			if (result.length == 0) {
				let res = this.listSM_kirim_less.filter(function (e) {
					let filtered = true
					if (filterST && filterST.length > 0) {
						filtered = e.ledgername.includes(filterST)
					}
					return filtered;

				}).sort((a, b) => {
					let modifier = 1;
					if (this.currentSortDir === 'desc') modifier = -1;
					if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
					if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
					return 0;
				});
				return res;
			} else {
				return result;
			}
		},
		totalpages() {
			//This calculates the amount of pages
			return Math.ceil(this.filterListST.length / this.itemsperpage);
		},
		sortedData: function () {
			return this.listSM_kirim_less.sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});
		},
		muatan() {
			let total = 0;
			this.pilihSO.forEach(so => {
				// console.log(so);
				total += so.ton;
			});
			this.totalMuatan = Math.round(total);
			return total;
		}
	},
	watch: {
		data: function () {
			this.pagecurrent = 1;
			this.pagestart = 0;
			this.pageend = this.itemsperpage;
		},
	}

});

Vue.component('daf_so_kirim_new', {


	props: ['data_less', 'data_more', 'data_sna', 'username', 'loc'],
	template: `
	<div>		
		<div class="row">
			<div class="col-md-12">
				<div class="lds-facebook" v-if="loading"><div></div><div></div><div></div></div>
				<tabs_distribution_plan_component>
					<tab_distribution_plan_component v-for="(dt,i) in tabs" :key="i" :name="dt.name" :icon="dt.icon" :selected="i === 0 ? 'true' : ''">
						<div v-if="i == 0">
							<!-- <v-client-table style="font-size: 11px;" :data="data_more" :options="optionsmore" :columns="coloumn_more"></v-client-table> -->
							<daf_so_kirim_new_more2 :data="data_more" :username="username" :loc="loc"></daf_so_kirim_new_more2>
						</div>
						<div v-if="i == 1">
							<!-- <v-client-table style="font-size: 11px;" :data="data_less" :options="optionsless" :columns="coloumn_less"></v-client-table> -->
							<daf_so_kirim_new_less :data="data_less" :username="username" :loc="loc"></daf_so_kirim_new_less>
						</div>
						<div v-if="i == 2">
							<daf_so_kirim_new_sna :data="data_sna" :username="username" :loc="loc"></daf_so_kirim_new_sna>
						</div>
					</tab_distribution_plan_component>
				</tabs_distribution_plan_component>
			</div>
		</div>
	</div>
	`,
	data() {
		return {
			loading: false,
			tabs: [
				{
					name: 'SAA (10 TON)',
					icon: '<i class="fa fa-print"></i>',
					counter: 0,
				},
				{
					name: 'SAA',
					icon: '<i class="fa fa-print"></i>',
					counter: 1,
				},
				{
					name: 'SNA',
					icon: '<i class="fa fa-print"></i>',
					counter: 2,
				},
			],
			coloumn_more: [
				'tipe_muat',
				'code',
				'ledgername',
				'tanggal',
				'delivery',
				'sm_no',
				'so_no',
				'item_code',
				'alamat_toko',
				'keterangan',
				'sales_code',
				'ton',
			],
			optionsmore: {
				headings: {
					ledgername: 'Nama',
					sm_no: 'SM',
					so_no: 'SO',
				},
				sortable: [],
			},
			coloumn_less: [
				'tipe_muat',
				'code',
				'ledgername',
				'tanggal',
				'delivery',
				'sm_no',
				'so_no',
				'item_code',
				'alamat_toko',
				'keterangan',
				'sales_code',
				'ton',
			],
			optionsless: {
				sortable: [],
				headings: {
					ledgername: 'Nama',
					sm_no: 'SM',
					so_no: 'SO',
				},
			}
		}
	},
	created() {
		EventBus.$on('refreshSNA', () => {
			// alert('SNA');
			EventBus.$emit('refreshData');
		});
	}

});

Vue.component('daf_so_kirim_new_less', {

	template: `
	<div>
		<div class="row">
			<div class="col-md-2" style="text-align: start;">
				<label class="control-label text-center">Lokasi Ambil</label>
			</div>
			<div class="col-md-2" style="text-align: start;">
				<label class="control-label text-center">Tanggal Kirim</label>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<v-select style="text-transform: capitalize" label="kd_off" :options="officeCode" v-model="v_office" :reduce="officeCode=>officeCode.kd_off"></v-select>
			</div>
			<div class="col-md-2">
				<vuejs-datepicker
					v-model="v_tgl_perintah_kirim"
					:format="DatePickerFormatb"
					:disabledDates="disabledDates"
					:bootstrap-styling="true"
					:placeholder="holderb"
					required
					style="width: 290px;">
				</vuejs-datepicker>
			</div>
			<div class="col-md-1">
				<button type="button" class="btn btn-xm btn-primary" @click="add_SO" :disabled="totalMuatan < 0">Simpan</button>
			</div>
			<div class="col-md-1">
				<button type="button" class="btn btn-xm btn-success" @click="load"><i class="fa fa-refresh"></i></button>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-10" style="margin-top:20px;text-align: left;">
				<h4>TOTAL MUATAN: {{ Math.round(muatan) }} TON</h4>
			</div>
			<div class="col-sm-2">
				<label>Cari</label>
				<input type="text" class="form-control" placeholder="No. SO" v-model="v_search_so">
			</div>
		</div>
		</br>
		<div class="row">
			<div class="col-md-12" style="text-transform: capitalize;">
				<table class="table table-hover table-responsive table-striped" style="font-size: 12px;">
					<thead>
						<tr>
							<th class="active">#</th>
							<th class="active">Type</th>
							<th class="active">Ledger</th>
							<th class="active">Customer</th>
							<th v-for="column in new_columns" @click="activeColumn = column" class="active">
								{{ colTitles[column.name] }} 
								<span @click="column.order = column.order * (-1), sortBy(column.name)" class="arrow" :class="column.order > 0 ? 'asc' : 'dsc'" ></span>
							</th>
							<th class="active">Alamat</th>
							<th class="active">Keterangan</th>
							<th class="active">Sales</th>
							<th class="active">Ton</th>
						</tr>
					</thead>
					<tbody>
						<tr v-for="(dt,i) in filterListST" :key="i"  v-if="i >= pagestart && i < pageend" style="text-transform: uppercase;"
							:style="color(dt.tanggal)">
							<td>
								<label class="form-checkbox">
									<input type="checkbox" :value="dt" v-model="checkedSO">
									<i class="form-icon"></i>
								</label>
							</td>
							<td>{{ dt.code_comp }}</td>
							<td>{{ dt.code }}</td>
							<td style="width: 10%;">{{ dt.ledgername }}</td>
							<td style="width: 10%;">{{ dt.tanggal }} {{ dt.jaminput }}</td>
							<td style="width: 10%;">{{ dt.delivery_time }}</td>
							<td style="width: 5%;">{{ dt.sm_no }}</td>
							<td>{{ dt.so_no }}</td>
							<td style="width: 13%;">{{ dt.item_code }}</td>
							<td>{{ dt.alamat_toko }}</td>
							<td>{{ dt.keterangan }}</td>
							<td>{{ dt.sales_code }}</td>
							<td>{{ dt.ton }}</td>
						</tr>
					</tbody>
				</table>
			</div>
			</br></br></br>
			<div class="col-md-12" style="text-transform: capitalize; text-align: center;">
			<paginate 
				:pagecount="totalpages"
				:containerclass="'pagination pagination-sm'"
				:clickhandler="pagination"
				:prevtext="prevButtonIcon"
				:nexttext="nextButtonIcon">
			</paginate>
			</div>
		</div>
		<div class="row">
			<modal-biaya-tambahan />
		</div>
	</div>
	`,
	components: {
		vuejsDatepicker
	},
	props: {
		username: {
			required: true
		},
		loc: {
			required: true
		},
		data: {
			type: Array,
			required: true
		},
	},
	data() {
		return {
			v_tgl_perintah_kirim: '',
			officeCode: [],
			current_kdOff: [],
			checkedSO: [],
			v_office: this.loc,
			v_office_current: this.loc,
			columns: ['type', 'code', 'ledgername', 'tanggal', 'sm_no', 'so_no',
				'item_code', 'alamat_toko', 'keterangan', 'jaminput', 'code_comp', 'sales_code', 'status_so', 'dept_code', 'pilih'],
			pagestart: 0,
			pagecurrent: 1,
			itemsperpage: 100,
			pageend: 100,
			prevButtonIcon: '<i class="fa fa-chevron-left"></i>',
			nextButtonIcon: '<i class="fa fa-chevron-right"></i>',
			currentSort: 'name',
			currentSortDir: 'asc',
			activeColumn: {},
			new_columns: [
				{ name: 'tanggal', order: 1 },
				{ name: 'delivery', order: 1 },
				{ name: 'sm_no', order: 1 },
				{ name: 'so_no', order: 1 },
				{ name: 'item_code', order: 1 },
			],
			colTitles: {
				'tanggal': 'Tgl. SO / SM',
				'delivery': 'Tgl. Kirim',
				'sm_no': 'No. SM',
				'so_no': 'No. SO',
				'item_code': 'Item & QTY',
			},
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 8640000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
			v_search_so: '',
			styleObject: {
				background: 'none',
			},
			totalMuatan: Math.round(0),
		}
	},
	methods: {
		getOfficeCode() {
			axios.post(this.action, {
				case: 'getOfficeCode',
				username: 'rachmadi'
			}).then(response => {
				this.officeCode = response.data.dt_kd_offifce[0];
			});
		},
		add_SO() {
			if (this.totalMuatan >= 5 && this.totalMuatan < 10 && this.totalMuatan !== 0 || this.totalMuatan < 4) {
				let userdata = {
					listSo: this.checkedSO,
					totalMuatan: this.totalMuatan,
					loc: this.loc,
					username: this.username,
					tglPerintahKirim: this.v_tgl_perintah_kirim,
					vOffice: this.v_office
				};
				EventBus.$emit('modalBiayaTambahan', userdata);
			} else if (this.totalMuatan < 5 && this.totalMuatan >= 4) {
				this.saveSO();
			} else if (this.totalMuatan >= 10) {
				this.saveSO();
				// alert('hello world');
			} else {
				swal.fire({
					title: "Gagal Simpan SO",
					text: "Silahkan Pilih SO Terlebih Dahulu",
					icon: "error",
				}).then(function (isConfirm) {
					if (isConfirm) {
						console.log('gagal boss');
					}
				});
			}
		},
		saveSO2() {
			axios.post(this.action, {
				case: 'saveDistributionPlanTemp',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO
			}).then(response => {
				console.log(response.data);
				if (response.data.success == 1) {
					this.$toasted.success(response.data.response, { duration: 5000 });
				} else {
					this.$toasted.error(response.data.response, { duration: 5000 });
				}
				// console.log("satu cabang");
				// EventBus.$emit('confirmPrice_kirim', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				EventBus.$emit('refresh_so_sc');
				this.checkedSO = [];
			});
		},
		saveSO() {
			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
			}).then(response => {
				console.log(response.data);
				// EventBus.$emit('confirmPrice_kirim', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				// EventBus.$emit('confirmPrice_kirim_less', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO, this.totalMuatan);
				this.checkedSO = [];
			});
		},
		saveSOpengalihan() {
			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
			}).then(response => {
				// console.log(response.data);
				// console.log("pengalihan");
				EventBus.$emit('confirmPrice_kirim_pengalihan', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				this.checkedSO = [];
			});
		},
		pagination(index) {
			if (index >= 0 && index <= this.totalpages) {
				this.pagecurrent = index;
				this.pagestart = (this.itemsperpage * index) - this.itemsperpage;
				this.pageend = (this.itemsperpage * index);
			}
		},
		sortBy(column) {
			if (column === this.currentSort) {
				this.currentSortDir = this.currentSortDir === 'asc' ? 'desc' : 'asc';
			}
			this.currentSort = column;
		},
		diffDay(dateSo) {
			var dateNow = new Date().getTime();
			var dateSo = new Date(dateSo).getTime();
			var diffDay = parseInt((dateNow - dateSo) / (24 * 3600 * 1000));
			return diffDay;
		},
		getNow() {
			const today = new Date();
			const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
			const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			const dateTime = date + ' ' + time;
			this.v_tgl_perintah_kirim = dateTime;
		},
		send_mail_approve(data) {
			// console.log(data);
			this.loading = true;
			axios.post(this.action, {
				case: 'send_mail_approve',
				dt_so: data,
			}).then(response => {
				console.log(response.data);
				this.loading = false;
				this.$toasted.success(response.data, { duration: 5000 });
				// console.log("satu cabang");
				this.checkedSO = [];
			});
		},
		load() {
			EventBus.$emit('refresh_so_sc');
		},
		color(tgl) {
			var dateNow = new Date().getTime();
			var dateSo = new Date(tgl).getTime();
			var diffDay = parseInt((dateNow - dateSo) / (24 * 3600 * 1000));

			if (diffDay >= 3 && diffDay < 5) {
				return 'color: orange';
			} else if (diffDay >= 5) {
				return 'color: red';
			} else {
				return 'color: black';
			}
		},
	},
	created() {
		this.getOfficeCode();
		this.getNow();
		EventBus.$on('resetCheckBox', () => {
			this.checkedSO = [];
		});
		EventBus.$on('refreshModal', () => {
			// alert('hi');
			EventBus.$emit('refreshSNA');
		});
	},
	computed: {
		filterListST() {
			let filterST = this.v_search_so.toUpperCase();

			let result = this.data.filter(function (e) {
				let filtered = true
				if (filterST && filterST.length > 0) {
					filtered = e.so_no.includes(filterST)
				}
				return filtered;

			}).sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});

			if (result.length == 0) {
				let res = this.data.filter(function (e) {
					let filtered = true
					if (filterST && filterST.length > 0) {
						filtered = e.ledgername.includes(filterST)
					}
					return filtered;

				}).sort((a, b) => {
					let modifier = 1;
					if (this.currentSortDir === 'desc') modifier = -1;
					if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
					if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
					return 0;
				});
				return res;
			} else {
				return result;
			}
		},
		totalpages() {
			//This calculates the amount of pages
			return Math.ceil(this.filterListST.length / this.itemsperpage);
		},
		sortedData: function () {
			return this.data.sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});
		},
		muatan() {
			let total = 0;
			this.checkedSO.forEach(so => {
				// console.log(so.ton);
				total += so.ton;
			});
			this.totalMuatan = Math.round(total);
			return total;
		}
	},
	watch: {
		data: function () {
			this.pagecurrent = 1;
			this.pagestart = 0;
			this.pageend = this.itemsperpage;
		},
	}

});

Vue.component('daf_so_kirim_new_more', {

	template:
		`
	<div>
		<div class="row">
			<div class="col-md-2" style="text-align: start;">
				<label class="control-label text-center">Lokasi Ambil</label>
			</div>
			<div class="col-md-2" style="text-align: start;">
				<label class="control-label text-center">Tanggal Kirim</label>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<v-select style="text-transform: capitalize" label="kd_off" :options="officeCode" v-model="v_office" :reduce="officeCode=>officeCode.kd_off"></v-select>
			</div>
			<div class="col-md-2">
				<vuejs-datepicker
					v-model="v_tgl_perintah_kirim"
					:format="DatePickerFormatb"
					:disabledDates="disabledDates"
					:bootstrap-styling="true"
					:placeholder="holderb"
					required
					style="width: 290px;">
				</vuejs-datepicker>
			</div>
			<div class="col-md-1">
				<button type="button" class="btn btn-xm btn-primary" @click="add_SO">Simpan</button>
			</div>
			<!-- <div class="col-md-1">
				<button type="button" class="btn btn-xm btn-success" @click="load"><i class="fa fa-refresh"></i></button>
			</div> -->
		</div>
		</br></br>
		<div class="row">
			<div class="col-sm-1">
				<button type="button" class="btn btn-xm btn-success" @click="load"><i class="fa fa-refresh"></i></button>
			</div>
			<div class="col-sm-9" style="text-align: end;">
				<label>Cari</label>
			</div>
			<div class="col-sm-2">
				<input type="text" class="form-control" placeholder="No. SO" v-model="v_search_so">
			</div>
		</div>
		</br>
		<div class="row">
			<div class="col-md-12" style="text-transform: capitalize;">
				<table class="table table-hover table-responsive table-striped" style="font-size: 12px;">
					<thead>
						<tr>
							<th v-for="column in new_columns" @click="activeColumn = column" class="active">
								{{ colTitles[column.name] }} 
								<span @click="column.order = column.order * (-1), sortBy(column.name)" class="arrow" :class="column.order > 0 ? 'asc' : 'dsc'" ></span>
							</th>
							<th class="active">Ton</th>
							<th class="active">Pilih</th>
						</tr>
					</thead>
					<tbody>
						<tr v-for="(dt,i) in filterListST" :key="i"  v-if="i >= pagestart && i < pageend" style="text-transform: uppercase;"
							:style="color(dt.tanggal)">
							<td>{{ dt.code_comp }}</td>
							<td>{{ dt.code }}</td>
							<td style="width: 10%;">{{ dt.ledgername }}</td>
							<td style="width: 10%;">{{ dt.tanggal }} {{ dt.jaminput }}</td>
							<td style="width: 10%;">{{ dt.delivery_time }}</td>
							<td style="width: 5%;">{{ dt.sm_no }}</td>
							<td>{{ dt.so_no }}</td>
							<td style="width: 13%;">{{ dt.item_code }}</td>
							<td>{{ dt.alamat_toko }}</td>
							<td>{{ dt.keterangan }}</td>
							<td>{{ dt.sales_code }}</td>
							<td>{{ dt.ton }}</td>
							<td>
								<label class="form-checkbox">
									<input type="checkbox" :value="dt" v-model="checkedSO">
									<i class="form-icon"></i>
								</label>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			</br></br></br>
			<div class="col-md-12" style="text-transform: capitalize; text-align: center;">
			<paginate 
				:pagecount="totalpages"
				:containerclass="'pagination pagination-sm'"
				:clickhandler="pagination"
				:prevtext="prevButtonIcon"
				:nexttext="nextButtonIcon">
			</paginate>
			</div>
		</div>
	</div>
	`,
	components: {
		vuejsDatepicker
	},
	props: {
		username: {
			required: true
		},
		loc: {
			required: true
		},
		data: {
			type: Array,
			required: true
		},
	},
	data() {
		return {
			v_tgl_perintah_kirim: moment(this.v_tgl_kirim).format("YYYY-MM-DD"),
			officeCode: [],
			current_kdOff: [],
			checkedSO: [],
			v_office: this.loc,
			v_office_current: this.loc,
			columns: ['type', 'code', 'ledgername', 'tanggal', 'sm_no', 'so_no',
				'item_code', 'alamat_toko', 'keterangan', 'jaminput', 'code_comp', 'sales_code', 'status_so', 'dept_code', 'pilih'],
			pagestart: 0,
			pagecurrent: 1,
			itemsperpage: 100,
			pageend: 100,
			prevButtonIcon: '<i class="fa fa-chevron-left"></i>',
			nextButtonIcon: '<i class="fa fa-chevron-right"></i>',
			currentSort: 'name',
			currentSortDir: 'asc',
			activeColumn: {},
			new_columns: [
				{ name: 'type', order: 1 },
				{ name: 'code', order: 1 },
				{ name: 'ledgername', order: 1 },
				{ name: 'tanggal', order: 1 },
				{ name: 'delivery', order: 1 },
				{ name: 'sm_no', order: 1 },
				{ name: 'so_no', order: 1 },
				{ name: 'item_code', order: 1 },
				{ name: 'alamat_toko', order: 1 },
				{ name: 'keterangan', order: 1 },
				{ name: 'sales_code', order: 1 },
			],
			colTitles: {
				'type': 'Type',
				'code': 'Ledger',
				'ledgername': 'Customer',
				'tanggal': 'Tgl. SO / SM',
				'delivery': 'Tgl. Kirim',
				'sm_no': 'No. SM',
				'so_no': 'No. SO',
				'item_code': 'Item & QTY',
				'alamat_toko': 'Alamat',
				'keterangan': 'Keterangan',
				'sales_code': 'Sales Code',
			},
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 8640000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
			v_search_so: '',
			styleObject: {
				background: 'none',
			}
		}
	},
	methods: {
		getOfficeCode() {
			axios.post(this.action, {
				case: 'getOfficeCode',
				username: 'rachmadi'
			}).then(response => {
				this.officeCode = response.data.dt_kd_offifce[0];
			});
		},
		add_SO() {
			//gantung
			var total_ton = 0;

			if (this.checkedSO.length > 0) {
				this.saveSO();
			} else {
				swal.fire({
					title: "Gagal Simpan SO",
					text: "Silahkan Pilih SO Terlebih Dahulu",
					icon: "error",
				}).then(function (isConfirm) {
					if (isConfirm) {
						console.log('gagal boss');
					}
				});
			}
		},
		saveSO2() {
			axios.post(this.action, {
				case: 'saveDistributionPlanTemp',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO
			}).then(response => {
				console.log(response.data);
				if (response.data.success == 1) {
					this.$toasted.success(response.data.response, { duration: 5000 });
				} else {
					this.$toasted.error(response.data.response, { duration: 5000 });
				}
				// console.log("satu cabang");
				// EventBus.$emit('confirmPrice_kirim', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				EventBus.$emit('refresh_so_sc');
				this.checkedSO = [];
			});
		},
		saveSO() {
			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
			}).then(response => {
				// console.log(response.data);
				EventBus.$emit('confirmPrice_kirim', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				this.checkedSO = [];
			});
		},
		saveSOpengalihan() {
			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
			}).then(response => {
				// console.log(response.data);
				// console.log("pengalihan");
				EventBus.$emit('confirmPrice_kirim_pengalihan', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				this.checkedSO = [];
			});
		},
		pagination(index) {
			if (index >= 0 && index <= this.totalpages) {
				this.pagecurrent = index;
				this.pagestart = (this.itemsperpage * index) - this.itemsperpage;
				this.pageend = (this.itemsperpage * index);
			}
		},
		sortBy(column) {
			if (column === this.currentSort) {
				this.currentSortDir = this.currentSortDir === 'asc' ? 'desc' : 'asc';
			}
			this.currentSort = column;
		},
		diffDay(dateSo) {
			var dateNow = new Date().getTime();
			var dateSo = new Date(dateSo).getTime();
			var diffDay = parseInt((dateNow - dateSo) / (24 * 3600 * 1000));
			return diffDay;
		},
		getNow() {
			const today = new Date();
			const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
			const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			const dateTime = date + ' ' + time;
			this.v_tgl_perintah_kirim = dateTime;
		},
		send_mail_approve(data) {
			// console.log(data);
			this.loading = true;
			axios.post(this.action, {
				case: 'send_mail_approve',
				dt_so: data,
			}).then(response => {
				console.log(response.data);
				this.loading = false;
				this.$toasted.success(response.data, { duration: 5000 });
				// console.log("satu cabang");
				this.checkedSO = [];
			});
		},
		load() {
			EventBus.$emit('refresh_so_sc');
		},
		color(tgl) {
			var dateNow = new Date().getTime();
			var dateSo = new Date(tgl).getTime();
			var diffDay = parseInt((dateNow - dateSo) / (24 * 3600 * 1000));

			if (diffDay >= 3 && diffDay < 5) {
				return 'color: orange';
			} else if (diffDay >= 5) {
				return 'color: red';
			} else {
				return 'color: black';
			}
		},
	},
	created() {
		this.getOfficeCode();
		this.getNow();
	},
	computed: {
		filterListST() {
			let filterST = this.v_search_so

			let result = this.data.filter(function (e) {
				let filtered = true
				if (filterST && filterST.length > 0) {
					filtered = e.so_no.includes(filterST)
				}
				return filtered;

			}).sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});

			return result;
		},
		totalpages() {
			//This calculates the amount of pages
			return Math.ceil(this.filterListST.length / this.itemsperpage);
		},
		sortedData: function () {
			return this.data.sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});
		}
	},
	watch: {
		data: function () {
			this.pagecurrent = 1;
			this.pagestart = 0;
			this.pageend = this.itemsperpage;
		},
	}

});

Vue.component('daf_so_kirim_new_more2', {

	template: `
	<div>
		<div class="row">
			<!-- <div class="col-md-2" style="text-align: start;">
				<label class="control-label text-center">Lokasi Berangkat</label>
			</div> -->
			<!-- <div class="col-md-2" style="text-align: start;">
				<label class="control-label text-center">Tanggal Kirim</label>
			</div> -->
		</div>
		<div class="row">
			<!-- <div class="col-md-2">
				<v-select style="text-transform: capitalize" label="kd_off" :options="officeCode" v-model="v_office" :reduce="officeCode=>officeCode.kd_off"></v-select>
			</div> -->
			<!-- <div class="col-md-2">
				<vuejs-datepicker
					v-model="v_tgl_perintah_kirim"
					:format="DatePickerFormatb"
					:disabledDates="disabledDates"
					:bootstrap-styling="true"
					:placeholder="holderb"
					required
					style="width: 290px;">
				</vuejs-datepicker>
			</div> -->
			<!-- <div class="col-md-1">
				<button type="button" class="btn btn-xm btn-primary" @click="add_SO">Simpan</button>
			</div> -->
			<!-- <div class="col-md-1">
				<button type="button" class="btn btn-xm btn-success" @click="load"><i class="fa fa-refresh"></i></button>
			</div> -->
		</div>
		<div class="row">
			<div class="col-sm-10" style="margin-top:25px;">
				<button type="button" class="btn btn-xm btn-success" @click="load"><i class="fa fa-refresh"></i></button>
			</div>
			<div class="col-sm-2">
				<label>Cari</label>
				<input type="text" class="form-control" placeholder="No. SO" v-model="v_search_so">
			</div>
		</div>
		</br>
		<div class="row">
			<div class="col-md-12" style="text-transform: capitalize;">
				<table class="table table-hover table-responsive table-striped" style="font-size: 12px;">
					<thead>
						<tr>
							<th class="active">Type</th>						
							<th class="active">Ledger</th>						
							<th class="active">Customer</th>						
							<th v-for="column in new_columns" @click="activeColumn = column" class="active">
								{{ colTitles[column.name] }} 
								<span @click="column.order = column.order * (-1), sortBy(column.name)" class="arrow" :class="column.order > 0 ? 'asc' : 'dsc'" ></span>
							</th>
							<th class="active">Alamat</th>
							<th class="active">Keterangan</th>
							<th class="active">Sales</th>
							<th class="active">Ton</th>
							<th class="active">Lokasi Berangkat</th>
							<th class="active">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<tr v-for="(dt,i) in filterListST" :key="i"  v-if="i >= pagestart && i < pageend" style="text-transform: uppercase;"
							:style="color(dt.tanggal)">
							<td>{{ dt.code_comp }}</td>
							<td>{{ dt.code }}</td>
							<td style="width: 10%;">{{ dt.ledgername }}</td>
							<td style="width: 10%;">{{ dt.tanggal }} {{ dt.jaminput }}</td>
							<td style="width: 10%;">{{ dt.delivery_time }}</td>
							<td style="width: 5%;">{{ dt.sm_no }}</td>
							<td>{{ dt.so_no }}</td>
							<td style="width: 13%;">{{ dt.item_code }}</td>
							<td>{{ dt.alamat_toko }}</td>
							<td>{{ dt.keterangan }}</td>
							<td>{{ dt.sales_code }}</td>
							<td>{{ dt.ton }}</td>
							<td><v-select style="text-transform: capitalize" label="kd_off" :options="officeCode" v-model="v_office" :reduce="officeCode=>officeCode.kd_off"></v-select></td>
							<!-- <td>
								<label class="form-checkbox">
									<input type="checkbox" :value="dt" v-model="checkedSO">
									<i class="form-icon"></i>
								</label>
							</td> --> 
							<td>
								<button type="button" class="btn btn-xs btn-primary" @click="saveSO3(dt)">Simpan</button>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			</br></br></br>
			<div class="col-md-12" style="text-transform: capitalize; text-align: center;">
			<paginate 
				:pagecount="totalpages"
				:containerclass="'pagination pagination-sm'"
				:clickhandler="pagination"
				:prevtext="prevButtonIcon"
				:nexttext="nextButtonIcon">
			</paginate>
			</div>
		</div>
	</div>
	`,
	components: {
		vuejsDatepicker
	},
	props: {
		username: {
			required: true
		},
		loc: {
			required: true
		},
		data: {
			type: Array,
			required: true
		},
	},
	data() {
		return {
			v_tgl_perintah_kirim: '',
			officeCode: [],
			current_kdOff: [],
			checkedSO: [],
			v_office: this.loc,
			v_office_current: this.loc,
			columns: ['type', 'code', 'ledgername', 'tanggal', 'sm_no', 'so_no',
				'item_code', 'alamat_toko', 'keterangan', 'jaminput', 'code_comp', 'sales_code', 'status_so', 'dept_code', 'pilih'],
			pagestart: 0,
			pagecurrent: 1,
			itemsperpage: 100,
			pageend: 100,
			prevButtonIcon: '<i class="fa fa-chevron-left"></i>',
			nextButtonIcon: '<i class="fa fa-chevron-right"></i>',
			currentSort: 'name',
			currentSortDir: 'asc',
			activeColumn: {},
			new_columns: [
				// { name: 'type', order: 1 },
				// { name: 'code', order: 1 },
				// { name: 'ledgername', order: 1 },
				{ name: 'tanggal', order: 1 },
				{ name: 'delivery', order: 1 },
				{ name: 'sm_no', order: 1 },
				{ name: 'so_no', order: 1 },
				{ name: 'item_code', order: 1 },
				// { name: 'alamat_toko', order: 1 },
				// { name: 'keterangan', order: 1 },
				// { name: 'sales_code', order: 1 },
			],
			colTitles: {
				// 'type': 'Type',
				// 'code': 'Ledger',
				// 'ledgername': 'Customer',
				'tanggal': 'Tgl. SO / SM',
				'delivery': 'Tgl. Kirim',
				'sm_no': 'No. SM',
				'so_no': 'No. SO',
				'item_code': 'Item & QTY',
				// 'alamat_toko': 'Alamat',
				// 'keterangan': 'Keterangan',
				// 'sales_code': 'Sales Code',
			},
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 8640000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
			v_search_so: '',
			styleObject: {
				background: 'none',
			}
		}
	},
	methods: {
		getOfficeCode() {
			axios.post(this.action, {
				case: 'getOfficeCode',
				username: 'rachmadi'
			}).then(response => {
				this.officeCode = response.data.dt_kd_offifce[0];
			});
		},
		add_SO() {
			//gantung
			var total_ton = 0;

			if (this.checkedSO.length > 0) {
				this.saveSO();
			} else {
				swal.fire({
					title: "Gagal Simpan SO",
					text: "Silahkan Pilih SO Terlebih Dahulu",
					icon: "error",
				}).then(function (isConfirm) {
					if (isConfirm) {
						console.log('gagal boss');
					}
				});
			}
		},
		saveSO2() {
			axios.post(this.action, {
				case: 'saveDistributionPlanTemp',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO
			}).then(response => {
				console.log(response.data);
				if (response.data.success == 1) {
					this.$toasted.success(response.data.response, { duration: 5000 });
				} else {
					this.$toasted.error(response.data.response, { duration: 5000 });
				}
				// console.log("satu cabang");
				// EventBus.$emit('confirmPrice_kirim', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				EventBus.$emit('refresh_so_sc');
				this.checkedSO = [];
			});
		},
		saveSO() {
			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
			}).then(response => {
				console.log(response.data);
				// EventBus.$emit('confirmPrice_kirim', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				this.checkedSO = [];
			});
		},
		saveSO3(data) {
			// console.log(data);
			let arr_so = [];
			arr_so.push(data);

			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: arr_so,
			}).then(response => {
				// console.log(response.data);
				EventBus.$emit('confirmPrice_kirim', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, arr_so, data.ton);
				this.checkedSO = [];
			});
		},
		saveSOpengalihan() {
			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
			}).then(response => {
				// console.log(response.data);
				// console.log("pengalihan");
				EventBus.$emit('confirmPrice_kirim_pengalihan', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				this.checkedSO = [];
			});
		},
		pagination(index) {
			if (index >= 0 && index <= this.totalpages) {
				this.pagecurrent = index;
				this.pagestart = (this.itemsperpage * index) - this.itemsperpage;
				this.pageend = (this.itemsperpage * index);
			}
		},
		sortBy(column) {
			if (column === this.currentSort) {
				this.currentSortDir = this.currentSortDir === 'asc' ? 'desc' : 'asc';
			}
			this.currentSort = column;
		},
		diffDay(dateSo) {
			var dateNow = new Date().getTime();
			var dateSo = new Date(dateSo).getTime();
			var diffDay = parseInt((dateNow - dateSo) / (24 * 3600 * 1000));
			return diffDay;
		},
		getNow() {
			const today = new Date();
			const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
			const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			const dateTime = date + ' ' + time;
			this.v_tgl_perintah_kirim = dateTime;
		},
		send_mail_approve(data) {
			// console.log(data);
			this.loading = true;
			axios.post(this.action, {
				case: 'send_mail_approve',
				dt_so: data,
			}).then(response => {
				console.log(response.data);
				this.loading = false;
				this.$toasted.success(response.data, { duration: 5000 });
				// console.log("satu cabang");
				this.checkedSO = [];
			});
		},
		load() {
			EventBus.$emit('refresh_so_sc');
			this.recordRefresh('button');
		},
		recordRefresh(source) {
			axios.post(this.action, {
				case: 'recordRefresh',
				username: this.username,
				source: source
			}).then(response => {
				console.log(response.data);
			});
		},
		color(tgl) {
			var dateNow = new Date().getTime();
			var dateSo = new Date(tgl).getTime();
			var diffDay = parseInt((dateNow - dateSo) / (24 * 3600 * 1000));

			if (diffDay >= 3 && diffDay < 5) {
				return 'color: orange';
			} else if (diffDay >= 5) {
				return 'color: red';
			} else {
				return 'color: black';
			}
		},
	},
	created() {
		this.getOfficeCode();
		this.getNow();
		this.recordRefresh('page');
	},
	computed: {
		filterListST() {
			let filterST = this.v_search_so.toUpperCase();

			let result = this.data.filter(function (e) {
				let filtered = true
				if (filterST && filterST.length > 0) {
					filtered = e.so_no.includes(filterST)
				}
				return filtered;

			}).sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});

			if (result.length == 0) {
				let res = this.data.filter(function (e) {
					let filtered = true
					if (filterST && filterST.length > 0) {
						filtered = e.ledgername.includes(filterST)
					}
					return filtered;

				}).sort((a, b) => {
					let modifier = 1;
					if (this.currentSortDir === 'desc') modifier = -1;
					if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
					if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
					return 0;
				});
				return res;
			} else {
				return result;
			}
		},
		totalpages() {
			//This calculates the amount of pages
			return Math.ceil(this.filterListST.length / this.itemsperpage);
		},
		sortedData: function () {
			return this.data.sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});
		}
	},
	watch: {
		data: function () {
			this.pagecurrent = 1;
			this.pagestart = 0;
			this.pageend = this.itemsperpage;
		},
	}

});

Vue.component('daf_so_kirim_new_sna', {
	template: `
	<div>
		<div class="row">
			<div class="col-md-2" style="text-align: start;">
				<label class="control-label text-center">Lokasi Ambil</label>
			</div>
			<div class="col-md-2" style="text-align: start;">
				<label class="control-label text-center">Tanggal Kirim</label>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<v-select style="text-transform: capitalize" label="kd_off" :options="officeCode" v-model="v_office" :reduce="officeCode=>officeCode.kd_off"></v-select>
			</div>
			<div class="col-md-2">
				<vuejs-datepicker
					v-model="v_tgl_perintah_kirim"
					:format="DatePickerFormatb"
					:disabledDates="disabledDates"
					:bootstrap-styling="true"
					:placeholder="holderb"
					required
					style="width: 290px;">
				</vuejs-datepicker>
			</div>
			<div class="col-md-1">
				<button type="button" class="btn btn-xm btn-primary" @click="add_SO">Simpan</button>
			</div>
			<div class="col-md-1">
				<button type="button" class="btn btn-xm btn-success" @click="load"><i class="fa fa-refresh"></i></button>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-10" style="margin-top:20px;text-align: left;">
				<h4>TOTAL MUATAN: {{ Math.round(muatan) }} TON</h4>
			</div>
			<div class="col-sm-2">
				<label>Cari</label>
				<input type="text" class="form-control" placeholder="No. SO" v-model="v_search_so">
			</div>
		</div>
		</br>
		<div class="row">
			<div class="col-md-12" style="text-transform: capitalize;">
				<table class="table table-hover table-responsive table-striped" style="font-size: 12px;">
					<thead>
						<tr>
							<th class="active">#</th>
							<th class="active">Type</th>
							<th class="active">Ledger</th>
							<th class="active">Customer</th>
							<th v-for="column in new_columns" @click="activeColumn = column" class="active">
								{{ colTitles[column.name] }} 
								<span @click="column.order = column.order * (-1), sortBy(column.name)" class="arrow" :class="column.order > 0 ? 'asc' : 'dsc'" ></span>
							</th>
							<th class="active">Alamat</th>
							<th class="active">Keterangan</th>
							<th class="active">Sales</th>
							<th class="active">Ton</th>
						</tr>
					</thead>
					<tbody>
						<tr v-for="(dt,i) in filterListST" :key="i"  v-if="i >= pagestart && i < pageend" style="text-transform: uppercase;"
							:style="color(dt.tanggal)">
							<td>
								<label class="form-checkbox">
									<input type="checkbox" :value="dt" v-model="checkedSO">
									<i class="form-icon"></i>
								</label>
							</td>
							<td>{{ dt.code_comp }}</td>
							<td>{{ dt.code }}</td>
							<td style="width: 10%;">{{ dt.ledgername }}</td>
							<td style="width: 10%;">{{ dt.tanggal }} {{ dt.jaminput }}</td>
							<td style="width: 10%;">{{ dt.delivery_time }}</td>
							<td style="width: 5%;">{{ dt.sm_no }}</td>
							<td>{{ dt.so_no }}</td>
							<td style="width: 13%;">{{ dt.item_code }}</td>
							<td>{{ dt.alamat_toko }}</td>
							<td>{{ dt.keterangan }}</td>
							<td>{{ dt.sales_code }}</td>
							<td>{{ dt.ton }}</td>
						</tr>
					</tbody>
				</table>
			</div>
			</br></br></br>
			<div class="col-md-12" style="text-transform: capitalize; text-align: center;">
			<paginate 
				:pagecount="totalpages"
				:containerclass="'pagination pagination-sm'"
				:clickhandler="pagination"
				:prevtext="prevButtonIcon"
				:nexttext="nextButtonIcon">
			</paginate>
			</div>
		</div>
		<div class="row">
			<modal-biaya-tambahan />
		</div>
	</div>
	`,
	components: {
		vuejsDatepicker
	},
	props: {
		username: {
			required: true
		},
		loc: {
			required: true
		},
		data: {
			type: Array,
			required: true
		},
	},
	data() {
		return {
			v_tgl_perintah_kirim: "",
			officeCode: [],
			current_kdOff: [],
			checkedSO: [],
			v_office: this.loc,
			v_office_current: this.loc,
			columns: ['type', 'code', 'ledgername', 'tanggal', 'sm_no', 'so_no',
				'item_code', 'alamat_toko', 'keterangan', 'jaminput', 'code_comp', 'sales_code', 'status_so', 'dept_code', 'pilih'],
			pagestart: 0,
			pagecurrent: 1,
			itemsperpage: 100,
			pageend: 100,
			prevButtonIcon: '<i class="fa fa-chevron-left"></i>',
			nextButtonIcon: '<i class="fa fa-chevron-right"></i>',
			currentSort: 'name',
			currentSortDir: 'asc',
			activeColumn: {},
			new_columns: [
				{ name: 'tanggal', order: 1 },
				{ name: 'delivery', order: 1 },
				{ name: 'sm_no', order: 1 },
				{ name: 'so_no', order: 1 },
				{ name: 'item_code', order: 1 },
			],
			colTitles: {
				'tanggal': 'Tgl. SO / SM',
				'delivery': 'Tgl. Kirim',
				'sm_no': 'No. SM',
				'so_no': 'No. SO',
				'item_code': 'Item & QTY',
			},
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 8640000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
			v_search_so: '',
			styleObject: {
				background: 'none',
			},
			totalMuatan: Math.round(0)
		}
	},
	methods: {
		getOfficeCode() {
			axios.post(this.action, {
				case: 'getOfficeCode',
				username: 'rachmadi'
			}).then(response => {
				this.officeCode = response.data.dt_kd_offifce[0];
			});
		},
		add_SO() {
			// if (this.totalMuatan >= 5 && this.totalMuatan < 10 && this.totalMuatan !== 0) {
			// 	let userdata = {
			// 		listSo: this.checkedSO,
			// 		totalMuatan: this.totalMuatan,
			// 		loc: this.loc,
			// 		username: this.username,
			// 		tglPerintahKirim: this.v_tgl_perintah_kirim,
			// 		vOffice: this.v_office
			// 	};
			// 	EventBus.$emit('modalBiayaTambahan', userdata);
			// } else if (this.totalMuatan < 5 && this.totalMuatan >= 4) {
			// 	alert('gratis');
			// } else if (this.totalMuatan >= 10) {
			// 	this.saveSO();
			// 	// alert('hello world');
			// } else {
			// 	swal.fire({
			// 		title: "Gagal Simpan SO",
			// 		text: "Silahkan Pilih SO Terlebih Dahulu",
			// 		icon: "error",
			// 	}).then(function (isConfirm) {
			// 		if (isConfirm) {
			// 			console.log('gagal boss');
			// 		}
			// 	});
			// }
			//gantung
			// var total_ton = 0;

			if (this.checkedSO.length > 0) {
				this.saveSO();
			} else {
				swal.fire({
					title: "Gagal Simpan SO",
					text: "Silahkan Pilih SO Terlebih Dahulu",
					icon: "error",
				}).then(function (isConfirm) {
					if (isConfirm) {
						console.log('gagal boss');
					}
				});
			}
		},
		saveSO2() {
			axios.post(this.action, {
				case: 'saveDistributionPlanTemp',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO
			}).then(response => {
				console.log(response.data);
				if (response.data.success == 1) {
					this.$toasted.success(response.data.response, { duration: 5000 });
				} else {
					this.$toasted.error(response.data.response, { duration: 5000 });
				}
				// console.log("satu cabang");
				// EventBus.$emit('confirmPrice_kirim', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				EventBus.$emit('refresh_so_sc');
				this.checkedSO = [];
			});
		},
		saveSO() {
			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
			}).then(response => {
				// console.log(response.data);
				// EventBus.$emit('confirmPrice_kirim', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				// EventBus.$emit('confirmPrice_kirim_less', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO, this.totalMuatan);
				this.checkedSO = [];
			});
		},
		saveSOpengalihan() {
			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
			}).then(response => {
				// console.log(response.data);
				// console.log("pengalihan");
				EventBus.$emit('confirmPrice_kirim_pengalihan', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				this.checkedSO = [];
			});
		},
		pagination(index) {
			if (index >= 0 && index <= this.totalpages) {
				this.pagecurrent = index;
				this.pagestart = (this.itemsperpage * index) - this.itemsperpage;
				this.pageend = (this.itemsperpage * index);
			}
		},
		sortBy(column) {
			if (column === this.currentSort) {
				this.currentSortDir = this.currentSortDir === 'asc' ? 'desc' : 'asc';
			}
			this.currentSort = column;
		},
		diffDay(dateSo) {
			var dateNow = new Date().getTime();
			var dateSo = new Date(dateSo).getTime();
			var diffDay = parseInt((dateNow - dateSo) / (24 * 3600 * 1000));
			return diffDay;
		},
		getNow() {
			const today = new Date();
			const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
			const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			const dateTime = date + ' ' + time;
			this.v_tgl_perintah_kirim = dateTime;
		},
		send_mail_approve(data) {
			// console.log(data);
			this.loading = true;
			axios.post(this.action, {
				case: 'send_mail_approve',
				dt_so: data,
			}).then(response => {
				console.log(response.data);
				this.loading = false;
				this.$toasted.success(response.data, { duration: 5000 });
				// console.log("satu cabang");
				this.checkedSO = [];
			});
		},
		load() {
			EventBus.$emit('refresh_so_sc');
		},
		color(tgl) {
			var dateNow = new Date().getTime();
			var dateSo = new Date(tgl).getTime();
			var diffDay = parseInt((dateNow - dateSo) / (24 * 3600 * 1000));

			if (diffDay >= 3 && diffDay < 5) {
				return 'color: orange';
			} else if (diffDay >= 5) {
				return 'color: red';
			} else {
				return 'color: black';
			}
		},
	},
	created() {
		this.getOfficeCode();
		this.getNow();
		EventBus.$on('resetCheckBox', () => {
			this.checkedSO = [];
		});
		EventBus.$on('refreshModal', () => {
			// alert('hi');
			EventBus.$emit('refreshSNA');
		});
	},
	computed: {
		filterListST() {
			let filterST = this.v_search_so.toUpperCase();

			let result = this.data.filter(function (e) {
				let filtered = true
				if (filterST && filterST.length > 0) {
					filtered = e.so_no.includes(filterST)
				}
				return filtered;

			}).sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});

			if (result.length == 0) {
				let res = this.data.filter(function (e) {
					let filtered = true
					if (filterST && filterST.length > 0) {
						filtered = e.ledgername.includes(filterST)
					}
					return filtered;

				}).sort((a, b) => {
					let modifier = 1;
					if (this.currentSortDir === 'desc') modifier = -1;
					if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
					if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
					return 0;
				});
				return res;
			} else {
				return result;
			}
		},
		totalpages() {
			//This calculates the amount of pages
			return Math.ceil(this.filterListST.length / this.itemsperpage);
		},
		sortedData: function () {
			return this.data.sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});
		},
		muatan() {
			let total = 0;
			this.checkedSO.forEach(so => {
				// console.log(so.ton);
				total += so.ton;
			});
			this.totalMuatan = Math.round(total);
			return total;
		}
	},
	watch: {
		data: function () {
			this.pagecurrent = 1;
			this.pagestart = 0;
			this.pageend = this.itemsperpage;
		},
	}

});

Vue.component('modalBiayaTambahan', {
	template: `
	<div class="modal fade" id="biayaTambahan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="row">
						<div class="col-sm-6">
							<h4 class="modal-title" id="exampleModalLabel">Biaya Tambahan</h4>
						</div>
						<div class="col-sm-6">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					</div>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group col-md-12">
								<p>Total muatan tidak memenuhi standar yang telah ditentukan, yaitu <span class="label label-warning">10 TON</span>. Muatan ini akan dikenakan biaya tambahan</p>
								<p>Silahkan masukkan jumlah biaya tambahan</p>
							</div>
							<div class="form-group col-md-8">
							
								<label>Total Biaya Tambahan</label>
								<!-- <currency-input
									class="form-control"
									v-model="harga"
									currency="IDR"
									locale="id" /> -->
								<input type="number" class="form-control" placeholder="Harga" v-model="harga">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal" @click="simpanData">Simpan</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal" @click="resetData">Batalkan</button>
				</div>
			</div>
		</div>
	</div>
	`,
	data() {
		return {
			listSo: [],
			totalMuatan: 0,
			harga: 0,
			loc: '',
			username: '',
			tglPerintahKirim: moment(new Date().toISOString()).format("YYYY-MM-DD"),
			vOffice: '',
		}
	},
	methods: {
		resetData() {
			this.listSo = [];
			this.totalMuatan = 0;
			EventBus.$emit('resetCheckBox');
		},

		simpanData() {

			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.tglPerintahKirim,
				username: this.username,
				office: this.vOffice,
				dt_so: this.listSo,
			}).then(response => {
				console.log(response.data);
				// EventBus.$emit('confirmPrice_kirim_less', response.data, this.tglPerintahKirim, this.username, this.vOffice, this.listSo, this.totalMuatan, this.harga);
				this.resetData();
				EventBus.$emit('refreshModal');
				this.checkedSO = [];
			});

			// axios.post(this.action, {
			// 	case: 'saveDpPrice',
			// 	tgl_perintah: this.tglPerintahKirim,
			// 	username: this.username,
			// 	office: this.vOffice,
			// 	harga: this.harga,
			// 	dt_so: this.listSo,
			// 	totalMuatan: this.totalMuatan,
			// }).then(response => {
			// 	let rspn = response.data;
			// 	console.log(rspn);
			// 	// if(rspn.message)
			// 	this.resetData();
			// 	// EventBus.$emit('refreshModal');
			// });
		},
	},
	created() {
		EventBus.$on('modalBiayaTambahan', (userdata) => {
			console.log(userdata);
			this.listSo = userdata.listSo;
			this.totalMuatan = userdata.totalMuatan;
			this.loc = userdata.loc;
			this.username = userdata.username;
			this.tglPerintahKirim = userdata.tglPerintahKirim;
			this.vOffice = userdata.vOffice;
			$('#biayaTambahan').modal('show');
		});
	}
});

Vue.component('modalBiayaTambahanNew', {
	template: `
	<div class="modal fade" id="biayaTambahan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="row">
						<div class="col-sm-6">
							<h4 class="modal-title" id="exampleModalLabel">Biaya Tambahan</h4>
						</div>
						<div class="col-sm-6">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					</div>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group col-md-12">
								<p>Total muatan tidak memenuhi standar yang telah ditentukan, yaitu <span class="label label-warning">10 TON</span>. Muatan ini akan dikenakan biaya tambahan</p>
								<p>Silahkan masukkan jumlah biaya tambahan</p>
							</div>
							<div class="form-group col-md-8">
							
								<label>Total Biaya Tambahan</label>
								<!-- <currency-input
									class="form-control"
									v-model="harga"
									currency="IDR"
									locale="id" /> -->
								<input type="number" class="form-control" placeholder="Harga" v-model="harga">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal" @click="simpanData">Simpan</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal" @click="resetData">Batalkan</button>
				</div>
			</div>
		</div>
	</div>
	`,
	data() {
		return {
			listSo: [],
			totalMuatan: 0,
			harga: 0,
			loc: '',
			username: '',
			tglPerintahKirim: moment(new Date().toISOString()).format("YYYY-MM-DD"),
			vOffice: '',
			confirmPrice_kirim: [],
			new_arr_price_kirim: [{
				data_so: []
			}],
		}
	},
	methods: {
		resetData() {
			this.listSo = [];
			this.totalMuatan = 0;
			EventBus.$emit('resetCheckBox');
		},

		simpanData() {

			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.tglPerintahKirim,
				username: this.username,
				office: this.vOffice,
				dt_so: this.listSo,
			}).then(response => {
				console.log(response.data);
				EventBus.$emit('confirmPrice_kirim_less_new2', response.data, this.tglPerintahKirim, this.username, this.vOffice, this.listSo, this.totalMuatan, this.harga);
				this.resetData();
				EventBus.$emit('refreshSM');
				this.checkedSO = [];
			});
		},
		simpanHargaKirim2() {
			this.new_arr_price_kirim[0].data_so = [];

			for (let i = 0; i < this.confirmPrice_kirim.length; i++) {
				this.new_arr_price_kirim[0].data_so.push(this.confirmPrice_kirim[i].data_so);
			}

			axios.post(this.action, {
				case: 'simpanDistributionPlanKirimLess',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
				dt_price: this.new_arr_price_kirim,
				totalMuatan: this.totalMuatan,
				totalharga: this.totalHarga,
			}).then(response => {
				this.new_arr_price_kirim[0].data_so = [];
				this.checkedSO = [];
				this.confirmPrice_kirim = [];

				let rspn = response.data;
				if (rspn.success === 1) {
					this.$toasted.success(rspn.message, { duration: 5000 });
				} else {
					console.log(response.data);
					this.$toasted.error(rspn.message, { duration: 5000 });
				}
			});
		},
	},
	created() {
		EventBus.$on('modalBiayaTambahanNew', (userdata) => {
			console.log(userdata);
			this.listSo = userdata.listSo;
			this.totalMuatan = userdata.totalMuatan;
			this.loc = userdata.loc;
			this.username = userdata.username;
			this.tglPerintahKirim = userdata.tglPerintahKirim;
			this.vOffice = userdata.vOffice;
			$('#biayaTambahan').modal('show');
		});

		EventBus.$on('confirmPrice_kirim_less_new2', (price_data, tgl_perintah, username, v_office, checkedSO, totalMuatan, totalHarga) => {

			this.confirmPrice_kirim = price_data;
			this.v_tgl_perintah_kirim = tgl_perintah;
			this.username = username;
			this.v_office = v_office;
			this.checkedSO = checkedSO;
			this.totalMuatan = totalMuatan;
			this.totalHarga = totalHarga;

			this.simpanHargaKirim2();
		});
	}
})

Vue.component('daf_so_ambil_component', {

	// <div class="col-md-2" style="text-align: start;">
	// 	<label class="control-label text-center">Lokasi Ambil</label>
	// </div>
	// <div class="col-md-2">
	// 	<v-select style="text-transform: capitalize" label="kd_off" :options="officeCode" v-model="v_office" :reduce="officeCode=>officeCode.kd_off"></v-select>
	// </div>
	template: `
	<div>
		<div class="row">
			<div class="col-md-2" style="text-align: start;">
				<label class="control-label text-center">Lokasi Ambil</label>
			</div>
			<div class="col-md-2" style="text-align: start;">
				<label class="control-label text-center">Tanggal Ambil</label>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<v-select style="text-transform: capitalize" label="kd_off" :options="officeCode" v-model="v_office" :reduce="officeCode=>officeCode.kd_off"></v-select>
			</div>
			<div class="col-md-2">
				<vuejs-datepicker
					v-model="v_tgl_perintah_kirim"
					:format="DatePickerFormatb"
					:disabledDates="disabledDates"
					:bootstrap-styling="true"
					:placeholder="holderb"
					required
					style="width: 290px;">
				</vuejs-datepicker>
			</div>
			<div class="col-md-1">
				<button type="button" class="btn btn-xm btn-primary" @click="add_SO">Simpan</button>
			</div>
			<div class="col-md-1">
				<button type="button" class="btn btn-xm btn-success" @click=""><i class="fa fa-refresh"></i></button>
			</div>
		</div>
		</br></br></br></br>
		<div class="row">
			<div class="col-sm-10" style="text-align: end;">
				<label>Cari</label>
			</div>
			<div class="col-sm-2">
				<input type="text" class="form-control" placeholder="No. SO" v-model="v_search_so">
			</div>
		</div>
		</br>
		<div class="row">
			<div class="col-md-12" style="text-transform: capitalize;">
				<table class="table table-hover table-responsive table-striped" style="font-size: 12px;">
					<thead>
						<tr>
							<th v-for="column in new_columns" @click="activeColumn = column" class="active">
								{{ colTitles[column.name] }} 
								<span @click="column.order = column.order * (-1), sortBy(column.name)" class="arrow" :class="column.order > 0 ? 'asc' : 'dsc'" ></span>
							</th>
							<th class="active">Pilih</th>
						</tr>
					</thead>
					<tbody>
						<tr v-for="(dt,i) in filterListST" :key="i"  v-if="i >= pagestart && i < pageend" style="text-transform: uppercase;">
							<td>{{ dt.code_comp }}</td>
							<td>{{ dt.code }}</td>
							<td style="width: 10%;">{{ dt.ledgername }}</td> 
							<td style="width: 10%;">{{ dt.tanggal }} {{ dt.jaminput }}</td> 
							<td style="width: 10%;">{{ dt.delivery_time }}</td> 
							<td style="width: 5%;">{{ dt.sm_no }}</td>
							<td>{{ dt.so_no }}</td>
							<td style="width: 13%;">{{ dt.item_code }}</td>
							<td>{{ dt.alamat_toko }}</td>
							<td>{{ dt.keterangan }}</td>
							<td>
								<label class="form-checkbox">
									<input type="checkbox" :value="dt" v-model="checkedSO">
									<i class="form-icon"></i>
								</label>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			</br></br></br>
			<div class="col-md-12" style="text-transform: capitalize; text-align: center;">
				<paginate 
					:pagecount="totalpages"
					:containerclass="'pagination pagination-sm'"
					:clickhandler="pagination"
					:prevtext="prevButtonIcon"
					:nexttext="nextButtonIcon">
				</paginate>
			</div>
		</div>
	</div>
	`,
	components: {
		vuejsDatepicker
	},
	props: {
		username: {
			required: true
		},
		loc: {
			required: true
		},
		data: {
			type: Array,
			required: true,
		}
	},
	data() {
		return {
			v_tgl_perintah_kirim: '',
			checkedSO: [],
			officeCode: [],
			v_office: this.loc,
			// v_office: "",
			v_office_current: this.loc,
			columns: ['type', 'code', 'ledgername', 'tanggal', 'sm_no', 'so_no',
				'item_code', 'alamat_toko', 'keterangan', 'jaminput', 'pilih'],
			// options: {
			// 	perPage: 50,
			// 	sortable: ['type', 'code', 'ledgername'],
			// 	headings: {
			// 		type: 'Type',
			// 		code: 'Ledger',
			// 		ledgername: 'Customer',
			// 		tanggal: 'Tgl. SO',
			// 		sm_no: 'No. SM',
			// 		so_no: 'No. SO',
			// 		item_code: 'Item & Qty',
			// 		alamat_toko: 'Alamat',
			// 		keterangan: 'Keterangan',
			// 		jaminput: 'Jam Buat'
			// 	},
			// 	templates: {
			// 		pilih: 'pilih-so-ambil-component',
			// 	},
			// 	sortIcon: {
			// 		base : 'fa',
			// 		is: 'fa-sort',
			// 		up: 'fa-sort-asc',
			// 		down: 'fa-sort-desc'
			// 	}
			// },
			/* pagination setting */
			pagestart: 0,
			pagecurrent: 1,
			itemsperpage: 50,
			pageend: 50,
			prevButtonIcon: '<i class="fa fa-chevron-left"></i>',
			nextButtonIcon: '<i class="fa fa-chevron-right"></i>',
			/* sort setting */
			currentSort: 'name',
			currentSortDir: 'asc',
			/* table column setting */
			activeColumn: {},
			new_columns: [
				{ name: 'type', order: 1 },
				{ name: 'code', order: 1 },
				{ name: 'ledgername', order: 1 },
				{ name: 'tanggal', order: 1 },
				{ name: 'delivery_time', order: 1 },
				{ name: 'sm_no', order: 1 },
				{ name: 'so_no', order: 1 },
				{ name: 'item_code', order: 1 },
				{ name: 'alamat_toko', order: 1 },
				{ name: 'keterangan', order: 1 },
				// { name: 'jaminput', order: 1 },
				// { name: 'debt_code', order: 1 },
			],
			colTitles: {
				'type': 'Type',
				'code': 'Ledger',
				'ledgername': 'Customer',
				'tanggal': 'Tgl. SO / SM',
				'delivery_time': 'Tgl. Ambil',
				'sm_no': 'No. SM',
				'so_no': 'No. SO',
				'item_code': 'Item & QTY',
				'alamat_toko': 'Alamat',
				'keterangan': 'Keterangan',
				// 'jaminput': 'Jam Buat',
				// 'debt_code': 'Dect Code'
			},
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 8640000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
			v_search_so: ''
		}
	},
	methods: {
		getOfficeCode() {
			axios.post(this.action, {
				case: 'getOfficeCode',
				username: 'rachmadi'
			}).then(response => {
				this.officeCode = response.data.dt_kd_offifce[0];
			});
		},
		add_SO() {
			if (this.checkedSO.length > 0) {
				// if(this.v_office != this.checkedSO[0].cabang && this.v_office != this.v_office_current) {
				// 	console.log(
				// 		"cabang asal :"+this.v_office_current+","+
				// 		"cabang pilih :"+this.v_office+","+
				// 		"cabang SO :"+this.checkedSO[0].cabang+","+
				// 		"hasil : "+"pengalihan");
				// 	if(this.checkedSO.length > 1) {
				// 		swal.fire({
				// 			title: "Gagal Simpan SO",
				// 			text: "Asal SO dan Asal Gudang Berbeda Silahkan Pilih Hanya Satu SO",
				// 			icon: "error",
				// 		}).then(function (isConfirm) {
				// 			if (isConfirm) {
				// 				console.log('gagal boss');
				// 			}
				// 		});
				// 	} else {
				// 		this.saveSOpengalihan();
				// 	}
				// } else if(this.v_office != this.checkedSO[0].cabang && this.v_office == this.v_office_current) {
				// 	console.log(
				// 		"cabang asal :"+this.v_office_current+","+
				// 		"cabang pilih :"+this.v_office+","+
				// 		"cabang SO :"+this.checkedSO[0].cabang+","+
				// 		"hasil : "+"simpan DP 1");
				// 	this.saveSO();
				// } else if(this.v_office == this.checkedSO[0].cabang) {
				// 	console.log(
				// 		"cabang asal :"+this.v_office_current+","+
				// 		"cabang pilih :"+this.v_office+","+
				// 		"cabang SO :"+this.checkedSO[0].cabang+","+
				// 		"hasil : "+"simpan DP 2");
				// 	this.saveSO();
				// }
				this.saveSO();
			} else {
				swal.fire({
					title: "Gagal Simpan SO",
					text: "Silahkan Pilih SO Terlebih Dahulu",
					icon: "error",
				}).then(function (isConfirm) {
					if (isConfirm) {
						console.log('gagal boss');
					}
				});
			}
		},
		saveSO() {
			axios.post(this.action, {
				case: 'saveDistributionPlanAmbil',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
			}).then(response => {
				// console.log(response.data);
				EventBus.$emit('confirmPrice_ambil', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				this.checkedSO = [];
			});
		},
		saveSOpengalihan() {
			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
			}).then(response => {
				// console.log(response.data);
				// console.log("pengalihan");
				EventBus.$emit('confirmPrice_ambil_pengalihan', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				this.checkedSO = [];
			});
		},
		pagination(index) {
			//This calculates the range of data to show depending on the selected page
			if (index >= 0 && index <= this.totalpages) {
				this.pagecurrent = index;
				this.pagestart = (this.itemsperpage * index) - this.itemsperpage;
				this.pageend = (this.itemsperpage * index);
			}
		},
		getNow() {
			const today = new Date();
			const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
			const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			const dateTime = date + ' ' + time;
			this.v_tgl_perintah_kirim = dateTime;
		}
	},
	created() {
		this.getOfficeCode();
		EventBus.$on('addItemSOambil', (data) => {
			this.checkedSO = data;
		});
		this.getNow();
	},
	computed: {
		filterListST() {
			let filterST = this.v_search_so

			let result = this.data.filter(function (e) {
				let filtered = true
				if (filterST && filterST.length > 0) {
					filtered = e.so_no.includes(filterST)
				}
				return filtered;

			}).sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});

			return result;
		},
		totalpages() {
			//This calculates the amount of pages
			return Math.ceil(this.data.length / this.itemsperpage);
		},
		sortedData: function () {
			return this.data.sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});
		}
	},
	watch: {
		data: function () {
			this.pagecurrent = 1;
			this.pagestart = 0;
			this.pageend = this.itemsperpage;
		},
	}

});

Vue.component('pilih-so-component', {

	props: ["data", "index", "column"],
	// template: `<input type="checkbox" :value="data" id="data.so_no" v-model="checkedSO" @change="check($event)">`,
	template: `<input type="checkbox" v-bind:id="data.so_no" v-bind:value="data" v-model="checkedSO" @change="check($event)">`,
	data() {
		return {
			all_data: [],
			checkedSO: []
		}
	},
	methods: {
		check: function (data) {
			this.checkedSO.push(data);
			EventBus.$emit('addItemSO', this.checkedSO);
		}
	},
	created() {

	},
	computed: {

	},
	watch: {

	}

});

Vue.component('pilih-so-ambil-component', {

	props: ["data", "index", "column"],
	template: `<input type="checkbox" :value="data" id="data.so_no" v-model="checkedSO" @change="check($event)">`,
	data() {
		return {
			checkedSO: []
		}
	},
	methods: {
		check: function (e) {
			EventBus.$emit('addItemSOambil', this.checkedSO);
		}
	},
	created() {
	},
	computed: {

	},

});

Vue.component('distribution_plan_component', {

	template: `
		<div>
			<div class="row" v-if="view_list_plan" style="">
				<div class="col-md-12">
					<tabs_distribution_plan_component>
						<tab_distribution_plan_component v-for="(dt,i) in tabs" :key="i" :name="dt.name" :icon="dt.icon" :selected="i === 0 ? 'true' : ''">
							<div v-if="i == 0">
								<div class="lds-facebook" v-if="loading"><div></div><div></div><div></div></div>
								<dis-plan-kirim-component :data="dis_plan_list_kirim" :username="username" ></dis-plan-kirim-component>
							</div>
							<div v-if="i == 1">
								<div class="lds-facebook" v-if="loading"><div></div><div></div><div></div></div>
								<dis-plan-ambil-component :data="dis_plan_list_ambil" :username="username"></dis-plan-ambil-component>
							</div>
							<div v-if="i == 2">
								<daf_outstanding_component :username="username"></daf_outstanding_component>
							</div>
							<div v-if="i == 3">
								<daf_force_component :username="username" :usergroup="usergroup"></daf_force_component>
							</div>
						</tab_distribution_plan_component>
					</tabs_distribution_plan_component>
					<modal-proses-dp-comp :username="username"></modal-proses-dp-comp>
				</div>
			</div>
			<div v-if="view_dp_plan">
				<div class="row">
					<div class="col-md-10">
						<h4>Rencana Kirim</h4>
					</div>
					<div class="col-md-2" style="text-align: end;">
						<button type="button" class="btn btn-xm btn-danger" @click="btn_list_plan">Kembali</button>
					</div>
				</div>
				</br></br>
				<div>
					<div class="row">
						<div class="col-md-10" style="margin-left: 80px; text-transform: capitalize;">
							<table class="table table-hover table-responsive table-striped">
								<thead>
									<tr>
										<th class="active">NO. LEDGER</th>
										<th class="active">NAMA TOKO</th>
										<th class="active">NO. SM</th>
										<th class="active">NO. SO</th>
										<th class="active">ITEM</th>
										<th class="active">TAMBAH/KURANG QTY</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="(dt,i) in data.item" :key="i">
										<td>{{ dt.code }}</td>
										<td>{{ dt.name }}</td>
										<td v-if="dt.type === 'SM'">{{ dt.no }}</td>
										<td v-else></td>
										<td>{{ dt.so_no }}</td>
										<td>{{ dt.item_code }}</td>
										<td>{{ dt.item_qty2 }}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					</br>
					<form style="margin-left: 80px; margin-right: 100px;" @submit.prevent="updateDP">
						<div class="form-group">
							<label class="col-form-label">Pilih Truck</label>
							<div class="row">
								<div class="col-sm-4">
									<label>
										Truck Operasional &nbsp;&nbsp;
										<input type="radio" id="truck_dalam" v-model="v_pilih_truck" value="truck_dalam">
									</label>
								</div>
								<div class="col-sm-4">
									<label>
										Truck Luar &nbsp;&nbsp;
										<input type="radio" id="truck_luar" v-model="v_pilih_truck" value="truck_luar">
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">No. Plat Truck</label>
							<v-select style="text-transform: capitalize" label="nopol" :options="dt_truck" v-model="v_nopol_truck" :reduce="dt_truck=>dt_truck.nopol" v-if="v_pilih_truck === 'truck_dalam'" @input="getSupirByNopol" required></v-select>
							<input class="form-control" type="text" v-model="v_nopol_truck" v-else required/>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Nama Supir</label>
							<v-select style="text-transform: capitalize" label="sopir" :options="dt_supir" v-model="v_sopir_name" v-if="v_pilih_truck === 'truck_dalam'" @input="getSupirBySupir" required></v-select>
							<input class="form-control" type="text" v-model="v_sopir_name" v-else required/>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Pilih Gudang</label>
							<v-select style="text-transform: capitalize" label="wh_code" :options="dt_gudang" v-model="v_gudang" :reduce="dt_gudang=>dt_gudang.wh_code" required></v-select>
						</div>
						<div class="form-group" style="display: none;">
							<label for="recipient-name" class="col-form-label">Tanggal Kirim</label>
							<vuejs-datepicker
								v-model="v_tgl_kirim"
								:format="DatePickerFormatb"
								:disabledDates="disabledDates"
								:bootstrap-styling="true"
								:placeholder="holderb"
								required
								style="width: 290px;"
								@input="countDate"
								required>
							</vuejs-datepicker>
						</div>
						<div class="form-group" v-if="v_totalPengiriman > 3">
							<label for="recipient-name" class="col-form-label">Alasan</label>
							<textarea class="form-control" id="exampleFormControlTextarea1" rows="3" v-model="v_alasan" style="resize: none;" required="required"></textarea>
						</div>
						<div class="form-group" v-else>
							
						</div>
						<div class="form-group" v-if="v_pilih_truck === 'truck_dalam'">
							<label for="recipient-name" class="col-form-label">Pilih Ritase</label>
							<select class="form-control" id="ritaseplan" name="ritaseplan" v-model="v_ritase" :value="truk_luar">
								<option v-for="option in options_ritase" v-bind:value="option.value">
									{{ option.text }}
								</option>
							</select>
						</div>
						<div class="form-group" v-else>
							
						</div>
						</br></br></br>
						<div class="row">
							<div class="col-md-11" style="text-align: end; margin-right: -20px;">
								<button type="submit" class="btn btn-primary" :disabled='isDisabled'>Simpan & Cetak DO</button>
							</div>
							<div class="col-md-1" style="text-align: end;">
								<button type="button" class="btn btn-danger" @click="btn_list_plan">Batalkan</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div v-if="view_dp_plan_ambil">
				<div class="row">
					<div class="col-md-10">
						<h4>Rencana Ambil</h4>
					</div>
					<div class="col-md-2" style="text-align: end;">
						<button type="button" class="btn btn-xm btn-danger" @click="btn_list_plan">Kembali</button>
					</div>
				</div>
				</br></br>
				<div>
					<div class="row">
						<div class="col-md-10" style="margin-left: 80px; text-transform: capitalize;">
							<table class="table table-hover table-responsive table-striped">
								<thead>
									<tr>
										<th class="active">NO. LEDGER</th>
										<th class="active">NAMA TOKO</th>
										<th class="active">NO. SM</th>
										<th class="active">NO. SO</th>
										<th class="active">ITEM</th>
										<th class="active">TAMBAH/KURANG QTY</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="(dt,i) in data_ambil.item" :key="i">
										<td>{{ dt.code }}</td>
										<td>{{ dt.name }}</td>
										<td v-if="dt.type === 'SM'">{{ dt.no }}</td>
										<td v-else></td>
										<td>{{ dt.so_no }}</td>
										<td>{{ dt.item_code }}</td>
										<td>{{ dt.item_qty2 }}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				</br>
				<div>
					<form style="margin-left: 80px; margin-right: 100px;" @submit.prevent="updateDPAmbil">
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">No. Plat Truck</label>
							<input class="form-control" type="text" v-model="v_nopol_truck" required="required"/>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Nama Supir</label>
							<input class="form-control" type="text" v-model="v_sopir_name" required="required"/>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Kuantiti</label>
							<input class="form-control" type="text" v-model="v_kuantiti" disabled/>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Pilih Gudang</label>
							<v-select style="text-transform: capitalize" label="wh_code" :options="dt_gudang" v-model="v_gudang" :reduce="dt_gudang=>dt_gudang.wh_code" required="required"></v-select>
						</div>
						</br></br></br>
						<div class="row">
							<div class="col-md-11" style="text-align: end; margin-right: -20px;">
								<button type="submit" class="btn btn-primary" :disabled='isDisabled'>Simpan & Cetak DO</button>
							</div>
							<div class="col-md-1" style="text-align: end;">
								<button type="button" class="btn btn-danger" @click="btn_list_plan">Batalkan</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div v-if="view_dp_plan_edit_kirim">
				<div class="row">
					<div class="col-md-10">
						<h4>Transfer DO Kirim</h4>
					</div>
					<div class="col-md-2" style="text-align: end;">
						<button type="button" class="btn btn-xm btn-danger" @click="btn_list_plan">Kembali</button>
					</div>
				</div>
				</br></br>
				<div>
					<div class="row">
						<div class="col-md-10" style="margin-left: 80px; text-transform: capitalize;">
							<table class="table table-hover table-responsive table-striped">
								<thead>
									<tr>
										<th class="active">NO. LEDGER</th>
										<th class="active">NAMA TOKO</th>
										<th class="active">NO. SM</th>
										<th class="active">NO. SO</th>
										<th class="active">ITEM</th>
										<th class="active">TAMBAH/KURANG QTY</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="(dt,i) in detail_dp_kirim.item" :key="i">
										<td>{{ dt.code }}</td>
										<td>{{ dt.name }}</td>
										<td v-if="dt.type === 'SM'">{{ dt.no }}</td>
										<td v-else></td>
										<td>{{ dt.so_no }}</td>
										<td>{{ dt.item_code }}</td>
										<td>{{ dt.item_qty2 }}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					</br>
					<form style="margin-left: 80px; margin-right: 100px;" @submit.prevent="updateDetailDPKirim">
						<div class="form-group">
							<label class="col-form-label">Pilih Truck</label>
							<div class="row">
								<div class="col-sm-4">
									<label>
										Truck Operasional &nbsp;&nbsp;
										<input type="radio" id="truck_dalam" v-model="v_pilih_truck" value="truck_dalam">
									</label>
								</div>
								<div class="col-sm-4">
									<label>
										Truck Luar &nbsp;&nbsp;
										<input type="radio" id="truck_luar" v-model="v_pilih_truck" value="truck_luar">
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">No. Plat Truck</label>
							<v-select style="text-transform: capitalize" label="nopol" :options="dt_truck" v-model="v_nopol_truck" :key="v_nopol_truck" :reduce="dt_truck=>dt_truck.nopol" v-if="v_pilih_truck === 'truck_dalam'" required></v-select>
							<input class="form-control" type="text" v-model="v_nopol_truck" v-else required="required"/>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Nama Supir</label>
							<v-select style="text-transform: capitalize" label="sopir" :options="dt_supir" v-model="v_sopir_name" :key="v_sopir_name" :reduce="dt_sopir=>dt_sopir.sopir" v-if="v_pilih_truck === 'truck_dalam'" required></v-select>
							<input class="form-control" type="text" v-model="v_sopir_name" v-else required="required"/>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Pilih Gudang</label>
							<v-select style="text-transform: capitalize" label="wh_code" :options="dt_gudang" v-model="v_gudang" :key="v_gudang" :reduce="dt_gudang=>dt_gudang.wh_code" required="required" @input="getNoTransfer(v_gudang)"></v-select>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Pilih No. Transfer</label>
							<v-select style="text-transform: capitalize" label="ref_no" :options="no_transfer" v-model="v_notransfer" :key="v_notransfer" :reduce="no_transfer=>no_transfer.ref_no" required="required"></v-select>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Tanggal Terima Transfer</label>
							<vuejs-datepicker
								v-model="v_tgl_terima"
								:format="DatePickerFormatb"
								:bootstrap-styling="true"
								:placeholder="holderb"
								style="width: 290px;"
								required>
							</vuejs-datepicker>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Tanggal Kirim</label>
							<vuejs-datepicker
								v-model="v_tgl_kirim"
								:format="DatePickerFormatb"
								:bootstrap-styling="true"
								:placeholder="holderb"
								style="width: 290px;"
								@input="countDate"
								required>
							</vuejs-datepicker>
						</div>
						<div class="form-group" v-if="v_totalPengiriman > 3">
							<label for="recipient-name" class="col-form-label">Alasan</label>
							<textarea class="form-control" id="exampleFormControlTextarea1" rows="3" v-model="v_alasan" style="resize: none;" required="required"></textarea>
						</div>
						<div class="form-group" v-else>
							
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Catatan Tambahan (Jika Ada)</label>
							<textarea class="form-control" id="exampleFormControlTextarea1" rows="3" v-model="v_catatan_to" style="resize: none;"></textarea>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Pilih Ritase</label>
							<select class="form-control" id="ritaseplan" name="ritaseplan" v-model="v_ritase" required="required">
								<option v-for="option in options_ritase" v-bind:value="option.value">
									{{ option.text }}
								</option>
							</select>
						</div>
						</br></br></br>
						<div class="row">
							<div class="col-md-11" style="text-align: end; margin-right: -20px;">
								<button type="submit" class="btn btn-primary" :disabled='isDisabled'>Simpan & Cetak DO</button>
							</div>
							<div class="col-md-1" style="text-align: end;">
								<button type="button" class="btn btn-danger" @click="btn_list_plan">Batalkan</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div v-if="view_dp_plan_edit_ambil">
				<div class="row">
					<div class="col-md-10">
						<h4>Edit DO Ambil</h4>
					</div>
					<div class="col-md-2" style="text-align: end;">
						<button type="button" class="btn btn-xm btn-danger" @click="btn_list_plan">Kembali</button>
					</div>
				</div>
				</br></br>
				<div>
					<form style="margin-left: 80px; margin-right: 100px;" @submit.prevent="updateDetailDPAmbil">
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">No. Plat Truck</label>
							<input class="form-control" type="text" v-model="v_nopol_truck" required="required"/>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Nama Supir</label>
							<input class="form-control" type="text" v-model="v_sopir_name" required="required"/>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Kuantiti</label>
							<input class="form-control" type="text" v-model="v_kuantiti" disabled/>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Pilih Gudang</label>
							<v-select style="text-transform: capitalize" label="wh_code" :options="dt_gudang" v-model="v_gudang" :key="v_gudang" :reduce="dt_gudang=>dt_gudang.wh_code" required="required"></v-select>
						</div>
						</br></br></br>
						<div class="row">
							<div class="col-md-11" style="text-align: end; margin-right: -20px;">
								<button type="submit" class="btn btn-primary">Simpan & Cetak DO</button>
							</div>
							<div class="col-md-1" style="text-align: end;">
								<button type="button" class="btn btn-danger" @click="btn_list_plan">Batalkan</button>
							</div>
						</div>
					</form>	
				</div>
			</div>
			<div v-if="view_dp_plan_transfer_kirim">
				<div class="row">
					<div class="col-md-10">
						<h4>Transfer DO Kirim</h4>
					</div>
					<div class="col-md-2" style="text-align: end;">
						<button type="button" class="btn btn-xm btn-danger" @click="btn_list_plan">Kembali</button>
					</div>
				</div>
				</br></br>
				<div>
					<div class="row">
						<div class="col-md-10" style="margin-left: 80px; text-transform: capitalize;">
							<table class="table table-hover table-responsive table-striped">
								<thead>
									<tr>
										<th class="active">NO. LEDGER</th>
										<th class="active">NAMA TOKO</th>
										<th class="active">NO. SM</th>
										<th class="active">NO. SO</th>
										<th class="active">ITEM</th>
										<th class="active">TAMBAH/KURANG QTY</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="(dt,i) in detail_dp_kirim.item" :key="i">
										<td>{{ dt.code }}</td>
										<td>{{ dt.name }}</td>
										<td v-if="dt.type === 'SM'">{{ dt.no }}</td>
										<td v-else></td>
										<td>{{ dt.so_no }}</td>
										<td>{{ dt.item_code }}</td>
										<td>{{ dt.item_qty2 }}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					</br>
					<form style="margin-left: 80px; margin-right: 100px;" @submit.prevent="updateProsesTransferDPKirim">
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">No. Plat Truck</label>
							<input class="form-control" type="text" v-model="v_nopol_truck" disabled />
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Nama Supir</label>
							<input class="form-control" type="text" v-model="v_sopir_name" disabled />
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Pilih Gudang</label>
							<v-select style="text-transform: capitalize" label="wh_code" :options="dt_gudang" v-model="v_gudang" :key="v_gudang" :reduce="dt_gudang=>dt_gudang.wh_code" required="required"></v-select>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Pilih No. Transfer</label>
							<v-select style="text-transform: capitalize" label="ref_no" :options="no_transfer" v-model="v_notransfer" :key="v_notransfer" :reduce="no_transfer=>no_transfer.ref_no" required="required"></v-select>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Tanggal Kirim</label>
							<vuejs-datepicker
								v-model="v_tgl_kirim"
								:format="DatePickerFormatb"
								:disabledDates="disabledDates"
								:bootstrap-styling="true"
								:placeholder="holderb"
								style="width: 290px;"
								@input="countDate"
								required>
							</vuejs-datepicker>
						</div>
						<div class="form-group" v-if="v_totalPengiriman > 3">
							<label for="recipient-name" class="col-form-label">Alasan</label>
							<textarea class="form-control" id="exampleFormControlTextarea1" rows="3" v-model="v_alasan" style="resize: none;" required="required"></textarea>
						</div>
						<div class="form-group" v-else>
							
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Pilih Ritase</label>
							<select class="form-control" id="ritaseplan" name="ritaseplan" v-model="v_ritase" required="required">
								<option v-for="option in options_ritase" v-bind:value="option.value">
									{{ option.text }}
								</option>
							</select>
						</div>
						</br></br></br>
						<div class="row">
							<div class="col-md-11" style="text-align: end; margin-right: -20px;">
								<button type="submit" class="btn btn-primary">Simpan & Cetak DO</button>
							</div>
							<div class="col-md-1" style="text-align: end;">
								<button type="button" class="btn btn-danger" @click="btn_list_plan">Batalkan</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div v-if="view_dp_plan_transfer_terima">
				<div class="row">
					<div class="col-md-10">
						<h4>Buat DO Pengalihan & Input Terima Konsumen</h4>
					</div>
					<div class="col-md-2" style="text-align: end;">
						<button type="button" class="btn btn-xm btn-danger" @click="btn_list_plan">Kembali</button>
					</div>
				</div>
				</br></br>
				<div>
					<div class="row">
						<div class="col-md-10" style="margin-left: 80px; text-transform: capitalize;">
							<table class="table table-hover table-responsive table-striped">
								<thead>
									<tr>
										<th class="active">NO. LEDGER</th>
										<th class="active">NAMA TOKO</th>
										<th class="active">NO. SM</th>
										<th class="active">NO. SO</th>
										<th class="active">ITEM</th>
										<th class="active">TAMBAH/KURANG QTY</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="(dt,i) in detail_dp_transfer.item" :key="i">
										<td>{{ dt.code }}</td>
										<td>{{ dt.name }}</td>
										<td v-if="dt.type === 'SM'">{{ dt.no }}</td>
										<td v-else></td>
										<td>{{ dt.so_no }}</td>
										<td>{{ dt.item_code }}</td>
										<td>{{ dt.item_qty2 }}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					</br>
					<form style="margin-left: 80px; margin-right: 100px;" @submit.prevent="updateProsesTransferDPKirimx">
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">No. Plat Truck</label>
							<input class="form-control" type="text" v-model="v_nopol_truck" disabled />
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Nama Supir</label>
							<input class="form-control" type="text" v-model="v_sopir_name" disabled />
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Tanggal Terima</label>
							<vuejs-datepicker
								v-model="v_tgl_terima"
								:format="DatePickerFormatb"
								:disabledDates="disabledDates"
								:bootstrap-styling="true"
								:placeholder="holderb"
								style="width: 290px;"
								@input="countDate"
								required>
							</vuejs-datepicker>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Catatan Tambahan (Jika Ada)</label>
							<textarea class="form-control" id="exampleFormControlTextarea1" rows="3" v-model="v_catatan_to" style="resize: none;"></textarea>
						</div>
						</br></br></br>
						<div class="row">
							<div class="col-md-11" style="text-align: end; margin-right: -20px;">
								<button type="submit" class="btn btn-primary">Simpan & Cetak DO</button>
							</div>
							<div class="col-md-1" style="text-align: end;">
								<button type="button" class="btn btn-danger" @click="btn_list_plan">Batalkan</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div v-if="view_pengiriman_kirim">
				<div class="row">
					<div class="col-md-10">
						<h4>Buat DO Kirim</h4>
					</div>
					<div class="col-md-2" style="text-align: end;">
						<button type="button" class="btn btn-xm btn-danger" @click="btn_list_plan">Kembali</button>
					</div>
				</div>
				</br></br>
				<div class="row">
					<div class="col-md-12">
						<table class="table table-hover table-responsive table-striped">
							<thead>
								<tr>
									<th class="active">NO. DP</th>
									<th class="active">TGL. KIRIM</th>
									<th class="active">GUDANG ASAL</th>
									<th class="active">TRUCK</th>
									<th class="active">SUPIR</th>
								</tr>
							</thead>
							<tbody>
								<tr v-for="(dt,i) in detail_kirim_byid">
									<td>{{ dt.no_dp }}</td>
									<td>{{ dt.tgl_kirim2 }}</td>
									<td>{{ dt.wh_code }}</td>
									<td>{{ dt.no_pol }}</td>
									<td>{{ dt.sopir }}</td>
								</tr>
							</tbody>
						</table>
					</div>
					</br></br></br></br>
					<div class="col-md-12">
						<table class="table table-hover table-responsive table-striped">
							<thead>
								<tr>
									<th class="active">No</th>
									<th class="active">No SO</th>
									<th class="active">No Ledger</th>
									<th class="active">Nama Toko</th>
									<th class="active">Item</th>
									<th class="active">Qty</th>
									<th class="active">Tgl. Kirim</th>
									<th class="active">&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<tr v-for="(dta,i) in detail_kirim_byid_item">
									<td>{{ i+1 }}</td>
									<td>{{ dta.so_no }}</td>
									<td>{{ dta.code }}</td>
									<td>{{ dta.name }}</td>
									<td>{{ dta.item_code }}</td>
									<td>{{ dta.item_qty2 }}</td>
									<td>{{ dta.tanggal }}</td>
									<td>
										<button name="btn-load" class="btn btn-info btn-flat btn-xs" @click="cetak_do(dta)"> <i class="fa fa-print"></i> CETAK DO</button>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div v-if="view_pengiriman_ambil">
				<div class="row">
					<div class="col-md-10">
						<h4>Buat DO Ambil</h4>
					</div>
					<div class="col-md-2" style="text-align: end;">
						<button type="button" class="btn btn-xm btn-danger" @click="btn_list_plan">Kembali</button>
					</div>
				</div>
				</br></br>
				<div class="row">
					<div class="col-md-12">
						<table class="table table-hover table-responsive table-striped">
							<thead>
								<tr>
									<th class="active">NO. DP</th>
									<th class="active">TGL. KIRIM</th>
									<th class="active">GUDANG ASAL</th>
									<th class="active">TRUCK</th>
									<th class="active">SUPIR</th>
								</tr>
							</thead>
							<tbody>
								<tr v-for="(dt,i) in detail_ambil_byid">
									<td>{{ dt.no_dp }}</td>
									<td>{{ dt.tgl_perintah2 }}</td>
									<td>{{ dt.wh_code }}</td>
									<td>{{ dt.no_pol }}</td>
									<td>{{ dt.sopir }}</td>
								</tr>
							</tbody>
						</table>
					</div>
					</br></br></br></br>
					<div class="col-md-12">
						<table class="table table-hover table-responsive table-striped">
							<thead>
								<tr>
									<th class="active">No</th>
									<th class="active">No SO</th>
									<th class="active">No Ledger</th>
									<th class="active">Nama Toko</th>
									<th class="active">Item</th>
									<th class="active">Qty</th>
									<th class="active">Tgl. Kirim</th>
									<th class="active">&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<tr v-for="(dtaa,i) in detail_ambil_byid_item">
									<td>{{ i+1 }}</td>
									<td>{{ dtaa.so_no }}</td>
									<td>{{ dtaa.code }}</td>
									<td>{{ dtaa.name }}</td>
									<td>{{ dtaa.item_code }}</td>
									<td>{{ dtaa.item_qty2 }}</td>
									<td>{{ dtaa.tanggal }}</td>
									<td>
										<button name="btn-load" class="btn btn-info btn-flat btn-xs" @click="cetak_do(dtaa)"> <i class="fa fa-print"></i> CETAK DO</button>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div v-if="view_pengiriman_transfer">
				<div class="row">
					<div class="col-md-10">
						<h4>Buat DO TRANSFER</h4>
					</div>
					<div class="col-md-2" style="text-align: end;">
						<button type="button" class="btn btn-xm btn-danger" @click="btn_list_plan">Kembali</button>
					</div>
				</div>
				</br></br>
				<div class="row">
					<div class="col-md-12">
						<table class="table table-hover table-responsive table-striped">
							<thead>
								<tr>
									<th class="active">NO. DP</th>
									<th class="active">TGL. KIRIM</th>
									<th class="active">GUDANG ASAL</th>
									<th class="active">TRUCK</th>
									<th class="active">SUPIR</th>
									<th class="active">NO. TO</th>
									<th class="active">CATATAN TO</th>
									<th class="active">TGL. TERIMA</th>
								</tr>
							</thead>
							<tbody>
								<tr v-for="(dt,i) in detail_transfer_byid">
									<td>{{ dt.no_dp }}</td>
									<td>{{ dt.tgl_kirim2 }}</td>
									<td>{{ dt.wh_code }}</td>
									<td>{{ dt.no_pol }}</td>
									<td>{{ dt.sopir }}</td>
									<td>{{ dt.no_to }}</td>
									<td>{{ dt.catatan_to }}</td>
									<td>{{ dt.tgl_terima }}</td>
								</tr>
							</tbody>
						</table>
					</div>
					</br></br></br></br>
					<div class="col-md-12">
						<table class="table table-hover table-responsive table-striped">
							<thead>
								<tr>
									<th class="active">No</th>
									<th class="active">No SO</th>
									<th class="active">No Ledger</th>
									<th class="active">Nama Toko</th>
									<th class="active">Item</th>
									<th class="active">Qty</th>
									<th class="active">Tgl. Kirim</th>
									<th class="active">&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<tr v-for="(data,i) in detail_transfer_byid_item">
									<td>{{ i+1 }}</td>
									<td>{{ data.so_no }}</td>
									<td>{{ data.code }}</td>
									<td>{{ data.name }}</td>
									<td>{{ data.item_code }}</td>
									<td>{{ data.item_qty2 }}</td>
									<td>{{ data.tanggal }}</td>
									<td v-if="data.so_no !== xxxno_dp">
										<button name="btn-load" class="btn btn-info btn-flat btn-xs" @click="cetak_do(data)"> <i class="fa fa-print"></i> CETAK DO</button>
									</td>
									<td v-else></td>
									<span style="display: none;">{{ xxxno_dp = data.so_no }}</span>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	`,
	components: {
		vuejsDatepicker
	},
	data() {
		return {
			loading: false,
			tabs: [
				{
					name: 'KIRIM',
					icon: '<i class="fa fa-print"></i>',
					counter: 0,
				},
				{
					name: 'AMBIL',
					icon: '<i class="fa fa-print"></i>',
					counter: 1,
				},
				{
					name: 'OUTSTANDING',
					icon: '<i class="fa fa-print"></i>',
					counter: 2,
				},
				{
					name: 'REQUEST CLOSE',
					icon: '<i class="fa fa-print"></i>',
					counter: 3,
				}
			],
			options_ritase: [
				{ text: '...', value: '0' },
				{ text: '1', value: '1' },
				{ text: '2', value: '2' },
				{ text: '3', value: '3' },
				{ text: '4', value: '4' },
				{ text: '5', value: '5' },
			],
			view_list_plan: true,
			view_dp_plan: false,
			view_dp_plan_ambil: false,
			view_dp_plan_edit_kirim: false,
			view_dp_plan_edit_ambil: false,
			view_dp_plan_transfer_kirim: false,
			view_dp_plan_transfer_terima: false,
			view_pengiriman_kirim: false,
			view_pengiriman_ambil: false,
			view_pengiriman_transfer: false,
			dis_plan_list_kirim: [],
			dis_plan_list_ambil: [],
			dis_plan_list_transfer: [],
			data: [],
			show: false,
			id: 0,
			data: [],
			data_ambil: [],
			v_gd_start: '',
			v_nama_supir: '',
			v_no_plat: '',
			tgl_kirim: '',
			v_alasan: '',
			v_qty: '',
			v_kuantiti: '',
			dt_truck: [],
			dt_supir: [],
			dt_gudang: [],
			v_nopol_truck: "",
			v_sopir_name: "",
			v_totalPengiriman: '',
			v_tgl_kirim: '',
			v_pilih_truck: '',
			v_gudang: '',
			v_catatan: '',
			v_catatan_to: '',
			v_ritase: '0',
			v_notransfer: '',
			v_tgl_terima: '',
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 8640000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
			detail_dp_kirim: [],
			detail_dp_ambil: [],
			no_transfer: [],
			detail_dp_transfer: [],
			detail_kirim_byid: [],
			detail_kirim_byid_item: [],
			detail_ambil_byid: [],
			detail_ambil_byid_item: [],
			detail_transfer_byid: [],
			detail_transfer_byid_item: [],
			xno_dp: "0",
			xxno_dp: "00",
			xxxno_dp: "000",
			terms: true
		}
	},
	props: ["username", "where", "usergroup", "loc"],
	created() {
		this.getDisPlanListKirim();
		this.getDisPlanListAmbil();
		this.getDisPlanListTransfer();

		EventBus.$on("open_modal_proses_kirim", (data) => {
			this.v_nopol_truck = "";
			this.v_sopir_name = "";
			this.v_gudang = "";
			this.v_tgl_kirim = "";
			this.v_alasan = "";
			this.v_ritase = "0";
			this.data = "";

			this.data = data;
			this.getTruck();
			this.getSopir();
			this.getGudang();

			this.view_dp_plan = true;
			this.view_dp_plan_ambil = false;
			this.view_dp_plan_edit_kirim = false;
			this.view_dp_plan_edit_ambil = false;
			this.view_dp_plan_transfer_kirim = false;
			this.view_dp_plan_transfer_terima = false;
			this.view_list_plan = false;
			this.view_pengiriman_kirim = false;
			this.view_pengiriman_ambil = false;
			this.view_pengiriman_transfer = false;
		});

		EventBus.$on("open_modal_edit_kirim", (data) => {
			this.getGudang();
			this.getTruck();
			this.getSopir();

			this.getDisPlanKirimByDP(data.no_dp);

			this.view_dp_plan = false;
			this.view_dp_plan_ambil = false;
			this.view_dp_plan_edit_kirim = true;
			this.view_dp_plan_edit_ambil = false;
			this.view_dp_plan_transfer_kirim = false;
			this.view_dp_plan_transfer_terima = false;
			this.view_list_plan = false;
			this.view_pengiriman_kirim = false;
			this.view_pengiriman_ambil = false;
			this.view_pengiriman_transfer = false;

		});

		EventBus.$on('open_modal_proses_ambil', (data) => {
			this.v_nopol_truck = "";
			this.v_sopir_name = "";
			this.v_gudang = "";
			this.v_tgl_kirim = "";
			this.v_alasan = "";
			this.v_ritase = '0';
			this.data = "";

			this.data_ambil = data;
			this.getTruck();
			this.getSopir();
			this.getGudang();

			console.log(this.data_ambil);

			this.view_dp_plan = false;
			this.view_dp_plan_ambil = true;
			this.view_dp_plan_edit_kirim = false;
			this.view_dp_plan_edit_ambil = false;
			this.view_dp_plan_transfer_kirim = false;
			this.view_dp_plan_transfer_terima = false;
			this.view_list_plan = false;
			this.view_pengiriman_kirim = false;
			this.view_pengiriman_ambil = false;
			this.view_pengiriman_transfer = false;
			this.v_kuantiti = this.data_ambil.item[0].item_qty;
		});

		EventBus.$on("open_modal_edit_ambil", (data) => {
			this.getGudang();

			this.getDisPlanAmbilByDP(data.no_dp);

			this.view_dp_plan = false;
			this.view_dp_plan_ambil = false;
			this.view_dp_plan_edit_kirim = false;
			this.view_dp_plan_edit_ambil = true;
			this.view_dp_plan_transfer_kirim = false;
			this.view_dp_plan_transfer_terima = false;
			this.view_list_plan = false;
			this.view_pengiriman_kirim = false;
			this.view_pengiriman_ambil = false;
			this.view_pengiriman_transfer = false;

		});

		EventBus.$on("open_modal_transfer_kirim", (data) => {
			this.getGudang();
			this.getNoTransfer(data.wh_code);
			this.getDisPlanKirimByDP(data.no_dp);

			this.view_dp_plan = false;
			this.view_dp_plan_ambil = false;
			this.view_dp_plan_edit_kirim = false;
			this.view_dp_plan_edit_ambil = false;
			this.view_dp_plan_transfer_kirim = true;
			this.view_dp_plan_transfer_terima = false;
			this.view_list_plan = false;
			this.view_pengiriman_kirim = false;
			this.view_pengiriman_ambil = false;
			this.view_pengiriman_transfer = false;

		});

		EventBus.$on("open_modal_proses_transfer", (data) => {
			this.getDisPlanTransferByDP(data.no_dp);

			this.view_dp_plan = false;
			this.view_dp_plan_ambil = false;
			this.view_dp_plan_edit_kirim = false;
			this.view_dp_plan_edit_ambil = false;
			this.view_dp_plan_transfer_kirim = false;
			this.view_dp_plan_transfer_terima = true;
			this.view_list_plan = false;
			this.view_pengiriman_kirim = false;
			this.view_pengiriman_ambil = false;

		});

		EventBus.$on("load_distribution_kirim", () => {
			this.getDisPlanListKirim();
			this.getDisPlanListAmbil();
		});

		EventBus.$on("load_distribution_ambil", () => {
			this.getDisPlanListKirim();
			this.getDisPlanListAmbil();
		});
	},
	methods: {
		btn_list_plan() {
			this.view_dp_plan = false;
			this.view_dp_plan_ambil = false;
			this.view_dp_plan_edit_kirim = false;
			this.view_dp_plan_edit_ambil = false;
			this.view_dp_plan_transfer_kirim = false;
			this.view_dp_plan_transfer_terima = false;
			this.view_list_plan = true;
			this.view_pengiriman_kirim = false;
			this.view_pengiriman_ambil = false;
			this.view_pengiriman_transfer = false;

			this.getDisPlanListKirim();
			this.getDisPlanListAmbil();

			this.xno_dp = "";
		},
		getDisPlanListKirim() {
			this.loading = true;
			axios.post(this.action, {
				case: 'getDisPlanListKirim',
				loc: this.loc
			}).then(response => {
				if (response.data != null) {
					this.dis_plan_list_kirim = response.data;
				} else {
					this.dis_plan_list_kirim = [];
				}
				this.loading = false;
			});
		},
		getDisPlanListAmbil() {
			this.loading = true;
			axios.post(this.action, {
				case: 'getDisPlanListAmbil',
			}).then(response => {
				if (response.data != null) {
					this.dis_plan_list_ambil = response.data;
				} else {
					this.dis_plan_list_ambil = [];
				}
				this.loading = false;
			});
		},
		getDisPlanListTransfer() {
			axios.post(this.action, {
				case: 'getDisPlanListTransfer',
			}).then(response => {
				this.dis_plan_list_transfer = response.data;
			});
		},
		getNoTransfer(wh_code) {
			axios.post(this.action, {
				case: "getNoTransfer",
				wh_code: wh_code
			}).then(response => {
				this.no_transfer = response.data;
			})
		},
		countDate() {
			var tgl_perintah = this.data.tgl_perintah;
			var tgl_kirim = moment(this.v_tgl_kirim).format("YYYY-MM-DD");

			dt1 = new Date(tgl_perintah);
			dt2 = new Date(tgl_kirim);

			this.v_totalPengiriman = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
		},
		getTruck() {
			axios.post(this.action, {
				case: "getTruck",
				where: this.where
			}).then(response => {
				this.dt_truck = response.data;
			})
		},
		getSopir() {
			axios.post(this.action, {
				case: "getSopir",
				where: this.where
			}).then(response => {
				this.dt_supir = response.data;
			})
		},
		getGudang() {
			axios.post(this.action, {
				case: "getGudang",
				username: 'rachmadi',
			}).then(response => {
				this.dt_gudang = response.data;
			})
		},
		updateDP() {
			// let v_xritase = '';

			// if (this.v_pilih_truck == 'truck_dalam') {
			// 	this.v_ritase = this.v_ritase
			// } else {
			// 	this.v_ritase = 0
			// }

			axios.post(this.action, {
				case: 'updateDistributionPlan',
				nopol_truck: this.v_nopol_truck,
				sopir_name: this.v_sopir_name,
				gudang: this.v_gudang,
				tgl_kirim: this.v_tgl_kirim,
				alasan: this.v_alasan,
				ritase: this.v_ritase,
				dt_so: this.data,
			}).then(response => {

				// console.log(response.data);

				this.v_nopol_truck = "";
				this.v_sopir_name = "";
				this.v_gudang = "";
				this.v_tgl_kirim = "";
				this.v_alasan = "";
				this.v_ritase = "";
				this.detail_kirim_byid = []
				// this.data = "";

				axios.post(this.action, {
					case: 'getDaftarPengirimanKirimByID',
					id_dt: this.data.id,
				}).then(response => {
					this.data = "";
					this.detail_kirim_byid = response.data
					this.detail_kirim_byid_item = response.data[0].item

					this.view_dp_plan = false;
					this.view_dp_plan_ambil = false;
					this.view_dp_plan_edit_kirim = false;
					this.view_dp_plan_edit_ambil = false;
					this.view_dp_plan_transfer_kirim = false;
					this.view_dp_plan_transfer_terima = false;
					this.view_list_plan = false;
					this.view_pengiriman_kirim = true;
					this.view_pengiriman_ambil = false;
					this.view_pengiriman_transfer = false;

					this.getDisPlanListKirim();
					this.getDisPlanListAmbil();

					console.log(this.detail_kirim_byid);
					console.log(this.detail_kirim_byid_item);
				});
			});
		},
		updateDPAmbil() {
			axios.post(this.action, {
				case: 'updateDistributionPlanAmbil',
				nopol_truck: this.v_nopol_truck,
				sopir_name: this.v_sopir_name,
				gudang: this.v_gudang,
				dt_so: this.data_ambil,
			}).then(response => {

				this.v_nopol_truck = "";
				this.v_sopir_name = "";
				this.v_gudang = "";
				// this.data_ambil = "";

				axios.post(this.action, {
					case: 'getDaftarPengirimanAmbilByID',
					id_dt: this.data_ambil.id
				}).then(response => {
					this.data = "";
					this.detail_ambil_byid = response.data
					this.detail_ambil_byid_item = response.data[0].item

					this.view_dp_plan = false;
					this.view_dp_plan_ambil = false;
					this.view_dp_plan_edit_kirim = false;
					this.view_dp_plan_edit_ambil = false;
					this.view_dp_plan_transfer_kirim = false;
					this.view_dp_plan_transfer_terima = false;
					this.view_list_plan = false;
					this.view_pengiriman_kirim = false;
					this.view_pengiriman_ambil = true;
					this.view_pengiriman_transfer = false;

					this.getDisPlanListKirim();
					this.getDisPlanListAmbil();

					console.log(this.detail_ambil_byid);
					console.log(this.detail_ambil_byid_item);
				});
			});
		},
		getDisPlanKirimByDP(no_dp) {
			axios.post(this.action, {
				case: "getDisPlanKirimByDP",
				no_dp: no_dp,
			}).then(response => {
				this.detail_dp_kirim = response.data[0];

				this.v_nopol_truck = this.detail_dp_kirim.no_pol;
				this.v_sopir_name = this.detail_dp_kirim.sopir;
				this.v_gudang = this.detail_dp_kirim.wh_code;
				this.v_tgl_kirim = this.detail_dp_kirim.tgl_kirim;
				this.v_ritase = this.detail_dp_kirim.ritase;
				this.v_alasan = this.detail_dp_kirim.catatan;
				this.data = this.detail_dp_kirim;
			})
		},
		getDisPlanAmbilByDP(no_dp) {
			axios.post(this.action, {
				case: "getDisPlanAmbilByDP",
				no_dp: no_dp,
			}).then(response => {
				this.detail_dp_ambil = response.data[0];

				this.v_nopol_truck = this.detail_dp_ambil.no_pol;
				this.v_sopir_name = this.detail_dp_ambil.sopir;
				this.v_gudang = this.detail_dp_ambil.wh_code;
				this.v_kuantiti = this.detail_dp_ambil.item[0].item_qty;
				this.data = this.detail_dp_ambil;
			})
		},
		getDisPlanTransferByDP(no_dp) {
			axios.post(this.action, {
				case: "getDisPlanTransferByDP",
				no_dp: no_dp,
			}).then(response => {
				this.detail_dp_transfer = response.data[0];

				this.v_nopol_truck = this.detail_dp_transfer.no_pol;
				this.v_sopir_name = this.detail_dp_transfer.sopir;
				this.v_gudang = this.detail_dp_transfer.wh_code;
				this.v_ritase = this.detail_dp_transfer.ritase;
				this.v_alasan = this.detail_dp_transfer.catatan_so;
				this.data = this.detail_dp_transfer;
			})
		},
		updateDetailDPKirim() {
			axios.post(this.action, {
				case: 'updateDistributionPlanDetail',
				nopol_truck: this.v_nopol_truck,
				sopir_name: this.v_sopir_name,
				gudang: this.v_gudang,
				no_to: this.v_notransfer,
				tgl_terima: this.v_tgl_terima,
				tgl_kirim: this.v_tgl_kirim,
				alasan: this.v_alasan,
				ritase: this.v_ritase,
				catatan_to: this.v_catatan_to,
				dt_so: this.data,
			}).then(response => {
				console.log(response.data);

				this.v_nopol_truck = "";
				this.v_sopir_name = "";
				this.v_tgl_terima = "";
				this.v_catatan_to = "";
				// this.data = "";

				axios.post(this.action, {
					case: 'getDaftarPengirimanTransferByID',
					id_dt: this.data.id,
				}).then(response => {
					this.data = "";
					this.detail_transfer_byid = response.data
					this.detail_transfer_byid_item = response.data[0].item

					this.view_dp_plan = false;
					this.view_dp_plan_ambil = false;
					this.view_dp_plan_edit_kirim = false;
					this.view_dp_plan_edit_ambil = false;
					this.view_dp_plan_transfer_kirim = false;
					this.view_dp_plan_transfer_terima = false;
					this.view_list_plan = false;
					this.view_pengiriman_kirim = false;
					this.view_pengiriman_ambil = false;
					this.view_pengiriman_transfer = true;

					this.getDisPlanListKirim();
					this.getDisPlanListAmbil();

					console.log(this.detail_transfer_byid);
					console.log(this.detail_transfer_byid_item);
				});
			});
		},
		updateDetailDPAmbil() {
			axios.post(this.action, {
				case: 'updateDistributionPlanAmbil',
				nopol_truck: this.v_nopol_truck,
				sopir_name: this.v_sopir_name,
				gudang: this.v_gudang,
				tgl_kirim: this.v_tgl_kirim,
				alasan: this.v_alasan,
				ritase: this.v_ritase,
				dt_so: this.data,
			}).then(response => {

				this.v_nopol_truck = "";
				this.v_sopir_name = "";
				this.v_gudang = "";
				this.v_tgl_kirim = "";
				this.v_alasan = "";
				this.v_ritase = "";
				this.data = "";

				this.view_dp_plan = false;
				this.view_dp_plan_ambil = false;
				this.view_dp_plan_edit_kirim = false;
				this.view_dp_plan_edit_ambil = false;
				this.view_dp_plan_transfer_kirim = false;
				this.view_dp_plan_transfer_terima = false;
				this.view_list_plan = true;
				this.view_pengiriman_kirim = false;
				this.view_pengiriman_ambil = false;
				this.view_pengiriman_transfer = false;


				this.getDisPlanListKirim();
				this.getDisPlanListAmbil();
			});
		},
		updateTransferDPKirim() {
			axios.post(this.action, {
				case: 'updateTransferDPKirim',
				nopol_truck: this.v_nopol_truck,
				sopir_name: this.v_sopir_name,
				gudang: this.v_gudang,
				tgl_kirim: this.v_tgl_kirim,
				ritase: this.v_ritase,
				no_to: this.v_notransfer,
				alasan: this.v_alasan,
				dt_so: this.data,
			}).then(response => {
				console.log(response.data);

				this.v_nopol_truck = "";
				this.v_sopir_name = "";
				this.v_gudang = "";
				this.v_notransfer = "";
				this.v_tgl_kirim = "";
				this.v_alasan = "";
				this.v_ritase = "";
				this.data = "";

				this.view_dp_plan = false;
				this.view_dp_plan_ambil = false;
				this.view_dp_plan_edit_kirim = false;
				this.view_dp_plan_edit_ambil = false;
				this.view_dp_plan_transfer_kirim = false;
				this.view_dp_plan_transfer_terima = false;
				this.view_list_plan = true;
				this.view_pengiriman_kirim = false;
				this.view_pengiriman_ambil = false;
				this.view_pengiriman_transfer = false;

				this.getDisPlanListKirim();
				this.getDisPlanListAmbil();
			});
		},
		updateProsesTransferDPKirim() {
			axios.post(this.action, {
				case: 'updateProsesTransferDPKirim',
				nopol_truck: this.v_nopol_truck,
				sopir_name: this.v_sopir_name,
				tgl_terima: this.v_tgl_terima,
				catatan_to: this.v_catatan_to,
				dt_so: this.data,
			}).then(response => {
				console.log(response.data);

				this.v_nopol_truck = "";
				this.v_sopir_name = "";
				this.v_tgl_terima = "";
				this.v_catatan_to = "";
				// this.data = "";

				axios.post(this.action, {
					case: 'getDaftarPengirimanTransferByID',
					id_dt: this.data.id,
				}).then(response => {
					this.data = "";
					this.detail_transfer_byid = response.data
					this.detail_transfer_byid_item = response.data[0].item

					this.view_dp_plan = false;
					this.view_dp_plan_ambil = false;
					this.view_dp_plan_edit_kirim = false;
					this.view_dp_plan_edit_ambil = false;
					this.view_dp_plan_transfer_kirim = false;
					this.view_dp_plan_transfer_terima = false;
					this.view_list_plan = false;
					this.view_pengiriman_kirim = false;
					this.view_pengiriman_ambil = false;
					this.view_pengiriman_transfer = true;

					this.getDisPlanListKirim();
					this.getDisPlanListAmbil();

					console.log(this.detail_transfer_byid);
					console.log(this.detail_transfer_byid_item);
				});
			});
		},
		cetak_do(data) {
			function submit_hidden_form(url, params) {
				var f = $("<form method='POST' target='_blank' style='display:none;'></form>").attr({
					action: url
				}).appendTo(document.body);
				for (var i in params) {
					if (params.hasOwnProperty(i)) {
						$('<input type="hidden" />').attr({
							name: i,
							value: params[i]
						}).appendTo(f);
					}
				}
				f.submit();
				f.remove();
			}

			submit_hidden_form(
				this.doPage, {
				id: data.id,
			}
			)
		},
		getSupirByNopol() {
			axios.post(this.action, {
				case: "getSupirByNopol",
				no_pol: this.v_nopol_truck,
			}).then(response => {
				// console.log(response.data.data[0].sopir);
				this.v_sopir_name = response.data.data[0].sopir;
			})

			// axios.post(this.action, {
			// 	case: "getRitaseByNopol",
			// 	no_pol: this.v_nopol_truck,
			// }).then(response => {
			// 	// console.log(response.data.data[0].sopir);
			// 	this.v_ritase = response.data.data[0].standar_rit;
			// })
		},
		getSupirBySupir() {
			this.v_sopir_name = this.v_sopir_name.sopir;
			console.log(this.v_sopir_name);
		},
		getRitaseByRitase() {
			console.log(this.v_ritase);
		}
	},
	computed: {
		isDisabled: function () {
			if (this.v_nopol_truck == '' || this.v_sopir_name == '' || this.v_gudang == '') {
				this.terms = true;
			} else {
				this.terms = false;
			}
			return this.terms;
		},
		truk_luar: function () {
			if (this.v_pilih_truck == 'truck_dalam') {
				this.v_ritase = this.v_ritase
			} else {
				this.v_ritase = 0
			}
			return this.v_ritase;
		}
	}

});

Vue.component('tabs_distribution_plan_component', {

	template: `
	<div>
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li v-for="tab in tabs" :class="{ 'active': tab.isActive }">
					<a :href="tab.href" @click="selectTab(tab)"><span v-html="tab.icon"></span> {{tab.name}}</a>
				</li>
			</ul>
		</div>
		<div class="tab-content">
			<slot></slot>
		</div>
	</div>
	`,
	data() {
		return {
			tabs: []
		};
	},
	created() {
		this.tabs = this.$children;
	},
	methods: {
		selectTab(selectedTab) {
			this.tabs.forEach(tab => {
				tab.isActive = (tab.name == selectedTab.name);
			});
		}
	}

});

Vue.component('tab_distribution_plan_component', {

	template: `
	<div v-show="isActive">
		<slot></slot>
	</div>
	`,
	props: {
		name: { required: true },
		selected: { default: false },
		icon: { required: true }
	},

	data() {
		return {
			isActive: false
		};
	},
	computed: {
		href() {
			return '#' + this.name.toLowerCase().replace(/ /g, '-');
		}
	},

	mounted() {
		this.isActive = this.selected;
	}

});

Vue.component('dis-plan-kirim-component', {

	template: `
	<div>
		<div class="row">
			<div class="col-md-12" style="text-align: end;">
				<button type="button" class="btn btn-xm btn-success" @click="load"><i class="fa fa-refresh"></i></button>
			</div>	
		</div>
		</br>
		<div class="row">
			<div class="col-md-12">
				<v-client-table :data="data" :columns="col_kirim" :options="opt_kirim"></v-client-table>
			</div>	
		</div>
	</div>
	`,
	props: {
		username: {
			required: true
		},
		data: {
			type: Array,
			required: true,
		}
	},
	data() {
		return {
			col_kirim: ['no_dp', 'asal_gudang', 'wh_code', 'tgl_perintah2', 'action'],
			opt_kirim: {
				headings: {
					no_dp: "NO. DP",
					asal_gudang: "LOKASI KEBERANGKATAN",
					wh_code: "SO. ASAL",
					tgl_perintah2: "TGL. PERINTAH",
					action: "ACTION"
				},
				sortable: ['no_dp', 'wh_code'],
				texts: {
					count: "",
					filter: "Cari",
					filterPlaceholder: "Cari No. DP",
				},
				templates: {
					asal_gudang: "temp-dis-plan-gudang-asal",
					wh_code: "temp-dis-plan-so-asal",
					action: "action-dis-plan-kirim-comp"
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				filterable: false,
				childRow: "detail-displan-kirim-component",
				showChildRowToggler: true,
				footerHeadings: true,
				highlightMatches: true,
				columnsDropdown: false,
				uniqueKey: "id",
				childRowTogglerFirst: false,
			}
		}
	},
	methods: {
		load() {
			EventBus.$emit('load_distribution_kirim');
		}
	}

});

Vue.component('detail-displan-kirim-component', {

	props: ["data", "index", "columns"],
	template: `
	<div>
		<div class="row">
			<div class="col-sm-12" style="text-transform: capitalize;">
				<v-client-table :data="data.item" :columns="col_kirim" :options="opt_kirim"></v-client-table>
			</div>
		</div>
	</div>
	`,
	data() {
		return {
			col_kirim: ['sm_no', 'so_no', 'code', 'name', 'code_comp', 'item_code', 'tanggal', 'SO', 'keterangan', 'sales_from_no'],
			opt_kirim: {
				headings: {
					name: "NAMA",
					code: "LEDGER",
					code_comp: 'CODE COMP',
					item_code: 'ITEM CODE',
					tanggal: 'TANGGAL SO',
					price_amount: "HARGA",
					keterangan: "KET.",
					sm_no: "NO. SM",
					so_no: "NO. SO",
					sales_from_no: "SALES CODE"
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				templates: {

				},
				filterable: false
			}
		}
	},
	methods: {

	},
	created() {

	}

});

Vue.component('dis-plan-ambil-component', {

	template: `
	<div>
		<div class="row">
			<div class="col-md-12" style="text-align: end;">
				<button type="button" class="btn btn-xm btn-success" @click="load"><i class="fa fa-refresh"></i></button>
			</div>	
		</div>
		</br>
		<div class="row">
			<div class="col-md-12">
				<v-client-table :data="data" :columns="col_ambil" :options="opt_ambil"></v-client-table>
			</div>	
		</div>
	</div>
	`,
	props: {
		username: {
			required: true
		},
		data: {
			type: Array,
			required: true,
		}
	},
	data() {
		return {
			col_ambil: ['no_dp', 'asal_gudang', 'wh_code', 'tgl_perintah2', 'action'],
			opt_ambil: {
				headings: {
					no_dp: "NO. DP",
					asal_gudang: "LOKASI AMBIL",
					wh_code: "SO. ASAL",
					tgl_perintah2: "TGL. AMBIL",
					action: "ACTION"
				},
				sortable: ['no_dp'],
				texts: {
					count: "",
					filter: "Cari",
					filterPlaceholder: "Cari No. DP",
				},
				templates: {
					asal_gudang: "temp-dis-plan-gudang-asal",
					wh_code: "temp-dis-plan-so-asal",
					action: "action-dis-plan-ambil-comp"
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				filterable: false,
				childRow: "detail-displan-ambil-component",
				showChildRowToggler: true,
				footerHeadings: true,
				highlightMatches: true,
				columnsDropdown: false,
				uniqueKey: "id",
				childRowTogglerFirst: false,
			}
		}
	},
	methods: {
		load() {
			EventBus.$emit('load_distribution_ambil');
		}
	}

});

Vue.component('detail-displan-ambil-component', {

	props: ["data", "index", "columns"],
	template: `
	<div>
		<div class="row">
			<div class="col-sm-12" style="text-transform: capitalize;">
				<v-client-table :data="data.item" :columns="col_kirim" :options="opt_kirim"></v-client-table>
			</div>
		</div>
	</div>
	`,
	data() {
		return {
			col_kirim: ['sm_no', 'so_no', 'code', 'name', 'code_comp', 'item_code', 'tanggal', 'SO', 'keterangan', 'sales_from_no'],
			opt_kirim: {
				headings: {
					name: "NAMA",
					code: "LEDGER",
					code_comp: 'CODE COMP',
					item_code: 'ITEM CODE',
					tanggal: 'TANGGAL SO',
					price_amount: "HARGA",
					keterangan: "KET.",
					sm_no: "NO. SM",
					so_no: "NO. SO",
					sales_from_no: "SALES CODE"
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				templates: {

				},
				filterable: false
			}
		}
	},
	methods: {

	},
	created() {

	}

});

Vue.component('dis-plan-transfer-component', {
	template: `
	<div>
		<div class="row">
			<div class="col-md-12" style="text-align: end;">
				<button type="button" class="btn btn-xm btn-success" @click="load"><i class="fa fa-refresh"></i></button>
			</div>	
		</div>
		</br>
		<div class="row">
			<div class="col-md-12">
				<v-client-table :data="data" :columns="col_transfer" :options="opt_transfer"></v-client-table>
			</div>	
		</div>
	</div>
	`,
	props: {
		username: {
			required: true
		},
		data: {
			type: Array,
			required: true,
		}
	},
	data() {
		return {
			col_transfer: ['no_dp', 'asal_gudang', 'wh_code', 'tgl_kirim2', 'action'],
			opt_transfer: {
				headings: {
					no_dp: "NO. DP",
					asal_gudang: "GUDANG ASAL",
					wh_code: "SO. ASAL",
					tgl_kirim2: "TGL. AMBIL",
					action: "ACTION"
				},
				sortable: ['no_dp'],
				texts: {
					count: "",
					filter: "Cari",
					filterPlaceholder: "Cari No. DP",
				},
				templates: {
					asal_gudang: "temp-dis-plan-gudang-asal",
					wh_code: "temp-dis-plan-so-asal",
					action: "action-dis-plan-transfer-comp"
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				filterable: false,
				childRow: "detail-displan-transfer-component",
				showChildRowToggler: true,
				footerHeadings: true,
				highlightMatches: true,
				columnsDropdown: false,
				uniqueKey: "id",
				childRowTogglerFirst: false,
			}
		}
	},
	methods: {
		load() {
			EventBus.$emit('load_distribution_transfer');
		}
	}
});

Vue.component('detail-displan-transfer-component', {

	props: ["data", "index", "columns"],
	template: `
	<div>
		<div class="row">
			<div class="col-sm-12" style="text-transform: capitalize;">
				<v-client-table :data="data.item" :columns="col_kirim" :options="opt_kirim"></v-client-table>
			</div>
		</div>
	</div>
	`,
	data() {
		return {
			col_kirim: ['sm_no', 'so_no', 'code', 'name', 'code_comp', 'item_code', 'tanggal', 'SO', 'keterangan', 'sales_from_no'],
			opt_kirim: {
				headings: {
					name: "NAMA",
					code: "LEDGER",
					code_comp: 'CODE COMP',
					item_code: 'ITEM CODE',
					tanggal: 'TANGGAL SO',
					price_amount: "HARGA",
					keterangan: "KET.",
					sm_no: "NO. SM",
					so_no: "NO. SO",
					sales_from_no: "SALES CODE"
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				templates: {

				},
				filterable: false
			}
		}
	},
	methods: {

	},
	created() {

	}

});

Vue.component('daf_outstanding_component', {
	template:
		`
		<div>
			<div class="row">
				<div class="col-md-1">
					<label class="control-label text-center">Produk</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Cabang</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Gudang</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Ledger</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Periode</label>
				</div>
				<div class="col-md-1">
					<label class="control-label text-center">Report</label>
				</div>
			</div>
			<div class="row">
				<form @submit.prevent="getOutstanding">
					<div class="col-md-1">
						<v-select style="text-transform: capitalize" label="category" :options="dt_produk" v-model="v_produk" :reduce="dt_produk=>dt_produk.category" @input="changeKategory"></v-select>
					</div>
					<div class="col-md-2">
						<v-select style="text-transform: capitalize" label="coce_code" :options="dt_cabang" v-model="v_cabang" :reduce="dt_cabang=>dt_cabang.coce_code"></v-select>
					</div>
					<div class="col-md-2">
						<v-select style="text-transform: capitalize" label="wh_code" :options="dt_gudang" v-model="v_gudang" :reduce="dt_gudang=>dt_gudang.wh_code"></v-select>
					</div>
					<div class="col-md-2">
						<input type="text" class="form-control" v-model="v_ledger">
					</div>
					<div class="col-md-2">
						<vuejs-datepicker
							v-model="v_periode"
							:format="DatePickerFormatb"
							:bootstrap-styling="true"
							:placeholder="holderb"
							required
							style="width: 290px;">
						</vuejs-datepicker>
					</div>
					<div class="col-md-2">
						<v-select style="text-transform: capitalize" label="report" :options="dt_repot" v-model="v_report" :reduce="dt_repot=>dt_repot.report"></v-select>
					</div>
					<div class="col-md-1">
						<button type="submit" class="btn btn-xm btn-primary">Load</button>
					</div>
				</form>
			</div>
			</br></br>
			<div class="row">
				<div class="col-md-12">
					<div class="lds-facebook" v-if="loading"><div></div><div></div><div></div></div>
					<table class="table table-hover table-responsive table-striped">
						<thead>
							<tr>
								<th class="active">#</th>
								<th class="active" v-if="ar_cabang === 'ALL'">Cabang</th>
								<th class="active">Ledger</th>
								<th class="active">Customer</th>
								<th class="active">Tgl SO</th>
								<th class="active">Nomor SO</th>
								<th class="active">WH</th>
								<th class="active">Item</th>
								<th class="active">QTY SO</th>
								<th class="active" v-if="ar_report === 'detail'">TGL SO</th>
								<th class="active" v-if="ar_report === 'detail'">NOMOR DO</th>
								<th class="active" v-if="ar_report === 'detail'">NO PLAT</th>
								<th class="active" v-if="ar_report === 'detail'">QTY DO</th>
								<th class="active" v-else>QTY DO</th>
								<th class="active">Outs</th>
								<th class="active" v-if="ar_report === 'detail'"></th>
								<th class="active" v-else>Description</th>
								<th class="active"></th>
							</tr>
						</thead>
						<tbody>
							<tr v-for="(dt,i) in dt_outstanding">
								<td>{{ dt.no }}</td>
								<td v-if="ar_cabang === 'ALL'">{{ dt.txt_coce_code }}</td>
								<td>{{ dt.vend_code }}</td>
								<td>{{ dt.name }}</td>
								<td>{{ dt.pr_date }}</td>
								<td>{{ dt.txt_pr_no }}</td>
								<td>{{ dt.kp_code }}</td>
								<td>{{ dt.txt_item_code }}</td>
								<td>{{ dt.item_qty }}</td>
								<td v-if="ar_report === 'detail'">{{ dt.do_date }}</td>
								<td v-if="ar_report === 'detail'">{{ dt.ref_no }}</td>
								<td v-if="ar_report === 'detail'">{{ dt.car_no }}</td>
								<td v-if="ar_report === 'detail'">{{ dt.do_qty }}</td>
								<td v-else>{{ dt.do_sum }}</td>
								<td style="color: red;">{{ dt.outs_do }}</td>
								<td v-if="ar_report === 'detail'"></td>
								<td v-else>{{ dt.pr_desc }}</td>
								<td v-if="dt.txt_pr_no === xso_no"></td>
								<td v-else><button name="btn-load" class="btn btn-info btn-flat btn-xs" style="width:100%;" @click="do_outstanding(dt)">BUAT DO</button></td>
							</tr>
						</tbody>
						<tfoot>
							<tr v-for="(dta,i) in total_outstanding" style="font-weight:bold; background-color:#F8F8F8;">
								<th></th>
								<th v-if="ar_cabang === 'ALL'"></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<td>{{dta.item_code}}</td>
								<td>{{dta.item_qty}}</td>
								<th v-if="ar_report === 'detail'"></th>
								<th v-if="ar_report === 'detail'"></th>
								<th v-if="ar_report === 'detail'"></th>
								<td v-if="ar_report === 'detail'">{{dta.do_sum}}</td>
								<td v-else>{{dta.do_sum}}</td>
								<td>{{dta.outs_do}}</td>
								<th></th>
							</tr>
							<tr style="font-weight:bold; background-color:#F8F8F8;">
								<th></th>
								<th v-if="ar_cabang === 'ALL'"></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<td style="text-align:right;">{{ grand_total_outstanding.sum_so_qty }}</td>
								<th v-if="ar_report === 'detail'"></th>
								<th v-if="ar_report === 'detail'"></th>
								<th v-if="ar_report === 'detail'"></th>
								<td style="text-align:right;" v-if="ar_report === 'detail'">{{ grand_total_outstanding.sum_do_qty }}</td>
								<td style="text-align:right;" v-else>{{ grand_total_outstanding.sum_do_sum }}</td>
								<td style="text-align:right; color:red;">{{ grand_total_outstanding.sum_outs_do }}</td>
								<th></th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
	`,
	components: {
		vuejsDatepicker
	},
	props: ["username"],
	data() {
		return {
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 8640000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
			loading: false,
			v_produk: "SAA",
			v_cabang: "ALL",
			v_gudang: "ALL",
			v_report: "Summary",
			v_periode: moment(this.v_periode).format("YYYY-MM-DD"),
			v_ledger: "",
			dt_produk: [],
			dt_cabang: [],
			dt_gudang: [],
			dt_gudang_all: [
				{
					"wh_code": "ALL"
				}
			],
			dt_repot: [
				{
					'report': 'summary'
				},
				{
					'report': 'detail'
				}
			],
			dt_outstanding: [],
			ar_report: "summary",
			ar_cabang: "ALL",
			total_outstanding: [],
			grand_total_outstanding: [],
			xso_no: ""
		}
	},
	methods: {
		getKategori() {
			axios.post(this.action, {
				case: 'getKategori',
				username: this.username
			}).then(response => {
				this.dt_produk = response.data.data;
				this.getCabang(this.dt_produk[0].category, this.username);
			});
		},
		getCabang(category) {
			axios.post(this.action, {
				case: 'getCabang',
				category: category,
				username: this.username,
			}).then(response => {
				this.dt_cabang = response.data.data;
				this.getGudang(this.dt_cabang[0].coce_code);
			});
		},
		getGudang(coce_code) {
			axios.post(this.action, {
				case: 'getGudangOutstanding',
				coce_code: coce_code,
				username: this.username,
			}).then(response => {
				this.dt_gudang = response.data[0];
				this.dt_gudang.push(this.dt_gudang_all[0]);
			});
		},
		changeKategory() {
			this.getCabang(this.v_produk);
		},
		getOutstanding() {
			this.loading = true;
			axios.post(this.action, {
				case: 'getOutstanding',
				category: this.v_produk,
				cabang: this.v_cabang,
				gudang: this.v_gudang,
				ledger: this.v_ledger,
				periode: this.v_periode,
				report: this.v_report
			}).then(response => {
				this.loading = false;
				this.dt_outstanding = response.data.outstanding;
				this.total_outstanding = response.data.total_outstanding;
				this.grand_total_outstanding = response.data.grand_total_outstanding[0];

				this.ar_report = this.dt_outstanding[0].report;
				this.ar_cabang = this.dt_outstanding[0].kode_cab;
			});
		},
		do_outstanding(data) {
			console.log(data);
			function submit_hidden_form(url, params) {
				var f = $("<form method='POST' target='_blank' style='display:none;'></form>").attr({
					action: url
				}).appendTo(document.body);
				for (var i in params) {
					if (params.hasOwnProperty(i)) {
						$('<input type="hidden" />').attr({
							name: i,
							value: params[i]
						}).appendTo(f);
					}
				}
				f.submit();
				f.remove();
			}

			submit_hidden_form(
				this.doPage2, {
				shift: this.v_produk,
				whCode: data.kp_code + '-' + data.kode_cab,
			}
			)
		},
		getNow() {
			const today = new Date();
			const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
			const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			const dateTime = date + ' ' + time;
			this.v_periode = dateTime;
		}
	},
	created() {
		this.getKategori();
	}
});

Vue.component('daf_force_component2', {
	template:
		`
		<div>
			<div class="row">
				<div class="col-md-2">
					<label class="control-label text-center">Produk</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Dari Tanggal</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Sampai Tanggal</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Search</label>
				</div>
			</div>
			<div class="row">
				<form @submit.prevent="getForceClose">
					<div class="col-md-2">
						<v-select style="text-transform: capitalize" label="category" :options="dt_produk" v-model="v_produk" :reduce="dt_produk=>dt_produk.category"></v-select>
					</div>
					<div class="col-md-2">
						<vuejs-datepicker
							v-model="v_date_start"
							:format="DatePickerFormatb"
							:bootstrap-styling="true"
							:placeholder="holderb"
							required
							style="width: 290px;">
						</vuejs-datepicker>
					</div>
					<div class="col-md-2">
						<vuejs-datepicker
							v-model="v_date_end"
							:format="DatePickerFormatb"
							:bootstrap-styling="true"
							:placeholder="holderb"
							required
							style="width: 290px;">
						</vuejs-datepicker>
					</div>
					<div class="col-md-2">
						<input type="text" class="form-control" v-model="v_search">
					</div>
					<div class="col-md-2">
						<button type="submit" class="btn btn-xm btn-primary">Load</button>
					</div>
				</form>
			</div>
			</br></br>
			<div class="row">
				<div class="col-md-12">
					<div class="lds-facebook" v-if="loading"><div></div><div></div><div></div></div>
					<v-client-table :data="dt_requestClose" :columns="col_requestclose" :options="opt_requestclose"></v-client-table>
				</div>
			</div>
		</div>
	`,
	components: {
		vuejsDatepicker
	},
	props: ["username"],
	data() {
		return {
			loading: false,
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 8640000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
			v_search: "",
			v_date_start: "",
			v_date_end: "",
			v_produk: "",
			dt_produk: [],
			dt_requestClose: [],
			col_requestclose: ['event_date', 'user_id', 'event_desc', 'reason', 'qty_so', 'qty_do', 'qty_outs', 'action'],
			opt_requestclose: {
				headings: {
					event_date: "DATETIME",
					user_id: "USER",
					event_desc: 'DESCRIPTION',
					reason: 'REASON',
					qty_so: "QTY SO",
					qty_do: "QTY DO",
					qty_outs: "QTY OUTS"
				},
				templates: {
					action: 'action-force-close-comp'
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				filterable: true,
				customFilters: ['event_date', 'user_id', 'event_desc', 'reason']
			}
		}
	},
	methods: {
		getKategori() {
			axios.post(this.action, {
				case: 'getKategori',
				username: this.username
			}).then(response => {
				this.dt_produk = response.data.data;
			});
		},
		getForceClose() {
			this.loading = true;
			axios.post(this.action, {
				case: 'getForceClose',
				shift: this.v_produk,
				status: 'REQUEST CLOSED',
				cari: this.v_search,
				date1: moment(this.v_date_start).format("YYYY-MM-DD"),
				date2: moment(this.v_date_end).format("YYYY-MM-DD"),
			}).then(response => {
				this.loading = false;
				this.dt_requestClose = response.data;
			});
		}
	},
	created() {
		this.getKategori();
	}
});

Vue.component('daf_force_component', {
	template:
		`
		<div>
			<div class="row">
				<div class="col-md-1">
					<label class="control-label text-center">Produk</label>
				</div>
				<div class="col-md-1">
					<label class="control-label text-center">Cabang</label>
				</div>
				<div class="col-md-1">
					<label class="control-label text-center">Gudang</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Sales</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Ledger</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Periode</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Report</label>
				</div>
			</div>
			<div class="row">
				<form @submit.prevent="getForceClose">
					<div class="col-md-1">
						<v-select style="text-transform: capitalize" label="category" :options="dt_produk" v-model="v_produk" :reduce="dt_produk=>dt_produk.category" @input="changeKategory"></v-select>
					</div>
					<div class="col-md-1">
						<v-select style="text-transform: capitalize" label="coce_code" :options="dt_cabang" v-model="v_cabang" :reduce="dt_cabang=>dt_cabang.coce_code" @input="getSales"></v-select>
					</div>
					<div class="col-md-1">
						<v-select style="text-transform: capitalize" label="wh_code" :options="dt_gudang" v-model="v_gudang" :reduce="dt_gudang=>dt_gudang.wh_code"></v-select>
					</div>
					<div class="col-md-2">
						<v-select style="text-transform: capitalize" label="sales_code" :options="dt_sales" v-model="v_sales_code" :reduce="dt_sales=>dt_sales.sales_code"></v-select>
					</div>
					<div class="col-md-2">
						<input type="text" class="form-control" v-model="v_search">
					</div>
					<div class="col-md-2">
						<vuejs-datepicker
							v-model="v_date_start"
							:format="DatePickerFormatb"
							:bootstrap-styling="true"
							:placeholder="holderb"
							required
							style="width: 290px;">
						</vuejs-datepicker>
					</div>
					<div class="col-md-2">
						<v-select style="text-transform: capitalize" label="label_report" :options="dt_report" v-model="v_report" :reduce="dt_report=>dt_report.report"></v-select>
					</div>
					<div class="col-md-1">
						<button type="submit" class="btn btn-xm btn-primary">Load</button>
					</div>
					<div class="col-md-1" style="display: none;">
						<button type="submit" class="btn btn-xm btn-primary" @click="send_mail">Send</button>
					</div>
				</form>
			</div>
			</br></br>
			<div class="row">
				<div class="col-md-12">
					<div class="lds-facebook" v-if="loading"><div></div><div></div><div></div></div>
					<v-client-table :data="dt_requestClose" :columns="col_requestclose" :options="opt_requestclose"></v-client-table>
				</div>
			</div>
			<div class="modal fade" id="proses_force_close" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<div class="row">
								<div class="col-sm-11">
									<h3 class="modal-title">FORCE CLOSE SO #{{dt_force.pr_no}}</h3>
								</div>
								<div class="col-sm-1" style="text-align: end;">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							</div>
						</div>
						<div class="modal-body" style="background: white; padding-top: 1px;">
							<div class="form-group">
								<div class="row">
									<div class="col-sm-4">
										<label for="recipient-name" class="col-form-label">NAMA PELANGGAN</label>
									</div>
									<div class="col-sm-1">
										<p>:</p>
									</div>
									<div class="col-sm-6">
										<label for="recipient-name" class="col-form-label">{{dt_force.name}} [{{dt_force.vend_code}}]</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-4">
										<label for="recipient-name" class="col-form-label">PERMINTAAN DARI</label>
									</div>
									<div class="col-sm-1">
										<p>:</p>
									</div>
									<div class="col-sm-6">
										<label for="recipient-name" class="col-form-label">{{dt_force.entry_by}}</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="recipient-name" class="col-form-label">ALASAN KARENA</label>
								<textarea class="form-control" rows="3" style="resize: none;" readonly>{{dt_reason.reason}}</textarea>
							</div>
							<div class="form-group">
								<select class="form-control" v-model="v_force_aksi">
									<option selected value="1">Menyetujui</option>
									<option value="0">Menolak</option>
								</select>
							</div>
							<div class="form-group">
								<label for="recipient-name" class="col-form-label">ALASANNYA</label>
								<textarea class="form-control" rows="3" style="resize: none;" v-model="v_force_alasan_hoo"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger btn-sm" @click="closeModalForce()">Batal</button>
							<button type="button" class="btn btn-primary btn-sm" @click="actionForceClose">Simpan</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	`,
	components: {
		vuejsDatepicker
	},
	props: ["username", "usergroup"],
	data() {
		return {
			loading: false,
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 8640000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
			v_search: "",
			v_date_start: moment(this.v_date_start).format("YYYY-MM-DD"),
			v_date_end: "",
			v_produk: "SAA",
			v_cabang: "ALL",
			v_gudang: "ALL",
			v_report: "Summary",
			v_periode: "",
			v_ledger: "",
			v_sales_code: "ALL",
			dt_produk: [],
			dt_requestClose: [],
			dt_cabang: [],
			dt_gudang: [],
			dt_sales: [],
			dt_report: [
				{
					"report": "summary",
					"label_report": "summary",
				},
				{
					"report": "detail",
					"label_report": "Detail",
				}
			],
			dt_gudang_all: [
				{
					"wh_code": "ALL"
				}
			],
			dt_sales_all: [
				{
					"sales_code": "ALL"
				}
			],
			col_requestclose: ['spk_no', 'vend_code', 'name',
				'pr_no', 'item_code', 'item_qty', 'ref_no_out',
				'sum_out', 'sum_in', 'pr_desc', 'action'],
			opt_requestclose: {
				headings: {
					spk_no: "CABANG",
					vend_code: "LEDGER",
					name: 'NAME',
					pr_no: 'NO. SO',
					item_code: "ITEM CODE",
					item_qty: "QTY SO",
					ref_no_out: "REF NO OUT",
					sum_out: "QTY DO",
					sum_in: "OUTS",
					pr_desc: "DESCRIPTION"
				},
				templates: {
					action: 'action-force-close-comp'
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				filterable: true,
				childRow: "detail-force-close-component",
				showChildRowToggler: true,
				footerHeadings: true,
				highlightMatches: true,
				columnsDropdown: false,
				uniqueKey: "pr_no",
				childRowTogglerFirst: false,
			},
			dt_force: [],
			dt_reason: [],
			v_force_aksi: "1",
			v_force_alasan_hoo: ""
		}
	},
	methods: {
		openModalForce() {
			$("#proses_force_close").modal("show");
		},
		closeModalForce() {
			$("#proses_force_close").modal("hide");
		},
		getKategori() {
			axios.post(this.action, {
				case: 'getKategori',
				username: this.username
			}).then(response => {
				this.dt_produk = response.data.data;
				this.getCabang(this.dt_produk[0].category, this.username);
			});
		},
		getCabang(category) {
			axios.post(this.action, {
				case: 'getCabang',
				category: category,
				username: this.username,
			}).then(response => {
				this.dt_cabang = response.data.data;
				this.getGudang(this.dt_cabang[0].coce_code);
			});
		},
		getGudang(coce_code) {
			axios.post(this.action, {
				case: 'getGudangOutstanding',
				coce_code: coce_code,
				username: this.username,
			}).then(response => {
				this.dt_gudang = response.data[0];
				this.dt_gudang.push(this.dt_gudang_all[0]);
			});
		},
		getSales() {
			axios.post(this.action, {
				case: 'getSales',
				coce_code: this.v_cabang,
				username: this.username,
				usergroup: this.usergroup
			}).then(response => {
				this.dt_sales = response.data.data;
				this.dt_sales.push(this.dt_sales_all[0]);
			});
		},
		changeKategory() {
			this.getCabang(this.v_produk);
		},
		getForceClose() {
			this.loading = true;
			axios.post(this.action, {
				case: 'getForceClose',
				shift: this.v_produk,
				coce_code: this.v_cabang,
				sales_code: this.v_sales_code,
				wh_code: this.v_gudang,
				ledger: this.v_search,
				report: this.v_report,
				date2: moment(this.v_date_start).format("YYYY-MM-DD"),
			}).then(response => {
				this.loading = false;
				this.dt_requestClose = response.data;
				console.log(this.dt_requestClose);
			});
		},
		getNow() {
			const today = new Date();
			const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
			const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			const dateTime = date + ' ' + time;
			this.v_periode = dateTime;
		},
		actionForceClose() {
			//gantung
			var data = {
				"action": 'approvedCloseSO',
				"aksi": 'F',
				"coce_code": this.v_cabang,
				"status": 'W',
				"shift": this.dt_force.code_comp.toLowerCase(),
				"date2": moment(this.dt_force.pr_date).format("YYYY-MM-DD"),
				"pr_no": this.dt_force.pr_no,
				"usergroup": 'EDP',
				"reason": '',
				"user_id": this.dt_force.entry_by,
				"reason_desc2": this.dt_reason.reason,
				"aksi_hoo": this.v_force_aksi,
				"reason_hoo": this.v_force_alasan_hoo,
				"reason_desc": ''
			};

			$.post("../../sales/actionForceClose_bak.php",
				data,
				function (data, status) {
					data = JSON.parse(data);
					console.log(data);
				}
			);

		},
		send_mail() {
			// console.log(this.dt_force);
			axios.post(this.action, {
				case: 'sendEmail',
				action: 'approvedCloseSO',
				aksi: 'F',
				coce_code: this.v_cabang,
				status: 'W',
				shift: this.dt_force.code_comp.toLowerCase(),
				date2: moment(this.dt_force.pr_date).format("YYYY-MM-DD"),
				pr_no: this.dt_force.pr_no,
				usergroup: 'EDP',
				reason: '',
				user_id: this.dt_force.entry_by,
				reason_desc2: this.dt_reason.reason,
				aksi_hoo: this.v_force_aksi,
				reason_hoo: this.v_force_alasan_hoo,
				reason_desc: '',
				data_all: this.dt_force
			}).then(response => {
				console.log(response.data);
			});
		}
	},
	created() {
		this.getKategori();
		this.getNow();
		EventBus.$on("open_modal_proses_force_close", (data) => {
			this.dt_force = data;
			this.dt_reason = data.reason[0]
			this.openModalForce();
		});

		EventBus.$on("open_email_force_close", (data) => {
			this.dt_force = data;
			this.dt_reason = data.reason[0];
			this.send_mail();
		});
	}
});

Vue.component('temp-dis-plan-gudang-asal', {
	props: ["data", "index", "column"],
	template: `<label class='label label-primary lb-sm'>{{data.asal_gudang}}</label>`,
});

Vue.component('temp-dis-plan-so-asal', {
	props: ["data", "index", "column"],
	template: `<label class='label label-primary lb-sm'>{{data.item[0].cabang}}</label>`,
});

Vue.component('action-dis-plan-kirim-comp', {

	props: ['data', 'index', 'columns'],
	template: `
	<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
		<div class="btn-group mr-2" role="group" aria-label="First group" v-if="data.status_so === '0'">
			<button name="btn-load" class="btn btn-info btn-flat btn-xs" style="width:100%;" @click="proses(data)">BUAT DO</button>
		</div>
		<div v-else>
		</div>
		<div class="btn-group mr-2" role="group" aria-label="First group" v-if="data.status_so === '0'">
			<button name="btn-load" class="btn btn-primary btn-flat btn-xs" style="width:100%;" @click="edit(data)">TRANSFER</button>
		</div>
		<div v-else>
		</div>
		<div class="btn-group mr-2" role="group" aria-label="Second group">
			<button name="btn-load" class="btn btn-danger btn-flat btn-xs" style="width:100%;" @click="batal(data)">BATAL</button>
		</div>
	</div>
	`,
	methods: {
		proses(data) {
			EventBus.$emit('open_modal_proses_kirim', data);
		},
		edit(data) {
			EventBus.$emit('open_modal_edit_kirim', data);
		},
		transfer(data) {
			EventBus.$emit('open_modal_transfer_kirim', data);
		},
		batal(data) {
			const swalWithBootstrapButtons = Swal.mixin({
				customClass: {
					confirmButton: 'btn btn-success',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})

			swalWithBootstrapButtons.fire({
				title: 'Are you sure?',
				text: "You want to delete this data?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					axios.post(this.action, {
						case: 'deleteDistributionPlan',
						id_dp: data.id
					}).then(response => {
						console.log(response.data);
						location.reload();
					});
				} else if (
					result.dismiss === Swal.DismissReason.cancel
				) {
					swalWithBootstrapButtons.fire(
						'Cancelled',
						'Your data is safe :)',
						'error'
					)
				}
			})

			// axios.post(this.action, { 
			// 	case: 'deleteDistributionPlan',
			// 	id_dp: data.id
			// }).then(response => {
			// 	console.log(response.data);
			// });
		}
	}

});

Vue.component('action-dis-plan-ambil-comp', {

	props: ['data', 'index', 'columns'],
	template: `
	<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
		<div class="btn-group mr-2" role="group" aria-label="First group" v-if="data.status_so === '0'">
			<button name="btn-load" class="btn btn-info btn-flat btn-xs" style="width:100%;" @click="proses(data)">BUAT DO</button>
		</div>
		<div v-else>
		</div>
		<div class="btn-group mr-2" role="group" aria-label="Second group">
			<button name="btn-load" class="btn btn-danger btn-flat btn-xs" style="width:100%;" @click="batal(data)">BATAL</button>
		</div>
	</div>
	`,
	methods: {
		proses(data) {
			EventBus.$emit('open_modal_proses_ambil', data);
		},
		edit(data) {
			EventBus.$emit('open_modal_edit_ambil', data);
		},
		batal(data) {
			const swalWithBootstrapButtons = Swal.mixin({
				customClass: {
					confirmButton: 'btn btn-success',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})

			swalWithBootstrapButtons.fire({
				title: 'Are you sure?',
				text: "You want to delete this data?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					axios.post(this.action, {
						case: 'deleteDistributionPlan',
						id_dp: data.id
					}).then(response => {
						console.log(response.data);
						location.reload();
					});
				} else if (
					result.dismiss === Swal.DismissReason.cancel
				) {
					swalWithBootstrapButtons.fire(
						'Cancelled',
						'Your data is safe :)',
						'error'
					)
				}
			})

			// axios.post(this.action, { 
			// 	case: 'deleteDistributionPlan',
			// 	id_dp: data.id
			// }).then(response => {
			// 	console.log(response.data);
			// });
		}
	}

});

Vue.component('action-dis-plan-transfer-comp', {

	props: ['data', 'index', 'columns'],
	template: `
	<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
		<div class="btn-group mr-2" role="group" aria-label="First group">
			<button name="btn-load" class="btn btn-info btn-flat btn-xs" style="width:100%;" @click="proses(data)">BUAT DO & TERIMA</button>
		</div>
		<div class="btn-group mr-2" role="group" aria-label="Second group">
			<button name="btn-load" class="btn btn-danger btn-flat btn-xs" style="width:100%;" @click="batal(data)">BATAL</button>
		</div>
	</div>
	`,
	methods: {
		proses(data) {
			EventBus.$emit('open_modal_proses_transfer', data);
		},
		batal(data) {
			const swalWithBootstrapButtons = Swal.mixin({
				customClass: {
					confirmButton: 'btn btn-success',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})

			swalWithBootstrapButtons.fire({
				title: 'Are you sure?',
				text: "You want to delete this data?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					axios.post(this.action, {
						case: 'deleteDistributionPlan',
						id_dp: data.id
					}).then(response => {
						console.log(response.data);
						location.reload();
					});
				} else if (
					result.dismiss === Swal.DismissReason.cancel
				) {
					swalWithBootstrapButtons.fire(
						'Cancelled',
						'Your data is safe :)',
						'error'
					)
				}
			})

			// axios.post(this.action, { 
			// 	case: 'deleteDistributionPlan',
			// 	id_dp: data.id
			// }).then(response => {
			// 	console.log(response.data);
			// });
		}
	}

});

Vue.component('action-force-close-comp', {
	props: ['data', 'index', 'columns'],
	template:
		`
	<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
		<div class="btn-group mr-2" role="group" aria-label="First group" v-if="data.sum_in > 10">
			<button name="btn-load" class="btn btn-info btn-flat btn-xs" style="width:100%;" @click="email(data)">KIRIM EMAIL</button>
		</div>
		<div class="btn-group mr-2" role="group" aria-label="First group" v-else>
			<button name="btn-load" class="btn btn-warning btn-flat btn-xs" style="width:100%;" @click="proses(data)">FORCE CLOSE</button>
		</div>
	</div>
	`,
	methods: {
		proses(data) {
			EventBus.$emit('open_modal_proses_force_close', data);
		},
		email(data) {
			EventBus.$emit('open_email_force_close', data);
		},
	}
});

Vue.component('modal-proses-dp-comp', {

	template: `
	<div class="modal fade" id="proses_dp_so" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<div class="row">
						<div class="col-sm-8">
							<h3 class="modal-title">Buat DO & Kirim</h3>
						</div>
					</div>
				</div>
				</br>
				<div class="modal-body" style="background: white; padding-top: 1px;">
					<div class="form-group">
						<label class="col-form-label">Pilih Truck</label>
						<div class="row">
							<div class="col-sm-4">
								<label>
									Truck Operasional &nbsp;&nbsp;
									<input type="radio" id="truck_dalam" v-model="v_pilih_truck" value="truck_dalam">
								</label>
							</div>
							<div class="col-sm-4">
								<label>
									Truck Luar &nbsp;&nbsp;
									<input type="radio" id="truck_luar" v-model="v_pilih_truck" value="truck_luar">
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="col-form-label">No. Plat Truck</label>
						<v-select style="text-transform: capitalize" label="nopol" :options="dt_truck" v-model="v_nopol_truck" :reduce="dt_truck=>dt_truck.nopol" v-if="v_pilih_truck === 'truck_dalam'"></v-select>
						<input class="form-control" type="text" v-model="v_nopol_truck" v-else/>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="col-form-label">Nama Supir</label>
						<v-select style="text-transform: capitalize" label="sopir" :options="dt_supir" v-model="v_sopir_name" :reduce="dt_sopir=>dt_sopir.sopir" v-if="v_pilih_truck === 'truck_dalam'"></v-select>
						<input class="form-control" type="text" v-model="v_sopir_name" v-else/>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="col-form-label">Pilih Gudang</label>
						<v-select style="text-transform: capitalize" label="wh_code" :options="dt_gudang" v-model="v_gudang" :reduce="dt_gudang=>dt_gudang.wh_code"></v-select>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="col-form-label">Tanggal Kirim</label>
						<vuejs-datepicker
							v-model="v_tgl_kirim"
							:format="DatePickerFormatb"
							:disabledDates="disabledDates"
							:bootstrap-styling="true"
							:placeholder="holderb"
							required
							style="width: 290px;"
							@input="countDate">
						</vuejs-datepicker>
					</div>
					<div class="form-group" v-if="v_totalPengiriman > 3">
						<label for="recipient-name" class="col-form-label">Alasan</label>
						<textarea class="form-control" id="exampleFormControlTextarea1" rows="3" v-model="v_alasan" style="resize: none;"></textarea>
					</div>
					<div class="form-group" v-else>
						
					</div>
					<div class="form-group">
						<label for="recipient-name" class="col-form-label">Pilih Ritase</label>
						<select class="form-control" id="ritaseplan" name="ritaseplan" v-model="v_ritase">
							<option value="">...</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
						</select>
					</div>
				</div>
				<div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" @click="closeModal()">Batal</button>
                    <button type="button" class="btn btn-primary btn-sm" @click="saveProsesPengiriman">Simpan & Cetak DO</button>
				</div>
			</div>
		</div>
	</div>
	`,
	components: {
		vuejsDatepicker
	},
	username: {
		required: true
	},
	data() {
		return {
			show: false,
			id: 0,
			data: [],
			v_gd_start: '',
			v_nama_supir: '',
			v_no_plat: '',
			tgl_kirim: '',
			v_alasan: '',
			v_qty: '',
			dt_truck: [],
			dt_supir: [],
			dt_gudang: [],
			v_nopol_truck: "",
			v_sopir_name: "",
			username: 'rachmadi',
			v_totalPengiriman: '',
			v_tgl_kirim: '',
			v_pilih_truck: '',
			v_gudang: '',
			v_ritase: '',
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 8640000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
		};
	},
	created() {
		EventBus.$on("open_modal_proses_kirim2", (data) => {
			this.data = data;
			this.getTruck();
			this.getSopir();
			this.getGudang();
			this.openModal();
		});
	},
	methods: {
		openModal() {
			$("#proses_dp_so").modal("show");
		},
		closeModal() {
			$("#proses_dp_so").modal("hide");
		},
		saveProsesPengiriman() {
			axios.post(this.action, {
				case: "saveProsesPengiriman",
				nama_supir: this.v_sopir_name,
				no_plat: this.v_nopol_truck,
				tgl_kirim: this.tgl_kirim,
				alasan: this.v_alasan,
				detail: this.data
			}).then(response => {
				console.log(response.data);
				this.closeModal();
			})
		},
		countDate() {
			var tgl_perintah = this.data.tgl_kirim;
			var tgl_kirim = moment(this.v_tgl_kirim).format("YYYY-MM-DD");

			dt1 = new Date(tgl_perintah);
			dt2 = new Date(tgl_kirim);

			this.v_totalPengiriman = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
		},
		getTruck() {
			axios.post(this.action, {
				case: "getTruck",
				username: "rachmadi",
			}).then(response => {
				this.dt_truck = response.data;
			})
		},
		getSopir() {
			axios.post(this.action, {
				case: "getSopir",
				username: "rachmadi",
			}).then(response => {
				this.dt_supir = response.data;
			})
		},
		getGudang() {
			axios.post(this.action, {
				case: "getGudang",
				username: "rachmadi",
			}).then(response => {
				this.dt_gudang = response.data;
				console.log(this.dt_gudang);
			})
		}
	},
	watch: {
		v_pilih_truck(value) {
			console.log(value);
		}
	}

});

Vue.component('daftar-pengiriman-comp', {

	template: `
	<div>
		<div class="row">
			<div class="col-md-12" style="text-transform: capitalize;">
				<tabs_distribution_plan_component>
					<tab_distribution_plan_component v-for="(dt,i) in tabs" :key="i" :name="dt.name" :icon="dt.icon" :selected="i === 0 ? 'true' : ''">
						<div v-if="i == 0">
							<daf_pengiriman_kirim_component :data="pengiriman_kirim"></daf_pengiriman_kirim_component>
						</div>
						<div v-if="i == 1">
							<daf_pengiriman_ambil_component :data="pengiriman_ambil"></daf_pengiriman_ambil_component>
						</div>
						<div v-if="i == 2">
							<daf_pengiriman_transfer_component :data="pengiriman_transfer"></daf_pengiriman_transfer_component>
						</div>
					</tab_distribution_plan_component>
				</tabs_distribution_plan_component>
			</div>
		</div>
	</div>
	`,
	props: ['loc'],
	components: {
		vuejsDatepicker
	},
	data() {
		return {
			tabs: [
				{
					name: 'DO KIRIM',
					icon: '<i class="fa fa-print"></i>',
					counter: 0,
				},
				{
					name: 'DO AMBIL',
					icon: '<i class="fa fa-print"></i>',
					counter: 1,
				},
				{
					name: 'DO TRANSFER',
					icon: '<i class="fa fa-print"></i>',
					counter: 2,
				},
			],
			v_tgl_perintah_kirim: '',
			pengiriman_kirim: [],
			pengiriman_ambil: [],
			pengiriman_transfer: []
		}
	},
	methods: {
		getDaftarPengirimanKirim() {
			axios.post(this.action, {
				case: 'getDaftarPengirimanKirim',
				loc: this.loc
			}).then(response => {
				this.pengiriman_kirim = response.data;
			});
		},
		getDaftarPengirimanAmbil() {
			axios.post(this.action, {
				case: 'getDaftarPengirimanAmbil',
				loc: this.loc
			}).then(response => {
				this.pengiriman_ambil = response.data;
			});
		},
		getDaftarPengirimanTransfer() {
			axios.post(this.action, {
				case: 'getDaftarPengirimanTransfer',
				loc: this.loc
			}).then(response => {
				this.pengiriman_transfer = response.data;
			});
		}
	},
	created() {
		this.getDaftarPengirimanKirim();
		this.getDaftarPengirimanAmbil();
		this.getDaftarPengirimanTransfer();
	}

});

Vue.component('daf_pengiriman_kirim_component', {

	template: `
	<div>
		<div class="row">
			<div class="col-md-12" style="text-transform: capitalize;">
				<v-client-table :data="data" :columns="columns" :options="options"></v-client-table>
			</div>
		</div>
	</div>
	`,
	props: {
		data: {
			type: Array,
			required: true
		},
	},
	data() {
		return {
			columns: ['no_dp', 'name', 'tgl_kirim2', 'wh_code', 'action'],
			options: {
				perPage: 50,
				sortable: ['type', 'code', 'ledgername'],
				headings: {
					no_dp: 'NO. DP',
					name: 'NAMA',
					tgl_kirim2: 'TGL. KIRIM',
					wh_code: 'GUDANG ASAL'
				},
				sortable: ['no_dp', 'wh_code'],
				texts: {
					count: "",
					filter: "Cari",
					filterPlaceholder: "Cari No. DP",
				},
				templates: {
					action: 'action-daf-dist-kirim-comp'
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				filterable: false,
				childRow: "detail-pengiriman-kirim-component",
				showChildRowToggler: true,
				footerHeadings: true,
				highlightMatches: true,
				columnsDropdown: false,
				uniqueKey: "id",
				childRowTogglerFirst: false,
			}
		}
	}

});

Vue.component('daf_pengiriman_ambil_component', {

	template: `
	<div>
		<div class="row">
			<div class="col-md-12" style="text-transform: capitalize;">
				<v-client-table :data="data" :columns="columns" :options="options"></v-client-table>
			</div>
		</div>
	</div>
	`,
	props: {
		data: {
			type: Array,
			required: true
		},
	},
	data() {
		return {
			columns: ['no_dp', 'tgl_perintah2', 'wh_code', 'action'],
			options: {
				perPage: 50,
				sortable: ['type', 'code', 'ledgername'],
				headings: {
					no_dp: 'NO. DP',
					tgl_perintah2: 'TGL. AMBIL',
					wh_code: 'GUDANG ASAL'
				},
				sortable: ['no_dp', 'wh_code'],
				texts: {
					count: "",
					filter: "Cari",
					filterPlaceholder: "Cari No. DP",
				},
				templates: {
					action: 'action-daf-dist-kirim-comp'
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				filterable: false,
				childRow: "detail-pengiriman-kirim-component",
				showChildRowToggler: true,
				footerHeadings: true,
				highlightMatches: true,
				columnsDropdown: false,
				uniqueKey: "id",
				childRowTogglerFirst: false,
			}
		}
	}

});

Vue.component('daf_pengiriman_transfer_component', {

	template: `
	<div>
		<div class="row">
			<div class="col-md-12" style="text-transform: capitalize;">
				<v-client-table :data="data" :columns="columns" :options="options"></v-client-table>
			</div>
		</div>
	</div>
	`,
	props: {
		data: {
			type: Array,
			required: true
		},
	},
	data() {
		return {
			columns: ['no_dp', 'tgl_kirim2', 'wh_code', 'action'],
			options: {
				perPage: 50,
				sortable: ['type', 'code', 'ledgername'],
				headings: {
					no_dp: 'NO. DP',
					tgl_kirim2: 'TGL. KIRIM',
					wh_code: 'GUDANG ASAL',
				},
				sortable: ['no_dp', 'wh_code'],
				texts: {
					count: "",
					filter: "Cari",
					filterPlaceholder: "Cari No. DP",
				},
				templates: {
					action: 'action-daf-dist-kirim-comp'
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				filterable: false,
				childRow: "detail-pengiriman-kirim-component",
				showChildRowToggler: true,
				footerHeadings: true,
				highlightMatches: true,
				columnsDropdown: false,
				uniqueKey: "id",
				childRowTogglerFirst: false,
			}
		}
	}

});

Vue.component('detail-pengiriman-kirim-component', {

	props: ["data", "index", "columns"],
	template: `
	<div>
		<div class="row">
			<div class="col-sm-12" style="text-transform: capitalize;">
				<v-client-table :data="data.item" :columns="col_kirim" :options="opt_kirim"></v-client-table>
			</div>
		</div>
	</div>
	`,
	data() {
		return {
			col_kirim: ['code', 'name', 'item_code', 'item_qty2', 'action'],
			opt_kirim: {
				headings: {
					code: "NAMA",
					name: "LEDGER",
					item_code: 'CODE COMP',
					item_qty2: 'ITEM CODE'
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc',
				},
				templates: {
					action: 'action-pengiriman-kirim-comp'
				},
				filterable: false
			}
		}
	},
	methods: {

	},
	created() {

	}

});

Vue.component('detail-force-close-component', {

	props: ["data", "index", "columns"],
	template: `
	<div>
		<div class="row">
			<div class="col-sm-12" style="text-transform: capitalize;">
				<v-client-table :data="data.item" :columns="col_kirim" :options="opt_kirim"></v-client-table>
			</div>
		</div>
	</div>
	`,
	data() {
		return {
			col_kirim: ['ref_no', 'voucher_desc', 'car_no', 'driver'],
			opt_kirim: {
				headings: {
					ref_no: "REF NO IN",
					voucher_desc: "REASON",
					car_no: 'PLAT',
					driver: 'SOPIR'
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				templates: {

				},
				filterable: false
			}
		}
	},
	methods: {

	},
	created() {

	}

});

Vue.component('action-pengiriman-kirim-comp', {

	props: ['data', 'index', 'columns'],
	template: `
	<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
		<div class="btn-group mr-2" role="group" aria-label="First group">
			<button name="btn-load" class="btn btn-info btn-flat btn-xs" style="width:100%;" @click="cetak(data)"><i class="fa fa-print"></i> CETAK</button>
		</div>
	</div>
	`,
	methods: {
		cetak(data) {
			// console.log(data.id);
			// console.log(this.doPage);

			function submit_hidden_form(url, params) {
				var f = $("<form method='POST' target='_blank' style='display:none;'></form>").attr({
					action: url
				}).appendTo(document.body);
				for (var i in params) {
					if (params.hasOwnProperty(i)) {
						$('<input type="hidden" />').attr({
							name: i,
							value: params[i]
						}).appendTo(f);
					}
				}
				f.submit();
				f.remove();
			}

			submit_hidden_form(
				this.doPage, {
				id: data.id,
			}
			)
		}
	}

});

Vue.component('action-daf-dist-kirim-comp', {

	props: ['data', 'index', 'columns'],
	template: `
	<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
		<div class="btn-group mr-2" role="group" aria-label="Second group">
			<button name="btn-load" class="btn btn-danger btn-flat btn-xs" style="width:100%;" @click="batal(data)">BATAL</button>
		</div>
	</div>
	`,
	methods: {
		batal(data) {
			// console.log(data);
			const swalWithBootstrapButtons = Swal.mixin({
				customClass: {
					confirmButton: 'btn btn-success',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})

			swalWithBootstrapButtons.fire({
				title: 'Are you sure?',
				text: "You want to delete this data?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					axios.post(this.action, {
						case: 'deleteDistributionPlan',
						id_dp: data.id
					}).then(response => {
						console.log(response.data);
						location.reload();
					});
				} else if (
					result.dismiss === Swal.DismissReason.cancel
				) {
					swalWithBootstrapButtons.fire(
						'Cancelled',
						'Your data is safe :)',
						'error'
					)
				}
			})

			axios.post(this.action, {
				case: 'deleteDistributionPlan',
				id_dp: data.id
			}).then(response => {
				console.log(response.data);
			});
		}
	}

});

Vue.component('daf_so_rektur_component', {

	template: `
	<div>
		<div class="row">
			<div class="col-sm-12" style="text-align: end;">
				<v-client-table :data="listSO_rektur" :columns="columns" :options="options"></v-client-table>
			</div>
		</div>
	</div>
	`,
	props: {

	},
	data() {
		return {
			columns: ['voucher_date', 'wh_code', 'voucher_doc', 'pr_no', 'voucher_desc',
				'car_no', 'remark', 'angk_date', 'expd_code', 'in_code',
				'item_qty'],
			options: {
				perPage: 50,
				sortable: ['type', 'code', 'ledgername'],
				headings: {
					voucher_date: 'Date',
					wh_code: 'WH',
					voucher_doc: 'Dokumen',
					pr_no: 'Nomor. PO',
					voucher_desc: 'Keterangan',
					car_no: 'Plat',
					remark: 'Remarkd',
					angk_date: 'Tgl. Bkr',
					expd_code: 'PBM',
					in_code: 'Type',
					item_qty: 'Qty',
				},
				sortable: ['no_dp', 'wh_code'],
				texts: {
					count: "",
					filter: "Cari",
					filterPlaceholder: "Cari No. DP",
				},
				templates: {

				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
			},
			listSO_rektur: [],
		}
	},
	methods: {
		getDataSORektur() {
			axios.post(this.action, {
				case: 'getDataSORektur',
				username: this.username,
				where: this.where,
				pin2: this.pin2
			}).then(response => {
				if (response.data != null) {
					this.listSO_rektur = response.data[0];
				} else {
					this.listSO_rektur = [];
				}
			});
		},
	},
	created() {
		this.getDataSORektur();
	}
});

