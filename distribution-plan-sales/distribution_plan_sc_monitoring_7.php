<?
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(-1);

$menu_code = 'sales';
$accGroup = get_acc_group($menu_code, $dbh);

if ( $accGroup['acc_menu'] ) :
    $messageBot = realpath(dirname(__FILE__))."/".basename(__FILE__)."%0A#distributionplan_belum_demo_".$username."%0A";
    $username = $_SESSION['username'];
    $menu_desc = $accGroup['menu_desc'];
    $acc_save = $accGroup['acc_save'];
    $acc_edit = $accGroup['acc_edit'];
    $acc_delete = $accGroup['acc_delete'];
    $acc_download = $accGroup['acc_download'];
    $location = $_SESSION['location'];

    $status = isset($_POST['status']) ? $_POST['status'] : 0;
    $shift = 'SAA&SNA';
    $date1 = '';
    $date2 = '';

    $salesCode = !$MobileApp ? 'OFFICE' : strtoupper($username);
    if($_SESSION['usergroup']=='SR') $salesCode = strtoupper($username);

    // DO KIRIM 
    //---------

    // $action = "actionDistributionPlans.php";
    // $production = true;
    // if ($production === false) {
    //     $action = "actionDistributionPlansz.php";
    // }

    //     // userlogs
    //     $datalogs = new stdClass();
    //     $datalogs->dbh = $dbh;
    //     $datalogs->menu_code = $menu_code;
    //     $datalogs->statuslogs = strtoupper($shift." - ".$date1." - ".$date2);
    //     userlogs($datalogs);

    // $rows = [];
    // $list_coce = listCostCenter($dbgl, $username);
    // $list_coce = "'".implode("', '", $list_coce)."'";
    // $where = " a.cabang IN (".$list_coce.")";
    // if ($location == 'TBN') {
    //     $where.= " AND a.tanggal >= '2020-01-06'";
    // }
	
	// if ($location == 'LTM') {
	// 	$where .= " AND a.tanggal >= '2020-11-24'";
	// }
	
    // if ($location == 'BPN') {
    //     $where.= " AND a.tanggal >= '2020-02-24'";
	// }
    
    // $query = '
    //     SELECT 
    //         a.code_comp, a.type, a.cabang, a.tanggal, a.jaminput, a.code, a.name, a.no, a.so_no, a.item_code, a.item_qty, a.item_qty2, 
    //         a.keterangan, a.item_ton, a.desa, a.alamat_toko, a.car_no, a.wh_code, d.address1 as alamat_promosi
    //         , 0 AS total_do, a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type
    //     FROM v_displan a
    //     LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp
    //     WHERE '.$where.' ORDER BY a.code, a.tanggal, a.no 
    // ';

    // sendMessageBot('74775504', urldecode($messageBot.$query), $botDeliveryPlan);
    // $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);

    // $reason = ['Stok', 'Truk', 'Gandengan', 'Plafond', 'TOP', 'Human Error', 'Permintaan Toko'];
    // if (strtoupper($_SESSION['username']) == 'ARIDARMA') {
    //     $reason[] = 'Uji Coba Sistem';
    // }

    // DO AMBIL
    //---------
    
    $query = "
        SELECT TRIM(a.wh_code) AS wh_code, a.wh_office FROM in_warehouse a 
        INNER JOIN in_warehouseuser b ON a.wh_code = b.wh_code AND b.userid = '".strtoupper($username)."'
        WHERE a.wh_status = '1'";
    $rowsWh = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
    //data so ambil
    $rows = [];
    $list_coce = listCostCenter($dbgl, $username);
    $list_coce = "'".implode("', '", $list_coce)."'";
    $where = " a.cabang IN (".$list_coce.")";
    if ($location == 'TBN') {
        $where.= " AND a.tanggal >= '2020-01-06'";
    }
    $query = "
        SELECT 
            a.code_comp, a.type, a.cabang, a.tanggal, a.jaminput, a.code, a.name, a.no, a.so_no, a.item_code, a.item_qty, a.item_qty2, 
            a.keterangan, a.item_ton, a.desa, a.alamat_toko, a.car_no, a.wh_code
            , 0 AS total_do, a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type
        FROM v_displan a
        --LEFT JOIN t_dp_detail c ON a.code_comp = c.code_comp and a.no = c.no and a.tanggal = DATE(c.tanggal::date)
        LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp
        WHERE ".$where." ORDER BY a.code, a.tanggal, a.no 
    ";

    // $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
    
?>

    <link rel="stylesheet" href="https://unpkg.com/vue-select@3.0.0/dist/vue-select.css">
    <link rel="stylesheet" href="../../cdn/vue/vue-toast/vue-toast.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.0/dist/sweetalert2.min.css">

    <style>
        /* Loader  */
        .lds-ripple {
            display: inline-block;
            position: relative;
            width: 80px;
            height: 80px;
            }
            .lds-ripple div {
            position: absolute;
            border: 4px solid #fff;
            opacity: 1;
            border-radius: 50%;
            animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
            }
            .lds-ripple div:nth-child(2) {
            animation-delay: -0.5s;
            }
            @keyframes lds-ripple {
            0% {
                top: 36px;
                left: 36px;
                width: 0;
                height: 0;
                opacity: 1;
            }
            100% {
                top: 0px;
                left: 0px;
                width: 72px;
                height: 72px;
                opacity: 0;
            }
            }


        /* ============== */
        .v-select .vs__dropdown-toggle {
            height: 34px!important;
            border-radius: 0px!important;
        }
        .pad-5-right {
            padding-right: 5px;
        }
        .pad-5-left {
            padding-left: 10px;
        }
        li.link {
            border: 1px solid #fff;
            border-radius: 4px;	
            margin-top: 5px;
            margin-bottom: 5px;
            margin-right: 5px;
            background-color: #5bc0de;
            color: #fff;
        }
        li.link:hover {
            background-color: #5dbcd0;
            color: #fff;
        }
        ul.navbar-nav li.link a {
            padding: 8px 18px;
            color: #fff;
        }
        ul.navbar-nav li.link a:hover {
            color: #fff;
        }
        th {
            padding: 6px 10px;
            text-transform: uppercase;
        }
        th.active .arrow {
            opacity: 1;
        }

        .arrow {
            display: inline-block;
            vertical-align: middle;
            width: 0;
            height: 0;
            margin-left: 5px;
            margin-top: -5px;
            opacity: 0.66;
            cursor: pointer;
        }

        .arrow.asc {
            border-left: 6px solid transparent;
            border-right: 6px solid transparent;
            border-bottom: 6px solid #42b983;
        }

        .arrow.dsc {
            border-left: 6px solid transparent;
            border-right: 6px solid transparent;
            border-top: 6px solid #42b983;
        }

        .VuePagination {
        text-align: center;
        }

        .vue-title {
        text-align: center;
        margin-bottom: 10px;
        }

        .vue-pagination-ad {
        text-align: center;
        }

        .glyphicon.glyphicon-eye-open {
        width: 16px;
        display: block;
        margin: 0 auto;
        }

        th:nth-child(3) {
        text-align: center;
        }

        .VueTables__child-row-toggler {
        width: 16px;
        height: 16px;
        line-height: 16px;
        display: block;
        margin: auto;
        text-align: center;
        }

        .VueTables__child-row-toggler--closed::before {
        content: "+";
        }

        .VueTables__child-row-toggler--open::before {
        content: "-";
        }

        .VueTables__heading {
            text-align: left;
        }

        [v-cloak] {
        display:none;
        }

        .lds-facebook {
            display: inline-block;
            position: relative;
            left: 48%;
            text-align: center;
            width: 64px;
            height: 74px;
        }
        .lds-facebook div {
            display: inline-block;
            position: absolute;
            left: 6px;
            width: 13px;
            background: rgba(39, 34, 229, 0.3);
            animation: lds-facebook 1.2s cubic-bezier(0, 0.5, 0.5, 1) infinite;
        }
        .lds-facebook div:nth-child(1) {
            left: 6px;
            animation-delay: -0.24s;
        }
        .lds-facebook div:nth-child(2) {
            left: 26px;
            animation-delay: -0.12s;
        }
        .lds-facebook div:nth-child(3) {
            left: 45px;
            animation-delay: 0;
        }
        @keyframes lds-facebook {
            0% {
                top: 6px;
                height: 61px;
            }
            50%, 100% {
                top: 19px;
                height: 36px;
            }
        }
    </style>

    <aside class="content-wrapper" id="app" v-cloak>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#" style="font-weight: bold;text-shadow: 1px 0px 1px #fff;"> SALES - RENCANA KIRIM</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#" @click="showList('daftar_so')">Daftar SO</a></li>
                        <li><a href="#" @click="showList('daftar_sm')">Daftar SM</a></li>
                        <li><a href="#" @click="showList('daftar_dp')">Daftar DP</a></li>
                        <li><a href="#" @click="showList('monitoring')">Monitoring</a></li>
                        <li><a href="#" @click="showList('retur')">Retur</a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>
        <section class="content">
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <div class="box box-solid">
                        <div class="box-body">
                            <?php //print_r($salesCode); ?>
                            <daf_so_kirim_comp_so v-if="view_daftar_so" :username="username" :where="where" :loc="loc" :acc_delete="acc_delete" :salesCode="salesCode"></daf_so_kirim_comp_so>
                            <!-- <daftar_sm_component v-if="view_daftar_sm" :username="username" :loc="loc" :where="where"></daftar_sm_component>  -->
                            <daf_so_kirim_comp_sm_less v-if="view_daftar_sm" :username="username" :loc="loc" :where="where"></daf_so_kirim_comp_sm_less> 
                            <daf_so_rektur_component v-if="view_retur" :username="username" :loc="loc"></daf_so_rektur_component>
                            <!-- <daftar_so_component v-if="view_daftar_so" :username="username" :where="where" :loc="loc"></daftar_so_component> -->
                            <!-- <distribution_plan_component v-if="view_distribution_plan" :username="username" :where="where" :usergroup="usergroup" :loc="loc"></distribution_plan_component> -->
                            <!-- <daftar-pengiriman-comp v-if="view_daftar_pengiriman" :loc="loc"></daftar-pengiriman-comp> -->
                            <!-- <h4><i class="fa fa-list"></i> Anda Belum Dapat Mengakses Halaman Ini</h4> -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </aside>


    <script src="../2.4.0/plugins/slimScroll/jquery.slimscroll.js"></script>
    <script src="../cdn/vue/vue.dev.js"></script>
    <script src="../cdn/vue/axios.min.js"></script>
    <script src="../cdn/vue/datepicker/datepicker.js"></script>
    <script src="../cdn/vue/datepicker/langid.js"></script>
    <script src="../cdn/vue/currency/currency.js"></script>
    <script src="../cdn/vue/vuelidate/validators.min.js"></script>
    <script src="../cdn/vue/vuelidate/vuelidate.min.js"></script>
    <script src="../cdn/vue/eventbus.js"></script>
    <script src="https://unpkg.com/vue-select@3.0.0"></script>
    <script src="../../cdn/vue/vue-toast/vue-toast.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-numeral-filter/dist/vue-numeral-filter.min.js"></script>
    <!-- <script src="https://unpkg.com/vue-currency-input"></script> -->
    <script src="https://unpkg.com/vue-toasted"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
    <script src="../cdn/vue-select/vue-moment.min.js"></script>
    <script src="../pluginz/vuejs/shared_components.js"></script>
    <script src="../cdn/vue/vue-table-2.js"></script>
    <script src="component/distribution_plan_sc_monitoring_7.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.0/dist/sweetalert2.all.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>

    <script>
        Vue.component('v-select', VueSelect.VueSelect);
	    Vue.use(VueTables.ClientTable);
        Vue.use(Toasted);
        Vue.use(VueCurrencyFilter, {
            symbol: "",
            thousandsSeparator: ".",
            fractionCount: 0,
            fractionSeparator: ",",
            symbolPosition: "front",
            symbolSpacing: false
        });
        // const pluginOptions = {
        //     /* see config reference */
        //     globalOptions: { currency: 'IDR', }
        // }
        // Vue.use(VueCurrencyInput, pluginOptions)
        
        let app = new Vue({
            el: '#app',
            data() {
                return {
                    text_judul: ' RENCANA PENGIRIMAN',
                    view_daftar_so: true,
                    view_daftar_sm: false,
                    view_distribution_plan: false,
                    view_monitoring: false,
                    view_retur: false,
                    username: '<?php echo $username; ?>',
                    where: `<?php echo $list_coce; ?>`,
                    // where: `<?php //echo "'GYR'" ?>`,
                    loc: `<?php echo $_SESSION['location']; ?>`,
                    usergroup: `<?php echo $_SESSION['usergroup']; ?>`,
                    acc_delete: <?php echo $acc_delete; ?>, 
                    salesCode: `<?php echo $salesCode; ?>`, 
                }
            },
            computed: {
                
            },
            methods: {
                showList(value) {
                    switch (value) {
                        case 'daftar_so':
                            this.text_judul = ' DAFTAR SO';
                            this.view_daftar_so = true;
                            this.view_daftar_sm = false;
                            this.view_distribution_plan = false;
                            this.view_monitoring = false;
                            this.view_retur = false;
                            location.reload();
                            break;
                        case 'daftar_sm':
                            this.text_judul = ' DAFTAR SM';
                            this.view_daftar_so = false;
                            this.view_daftar_sm = true;
                            this.view_distribution_plan = false;
                            this.view_monitoring = false;
                            this.view_retur = false;
                            break;
                        case 'daftar_dp':
                            this.text_judul = ' RENCANA DISTRIBUSI';
                            this.view_daftar_so = false;
                            this.view_daftar_sm = false;
                            this.view_distribution_plan = true;
                            this.view_monitoring = false;
                            this.view_retur = false;
                            break;
                        case 'monitoring':
                            this.text_judul = ' MONITORING';
                            this.view_daftar_so = false;
                            this.view_daftar_sm = false;
                            this.view_distribution_plan = false;
                            this.view_monitoring = true;
                            this.view_retur = false;
                            break;
                        case 'retur':
                            this.text_judul = ' RETUR';
                            this.view_daftar_so = false;
                            this.view_daftar_sm = false;
                            this.view_distribution_plan = false;
                            this.view_monitoring = false;
                            this.view_retur = true;
                            break;
                        default:
                            break;
                    }
                }	
            },
            watch: {
                
            },
            created() {
                			
            },
        });
    </script>

<? else : 
    header('location:..');
endif; 
?>
