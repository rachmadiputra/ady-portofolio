<?
    include_once 'invconfig.php';
    include_once '/var/www/web-bin/function.php';
    include_once '../function/function_sendmail.php';

    if (!isset($dbgl)) {
		try {
			$dbgl = new PDO($pggl, $pguser, $pgpass);
			$dbgl->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$dbgl->query("SET application_name = '".realpath(dirname(__FILE__))."/".basename(__FILE__)."/".$username."'");
		} catch (PDOException $e) {
			$errorMsg = 'DBGL '.$username.' '.$e->getMessage()."\nError on line ".$e->getLine()." in ".$e->getFile();
			sendMessageBot($groupWebChatId, $errorMsg, $webSupportBot);
			// throw new pdoDbException($e);
		}
	}

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(1);

    $GLOBALS['groupWebChatId']  = $groupWebChatId;
    $GLOBALS['botInventory']    = $botInventory;
    $GLOBALS['botWebSupport']   = $webSupportBot;
    $username   = $_SESSION['username'];
    // $location   = $_SESSION['location'];
    $menu_desc  = $accGroup['menu_desc'];
	$data = json_decode(file_get_contents("php://input"));
	
    // $data->location = $location;
    
    $filename = $_FILES['file']['name'];

    $case = $data->case? $data->case : $_POST['case'];
	
    switch ($case) {
        case 'getDataSOKirim': 
            $result = getDataSOKirim($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getDataSOKirimNew': 
            $result = getDataSOKirimNew($dbgl,$data);
            echo json_encode($result);
            break; 
        case 'getDataSOKirimSna': 
            $result = getDataSOKirimSna($dbgl,$data);
            echo json_encode($result);
            break; 
        case 'getDataSOAmbil': 
            $result = getDataSOAmbil($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getOfficeCode':
            $result = getOfficeCode($dbh,$data);
            echo json_encode($result);
            break;
        case 'saveDistributionPlanKirim': 
            $result = saveDistributionPlanKirim($dbgl,$data);
            echo json_encode($result);
            break;
        case 'saveDistributionPlanAmbil': 
            $result = saveDistributionPlanAmbil($dbgl,$data);
            echo json_encode($result);
            break;
        case 'simpanDistributionPlanKirim': 
            $result = actionSaveDistributionPlanKirim($dbgl,$data);
            echo json_encode($result);
            break; 
        case 'simpanDistributionPlanKirimLess': 
            $result = simpanDistributionPlanKirimLess($dbgl,$data);
            echo json_encode($result);
            break;
        case 'saveDpPrice': 
            $result = saveDpPrice($dbgl,$data);
            echo json_encode($result);
            break; 
        case 'simpanDistributionPlanAmbil':
            $result = actionSaveDistributionPlanAmbil($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getDisPlanListKirim': 
            $result = getDisPlanListKirim($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getDisPlanListAmbil': 
            $result = getDisPlanListAmbil($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getDisPlanListTransfer': 
            $result = getDisPlanListTransfer($dbgl,$data);
            echo json_encode($result);
            break;
        case 'saveProsesPengiriman': 
            $result = saveProsesPengiriman($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getDaftarPengirimanKirim': 
            $result = getDaftarPengirimanKirim($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getDaftarPengirimanKirimByID': 
            $result = getDaftarPengirimanKirimByID($dbgl,$data);
            echo json_encode($result); 
            break;
        case 'getDaftarPengirimanAmbil':
            $result = getDaftarPengirimanAmbil($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getDaftarPengirimanAmbilByID':
            $result = getDaftarPengirimanAmbilByID($dbgl,$data);
            echo json_encode($result);
            break; 
        case 'deleteDistributionPlan': 
            $result = deleteDistributionPlan($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getTruck': 
            $result = getTruck($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getSopir':
            $result = getSopir($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getGudang': 
            $result = getGudang($dbgl,$data);
            echo json_encode($result);
            break;
        case 'updateDistributionPlan': 
            $result = updateDistributionPlan($dbgl,$data);
            echo json_encode($result);
            break;
        case 'updateDistributionPlanAmbil': 
            $result = updateDistributionPlanAmbil($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getDisPlanKirimByDP': 
            $result = getDisPlanKirimByDP($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getDisPlanTransferByDP': 
            $result = getDisPlanTransferByDP($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getDisPlanAmbilByDP': 
            $result = getDisPlanAmbilByDP($dbgl,$data);
            echo json_encode($result);
            break;
        case 'updateDistributionPlanDetail': 
            $result = updateDistributionPlanDetail($dbgl,$data);
            echo json_encode($result);
            break; 
        case 'getNoTransfer': 
            $result = getNoTransfer($dbgl,$data);
            echo json_encode($result);
            break;
        case 'updateTransferDPKirim': 
            $result = updateTransferDPKirim($dbgl,$data);
            echo json_encode($result); 
            break;
        case 'updateProsesTransferDPKirim': 
            $result = updateProsesTransferDPKirim($dbgl,$data);
            echo json_encode($result);
            break; 
        case 'getDaftarPengirimanTransfer': 
            $result = getDaftarPengirimanTransfer($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getDaftarPengirimanTransferByID': 
            $result = getDaftarPengirimanTransferByID($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getKategori': 
            $result = getKategori($dbgl,$data);
            echo json_encode($result);
            break; 
        case 'getCabang': 
            $result = getCabang($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getGudangOutstanding': 
            $result = getGudangOutstanding($dbgl,$data,$dbh);
            echo json_encode($result);
            break;
        case 'getOutstanding': 
            $result = getOutstanding($dbgl,$data,$dbh);
            echo json_encode($result); 
            break;
        case 'getForceClose': 
            $result = getForceClose($dbgl,$data,$dbh);
            echo json_encode($result);
            break;
        case 'getSales': 
            $result = getSales($dbgl,$data,$dbh);
            echo json_encode($result);
            break;
        case 'sendEmail':
            $result = sendEmail($dbgl,$data,$dbh);
            echo json_encode($result); 
            break; 
        case 'getSupirByNopol':
            $result = getSupirByNopol($dbgl,$data,$dbh);
            echo json_encode($result); 
            break;
        case 'getRitaseByNopol':
            $result = getRitaseByNopol($dbgl,$data,$dbh);
            echo json_encode($result); 
            break;  
        case 'updateStatusSO':
            $result = updateStatusSO($dbgl,$data,$dbh);
            echo json_encode($result); 
            break; 
        case 'getDataSORektur':
            $result = getDataSORektur($dbgl,$data,$dbh);
            echo json_encode($result);  
            break; 
        case 'send_mail_approve':
            $result = send_mail_approve($dbgl,$data,$dbh);
            echo json_encode($result); 
            break; 
        case 'saveDistributionPlanTemp':
            $result = saveDistributionPlanTemp($dbgl,$data,$dbh);
            echo json_encode($result); 
            break;
        case 'recordRefresh':
            $result = recordRefresh($dbgl,$data,$dbh);
            echo json_encode($result); 
            break; 
        case 'getDataSMKirim':
            $result = getDataSMKirim($dbgl,$data,$dbh);
            echo json_encode($result);  
            break;  
        case 'getDataSMKirimByNo':
            $result = getDataSMKirimByNo($dbgl,$data,$dbh);
            echo json_encode($result);  
            break;  
        case 'deletedSM':
            $result = deletedSM($dbgl,$data,$dbh);
            echo json_encode($result); 
            break;  
        case 'getLastNoSM':
            $result = formatNo($dbgl,$data,$dbh);
            echo json_encode($result); 
            break;
        case 'saveData':
            $result = saveDataSM($dbgl,$data,$dbh);
            echo json_encode($result); 
            break;
        default:
            # code...
            break;
    }

    function saveDataSM($dbgl,$data,$dbh)
    {

        if (trim($data->cabang)=='') {
            $responseData = array(
                'statusMessage' => 'failed',
                'cabang' => $data->cabang,
                'message' => "Data tidak disimpan [".$data->salesFormNo."]"
            );
            echo json_encode($responseData);
            return;
        }

        return $data;
    }

    function formatNo($dbgl,$data,$dbh) {
        $tahun = date("Y", strtotime($data->orderDate));
        $reqPrdNo = date("y", strtotime($data->orderDate));
        $data->insertUser = isset($data->insertUser) && $data->insertUser != '' ? $data->insertUser : $data->username;
        $query = "SELECT COUNT(*) FROM t_person WHERE record_id = 'S' AND trim(nik) = trim(?)"; // 20190716
        $stmt = $dbgl->prepare($query); $stmt->execute([strtoupper($data->insertUser)]);
        $isSalesUser = $stmt->fetchColumn();
        $arrayNo = [':shift'=>$data->shift,':tahun'=>$tahun."%",':coceCode'=>$data->coceCode];
        if ($data->typeForm == 'titipan') { //1: Baru, 2: Titipan
            $query = "
                SELECT max(last_no) AS lastNo
                FROM sales_memo
                WHERE
                    kategori = :shift
                    AND periode LIKE :tahun
                    AND cabang = :coceCode
            ";
        } else {
            if ($isSalesUser) {
                $whereNo = " AND insert_user = :insertUser";
                $arrayNo[':insertUser'] = $data->insertUser;
            } else {
                $data->insertUser = 'OFFICE';
                $data->salesCode = 'OFFICE';
                $whereNo = " AND no LIKE 'OFFIC%'";
            }
            $tableSalesForm = $data->tableSalesForm;
            $query = "
                SELECT max(last_no) AS lastNo
                FROM {$tableSalesForm}
                WHERE
                    kategori = :shift
                    AND periode LIKE :tahun
                    AND cabang = :coceCode
            ".$whereNo;
        }
        try {
            $stmt = $dbgl->prepare($query); $stmt->execute($arrayNo);
            $lastNo = $stmt->fetchColumn();
        } catch (PDOException $e) {
            echo $e->getMessage(); return;
        }
        $lastNo = $lastNo + 1;
        if ($data->typeForm == 'titipan') {
            $lastNoText = $reqPrdNo * 1000000 + $lastNo;
            $noText = $data->coceCode.'/SM/'.$lastNoText;
        } else {
            $lastNoText = $reqPrdNo * 100000 + $lastNo;
            $username = strlen($data->salesCode) > 5 ? substr($data->salesCode, 0 , 5) : $data->salesCode;
            $noText = strtoupper($username).'/'.$lastNoText;
        }
        $responseData = array(
            'lastNo' => $lastNo,
            'noText' => $noText,
            'ismobile' => $_SESSION['MobileApp'],
            '$query' => $query
        );
        return $responseData;
    }

    function deletedSM($dbgl,$data,$dbh)
    {
        $so_no = $data->dt_so->so_no;
        $code_comp = $data->dt_so->code_comp;
        $cabang = $data->dt_so->cabang;
        $code = $data->dt_so->code;

        $qry = "SELECT id_temp from t_distribution_plan_temp2 tdpt 
                where so_no = '$so_no' AND code_comp = '$code_comp' and cabang = '$cabang' and code = '$code'";

        $res = $dbgl->query($qry)->fetchAll(PDO::FETCH_ASSOC);

        foreach ($res as $value) {
            $id_temp = $value['id_temp'];

            try {

                $query = "DELETE FROM t_distribution_plan_temp2 WHERE id_temp = $id_temp";
    
                $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
                $response1 = array(
                    'result' => 1,
                    "message" => "Data Berhasil Di Hapus"
                );
    
            } catch (PDOException $e) {
                $response1 = array(
                    'result' => 0,
                    "message" => $e
                );
            }
        }

    
        return $response1;
    }

    function getDataSMKirim($dbgl,$data)
    {   
        $q = "SELECT a.code_comp, a.type, a.cabang, 
        a.tanggal, a.jaminput, a.code, 
        a.name, a.no, a.so_no, a.item_code, 
        a.item_qty, a.item_qty2, a.keterangan, 
        a.item_ton, a.desa, a.alamat_toko, 
        a.car_no, a.wh_code, d.address1 as alamat_promosi , 0 AS total_do, 
        a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type, a.sales_code,
        (SELECT wh_office FROM in_warehouse WHERE wh_code = a.kp_code) AS kp_office,
        CONCAT(d.name,'-',a.name) AS ledgername, a.pr_stat, to_char(a.delivery_time, 'YYYY-MM-DD ') AS delivery_time,
        CASE
            WHEN a.item_qty2 < a.item_qty THEN 'Outstanding'
            ELSE 'Langsung'
        END AS tipe_muat
        FROM v_displan2 a 
        LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp 
        left join sales_memo c on c.no = a.no and upper(c.kategori) = a.code_comp and c.ledger_code = a.code 
        WHERE 
        a.type = 'SM'
        AND 
            a.cabang IN (".$data->where.") or c.kp_code in (".$data->where.")
        AND a.freight_type = 'kirim'
        ORDER BY a.code_comp ASC";

        $res = $dbgl->query($q)->fetchAll(PDO::FETCH_ASSOC);

        $arrayItem = [];
        foreach ($res as $key => $row) {
            // print_r($row);

            $so = $row['so_no'].$row['no'].$row['code_comp'];

            if (isset($arrayItem[$so])) {
                $arrayItem[$so]['item_code'] .= '['.$row['item_code'].' - '.$row['item_qty2'].']';
                $arrayItem[$so]['total_ton'] .= $row['item_ton'].',';
                unset($res[$key]);
            } else {
                $arrayItem[$so] = [
                    'item_code' => '['.$row['item_code'].' - '.$row['item_qty2'].']',
                    'total_ton' => $row['item_ton'].',',
                ];
            }
        }

        foreach ($res as $row) {
            $so = $row['so_no'].$row['no'].$row['code_comp'];
            
            $ledgerName = $row['name'];
            if ($row['dept_code'] <> NULL || $row['dept_code'] <> '') {
                $ledgerName = $row['dept_name'].' - '.$ledgerName;
                $row['alamat_toko'] = $row['alamat_promosi'];
            }

            if($row['type'] == 'SO' && $row['ot_flag'] == 1) {
                continue;
            }

            if ($row['freight_type'] == 'ambil') {
                continue;
            }

            $hasil[] = array(
                'type' => $row['type'], 
                'code' => $row['code'],
                'ledgername' => $ledgerName, 
                'tanggal' => $row['tanggal'],
                'sm_no' => $row['type'] == 'SM' ? $row['no'] : '',
                'so_no' => $row['so_no'], 
                'item_code' => $arrayItem[$so]['item_code'], 
                'alamat_toko' => $row['alamat_toko'], 
                'keterangan' => $row['keterangan'], 
                'jaminput' => $row['jaminput'], 
                'code_comp' => $row['code_comp'],
                'sales_code' => $row['sales_code'],
                'cabang' => $row['cabang'],
                'freight_type' => $row['freight_type'],
                'dept_code' => $row['dept_code'],
                'type_so' => 'cabang_int',
                'kp_office' => $row['kp_office'],
                'pr_stat' => $row['pr_stat'], 
                'delivery_time' => $row['delivery_time'],
                'tipe_muat' => $row['tipe_muat'], 
                'total_do' => $row['total_do'], 
                'alamat_promosi' => $row['alamat_promosi'], 
            );
        }

    
        $newHasil = [];
        foreach ($hasil as $value) {

            $type  = $value['type'];
            $sm_no = $value['sm_no'];
            $so_no = $value['so_no'];

            if ($type == 'SM') {

                $qry = "SELECT DISTINCT 'SM'::text AS type,
                            a.cabang,
                            c.kp_code,
                            a.order_date AS tanggal,
                            to_char(a.insert_date, 'HH24:MI:SS'::text) AS jaminput,
                            a.no,
                            a.so_no,
                            a.ledger_code AS code,
                            a.item_code,
                            c.item_qty::double precision AS item_qty,
                            a.item_qty::double precision AS item_qty2,
                            upper(a.keterangan::text) AS keterangan,
                            upper(a.kategori::text) AS code_comp,
                            (a.item_qty * b.item_weight / 1000000::numeric)::double precision AS item_ton,
                            a.desa,
                            c.ref_no AS sales_form_no,
                            d.name,
                            d.address1 AS alamat_toko,
                            d.sales_code,
                            ''::text AS car_no,
                            ''::text AS wh_code,
                            ''::text AS dept_code,
                            a.ot_flag,
                            a.freight_type,
                            c.pr_stat,
                            c.delivery_time,
                            c.close_by,
                            c.close_date,
                            c.item_qty_ton,
                            b.item_weight
                        FROM sales_memo a
                            LEFT JOIN t_dp_detail e ON upper(a.kategori::text) = e.code_comp AND a.no = e.no AND a.so_no = e.so_no
                            JOIN po_req c ON a.so_no = c.pr_no AND a.item_code::bpchar = c.item_code AND upper(a.kategori::text) = c.code_comp::text
                            JOIN t_card d ON a.ledger_code::bpchar = d.code AND upper(a.kategori::text) = d.code_comp::text
                            JOIN t_item b ON a.item_code::bpchar = b.item_code AND upper(a.kategori::text) = b.code_comp::text
                        WHERE a.so_no IS NOT NULL AND a.status = '1'::bpchar AND e.id IS NULL AND date(c.pr_date) >= date(now() - '1000 days'::interval) AND c.pr_stat = 'A'::bpchar AND btrim(c.pending_stat::text) <> 'F'::text AND a.item_qty <= c.item_qty2 AND c.item_qty2 <> 0::numeric AND a.do_no IS NULL
                        AND a.no = '$sm_no'";
            } else {

                $qry = "SELECT DISTINCT 'SO'::text AS type,
                            a.spk_no AS cabang,
                            a.kp_code,
                            a.pr_date::date AS tanggal,
                            to_char(a.entry_date, 'HH24:MI:SS'::text) AS jaminput,
                            a.pr_no AS no,
                            a.pr_no AS so_no,
                            a.vend_code AS code,
                            a.item_code,
                            a.item_qty::double precision AS item_qty,
                            a.item_qty2::double precision AS item_qty2,
                            (upper(a.pr_desc::text) || ' '::text) || to_char(a.delivery_time, 'YYYY-MM-DD'::text) AS keterangan,
                            a.code_comp,
                            (a.item_qty2 * b.item_weight / 1000000::numeric)::double precision AS item_ton,
                            c.desa,
                            c.no AS sales_form_no,
                            d.name,
                            d.address1 AS alamat_toko,
                            d.sales_code,
                            ''::text AS car_no,
                            ''::text AS wh_code,
                            a.dept_code,
                            c.ot_flag,
                            lower(a.car_no::text) AS freight_type,
                            a.pr_stat,
                            a.delivery_time,
                            a.close_by,
                            a.close_date,
                            a.item_qty_ton,
                            b.item_weight
                            FROM po_req a
                                LEFT JOIN t_dp_detail e ON a.code_comp::text = e.code_comp AND a.pr_no = e.no AND a.item_code = e.item_code::bpchar
                                JOIN t_card d ON a.vend_code = d.code AND a.code_comp = d.code_comp
                                JOIN t_item b ON a.item_code = b.item_code AND a.code_comp = b.code_comp
                                LEFT JOIN sales_form c ON a.ref_no = c.no AND a.code_comp::text = upper(c.kategori::text) AND a.spk_no::bpchar = c.cabang
                            WHERE (e.id IS NULL OR e.status = '1'::bpchar) AND date(a.pr_date) >= date(now() - '120 days'::interval) AND a.pr_stat = 'A'::bpchar AND btrim(a.pending_stat::text) <> 'F'::text AND a.pr_type = 'S'::bpchar AND a.item_qty2 <> 0::numeric
                            AND a.pr_no = '$so_no'";

            }

            // $qry = "SELECT a.pr_no, TRIM(a.item_code) as item_code, a.item_qty::integer, a.item_qty2::integer, b.item_weight, a.price_amt 
            //         FROM po_req a 
            //         -- LEFT JOIN t_dp_detail e ON a.code_comp::text = e.code_comp AND a.pr_no = e.no AND a.item_code = e.item_code::bpchar
            //         JOIN t_card d ON a.vend_code = d.code AND a.code_comp = d.code_comp
            //         JOIN t_item b ON a.item_code = b.item_code AND a.code_comp = b.code_comp
            //         WHERE date(a.pr_date) >= date(now() - '120 days'::interval) 
            //         AND a.pr_stat = 'A' 
            //         AND btrim(a.pending_stat::text) <> 'F'
            //         AND a.pr_type = 'S'::bpchar AND a.item_qty2 <> 0::numeric 
            //         -- AND (e.id IS NULL OR e.status = '1'::bpchar) 
            //         AND a.pr_no = '$so_no'";
            
            $rq = $dbgl->query($qry)->fetchAll(PDO::FETCH_ASSOC);

            $newHasil[] = array(
                'type' => $value['type'], 
                'code' => $value['code'],
                'ledgername' => $value['ledgername'], 
                'tanggal' => $value['tanggal'],
                'sm_no' => $value['sm_no'],
                'so_no' => $value['so_no'], 
                'item_code' => $value['item_code'], 
                'alamat_toko' => $value['alamat_toko'], 
                'keterangan' => $value['keterangan'], 
                'jaminput' => $value['jaminput'], 
                'code_comp' => $value['code_comp'],
                'sales_code' => $value['sales_code'],
                'cabang' => $value['cabang'],
                'freight_type' => $value['freight_type'],
                'dept_code' => $value['dept_code'],
                'type_so' => 'cabang_int',
                'kp_office' => $value['kp_office'],
                'pr_stat' => $value['pr_stat'], 
                'delivery_time' => $value['delivery_time'],
                'tipe_muat' => $value['tipe_muat'],
                'total_do' => $value['total_do'],
                'alamat_promosi' => $value['alamat_promosi'],
                'detail' => $rq,
            );
            $rq = [];
        }

        foreach($newHasil as $val) {
            $tonase = getTonase($val['detail']);

            $result[] = array(
                "type" => $val['type'],
                "code" => $val['code'],
                "ledgername" => $val['ledgername'],
                "tanggal" => $val['tanggal'],
                "sm_no" => $val['sm_no'],
                "so_no" => $val['so_no'],
                "alamat_toko" => $val['alamat_toko'],
                "keterangan" => $val['keterangan'],
                "jaminput" => $val['jaminput'],
                "code_comp" => $val['code_comp'],
                "sales_code" => $val['sales_code'],
                "cabang" => $val['cabang'],
                "freight_type" => $val['freight_type'],
                "dept_code" => $val['dept_code'],
                "type_so" => $val['type_so'],
                "kp_office" => $val['kp_office'],
                "pr_stat" => $val['pr_stat'],
                "delivery_time" => $val['delivery_time'],
                "tipe_muat" => $val['tipe_muat'],
                "ton" => $tonase,
                "item_code" => $val['item_code'],
                "total_do" => $val['total_do'],
                "alamat_promosi" => $val['alamat_promosi'],
                "detail" => $val['detail']
            );
            $tonase = [];
        }

        return $result;
    }

    function getDataSMKirimByNo($dbgl,$data)
    {   
        // $q = "SELECT a.code_comp, a.type, a.cabang, 
        // a.tanggal, a.jaminput, a.code, 
        // a.name, a.no, a.so_no, a.item_code, 
        // a.item_qty, a.item_qty2, a.keterangan, 
        // a.item_ton, a.desa, a.alamat_toko, 
        // a.car_no, a.wh_code, d.address1 as alamat_promosi , 0 AS total_do, 
        // a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type, a.sales_code,
        // (SELECT wh_office FROM in_warehouse WHERE wh_code = a.kp_code) AS kp_office,
        // CONCAT(d.name,'-',a.name) AS ledgername, a.pr_stat, to_char(a.delivery_time, 'YYYY-MM-DD ') AS delivery_time,
        // CASE
        //     WHEN a.item_qty2 < a.item_qty THEN 'Outstanding'
        //     ELSE 'Langsung'
        // END AS tipe_muat
        // FROM v_displan2 a 
        // LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp 
        // WHERE a.cabang IN (".$data->where.")
        // AND a.freight_type = 'kirim'
        // AND a.type = 'SM'
        // AND a.no = '$data->sm_no'
        // ORDER BY a.code_comp ASC";

        $q = "SELECT a.code_comp, a.type, a.cabang, a.term,
                a.tanggal, a.jaminput, a.code, 
                a.name, a.no, a.so_no, a.item_code, 
                a.item_qty, a.item_qty2, a.keterangan, 
                a.item_ton, a.desa, a.alamat_toko, 
                a.car_no, a.wh_code, d.address1 as alamat_promosi , 0 AS total_do, 
                a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type, a.sales_code,
                (SELECT wh_office FROM in_warehouse WHERE wh_code = a.kp_code) AS kp_office,
                CONCAT(d.name,'-',a.name) AS ledgername, a.pr_stat, to_char(a.delivery_time, 'YYYY-MM-DD ') AS delivery_time,
                CASE
                    WHEN a.item_qty2 < a.item_qty THEN 'Outstanding'
                    ELSE 'Langsung'
                END AS tipe_muat,
                a.print_soa,
                CASE 
                    WHEN e.ot_flag_so = '1' THEN 'T'
                    ELSE 'L'
                END AS tipe,
                e.ot_flag_so,
                f.kp_code,
                a.idno1
                FROM v_displan2 a 
                LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp
                INNER JOIN v_po_req_header e ON a.so_no = e.pr_no AND a.spk_no = e.spk_no
                left join v_po_req_sales_form f on f.pr_no = a.so_no and f.kp_code is not null and f.code_comp = a.code_comp and f.vend_code = a.code 
                WHERE a.cabang IN (".$data->where.")
                AND a.freight_type = 'kirim'
                AND a.type = 'SM'
                AND a.no = '$data->sm_no'
                union all 
                SELECT a.code_comp, a.type, a.cabang, a.term,
                a.tanggal, a.jaminput, a.code, 
                a.name, a.no, a.so_no, a.item_code, 
                a.item_qty, a.item_qty2, a.keterangan, 
                a.item_ton, a.desa, a.alamat_toko, 
                a.car_no, a.wh_code, d.address1 as alamat_promosi , 0 AS total_do, 
                a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type, a.sales_code,
                (SELECT wh_office FROM in_warehouse WHERE wh_code = a.kp_code) AS kp_office,
                CONCAT(d.name,'-',a.name) AS ledgername, a.pr_stat, to_char(a.delivery_time, 'YYYY-MM-DD ') AS delivery_time,
                CASE
                    WHEN a.item_qty2 < a.item_qty THEN 'Outstanding'
                    ELSE 'Langsung'
                END AS tipe_muat,
                a.print_soa,
                CASE 
                    WHEN e.ot_flag_so = '1' THEN 'T'
                    ELSE 'L'
                END AS tipe,
                e.ot_flag_so,
                f.kp_code,
                a.idno1
                FROM v_displan2 a 
                LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp
                INNER JOIN v_po_req_header e ON a.so_no = e.pr_no AND a.spk_no = e.spk_no
                left join v_po_req_sales_form f on f.pr_no = a.so_no and f.kp_code is not null and f.code_comp = a.code_comp and f.vend_code = a.code 
                WHERE f.kp_code in (".$data->where.")
                AND a.freight_type = 'kirim'
                AND a.type = 'SM'
                AND a.no = '$data->sm_no'";

        $res = $dbgl->query($q)->fetchAll(PDO::FETCH_ASSOC);

        $arrayItem = [];
        foreach ($res as $key => $row) {
            // print_r($row);

            $so = $row['so_no'].$row['no'].$row['code_comp'];

            if (isset($arrayItem[$so])) {
                $arrayItem[$so]['item_code'] .= '['.$row['item_code'].' - '.$row['item_qty2'].']';
                $arrayItem[$so]['total_ton'] .= $row['item_ton'].',';
                unset($res[$key]);
            } else {
                $arrayItem[$so] = [
                    'item_code' => '['.$row['item_code'].' - '.$row['item_qty2'].']',
                    'total_ton' => $row['item_ton'].',',
                ];
            }
        }

        foreach ($res as $row) {
            $so = $row['so_no'].$row['no'].$row['code_comp'];
            
            $ledgerName = $row['name'];
            if ($row['dept_code'] <> NULL || $row['dept_code'] <> '') {
                $ledgerName = $row['dept_name'].' - '.$ledgerName;
                $row['alamat_toko'] = $row['alamat_promosi'];
            }

            if($row['type'] == 'SO' && $row['ot_flag'] == 1) {
                continue;
            }

            if ($row['freight_type'] == 'ambil') {
                continue;
            }

            $hasil[] = array(
                'type' => $row['type'], 
                'code' => $row['code'],
                'ledgername' => $ledgerName, 
                'tanggal' => $row['tanggal'],
                'sm_no' => $row['type'] == 'SM' ? $row['no'] : '',
                'so_no' => $row['so_no'], 
                'item_code' => $arrayItem[$so]['item_code'], 
                'alamat_toko' => $row['alamat_toko'], 
                'keterangan' => $row['keterangan'], 
                'jaminput' => $row['jaminput'], 
                'code_comp' => $row['code_comp'],
                'sales_code' => $row['sales_code'],
                'cabang' => $row['cabang'],
                'freight_type' => $row['freight_type'],
                'dept_code' => $row['dept_code'],
                'type_so' => 'cabang_int',
                'kp_office' => $row['kp_office'],
                'pr_stat' => $row['pr_stat'], 
                'delivery_time' => $row['delivery_time'],
                'tipe_muat' => $row['tipe_muat'], 
                'total_do' => $row['total_do'], 
                'alamat_promosi' => $row['alamat_promosi'], 
                'kpCode' => $row['kp_code'],
            );
        }

    
        $newHasil = [];
        foreach ($hasil as $value) {

            $type  = $value['type'];
            $sm_no = $value['sm_no'];
            $so_no = $value['so_no'];

            if ($type == 'SM') {

                $qry = "SELECT DISTINCT 'SM'::text AS type,
                            a.cabang,
                            c.kp_code,
                            a.order_date AS tanggal,
                            to_char(a.insert_date, 'HH24:MI:SS'::text) AS jaminput,
                            a.no,
                            a.so_no,
                            a.ledger_code AS code,
                            a.item_code,
                            c.item_qty::double precision AS item_qty,
                            a.item_qty::double precision AS item_qty2,
                            upper(a.keterangan::text) AS keterangan,
                            upper(a.kategori::text) AS code_comp,
                            (a.item_qty * b.item_weight / 1000000::numeric)::double precision AS item_ton,
                            a.desa,
                            c.ref_no AS sales_form_no,
                            d.name,
                            d.address1 AS alamat_toko,
                            d.sales_code,
                            ''::text AS car_no,
                            ''::text AS wh_code,
                            ''::text AS dept_code,
                            a.ot_flag,
                            a.freight_type,
                            c.pr_stat,
                            c.delivery_time,
                            c.close_by,
                            c.close_date,
                            c.item_qty_ton,
                            b.item_weight
                        FROM sales_memo a
                            LEFT JOIN t_dp_detail e ON upper(a.kategori::text) = e.code_comp AND a.no = e.no AND a.so_no = e.so_no
                            JOIN po_req c ON a.so_no = c.pr_no AND a.item_code::bpchar = c.item_code AND upper(a.kategori::text) = c.code_comp::text
                            JOIN t_card d ON a.ledger_code::bpchar = d.code AND upper(a.kategori::text) = d.code_comp::text
                            JOIN t_item b ON a.item_code::bpchar = b.item_code AND upper(a.kategori::text) = b.code_comp::text
                        WHERE a.so_no IS NOT NULL AND a.status = '1'::bpchar AND e.id IS NULL AND date(c.pr_date) >= date(now() - '1000 days'::interval) AND c.pr_stat = 'A'::bpchar AND btrim(c.pending_stat::text) <> 'F'::text AND a.item_qty <= c.item_qty2 AND c.item_qty2 <> 0::numeric AND a.do_no IS NULL
                        AND a.no = '$sm_no'";
            } else {

                $qry = "SELECT DISTINCT 'SO'::text AS type,
                            a.spk_no AS cabang,
                            a.kp_code,
                            a.pr_date::date AS tanggal,
                            to_char(a.entry_date, 'HH24:MI:SS'::text) AS jaminput,
                            a.pr_no AS no,
                            a.pr_no AS so_no,
                            a.vend_code AS code,
                            a.item_code,
                            a.item_qty::double precision AS item_qty,
                            a.item_qty2::double precision AS item_qty2,
                            (upper(a.pr_desc::text) || ' '::text) || to_char(a.delivery_time, 'YYYY-MM-DD'::text) AS keterangan,
                            a.code_comp,
                            (a.item_qty2 * b.item_weight / 1000000::numeric)::double precision AS item_ton,
                            c.desa,
                            c.no AS sales_form_no,
                            d.name,
                            d.address1 AS alamat_toko,
                            d.sales_code,
                            ''::text AS car_no,
                            ''::text AS wh_code,
                            a.dept_code,
                            c.ot_flag,
                            lower(a.car_no::text) AS freight_type,
                            a.pr_stat,
                            a.delivery_time,
                            a.close_by,
                            a.close_date,
                            a.item_qty_ton,
                            b.item_weight
                            FROM po_req a
                                LEFT JOIN t_dp_detail e ON a.code_comp::text = e.code_comp AND a.pr_no = e.no AND a.item_code = e.item_code::bpchar
                                JOIN t_card d ON a.vend_code = d.code AND a.code_comp = d.code_comp
                                JOIN t_item b ON a.item_code = b.item_code AND a.code_comp = b.code_comp
                                LEFT JOIN sales_form c ON a.ref_no = c.no AND a.code_comp::text = upper(c.kategori::text) AND a.spk_no::bpchar = c.cabang
                            WHERE (e.id IS NULL OR e.status = '1'::bpchar) AND date(a.pr_date) >= date(now() - '120 days'::interval) AND a.pr_stat = 'A'::bpchar AND btrim(a.pending_stat::text) <> 'F'::text AND a.pr_type = 'S'::bpchar AND a.item_qty2 <> 0::numeric
                            AND a.pr_no = '$so_no'";

            }

            // $qry = "SELECT a.pr_no, TRIM(a.item_code) as item_code, a.item_qty::integer, a.item_qty2::integer, b.item_weight, a.price_amt 
            //         FROM po_req a 
            //         -- LEFT JOIN t_dp_detail e ON a.code_comp::text = e.code_comp AND a.pr_no = e.no AND a.item_code = e.item_code::bpchar
            //         JOIN t_card d ON a.vend_code = d.code AND a.code_comp = d.code_comp
            //         JOIN t_item b ON a.item_code = b.item_code AND a.code_comp = b.code_comp
            //         WHERE date(a.pr_date) >= date(now() - '120 days'::interval) 
            //         AND a.pr_stat = 'A' 
            //         AND btrim(a.pending_stat::text) <> 'F'
            //         AND a.pr_type = 'S'::bpchar AND a.item_qty2 <> 0::numeric 
            //         -- AND (e.id IS NULL OR e.status = '1'::bpchar) 
            //         AND a.pr_no = '$so_no'";
            
            $rq = $dbgl->query($qry)->fetchAll(PDO::FETCH_ASSOC);

            $newHasil[] = array(
                'type' => $value['type'], 
                'code' => $value['code'],
                'ledgername' => $value['ledgername'], 
                'tanggal' => $value['tanggal'],
                'sm_no' => $value['sm_no'],
                'so_no' => $value['so_no'], 
                'item_code' => $value['item_code'], 
                'alamat_toko' => $value['alamat_toko'], 
                'keterangan' => $value['keterangan'], 
                'jaminput' => $value['jaminput'], 
                'code_comp' => $value['code_comp'],
                'sales_code' => $value['sales_code'],
                'cabang' => $value['cabang'],
                'freight_type' => $value['freight_type'],
                'dept_code' => $value['dept_code'],
                'type_so' => 'cabang_int',
                'kp_office' => $value['kp_office'],
                'pr_stat' => $value['pr_stat'], 
                'delivery_time' => $value['delivery_time'],
                'tipe_muat' => $value['tipe_muat'],
                'total_do' => $value['total_do'],
                'alamat_promosi' => $value['alamat_promosi'],
                'detail' => $rq,
                'kpCode' => $row['kpCode'],
            );
            $rq = [];
        }

        foreach($newHasil as $val) {
            $tonase = getTonase($val['detail']);

            $result[] = array(
                "type" => $val['type'],
                "code" => $val['code'],
                "ledgername" => $val['ledgername'],
                "tanggal" => $val['tanggal'],
                "sm_no" => $val['sm_no'],
                "so_no" => $val['so_no'],
                "alamat_toko" => $val['alamat_toko'],
                "keterangan" => $val['keterangan'],
                "jaminput" => $val['jaminput'],
                "code_comp" => $val['code_comp'],
                "sales_code" => $val['sales_code'],
                "cabang" => $val['cabang'],
                "freight_type" => $val['freight_type'],
                "dept_code" => $val['dept_code'],
                "type_so" => $val['type_so'],
                "kp_office" => $val['kp_office'],
                "pr_stat" => $val['pr_stat'],
                "delivery_time" => $val['delivery_time'],
                "tipe_muat" => $val['tipe_muat'],
                "ton" => $tonase,
                "item_code" => $val['item_code'],
                "total_do" => $val['total_do'],
                "alamat_promosi" => $val['alamat_promosi'],
                "detail" => $val['detail']
            );
            $tonase = [];
        }

        return $result;
    }

    function saveDistributionPlanSM($dbgl,$data,$dbh)
    {

        for ($i=0; $i < count($data->dt_so); $i++) {

            $code_comp = $data->dt_so[$i]->code_comp;
            $type = $data->dt_so[$i]->type;
            $cabang = trim($data->dt_so[$i]->cabang);
            $tanggal = $data->dt_so[$i]->tanggal;
            $code = $data->dt_so[$i]->code;
            $name = $data->dt_so[$i]->ledgername;
            if($data->dt_so[$i]->sm_no != "") { $no = $data->dt_so[$i]->sm_no; } else { $no = $data->dt_so[$i]->so_no; }
            $so_no = $data->dt_so[$i]->so_no;
            $item_code = $data->dt_so[$i]->item_code;
            $item_qty = $data->dt_so[$i]->item_qty;
            $item_qty2 = $data->dt_so[$i]->item_qty2;
            $keterangan = $data->dt_so[$i]->keterangan;
            $item_ton = $data->dt_so[$i]->item_ton;
            $total_do = $data->dt_so[$i]->total_do;
            $freight_type = $data->dt_so[$i]->freight_type;
            $sales_code = $data->dt_so[$i]->sales_code;
            $kp_office = $data->dt_so[$i]->kp_office;
            $ledgername = $data->dt_so[$i]->ledgername;
            $pr_stat = $data->dt_so[$i]->pr_stat;
            $delivery_time = $data->dt_so[$i]->delivery_time;
            $jaminput = $data->dt_so[$i]->jaminput;
            $code_comp = $data->dt_so[$i]->code_comp;
            $alamat_toko = $data->dt_so[$i]->alamat_toko;
            $alamat_promosi = $data->dt_so[$i]->alamat_promosi;
            $now            = date('Y-m-d H:i:s');

            try {
                $query[] = "INSERT INTO t_distribution_plan_temp2(code_comp,type,cabang,tanggal,code,name,no,so_no,item_code,
                                                                item_qty,item_qty2,keterangan,item_ton,total_do,freight_type,
                                                                sales_code,kp_office,ledgername,pr_stat,delivery_time,jaminput,sc_status,asal_gudang,alamat_toko,alamat_promosi,insert_date) 
                            VALUES ('$code_comp','$type','$cabang','$tanggal','$code','$name','$no','$so_no','$item_code',
                                                                '$item_qty','$item_qty2','$keterangan','$item_ton','$total_do','$freight_type',
                                                                '$sales_code','$kp_office','$ledgername','$pr_stat','$delivery_time','$jaminput','0','$data->office','$alamat_toko','$alamat_promosi','$now')";

                // $stmt = $dbgl->prepare($query);
                // $stmt->execute();
                $response = [
                    'success' => 1,
                    'response' => 'Data Berhasil Disimpan',
                    'sql' => $query,
                ];
            } catch (PDOException $e) {
                $response = [
                    'success' => 0, 
                    'response' => 'Gagal Simpan Data Silahkan Coba Kembali',
                    'error' => $e
                ];
            }

        }

        return $response;
    }

    function recordRefresh($dbgl,$data,$dbh)
    {
        $now            = date('Y-m-d H:i:s');
        $username       = $data->username;
        $source         = $data->source;

        $query = "INSERT INTO t_dp_refresh(source, created_at, insert_by) VALUES (:source, :created_at, :insert_by)";
        $stmt = $dbgl->prepare($query);
        $dataheader = [
            ':source' => $source,
            ':created_at' => $now,
            ':insert_by' => $username
        ];

        try {
            $stmt->execute($dataheader);
            $response = [
                'success' => 1,
                'response' => 'Refresh Inserted',
            ];
        } catch (PDOException $e) {
            $response = [
                'success' => 0, 
                'response' => 'Refresh Failed',
                'error' => $e
            ];
        }

        return $response;
    }
    
    function generateNoDp($dbgl,$data) 
    {
        $dpPeriode = "DP/".date('ym');
        $sql = "SELECT no_dp FROM t_distribution_plan WHERE no_dp LIKE '".$dpPeriode."%' ORDER BY id DESC LIMIT 1";
        $noDp = '';
        try {
            $stmt = $dbgl->query($sql);
            $result = $stmt->fetchColumn();
            if ($result === false) {
                $noDp = $dpPeriode.'/000001';
            } else {
                $noDp = (int) substr($result, 8);
                $noDp = $noDp + 1;
                $noDp = $dpPeriode."/".sprintf("%06s", $noDp);
            }

        } catch (PDOException $e) {
            $noDp = $e;    
        }

        return $noDp;
    }

    function getDataSOKirim($dbgl,$data)
    {   
        $q = "SELECT a.code_comp, a.type, a.cabang, a.term,
                a.tanggal, a.jaminput, a.code, 
                a.name, a.no, a.so_no, a.item_code, 
                a.item_qty, a.item_qty2, a.keterangan, 
                a.item_ton, a.desa, a.alamat_toko, 
                a.car_no, a.wh_code, d.address1 as alamat_promosi , 0 AS total_do, 
                a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type, a.sales_code,
                (SELECT wh_office FROM in_warehouse WHERE wh_code = a.kp_code) AS kp_office,
                CONCAT(d.name,'-',a.name) AS ledgername, a.pr_stat, to_char(a.delivery_time, 'YYYY-MM-DD ') AS delivery_time,
                CASE
                    WHEN a.item_qty2 < a.item_qty THEN 'Outstanding'
                    ELSE 'Langsung'
                END AS tipe_muat,
                a.print_soa,
                CASE 
                    WHEN e.ot_flag_so = '1' THEN 'T'
                    ELSE 'L'
                END AS tipe,
                e.ot_flag_so,
                f.kp_code,
                a.idno1
                FROM v_displan2 a 
                LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp
                INNER JOIN v_po_req_header e ON a.so_no = e.pr_no AND a.spk_no = e.spk_no
                left join v_po_req_sales_form f on f.pr_no = a.so_no and f.kp_code is not null and f.code_comp = a.code_comp and f.vend_code = a.code 
                WHERE a.cabang IN (".$data->where.") or f.kp_code in (".$data->where.")
                AND a.freight_type = 'kirim'
                AND a.type = 'SO'
                ORDER BY a.code_comp asc";

        $res = $dbgl->query($q)->fetchAll(PDO::FETCH_ASSOC);

        $arrayItem = [];
        foreach ($res as $key => $row) {
            // print_r($row);

            $so = $row['so_no'].$row['no'].$row['code_comp'];

            if (isset($arrayItem[$so])) {
                $arrayItem[$so]['item_code'] .= '['.$row['item_code'].' - '.$row['item_qty2'].']';
                $arrayItem[$so]['total_ton'] .= $row['item_ton'].',';
                unset($res[$key]);
            } else {
                $arrayItem[$so] = [
                    'item_code' => '['.$row['item_code'].' - '.$row['item_qty2'].']',
                    'total_ton' => $row['item_ton'].',',
                ];
            }
        }

        foreach ($res as $row) {
            $so = $row['so_no'].$row['no'].$row['code_comp'];
            
            $ledgerName = $row['name'];
            if ($row['dept_code'] <> NULL || $row['dept_code'] <> '') {
                $ledgerName = $row['dept_name'].' - '.$ledgerName;
                $row['alamat_toko'] = $row['alamat_promosi'];
            }

            // if($row['type'] == 'SO' && $row['ot_flag'] == 0) {
            //     continue;
            // }

            if ($row['freight_type'] == 'ambil') {
                continue;
            }

            $hasil[] = array(
                'type' => $row['type'], 
                'code' => $row['code'],
                'ledgername' => $ledgerName, 
                'tanggal' => $row['tanggal'],
                'sm_no' => $row['type'] == 'SM' ? $row['no'] : '',
                'so_no' => $row['so_no'], 
                'item_code' => $arrayItem[$so]['item_code'], 
                'alamat_toko' => $row['alamat_toko'], 
                'keterangan' => $row['keterangan'], 
                'jaminput' => $row['jaminput'], 
                'code_comp' => $row['code_comp'],
                'sales_code' => $row['sales_code'],
                'cabang' => $row['cabang'],
                'freight_type' => $row['freight_type'],
                'dept_code' => $row['dept_code'],
                'type_so' => 'cabang_int',
                'kp_office' => $row['kp_office'],
                'pr_stat' => $row['pr_stat'], 
                'delivery_time' => $row['delivery_time'],
                'tipe_muat' => $row['tipe_muat'], 
                'total_do' => $row['total_do'], 
                'alamat_promosi' => $row['alamat_promosi'], 
                'tipe' => $row['tipe'], 
                'idno1' => $row['idno1'], 
                'kp_code' => $row['kp_code'] 
            );
        }

    
        $newHasil = [];
        foreach ($hasil as $value) {

            $type  = $value['type'];
            $sm_no = $value['sm_no'];
            $so_no = $value['so_no'];

            if ($type == 'SM') {

                $qry = "SELECT DISTINCT 'SM'::text AS type,
                            a.cabang,
                            c.kp_code,
                            a.order_date AS tanggal,
                            to_char(a.insert_date, 'HH24:MI:SS'::text) AS jaminput,
                            a.no,
                            a.so_no,
                            a.ledger_code AS code,
                            a.item_code,
                            c.item_qty::double precision AS item_qty,
                            a.item_qty::double precision AS item_qty2,
                            upper(a.keterangan::text) AS keterangan,
                            upper(a.kategori::text) AS code_comp,
                            (a.item_qty * b.item_weight / 1000000::numeric)::double precision AS item_ton,
                            a.desa,
                            c.ref_no AS sales_form_no,
                            d.name,
                            d.address1 AS alamat_toko,
                            d.sales_code,
                            ''::text AS car_no,
                            ''::text AS wh_code,
                            ''::text AS dept_code,
                            a.ot_flag,
                            a.freight_type,
                            c.pr_stat,
                            c.delivery_time,
                            c.close_by,
                            c.close_date,
                            c.item_qty_ton,
                            b.item_weight,
                            b.item_name,
                            c.price_amt
                        FROM sales_memo a
                            LEFT JOIN t_dp_detail e ON upper(a.kategori::text) = e.code_comp AND a.no = e.no AND a.so_no = e.so_no
                            JOIN po_req c ON a.so_no = c.pr_no AND a.item_code::bpchar = c.item_code AND upper(a.kategori::text) = c.code_comp::text
                            JOIN t_card d ON a.ledger_code::bpchar = d.code AND upper(a.kategori::text) = d.code_comp::text
                            JOIN t_item b ON a.item_code::bpchar = b.item_code AND upper(a.kategori::text) = b.code_comp::text
                        WHERE a.so_no IS NOT NULL AND a.status = '1'::bpchar AND e.id IS NULL AND date(c.pr_date) >= date(now() - '1000 days'::interval) AND c.pr_stat = 'A'::bpchar AND btrim(c.pending_stat::text) <> 'F'::text AND a.item_qty <= c.item_qty2 AND c.item_qty2 <> 0::numeric AND a.do_no IS NULL
                        AND a.no = '$sm_no'";
            } else {

                $qry = "SELECT DISTINCT 'SO'::text AS type,
                            a.spk_no AS cabang,
                            a.kp_code,
                            a.pr_date::date AS tanggal,
                            to_char(a.entry_date, 'HH24:MI:SS'::text) AS jaminput,
                            a.pr_no AS no,
                            a.pr_no AS so_no,
                            a.vend_code AS code,
                            a.item_code,
                            a.item_qty::double precision AS item_qty,
                            a.item_qty2::double precision AS item_qty2,
                            (upper(a.pr_desc::text) || ' '::text) || to_char(a.delivery_time, 'YYYY-MM-DD'::text) AS keterangan,
                            a.code_comp,
                            (a.item_qty2 * b.item_weight / 1000000::numeric)::double precision AS item_ton,
                            c.desa,
                            c.no AS sales_form_no,
                            d.name,
                            d.address1 AS alamat_toko,
                            d.sales_code,
                            ''::text AS car_no,
                            ''::text AS wh_code,
                            a.dept_code,
                            c.ot_flag,
                            lower(a.car_no::text) AS freight_type,
                            a.pr_stat,
                            a.delivery_time,
                            a.close_by,
                            a.close_date,
                            a.item_qty_ton,
                            b.item_weight,
                            b.item_name,
                            a.price_amt
                            FROM po_req a
                                LEFT JOIN t_dp_detail e ON a.code_comp::text = e.code_comp AND a.pr_no = e.no AND a.item_code = e.item_code::bpchar
                                JOIN t_card d ON a.vend_code = d.code AND a.code_comp = d.code_comp
                                JOIN t_item b ON a.item_code = b.item_code AND a.code_comp = b.code_comp
                                LEFT JOIN sales_form c ON a.ref_no = c.no AND a.code_comp::text = upper(c.kategori::text) AND a.spk_no::bpchar = c.cabang
                            WHERE (e.id IS NULL OR e.status = '1'::bpchar) AND date(a.pr_date) >= date(now() - '120 days'::interval) AND a.pr_stat = 'A'::bpchar AND btrim(a.pending_stat::text) <> 'F'::text AND a.pr_type = 'S'::bpchar AND a.item_qty2 <> 0::numeric
                            AND a.pr_no = '$so_no'";

            }

            // $qry = "SELECT a.pr_no, TRIM(a.item_code) as item_code, a.item_qty::integer, a.item_qty2::integer, b.item_weight, a.price_amt 
            //         FROM po_req a 
            //         -- LEFT JOIN t_dp_detail e ON a.code_comp::text = e.code_comp AND a.pr_no = e.no AND a.item_code = e.item_code::bpchar
            //         JOIN t_card d ON a.vend_code = d.code AND a.code_comp = d.code_comp
            //         JOIN t_item b ON a.item_code = b.item_code AND a.code_comp = b.code_comp
            //         WHERE date(a.pr_date) >= date(now() - '120 days'::interval) 
            //         AND a.pr_stat = 'A' 
            //         AND btrim(a.pending_stat::text) <> 'F'
            //         AND a.pr_type = 'S'::bpchar AND a.item_qty2 <> 0::numeric 
            //         -- AND (e.id IS NULL OR e.status = '1'::bpchar) 
            //         AND a.pr_no = '$so_no'";
            
            $rq = $dbgl->query($qry)->fetchAll(PDO::FETCH_ASSOC);

            $newHasil[] = array(
                'type' => $value['type'], 
                'code' => $value['code'],
                'ledgername' => $value['ledgername'], 
                'tanggal' => $value['tanggal'],
                'sm_no' => $value['sm_no'],
                'so_no' => $value['so_no'], 
                'item_code' => $value['item_code'], 
                'alamat_toko' => $value['alamat_toko'], 
                'keterangan' => $value['keterangan'], 
                'jaminput' => $value['jaminput'], 
                'code_comp' => $value['code_comp'],
                'sales_code' => $value['sales_code'],
                'cabang' => $value['cabang'],
                'freight_type' => $value['freight_type'],
                'dept_code' => $value['dept_code'],
                'type_so' => 'cabang_int',
                'kp_office' => $value['kp_office'],
                'pr_stat' => $value['pr_stat'], 
                'delivery_time' => $value['delivery_time'],
                'tipe_muat' => $value['tipe_muat'],
                'total_do' => $value['total_do'],
                'alamat_promosi' => $value['alamat_promosi'],
                'term' => $value['term'],
                'tipe' => $value['tipe'], 
                'idno1' => $value['idno1'], 
                'kp_code' => $value['kp_code'], 
                'detail' => $rq,
            );
            $rq = [];
        }

        foreach($newHasil as $val) {
            $tonase = getTonase($val['detail']);

            $result[] = array(
                "type" => $val['type'],
                "code" => $val['code'],
                "ledgername" => $val['ledgername'],
                "tanggal" => $val['tanggal'],
                "sm_no" => $val['sm_no'],
                "so_no" => $val['so_no'],
                "alamat_toko" => $val['alamat_toko'],
                "keterangan" => $val['keterangan'],
                "jaminput" => $val['jaminput'],
                "code_comp" => $val['code_comp'],
                "sales_code" => $val['sales_code'],
                "cabang" => $val['cabang'],
                "freight_type" => $val['freight_type'],
                "dept_code" => $val['dept_code'],
                "type_so" => $val['type_so'],
                "kp_office" => $val['kp_office'],
                "pr_stat" => $val['pr_stat'],
                "delivery_time" => $val['delivery_time'],
                "tipe_muat" => $val['tipe_muat'],
                "ton" => $tonase,
                "item_code" => $val['item_code'],
                "total_do" => $val['total_do'],
                "alamat_promosi" => $val['alamat_promosi'],
                "term" => $val['term'],
                "tipe" => $val['tipe'],
                "detail" => $val['detail'], 
                "idno1" => $val['idno1'], 
                "kp_code" => $val['kp_code'] 
            );
            $tonase = [];
        }

        return $result;
    }

    function getDataSOKirimNew2($dbgl,$data)
    {   
        $q = "SELECT a.code_comp, a.type, a.cabang, 
        a.tanggal, a.jaminput, a.code, 
        a.name, a.no, a.so_no, a.item_code, 
        a.item_qty, a.item_qty2, a.keterangan, 
        a.item_ton, a.desa, a.alamat_toko, 
        a.car_no, a.wh_code, d.address1 as alamat_promosi , 0 AS total_do, 
        a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type, a.sales_code,
        (SELECT wh_office FROM in_warehouse WHERE wh_code = a.kp_code) AS kp_office,
        CONCAT(d.name,'-',a.name) AS ledgername, a.pr_stat, to_char(a.delivery_time, 'YYYY-MM-DD ') AS delivery_time,
        CASE
            WHEN a.item_qty2 < a.item_qty THEN 'Outstanding'
            ELSE 'Langsung'
        END AS tipe_muat
        FROM v_displan a 
        LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp 
        WHERE a.cabang IN (".$data->where.")
        AND a.freight_type = 'kirim'";

        $res = $dbgl->query($q)->fetchAll(PDO::FETCH_ASSOC);

        $arrayItem = [];
        foreach ($res as $key => $row) {

            $so = $row['so_no'].$row['no'].$row['code_comp'];
            // if ($row['type'] == 'SM') {
            //     $so = $row['no'];
            // }
            if (isset($arrayItem[$so])) {
                $arrayItem[$so]['item_code'] .= '['.$row['item_code'].' - '.$row['item_qty2'].']';
                $arrayItem[$so]['total_ton'] .= $row['item_ton'].',';
                unset($res[$key]);
            } else {
                $arrayItem[$so] = [
                    'item_code' => '['.$row['item_code'].' - '.$row['item_qty2'].']',
                    'total_ton' => $row['item_ton'].',',
                ];
            }
        }

        foreach ($res as $row) {
            $so = $row['so_no'].$row['no'].$row['code_comp'];
            // if ($row['type'] == 'SM') {
            //     $so = $row['no'];
            // }
            $ledgerName = $row['name'];
            if ($row['dept_code'] <> NULL || $row['dept_code'] <> '') {
                $ledgerName = $row['dept_name'].' - '.$ledgerName;
                $row['alamat_toko'] = $row['alamat_promosi'];
            }

            if($row['type'] == 'SO' && $row['ot_flag'] == 1) {
                continue;
            }

            if ($row['freight_type'] == 'ambil') {
                continue;
            }

            $hasil[] = array(
                'type' => $row['type'], 
                'code' => $row['code'],
                'ledgername' => $ledgerName, 
                'tanggal' => $row['tanggal'],
                'sm_no' => $row['type'] == 'SM' ? $row['no'] : '',
                'so_no' => $row['so_no'], 
                'item_code' => $arrayItem[$so]['item_code'], 
                'alamat_toko' => $row['alamat_toko'], 
                'keterangan' => $row['keterangan'], 
                'jaminput' => $row['jaminput'], 
                'code_comp' => $row['code_comp'],
                'sales_code' => $row['sales_code'],
                'cabang' => $row['cabang'],
                'freight_type' => $row['freight_type'],
                'dept_code' => $row['dept_code'],
                'type_so' => 'cabang_int',
                'kp_office' => $row['kp_office'],
                'pr_stat' => $row['pr_stat'], 
                'delivery_time' => $row['delivery_time'],
                'tipe_muat' => $row['tipe_muat'],
                // 'total_ton' => $arrayItem[$so]['total_ton']
            );
        }
        // echo count($hasil);

    
        $newHasil = [];
        foreach ($hasil as $value) {
            # code...
            $so_no = $value['so_no'];
            $qry = "SELECT a.pr_no, TRIM(a.item_code) as item_code, a.item_qty::integer, a.item_qty2::integer, b.item_weight, a.price_amt 
                    FROM po_req a 
                    LEFT JOIN t_dp_detail e ON a.code_comp::text = e.code_comp AND a.pr_no = e.no AND a.item_code = e.item_code::bpchar
                    JOIN t_card d ON a.vend_code = d.code AND a.code_comp = d.code_comp
                    JOIN t_item b ON a.item_code = b.item_code AND a.code_comp = b.code_comp
                    WHERE date(a.pr_date) >= date(now() - '120 days'::interval) 
                    AND a.pr_stat = 'A' 
                    AND btrim(a.pending_stat::text) <> 'F'
                    AND a.pr_type = 'S'::bpchar AND a.item_qty2 <> 0::numeric 
                    AND (e.id IS NULL OR e.status = '1'::bpchar) 
                    AND a.pr_no = '$so_no'";
            
            $rq = $dbgl->query($qry)->fetchAll(PDO::FETCH_ASSOC);

            $newHasil[] = array(
                'type' => $value['type'], 
                'code' => $value['code'],
                'ledgername' => $value['ledgername'], 
                'tanggal' => $value['tanggal'],
                'sm_no' => $value['sm_no'],
                'so_no' => $value['so_no'], 
                'item_code' => $value['item_code'], 
                'alamat_toko' => $value['alamat_toko'], 
                'keterangan' => $value['keterangan'], 
                'jaminput' => $value['jaminput'], 
                'code_comp' => $value['code_comp'],
                'sales_code' => $value['sales_code'],
                'cabang' => $value['cabang'],
                'freight_type' => $value['freight_type'],
                'dept_code' => $value['dept_code'],
                'type_so' => 'cabang_int',
                'kp_office' => $value['kp_office'],
                'pr_stat' => $value['pr_stat'], 
                'delivery_time' => $value['delivery_time'],
                'tipe_muat' => $value['tipe_muat'],
                'detail' => $rq,
            );
            $rq = [];
        }


        $resmore = [];
        $resless = [];
        foreach($newHasil as $val) {
            // $tonase = getTonase($val['detail']);
            // $itemqty = getItemQty($val['detail']);

            if($val['code_comp'] == 'SAA') {
                $tonase = getTonase($val['detail']);
            } else {
                $tonase = 0;
            }

            if($tonase >= 10) {
                $resmore[] = array(
                    "type" => $val['type'],
                    "code" => $val['code'],
                    "ledgername" => $val['ledgername'],
                    "tanggal" => $val['tanggal'],
                    "sm_no" => $val['sm_no'],
                    "so_no" => $val['so_no'],
                    "alamat_toko" => $val['alamat_toko'],
                    "keterangan" => $val['keterangan'],
                    "jaminput" => $val['jaminput'],
                    "code_comp" => $val['code_comp'],
                    "sales_code" => $val['sales_code'],
                    "cabang" => $val['cabang'],
                    "freight_type" => $val['freight_type'],
                    "dept_code" => $val['dept_code'],
                    "type_so" => $val['type_so'],
                    "kp_office" => $val['kp_office'],
                    "pr_stat" => $val['pr_stat'],
                    "delivery" => $val['delivery_time'],
                    "tipe_muat" => $val['tipe_muat'],
                    "ton" => $tonase,
                    "item_code" => $val['item_code'],
                    "detail" => $val['detail']
                );
            } else {
                $resless[] = array(
                    "type" => $val['type'],
                    "code" => $val['code'],
                    "ledgername" => $val['ledgername'],
                    "tanggal" => $val['tanggal'],
                    "sm_no" => $val['sm_no'],
                    "so_no" => $val['so_no'],
                    "alamat_toko" => $val['alamat_toko'],
                    "keterangan" => $val['keterangan'],
                    "jaminput" => $val['jaminput'],
                    "code_comp" => $val['code_comp'],
                    "sales_code" => $val['sales_code'],
                    "cabang" => $val['cabang'],
                    "freight_type" => $val['freight_type'],
                    "dept_code" => $val['dept_code'],
                    "type_so" => $val['type_so'],
                    "kp_office" => $val['kp_office'],
                    "pr_stat" => $val['pr_stat'],
                    "delivery" => $val['delivery_time'],
                    "tipe_muat" => $val['tipe_muat'],
                    "ton" => $tonase,
                    "item_code" => $val['item_code'],
                    "detail" => $new['detail']
                );
            }
            $tonase = [];
            $itemqty = [];
        }

        $result = array(
            "more" => $resmore,
            "less" => $resless,
        );

        // print_r($result);

        return $result;
    }

    function getDataSOKirimNew($dbgl,$data)
    {   
        $q = "SELECT a.code_comp, a.type, a.cabang, 
        a.tanggal, a.jaminput, a.code, 
        a.name, a.no, a.so_no, a.item_code, 
        a.item_qty, a.item_qty2, a.keterangan, 
        a.item_ton, a.desa, a.alamat_toko, 
        a.car_no, a.wh_code, d.address1 as alamat_promosi , 0 AS total_do, 
        a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type, a.sales_code,
        (SELECT wh_office FROM in_warehouse WHERE wh_code = a.kp_code) AS kp_office,
        CONCAT(d.name,'-',a.name) AS ledgername, a.pr_stat, to_char(a.delivery_time, 'YYYY-MM-DD ') AS delivery_time,
        CASE
            WHEN a.item_qty2 < a.item_qty THEN 'Outstanding'
            ELSE 'Langsung'
        END AS tipe_muat
        FROM v_displan a 
        LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp 
        WHERE a.cabang IN (".$data->where.")
        AND a.freight_type = 'kirim'
        AND a.code_comp = upper('saa')";

        $res = $dbgl->query($q)->fetchAll(PDO::FETCH_ASSOC);

        $arrayItem = [];
        foreach ($res as $key => $row) {

            $so = $row['so_no'].$row['no'].$row['code_comp'];
            // if ($row['type'] == 'SM') {
            //     $so = $row['no'];
            // }
            if (isset($arrayItem[$so])) {
                $arrayItem[$so]['item_code'] .= '['.$row['item_code'].' - '.$row['item_qty2'].']';
                $arrayItem[$so]['total_ton'] .= $row['item_ton'].',';
                unset($res[$key]);
            } else {
                $arrayItem[$so] = [
                    'item_code' => '['.$row['item_code'].' - '.$row['item_qty2'].']',
                    'total_ton' => $row['item_ton'].',',
                ];
            }
        }

        foreach ($res as $row) {
            $so = $row['so_no'].$row['no'].$row['code_comp'];
            // if ($row['type'] == 'SM') {
            //     $so = $row['no'];
            // }
            $ledgerName = $row['name'];
            if ($row['dept_code'] <> NULL || $row['dept_code'] <> '') {
                $ledgerName = $row['dept_name'].' - '.$ledgerName;
                $row['alamat_toko'] = $row['alamat_promosi'];
            }

            if($row['type'] == 'SO' && $row['ot_flag'] == 1) {
                continue;
            }

            if ($row['freight_type'] == 'ambil') {
                continue;
            }

            $hasil[] = array(
                'type' => $row['type'], 
                'code' => $row['code'],
                'ledgername' => $ledgerName, 
                'tanggal' => $row['tanggal'],
                'sm_no' => $row['type'] == 'SM' ? $row['no'] : '',
                'so_no' => $row['so_no'], 
                'item_code' => $arrayItem[$so]['item_code'], 
                'alamat_toko' => $row['alamat_toko'], 
                'keterangan' => $row['keterangan'], 
                'jaminput' => $row['jaminput'], 
                'code_comp' => $row['code_comp'],
                'sales_code' => $row['sales_code'],
                'cabang' => $row['cabang'],
                'freight_type' => $row['freight_type'],
                'dept_code' => $row['dept_code'],
                'type_so' => 'cabang_int',
                'kp_office' => $row['kp_office'],
                'pr_stat' => $row['pr_stat'], 
                'delivery_time' => $row['delivery_time'],
                'tipe_muat' => $row['tipe_muat'],
                // 'total_ton' => $arrayItem[$so]['total_ton']
            );
        }
        // echo count($hasil);

    
        $newHasil = [];
        foreach ($hasil as $value) {
            # code...
            $so_no = $value['so_no'];
            $qry = "SELECT a.pr_no, TRIM(a.item_code) as item_code, a.item_qty::integer, a.item_qty2::integer, b.item_weight, a.price_amt 
                    FROM po_req a 
                    -- LEFT JOIN t_dp_detail e ON a.code_comp::text = e.code_comp AND a.pr_no = e.no AND a.item_code = e.item_code::bpchar
                    JOIN t_card d ON a.vend_code = d.code AND a.code_comp = d.code_comp
                    JOIN t_item b ON a.item_code = b.item_code AND a.code_comp = b.code_comp
                    WHERE date(a.pr_date) >= date(now() - '120 days'::interval) 
                    AND a.pr_stat = 'A' 
                    AND btrim(a.pending_stat::text) <> 'F'
                    AND a.pr_type = 'S'::bpchar AND a.item_qty2 <> 0::numeric 
                    -- AND (e.id IS NULL OR e.status = '1'::bpchar) 
                    AND a.pr_no = '$so_no'";
            
            $rq = $dbgl->query($qry)->fetchAll(PDO::FETCH_ASSOC);

            $newHasil[] = array(
                'type' => $value['type'], 
                'code' => $value['code'],
                'ledgername' => $value['ledgername'], 
                'tanggal' => $value['tanggal'],
                'sm_no' => $value['sm_no'],
                'so_no' => $value['so_no'], 
                'item_code' => $value['item_code'], 
                'alamat_toko' => $value['alamat_toko'], 
                'keterangan' => $value['keterangan'], 
                'jaminput' => $value['jaminput'], 
                'code_comp' => $value['code_comp'],
                'sales_code' => $value['sales_code'],
                'cabang' => $value['cabang'],
                'freight_type' => $value['freight_type'],
                'dept_code' => $value['dept_code'],
                'type_so' => 'cabang_int',
                'kp_office' => $value['kp_office'],
                'pr_stat' => $value['pr_stat'], 
                'delivery_time' => $value['delivery_time'],
                'tipe_muat' => $value['tipe_muat'],
                'detail' => $rq,
            );
            $rq = [];
        }


        $resmore = [];
        $resless = [];
        foreach($newHasil as $val) {
            if($val['code_comp'] == 'SAA') {
                $tonase = getTonase($val['detail']);
            } else {
                $tonase = 0;
            }
            // $itemqty = getItemQty($val['detail']);

            if($tonase >= 10) {
                $resmore[] = array(
                    "type" => $val['type'],
                    "code" => $val['code'],
                    "ledgername" => $val['ledgername'],
                    "tanggal" => $val['tanggal'],
                    "sm_no" => $val['sm_no'],
                    "so_no" => $val['so_no'],
                    "alamat_toko" => $val['alamat_toko'],
                    "keterangan" => $val['keterangan'],
                    "jaminput" => $val['jaminput'],
                    "code_comp" => $val['code_comp'],
                    "sales_code" => $val['sales_code'],
                    "cabang" => $val['cabang'],
                    "freight_type" => $val['freight_type'],
                    "dept_code" => $val['dept_code'],
                    "type_so" => $val['type_so'],
                    "kp_office" => $val['kp_office'],
                    "pr_stat" => $val['pr_stat'],
                    "delivery_time" => $val['delivery_time'],
                    "tipe_muat" => $val['tipe_muat'],
                    "ton" => $tonase,
                    // "item_code" => $itemqty,
                    "item_code" => $val['item_code'],
                    "detail" => $val['detail']
                );
                $tonase = [];
            } else {
                $resless[] = array(
                    "type" => $val['type'],
                    "code" => $val['code'],
                    "ledgername" => $val['ledgername'],
                    "tanggal" => $val['tanggal'],
                    "sm_no" => $val['sm_no'],
                    "so_no" => $val['so_no'],
                    "alamat_toko" => $val['alamat_toko'],
                    "keterangan" => $val['keterangan'],
                    "jaminput" => $val['jaminput'],
                    "code_comp" => $val['code_comp'],
                    "sales_code" => $val['sales_code'],
                    "cabang" => $val['cabang'],
                    "freight_type" => $val['freight_type'],
                    "dept_code" => $val['dept_code'],
                    "type_so" => $val['type_so'],
                    "kp_office" => $val['kp_office'],
                    "pr_stat" => $val['pr_stat'],
                    "delivery_time" => $val['delivery_time'],
                    "tipe_muat" => $val['tipe_muat'],
                    "ton" => $tonase,
                    // "item_code" => $itemqty,
                    "item_code" => $val['item_code'],
                    "detail" => $new['detail']
                );
                $tonase = [];
            }
            // $itemqty = [];
        }

        $result = array(
            "more" => $resmore,
            "less" => $resless,
        );

        // print_r($result);

        return $result;
    }

    function getDataSOKirimSna($dbgl,$data)
    {   
        $q = "SELECT a.code_comp, a.type, a.cabang, 
        a.tanggal, a.jaminput, a.code, 
        a.name, a.no, a.so_no, a.item_code, 
        a.item_qty, a.item_qty2, a.keterangan, 
        a.item_ton, a.desa, a.alamat_toko, 
        a.car_no, a.wh_code, d.address1 as alamat_promosi , 0 AS total_do, 
        a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type, a.sales_code,
        (SELECT wh_office FROM in_warehouse WHERE wh_code = a.kp_code) AS kp_office,
        CONCAT(d.name,'-',a.name) AS ledgername, a.pr_stat, to_char(a.delivery_time, 'YYYY-MM-DD ') AS delivery_time,
        CASE
            WHEN a.item_qty2 < a.item_qty THEN 'Outstanding'
            ELSE 'Langsung'
        END AS tipe_muat
        FROM v_displan a 
        LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp 
        WHERE a.cabang IN (".$data->where.")
        AND a.freight_type = 'kirim'
        AND a.code_comp = upper('sna')";

        $res = $dbgl->query($q)->fetchAll(PDO::FETCH_ASSOC);

        $arrayItem = [];
        foreach ($res as $key => $row) {

            $so = $row['so_no'].$row['no'].$row['code_comp'];
            // if ($row['type'] == 'SM') {
            //     $so = $row['no'];
            // }
            if (isset($arrayItem[$so])) {
                $arrayItem[$so]['item_code'] .= '['.$row['item_code'].' - '.$row['item_qty2'].']';
                $arrayItem[$so]['total_ton'] .= $row['item_ton'].',';
                unset($res[$key]);
            } else {
                $arrayItem[$so] = [
                    'item_code' => '['.$row['item_code'].' - '.$row['item_qty2'].']',
                    'total_ton' => $row['item_ton'].',',
                ];
            }
        }

        foreach ($res as $row) {
            $so = $row['so_no'].$row['no'].$row['code_comp'];
            // if ($row['type'] == 'SM') {
            //     $so = $row['no'];
            // }
            $ledgerName = $row['name'];
            if ($row['dept_code'] <> NULL || $row['dept_code'] <> '') {
                $ledgerName = $row['dept_name'].' - '.$ledgerName;
                $row['alamat_toko'] = $row['alamat_promosi'];
            }

            if($row['type'] == 'SO' && $row['ot_flag'] == 1) {
                continue;
            }

            if ($row['freight_type'] == 'ambil') {
                continue;
            }

            $hasil[] = array(
                'type' => $row['type'], 
                'code' => $row['code'],
                'ledgername' => $ledgerName, 
                'tanggal' => $row['tanggal'],
                'sm_no' => $row['type'] == 'SM' ? $row['no'] : '',
                'so_no' => $row['so_no'], 
                'item_code' => $arrayItem[$so]['item_code'], 
                'alamat_toko' => $row['alamat_toko'], 
                'keterangan' => $row['keterangan'], 
                'jaminput' => $row['jaminput'], 
                'code_comp' => $row['code_comp'],
                'sales_code' => $row['sales_code'],
                'cabang' => $row['cabang'],
                'freight_type' => $row['freight_type'],
                'dept_code' => $row['dept_code'],
                'type_so' => 'cabang_int',
                'kp_office' => $row['kp_office'],
                'pr_stat' => $row['pr_stat'], 
                'delivery_time' => $row['delivery_time'],
                'tipe_muat' => $row['tipe_muat'],
                // 'total_ton' => $arrayItem[$so]['total_ton']
            );
        }
        // echo count($hasil);

    
        $newHasil = [];
        foreach ($hasil as $value) {
            # code...
            $so_no = $value['so_no'];
            $qry = "SELECT a.pr_no, TRIM(a.item_code) as item_code, a.item_qty::integer, a.item_qty2::integer, b.item_weight, a.price_amt 
                    FROM po_req a 
                    -- LEFT JOIN t_dp_detail e ON a.code_comp::text = e.code_comp AND a.pr_no = e.no AND a.item_code = e.item_code::bpchar
                    JOIN t_card d ON a.vend_code = d.code AND a.code_comp = d.code_comp
                    JOIN t_item b ON a.item_code = b.item_code AND a.code_comp = b.code_comp
                    WHERE date(a.pr_date) >= date(now() - '120 days'::interval) 
                    AND a.pr_stat = 'A' 
                    AND btrim(a.pending_stat::text) <> 'F'
                    AND a.pr_type = 'S'::bpchar AND a.item_qty2 <> 0::numeric 
                    -- AND (e.id IS NULL OR e.status = '1'::bpchar) 
                    AND a.pr_no = '$so_no'";
            
            $rq = $dbgl->query($qry)->fetchAll(PDO::FETCH_ASSOC);

            $newHasil[] = array(
                'type' => $value['type'], 
                'code' => $value['code'],
                'ledgername' => $value['ledgername'], 
                'tanggal' => $value['tanggal'],
                'sm_no' => $value['sm_no'],
                'so_no' => $value['so_no'], 
                'item_code' => $value['item_code'], 
                'alamat_toko' => $value['alamat_toko'], 
                'keterangan' => $value['keterangan'], 
                'jaminput' => $value['jaminput'], 
                'code_comp' => $value['code_comp'],
                'sales_code' => $value['sales_code'],
                'cabang' => $value['cabang'],
                'freight_type' => $value['freight_type'],
                'dept_code' => $value['dept_code'],
                'type_so' => 'cabang_int',
                'kp_office' => $value['kp_office'],
                'pr_stat' => $value['pr_stat'], 
                'delivery_time' => $value['delivery_time'],
                'tipe_muat' => $value['tipe_muat'],
                'detail' => $rq,
            );
            $rq = [];
        }


        $resmore = [];
        $resless = [];
        foreach($newHasil as $val) {
            $tonase = getTonase($val['detail']);

            $resless[] = array(
                "type" => $val['type'],
                "code" => $val['code'],
                "ledgername" => $val['ledgername'],
                "tanggal" => $val['tanggal'],
                "sm_no" => $val['sm_no'],
                "so_no" => $val['so_no'],
                "alamat_toko" => $val['alamat_toko'],
                "keterangan" => $val['keterangan'],
                "jaminput" => $val['jaminput'],
                "code_comp" => $val['code_comp'],
                "sales_code" => $val['sales_code'],
                "cabang" => $val['cabang'],
                "freight_type" => $val['freight_type'],
                "dept_code" => $val['dept_code'],
                "type_so" => $val['type_so'],
                "kp_office" => $val['kp_office'],
                "pr_stat" => $val['pr_stat'],
                "delivery_time" => $val['delivery_time'],
                "tipe_muat" => $val['tipe_muat'],
                "ton" => $tonase,
                // "item_code" => $itemqty,
                "item_code" => $val['item_code'],
                "detail" => $new['detail']
            );
            $tonase = [];
        }

        // $result = array(
        //     "more" => $resmore,
        //     "less" => $resless,
        // );

        // print_r($result);

        return $resless;
    }

    function getTonase($new) {
        $jumlah_ton = 0;
        $jml_qty = 0;
        foreach($new as $dt) {
            $mystring = $dt['item_code'];
            $findme   = 'B';
            $pos = strpos($mystring, $findme);
            if($pos == false) {
                $itemweight = $dt['item_weight'];
                if($dt['item_qty'] == $dt['item_qty2']) {
                    $jml_qty += $dt['item_qty'];
                } else {
                    $jml_qty += $dt['item_qty2'];
                }
                $ton = ($jml_qty * $itemweight) / 1000000; 
                $jumlah_ton += $ton;
            }
        }
        return $jumlah_ton;
    }

    function getItemQty($new) {
        $string = "";
        foreach($new as $dt) {
            if($dt['item_qty'] == $dt['item_qty2']) {
                $string .= "[".$dt['item_code']."-".$dt['item_qty']."]";
            } else {
                $string .= "[".$dt['item_code']."-".$dt['item_qty2']."]"; 
            }
        }
        return $string;

    }

    function getDataSOKirimPengalihan($dbgl,$data)
    {
        $responseData = [];

        try {

            $query = "SELECT a.type, a.cabang, 
                             a.tanggal, a.jaminput, a.code, a.so_no, a.item_code, a.keterangan, 
                             a.alamat_toko, a.code_comp,
                             a.dept_code, a.freight_type, a.sales_code,
                             a.ledgername, 'cabang_eks' AS type_so
                        FROM t_distribution_plan_pengalihan a
                        WHERE freight_type = 'kirim'
                        AND a.so_no NOT IN (SELECT so_no FROM t_distribution_plan_list)
                        AND a.cabang_pengalihan IN (".$data->where.")";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }
    
        return $responseData;
    }

    function getDataSOAmbil($dbgl,$data)
    {   

        $all_ambil = [];

        try {
            $query = "SELECT a.code_comp, a.type, a.cabang, 
                            cast(a.tanggal as varchar), a.jaminput, a.code, 
                            a.name, a.no, a.so_no, a.item_code, 
                            cast(a.item_qty as varchar), cast(a.item_qty2 as varchar), a.keterangan, 
                            cast(a.item_ton as varchar), a.desa, a.alamat_toko, 
                            a.car_no, a.wh_code, d.address1 as alamat_promosi , cast(0 as varchar) AS total_do, 
                            a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type, a.sales_code,
                            (SELECT wh_office FROM in_warehouse WHERE wh_code = a.kp_code) AS kp_office,
                            CONCAT(d.name,'-',a.name) AS ledgername, a.pr_stat, to_char(a.delivery_time, 'YYYY-MM-DD ') AS delivery_time,
                            'Gandengan' AS tipe_muat, a.item_qty_ton
                        FROM v_displan a 
                        LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp 
                        WHERE a.cabang IN (".$data->where.")
                        AND a.freight_type = 'ambil'";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);

            $arrayItem = [];

            foreach ($rows as $key => $row) {
                $so = $row['so_no'].$row['no'];
                // if ($row['type'] == 'SM') {
                //     $so = $row['no'];
                // }
                if (isset($arrayItem[$so])) {
                    $arrayItem[$so]['item_code'] .= '['.$row['item_code'].' - '.$row['item_qty2'].']';
                    unset($rows[$key]);
                } else {
                    $arrayItem[$so] = [
                        'item_code' => '['.$row['item_code'].' - '.$row['item_qty2'].']',
                    ];
                }
            }

            foreach ($rows as $row) {

                $so = $row['so_no'].$row['no'];
                // if ($row['type'] == 'SM') {
                //     $so = $row['no'];
                // }
                $ledgerName = $row['name'];
                if ($row['dept_code'] <> NULL || $row['dept_code'] <> '') {
                    $ledgerName = '['.$row['dept_name'].']'.$ledgerName;
                }

                if($row['type'] == 'SO' && $row['ot_flag'] == 1) {
                    continue;
                }

                if ($row['freight_type'] == 'kirim') {
                    continue;
                }

                $arr_hasil[] = array(

                    'type' => $row['type'], 
                    'code' => $row['code'],
                    'ledgername' => $ledgerName, 
                    'tanggal' => $row['tanggal'],
                    'sm_no' => $row['type'] == 'SM' ? $row['no'] : '',
                    'so_no' => $row['so_no'], 
                    'item_code' => $arrayItem[$so]['item_code'], 
                    'alamat_toko' => $row['alamat_toko'], 
                    'keterangan' => $row['keterangan'], 
                    'jaminput' => $row['jaminput'], 
                    'code_comp' => $row['code_comp'],
                    'sales_code' => $row['sales_code'],
                    'alamat_toko' => $row['alamat_toko'],
                    'cabang' => $row['cabang'],
                    'freight_type' => $row['freight_type'],
                    'dept_code' => $row['dept_code'],
                    'type_so' => 'cabang_int',
                    'cabang' => $row['cabang'],
                    'kp_office' => $row['kp_office'], 
                    'delivery_time' => $row['delivery_time'],
                    'item_ton' => $row['item_ton']

                );

            }

        } catch (PDOException $e) {
            $e->getMessage();
            $arr_hasil = $e; 
        }

        $hasilAmbil2 = getDataSOAmbilPengalihan($dbgl,$data);

        $all_ambil = array_merge($hasilAmbil2,$arr_hasil);

        return $all_ambil;
    }

    function getDataSOAmbilPengalihan($dbgl,$data)
    {
        $responseData = [];

        try {

            $query = "SELECT a.type, a.cabang, 
                                a.tanggal, a.jaminput, a.code, a.so_no, a.item_code, a.keterangan, 
                                a.alamat_toko, a.code_comp,
                                a.dept_code, a.freight_type, a.sales_code,
                                a.ledgername, 'cabang_eks' AS type_so
                        FROM t_distribution_plan_pengalihan a
                        WHERE freight_type = 'ambil'
                        AND a.so_no NOT IN (SELECT so_no FROM t_distribution_plan_list)
                        AND a.cabang_pengalihan IN (".$data->where.")";
            
            // echo $query;

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }
    
        return $responseData;
    }

    function getOfficeCode($dbh,$data)
    {
        $responseData = [];

        try {

            $username = strtolower($data->username);
            $query = "SELECT kd_off FROM hrm_officeacc WHERE username = '{$username}' AND kd_off NOT IN ('KTW', 'HO', 'ALL', 'DPS')";

            $rows = $dbh->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData['dt_kd_offifce'][] = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }
    
        return $responseData;
    }

    function getPriceAmtBySO($dbgl,$data)
    {
        for($i=0; $i<count($data->dt_so);$i++) {

            if ($data->dt_so[$i]->sm_no != "") {
                $ket = "SM";
                $no  = $data->dt_so[$i]->sm_no;
                $kol = "AND a.no";
            } else {
                $ket = "SO";
                $no = $data->dt_so[$i]->so_no;
                $kol = "AND a.so_no";
            }

            // $so_no = $data->dt_so[$i]->so_no;

            try {
                $query = "SELECT a.code_comp, a.type, a.cabang,
                                    a.tanggal, a.jaminput, a.code, 
                                    a.name, a.no, a.so_no, a.item_code, 
                                    a.item_qty, a.item_qty2, a.keterangan, 
                                    a.item_ton, a.desa, a.alamat_toko, 
                                    a.car_no, a.wh_code, d.address1 as alamat_promosi , 0 AS total_do, 
                                    a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type, a.sales_code,
                                    CONCAT(d.name,'-',a.name) AS ledgername,
                                    (SELECT lob_code FROM t_card WHERE code = a.code AND code_comp = a.code_comp) AS lob_code,
                                    to_char(delivery_time, 'YYYY-MM-DD') AS delivery_time
                            FROM v_displan a 
                            LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp
                            WHERE a.type = '$ket'
                            $kol = '$no'
                            ORDER BY a.code, a.tanggal, a.no";

                $stmt = $dbgl->prepare($query);
                $stmt->execute();
            
                while($rows_so = $stmt->fetch(PDO::FETCH_ASSOC)) {
                  $arr_so[] = $rows_so;
                }

            } catch (PDOException $e) {
                $xarr_data = $e;
            }
        }

        for ($j=0; $j < count($arr_so); $j++) { 

            $item_code = $arr_so[$j]['item_code'];
            $so_no =  $arr_so[$j]['so_no'];

            $lob_code =  $arr_so[$j]['lob_code'];
            $code_comp =  $arr_so[$j]['code_comp'];

            try {
                $query = "SELECT 
                        a.vend_code, a.pr_no, a.item_code, 
                        a.spk_no, a.item_qty, a.price_amt,
                        a.car_no, a.so_code, '$lob_code' AS lob_code, 
                        '$code_comp' AS code_comp
                    FROM po_req a
                    WHERE pr_no = '$so_no'
                    AND item_code = '$item_code'";

                $stmt = $dbgl->prepare($query);
                $stmt->execute();

                while($rows_so = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $xarr_data[] = $rows_so;
                }

            } catch (PDOException $e) {
                $xarr_data = $e;
            }

            $arr_data['data_so'][] = array(
                'code_comp' => $arr_so[$j]['code_comp'],
                'type' => $arr_so[$j]['type'],   
                'cabang' => $arr_so[$j]['cabang'],   
                'tanggal' => $arr_so[$j]['tanggal'],   
                'jaminput' => $arr_so[$j]['jaminput'],   
                'code' => $arr_so[$j]['code'],   
                'name' => $arr_so[$j]['name'],   
                'no' => $arr_so[$j]['no'],   
                'so_no' => $arr_so[$j]['so_no'],   
                'item_code' => $arr_so[$j]['item_code'],   
                'item_qty' => $arr_so[$j]['item_qty'],   
                'item_qty2' => $arr_so[$j]['item_qty2'],   
                'keterangan' => $arr_so[$j]['keterangan'],   
                'item_ton' => $arr_so[$j]['item_ton'],   
                'desa' => $arr_so[$j]['desa'],   
                'alamat_toko' => $arr_so[$j]['alamat_toko'],   
                'car_no' => $arr_so[$j]['car_no'],   
                'wh_code' => $arr_so[$j]['wh_code'],   
                'alamat_promosi' => $arr_so[$j]['alamat_promosi'],   
                'total_do' => $arr_so[$j]['total_do'],   
                'dept_code' => $arr_so[$j]['dept_code'],   
                'dept_name' => $arr_so[$j]['dept_name'],   
                'ot_flag' => $arr_so[$j]['ot_flag'],   
                'freight_type' => $arr_so[$j]['freight_type'],   
                'sales_code' => $arr_so[$j]['sales_code'],
                'ledgername' => $arr_so[$j]['ledgername'],
                'status_so' => $arr_so[$j]['status_so'],
                'lob_code' => $arr_so[$j]['lob_code'],
                'dept_code' => $arr_so[$j]['dept_code'],
                'delivery_time' => $arr_so[$j]['delivery_time'],
                'item' => $xarr_data
            );
            $xarr_data = [];
        }

        return $arr_data;
    }

    function getPriceAmtBySales($dbgl,$data,$price_so)
    {
        $result = [];
        $cabang = $data->office;

        for($i=0; $i<count($price_so['data_so']); $i++) {

            $code_comp = $price_so['data_so'][$i]['code_comp'];
            $freight_type = $price_so['data_so'][$i]['freight_type'];

            $item_code = trim($price_so['data_so'][$i]['item'][0]['item_code']);
            $lob_code = trim($price_so['data_so'][$i]['item'][0]['lob_code']);

            try {
                $query = "SELECT a.*, b.inv_uom, b.item_group, upper(b.item_name) AS item_name
                            FROM t_item_price a INNER JOIN t_item b 
                            ON a.item_code = b.item_code AND a.code_comp = b.code_comp
                            WHERE lower(a.code_comp) = lower('$code_comp') 
                            AND a.delete_time IS NULL
                            AND a.coce_code = '$cabang'
                            AND a.item_code = '$item_code'
                            AND a.cash_flag = '1'
                            AND (a.lob_code = '$lob_code' OR a.lob_code = 'ALL')
                            ORDER BY price_date DESC LIMIT 1";

                $stmt = $dbgl->prepare($query);
                $stmt->execute();

                while($rows_pricelist = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $arr_pricelist[] = $rows_pricelist;
                }

            } catch (PDOException $e) {
                $arr_pricelist = $e;
            }

        }

        return $arr_pricelist;
    }

    function saveDistributionPlanKirim($dbgl,$data)
    {
        $now = date('Y-m-d');

        $xprice_so = getPriceAmtBySO($dbgl,$data);
        $price_so = $xprice_so['data_so'];

        $price_sales = getPriceAmtBySales($dbgl,$data,$xprice_so);

        // $arr_all[] = array (
        //     'price_so' => $xprice_so,
        //     'price_sales' => $price_sales
        // );

        // return $xprice_so;

        for ($i=0; $i < count($price_so) ; $i++) {
            
            if (trim($price_so[$i]['item'][0]['item_code']) == $price_sales[$i]['item_code']) {
            
                if ($price_so[$i]['item'][0]['price_amt'] == $price_sales[$i]['kirim']) {
                    $arr_all[] = array(
                        'hasil' => 1,
                        'data_so' => $price_so[$i],
                        'price_so' => array(
                            'so_no' => $price_so[$i]['item'][0]['pr_no'],
                            'cabang' => $price_so[$i]['item'][0]['spk_no'],
                            'item_code' => trim($price_so[$i]['item'][0]['item_code']),
                            'price_amount' => $price_so[$i]['item'][0]['price_amt'],
                        ),
                        'price_sales' => array(
                            'cabang' => $price_sales[$i]['coce_code'],
                            'item_code' => $price_sales[$i]['item_code'],
                            'price_amount' => $price_sales[$i]['kirim']
                        )
                    );
                } else {
                    $arr_all[] = array(
                        'hasil' => 2,
                        'data_so' => $price_so[$i],
                        'price_so' => array(
                            'so_no' => $price_so[$i]['item'][0]['pr_no'],
                            'cabang' => $price_so[$i]['item'][0]['spk_no'],
                            'item_code' => trim($price_so[$i]['item'][0]['item_code']),
                            'price_amount' => $price_so[$i]['item'][0]['price_amt'],
                        ),
                        'price_sales' => array(
                            'cabang' => $price_sales[$i]['coce_code'],
                            'item_code' => $price_sales[$i]['item_code'],
                            'price_amount' => $price_sales[$i]['kirim']
                        )
                    );
                }
            } else {
                $arr_all[] = array(
                    'hasil' => 0,
                    'data_so' => $price_so[$i],
                    'price_so' => array(
                        'so_no' => $price_so[$i]['item'][0]['pr_no'],
                        'cabang' => $price_so[$i]['item'][0]['spk_no'],
                        'item_code' => trim($price_so[$i]['item'][0]['item_code']),
                        'price_amount' => $price_so[$i]['item'][0]['price_amt'],
                    ),
                    'price_sales' => array(
                        'cabang' => $price_sales[$i]['coce_code'],
                        'item_code' => $price_sales[$i]['item_code'],
                        'price_amount' => $price_sales[$i]['kirim']
                    )
                );
            }
        }

        return $arr_all;
    }

    function actionSaveDistributionPlanKirim2($dbgl,$data)
    {
        $noDp = generateNoDp($dbgl,$data);

        // return $data;

        $response = [];
        if($data->office == '' || $data->tgl_perintah == '' || $data->username == '' || count($data->dt_so) <= 0){
            $response = [
                'success' => 0,
                'response' => 'Validasi error, silahkan isi data yang diperlukan'
            ];
        } else {
            $id             = "";
            $office         = $data->office;
            $tgl_perintah   = $data->tgl_perintah;
            $username       = $data->username;
            $now            = date('Y-m-d H:i:s');

            $query = "INSERT INTO t_distribution_plan_item(tgl_perintah, no_dp, created_at, updated_at, insert_by, freight_type, asal_gudang, status_so) VALUES (:tgl_perintah, :no_dp, :created_at, :updated_at, :insert_by, :freight_type, :asal_gudang, :status_so) RETURNING id";
            $stmt = $dbgl->prepare($query);
            $dataheader = [
                ':tgl_perintah' => $tgl_perintah,
                ':no_dp' => $noDp,
                ':created_at' => $now,
                ':updated_at' => $now,
                ':insert_by' => $username,
                ':freight_type' => "kirim",
                ':asal_gudang' => $office,
                ':status_so' => '0'
            ];

            try {
                $stmt->execute($dataheader);
                $stmt->bindColumn('id', $id);
                $stmt->fetch();
                $response = [
                    'success' => 1,
                    'response' => 'sukses',
                ];
            } catch (PDOException $e) {
                $response = [
                    'success' => 0, 
                    'response' => 'silahkan coba kembali!',
                    'error' => $e
                ];
            }

            $sql = "INSERT INTO 
                        t_distribution_plan_list(type,cabang,
                        tanggal,
                        no,
                        so_no,
                        code,
                        item_code,
                        item_qty,
                        item_qty2,
                        keterangan,
                        code_comp,
                        item_ton,
                        desa,
                        sales_from_no,
                        name,
                        alamat_toko,
                        insert_time,
                        insert_by,
                        status,
                        dept_code,
                        dp_id,
                        price_amt) 
                    VALUES 
                        (:type,:cabang,
                        :tanggal,
                        :no,
                        :so_no,
                        :code,
                        :item_code,
                        :item_qty,
                        :item_qty2,
                        :keterangan,
                        :code_comp,
                        :item_ton,
                        :desa,
                        :sales_from_no,
                        :name,
                        :alamat_toko,
                        :insert_time,
                        :insert_by,
                        :status,
                        :dept_code,
                        :dp_id,
                        :price_amt)";

            $stmt = $dbgl->prepare($sql);

            try {

                for($i=0; $i < count($data->dt_price[0]->data_so); $i++) {
                    $stmt->bindValue(':type', $data->dt_price[0]->data_so[$i]->type);
                    $stmt->bindValue(':cabang', $data->dt_price[0]->data_so[$i]->cabang);
                    $stmt->bindValue(':tanggal', $data->dt_price[0]->data_so[$i]->tanggal);
                    $stmt->bindValue(':no', $data->dt_price[0]->data_so[$i]->no);
                    $stmt->bindValue(':so_no', $data->dt_price[0]->data_so[$i]->so_no);
                    $stmt->bindValue(':code', $data->dt_price[0]->data_so[$i]->code);
                    $stmt->bindValue(':item_code', $data->dt_price[0]->data_so[$i]->item_code);
                    $stmt->bindValue(':item_qty', $data->dt_price[0]->data_so[$i]->item_qty);
                    $stmt->bindValue(':item_qty2', $data->dt_price[0]->data_so[$i]->item_qty2);
                    $stmt->bindValue(':keterangan', $data->dt_price[0]->data_so[$i]->keterangan);
                    $stmt->bindValue(':code_comp', $data->dt_price[0]->data_so[$i]->code_comp);
                    $stmt->bindValue(':item_ton', $data->dt_price[0]->data_so[$i]->item_ton);
                    $stmt->bindValue(':desa', $data->dt_price[0]->data_so[$i]->desa);
                    $stmt->bindValue(':sales_from_no', $data->dt_price[0]->data_so[$i]->sales_code);
                    $stmt->bindValue(':name', $data->dt_price[0]->data_so[$i]->name);
                    $stmt->bindValue(':alamat_toko', $data->dt_price[0]->data_so[$i]->alamat_toko);
                    $stmt->bindValue(':insert_time', $now);
                    $stmt->bindValue(':insert_by', $username);
                    $stmt->bindValue(':status', '0');
                    $stmt->bindValue(':dept_code', $data->dt_price[0]->data_so[$i]->dept_code);
                    $stmt->bindValue(':dp_id', $id);
                    $stmt->bindValue(':price_amt', $data->dt_price[0]->price[$i]->price_amount);
                    $stmt->execute();
                }

                $response = [
                    "success" => 1,
                    "message" => "Data DP Berhasil Disimpan"
                ];

            } catch (PDOException $err) {
                $response = [
                    "success" => 0,
                    "id" => $id,
                    "message" => $err
                ];
            }
            
            return $response; 
        }
    }

    function actionSaveDistributionPlanKirim($dbgl,$data)
    {
        $noDp = generateNoDp($dbgl,$data);

        // return $data;

        $response = [];
        if($data->office == '' || $data->tgl_perintah == '' || $data->username == '' || count($data->dt_so) <= 0){
            $response = [
                'success' => 0,
                'response' => 'Validasi error, silahkan isi data yang diperlukan'
            ];
        } else {
            $id             = "";
            $office         = $data->office;
            $tgl_perintah   = $data->tgl_perintah;
            $username       = $data->username;
            $now            = date('Y-m-d H:i:s');
            $loc            = $data->loc;
            $totalMuatan            = $data->totalMuatan;

            $query = "INSERT INTO t_distribution_plan(no_dp, created_at, insert_by, freight_type, tgl_kirim, sc_status, total_muatan) 
            VALUES (:no_dp, :created_at, :insert_by, :freight_type, :tgl_kirim, :sc_status, :total_muatan) RETURNING id";
            $stmt = $dbgl->prepare($query);
            $dataheader = [
                ':no_dp' => $noDp,
                ':created_at' => $now,
                ':insert_by' => $username,
                ':freight_type' => "kirim",
                ':tgl_kirim' => $data->tgl_perintah,
                ':sc_status' => 1,
                ':total_muatan' =>$totalMuatan,
            ];

            try {
                $stmt->execute($dataheader);
                $stmt->bindColumn('id', $id);
                $stmt->fetch();
                $response = [
                    'success' => 1,
                    'response' => 'sukses',
                ];
            } catch (PDOException $e) {
                $response = [
                    'success' => 0, 
                    'response' => 'silahkan coba kembali!',
                    'error' => $e
                ];
            }

            $sql = "INSERT INTO 
                        t_dp_detail(type,cabang,
                        tanggal,
                        no,
                        so_no,
                        code,
                        item_code,
                        item_qty,
                        item_qty2,
                        keterangan,
                        code_comp,
                        item_ton,
                        desa,
                        sales_form_no,
                        name,
                        alamat_toko,
                        insert_time,
                        insert_by,
                        status,
                        delivery_date,
                        dept_code,
                        dp_id,
                        pengalihan) 
                    VALUES 
                        (:type,:cabang,
                        :tanggal,
                        :no,
                        :so_no,
                        :code,
                        :item_code,
                        :item_qty,
                        :item_qty2,
                        :keterangan,
                        :code_comp,
                        :item_ton,
                        :desa,
                        :sales_form_no,
                        :name,
                        :alamat_toko,
                        :insert_time,
                        :insert_by,
                        :status,
                        :delivery_date,
                        :dept_code,
                        :dp_id,
                        :pengalihan)";

            $stmt = $dbgl->prepare($sql);

            try {

                for($i=0; $i < count($data->dt_price[0]->data_so); $i++) {

                    $lokasi_berangkat = "";
                    $pengalihan = null;

                    if ($data->office == $data->dt_price[0]->data_so[$i]->cabang) {
                        $lokasi_berangkat = $data->office;
                        $pengalihan = null;
                    } else {
                        $lokasi_berangkat = $data->office;
                        $pengalihan = $data->dt_price[0]->data_so[$i]->cabang;
                    }

                    $xtanggal = $data->tgl_perintah;
                    $timestamp = date('Y-m-d H:i:s', strtotime($xtanggal)); 

                    $stmt->bindValue(':type', $data->dt_price[0]->data_so[$i]->type);
                    $stmt->bindValue(':cabang', $lokasi_berangkat);
                    $stmt->bindValue(':tanggal', $data->dt_price[0]->data_so[$i]->tanggal);
                    $stmt->bindValue(':no', $data->dt_price[0]->data_so[$i]->no);
                    $stmt->bindValue(':so_no', $data->dt_price[0]->data_so[$i]->so_no);
                    $stmt->bindValue(':code', $data->dt_price[0]->data_so[$i]->code);
                    $stmt->bindValue(':item_code', $data->dt_price[0]->data_so[$i]->item_code);
                    $stmt->bindValue(':item_qty', $data->dt_price[0]->data_so[$i]->item_qty);
                    $stmt->bindValue(':item_qty2', $data->dt_price[0]->data_so[$i]->item_qty2);
                    $stmt->bindValue(':keterangan', $data->dt_price[0]->data_so[$i]->keterangan);
                    $stmt->bindValue(':code_comp', $data->dt_price[0]->data_so[$i]->code_comp);
                    $stmt->bindValue(':item_ton', $data->dt_price[0]->data_so[$i]->item_ton);
                    $stmt->bindValue(':desa', $data->dt_price[0]->data_so[$i]->desa);
                    $stmt->bindValue(':sales_form_no', $data->dt_price[0]->data_so[$i]->sales_code);
                    $stmt->bindValue(':name', $data->dt_price[0]->data_so[$i]->name);
                    $stmt->bindValue(':alamat_toko', $data->dt_price[0]->data_so[$i]->alamat_toko);
                    $stmt->bindValue(':insert_time', $now);
                    $stmt->bindValue(':insert_by', $username);
                    $stmt->bindValue(':status', '0');
                    $stmt->bindValue(':delivery_date', $timestamp);
                    $stmt->bindValue(':dept_code', $data->dt_price[0]->data_so[$i]->dept_code);
                    $stmt->bindValue(':dp_id', $id); 
                    $stmt->bindValue(':pengalihan', $pengalihan); 
                    $stmt->execute();
                }

                $response = [
                    "success" => 1,
                    "message" => "Data DP Berhasil Disimpan"
                ];

            } catch (PDOException $err) {
                $response = [
                    "success" => 0,
                    "id" => $id,
                    "message" => $err
                ];
            }

            // $query2 = "INSERT INTO t_dp_sc_detail(dp_id, sc_status) VALUES (:dp_id, :sc_status)";
            // $stmt2 = $dbgl->prepare($query2);
            // $dataheader2 = [
            //     ':dp_id' => $id,
            //     ':sc_status' => 0
            // ];

            // try {
            //     $stmt2->execute($dataheader2);
            // } catch (PDOException $ee) {
            //     $err = $ee;
            // }
            
            return $response; 
        }
    }

    function simpanDistributionPlanKirimLess($dbgl,$data)
    {
        $noDp = generateNoDp($dbgl,$data);

        // return $data;

        $response = [];
        if($data->office == '' || $data->tgl_perintah == '' || $data->username == '' || count($data->dt_so) <= 0){
            $response = [
                'success' => 0,
                'response' => 'Validasi error, silahkan isi data yang diperlukan'
            ];
        } else {
            $id             = "";
            $office         = $data->office;
            $tgl_perintah   = $data->tgl_perintah;
            $username       = $data->username;
            $now            = date('Y-m-d H:i:s');
            $loc            = $data->loc;
            $totalMuatan    = $data->totalMuatan;
            $totalharga     = $data->totalharga;

            $query = "INSERT INTO t_distribution_plan(no_dp, created_at, insert_by, freight_type, tgl_kirim, sc_status, total_muatan, total_biaya) 
                    VALUES (:no_dp, :created_at, :insert_by, :freight_type, :tgl_kirim, :sc_status, :total_muatan, :total_biaya) RETURNING id";
            $stmt = $dbgl->prepare($query);
            $dataheader = [
                ':no_dp' => $noDp,
                ':created_at' => $now,
                ':insert_by' => $username,
                ':freight_type' => "kirim",
                ':tgl_kirim' => $data->tgl_perintah,
                ':sc_status' => 0,
                ':total_muatan' => $totalMuatan,
                ':total_biaya' => $totalharga
            ];

            try {
                $stmt->execute($dataheader);
                $stmt->bindColumn('id', $id);
                $stmt->fetch();
                $response = [
                    'success' => 1,
                    'response' => 'sukses',
                ];
            } catch (PDOException $e) {
                $response = [
                    'success' => 0, 
                    'response' => 'silahkan coba kembali!',
                    'error' => $e
                ];
            }

            $sql = "INSERT INTO 
                        t_dp_detail(type,cabang,
                        tanggal,
                        no,
                        so_no,
                        code,
                        item_code,
                        item_qty,
                        item_qty2,
                        keterangan,
                        code_comp,
                        item_ton,
                        desa,
                        sales_form_no,
                        name,
                        alamat_toko,
                        insert_time,
                        insert_by,
                        status,
                        delivery_date,
                        dept_code,
                        dp_id,
                        pengalihan) 
                    VALUES 
                        (:type,:cabang,
                        :tanggal,
                        :no,
                        :so_no,
                        :code,
                        :item_code,
                        :item_qty,
                        :item_qty2,
                        :keterangan,
                        :code_comp,
                        :item_ton,
                        :desa,
                        :sales_form_no,
                        :name,
                        :alamat_toko,
                        :insert_time,
                        :insert_by,
                        :status,
                        :delivery_date,
                        :dept_code,
                        :dp_id,
                        :pengalihan)";

            $stmt = $dbgl->prepare($sql);

            try {

                for($i=0; $i < count($data->dt_price[0]->data_so); $i++) {

                    $lokasi_berangkat = "";
                    $pengalihan = null;

                    if ($data->office == $data->dt_price[0]->data_so[$i]->cabang) {
                        $lokasi_berangkat = $data->office;
                        $pengalihan = null;
                    } else {
                        $lokasi_berangkat = $data->office;
                        $pengalihan = $data->dt_price[0]->data_so[$i]->cabang;
                    }

                    $xtanggal = $data->tgl_perintah;
                    $timestamp = date('Y-m-d H:i:s', strtotime($xtanggal));

                    $stmt->bindValue(':type', $data->dt_price[0]->data_so[$i]->type);
                    $stmt->bindValue(':cabang', $lokasi_berangkat);
                    $stmt->bindValue(':tanggal', $data->dt_price[0]->data_so[$i]->tanggal);
                    $stmt->bindValue(':no', $data->dt_price[0]->data_so[$i]->no);
                    $stmt->bindValue(':so_no', $data->dt_price[0]->data_so[$i]->so_no);
                    $stmt->bindValue(':code', $data->dt_price[0]->data_so[$i]->code);
                    $stmt->bindValue(':item_code', $data->dt_price[0]->data_so[$i]->item_code);
                    $stmt->bindValue(':item_qty', $data->dt_price[0]->data_so[$i]->item_qty);
                    $stmt->bindValue(':item_qty2', $data->dt_price[0]->data_so[$i]->item_qty2);
                    $stmt->bindValue(':keterangan', $data->dt_price[0]->data_so[$i]->keterangan);
                    $stmt->bindValue(':code_comp', $data->dt_price[0]->data_so[$i]->code_comp);
                    $stmt->bindValue(':item_ton', $data->dt_price[0]->data_so[$i]->item_ton);
                    $stmt->bindValue(':desa', $data->dt_price[0]->data_so[$i]->desa);
                    $stmt->bindValue(':sales_form_no', $data->dt_price[0]->data_so[$i]->sales_code);
                    $stmt->bindValue(':name', $data->dt_price[0]->data_so[$i]->name);
                    $stmt->bindValue(':alamat_toko', $data->dt_price[0]->data_so[$i]->alamat_toko);
                    $stmt->bindValue(':insert_time', $now);
                    $stmt->bindValue(':insert_by', $username);
                    $stmt->bindValue(':status', '0');
                    $stmt->bindValue(':delivery_date', $timestamp);
                    $stmt->bindValue(':dept_code', $data->dt_price[0]->data_so[$i]->dept_code);
                    $stmt->bindValue(':dp_id', $id); 
                    $stmt->bindValue(':pengalihan', $pengalihan); 
                    $stmt->execute();
                }

                $response = [
                    "success" => 1,
                    "message" => "Data DP Berhasil Disimpan"
                ];

            } catch (PDOException $err) {
                $response = [
                    "success" => 0,
                    "id" => $id,
                    "message" => $err
                ];
            }

            // $query2 = "INSERT INTO t_dp_sc_detail(dp_id, sc_status) VALUES (:dp_id, :sc_status)";
            // $stmt2 = $dbgl->prepare($query2);
            // $dataheader2 = [
            //     ':dp_id' => $id,
            //     ':sc_status' => 0
            // ];

            // try {
            //     $stmt2->execute($dataheader2);
            // } catch (PDOException $ee) {
            //     $err = $ee;
            // }
            
            return $response; 
        }
    }
    
    function saveDpPrice($dbgl,$data)
    {
        // print_r($data);
        $response = [];
        $noDp = generateNoDp($dbgl,$data);

        $id             = "";
        $office         = $data->office;
        $tgl_perintah   = $data->tgl_perintah;
        $username       = $data->username;
        $now            = date('Y-m-d H:i:s');
        $loc            = $data->loc;
        $harga          = $data->harga;
        $totalMuatan    = $data->totalMuatan;

        $query = "INSERT INTO t_distribution_plan(no_dp, created_at, insert_by, freight_type, tgl_kirim, sc_status, total_muatan, total_biaya) 
                VALUES (:no_dp, :created_at, :insert_by, :freight_type, :tgl_kirim, :sc_status, :total_muatan, :total_biaya) 
                RETURNING id";
        $stmt = $dbgl->prepare($query);
        $dataheader = [
            ':no_dp' => $noDp,
            ':created_at' => $now,
            ':insert_by' => $username,
            ':freight_type' => "kirim",
            ':tgl_kirim' => $data->tgl_perintah,
            ':sc_status' => 0,
            ':total_muatan' => $totalMuatan,
            ':total_biaya' => $harga
        ];

        // print_r($dataheader);

        try {
            $stmt->execute($dataheader);
            $stmt->bindColumn('id', $id);
            $stmt->fetch();
            $response = [
                'success' => 1,
                'response' => 'sukses',
            ];
        } catch (PDOException $e) {
            $response = [
                'success' => 0, 
                'response' => 'silahkan coba kembali!',
                'error' => $e
            ];
        }

        $sql = "INSERT INTO 
                    t_dp_detail(type,cabang,
                    tanggal,
                    no,
                    so_no,
                    code,
                    item_code,
                    item_qty,
                    item_qty2,
                    keterangan,
                    code_comp,
                    item_ton,
                    desa,
                    sales_form_no,
                    name,
                    alamat_toko,
                    insert_time,
                    insert_by,
                    status,
                    delivery_date,
                    dept_code,
                    dp_id,
                    pengalihan) 
                VALUES 
                    (:type,:cabang,
                    :tanggal,
                    :no,
                    :so_no,
                    :code,
                    :item_code,
                    :item_qty,
                    :item_qty2,
                    :keterangan,
                    :code_comp,
                    :item_ton,
                    :desa,
                    :sales_form_no,
                    :name,
                    :alamat_toko,
                    :insert_time,
                    :insert_by,
                    :status,
                    :delivery_date,
                    :dept_code,
                    :dp_id,
                    :pengalihan)";

        $stmt = $dbgl->prepare($sql);

        try {

            // for($i=0; $i < count($data->dt_price[0]->data_so); $i++) {
                foreach ($data->dt_so as $items) {


                $lokasi_berangkat = "";
                $pengalihan = null;

                if ($data->office == $items->cabang) {
                    $lokasi_berangkat = $data->office;
                    $pengalihan = null;
                } else {
                    $lokasi_berangkat = $data->office;
                    $pengalihan = $items->cabang;
                }

                $xtanggal = $items->delivery_time;
                $timestamp = date('Y-m-d H:i:s', strtotime($xtanggal));

                $stmt->bindValue(':type', $items->type);
                $stmt->bindValue(':cabang', $lokasi_berangkat);
                $stmt->bindValue(':tanggal', $items->tanggal);
                $stmt->bindValue(':no', $items->no);
                $stmt->bindValue(':so_no', $items->so_no);
                $stmt->bindValue(':code', $items->code);
                $stmt->bindValue(':item_code', $items->item_code);
                $stmt->bindValue(':item_qty', $items->item_qty);
                $stmt->bindValue(':item_qty2', $items->item_qty2);
                $stmt->bindValue(':keterangan', $items->keterangan);
                $stmt->bindValue(':code_comp', $items->code_comp);
                $stmt->bindValue(':item_ton', $items->item_ton);
                $stmt->bindValue(':desa', $items->desa);
                $stmt->bindValue(':sales_form_no', $items->sales_code);
                $stmt->bindValue(':name', $items->name);
                $stmt->bindValue(':alamat_toko', $items->alamat_toko);
                $stmt->bindValue(':insert_time', $now);
                $stmt->bindValue(':insert_by', $username);
                $stmt->bindValue(':status', '0');
                $stmt->bindValue(':delivery_date', $timestamp);
                $stmt->bindValue(':dept_code', $items->dept_code);
                $stmt->bindValue(':dp_id', $id); 
                $stmt->bindValue(':pengalihan', $pengalihan); 
                $stmt->execute();
            }

        } catch (PDOException $err) {
            $response = [
                "success" => 0,
                "id" => $id,
                "message" => $err
            ];
        }


        $temp = "INSERT INTO 
                t_distribution_plan_temp2(code_comp, type, cabang, tanggal, code, no, so_no, item_code, item_qty, item_qty2, keterangan, item_ton, name, alamat_toko, insert_date, sc_status, dept_code, dp_id, harga) 
                VALUES (:code_comp, :type, :cabang, :tanggal, :code, :no, :so_no, :item_code, :item_qty, :item_qty2, :keterangan, :item_ton, :name, :alamat_toko, :insert_date, :sc_status, :dept_code, :dp_id, :harga)";
        $stmt = $dbgl->prepare($temp);


        try {
            foreach ($data->dt_so as $items) {
                # code...
                $lokasi_berangkat = "";
                // $pengalihan = null;

                if ($data->office == $items->cabang) {
                    $lokasi_berangkat = $data->office;
                    // $pengalihan = null;
                } else {
                    $lokasi_berangkat = $data->office;
                    // $pengalihan = $items->cabang;
                }

                $stmt->bindValue(':code_comp', $items->code_comp);
                $stmt->bindValue(':type', $items->type);
                $stmt->bindValue(':cabang', $lokasi_berangkat);
                $stmt->bindValue(':tanggal', $items->tanggal);
                $stmt->bindValue(':code', $items->code);
                $stmt->bindValue(':no', $items->no);
                $stmt->bindValue(':so_no', $items->so_no);
                $stmt->bindValue(':item_code', $items->item_code);
                $stmt->bindValue(':item_qty', $items->item_qty);
                $stmt->bindValue(':item_qty2', $items->item_qty2);
                $stmt->bindValue(':keterangan', $items->keterangan);
                $stmt->bindValue(':item_ton', $items->item_ton);
                $stmt->bindValue(':name', $items->ledgername);
                $stmt->bindValue(':alamat_toko', $items->alamat_toko);
                $stmt->bindValue(':insert_date', $now);
                $stmt->bindValue(':sc_status', 'CHARGE');
                $stmt->bindValue(':dept_code', $items->dept_code);
                $stmt->bindValue(':dp_id', $id);  
                $stmt->bindValue(':harga', $harga);  
                $stmt->execute();
            }

            $response = [
                "success" => 1,
                "message" => "Data DP Berhasil Disimpan"
            ];

        } catch (PDOException $err) {
            $response = [
                "success" => 0,
                "id" => $id,
                "message" => $err
            ];
        }
        return $response;
    }

    function saveDistributionPlanAmbil($dbgl,$data)
    {

        $now = date('Y-m-d');

        $xprice_so = getPriceAmtBySO($dbgl,$data);
        $price_so = $xprice_so['data_so'];

        $price_sales = getPriceAmtBySales($dbgl,$data,$xprice_so);

        // $arr_all[] = array (
        //     'price_so' => $data_price_so,
        //     'price_sales' => $price_sales
        // );

        // return $arr_all;

        for ($i=0; $i < count($price_so) ; $i++) {
            
            if (trim($price_so[$i]['item'][0]['item_code']) == $price_sales[$i]['item_code']) {
            
                if ($price_so[$i]['item'][0]['price_amt'] == $price_sales[$i]['kirim']) {
                    $arr_all[] = array(
                        'hasil' => 1,
                        'data_so' => $price_so[$i],
                        'price_so' => array(
                            'so_no' => $price_so[$i]['item'][0]['pr_no'],
                            'cabang' => $price_so[$i]['item'][0]['spk_no'],
                            'item_code' => trim($price_so[$i]['item'][0]['item_code']),
                            'price_amount' => $price_so[$i]['item'][0]['price_amt'],
                        ),
                        'price_sales' => array(
                            'cabang' => $price_sales[$i]['coce_code'],
                            'item_code' => $price_sales[$i]['item_code'],
                            'price_amount' => $price_sales[$i]['kirim']
                        )
                    );
                } else {
                    $arr_all[] = array(
                        'hasil' => 2,
                        'data_so' => $price_so[$i],
                        'price_so' => array(
                            'so_no' => $price_so[$i]['item'][0]['pr_no'],
                            'cabang' => $price_so[$i]['item'][0]['spk_no'],
                            'item_code' => trim($price_so[$i]['item'][0]['item_code']),
                            'price_amount' => $price_so[$i]['item'][0]['price_amt'],
                        ),
                        'price_sales' => array(
                            'cabang' => $price_sales[$i]['coce_code'],
                            'item_code' => $price_sales[$i]['item_code'],
                            'price_amount' => $price_sales[$i]['kirim']
                        )
                    );
                }
            } else {
                $arr_all[] = array(
                    'hasil' => 0,
                    'data_so' => $price_so[$i],
                    'price_so' => array(
                        'so_no' => $price_so[$i]['item'][0]['pr_no'],
                        'cabang' => $price_so[$i]['item'][0]['spk_no'],
                        'item_code' => trim($price_so[$i]['item'][0]['item_code']),
                        'price_amount' => $price_so[$i]['item'][0]['price_amt'],
                    ),
                    'price_sales' => array(
                        'cabang' => $price_sales[$i]['coce_code'],
                        'item_code' => $price_sales[$i]['item_code'],
                        'price_amount' => $price_sales[$i]['kirim']
                    )
                );
            }
        }

        return $arr_all;
        
    }

    function actionSaveDistributionPlanAmbil2($dbgl,$data)
    {
        $now = date('Y-m-d');

        $noDp = generateNoDp($dbgl,$data);

        $response = [];
        if($data->office == '' || $data->tgl_perintah == '' || $data->username == '' || count($data->dt_so) <= 0){
            $response = [
                'success' => 0,
                'response' => 'Validasi error, silahkan isi data yang diperlukan'
            ];
        } else {
            $id             = "";
            $tgl_perintah   = $data->tgl_perintah;
            $office         = $data->office;
            $username       = $data->username;
            $now            = date('Y-m-d H:i:s');

            $query = "INSERT INTO t_distribution_plan_item(tgl_kirim,tgl_perintah, no_dp, created_at, updated_at, insert_by, freight_type, asal_gudang, status_so) VALUES (:tgl_kirim, :tgl_perintah, :no_dp, :created_at, :updated_at, :insert_by, :freight_type, :asal_gudang, :status_so) RETURNING id";
            $stmt = $dbgl->prepare($query);
            $dataheader = [
                ':tgl_kirim' => $tgl_perintah,
                ':tgl_perintah' => $tgl_perintah,
                ':no_dp' => $noDp,
                ':created_at' => $now,
                ':updated_at' => $now,
                ':insert_by' => $username,
                ':freight_type' => "ambil",
                ':asal_gudang' => $office,
                ':status_so' => '0'
            ];

            try {
                $stmt->execute($dataheader);
                $stmt->bindColumn('id', $id);
                $stmt->fetch();
                $response = [
                    'success' => 1,
                    'response' => 'sukses',
                ];
            } catch (PDOException $e) {
                $response = [
                    'success' => 0,
                    'response' => 'silahkan coba kembali!',
                    'error' => $e
                ];
            }

            $sql = "INSERT INTO 
                        t_distribution_plan_list(type,cabang,
                        tanggal,
                        no,
                        so_no,
                        code,
                        item_code,
                        item_qty,
                        item_qty2,
                        keterangan,
                        code_comp,
                        item_ton,
                        desa,
                        sales_from_no,
                        name,
                        alamat_toko,
                        insert_time,
                        insert_by,
                        status,
                        dept_code,
                        dp_id) 
                    VALUES 
                        (:type,:cabang,
                        :tanggal,
                        :no,
                        :so_no,
                        :code,
                        :item_code,
                        :item_qty,
                        :item_qty2,
                        :keterangan,
                        :code_comp,
                        :item_ton,
                        :desa,
                        :sales_from_no,
                        :name,
                        :alamat_toko,
                        :insert_time,
                        :insert_by,
                        :status,
                        :dept_code,
                        :dp_id)";

            $stmt = $dbgl->prepare($sql);

            try {

                for($i=0; $i < count($data->dt_price[0]->data_so); $i++) {
                    $stmt->bindValue(':type', $data->dt_price[0]->data_so[$i]->type);
                    $stmt->bindValue(':cabang', $data->dt_price[0]->data_so[$i]->cabang);
                    $stmt->bindValue(':tanggal', $data->dt_price[0]->data_so[$i]->tanggal);
                    $stmt->bindValue(':no', $data->dt_price[0]->data_so[$i]->no);
                    $stmt->bindValue(':so_no', $data->dt_price[0]->data_so[$i]->so_no);
                    $stmt->bindValue(':code', $data->dt_price[0]->data_so[$i]->code);
                    $stmt->bindValue(':item_code', $data->dt_price[0]->data_so[$i]->item_code);
                    $stmt->bindValue(':item_qty', $data->dt_price[0]->data_so[$i]->item_qty);
                    $stmt->bindValue(':item_qty2', $data->dt_price[0]->data_so[$i]->item_qty2);
                    $stmt->bindValue(':keterangan', $data->dt_price[0]->data_so[$i]->keterangan);
                    $stmt->bindValue(':code_comp', $data->dt_price[0]->data_so[$i]->code_comp);
                    $stmt->bindValue(':item_ton', $data->dt_price[0]->data_so[$i]->item_ton);
                    $stmt->bindValue(':desa', $data->dt_price[0]->data_so[$i]->desa);
                    $stmt->bindValue(':sales_from_no', $data->dt_price[0]->data_so[$i]->sales_code);
                    $stmt->bindValue(':name', $data->dt_price[0]->data_so[$i]->name);
                    $stmt->bindValue(':alamat_toko', $data->dt_price[0]->data_so[$i]->alamat_toko);
                    $stmt->bindValue(':insert_time', $now);
                    $stmt->bindValue(':insert_by', $username);
                    $stmt->bindValue(':status', '0');
                    $stmt->bindValue(':dept_code', $data->dt_price[0]->data_so[$i]->dept_code);
                    $stmt->bindValue(':dp_id', $id);
                    $stmt->execute();
                }

                $response = [
                    "success" => 1,
                    "message" => "Data DP Berhasil Disimpan"
                ];

            } catch (PDOException $err) {
                $response = [
                    "success" => 0,
                    "id" => $id,
                    "message" => $err
                ];
            }
        }

        return $response;

    }

    function actionSaveDistributionPlanAmbil($dbgl,$data)
    {
        $noDp = generateNoDp($dbgl,$data);

        // return $data;

        $response = [];
        if($data->office == '' || $data->tgl_perintah == '' || $data->username == '' || count($data->dt_so) <= 0){
            $response = [
                'success' => 0,
                'response' => 'Validasi error, silahkan isi data yang diperlukan'
            ];
        } else {
            $id             = "";
            $office         = $data->office;
            $tgl_perintah   = $data->tgl_perintah;
            $username       = $data->username;
            $now            = date('Y-m-d H:i:s');
            $loc            = $data->loc;

            $query = "INSERT INTO t_distribution_plan(no_dp, created_at, insert_by, freight_type, tgl_kirim, sc_status) VALUES (:no_dp, :created_at, :insert_by, :freight_type, :tgl_kirim, :sc_status) RETURNING id";
            $stmt = $dbgl->prepare($query);
            $dataheader = [
                ':no_dp' => $noDp,
                ':created_at' => $now,
                ':insert_by' => $username,
                ':freight_type' => "ambil",
                ':tgl_kirim' => $data->dt_price[0]->data_so[0]->tanggal,
                ':sc_status' => 0,
            ];

            try {
                $stmt->execute($dataheader);
                $stmt->bindColumn('id', $id);
                $stmt->fetch();
                $response = [
                    'success' => 1,
                    'response' => 'sukses',
                ];
            } catch (PDOException $e) {
                $response = [
                    'success' => 0, 
                    'response' => 'silahkan coba kembali!',
                    'error' => $e
                ];
            }

            $sql = "INSERT INTO 
                        t_dp_detail(type,cabang,
                        tanggal,
                        no,
                        so_no,
                        code,
                        item_code,
                        item_qty,
                        item_qty2,
                        keterangan,
                        code_comp,
                        item_ton,
                        desa,
                        sales_form_no,
                        name,
                        alamat_toko,
                        insert_time,
                        insert_by,
                        status,
                        dept_code,
                        dp_id,
                        pengalihan) 
                    VALUES 
                        (:type,:cabang,
                        :tanggal,
                        :no,
                        :so_no,
                        :code,
                        :item_code,
                        :item_qty,
                        :item_qty2,
                        :keterangan,
                        :code_comp,
                        :item_ton,
                        :desa,
                        :sales_form_no,
                        :name,
                        :alamat_toko,
                        :insert_time,
                        :insert_by,
                        :status,
                        :dept_code,
                        :dp_id,
                        :pengalihan)";

            $stmt = $dbgl->prepare($sql);

            try {

                for($i=0; $i < count($data->dt_price[0]->data_so); $i++) {

                    $lokasi_berangkat = "";
                    $pengalihan = null;

                    if ($data->office == $data->dt_price[0]->data_so[$i]->cabang) {
                        $lokasi_berangkat = $data->office;
                        $pengalihan = null;
                    } else {
                        $lokasi_berangkat = $data->office;
                        $pengalihan = $data->dt_price[0]->data_so[$i]->cabang;
                    }

                    $stmt->bindValue(':type', $data->dt_price[0]->data_so[$i]->type);
                    $stmt->bindValue(':cabang', $lokasi_berangkat);
                    $stmt->bindValue(':tanggal', $data->dt_price[0]->data_so[$i]->tanggal);
                    $stmt->bindValue(':no', $data->dt_price[0]->data_so[$i]->no);
                    $stmt->bindValue(':so_no', $data->dt_price[0]->data_so[$i]->so_no);
                    $stmt->bindValue(':code', $data->dt_price[0]->data_so[$i]->code);
                    $stmt->bindValue(':item_code', $data->dt_price[0]->data_so[$i]->item_code);
                    $stmt->bindValue(':item_qty', $data->dt_price[0]->data_so[$i]->item_qty);
                    $stmt->bindValue(':item_qty2', $data->dt_price[0]->data_so[$i]->item_qty2);
                    $stmt->bindValue(':keterangan', $data->dt_price[0]->data_so[$i]->keterangan);
                    $stmt->bindValue(':code_comp', $data->dt_price[0]->data_so[$i]->code_comp);
                    $stmt->bindValue(':item_ton', $data->dt_price[0]->data_so[$i]->item_ton);
                    $stmt->bindValue(':desa', $data->dt_price[0]->data_so[$i]->desa);
                    $stmt->bindValue(':sales_form_no', $data->dt_price[0]->data_so[$i]->sales_code);
                    $stmt->bindValue(':name', $data->dt_price[0]->data_so[$i]->name);
                    $stmt->bindValue(':alamat_toko', $data->dt_price[0]->data_so[$i]->alamat_toko);
                    $stmt->bindValue(':insert_time', $now);
                    $stmt->bindValue(':insert_by', $username);
                    $stmt->bindValue(':status', '0');
                    $stmt->bindValue(':dept_code', $data->dt_price[0]->data_so[$i]->dept_code);
                    $stmt->bindValue(':dp_id', $id); 
                    $stmt->bindValue(':pengalihan', $pengalihan); 
                    $stmt->execute();
                }

                $response = [
                    "success" => 1,
                    "message" => "Data DP Berhasil Disimpan"
                ];

            } catch (PDOException $err) {
                $response = [
                    "success" => 0,
                    "id" => $id,
                    "message" => $err
                ];
            }

            // $query2 = "INSERT INTO t_dp_sc_detail(dp_id, sc_status) VALUES (:dp_id, :sc_status)";
            // $stmt2 = $dbgl->prepare($query2);
            // $dataheader2 = [
            //     ':dp_id' => $id,
            //     ':sc_status' => 0
            // ];

            // try {
            //     $stmt2->execute($dataheader2);
            // } catch (PDOException $ee) {
            //     $err = $ee;
            // }
            
            return $response; 
        }
    }

    function getDisPlanListKirim($dbgl,$data)
    {
        $responseData = [];

        try {

            $query = "SELECT a.*, to_char(a.tgl_kirim, 'DD Mon YYYY') AS tgl_kirim2, to_char(a.tgl_perintah, 'DD Mon YYYY') AS tgl_perintah2 
                        FROM t_distribution_plan_item a 
                        WHERE a.no_to IS NULL AND a.freight_type = 'kirim'
                        AND a.no_pol IS NULL AND a.sopir IS NULL";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $response = $rows;

        } catch (PDOException $e) {
            $response = $e;
        }

        for($i=0;$i<count($response);$i++) {

            $id_item = $response[$i]['id'];

            try {
                $sql = "SELECT b.*
                            FROM t_distribution_plan_list b
                            INNER JOIN t_card c on b.code = c.code and b.code_comp = c.code_comp
                            WHERE dp_id = '$id_item' AND b.status = '0'";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            $hasil[] = array(
                "id" => $response[$i]['id'],
                "no_dp" => $response[$i]['no_dp'],
                "asal_gudang" => $response[$i]['asal_gudang'],
                "wh_code" => $response[$i]['wh_code'],
                "freight_type" => $response[$i]['freight_type'],
                "tgl_kirim" => $response[$i]['tgl_kirim'],
                "tgl_kirim2" => $response[$i]['tgl_kirim2'],
                "tgl_perintah2" => $response[$i]['tgl_perintah2'],
                "price_amount" => $response[$i]['price_amount'],
                "status_so" => $response[$i]['status_so'],
                "item" => $arr
            );
            $arr = [];

        } 
    
        return $hasil;
    }

    function getDisPlanKirimByDP($dbgl,$data)
    {
        $response = [];

        try {

            $query = "SELECT a.*, to_char(a.tgl_kirim, 'DD Mon YYYY') AS tgl_kirim2 
                        FROM t_distribution_plan_item a 
                        WHERE a.no_to IS NULL AND a.freight_type = 'kirim' AND no_dp = '$data->no_dp'";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $response = $rows;

        } catch (PDOException $e) {
            $response = $e;
        }

        for($i=0;$i<count($response);$i++) {

            $id_item = $response[$i]['id'];

            try {
                $sql = "SELECT b.*
                            FROM t_distribution_plan_list b
                            INNER JOIN t_card c on b.code = c.code and b.code_comp = c.code_comp
                            WHERE dp_id = '$id_item' AND b.status = '0'";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            $hasil[] = array(
                "id" => $response[$i]['id'],
                "no_dp" => $response[$i]['no_dp'],
                "asal_gudang" => $response[$i]['asal_gudang'],
                "wh_code" => $response[$i]['wh_code'],
                "freight_type" => $response[$i]['freight_type'],
                "tgl_kirim" => $response[$i]['tgl_kirim'],
                "tgl_kirim2" => $response[$i]['tgl_kirim2'],
                "tgl_perintah" => $response[$i]['tgl_perintah'],
                "price_amount" => $response[$i]['price_amount'],
                "status_so" => $response[$i]['status_so'],
                "no_pol" => $response[$i]['no_pol'],
                "sopir" => $response[$i]['sopir'],
                "ritase" => $response[$i]['ritase'],
                "catatan" => $response[$i]['catatan'],
                "catatan_to" => $response[$i]['catatan_to'],
                "item" => $arr
            );
            $arr = [];

        } 
    
        return $hasil;
    }

    function getDisPlanAmbilByDP($dbgl,$data)
    {
        $response = [];

        try {

            $query = "SELECT a.*, to_char(a.tgl_kirim, 'DD Mon YYYY') AS tgl_kirim2 
                        FROM t_distribution_plan_item a 
                        WHERE a.no_to IS NULL AND a.freight_type = 'ambil' AND no_dp = '$data->no_dp'";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $response = $rows;

        } catch (PDOException $e) {
            $response = $e;
        }

        for($i=0;$i<count($response);$i++) {

            $id_item = $response[$i]['id'];

            try {
                $sql = "SELECT b.*
                            FROM t_distribution_plan_list b
                            INNER JOIN t_card c on b.code = c.code and b.code_comp = c.code_comp
                            WHERE dp_id = '$id_item' AND b.status = '0'";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            $hasil[] = array(
                "id" => $response[$i]['id'],
                "no_dp" => $response[$i]['no_dp'],
                "asal_gudang" => $response[$i]['asal_gudang'],
                "wh_code" => $response[$i]['wh_code'],
                "freight_type" => $response[$i]['freight_type'],
                "tgl_kirim" => $response[$i]['tgl_kirim'],
                "tgl_kirim2" => $response[$i]['tgl_kirim2'],
                "price_amount" => $response[$i]['price_amount'],
                "status_so" => $response[$i]['status_so'],
                "no_pol" => $response[$i]['no_pol'],
                "sopir" => $response[$i]['sopir'],
                "ritase" => $response[$i]['ritase'],
                "catatan_to" => $response[$i]['catatan_to'],
                "item" => $arr
            );
            $arr = [];

        } 
    
        return $hasil;
    }

    function getDisPlanTransferByDP($dbgl,$data)
    {
        $response = [];

        try {

            $query = "SELECT a.*, to_char(a.tgl_kirim, 'DD Mon YYYY') AS tgl_kirim2 
                        FROM t_distribution_plan_item a 
                        WHERE a.no_to IS NOT NULL AND a.freight_type = 'kirim' AND no_dp = '$data->no_dp'";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $response = $rows;

        } catch (PDOException $e) {
            $response = $e;
        }

        for($i=0;$i<count($response);$i++) {

            $id_item = $response[$i]['id'];

            try {
                $sql = "SELECT b.*
                            FROM t_distribution_plan_list b
                            INNER JOIN t_card c on b.code = c.code and b.code_comp = c.code_comp
                            WHERE dp_id = '$id_item' AND b.status = '0'";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            $hasil[] = array(
                "id" => $response[$i]['id'],
                "no_dp" => $response[$i]['no_dp'],
                "asal_gudang" => $response[$i]['asal_gudang'],
                "wh_code" => $response[$i]['wh_code'],
                "freight_type" => $response[$i]['freight_type'],
                "tgl_kirim" => $response[$i]['tgl_kirim'],
                "tgl_kirim2" => $response[$i]['tgl_kirim2'],
                "price_amount" => $response[$i]['price_amount'],
                "status_so" => $response[$i]['status_so'],
                "no_pol" => $response[$i]['no_pol'],
                "sopir" => $response[$i]['sopir'],
                "ritase" => $response[$i]['ritase'],
                "catatan_to" => $response[$i]['catatan_to'],
                "item" => $arr
            );
            $arr = [];

        } 
    
        return $hasil;
    }

    function getDisPlanListAmbil($dbgl,$data)
    {
        $responseData = [];

        try {

            $query = "SELECT a.*, to_char(a.tgl_kirim, 'DD Mon YYYY') AS tgl_kirim2, to_char(a.tgl_perintah, 'DD Mon YYYY') AS tgl_perintah2 
                        FROM t_distribution_plan_item a 
                        WHERE a.no_to IS NULL AND a.freight_type = 'ambil'
                        AND a.no_pol IS NULL AND a.sopir IS NULL";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $response = $rows;

        } catch (PDOException $e) {
            $response = $e;
        }

        for($i=0;$i<count($response);$i++) {

            $id_item = $response[$i]['id'];

            try {
                $sql = "SELECT b.* 
                        FROM t_distribution_plan_list b
                        INNER JOIN t_card c on b.code = c.code and b.code_comp = c.code_comp
                        WHERE dp_id = '$id_item'";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            $hasil[] = array(
                "id" => $response[$i]['id'],
                "no_dp" => $response[$i]['no_dp'],
                "asal_gudang" => $response[$i]['asal_gudang'],
                "wh_code" => $response[$i]['wh_code'],
                "freight_type" => $response[$i]['freight_type'],
                "tgl_kirim" => $response[$i]['tgl_kirim'],
                "tgl_kirim2" => $response[$i]['tgl_kirim2'],
                "tgl_perintah2" => $response[$i]['tgl_perintah2'],
                "price_amount" => $response[$i]['price_amount'],
                "status_so" => $response[$i]['status_so'],
                "item" => $arr
            );
            $arr = [];

        } 
    
        return $hasil;
    }

    function saveProsesPengiriman($dbgl,$data)
    {
        return $data;
        // try {

        //     $sql = "INSERT INTO 
        //               t_distribution_plan_pengiriman(id_dp,
        //               kd_off_start,
        //               kd_off_end,
        //               tgl_kirim,
        //               alasan,
        //               code,
        //               ledgername,
        //               alamat,
        //               qty_barang) 
        //             VALUES 
        //               (:id_dp,
        //                 :kd_off_start,
        //                 :kd_off_end,
        //                 :tgl_kirim,
        //                 :alasan,
        //                 :code,
        //                 :ledgername,
        //                 :alamat,
        //                 :qty_barang)";
        
        //     $stmt = $dbgl->prepare($sql);
        
        //     $stmt->bindValue(':id_dp', $data->detail->id_dp);
        //     $stmt->bindValue(':kd_off_start', $data->gd_start);
        //     $stmt->bindValue(':kd_off_end', $data->detail->cabang);
        //     $stmt->bindValue(':tgl_kirim', $data->tgl_kirim);
        //     $stmt->bindValue(':alasan', $data->alasan);
        //     $stmt->bindValue(':code', $data->detail->code);
        //     $stmt->bindValue(':ledgername', $data->detail->ledgername);
        //     $stmt->bindValue(':alamat', $data->detail->alamat_toko);
        //     $stmt->bindValue(':qty_barang', $data->qty);
        //     $stmt->execute();

        //     $respon = [
        //         "success" => 1,
        //         "message" => "Data Proses Pengiriman Berhasil Disimpan"
        //     ];

        // } catch (PDOException $e) {
        //     $e->getMessage();
        //     $respon = [
        //         "success" => 0,
        //         "message" => $e
        //     ];
        // }

        // return $respon;
    }

    function getDisPlanListTransfer($dbgl,$data)
    {
        $responseData = [];

        try {

            $query = "SELECT a.*, to_char(a.tgl_kirim, 'DD Mon YYYY') AS tgl_kirim2 
                        FROM t_distribution_plan_item a 
                        WHERE a.no_to IS NOT NULL AND a.freight_type = 'kirim'";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $response = $rows;

        } catch (PDOException $e) {
            $response = $e;
        }

        for($i=0;$i<count($response);$i++) {

            $id_item = $response[$i]['id'];

            try {
                $sql = "SELECT b.*
                            FROM t_distribution_plan_list b
                            INNER JOIN t_card c on b.code = c.code and b.code_comp = c.code_comp
                            WHERE dp_id = '$id_item' AND b.status = '0'";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            $hasil[] = array(
                "id" => $response[$i]['id'],
                "no_dp" => $response[$i]['no_dp'],
                "asal_gudang" => $response[$i]['asal_gudang'],
                "wh_code" => $response[$i]['wh_code'],
                "freight_type" => $response[$i]['freight_type'],
                "tgl_kirim" => $response[$i]['tgl_kirim'],
                "tgl_kirim2" => $response[$i]['tgl_kirim2'],
                "price_amount" => $response[$i]['price_amount'],
                "status_so" => $response[$i]['status_so'],
                "item" => $arr
            );
            $arr = [];

        } 
    
        return $hasil;
    }
    
    function getDaftarPengirimanKirim($dbgl,$data)
    {
        $responseData = [];

        try {

            $query = "SELECT a.*, to_char(a.tgl_kirim, 'DD Mon YYYY') AS tgl_kirim2, 
                      (SELECT string_agg(name::text, ',') FROM t_distribution_plan_list WHERE dp_id = a.id) AS name
                      FROM t_distribution_plan_item a WHERE status_so = '1' 
                      AND freight_type = 'kirim' AND no_to IS NULL";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }

        for($i=0;$i<count($responseData);$i++) {

            $dpId = $responseData[$i]['id'];

            try {
                $sql = "SELECT 
                            id, a.code_comp, a.type, a.cabang, a.tanggal, a.code, a.name, a.no, a.so_no, a.item_code, a.item_qty, a.item_qty2, 
                            a.keterangan, a.item_ton, a.desa, a.alamat_toko, 0 AS total_do, a.status, a.dept_code, d.name as dept_name
                        FROM t_distribution_plan_list a
                        LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp
                        WHERE a.status = '0' AND a.dp_id = {$dpId}
                        ORDER BY a.so_no";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            $hasil[] = array(
                "id" => $responseData[$i]['id'],
                "no_dp" => $responseData[$i]['no_dp'],
                "name" => $responseData[$i]['name'],
                "tgl_kirim" => $responseData[$i]['tgl_kirim'],
                "tgl_kirim2" => $responseData[$i]['tgl_kirim2'],
                "wh_code" => $responseData[$i]['wh_code'],
                "item" => $arr
            );
            $arr = [];

        }
    
        return $hasil;
    }

    function getDaftarPengirimanKirimByID($dbgl,$data)
    {
        $responseData = [];

        // return $data;

        try {

            $query = "SELECT a.*, to_char(a.tgl_kirim, 'DD Mon YYYY') AS tgl_kirim2 
                        FROM t_distribution_plan_item a 
                        WHERE status_so = '1' 
                        AND freight_type = 'kirim' 
                        AND no_to IS NULL
                        AND id = $data->id_dt";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }

        for($i=0;$i<count($responseData);$i++) {

            try {
                $sql = "SELECT 
                            id, a.code_comp, a.type, a.cabang, a.tanggal, a.code, a.name, a.no, a.so_no, a.item_code, a.item_qty, a.item_qty2, 
                            a.keterangan, a.item_ton, a.desa, a.alamat_toko, 0 AS total_do, a.status, a.dept_code, d.name as dept_name
                        FROM t_distribution_plan_list a
                        LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp
                        WHERE a.status = '0' AND a.dp_id = {$data->id_dt}
                        ORDER BY a.so_no";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            $hasil[] = array(
                "id" => $responseData[$i]['id'],
                "no_dp" => $responseData[$i]['no_dp'],
                "tgl_kirim" => $responseData[$i]['tgl_kirim'],
                "tgl_kirim2" => $responseData[$i]['tgl_kirim2'],
                "wh_code" => $responseData[$i]['wh_code'],
                "no_pol" => $responseData[$i]['no_pol'],
                "sopir" => $responseData[$i]['sopir'],
                "item" => $arr
            );
            $arr = [];

        }
    
        return $hasil;
    }

    function getDaftarPengirimanAmbil($dbgl,$data)
    {
        $responseData = [];

        try {
            $query = "SELECT a.*, to_char(a.tgl_perintah, 'DD Mon YYYY') AS tgl_perintah2 FROM t_distribution_plan_item a WHERE status_so = '1' 
                      AND freight_type = 'ambil' AND no_to IS NULL";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }

        for($i=0;$i<count($responseData);$i++) {

            $dpId = $responseData[$i]['id'];

            try {
                $sql = "SELECT 
                            id, a.code_comp, a.type, a.cabang, a.tanggal, a.code, a.name, a.no, a.so_no, a.item_code, a.item_qty, a.item_qty2, 
                            a.keterangan, a.item_ton, a.desa, a.alamat_toko, 0 AS total_do, a.status, a.dept_code, d.name as dept_name
                        FROM t_distribution_plan_list a
                        LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp
                        WHERE a.status = '0' AND a.dp_id = {$dpId}
                        ORDER BY a.so_no";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            $hasil[] = array(
                "id" => $responseData[$i]['id'],
                "no_dp" => $responseData[$i]['no_dp'],
                "tgl_perintah" => $responseData[$i]['tgl_perintah'],
                "tgl_perintah2" => $responseData[$i]['tgl_perintah2'],
                "wh_code" => $responseData[$i]['wh_code'],
                "item" => $arr
            );
            $arr = [];

        }
    
        return $hasil;
    }

    function getDaftarPengirimanAmbilByID($dbgl,$data)
    {
        $responseData = [];

        try {
            $query = "SELECT a.*, to_char(a.tgl_perintah, 'DD Mon YYYY') AS tgl_perintah2 
                        FROM t_distribution_plan_item a 
                        WHERE status_so = '1' 
                        AND freight_type = 'ambil'
                        AND id = $data->id_dt";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }

        for($i=0;$i<count($responseData);$i++) {

            $dpId = $responseData[$i]['id'];

            try {
                $sql = "SELECT 
                            id, a.code_comp, a.type, a.cabang, a.tanggal, a.code, a.name, a.no, a.so_no, a.item_code, a.item_qty, a.item_qty2, 
                            a.keterangan, a.item_ton, a.desa, a.alamat_toko, 0 AS total_do, a.status, a.dept_code, d.name as dept_name
                        FROM t_distribution_plan_list a
                        LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp
                        WHERE a.status = '0' AND a.dp_id = {$data->id_dt}
                        ORDER BY a.so_no";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            $hasil[] = array(
                "id" => $responseData[$i]['id'],
                "no_dp" => $responseData[$i]['no_dp'],
                "tgl_perintah" => $responseData[$i]['tgl_perintah'],
                "tgl_perintah2" => $responseData[$i]['tgl_perintah2'],
                "wh_code" => $responseData[$i]['wh_code'],
                "no_pol" => $responseData[$i]['no_pol'],
                "sopir" => $responseData[$i]['sopir'],
                "item" => $arr
            );
            $arr = [];

        }
    
        return $hasil;
    }

    function getDaftarPengirimanTransfer($dbgl,$data)
    {
        $responseData = [];

        try {

            $query = "SELECT a.*, to_char(a.tgl_kirim, 'DD Mon YYYY') AS tgl_kirim2 FROM t_distribution_plan_item a 
                      WHERE status_so = '1' AND freight_type = 'kirim' AND no_to IS NOT NULL";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }

        for($i=0;$i<count($responseData);$i++) {

            $dpId = $responseData[$i]['id'];

            try {
                $sql = "SELECT 
                            id, a.code_comp, a.type, a.cabang, a.tanggal, a.code, a.name, a.no, a.so_no, a.item_code, a.item_qty, a.item_qty2, 
                            a.keterangan, a.item_ton, a.desa, a.alamat_toko, 0 AS total_do, a.status, a.dept_code, d.name as dept_name
                        FROM t_distribution_plan_list a
                        LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp
                        WHERE a.status = '0' AND a.dp_id = {$dpId}
                        ORDER BY a.so_no";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            $hasil[] = array(
                "id" => $responseData[$i]['id'],
                "no_dp" => $responseData[$i]['no_dp'],
                "tgl_kirim" => $responseData[$i]['tgl_kirim'],
                "tgl_kirim2" => $responseData[$i]['tgl_kirim2'],
                "wh_code" => $responseData[$i]['wh_code'],
                "item" => $arr
            );
            $arr = [];

        }
    
        return $hasil;
    }

    function getDaftarPengirimanTransferByID($dbgl,$data)
    {
        $responseData = [];

        try {

            $query = "SELECT a.*, to_char(a.tgl_kirim, 'DD Mon YYYY') AS tgl_kirim2 
                        FROM t_distribution_plan_item a 
                        WHERE status_so = '1' 
                        AND freight_type = 'kirim' 
                        AND no_to IS NOT NULL
                        AND id = $data->id_dt";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }

        for($i=0;$i<count($responseData);$i++) {

            $dpId = $responseData[$i]['id'];

            try {
                $sql = "SELECT 
                            id, a.code_comp, a.type, a.cabang, a.tanggal, a.code, a.name, a.no, a.so_no, a.item_code, a.item_qty, a.item_qty2, 
                            a.keterangan, a.item_ton, a.desa, a.alamat_toko, 0 AS total_do, a.status, a.dept_code, d.name as dept_name
                        FROM t_distribution_plan_list a
                        LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp
                        WHERE a.status = '0' AND a.dp_id = {$data->id_dt}
                        ORDER BY a.so_no";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            $hasil[] = array(
                "id" => $responseData[$i]['id'],
                "no_dp" => $responseData[$i]['no_dp'],
                "tgl_kirim" => $responseData[$i]['tgl_kirim'],
                "tgl_kirim2" => $responseData[$i]['tgl_kirim2'],
                "wh_code" => $responseData[$i]['wh_code'],
                "no_pol" => $responseData[$i]['no_pol'],
                "sopir" => $responseData[$i]['sopir'],
                "no_to" => $responseData[$i]['no_to'],
                "catatan_to" => $responseData[$i]['catatan_to'],
                "tgl_terima" => $responseData[$i]['tgl_terima'],
                "item" => $arr
            );
            $arr = [];

        }
    
        return $hasil;
    }

    function deleteDistributionPlan($dbgl,$data)
    {
        $responseData = [];

        try {

            $query = "DELETE FROM t_distribution_plan_item WHERE id = $data->id_dp";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $response1[] = array(
                'result' => 1,
                "message" => "Data Berhasil Di Hapus"
            );

        } catch (PDOException $e) {
            $response1[] = array(
                'result' => 0,
                "message" => $e
            );
        }

        try {

            $query = "DELETE FROM t_distribution_plan_list WHERE dp_id = $data->id_dp";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $response2[] = array(
                'result' => 1,
                "message" => "Data Berhasil Di Hapus"
            );

        } catch (PDOException $e) {
            $response2[] = array(
                'result' => 0,
                "message" => $e
            );
        }

        $arr[] = array(
            'response1' => $response1,
            'response2' => $response2,
        );
    
        return $arr;
    }

    function getTruck($dbgl,$data)
    {
        $responseData = [];

        try {
            
            $query = "SELECT nopol FROM t_truck WHERE truck_stat='Y' AND cabang IN (".$data->where.")";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }
    
        return $responseData;
    }

    function getSopir($dbgl,$data)
    {
        $responseData = [];

        try {
        
            $query = "SELECT sopir_utama as sopir FROM t_truck WHERE trim(cabang) IN (".$data->where.") GROUP BY sopir_utama";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }
    
        return $responseData;
    }

    function getGudang($dbgl,$data)
    {
        $responseData = [];

        try {

            $query = "SELECT TRIM(a.wh_code) AS wh_code, a.wh_office FROM in_warehouse a 
                        INNER JOIN in_warehouseuser b ON a.wh_code = b.wh_code AND b.userid = '".strtoupper($data->username)."'
                        WHERE a.wh_status = '1'";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }
    
        return $responseData;
    }

    function updateDistributionPlan($dbgl,$data)
    {
        $respon = [];
        $id_dp = $data->dt_so->id;

        $tgl_perintah = "";

        try {

            $sql = "SELECT tgl_perintah FROM t_distribution_plan_item
                    WHERE id = '$id_dp'";
        
            $rows = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);

            $tgl_perintah = $rows[0]['tgl_perintah'];

        } catch (PDOException $e) {
            $e->getMessage();
            $tgl_perintah = [
                "success" => 0,
                "message" => $e
            ];
        }

        try {

            $sql = "UPDATE t_distribution_plan_item SET
                        no_pol = '$data->nopol_truck',
                        sopir = '$data->sopir_name',
                        wh_code = '$data->gudang',
                        tgl_kirim = '$tgl_perintah',
                        catatan = '$data->alasan',
                        ritase = '$data->ritase',
                        status_so = '1'
                    WHERE id = '$id_dp'";
        
            $stmt = $dbgl->prepare($sql);
            $stmt->execute();

            $respon = [
                "success" => 1,
                "message" => "Data DP Berhasil Diupdate"
            ];

        } catch (PDOException $e) {
            $e->getMessage();
            $respon = [
                "success" => 0,
                "message" => $e
            ];
        }

        return $respon;
    }

    function updateDistributionPlanDetail($dbgl,$data)
    {
        $respon = [];
        $id_dp = $data->dt_so->id;

        try {

            $sql = "UPDATE t_distribution_plan_item SET
                        no_pol = '$data->nopol_truck',
                        sopir = '$data->sopir_name',
                        wh_code = '$data->gudang',
                        tgl_terima = '$data->tgl_terima',
                        tgl_kirim = '$data->tgl_kirim',
                        catatan = '$data->alasan',
                        no_to = '$data->no_to',
                        catatan_to = '$data->catatan_to',
                        ritase = '$data->ritase',
                        status_so = '1'
                    WHERE id = '$id_dp'";
        
            $stmt = $dbgl->prepare($sql);
            $stmt->execute();

            $respon = [
                "success" => 1,
                "message" => "Data DP Berhasil Diupdate"
            ];

        } catch (PDOException $e) {
            $e->getMessage();
            $respon = [
                "success" => 0,
                "message" => $e
            ];
        }

        return $respon;
    }

    function updateDistributionPlanAmbil($dbgl,$data)
    {
        $respon = [];
        $id_dp = $data->dt_so->id;

        $tgl_perintah = "";

        try {

            $sql = "SELECT tgl_perintah FROM t_distribution_plan_item
                    WHERE id = '$id_dp'";
        
            $rows = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);

            $tgl_perintah = $rows[0]['tgl_perintah'];

        } catch (PDOException $e) {
            $e->getMessage();
            $tgl_perintah = [
                "success" => 0,
                "message" => $e
            ];
        }

        try {

            $sql = "UPDATE t_distribution_plan_item SET
                        no_pol = '$data->nopol_truck',
                        sopir = '$data->sopir_name',
                        wh_code = '$data->gudang',
                        tgl_kirim = '$tgl_perintah',
                        status_so = '1'
                    WHERE id = '$id_dp'";
        
            $stmt = $dbgl->prepare($sql);
            $stmt->execute();

            $respon = [
                "success" => 1,
                "message" => "Data DP Berhasil Diupdate"
            ];

        } catch (PDOException $e) {
            $e->getMessage();
            $respon = [
                "success" => 0,
                "message" => $e
            ];
        }

        return $respon;
    }

    function getNoTransfer($dbgl,$data)
    {
        $prd = date("Ym");
        $whCode = trim($data->wh_code);
        $now = date('Y-m-d');
        $before = date('Y-m-d', strtotime("-7 days {$now}"));
        $responseData = [];

        // if (isGudangTransfer($dbgl,$data) > 0) {
        //     $query = "SELECT ref_no, item_code, item_qty FROM in_trans WHERE wh_code = '{$whCode}' AND record_id = 'F' AND in_code IN ('TI','RTR') AND (DATE(voucher_date) BETWEEN '{$before}' AND '{$now}')";
        //     try {
        //         $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
        //         $responseData = $rows;
        //     } catch(PDOException $e) {
        //         $responseData = $e;
        //     }
        // }

        $query = "SELECT ref_no, item_code, item_qty 
                  FROM in_trans 
                  WHERE wh_code = '{$whCode}' 
                  AND record_id = 'F' AND in_code IN ('TI','TO','RTR') 
                  AND (DATE(voucher_date) BETWEEN '{$before}' AND '{$now}')
                  ORDER BY ref_no";
        try {
            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;
        } catch(PDOException $e) {
            $responseData = $e;
        }
        
        return $responseData;
        // return $query;
    }

    function isGudangTransfer($dbgl,$data)
    {
        $whCode = trim($data->wh_code);
        $query = "SELECT COUNT(*) FROM in_warehouse WHERE wh_code = '{$whCode}' AND capacity = -2";
        $result = $dbgl->query($query)->fetchColumn();
        return $result;
    }

    function updateTransferDPKirim($dbgl,$data)
    {
        $respon = [];
        $id_dp = $data->dt_so->id;

        try {

            $sql = "UPDATE t_distribution_plan_item SET
                        no_pol = '$data->nopol_truck',
                        sopir = '$data->sopir_name',
                        wh_code = '$data->gudang',
                        no_to = '$data->no_to',
                        catatan = '$data->alasan',
                        tgl_kirim = '$data->tgl_kirim',
                        ritase = '$data->ritase',
                        status_so = '1'
                    WHERE id = '$id_dp'";
        
            $stmt = $dbgl->prepare($sql);
            $stmt->execute();

            $respon = [
                "success" => 1,
                "message" => "Data DP Berhasil Diupdate"
            ];

        } catch (PDOException $e) {
            $e->getMessage();
            $respon = [
                "success" => 0,
                "message" => $e
            ];
        }

        return $respon;
    }

    function updateProsesTransferDPKirim($dbgl,$data)
    {
        $respon = [];
        $id_dp = $data->dt_so->id;

        try {

            $sql = "UPDATE t_distribution_plan_item SET
                        catatan_to = '$data->catatan_to',
                        tgl_terima = '$data->tgl_terima'
                    WHERE id = '$id_dp'";
        
            $stmt = $dbgl->prepare($sql);
            $stmt->execute();

            $respon = [
                "success" => 1,
                "message" => "Data DP Berhasil Diupdate"
            ];

        } catch (PDOException $e) {
            $e->getMessage();
            $respon = [
                "success" => 0,
                "message" => $e
            ];
        }

        return $respon;
    }

    function getKategori($dbgl,$data)
    {
        $responseData = [];
        $respon = [];

        try {
        
            $query = "SELECT category FROM v_username_category WHERE username = '$data->username' ORDER BY category";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

            $respon = [
                "success" => 1,
                "message" => "Data Berhasil Ditampilkan",
                "data" => $responseData
            ];

        } catch (PDOException $e) {
            $respon = [
                "success" => 0,
                "message" => "Data Gagal Ditampilkan",
                "data" => $e
            ];
        }
    
        return $respon;
    }

    function getCabang($dbgl,$data)
    {
        $responseData = [];
        $respon = [];

        try {
        
            $query = "SELECT trim(coce_code) AS coce_code FROM coce_codeuser WHERE username = '$data->username' AND code_comp = '$data->category'
                        AND trim(coce_code) <> 'PLR' 
                        GROUP BY trim(coce_code) ORDER BY trim(coce_code)";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

            $respon = [
                "success" => 1,
                "message" => "Data Berhasil Ditampilkan",
                "data" => $responseData
            ];

        } catch (PDOException $e) {
            $respon = [
                "success" => 0,
                "message" => "Data Gagal Ditampilkan",
                "data" => $e
            ];
        }
    
        return $respon;
    }

    function getGudangOutstanding($dbgl,$data,$dbh)
    {
        $datax = [];
        $respon = [];
        $rows = [];

        if (isset($data->username)) {

            $username = $data->username;
            $modEntry = isset($data->entry) ? $data->entry : 0;
            $modFisik = isset($data->fisik) ? $data->fisik : 0;
            $region = isset($data->region) ? $data->region : 0;
            $coce_code = trim($data->coce_code);
            $menu_code = isset($data->menu_code) ? $data->menu_code : null;
            $acc_approved = get_approved_acc('SAA', $menu_code, $dbh);

            if ($acc_approved && $modFisik) $modFisik = 0;

            $where = " A.wh_status = '1' ";
            if ($modFisik) $where .= " AND capacity > 0 ";
            if ($region) $where .= " AND wh_loc = '$region' ";
            if ($menu_code=='invdispatch'||$menu_code=='invmttruck') $where .= " AND dispatch_status = '1' ";
            if ($coce_code != '') {
                if ($coce_code == 'ALL') {
                    $where .= " AND 1=1 ";
                } elseif ($coce_code == 'SMD') {
                    $where .= " AND (A.wh_coce = 'SMD' OR A.wh_coce = 'PLR') ";
                } elseif ($coce_code == 'BWI') {
                    // $where = " (A.wh_coce = 'BWI' OR A.wh_coce = 'DPS' OR A.wh_coce = 'KLK' OR A.wh_coce = 'NGR' OR A.wh_coce = 'SIN' OR A.wh_coce = 'TBN') ";
                    $where = " (A.wh_coce = 'BWI' OR A.wh_coce = 'GYR' OR A.wh_coce = 'KLK' OR A.wh_coce = 'NGR' OR A.wh_coce = 'SIN' OR A.wh_coce = 'TBN') ";
                } else {
                    $where .= " AND A.wh_coce = '$coce_code' ";
                }
            }

            try {
                $sql = "
                    SELECT trim(A.wh_code) AS wh_code FROM in_warehouse A INNER JOIN in_warehouseuser B ON trim(A.wh_code) = trim(B.wh_code)
                    WHERE ".$where." AND B.userid = upper('$username') ORDER BY trim(A.wh_code)
                ";
                                
                $stmt = $dbh->prepare($sql);
                $stmt->execute();
                $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $stmt->closeCursor();

                $datax[] = $rows;
                // if (!$modEntry) $datax[0][count($rows)+1]['wh_code'] = 'ALL';
                
                // if (in_array($coce_code, ['SMD','TGT'])) {
                //     $data['KTITR'] = ['KTITR'];
                // } elseif (in_array($coce_code, ['BMA','SBW'])) {
                //     $data['NTBTR'] = ['NTBTR'];
                // }

                // asort($data);

                $respon = $datax;
            
            } catch (PDOException $e) {
                $respon = $e;
            }

        }

        return $respon;
    }

    function getOutstanding($dbgl,$data,$dbh)
    {
        $row = [];
        $shift = isset($data->category) ? $data->category : ''; $desc = '';
        $coce_code = isset($data->cabang) ? $data->cabang : ''; $kode_cab = $coce_code;
        $wh_code = isset($data->gudang) ? $data->gudang : ''; $kode_wh = $wh_code;
        $ledger = isset($data->ledger) ? $data->ledger : NULL;
        $report = isset($data->report) ? $data->report : NULL;
        $date2 = isset($data->periode) ? $data->periode : date("Y-m-d");

        if ($kode_cab == 'ALL') {
            ini_set("memory_limit", "5120M");
        } else {
            ini_set("memory_limit", "2048M"); 
        } 

        if (empty($ledger)) {
            $ledger = 'ALL';
        }
        $kode_ledger = $ledger;
        $list_coce = list_coce($dbgl,$username);

        // userlogs
        // $datalogs = new stdClass();
        // $datalogs->dbh = $dbh;
        // $datalogs->menu_code = $menu_code;
        // $datalogs->statuslogs = strtoupper($shift." - ".$kode_cab." - ".$kode_wh." - ".$report." - ".$date2);
        // userlogs($datalogs);

        $dbgl->query("DROP TABLE IF EXISTS tmp_po_req_group");
        $dbgl->query("DROP TABLE IF EXISTS tmp_po_req,tmp_po_req2,tmp_po_req3");
        $dbgl->query("DROP TABLE IF EXISTS tmp_t_card");
        $dbgl->query("DROP TABLE IF EXISTS tmp_in_trans");
        $dbgl->query("DROP TABLE IF EXISTS tmp_po_req_sum");
        $dbgl->query("DROP TABLE IF EXISTS salesoutstanding");

        $whereTCard = " WHERE code_comp = upper('{$shift}')";
        if ($kode_cab == 'ALL') {
            $whereTCard .= " AND coce_code = ANY('{" . implode(',', $list_coce) . "}'::text[]) ";
        } else {
            if ($kode_cab == 'SMD') {
                $whereTCard .= " AND (coce_code = 'SMD' OR coce_code = 'PLR') ";
            } elseif ($kode_cab == 'GYR') {
                // if ($area_code == 'ALL') {
                    $whereTCard .= " AND (coce_code = 'DPS' OR coce_code = 'GYR') ";
                // } else {
                //     $whereTCard .= " AND (coce_code = 'DPS' OR coce_code = 'GYR') AND area_code = '$area_code' ";
                // }
            } else {
                $whereTCard .= " AND coce_code = '{$kode_cab}' ";
            }
        }

        $query = "SELECT code, name, coce_code, area_code, group_code INTO TEMPORARY tmp_t_card FROM t_card {$whereTCard}";
        $dbgl->query($query);
        $query = "
        SELECT 
            A.voucher_prd, A.voucher_doc, A.voucher_no, A.wh_code, TRIM(A.remark) as remark, A.ref_no, 
            UPPER(A.item_code) as item_code, A.item_qty, A.in_type, A.update_date, A.voucher_date, 
            A.car_no, A.driver, A.received_time 
            INTO TEMPORARY tmp_in_trans 
        FROM in_trans A
        INNER JOIN tmp_t_card B ON A.code = B.code
        WHERE A.code_comp = upper('{$shift}') AND A.voucher_date <= '{$date2}' AND A.in_stat != 'C'";
        $dbgl->query($query);

        //total qty item per SO
        $wherePoReq = " WHERE A.comp_code = 'SAKA00' AND A.code_comp = upper('{$shift}') AND A.pr_type = 'S' AND A.pr_date <= '{$date2}' AND A.pr_stat = 'A'";
        if ($kode_wh != 'ALL') {
            $wherePoReq .= " AND A.kp_code = '{$kode_wh}' ";
        }

        if ($ledger != '' && $ledger != 'ALL') {
            $wherePoReq .= " AND (vend_code = '{$ledger}' OR dept_code = '{$ledger}') ";
        }
        $query = "
            SELECT 
                A.pr_no, A.item_code, SUM(A.item_qty) AS item_qty 
                INTO TEMPORARY tmp_po_req
            FROM po_req A
            INNER JOIN tmp_t_card B ON A.vend_code = B.code
            {$wherePoReq}
            GROUP BY A.pr_no, A.item_code
        ";
        $dbgl->query($query);

        //total qty realisasi DO per SO
        $query = "
            SELECT
                A.remark, A.item_code,
                (SUM(CASE WHEN A.in_type = 'O' THEN A.item_qty ELSE 0 END) - SUM(CASE WHEN A.in_type = 'I' THEN A.item_qty ELSE 0 END)) AS do_sum
                INTO TEMPORARY tmp_in_trans_sum
            FROM tmp_in_trans A
            GROUP BY A.remark, A.item_code
        ";
        $dbgl->query($query);

        //SO masih Outstanding
        $query = "
            SELECT
                A.*, (A.item_qty - coalesce(B.do_sum, 0)) AS sisa, B.do_sum 
                INTO TEMPORARY tmp_so_outs
            FROM tmp_po_req A
            LEFT JOIN tmp_in_trans_sum B
                ON (A.pr_no = B.remark) AND (A.item_code = B.item_code)
        ";
        $dbgl->query($query);

        //detail realisasi DO
        $query = "
            SELECT
                A.voucher_prd, A.voucher_doc, A.voucher_no, A.wh_code, A.ref_no, A.item_code AS item_code_in, 
                A.item_qty AS do_qty, A.in_type, A.update_date,
                TO_CHAR(A.voucher_date, 'YYYY-MM-DD') AS do_date, A.car_no, A.driver, 
                TO_CHAR(A.received_time, 'YYYY-MM-DD HH24:MI:SS') AS received_time, B.pr_no AS remark
                INTO TEMPORARY tmp_realisasi_so_outs
            FROM tmp_in_trans A
            INNER JOIN tmp_so_outs B
                ON (A.remark = B.pr_no) AND (A.item_code = B.item_code)
            WHERE B.sisa <> 0
        ";
        $dbgl->query($query);

        $query = "
            SELECT 
                A.pr_no, (A.pr_date)::date AS pr_date, A.vend_code, A.dept_code, A.sales_code, A.pr_stat, 
                    A.kp_code, upper(A.pr_desc) AS pr_desc, A.price_amt, A.pending_stat, A.code_comp,
                    C.item_code, C.item_qty, C.do_sum
                    INTO TEMPORARY tmp_po_req2
                FROM po_req A INNER JOIN tmp_so_outs C 
                    ON A.code_comp = upper('{$shift}') AND A.pr_no = C.pr_no AND A.item_code = C.item_code
        ";
        $dbgl->query($query);

        $query = "
            SELECT A.*, B.name, B.coce_code INTO TEMPORARY tmp_po_req3
                FROM tmp_po_req2 A INNER JOIN t_card B 
                    ON A.vend_code = B.code AND A.code_comp = B.code_comp
        ";
        $dbgl->query($query);

        $query = "
            SELECT 
                A.*, D.* INTO TEMPORARY salesoutstanding
                FROM tmp_po_req3 A LEFT JOIN tmp_realisasi_so_outs D 
                    ON A.pr_no = D.remark AND A.item_code = D.item_code_in 
        ";
        $query .= " ORDER BY A.vend_code, A.pr_no, do_date";
        $dbgl->query($query);

        $sql = "SELECT * FROM salesoutstanding WHERE (item_qty - COALESCE(do_sum, 0)) <> 0 ";
        $stmt = $dbgl->query($sql);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ( $stmt->rowCount() ) {
            $stmt = $dbh->prepare("DELETE FROM data_json WHERE lower(menu_code)=lower(?) AND lower(username)=lower(?)");
            $stmt->execute([$menu_code,$username]);
            $stmt = $dbh->prepare("INSERT INTO data_json (menu_code,data,username) VALUES(?,?,?) RETURNING id");
            try {
                $stmt->execute([$menu_code,json_encode($rows),$username]);
                $stmt->bindColumn('id', $id);
                $stmt->fetch();
            } catch (PDOException $e) {
                // echo $e;
            }
        }

        $query = "SELECT code, name FROM v_autoledger ORDER BY code";
        $stmt = $dbh->prepare($query);
        $stmt->execute();
        $rowx = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $arrayName = array();
        foreach ($rowx as $row) {
            $arrayName[$row['code']] = $row['name'];
        }

        $no = 0;
        $sum_do_qty = 0;
        $sum_do_sum = 0;
        $sum_so_qty = 0;
        $sum_outs_do = 0;
        $temp_pr_no = '';

        foreach ($rows as $row) {
            if (($row["item_qty"]-$row["do_sum"]) == 0)
            {
                continue;
            }
            $txt_wh_code = $row["wh_code"];
            $voucher_prd = $row["voucher_prd"];
            $voucher_doc = $row["voucher_doc"];
            $voucher_no  = $row["voucher_no"];
            $pr_no       = $row["pr_no"];
            $item_code   = $row["item_code"];

            if (isset($temp_pr_no) && $temp_pr_no==$pr_no)
            {
                if ($report != 'detail')
                {                       
                    continue;
                }
                $vend_code  = "";
                $name       = "";
                $sales_code = "";
                $pr_date    = "";
                $pr_stat    = "";
                $kp_code    = "";
                $txt_pr_no  = "";
                if ($shift == "saa")
                {
                    $txt_item_code  = "";
                    $item_qty       = 0;
                } else {
                    $txt_item_code  = $row["item_code"];
                    $item_qty       = $row["item_qty"];
                    $outs_do       = 0;
                }
            } else {
                $no++;
                $temp_pr_no    = $pr_no;
                $vend_code     = $row["vend_code"];
                $dept_code     = trim($row["dept_code"]);
                $name          = $arrayName[$vend_code];
                if (strpos($name,'PROMOSI') !== false) {
                    if ($dept_code!='') { 
                        $dept_name = $arrayName[$dept_code];
                    } else {
                        $dept_name = '';
                    }
                    $name = $dept_name.' - PROMOSI'; 
                }
                $sales_code    = $row["sales_code"];
                $pr_date       = $row["pr_date"];
                $txt_pr_no     = $row["pr_no"];
                // $car_no        = $row["car_no"];
                $txt_item_code = $row["item_code"];
                $item_qty      = $row["item_qty"];
                $do_sum        = $row["do_sum"];
                $outs_do       = $row["item_qty"]-$row["do_sum"];
                $pr_stat       = $row['pr_stat'];
                $kp_code       = $row['kp_code'];
                $pending_stat  = $row['pending_stat'];
            }
            $car_no        = $row["car_no"];
            $txt_coce_code = $row["coce_code"];  // echo $txt_coce_code;
            $pr_desc   = $row["pr_desc"];
            $do_date   = $row["do_date"];
            $ref_no    = $row["ref_no"];
            $item_code_in = $row["item_code_in"];
            $do_qty    = $row["do_qty"];
            $price_amt = $row["price_amt"];         //20150812
            $action = 'invtranout';
            if ($row["in_type"] == "I")
            {
                $do_qty = -1 * $do_qty;
                $action = 'invtranin';
            }

            $array_footer[] = array(
                'item_code'   => $txt_item_code,
                'item_qty'    => $item_qty,
                'do_sum'      => $do_sum,
                'outs_do'     => $outs_do
            );

            if ($item_qty == 0) {
                $xitem_qty = "";
            } else {
                $xitem_qty = number_format($item_qty,2);
            }

            if ($do_qty == 0) {
                $xdo_qty = "";
            } else {
                $xdo_qty = number_format($do_qty,2);
            }

            $xdo_sum = number_format($do_sum,2);
            $xouts_do = number_format($outs_do,2);

            //SUSUN ULANG 1
            $hasil['outstanding'][] = array(
                "no" => $no,
                "kode_cab" => $kode_cab,
                "vend_code" => $vend_code,
                "name" => $name,
                "pr_date" => $pr_date,
                "txt_pr_no" => $txt_pr_no,
                "kp_code" => $kp_code,
                "txt_item_code" => $txt_item_code,
                "item_qty" => $xitem_qty,
                "report" => $report,
                "do_date" => $do_date,
                "ref_no" => $ref_no,
                "car_no" => $car_no,
                "do_qty" => $xdo_qty,
                "do_sum" => $xdo_sum,
                "outs_do" => $xouts_do,
                "pr_desc" => $pr_desc
            );

            $sum_do_qty += $do_qty;
            $sum_do_sum += $do_sum;
            $sum_so_qty += $item_qty;
            $sum_outs_do += $outs_do;
        }

        if (isset($array_footer)) {
            $sum_array_footer = array(); $i = 0;
            foreach ($array_footer as $data) {
                $i = $data['item_code'];
                if (!array_key_exists($i, $sum_array_footer)) {
                    $sum_array_footer[$i] = array(
                        'item_code' => $data['item_code'],
                        'item_qty'  => $data['item_qty'],
                        'do_sum'    => $data['do_sum'],
                        'outs_do'   => $data['outs_do']
                    );
                } else {
                    $sum_array_footer[$i]['item_qty'] = $sum_array_footer[$i]['item_qty'] + $data['item_qty'];
                    $sum_array_footer[$i]['do_sum'] = $sum_array_footer[$i]['do_sum'] + $data['do_sum'];
                    $sum_array_footer[$i]['outs_do'] = $sum_array_footer[$i]['outs_do'] + $data['outs_do'];
                }
                $i++;
            }
            sort($sum_array_footer);

            foreach ($sum_array_footer as $x=>$v)
            {
                if ($sum_array_footer[$x]['item_code']==''){continue;}
                //SUSUN ULANG TOTAL
                $hasil['total_outstanding'][] = array(
                    "item_code" => $sum_array_footer[$x]['item_code'],
                    "item_qty" => number_format($sum_array_footer[$x]['item_qty'],2),
                    "do_sum" => number_format($sum_array_footer[$x]['do_sum'],2),
                    "outs_do" => number_format($sum_array_footer[$x]['outs_do'],2)
                );
            }
        }

        $hasil['grand_total_outstanding'][] = array(
            "sum_so_qty" => number_format($sum_so_qty,2),
            "sum_do_qty" => number_format($sum_do_qty,2),
            "sum_do_sum" => number_format($sum_do_sum,2),
            "sum_outs_do" => number_format($sum_outs_do,2)

        );
        
        return $hasil;
    }

    function getForceClose2($dbgl,$data,$dbh)
    {

        $shift = $data->shift;
        $status = $data->status;
        $cari = strtoupper(trim($data->cari));
        $date1 = $data->date1;
        $date2 = $data->date2;

            // userlogs
            $datalogs = new stdClass();
            $datalogs->dbh = $dbh;
            $datalogs->menu_code = $menu_code;
            $datalogs->statuslogs = strtoupper($shift." - ".$status." - ".$cari." - ".$date1." - ".$date2);
            userlogs($datalogs);

        if ($shift == 'saa')
        {
            $desc = "Semen";
            $company = "PT. Saka Agung Abadi";
        } else {
            $desc = "Non Semen";
            $company = "PT. Saka NiagaSukses Abadi";
        }

        $sql = "SELECT A. menuprg, A.user_id, A.event_code, A.event_desc, A.reason,
                TO_CHAR(A.event_date, 'YYYY-MM-DD HH24:MI:SS') AS event_date, C.name
                FROM s_audit_gab A
                LEFT JOIN t_card C
                    ON RIGHT(A.event_desc, 10) = C.code AND A.code_comp = C.code_comp
                WHERE A.code_comp = upper('$shift') AND (A.event_date::date BETWEEN '$date1' AND '$date2') 
                    AND (A.menuprg = 'SO111' OR A.menuprg = 'SO141') ";
                if ($status != 'ALL') {
                    $sql .= " AND A.event_code = '$status' ";
                }
                if ($cari != '') {
                    $sql .= " AND (A.reason LIKE '%$cari%' OR A.event_desc LIKE '%$cari%' OR A.user_id LIKE '%$cari%') ";
                    // $sql .= " AND ('$cari' LIKE '%' || A.event_desc || '%' OR '$cari' LIKE '%' || A.user_id || '%') ";
                    // $sql .= " AND ('$cari' LIKE '%' || A.event_desc || '%' OR A.user_id LIKE '%$cari%') ";
                }
        $sql .= " ORDER BY A.event_date, A.menuprg, A.event_desc, A.event_code ";

        $stmt = $dbh->prepare($sql); $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        $dataJson = array();

        if (isset($rows))
        {
            $no = 1;
            foreach ($rows as $row)
            {
                $menuprg    = TRIM($row['menuprg']);
                if($menuprg=="SO111"){
                    $menuprgText = "Sales Order";
                }else if($menuprg=="SO141"){
                    $menuprgText = "Sales Invoice";
                }
                $user_id    = $row['user_id'];
                $event_code = $row['event_code'];
                $event_date = $row['event_date'];
                $event_desc = $row['event_desc'];
                // if ($cari != '') {
                //     if (strpos($event_desc,$cari) === false) { 
                //         continue; 
                //     }
                // }
                $reason     = trim($row['reason']);

                $dataJson[] = array(
                    'no' => $no++,
                    'event_date' => $event_date,
                    'menuprg' => $menuprgText,
                    'user_id' => $user_id,
                    'event_code' => $event_code,
                    'event_desc' => $event_desc,
                    'reason' => $reason
                );
            }
        }

        return $dataJson;
    }

    function getForceClose($dbgl,$data,$dbh)
    {
        $shift = isset($data->shift) ? $data->shift : NULL;
        $coce_code = isset($data->coce_code) ? $data->coce_code : NULL; $kode_cab = $coce_code;
        $sales_code = isset($data->sales_code) ? $data->sales_code : NULL;
        $wh_code = isset($data->wh_code) ? $data->wh_code : NULL; $kode_wh = $wh_code;
        $tanggal1 = isset($data->date1) ? $data->date1 : date("Y-m-d",strtotime("first day of this month")); $date1 = $tanggal1;
        $tanggal2 = isset($data->date2) ? $data->date2 : date("Y-m-d"); $date2 = $tanggal2; $today = date("Y-m-d");
        $toDate = date("Y-m-d", strtotime("last day of next month", strtotime($date2)));
        $ledger = isset($data->ledger) ? $data->ledger : NULL; if (isset($ledger)&&!empty($ledger)) $accUpdate = true;
        $report = isset($data->report) ? $data->report : NULL;
        $status = "W";

        if ($coce_code != 'ALL') {
            $and_cabang = "AND A.spk_no = '$coce_code'";
        } else {
            $and_cabang = "";
        }

        if ($sales_code != 'ALL') {
            $and_sales_code = "AND trim(a.sales_code) = '$sales_code'";
        } else {
            $and_sales_code = "";
        }

        if ($ledger != null) {
            $and_ledger = "AND A.vend_code = '$ledger'";
        } else {
            $and_ledger = "";
        }
        

        $sql = "SELECT 
                    A.spk_no, 
                    A.vend_code,  
                    (SELECT b.name FROM in_trans b WHERE b.remark = A.pr_no AND b.in_type = 'O' AND b.item_code = A.item_code) AS name,
                    A.pr_no, 
                    A.item_code, 
                    A.item_qty,
                    (SELECT b.ref_no FROM in_trans b WHERE b.remark = A.pr_no AND b.in_type = 'O' AND b.item_code = A.item_code) AS ref_no_out,
                    ((SELECT SUM(b.item_qty) FROM in_trans b WHERE b.remark = A.pr_no AND b.in_type = 'O' AND b.item_code = A.item_code) - (SELECT SUM(b.item_qty) FROM in_trans b WHERE b.remark = A.pr_no AND b.in_type = 'I' AND b.item_code = A.item_code)) AS sum_out,
                    (SELECT SUM(b.item_qty) FROM in_trans b WHERE b.remark = A.pr_no AND b.in_type = 'I' AND b.item_code = A.item_code) AS sum_in,
                    A.pr_desc,
                    A.code_comp,
                    A.entry_by,
                    A.pr_date
                FROM po_req A
                INNER JOIN t_card B ON A.vend_code = B.code
                WHERE A.comp_code = 'SAKA00' 
                AND A.pr_type = 'S' 
                AND A.item_qty2 > 0
                AND A.code_comp = UPPER('$shift')
                AND A.pr_date <= '$date2'
                AND 1=1 
                AND A.pr_stat = 'A' 
                AND trim(A.pending_stat) = 'F'
                $and_cabang
                $and_sales_code
                $and_ledger
                GROUP BY A.spk_no, A.vend_code, A.pr_no, A.item_code, A.item_qty, A.pr_desc,A.entry_by,A.code_comp,A.pr_date";

        try {

            $rows = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }

        for($i=0;$i<count($responseData);$i++) {

            $pr_no = $responseData[$i]['pr_no'];
            $item_code = $responseData[$i]['item_code'];
            $listPrNo = implode(',', array_map('array_pop', $pr_no));

            try {
                $sql = "SELECT b.ref_no, b.voucher_desc, b.car_no, b.driver 
                        FROM in_trans b 
                        WHERE b.remark = '$pr_no'
                        AND b.in_type = 'I' 
                        AND b.item_code = '$item_code'";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            try {
                $sql2 = "SELECT trim(event_desc) AS pr_no,trim(user_id) AS user_id,reason
                            FROM s_audit_gab 
                            WHERE menuprg = 'SO111' 
                            AND event_code = 'REQUEST CLOSED' 
                            AND code_comp = upper('$shift') 
                            AND trim(event_desc) = '$pr_no' 
                            ORDER BY event_date DESC";
                $rowx = $dbh->query($sql2)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $th) {
                $th->getMessage();
                $rowx = $th;
            }

            $hasil[] = array(
                "spk_no" => $responseData[$i]['spk_no'],
                "vend_code" => $responseData[$i]['vend_code'],
                "name" => $responseData[$i]['name'],
                "pr_no" => $responseData[$i]['pr_no'],
                "item_code" => $responseData[$i]['item_code'],
                "item_qty" => $responseData[$i]['item_qty'],
                "ref_no_out" => $responseData[$i]['ref_no_out'],
                "sum_out" => $responseData[$i]['sum_out'],
                "sum_in" => $responseData[$i]['sum_in'],
                "pr_desc" => $responseData[$i]['pr_desc'],
                "pr_date" => $responseData[$i]['pr_date'],
                "entry_by" => $responseData[$i]['entry_by'], 
                "code_comp" => $responseData[$i]['code_comp'],
                "item" => $arr,
                "reason" => $rowx
            );
            $arr = [];

        }

        return $hasil;
        
    }

    function getSales($dbgl,$data,$dbh)
    {
        $responseData = [];
        $respon = [];

        $kode_cab = $data->coce_code;
        $usergroup = $data->usergroup;
        $username = $data->username;

        if ($usergroup == 'SR') {
            $sql = "SELECT sales_code FROM sales_codeuser WHERE username = '".$username."' ORDER BY sales_code";            
        } else {
            if ($kode_cab == 'ALL') {
                $sql = "SELECT trim(nik) AS sales_code FROM t_person WHERE active_flag = 'Y' AND record_id = 'S' ORDER BY nik";
            } else {
                $sql = "SELECT trim(nik) AS sales_code FROM t_person WHERE active_flag = 'Y' AND record_id = 'S' AND position = '$kode_cab' ORDER BY nik";
            }
        }

        try {

            $rows = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

            $respon = [
                "success" => 1,
                "message" => "Data Berhasil Ditampilkan",
                "data" => $responseData
            ];

        } catch (PDOException $e) {
            $respon = [
                "success" => 0,
                "message" => "Data Gagal Ditampilkan",
                "data" => $e
            ];
        }
    
        return $respon;
    }

    function sendEmail($dbgl,$data,$dbh)
    {
        $dataEmail              = headingTemplate("PENGAJUAN FORCE CLOSE");
        $dataEmail              .= bodyTemplateEmail($data);
        $dataEmail              .= footerTemplateEmail();

        $a                     = new Mail();
        $email                 = (object) array();
        $email->senderEmail    = 'rachmadi@ho.pt-saa.com';
        $email->senderUsername = 'rachmadi';
        $email->to             = 'ady.appkey@gmail.com';
        $email->cc             = ['brandalsanggulan@gmail.com'];
        // $email->attachment     = '';
        $token                 = $a->getToken();
        $email->token          = $token;
        $email->subject        = "Test Email Rachmadi";
        $email->modul          = "Modul Email Ady";
        $email->body           = $dataEmail;
        $email->tokenExpired   = 7;

        $a->sendMail($email);

        return "Email Send";
        // return $data;
    }

    function send_mail_approve($dbgl,$data,$dbh)
    {
        $dataEmail              = headingTemplate("PENGAJUAN SO DIBAWAH 8 TON");
        $dataEmail              .= bodyTemplateEmailApprove($data);
        $dataEmail              .= footerTemplateEmail();

        $a                     = new Mail();
        $email                 = (object) array();
        $email->senderEmail    = 'rachmadi@ho.pt-saa.com';
        $email->senderUsername = 'rachmadi';
        $email->to             = 'ady.appkey@gmail.com';
        $email->cc             = ['brandalsanggulan@gmail.com'];
        // $email->attachment     = '';
        $token                 = $a->getToken();
        $email->token          = $token;
        $email->subject        = "Test Email Rachmadi";
        $email->modul          = "Modul Email Ady";
        $email->body           = $dataEmail;
        $email->tokenExpired   = 7;

        $a->sendMail($email);

        return "Email Berhasil Dikirim";
    }

    function headingTemplate($titleNotification)
	{
		$htmlHeader = '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
        <head>
            <!--[if gte mso 9]>
            <xml>
                <o:OfficeDocumentSettings>
                <o:AllowPNG/>
                <o:PixelsPerInch>96</o:PixelsPerInch>
                </o:OfficeDocumentSettings>
            </xml>
            <![endif]-->
            <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
            <meta name="format-detection" content="date=no" />
            <meta name="format-detection" content="address=no" />
            <meta name="format-detection" content="telephone=no" />
            <meta name="x-apple-disable-message-reformatting" />
            <!--[if !mso]><!-->
            <link href="https://fonts.googleapis.com/css?family=Kreon:400,700|Playfair+Display:400,400i,700,700i|Raleway:400,400i,700,700i|Roboto:400,400i,700,700i" rel="stylesheet" />
            <!--<![endif]-->
            <title>Email Template</title>
            <!--[if gte mso 9]>
            <style type="text/css" media="all">
                sup { font-size: 100% !important; }
            </style>
            <![endif]-->


            <style type="text/css" media="screen">
                /* Linked Styles */
                body { padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#1e52bd; -webkit-text-size-adjust:none }
                a { color:#000001; text-decoration:none }
                p { padding:0 !important; margin:0 !important }
                img { -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }
                .mcnPreviewText { display: none !important; }
                .text-footer2 a { color: #ffffff; }

                /* Mobile styles */
                @media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
                    .mobile-shell { width: 100% !important; min-width: 100% !important; }

                    .m-center { text-align: center !important; }
                    .m-left { text-align: left !important; margin-right: auto !important; }

                    .center { margin: 0 auto !important; }
                    .content2 { padding: 8px 15px 12px !important; }
                    .t-left { float: left !important; margin-right: 30px !important; }
                    .t-left-2  { float: left !important; }

                    .td { width: 100% !important; min-width: 100% !important; }

                    .content { padding: 30px 15px !important; }
                    .section { padding: 30px 15px 0px !important; }

                    .m-br-15 { height: 15px !important; }
                    .mpb5 { padding-bottom: 5px !important; }
                    .mpb15 { padding-bottom: 15px !important; }
                    .mpb20 { padding-bottom: 20px !important; }
                    .mpb30 { padding-bottom: 30px !important; }
                    .mp30 { padding-bottom: 30px !important; }
                    .m-padder { padding: 0px 15px !important; }
                    .m-padder2 { padding-left: 15px !important; padding-right: 15px !important; }
                    .p70 { padding: 30px 0px !important; }
                    .pt70 { padding-top: 30px !important; }
                    .p0-15 { padding: 0px 15px !important; }
                    .p30-15 { padding: 30px 15px !important; }
                    .p30-15-0 { padding: 30px 15px 0px 15px !important; }
                    .p0-15-30 { padding: 0px 15px 30px 15px !important; }
                    .text-footer { text-align: center !important; }
                    .m-td,
                    .m-hide { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }
                    .m-block { display: block !important; }
                    .fluid-img img { width: 100% !important; max-width: 100% !important; height: auto !important; }
                    .column,
                    .column-dir,
                    .column-top,
                    .column-empty,
                    .column-top-30,
                    .column-top-60,
                    .column-empty2,
                    .column-bottom { float: left !important; width: 100% !important; display: block !important; }
                    .column-empty { padding-bottom: 15px !important; }
                    .column-empty2 { padding-bottom: 30px !important; }

                    .content-spacing { width: 15px !important; }
                    .text-button-info :hover{
                        background: #8ec9e5;
                        box-shadow: #ffffff;
                        transition: all 1s ease-in-out 0s;
                        -moz-transition: all 1s ease-in-out 0s;
                        -webkit-transition: all 1s ease-in-out 0s;
                        -o-transition: all 1s ease-in-out 0s;
                    }

                }
            </style>
        </head>
        <body class="body"style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#1e52bd; -webkit-text-size-adjust:none;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#1e52bd">
                <tr>
                    <td align="center" valign="top">
                        <!-- Main -->
                        <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
                            <tr>
                                <td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
                                    <!-- Header -->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="p30-15" style="padding: 40px 0px 20px 0px;">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <th class="column-top" width="200"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="text-top m-center mpb5"style="color:#9babdb; font-family:\'Times New Roman\', Georgia, serif; font-size:11px; line-height:22px; text-align:left; text-transform:uppercase;"><multiline></multiline></td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                        <th class="column-top"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td align="right">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <!-- END Top bar -->
                                        <!-- Logo saka agung abadi -->


                                        <tr>
                                            <td>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#1e52bd">
                                                    <tr>
                                                        <td class="p0-15" style="padding: 0px 20px;border-bottom:solid 2px #ffffff">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <!--header section  -->
                                                                    <td class="pb40"style="padding-bottom:20px;">
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <th class="column-top" width="60"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td class="day"style="color:#1e52bd; font-family:\'Raleway\', Arial,sans-serif; font-size:40px; line-height:44px; text-align:left; font-weight:bold;">
                                                                                                <multiline>
                                                                                                    <img style="margin-bottom: -30px;" src="https://drive.google.com/uc?id=1UWpvU7a8P3tPWy03Nh_UNApbo1DDEUw-&export=download" width="90" height="70" editable="true" border="0" alt="logo saka" />
                                                                                                </multiline>
                                                                                            </td>

                                                                                        </tr>
                                                                                    </table>
                                                                                </th>
                                                                                <th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
                                                                                <th class="column-top"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:bold; vertical-align:top;color:#ffffff">
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                        <tr style="padding-bottom: 0px;">
                                                                                            <td class="h5-black black"style="font-family:\'Raleway\', Arial,sans-serif; font-size:18px; line-height:18px; text-align:left; padding-bottom:0px;padding-top:15px; font-weight:bold; color:#ffffff;">
                                                                                                <multiline>SAKA Integrated Information System </multiline>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="h5-black black"style="font-family:\'Raleway\', Arial,sans-serif; font-size:12px; line-height:22px; text-align:left;font-weight:bold; color:#ffffff !important; text-decoration: none !important"><multiline>`https://siis.pt-saa.com`</multiline></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </th>
                                                                                <th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
                                                                                <th class="column-top" width="156"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
                                                                                    <td class="event-separator" style="padding-bottom: 2px;border-bottom:solid 1px #ffffff; "></td>
                                                                                </th>
                                                                            </tr>
                                                                        </table>
                                                                    </td>

                                                                    <!--end of header section  -->
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <!-- logo dan header table -->
                                        <!-- Nav -->
                                        <!-- END Nav -->
                                    </table>
                                    <!-- END Header -->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ebebeb">
                                        <tr>
                                            <td class="p30-15-0" style="padding: 10px 10px 0px;" bgcolor="#1e52bd">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="center">
                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="text-button-orange"style="background:#e85711; color:#ffffff; font-family:\'Raleway\', Arial,sans-serif; font-size:14px; font-weight:bold; line-height:18px; text-align:center; padding:10px 30px;  border-radius:0px;">
                                                                        <a href="#" target="_blank" class="link-white"style="color:#ffffff; text-decoration:none;">
                                                                            <span class="link-white"style="color:#ffffff; text-decoration:none; ">
                                                                                ' . $titleNotification . '
                                                                            </span>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>

                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ebebeb">
                                        <tr>
                                            <td class="p30-15-0" style="padding: 20px 20px 0px;" bgcolor="#1e52bd">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="center">
                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!--end of header template-->
                                    <!--content table is here  --> ';
		return $htmlHeader;
	}

    function bodyTemplateEmail($data) {
        $bodyTemplate = '
            <div style="overflow: auto;min-height: 200px;">
                <table width="100%" border="1" cellspacing="0" cellpadding="0">
                    <tr style="border: solid 1px #ffffff;">
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">CABANG</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">LEDGER</h4>
                        </td>
                        <td style="width:5%; padding: 2px !important; color:#ffffff; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">NAME</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">NO. SO</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">ITEM CODE</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">QTY SO</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">REF NO OUT</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">QTY DO</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">OUT</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">DESCRIPTION</h4>
                        </td>
                    </tr>
                    <tr style="border: solid 1px #ffffff;">
                        <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">'.$data->data_all->spk_no.'</h4>
                        </td>
                        <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">'.$data->data_all->vend_code.'</h4>
                        </td>
                        <td style="width:5%; height:10%; padding: 2px !important; color:#ffffff; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">'.$data->data_all->name.'</h4>
                        </td>
                        <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">'.$data->data_all->pr_no.'</h4>
                        </td>
                        <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">'.$data->data_all->item_code.'</h4>
                        </td>
                        <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">'.$data->data_all->item_qty.'</h4>
                        </td>
                        <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;"></h4>
                        </td>
                        <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">'.$data->data_all->sum_out.'</h4>
                        </td>
                        <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">'.$data->data_all->sum_in.'</h4>
                        </td>
                        <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;"></h4>
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top: 80px;">
                    <tr>
                        <td bgcolor="#e63d13f5" class="text-button-info" style="width: 30%; background-image: linear-gradient(-225deg, #f08b06 0%, #ee7715 48%, #ee6a12 100%);background: linear-gradient(-225deg, #e63d13f5 0%, #e2390f 48%, #e98708 100%); border-color:solid 1px #ffffff; color:#ffffff; font-family:\'Raleway\', Arial,sans-serif; font-size:16px; font-weight:bold; line-height:18px; text-align:center; padding:10px 20px; border-radius:20px;border:solid 2px #ffffff">
                            <a href="#"  class="link-white" style="color:#ffffff; text-decoration:none;">
                                <span class="link-white"style="color:#ffffff; text-decoration:none;">
                                    MENYETUJUI
                                </span>
                            </a>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td bgcolor="#e63d13f5" class="text-button-info" style="width: 30%; background-image: linear-gradient(-225deg, #f08b06 0%, #ee7715 48%, #ee6a12 100%);background: linear-gradient(-225deg, #e63d13f5 0%, #e2390f 48%, #e98708 100%); border-color:solid 1px #ffffff; color:#ffffff; font-family:\'Raleway\', Arial,sans-serif; font-size:16px; font-weight:bold; line-height:18px; text-align:center; padding:10px 20px; border-radius:20px;border:solid 2px #ffffff">
                            <a href="#"  class="link-white" style="color:#ffffff; text-decoration:none;">
                                <span class="link-white"style="color:#ffffff; text-decoration:none;">
                                    MENOLAK
                                </span>
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
        ';
        return $bodyTemplate;
    }

    function bodyTemplateEmailApprove($data)
    {
        for ($i=0; $i < count($data->dt_so); $i++) { 
            $dt_row[] = 
            '
                <tr style="border: solid 1px #ffffff;">
                    <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                        <h4 style="color: #ffffff;margin: 8px;">'.$data->dt_so[$i]->code_comp.'</h4>
                    </td>
                    <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                        <h4 style="color: #ffffff;margin: 8px;">'.$data->dt_so[$i]->code.'</h4>
                    </td>
                    <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                        <h4 style="color: #ffffff;margin: 8px;">'.$data->dt_so[$i]->ledgername.'</h4>
                    </td>
                    <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                        <h4 style="color: #ffffff;margin: 8px;">'.$data->dt_so[$i]->tanggal.' '.$data->dt_so[$i]->jaminput.'</h4>
                    </td>
                    <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                        <h4 style="color: #ffffff;margin: 8px;">'.$data->dt_so[$i]->delivery_time.'</h4>
                    </td>
                    <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                        <h4 style="color: #ffffff;margin: 8px;">'.$data->dt_so[$i]->sm_no.'</h4>
                    </td>
                    <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                        <h4 style="color: #ffffff;margin: 8px;">'.$data->dt_so[$i]->so_no.'</h4>
                    </td>
                    <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                        <h4 style="color: #ffffff;margin: 8px;">'.$data->dt_so[$i]->item_code.'</h4>
                    </td>
                    <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                        <h4 style="color: #ffffff;margin: 8px;">'.$data->dt_so[$i]->alamat_toko.'</h4>
                    </td>
                    <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                        <h4 style="color: #ffffff;margin: 8px;">'.$data->dt_so[$i]->keterangan.'</h4>
                    </td>
                    <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                        <h4 style="color: #ffffff;margin: 8px;">'.$data->dt_so[$i]->sales_code.'</h4>
                    </td>
                </tr>
            ';
        }

        $bodyTemplate = 
        '
            <div style="overflow: auto;min-height: 200px;">
                <table width="100%" border="1" cellspacing="0" cellpadding="0">
                    <tr style="border: solid 1px #ffffff;">
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">TYPE</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">LEDGER</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">CUSTOMER</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">TGL. SO / SM</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">TGL. KIRIM</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">NO. SM</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">NO. SO</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">ITEM & QTY</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">ALAMAT</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">KET.</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">SALES CODE</h4>
                        </td>
                    </tr>
                    '.$dt_row.'
                </table>
            </div>
        ';

        return $bodyTemplate;
    }

    function footerTemplateEmail()
	{
		$footerTempate = '
                                <!--end of content table is here  -->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="text-footer2 p30-15" style="padding: 30px 15px 50px 15px; color:#ffffff; font-family:\'Raleway\', Arial,sans-serif; font-size:12px; line-height:14px; text-align:left;">
                                                <multiline>
                                                    <h4 style="color: #ffffff;">PT SAKA AGUNG ABADI - KERAHASIAAN DATA</h4>
                                                </multiline>
                                                <multiline>Data dan informasi yang disajikan melalui
                                                SIIS (SAKA Integrated Information System) adalah
                                                milik perusahaan dan setiap karyawan wajib menjaga
                                                kerahasiaan data transaksi maupun data-data
                                                lainnya. Melakukan pelanggaran dalam menjaga
                                                kerahasiaan data perusahaan akan dikenakan sanksi
                                                yang berlaku di perusahaan.</multiline>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- END Footer -->
                                </td>
                            </tr>
                        </table>
                        <!-- END Main -->
                    </td>
                </tr>
            </table>
        </body>
        </html>
        ';
		return $footerTempate;
	}

    function getSupirByNopol($dbgl,$data,$dbh)
    {

        $sql = "SELECT sopir FROM t_truck WHERE nopol='{$data->no_pol}'";

        try {

            $rows = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

            $respon = [
                "success" => 1,
                "message" => "Data Berhasil Ditampilkan",
                "data" => $responseData
            ];

        } catch (PDOException $e) {
            $respon = [
                "success" => 0,
                "message" => "Data Gagal Ditampilkan",
                "data" => $e
            ];
        }
    
        return $respon;
    }

    function getRitaseByNopol($dbgl,$data,$dbh)
    {
        $sql = "SELECT standar_rit FROM t_truck WHERE nopol='{$data->no_pol}'";

        try {

            $rows = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

            $respon = [
                "success" => 1,
                "message" => "Data Berhasil Ditampilkan",
                "data" => $responseData
            ];

        } catch (PDOException $e) {
            $respon = [
                "success" => 0,
                "message" => "Data Gagal Ditampilkan",
                "data" => $e
            ];
        }
    
        return $respon;
    }

    function updateStatusSO($dbgl,$data,$dbh)
    {
        try {

            $sql = "UPDATE t_distribution_plan_item SET status_so = '3'
                    WHERE id = '$data->dpId'";
        
            $stmt = $dbgl->prepare($sql);
            $stmt->execute();

            $respon = [
                "success" => 1,
                "message" => "Data DP Berhasil Diupdate"
            ];

        } catch (PDOException $e) {
            $e->getMessage();
            $respon = [
                "success" => 0,
                "message" => $e
            ];
        }


        try {

            $sql = "UPDATE t_distribution_plan_list SET status = '1'
                    WHERE dp_id = '$data->dpId'";
        
            $stmt = $dbgl->prepare($sql);
            $stmt->execute();

            $respon = [
                "success" => 1,
                "message" => "Data DP Berhasil Diupdate"
            ];

        } catch (PDOException $e) {
            $e->getMessage();
            $respon = [
                "success" => 0,
                "message" => $e
            ];
        }

        return $respon;
    }

    function getDataSORektur($dbgl,$data)
    {
        $date1 = date("Y-m-d", strtotime("yesterday"));
        $date2 = date("Y-m-d");
        $coce_code = 'NGR';
        $username = $data->username;

        if ($coce_code == 'SMD') {
            $tmp_qry = " AND A.wh_code IN (
                SELECT A.wh_code FROM IN_WAREHOUSE A INNER JOIN IN_WAREHOUSEUSER B ON A.wh_code = B.wh_code
                    WHERE (A.wh_coce = 'SMD' OR A.wh_coce = 'PLR') AND B.userid = upper('".$username."') 
                ) ";
        } else {
            $tmp_qry = " AND A.wh_code IN (
                SELECT A.wh_code FROM IN_WAREHOUSE A INNER JOIN IN_WAREHOUSEUSER B ON A.wh_code = B.wh_code
                    WHERE A.wh_coce = '".$coce_code."' AND B.userid = upper('".$username."') 
                ) ";
        }

        try {
            $query = "SELECT 
                            A.item_code, TO_CHAR(A.voucher_date, 'YYYY-MM-DD') AS voucher_date, A.remark,
                            A.voucher_doc, A.voucher_no, A.voucher_desc, regexp_replace(A.car_no, ' ', '', 'g') AS car_no, 
                            A.in_code, A.name, A.in_type, A.item_qty, trim(A.wh_code) AS wh_code, A.update_date, A.dept_code, A.price_amt,
                            A.ref_no, A.angk_code, TO_CHAR(A.angk_date, 'YYYY-MM-DD') AS angk_date, A.in_stat,
                            A.expd_code, A.pr_no, C.item_name, D.wh_coce, A.voucher_prd, D.wh_loc
                        FROM IN_TRANS A
                        INNER JOIN t_item C ON A.item_code = C.item_code AND A.code_comp = C.code_comp
                        INNER JOIN IN_WAREHOUSE D ON A.wh_code = D.wh_code
                        WHERE A.code_comp = upper('saa') AND in_type = 'I' AND (A.voucher_date BETWEEN '$date1' AND '$date2')
                        AND in_code = 'RTR'
                        $tmp_qry
                        UNION ALL
                        SELECT 
                            A.item_code, TO_CHAR(A.voucher_date, 'YYYY-MM-DD') AS voucher_date, A.remark,
                            A.voucher_doc, A.voucher_no, A.voucher_desc, regexp_replace(A.car_no, ' ', '', 'g') AS car_no, 
                            A.in_code, A.name, A.in_type, A.item_qty, trim(A.wh_code) AS wh_code, A.update_date, A.dept_code, A.price_amt,
                            A.ref_no, A.angk_code, TO_CHAR(A.angk_date, 'YYYY-MM-DD') AS angk_date, A.in_stat,
                            A.expd_code, A.pr_no, C.item_name, D.wh_coce, A.voucher_prd, D.wh_loc
                        FROM IN_TRANS A
                        INNER JOIN t_item C ON A.item_code = C.item_code AND A.code_comp = C.code_comp
                        INNER JOIN IN_WAREHOUSE D ON A.wh_code = D.wh_code
                        WHERE A.code_comp = upper('sna') AND in_type = 'I' AND (A.voucher_date BETWEEN '$date1' AND '$date2')
                        AND in_code = 'RTR'
                        $tmp_qry";
            
            // echo $query;

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $hasil[] = $rows;

        } catch (PDOException $e) {
            $e->getMessage();
            $hasil = $e; 
        }

        return $hasil;
    }

    function saveDistributionPlanTemp2($dbgl,$data,$dbh)
    {

        // for ($i=0; $i < count($data->dt_so); $i++) {

        //     $code_comp = $data->dt_so[$i]->code_comp;
        //     $type = $data->dt_so[$i]->type;
        //     $cabang = $data->dt_so[$i]->cabang;
        //     $tanggal = $data->dt_so[$i]->tanggal;
        //     $code = $data->dt_so[$i]->code;
        //     $name = $data->dt_so[$i]->ledgername;
        //     if($data->dt_so[$i]->sm_no != "") { $no = $data->dt_so[$i]->sm_no; } else { $no = $data->dt_so[$i]->so_no; }
        //     $so_no = $data->dt_so[$i]->so_no;
        //     $item_code = $data->dt_so[$i]->item_code;
        //     $item_qty = $data->dt_so[$i]->item_qty;
        //     $item_qty2 = $data->dt_so[$i]->item_qty2;
        //     $keterangan = $data->dt_so[$i]->keterangan;
        //     $item_ton = $data->dt_so[$i]->item_ton;
        //     $total_do = $data->dt_so[$i]->total_do;
        //     $freight_type = $data->dt_so[$i]->freight_type;
        //     $sales_code = $data->dt_so[$i]->sales_code;
        //     $kp_office = $data->dt_so[$i]->kp_office;
        //     $ledgername = $data->dt_so[$i]->ledgername;
        //     $pr_stat = $data->dt_so[$i]->pr_stat;
        //     $delivery_time = $data->dt_so[$i]->delivery_time;
        //     $jaminput = $data->dt_so[$i]->jaminput;
        //     $code_comp = $data->dt_so[$i]->code_comp;
        //     $alamat_toko = $data->dt_so[$i]->alamat_toko;
        //     $alamat_promosi = $data->dt_so[$i]->alamat_promosi;
        //     $now            = date('Y-m-d H:i:s');

        //     try {
        //         $query = "INSERT INTO t_distribution_plan_temp2(code_comp,type,cabang,tanggal,code,name,no,so_no,item_code,
        //                                                         item_qty,item_qty2,keterangan,item_ton,total_do,freight_type,
        //                                                         sales_code,kp_office,ledgername,pr_stat,delivery_time,jaminput,sc_status,asal_gudang,alamat_toko,alamat_promosi,insert_date) 
        //                     VALUES ('$code_comp','$type','$cabang','$tanggal','$code','$name','$no','$so_no','$item_code',
        //                                                         '$item_qty','$item_qty2','$keterangan','$item_ton','$total_do','$freight_type',
        //                                                         '$sales_code','$kp_office','$ledgername','$pr_stat','$delivery_time','$jaminput','0','$data->office','$alamat_toko','$alamat_promosi','$now')";

        //         $stmt = $dbgl->prepare($query);
        //         $stmt->execute();
        //         $response = [
        //             'success' => 1,
        //             'response' => 'Data Berhasil Disimpan'
        //         ];
        //     } catch (PDOException $e) {
        //         $response = [
        //             'success' => 0, 
        //             'response' => 'Gagal Simpan Data Silahkan Coba Kembali',
        //             'error' => $e
        //         ];
        //     }

        // }

        // return $response;
        return $data;
    }

    function saveDistributionPlanTemp($dbgl,$data,$dbh)
    {
        foreach ($data->dt_so as $value_so) {

            $kp_office = $value_so->kp_office;
            $total_do = $value_so->total_do; 
            $alamat_promosi = $value_so->alamat_promosi; 
            
            foreach ($value_so->detail as $value_dt) {

                $tonase = getTonase($value_dt);
                
                $code_comp = $value_dt->code_comp;
                $type = $value_dt->type;
                $cabang = trim($value_dt->cabang);
                $tanggal = $value_dt->tanggal;
                $jaminput = $value_dt->jaminput;
                $code = $value_dt->code;
                $name = trim($value_dt->name);
                $no = $value_dt->no;
                $so_no = $value_dt->so_no;
                $item_code = trim($value_dt->item_code);
                $item_qty = $value_dt->item_qty;
                $item_qty2 = $value_dt->item_qty2;
                $keterangan = $value_dt->keterangan;
                $item_ton = $value_dt->item_ton;
                $desa = $value_dt->desa; 
                $freight_type = $value_dt->freight_type; 
                $alamat_toko = $value_dt->alamat_toko;
                $car_no = $value_dt->car_no;
                $wh_code = $value_dt->wh_code;
                $sales_code = trim($value_dt->sales_code);
                $ledgername = $value_dt->name;
                $pr_stat = $value_dt->pr_stat;
                $delivery_time = $value_dt->delivery_time;

                try {
                    $query = "INSERT INTO t_distribution_plan_temp2(
                                    code_comp,type,cabang,tanggal,jaminput,code,name,no,so_no,item_code,
                                    item_qty,item_qty2,keterangan,item_ton,desa,alamat_toko,car_no,wh_code,
                                    alamat_promosi,total_do,dept_code,dept_name,ot_flag,freight_type,
                                    sales_code,kp_office,ledgername,pr_stat,delivery_time
                                ) VALUES (
                                    '$code_comp','$type','$cabang','$tanggal','$jaminput','$code','$name','$no','$so_no','$item_code',
                                    $item_qty,$item_qty2,'$keterangan', $item_ton,'$desa','$alamat_toko','$car_no','$wh_code',
                                    '$alamat_promosi',$total_do,'$dept_code','$dept_name','$ot_flag','$freight_type',
                                    '$sales_code','$kp_office','$ledgername','$pr_stat','$delivery_time'
                                )";

                    $stmt = $dbgl->prepare($query);
                    $stmt->execute();
                    $response = [
                        'success' => 1,
                        'response' => 'Data Berhasil Disimpan'
                    ];
                } catch (PDOException $e) {
                    $response = [
                        'success' => 0, 
                        'response' => 'Gagal Simpan Data Silahkan Coba Kembali',
                        'error' => $e
                    ];
                }

                $tonase = [];

            }

        }

        return $response;
        // return $data;
    }

?>