<?
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(-1);

$menu_code = 'displan';
$accGroup = get_acc_group($menu_code, $dbh);

if ( $accGroup['acc_menu'] ) :
    $messageBot = realpath(dirname(__FILE__))."/".basename(__FILE__)."%0A#distributionplan_belum_demo_".$username."%0A";
    $username = $_SESSION['username'];
    $location = $_SESSION['location'];
    $menu_desc = $accGroup['menu_desc'];
    $acc_save = $accGroup['acc_save'];
    $acc_edit = $accGroup['acc_edit'];
    $acc_delete = $accGroup['acc_delete'];
    $acc_download = $accGroup['acc_download'];

    $status = isset($_POST['status']) ? $_POST['status'] : 0;
    $shift = 'SAA&SNA';
    $date1 = '';
    $date2 = '';

    // DO KIRIM 
    //---------

    // $action = "actionDistributionPlans.php";
    // $production = true;
    // if ($production === false) {
    //     $action = "actionDistributionPlansz.php";
    // }

    //     // userlogs
    //     $datalogs = new stdClass();
    //     $datalogs->dbh = $dbh;
    //     $datalogs->menu_code = $menu_code;
    //     $datalogs->statuslogs = strtoupper($shift." - ".$date1." - ".$date2);
    //     userlogs($datalogs);

    // $rows = [];
    // $list_coce = listCostCenter($dbgl, $username);
    // $list_coce = "'".implode("', '", $list_coce)."'";
    // $where = " a.cabang IN (".$list_coce.")";
    // if ($location == 'TBN') {
    //     $where.= " AND a.tanggal >= '2020-01-06'";
    // }
	
	// if ($location == 'LTM') {
	// 	$where .= " AND a.tanggal >= '2020-11-24'";
	// }
	
    // if ($location == 'BPN') {
    //     $where.= " AND a.tanggal >= '2020-02-24'";
	// }
    
    // $query = '
    //     SELECT 
    //         a.code_comp, a.type, a.cabang, a.tanggal, a.jaminput, a.code, a.name, a.no, a.so_no, a.item_code, a.item_qty, a.item_qty2, 
    //         a.keterangan, a.item_ton, a.desa, a.alamat_toko, a.car_no, a.wh_code, d.address1 as alamat_promosi
    //         , 0 AS total_do, a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type
    //     FROM v_displan a
    //     LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp
    //     WHERE '.$where.' ORDER BY a.code, a.tanggal, a.no 
    // ';

    // sendMessageBot('74775504', urldecode($messageBot.$query), $botDeliveryPlan);
    // $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);

    // $reason = ['Stok', 'Truk', 'Gandengan', 'Plafond', 'TOP', 'Human Error', 'Permintaan Toko'];
    // if (strtoupper($_SESSION['username']) == 'ARIDARMA') {
    //     $reason[] = 'Uji Coba Sistem';
    // }

    // DO AMBIL
    //---------
    
    $query = "
        SELECT TRIM(a.wh_code) AS wh_code, a.wh_office FROM in_warehouse a 
        INNER JOIN in_warehouseuser b ON a.wh_code = b.wh_code AND b.userid = '".strtoupper($username)."'
        WHERE a.wh_status = '1'";
    $rowsWh = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
    //data so ambil

    // if ($username == 'HO') {
    //     $query_wh = "SELECT wh_code FROM in_warehouse WHERE wh_coce = '$location' AND wh_status = '1'";
    // } else {
    //     $query_wh = "SELECT wh_code FROM in_warehouse wh_status = '1'";
    // }

    // $wh_code = $dbgl->query($query_wh)->fetchAll(PDO::FETCH_ASSOC);

    $rows = [];
    $list_coce = listCostCenter($dbgl, $username);

    if ($location == 'SIN' || $location == 'HO') {
        $list_coce[] = 'CLB';
        $list_coce[] = 'CBW';
        $list_coce[] = 'SIN';
    }

    if ($location == 'LBR' || $location == 'HO') {
        $list_coce[] = 'LBR';
        $list_coce[] = 'MTR';
        $list_coce[] = 'LTM';
    }

    if ($location == 'MTR' || $location == 'HO') {
        $list_coce[] = 'LBR';
        $list_coce[] = 'MTR';
        $list_coce[] = 'LTM';
    }

    $list_coce = "'".implode("', '", $list_coce)."'";

    $where = " a.cabang IN (".$list_coce.")";

    if ($location == 'TBN') {
        $where.= " AND a.tanggal >= '2020-01-06'";
    }
    $query = "
        SELECT 
            a.code_comp, a.type, a.cabang, a.tanggal, a.jaminput, a.code, a.name, a.no, a.so_no, a.item_code, a.item_qty, a.item_qty2, 
            a.keterangan, a.item_ton, a.desa, a.alamat_toko, a.car_no, a.wh_code
            , 0 AS total_do, a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type
        FROM v_displan a
        --LEFT JOIN t_dp_detail c ON a.code_comp = c.code_comp and a.no = c.no and a.tanggal = DATE(c.tanggal::date)
        LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp
        WHERE ".$where." ORDER BY a.code, a.tanggal, a.no 
    ";

    // if ($username == 'rachmadi') {
    //     $location = 'MTR';
    // } else {
    //     $location = $location;
    // }

    if ($location == 'SIN') {
        $xlocation[] = 'SIN';
        $xlocation[] = 'CLB';
        $xlocation[] = 'CBW';
    } else {
        $xlocation[] = $location;
    }

    if ($location == 'MTR') {
        $xlocation[] = 'MTR'; 
        $xlocation[] = 'LBR';
    } else {
        $xlocation[] = $location;
    }

    // $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
    
?>

    <link rel="stylesheet" href="https://unpkg.com/vue-select@3.0.0/dist/vue-select.css">
    <link rel="stylesheet" href="../../cdn/vue/vue-toast/vue-toast.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.0/dist/sweetalert2.min.css">

    <style>
        /* Loader  */
        .lds-ripple {
            display: inline-block;
            position: relative;
            width: 80px;
            height: 80px;
            }
            .lds-ripple div {
            position: absolute;
            border: 4px solid #fff;
            opacity: 1;
            border-radius: 50%;
            animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
            }
            .lds-ripple div:nth-child(2) {
            animation-delay: -0.5s;
            }
            @keyframes lds-ripple {
            0% {
                top: 36px;
                left: 36px;
                width: 0;
                height: 0;
                opacity: 1;
            }
            100% {
                top: 0px;
                left: 0px;
                width: 72px;
                height: 72px;
                opacity: 0;
            }
            }


        /* ============== */
        .v-select .vs__dropdown-toggle {
            height: 34px!important;
            border-radius: 0px!important;
        }
        .pad-5-right {
            padding-right: 5px;
        }
        .pad-5-left {
            padding-left: 10px;
        }
        li.link {
            border: 1px solid #fff;
            border-radius: 4px;	
            margin-top: 5px;
            margin-bottom: 5px;
            margin-right: 5px;
            background-color: #5bc0de;
            color: #fff;
        }
        li.link:hover {
            background-color: #5dbcd0;
            color: #fff;
        }
        ul.navbar-nav li.link a {
            padding: 8px 18px;
            color: #fff;
        }
        ul.navbar-nav li.link a:hover {
            color: #fff;
        }
        th {
            padding: 6px 10px;
            text-transform: uppercase;
        }
        th.active .arrow {
            opacity: 1;
        }

        .arrow {
            display: inline-block;
            vertical-align: middle;
            width: 0;
            height: 0;
            margin-left: 5px;
            margin-top: -5px;
            opacity: 0.66;
            cursor: pointer;
        }

        .arrow.asc {
            border-left: 6px solid transparent;
            border-right: 6px solid transparent;
            border-bottom: 6px solid #42b983;
        }

        .arrow.dsc {
            border-left: 6px solid transparent;
            border-right: 6px solid transparent;
            border-top: 6px solid #42b983;
        }

        .VuePagination {
        text-align: center;
        }

        .vue-title {
        text-align: center;
        margin-bottom: 10px;
        }

        .vue-pagination-ad {
        text-align: center;
        }

        .glyphicon.glyphicon-eye-open {
        width: 16px;
        display: block;
        margin: 0 auto;
        }

        th:nth-child(3) {
        text-align: center;
        }

        .VueTables__child-row-toggler {
        width: 16px;
        height: 16px;
        line-height: 16px;
        display: block;
        margin: auto;
        text-align: center;
        }

        .VueTables__child-row-toggler--closed::before {
        content: "+";
        }

        .VueTables__child-row-toggler--open::before {
        content: "-";
        }

        .VueTables__heading {
            text-align: left;
        }

        [v-cloak] {
        display:none;
        }

        .lds-facebook {
            display: inline-block;
            position: relative;
            left: 48%;
            text-align: center;
            width: 64px;
            height: 74px;
        }
        .lds-facebook div {
            display: inline-block;
            position: absolute;
            left: 6px;
            width: 13px;
            background: rgba(39, 34, 229, 0.3);
            animation: lds-facebook 1.2s cubic-bezier(0, 0.5, 0.5, 1) infinite;
        }
        .lds-facebook div:nth-child(1) {
            left: 6px;
            animation-delay: -0.24s;
        }
        .lds-facebook div:nth-child(2) {
            left: 26px;
            animation-delay: -0.12s;
        }
        .lds-facebook div:nth-child(3) {
            left: 45px;
            animation-delay: 0;
        }
        @keyframes lds-facebook {
            0% {
                top: 6px;
                height: 61px;
            }
            50%, 100% {
                top: 19px;
                height: 36px;
            }
        }
    </style>

    <aside class="content-wrapper" id="app" v-cloak>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#" style="font-weight: bold;text-shadow: 1px 0px 1px #fff;"> DISTRIBUTOR PLAN - INVENTORY</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#" @click="showList('daftar_so')">List SO</a></li>
                        <li><a href="#" @click="showList('distributor_plan')">Rencana Distribusi</a></li>
                        <!-- <li><a href="#" @click="showList('daftar_pengiriman')">Daftar Distribusi</a></li> -->
                        <li><a href="#" @click="showList('daftar_force')">Force Close</a></li>
                        <li><a href="#" @click="showList('report_displan')">Laporan Pengiriman Detail</a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>
        <section class="content">
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h4><i class="fa fa-list"></i> {{text_judul}}</h4>
                        </div>
                        <div class="box-body">
                            <?php 
                                // if ($username == 'rachmadi') {
                                //     print_r($xlocation);
                                // }
                            ?>
                            <daftar_so_component v-if="view_daftar_so" :username="username" :where="where" :where2="where2" :loc="loc"></daftar_so_component>
                            <distribution_plan_component v-if="view_distribution_plan" :username="username" :where="where" :where2="where2" :usergroup="usergroup" :loc="loc"></distribution_plan_component>
                            <daftar-pengiriman-comp v-if="view_daftar_pengiriman" :loc="loc" :where="where" :where2="where2"></daftar-pengiriman-comp>
                            <daf_force_close_comp v-if="view_force_close" :username="username" :usergroup="usergroup"></daf_force_close_comp>
                            <report_displan_comp v-if="view_report_displan"></report_displan_comp>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </aside>


    <script src="../2.4.0/plugins/slimScroll/jquery.slimscroll.js"></script>
    <script src="../cdn/vue/vue.dev.js"></script>
    <script src="../cdn/vue/axios.min.js"></script>
    <script src="../cdn/vue/datepicker/datepicker.js"></script>
    <script src="../cdn/vue/datepicker/langid.js"></script>
    <script src="../cdn/vue/currency/currency.js"></script>
    <script src="../cdn/vue/vuelidate/validators.min.js"></script>
    <script src="../cdn/vue/vuelidate/vuelidate.min.js"></script>
    <script src="../cdn/vue/eventbus.js"></script>
    <script src="https://unpkg.com/vue-select@3.0.0"></script>
    <script src="../../cdn/vue/vue-toast/vue-toast.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-numeral-filter/dist/vue-numeral-filter.min.js"></script>
    <script src="https://unpkg.com/vue-toasted"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
    <script src="../cdn/vue-select/vue-moment.min.js"></script>
    <script src="../pluginz/vuejs/shared_components.js"></script>
    <script src="../cdn/vue/vue-table-2.js"></script>
    <script src="component/distribution_plan_new_comp_6.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.17.0/dist/sweetalert2.all.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>

    <!-- <script>
        var action = '<?=$action?>';
        document.title = "<?=$menu_desc?>";
        var table, row, name_x, rowData, rowDataParent, indexes;
        var rowData = [];
        var currentLocation = '<?=$location;?>';

        $.post(action, {case: 'getOfficeCode'}, function (data, textStatus, jqXHR) {
                $("#lokasikeberangkatan, #lokasipengalihan").empty();
                $("#lokasikeberangkatan, #lokasipengalihan").append( $("<option />").val('').text('Lokasi') );
                $.each(data, function(key, value) {
                    var i = value;
                    $("#lokasikeberangkatan, #lokasipengalihan").append( $("<option />").val(i).text(i) );
                });
            },
            "json"
        );

        $(document).ready(function() {
            $('#btn-simpan').prop('disabled', true);
            document.title = "<? echo $menu_desc." List "; ?>";
            table = $("#datalist").DataTable({
                bPaginate: false,
                bInfo: true,
                pageLength: 500,
                rowCallback: function (row, data, dataIndex){
                    var diff =  diffDay(data[3]);
                    var classTr = 'black';
                    if (diff >= 3 && diff < 5) {
                        classTr = 'orange';
                    }

                    if (diff >= 5) {
                        classTr = 'red';
                    }
                    $(row).addClass(classTr);
                    return row;
                },
                select: {
                    'style': 'multi',
                },
              'order': [[1, 'asc']]
            });
            
            table.on( 'select' , function (e, dt, type, indexes){
								var selected = table.rows( { selected: true } ).count();
								var dataSelected = table.rows( { selected: true }).data().toArray();
								console.log(dataSelected);
                btnSave(selected);
            })
            .on( 'deselect' , function ( e, dt, type, indexes ){
								var selected = table.rows( { selected: true } ).count();
								console.log(selected);
                btnSave(selected);
            });

            $(".textFieldDate").datetimepicker({
                weekStart: 0,
                todayBtn:  1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
                format: 'YYYY-MM-DD'
            });
        })


        function btnSave(selected)
        {
            if (selected > 0) {
                $('#btn-simpan').prop('disabled', false);
            } else {
                $('#btn-simpan').prop('disabled', true);
            }
        }

        $('#lokasikeberangkatan').change(function (e) { 
            e.preventDefault();
            $('#lokasipengalihan').val($(this).val()).change();
        });

        $('#btn-simpan').click(function (e) { 
            e.preventDefault();
            var diffData = [];
            var tableData = table.rows( { selected: true }).data().toArray();
            tableData.forEach(function(element){
                diffData.push(diffDay(element[3]));
            });
            
            if(diffData.some(el => el >= 5) === true) {
                generateTable('#dataTableReject');
                $('#batal-kirim').val(5);
                $('#modal-rejected').modal('show');
                return;
            }

            if(diffData.some(el => el >= 3 && el < 5) === true) {
                generateTable('#dataTableReject');
                $('#batal-kirim').val(2);
                $('#modal-rejected').modal('show');
                return;
            }

            //validasi pengalihan lokasi
            var whCode = $('#lokasikeberangkatan').val();
            if (whCode != "") {
                if (whCode != currentLocation) {
                    generateTable('#dataTable');
                    $('#modal-pengalihan').modal('show');
                    return;
                }
            }

            var btnSave = $(this);
            if (whCode != '') {
                btnSave.html("<i class='fa fa-spinner fa-spin'></i> loading...");
                btnSave.prop('disabled', true);
                
                $.post(action, {data:tableData, whCode:whCode, case: 'saveData', origin:''},
                    function (data, textStatus, jqXHR) {
                        if (data.message === false) {
                            alert(data.response);
                        } else {
                            submit_hidden_form('distributionplan_planning', {dp_id: data.id});
                            table.rows({ selected: true }).remove().draw( false );
                        }
                        btnSave.html("Simpan");
                        btnSave.prop('disabled', true);
                    },
                    "json"
                );
            } else {
                alert('Lokasi Keberangkatan Diperlukan!');
            }
        });

        $('#btn-simpan-pengalihan').click(function (e) { 
            e.preventDefault();
            var btnPengalihan = $(this);
            var whCodePengalihan = $('#lokasipengalihan').val();
            var tableData = {};
            if (whCodePengalihan != '') {
                btnPengalihan.html("<i class='fa fa-spinner fa-spin'></i> loading...");
                btnPengalihan.prop('disabled', true);
                tableData = table.rows( { selected: true }).data().toArray();
                $.post(action, {data:tableData, whCode:whCodePengalihan, case: 'saveData', origin:currentLocation},
                    function (data, textStatus, jqXHR) {
                        if (data.message === false) {
                            alert(data.response);
                        } else {
                            table.rows({ selected: true }).remove().draw( false );
                            $('#modal-pengalihan').modal('hide');
                            $.notify( `<center>${data.response}</center>`, {type: 'success' ,z_index: 100000});
                        }
                        btnPengalihan.html("Submit");
                        btnPengalihan.prop('disabled', false);
                    },
                    "json"
                );
            } else {
                alert('Lokasi Pengalihan Diperlukan!');
            }
        });

        $('#btn-simpan-rejected').click(function (e) { 
            e.preventDefault();
            $('#btn-batal-kirim').prop('disabled', false);
            var btnReject = $(this);
            var whCode = $('#lokasikeberangkatan').val();
            var alasan = $('#alasan').val();
            var batalKirim = $('#batal-kirim').val();
            var tableData = table.rows( { selected: true }).data().toArray();
            debugger;
            if (whCode != '') {
                btnReject.html("<i class='fa fa-spinner fa-spin'></i> loading...");
                btnReject.prop('disabled', true);
                var formData = {
                    data: tableData,
                    whCode: whCode,
                    case: 'saveRejectData',
                    alasan: alasan,
                    batalKirim: batalKirim,
                    origin: '',
                };

                if (whCode != currentLocation) {
                    formData.origin = $('#lokasipengalihan').val();
                }

                $.post(action, formData, function (data, textStatus, jqXHR) {
                        if (data.message === false) {
                            alert(data.response);
                        } else {
                            if (parseInt(batalKirim) >= 2) {
                                if (whCode == currentLocation) {
                                    submit_hidden_form('distributionplan_planning', {dp_id: data.id}); 
                                }  
                            }
                            table.rows({ selected: true }).remove().draw( false );
                        }
                        btnReject.html("Submit");
                        btnReject.prop('disabled', false);
                        $('#modal-rejected').modal('hide');
                    },
                    "json"
                );
            } else {
                alert('Lokasi Keberangkatan Diperlukan!');
            }
        });

        $('#btn-batal-kirim').click(function (e){
            $('#batal-kirim').val(1);
            $('#btn-simpan-rejected').click();
            $(this).prop('disabled', true);
        });

        $('#btn-load').click(function (e) { 
            $(this).prop('disabled', true);
            window.location.reload();
        });

        function diffDay(dateSo)
        {
            var dateNow = new Date().getTime();
            var dateSo = new Date(dateSo).getTime();
            var diffDay = parseInt((dateNow - dateSo)/(24*3600*1000));
            return diffDay;
        }

        function generateTable(id)
        {
            $(id).html('');
            var tableData = table.rows( { selected: true }).data().toArray();
						var tablePengalihan = '';
						console.log(tableData);
            $.each(tableData, function (index, value) { 
                console.log(value);
                tablePengalihan += `<tr><td>${value[1]}</td><td>${value[4]}</td><td>${value[5]}</td><td>${value[6]}</td><td>${value[8]}</td></tr>`;
            });
            $(id).append(tablePengalihan);
        }

        function submit_hidden_form(url, params) {
            var f = $("<form method='POST' target='_blank' style='display:none;'></form>").attr({
                action: url
            }).appendTo(document.body);
            for (var i in params) {
                if (params.hasOwnProperty(i)) {
                    $('<input type="hidden" />').attr({
                        name: i,
                        value: params[i]
                    }).appendTo(f);
                }
            }
            f.submit();
            f.remove();
        }
    </script> -->

    <script>
        Vue.component('v-select', VueSelect.VueSelect);
	    Vue.use(VueTables.ClientTable);
        Vue.use(Toasted);
        
        let app = new Vue({
            el: '#app',
            data() {
                return {
                    text_judul: ' DISTRIBUTOR PLAN',
                    view_daftar_so: false,
                    view_distribution_plan: true,
                    view_daftar_pengiriman: false,
                    view_force_close: false,
                    view_report_displan: false,
                    username: '<?php echo $username; ?>',
                    where: `<?php echo $list_coce; ?>`,
                    // where: `<?php //echo "'GYR'" ?>`,
                    where2: `<?php echo $arr_list_loc ?>`,
                    loc: `<?php echo $location; ?>`,
                    usergroup: `<?php echo $_SESSION['usergroup']; ?>`
                }
            },
            provide: {
                location: `<?php echo json_encode($xlocation); ?>`
            },
            computed: {
                
            },
            methods: {
                showList(value) {
                    switch (value) {
                        case 'daftar_so':
                            this.text_judul = ' DAFTAR SO';
                            this.view_daftar_so = true;
                            this.view_distribution_plan = false;
                            this.view_daftar_pengiriman = false;
                            this.view_force_close = false;
                            this.view_report_displan = false;
                            break;
                        case 'distributor_plan':
                            this.text_judul = ' RENCANA DISTRIBUSI';
                            this.view_daftar_so = false;
                            this.view_distribution_plan = true;
                            this.view_daftar_pengiriman = false;
                            this.view_force_close = false;
                            this.view_report_displan = false;
                            break;
                        case 'daftar_pengiriman':
                            this.text_judul = ' DAFTAR DISTRIBUSI';
                            this.view_daftar_so = false;
                            this.view_distribution_plan = false;
                            this.view_daftar_pengiriman = true;
                            this.view_force_close = false;
                            this.view_report_displan = false;
                            break;
                        case 'daftar_force':
                            this.text_judul = ' FORCE CLOSE';
                            this.view_daftar_so = false;
                            this.view_distribution_plan = false;
                            this.view_daftar_pengiriman = false;
                            this.view_force_close = true;
                            this.view_report_displan = false;
                            break;
                        case 'report_displan':
                            this.text_judul = ' LAPORAN PENGIRIMAN DETAIL';
                            this.view_daftar_so = false;
                            this.view_distribution_plan = false;
                            this.view_daftar_pengiriman = false;
                            this.view_force_close = false;
                            this.view_report_displan = true;
                            break;
                        default:
                            break;
                    }
                }	
            },
            watch: {
                
            },
            created() {
                			
            },
        });
    </script>

<? else : 
    header('location:..');
endif; 
?>
