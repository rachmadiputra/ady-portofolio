const EventBus = new Vue();

Vue.use(window.vuelidate.default);
const { required } = window.validators;

// Vue.use(VueToast, {position: 'top'});
Vue.use(Toasted, {
	duration: 3000,
	iconPack: 'custom-class',
	theme: 'toasted-primary',
	action: {
		text: 'OK',
		onClick: (e, toastObject) => {
			toastObject.goAway(0);
		}
	},
});

Vue.mixin({
	data() {
		return {
			action: '../../inventory/services/distribution_plan_new_data_6.php',
			doPage: '../../inventory/invtranout_dist_bak',
			doPage2: '../../inventory/invtranout_bak',
			doPage3: '../../inventory/distributionplan_cetak_do',
			doPage4: '../../inventory/invtranout_bak2',
		}
	}
});

Vue.component('daftar_so_component', {

	template: `
	<div>
		<div class="row">
			<div class="col-md-12">
				<tabs_distribution_plan_component>
					<tab_distribution_plan_component v-for="(dt,i) in tabs" :key="i" :name="dt.name" :icon="dt.icon" :selected="i === 0 ? 'true' : ''">
						<!-- <div v-if="i == 0">
							<div class="lds-facebook" v-if="loading"><div></div><div></div><div></div></div>
							<daf_so_sc_req_component :data="listSC_req_kirim" :username="username" :loc="loc"></daf_so_sc_req_component>
						</div> --> 
						<div v-if="i == 0">
							<div class="lds-facebook" v-if="loading"><div></div><div></div><div></div></div>
							<daf_so_kirim_new :data_less="listSO_kirim_less" :data_more="listSO_kirim_more" :username="username" :loc="loc"></daf_so_kirim_new>
						</div>
						<div v-if="i == 1">
							<div class="lds-facebook" v-if="loading"><div></div><div></div><div></div></div>
							<daf_so_ambil_component :data="listSO_ambil" :username="username" :loc="loc"></daf_so_ambil_component>
						</div>
					</tab_distribution_plan_component>
				</tabs_distribution_plan_component>
			</div>
		</div>
		<div class="modal fade" id="modal_rencana_ambil" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<div class="row">
							<div class="col-sm-6">
								<h4 class="modal-title" id="exampleModalLabel">Rencana Ambil</h4>
							</div>
							<div class="col-sm-6">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
						</div>
					</div>
					<form @submit.prevent="updateDPAmbil">
						<div class="modal-body">
							<div class="row">
								<div class="col-md-12" style="text-transform: capitalize;">
									<table class="table table-hover table-responsive table-striped">
										<thead>
											<tr>
												<th class="active">NO. LEDGER</th>
												<th class="active">NAMA TOKO</th>
												<th class="active">NO. SM</th>
												<th class="active">NO. SO</th>
												<th class="active">ITEM</th>
												<th class="active">TAMBAH/KURANG QTY</th>
											</tr>
										</thead>
										<tbody>
											<tr v-for="(dt,i) in data_item_ambil" :key="i">
												<td>{{ dt.code }}</td>
												<td>{{ dt.name }}</td>
												<td v-if="dt.type === 'SM'">{{ dt.no }}</td>
												<td v-else></td>
												<td>{{ dt.so_no }}</td>
												<td>{{ dt.item_code }}</td>
												<td>{{ dt.item_qty2 }}</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							</br>
							<div>
								<div class="form-group">
									<label for="recipient-name" class="col-form-label">No. Plat Truck</label>
									<input class="form-control" type="text" v-model="v_nopol_truck"/>
								</div>
								<div class="form-group">
									<label for="recipient-name" class="col-form-label">Nama Supir</label>
									<input class="form-control" type="text" v-model="v_sopir_name"/>
								</div>
								<div class="form-group">
									<label for="recipient-name" class="col-form-label">Kuantiti</label>
									<input class="form-control" type="text" v-model="v_kuantiti" disabled/> 
								</div>
								<div class="form-group">
									<label for="recipient-name" class="col-form-label">Pilih Gudang</label>
									<v-select style="text-transform: capitalize" label="wh_code" :options="dt_gudang" v-model="v_gudang" :key="v_gudang" :reduce="dt_gudang=>dt_gudang.wh_code"></v-select>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary" :disabled="isDisabled">Simpan & Cetak DO</button>
							<button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="modal fade" id="modal_confirmPrice_kirim" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<div class="row">
							<div class="col-sm-6">
								<h4 class="modal-title" id="exampleModalLabel">Konfirmasi Harga</h4>
							</div>
							<div class="col-sm-6">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
						</div>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-sm-6"><h4>Harga Sales</h4></div>
							<div class="col-sm-6">
								<div class="row">
									<div class="col-sm-9" style="text-align: end;"><label for="harga_sales">Pilih Harga Sales</label></div>
									<div class="col-sm-2" style="text-align: center;"><input type="radio" id="harga_sales" v-model="pilih_harga_kirim" value="price_kirim_sales"></div>
								</div>
							</div>
						</div>
						<table class="table table-hover table-responsive table-striped">
							<thead>
								<tr>
									<th>Cabang</th>
									<th>Code Item</th>
									<th>Harga</th>
								</tr>
							</thead>
							<tbody v-for="(dta,i) in confirmPrice_kirim">
								<tr>
									<td>{{dta.price_sales.cabang}}</td>
									<td>{{dta.price_sales.item_code}}</td>
									<td>{{dta.price_sales.price_amount}}</td>
								</tr>
							</tbody>
						</table>

						</br></br>
						
						<div class="row">
							<div class="col-sm-6"><h4>Harga SO</h4></div>
							<div class="col-sm-6">
								<div class="row">
									<div class="col-sm-9" style="text-align: end;"><label for="harga_so">Pilih Harga SO</label></div>
									<div class="col-sm-2" style="text-align: center;"><input type="radio" id="harga_so" v-model="pilih_harga_kirim" value="price_kirim_so"></div>
								</div>
							</div>
						</div>
						<table class="table table-hover table-responsive table-striped">
							<thead>
								<tr>
									<th>Cabang</th>
									<th>No. SO</th>
									<th>Code Item</th>
									<th>Harga</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody v-for="(dt,i) in confirmPrice_kirim">
								<tr>
									<td>{{dt.price_so.cabang}}</td>
									<td>{{dt.price_so.so_no}}</td>
									<td>{{dt.price_so.item_code}}</td>
									<td>{{dt.price_so.price_amount}}</td>
									<td v-if="dt.hasil === 1">Berhasil Simpan</td>
									<td v-else-if="dt.hasil === 2">Data Harga Tidak Sama</td>
									<td v-else>Data Harga Tidak Tersedia</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" data-dismiss="modal" @click="simpanHargaKirim">Simpan</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="modal_confirmPrice_kirim_pengalihan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<div class="row">
							<div class="col-sm-6">
								<h4 class="modal-title" id="exampleModalLabel">Konfirmasi Harga Pengalihan</h4>
							</div>
							<div class="col-sm-6">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
						</div>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-sm-6"><h4>Harga Sales</h4></div>
							<div class="col-sm-6">
								<div class="row">
									<div class="col-sm-9" style="text-align: end;"><label for="harga_sales">Pilih Harga Sales</label></div>
									<div class="col-sm-2" style="text-align: center;"><input type="radio" id="harga_sales" v-model="pilih_harga_kirim" value="price_kirim_sales"></div>
								</div>
							</div>
						</div>
						<table class="table table-hover table-responsive table-striped">
							<thead>
								<tr>
									<th>Cabang</th>
									<th>Code Item</th>
									<th>Harga</th>
								</tr>
							</thead>
							<tbody v-for="(dta,i) in confirmPrice_kirim">
								<tr>
									<td>{{dta.price_sales.cabang}}</td>
									<td>{{dta.price_sales.item_code}}</td>
									<td>{{dta.price_sales.price_amount}}</td>
								</tr>
							</tbody>
						</table>

						</br></br>
						
						<div class="row">
							<div class="col-sm-6"><h4>Harga SO</h4></div>
							<div class="col-sm-6">
								<div class="row">
									<div class="col-sm-9" style="text-align: end;"><label for="harga_so">Pilih Harga SO</label></div>
									<div class="col-sm-2" style="text-align: center;"><input type="radio" id="harga_so" v-model="pilih_harga_kirim" value="price_kirim_so"></div>
								</div>
							</div>
						</div>
						<table class="table table-hover table-responsive table-striped">
							<thead>
								<tr>
									<th>Cabang</th>
									<th>No. SO</th>
									<th>Code Item</th>
									<th>Harga</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody v-for="(dt,i) in confirmPrice_kirim">
								<tr>
									<td>{{dt.price_so.cabang}}</td>
									<td>{{dt.price_so.so_no}}</td>
									<td>{{dt.price_so.item_code}}</td>
									<td>{{dt.price_so.price_amount}}</td>
									<td v-if="dt.hasil === 1">Berhasil Simpan</td>
									<td v-else-if="dt.hasil === 2">Data Harga Tidak Sama</td>
									<td v-else>Data Harga Tidak Tersedia</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" data-dismiss="modal" @click="simpanHargaKirimPengalihan">Simpan</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="modal_confirmPrice_ambil" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<div class="row">
							<div class="col-sm-6">
								<h4 class="modal-title" id="exampleModalLabel">Konfirmasi Harga</h4>
							</div>
							<div class="col-sm-6">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
						</div>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-sm-6"><h4>Harga Sales</h4></div>
							<div class="col-sm-6">
								<div class="row">
									<div class="col-sm-9" style="text-align: end;"><label for="harga_ambil_sales">Pilih Harga Sales</label></div>
									<div class="col-sm-2" style="text-align: center;"><input type="radio" id="harga_ambil_sales" v-model="pilih_harga_ambil" value="price_ambil_sales"></div>
								</div>
							</div>
						</div>
						<table class="table table-hover table-responsive table-striped">
							<thead>
								<tr>
									<th>Cabang</th>
									<th>Code Item</th>
									<th>Harga</th>
								</tr>
							</thead>
							<tbody v-for="(dta,i) in confirmPrice_ambil">
								<tr>
									<td>{{dta.price_sales.cabang}}</td>
									<td>{{dta.price_sales.item_code}}</td>
									<td>{{dta.price_sales.price_amount}}</td>
								</tr>
							</tbody>
						</table>

						</br></br>
						
						<div class="row">
							<div class="col-sm-6"><h4>Harga SO</h4></div>
							<div class="col-sm-6">
								<div class="row">
									<div class="col-sm-9" style="text-align: end;"><label for="harga_ambil_so">Pilih Harga SO</label></div>
									<div class="col-sm-2" style="text-align: center;"><input type="radio" id="harga_ambil_so" v-model="pilih_harga_ambil" value="price_ambil_so"></div>
								</div>
							</div>
						</div>
						<table class="table table-hover table-responsive table-striped">
							<thead>
								<tr>
									<th>Cabang</th>
									<th>No. SO</th>
									<th>Code Item</th>
									<th>Harga</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody v-for="(dt,i) in confirmPrice_ambil">
								<tr>
									<td>{{dt.price_so.cabang}}</td>
									<td>{{dt.price_so.so_no}}</td>
									<td>{{dt.price_so.item_code}}</td>
									<td>{{dt.price_so.price_amount}}</td>
									<td v-if="dt.hasil === 1">Berhasil Simpan</td>
									<td v-else-if="dt.hasil === 2">Data Harga Tidak Sama</td>
									<td v-else>Data Harga Tidak Tersedia</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" data-dismiss="modal" @click="simpanHargaAmbil">Simpan</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="modal_confirmPrice_ambil_pengalihan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<div class="row">
							<div class="col-sm-6">
								<h4 class="modal-title" id="exampleModalLabel">Konfirmasi Harga Pengalihan</h4>
							</div>
							<div class="col-sm-6">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
						</div>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-sm-6"><h4>Harga Sales</h4></div>
							<div class="col-sm-6">
								<div class="row">
									<div class="col-sm-9" style="text-align: end;"><label for="harga_ambil_sales">Pilih Harga Sales</label></div>
									<div class="col-sm-2" style="text-align: center;"><input type="radio" id="harga_ambil_sales" v-model="pilih_harga_ambil" value="price_ambil_sales"></div>
								</div>
							</div>
						</div>
						<table class="table table-hover table-responsive table-striped">
							<thead>
								<tr>
									<th>Cabang</th>
									<th>Code Item</th>
									<th>Harga</th>
								</tr>
							</thead>
							<tbody v-for="(dta,i) in confirmPrice_ambil">
								<tr>
									<td>{{dta.price_sales.cabang}}</td>
									<td>{{dta.price_sales.item_code}}</td>
									<td>{{dta.price_sales.price_amount}}</td>
								</tr>
							</tbody>
						</table>

						</br></br>
						
						<div class="row">
							<div class="col-sm-6"><h4>Harga SO</h4></div>
							<div class="col-sm-6">
								<div class="row">
									<div class="col-sm-9" style="text-align: end;"><label for="harga_ambil_so">Pilih Harga SO</label></div>
									<div class="col-sm-2" style="text-align: center;"><input type="radio" id="harga_ambil_so" v-model="pilih_harga_ambil" value="price_ambil_so"></div>
								</div>
							</div>
						</div>
						<table class="table table-hover table-responsive table-striped">
							<thead>
								<tr>
									<th>Cabang</th>
									<th>No. SO</th>
									<th>Code Item</th>
									<th>Harga</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody v-for="(dt,i) in confirmPrice_ambil">
								<tr>
									<td>{{dt.price_so.cabang}}</td>
									<td>{{dt.price_so.so_no}}</td>
									<td>{{dt.price_so.item_code}}</td>
									<td>{{dt.price_so.price_amount}}</td>
									<td v-if="dt.hasil === 1">Berhasil Simpan</td>
									<td v-else-if="dt.hasil === 2">Data Harga Tidak Sama</td>
									<td v-else>Data Harga Tidak Tersedia</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" data-dismiss="modal" @click="simpanHargaAmbilPengalihan">Simpan</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="modalDP" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<div class="row">
							<div class="col-sm-6">
								<h4 class="modal-title" id="exampleModalLabel">Modal DP</h4>
							</div>
							<div class="col-sm-6">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
						</div>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-md-12">
								<table class="table table-hover table-responsive table-striped">
									<thead>
										<tr>
											<th>Cabang</th>
											<th>No. SO</th>
											<th>Code Item</th>
											<th>Item Ton</th>
										</tr>
									</thead>
									<tbody v-for="(dt,i) in listSO_kirim_more">
										<tr>
											<td>{{dt.cabang}}</td>
											<td>{{dt.so_no}}</td>
											<td>{{dt.item_code}}</td>
											<td>{{dt.ton}}</td>
										</tr>
										<tr>
											<th>Item Code</th>
											<th>Item Qty</th>
											<th>Item Weight</th>
											<th>Price Amt</th>
										</tr>
										<tr v-for="detail in dt.detail">
											<td>{{detail.item_code}}</td>
											<td v-if="detail.item_qty == detail.item_qty2">{{detail.item_qty}}</td>
											<td v-else>{{detail.item_qty2}}</td>
											<td>{{detail.item_weight}}</td>
											<td>{{detail.price_amt}}</td>
										</tr>
										<tr></tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	`,
	components: {
		vuejsDatepicker
	},
	data() {
		return {
			loading: false,
			tabs: [
				// {
				// 	name: 'SALES REQUEST',
				// 	icon: '<i class="fa fa-print"></i>',
				// 	counter: 0,
				// },
				{
					name: 'DAFTAR SO KIRIM',
					icon: '<i class="fa fa-print"></i>',
					counter: 0,
				},
				{
					name: 'DAFTAR SO AMBIL',
					icon: '<i class="fa fa-print"></i>',
					counter: 1,
				},
			],
			listSO_kirim: [],
			listSO_kirim_pengalihan: [],
			listSO_ambil: [],
			listSO_ambil_pengalihan: [],
			listOfficeCode: [],
			listSC_req_kirim: [],
			v_tgl_perintah_kirim: '',
			officeCode: [],
			checkedSO: [],
			v_office: '',
			confirmPrice_kirim: [],
			confirmPrice_ambil: [],
			pilih_harga_kirim: '',
			pilih_harga_ambil: '',
			arr_price_kirim: [{
				data_so: [],
				price: []
			}],
			new_arr_price_kirim: [{
				data_so: []
			}],
			new_arr_price_ambil: [{
				data_so: []
			}],
			arr_price_ambil: [{
				data_so: [],
				price: []
			}],
			val_price: [],
			v_search_so: "",
			tanggalnew: [],
			id_displan: "",
			data_item_ambil: [],
			dt_truck: [],
			dt_supir: [],
			dt_gudang: [],
			v_nopol_truck: "",
			v_sopir_name: "",
			v_gudang: "",
			v_kuantiti: "",
			listSO_kirim_less: [],
			listSO_kirim_more: []

		}
	},
	props: ["username", "where", "loc", "where2"],
	methods: {
		getDateRange() {
			var today = '';

			axios.post(this.action, {
				case: 'getDatesFromRange'
			}).then(response => {
				today = response.data[0];
				this.getDataSOKirim(today);
			});
		},
		getDataSOKirim(date) {
			this.loading = true;
			this.new_arr_price_kirim[0].data_so = [];
			this.confirmPrice_kirim = [];

			axios.post(this.action, {
				case: 'getDataSOKirim',
				username: this.username,
				where: this.where,
				date: date
			}).then(response => {
				this.loading = false;
				if (response.data != null) {
					this.listSO_kirim = response.data;
				} else {
					this.listSO_kirim = [];
				}
			});
		},
		getDataSOKirimNew() {
			this.loading = true;

			axios.post(this.action, {
				case: 'getDataSOKirim',
				username: this.username,
				where: this.where
			}).then(response => {
				this.loading = false;
				console.log(response.data);
				this.listSO_kirim_less = response.data.less;
				this.listSO_kirim_more = response.data.more;
				// if (this.listSO_kirim_more.length > 0) {
				// 	$('#modalDP').modal('show');
				// }
			});
		},
		getDataSOKirimSCReq() {
			this.loading = true;
			this.new_arr_price_kirim[0].data_so = [];
			this.confirmPrice_kirim = [];

			axios.post(this.action, {
				case: 'getDataSOKirimSCReq',
				where: this.where
			}).then(response => {
				if (response.data != null) {
					this.listSC_req_kirim = response.data;
				} else {
					this.listSC_req_kirim = [];
				}
			});
		},
		getDataSOAmbil() {
			this.loading = true;
			this.new_arr_price_ambil[0].data_so = [];
			this.confirmPrice_ambil = [];

			axios.post(this.action, {
				case: 'getDataSOAmbil',
				username: this.username,
				where: this.where
			}).then(response => {
				this.loading = false;
				if (response.data != null) {
					this.listSO_ambil = response.data;
				} else {
					this.listSO_ambil = [];
				}
			});
		},
		getTruck() {
			axios.post(this.action, {
				case: "getTruck",
				where: this.where
			}).then(response => {
				this.dt_truck = response.data;
			})
		},
		getSopir() {
			axios.post(this.action, {
				case: "getSopir",
				where: this.where
			}).then(response => {
				this.dt_supir = response.data;
			})
		},
		getGudang() {
			axios.post(this.action, {
				case: "getGudang",
				username: 'rachmadi',
			}).then(response => {
				this.dt_gudang = response.data;
			})
		},
		add_SO() {
			axios.post(this.action, {
				case: 'saveDistributionPlan',
				tgl_perintah: this.v_tgl_perintah,
				username: this.username,
				dt_so: this.checkedSO,
			}).then(response => {
				console.log(response.data);
			});
		},
		simpanHargaKirim() {
			// if (this.val_price == 'price_kirim_so') {
			// 	for (let i = 0; i < this.confirmPrice_kirim.length; i++) {
			// 		this.arr_price_kirim[0].data_so.push(this.confirmPrice_kirim[i].data_so);
			// 		this.arr_price_kirim[0].price.push(this.confirmPrice_kirim[i].price_so);
			// 	}
			// } else {
			// 	for (let i = 0; i < this.confirmPrice_kirim.length; i++) {
			// 		this.arr_price_kirim[0].data_so.push(this.confirmPrice_kirim[i].data_so);
			// 		this.arr_price_kirim[0].price.push(this.confirmPrice_kirim[i].price_sales);
			// 	}
			// }

			// this.new_arr_price_kirim = [];
			for (let i = 0; i < this.confirmPrice_kirim.length; i++) {
				this.new_arr_price_kirim[0].data_so.push(this.confirmPrice_kirim[i].data_so);
			}
			// console.log(this.new_arr_price_kirim);

			axios.post(this.action, {
				case: 'simpanDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
				dt_price: this.new_arr_price_kirim,
				loc: this.loc
			}).then(response => {
				// location.reload();

				this.new_arr_price_kirim[0].data_so = [];
				// this.arr_price_kirim[0].price = [];
				this.checkedSO = [];

				let rspn = response.data;
				if (rspn.success === 1) {
					this.getDataSOKirim();
					this.getDataSOAmbil();
					console.log(response.data);
					this.$toasted.success(rspn.message, { duration: 5000 });
				} else {
					this.getDataSOKirim();
					this.getDataSOAmbil();
					console.log(response.data);
					this.$toasted.error(rspn.message, { duration: 5000 });
				}
			});
		},
		simpanHargaKirimNew() {
			for (let i = 0; i < this.confirmPrice_kirim.length; i++) {
				this.new_arr_price_kirim[0].data_so.push(this.confirmPrice_kirim[i].data_so);
			}

			axios.post(this.action, {
				case: 'simpanDistributionPlanKirimNew',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
				dt_price: this.new_arr_price_kirim,
				loc: this.loc
			}).then(response => {
				console.log(response.data);
			});
		},
		simpanHargaKirimPengalihan() {
			axios.post(this.action, {
				case: 'simpanDistributionPlanKirimPengalihan',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO
			}).then(response => {
				// location.reload();

				let rspn = response.data;
				if (rspn.success === 1) {
					this.getDataSOKirim();
					this.getDataSOAmbil();
					console.log(response.data);
					this.$toasted.success(rspn.message, { duration: 5000 });
				} else {
					this.getDataSOKirim();
					this.getDataSOAmbil();
					console.log(response.data);
					this.$toasted.error(rspn.message, { duration: 5000 });
				}
			});
		},
		simpanHargaAmbil() {
			// if (this.val_price == 'price_kirim_so') {
			// 	for (let i = 0; i < this.confirmPrice_ambil.length; i++) {
			// 		this.arr_price_ambil[0].data_so.push(this.confirmPrice_ambil[i].data_so);
			// 		this.arr_price_ambil[0].price.push(this.confirmPrice_ambil[i].price_so);
			// 	}
			// } else {
			// 	for (let i = 0; i < this.confirmPrice_ambil.length; i++) {
			// 		this.arr_price_ambil[0].data_so.push(this.confirmPrice_ambil[i].data_so);
			// 		this.arr_price_ambil[0].price.push(this.confirmPrice_ambil[i].price_sales);
			// 	}
			// }

			for (let i = 0; i < this.confirmPrice_ambil.length; i++) {
				this.new_arr_price_ambil[0].data_so.push(this.confirmPrice_ambil[i].data_so);
			}
			// console.log(this.new_arr_price_ambil);

			axios.post(this.action, {
				case: 'simpanDistributionPlanAmbil',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
				dt_price: this.new_arr_price_ambil
			}).then(response => {
				// location.reload();

				this.new_arr_price_ambil[0].data_so = [];
				// this.new_arr_price_ambil[0].price = [];
				this.checkedSO = [];
				this.id_displan = "";

				let rspn = response.data;
				if (rspn.success === 1) {
					console.log(response.data);
					this.id_displan = response.data.id;
					this.getDisPlanByID(this.id_displan);
					this.getDataSOKirim();
					this.getDataSOAmbil();
					// this.$toasted.success(rspn.message, { duration: 5000 });
				} else {
					console.log(response.data);
					this.id_displan = response.data.id;
					this.getDisPlanByID(this.id_displan);
					this.getDataSOKirim();
					this.getDataSOAmbil();
					// this.$toasted.error(rspn.message, { duration: 5000 });
				}
			});
		},
		simpanHargaAmbilPengalihan() {
			axios.post(this.action, {
				case: 'simpanDistributionPlanAmbilPengalihan',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO
			}).then(response => {
				// location.reload();

				let rspn = response.data;
				if (rspn.success === 1) {
					this.getDataSOKirim();
					this.getDataSOAmbil();
					console.log(response.data);
					this.$toasted.success(rspn.message, { duration: 5000 });
				} else {
					this.getDataSOKirim();
					this.getDataSOAmbil();
					console.log(response.data);
					this.$toasted.error(rspn.message, { duration: 5000 });
				}
			});
		},
		getDisPlanByID(id) {
			axios.post(this.action, {
				case: 'getDisPlanByID',
				id: id
			}).then(response => {
				// location.reload();
				this.data_item_ambil = response.data;
				this.v_kuantiti = this.data_item_ambil[0].item_qty2;
				console.log(this.v_kuantiti);
			});
		},
		updateDPAmbil2() {
			axios.post(this.action, {
				case: 'updateDistributionPlanAmbil2',
				nopol_truck: this.v_nopol_truck,
				sopir_name: this.v_sopir_name,
				gudang: this.v_gudang,
				dt_so: this.id_displan,
			}).then(response => {

				let rspn = response.data;

				this.v_nopol_truck = "";
				this.v_sopir_name = "";
				this.v_gudang = "";
				// this.data_ambil = "";

				this.$toasted.success(rspn.message, { duration: 3000 });
				$('#modal_rencana_ambil').modal('hide');

				function submit_hidden_form(url, params) {
					var f = $("<form method='POST' target='_blank' style='display:none;'></form>").attr({
						action: url
					}).appendTo(document.body);
					for (var i in params) {
						if (params.hasOwnProperty(i)) {
							$('<input type="hidden" />').attr({
								name: i,
								value: params[i]
							}).appendTo(f);
						}
					}
					f.submit();
					f.remove();
				}

				submit_hidden_form(this.doPage3, {
					dp_id: this.id_displan,
				})
				// location.reload();
			});
		},
	},
	created() {

		this.getDataSOKirimNew();
		this.getDateRange();
		this.getDataSOAmbil();
		this.getDataSOKirimSCReq();

		EventBus.$on('loadSOByTanggal', (tanggal) => {
			console.log(tanggal);
		});


		EventBus.$on('refreshSoAmbil', () => {
			this.getDataSOAmbil();
		})
		EventBus.$on('refresh_req_date', (today) => {
			this.getDataSOKirim(today);
			this.getDataSOAmbil();
			this.getDataSOKirimSCReq();
		});
		EventBus.$on('refresh_req_sc', () => {
			this.getDateRange();
			this.getDataSOAmbil();
			this.getDataSOKirimSCReq();
		});
		EventBus.$on('addItemSO', (data) => {
			this.checkedSO = data;
		});
		EventBus.$on('confirmPrice_kirim', (price_data, tgl_perintah, username, v_office, checkedSO) => {

			this.confirmPrice_kirim = price_data;
			this.v_tgl_perintah_kirim = tgl_perintah;
			this.username = username;
			this.v_office = v_office;
			this.checkedSO = checkedSO;

			this.simpanHargaKirim();
		});

		EventBus.$on('confirmPrice_kirim_new', (price_data, tgl_perintah, username, v_office, checkedSO) => {

			this.confirmPrice_kirim = price_data;
			this.v_tgl_perintah_kirim = tgl_perintah;
			this.username = username;
			this.v_office = v_office;
			this.checkedSO = checkedSO;

			this.simpanHargaKirimNew();
		});

		EventBus.$on('confirmPrice_kirim_pengalihan', (price_data, tgl_perintah, username, v_office, checkedSO) => {

			this.confirmPrice_kirim = price_data;
			this.v_tgl_perintah_kirim = tgl_perintah;
			this.username = username;
			this.v_office = v_office;
			this.checkedSO = checkedSO;

			$('#modal_confirmPrice_kirim_pengalihan').modal('show');
		});

		EventBus.$on('confirmPrice_ambil', (price_data, tgl_perintah, username, v_office, checkedSO) => {

			this.confirmPrice_ambil = price_data;
			this.v_tgl_perintah_kirim = tgl_perintah;
			this.username = username;
			this.v_office = v_office;
			this.checkedSO = checkedSO;

			this.getTruck();
			this.getSopir();
			this.getGudang();

			this.simpanHargaAmbil();
			$('#modal_rencana_ambil').modal('show');
		});

		EventBus.$on('confirmPrice_ambil_pengalihan', (price_data, tgl_perintah, username, v_office, checkedSO) => {

			this.confirmPrice_ambil = price_data;
			this.v_tgl_perintah_kirim = tgl_perintah;
			this.username = username;
			this.v_office = v_office;
			this.checkedSO = checkedSO;

			$('#modal_confirmPrice_ambil_pengalihan').modal('show');
		});
	},
	computed: {
		isDisabled: function () {
			if (this.v_nopol_truck == '' || this.v_sopir_name == '' || this.v_gudang == '') {
				return true;
			} else {
				return false;
			}
		},
		truk_luar: function () {
			if (this.v_pilih_truck == 'truck_dalam') {
				this.v_ritase = this.v_ritase
			} else {
				this.v_ritase = 0
			}
			return this.v_ritase;
		}
	},
	watch: {
		pilih_harga_kirim(value) {
			this.val_price = value;
		}
	}

});

Vue.component('daf_so_sc_req_component', {
	template:
		`
	<div>
		<div class="row">
			<div class="col-md-12" style="text-align: end;">
				<button type="button" class="btn btn-xm btn-success" @click="load"><i class="fa fa-refresh"></i></button>
			</div>	
		</div>
		</br>
		<div class="row">
			<div class="col-md-12" style="text-transform: capitalize;">
				<div style="overflow-x: auto;">
					<table class="table table-hover table-responsive table-striped" style="font-size: 12px;">
						<thead>
							<tr>
								<th class="active">Comp</th>
								<th class="active">Type</th>
								<th v-for="column in new_columns" @click="activeColumn = column" class="active" :style="widthHead(column.name)">
									{{ colTitles[column.name] }} 
									<span @click="column.order = column.order * (-1), sortBy(column.name)" class="arrow" :class="column.order > 0 ? 'asc' : 'dsc'" ></span>
								</th>
								<th class="active">Pilih</th>
							</tr>
						</thead>
						<tbody>
							<tr v-for="(dt,i) in filterListST" :key="i"  v-if="i >= pagestart && i < pageend" style="text-transform: uppercase;" 
								v-bind:style="{backgroundColor: dt.cabang == dt.kp_office ? 'none' : 'yellow'}">
								<td>{{ dt.code_comp }}</td>
								<td>{{ dt.tipe_muat }}</td>
								<td>{{ dt.code }}</td>
								<td>{{ dt.ledgername }}</td>
								<td>{{ dt.tanggal }} {{ dt.jaminput }}</td>
								<td>{{ dt.delivery_time }}</td>
								<td>{{ dt.sm_no }}</td>
								<td>{{ dt.so_no }}</td>
								<td>{{ dt.item_code }}</td>
								<td>{{ dt.alamat_toko }}</td>
								<td>{{ dt.keterangan }}</td>
								<td>{{ dt.sales_code }}</td>
								<td>
									<div class="row">
										<div class="col-md-6" style="text-align: end;">
											<button type="button" class="btn btn-xs btn-success" @click="approve(dt)">Setujui</button>
										</div>
										<div class="col-md-6" style="text-align: end;">
											<button type="button" class="btn btn-xs btn-danger" @click="cancel(dt)">Tolak</button>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			</br></br></br>
			<div class="col-md-12" style="text-transform: capitalize; text-align: center;">
			<paginate 
				:pagecount="totalpages"
				:containerclass="'pagination pagination-sm'"
				:clickhandler="pagination"
				:prevtext="prevButtonIcon"
				:nexttext="nextButtonIcon">
			</paginate>
			</div>
		</div>
	</div>
	`,
	props: {
		username: {
			required: true
		},
		data: {
			type: Array,
			required: true
		},
	},
	data() {
		return {
			columns: ['type', 'code', 'ledgername', 'tanggal', 'sm_no', 'so_no',
				'item_code', 'alamat_toko', 'keterangan', 'jaminput', 'code_comp', 'sales_code', 'status_so', 'dept_code', 'pilih'],
			/* pagination setting */
			pagestart: 0,
			pagecurrent: 1,
			itemsperpage: 50,
			pageend: 50,
			prevButtonIcon: '<i class="fa fa-chevron-left"></i>',
			nextButtonIcon: '<i class="fa fa-chevron-right"></i>',
			/* sort setting */
			currentSort: 'name',
			currentSortDir: 'asc',
			/* table column setting */
			activeColumn: {},
			new_columns: [
				// { name: 'type', order: 1 },
				// { name: 'tipe_muat', order: 1 },
				{ name: 'code', order: 1 },
				{ name: 'ledgername', order: 1 },
				{ name: 'tanggal', order: 1 },
				{ name: 'delivery', order: 1 },
				{ name: 'sm_no', order: 1 },
				{ name: 'so_no', order: 1 },
				{ name: 'item_code', order: 1 },
				{ name: 'alamat_toko', order: 1 },
				{ name: 'keterangan', order: 1 },
				{ name: 'sales_code', order: 1 },
				// { name: 'jaminput', order: 1 },
				// { name: 'code_comp', order: 1 },
				// { name: 'dept_code', order: 1 }
			],
			colTitles: {
				// 'type': 'Comp',
				// 'tipe_muat': 'Type',
				'code': 'Ledger',
				'ledgername': 'Customer',
				'tanggal': 'Tanggal',
				'delivery': 'Tgl. Kirim',
				'sm_no': 'No. SM',
				'so_no': 'No. SO',
				'item_code': 'Item & QTY',
				'alamat_toko': 'Alamat',
				'keterangan': 'Keterangan',
				'sales_code': 'Sales Code',
				// 'jaminput': 'Jam Buat',
				// 'code_comp': 'Comp',
				// 'dept_code': 'Dept Code'
			},
			v_search_so: '',
			styleObject: {
				background: 'none',
			}
		}
	},
	methods: {
		widthHead(name) {
			if (name == 'code') {
				return 'width: 7%';
			}
			if (name == 'ledgername') {
				return 'width: 8%';
			}
			if (name == 'tanggal') {
				return 'width: 10%';
			}
			if (name == 'delivery') {
				return 'width: 8%';
			}
			if (name == 'sm_no') {
				return 'width: 6%';
			}
			if (name == 'sales_code') {
				return 'width: 9%';
			}
		},
		load() {
			EventBus.$emit('refresh_req_sc');
		},
		approve(dt) {
			// console.log(dt.id_temp);
			axios.post(this.action, {
				case: 'updateApproveReqSales',
				id_temp: dt.id_temp
			}).then(response => {
				if (response.data.success == 1) {
					this.$toasted.success(response.data.message, { duration: 5000 });
				} else {
					this.$toasted.error(response.data.message, { duration: 5000 });
				}
				this.load();
			});
		},
		cancel(dt) {
			// console.log(dt);
			axios.post(this.action, {
				case: 'updateDeleteReqSales',
				id_temp: dt.id_temp
			}).then(response => {
				if (response.data.success == 1) {
					this.$toasted.success(response.data.message, { duration: 5000 });
				} else {
					this.$toasted.error(response.data.message, { duration: 5000 });
				}
				this.load();
			});
		},
		pagination(index) {
			if (index >= 0 && index <= this.totalpages) {
				this.pagecurrent = index;
				this.pagestart = (this.itemsperpage * index) - this.itemsperpage;
				this.pageend = (this.itemsperpage * index);
			}
		},
		sortBy(column) {
			if (column === this.currentSort) {
				this.currentSortDir = this.currentSortDir === 'asc' ? 'desc' : 'asc';
			}
			this.currentSort = column;
		},
	},
	computed: {
		filterListST() {
			let filterST = this.v_search_so

			let result = this.data.filter(function (e) {
				let filtered = true
				if (filterST && filterST.length > 0) {
					filtered = e.so_no.includes(filterST)
				}
				return filtered;

			}).sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});

			return result;
		},
		totalpages() {
			//This calculates the amount of pages
			return Math.ceil(this.filterListST.length / this.itemsperpage);
		},
		sortedData: function () {
			return this.data.sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});
		}
	},
	watch: {
		data: function () {
			this.pagecurrent = 1;
			this.pagestart = 0;
			this.pageend = this.itemsperpage;
		},
	}
});

Vue.component('daf_so_kirim_component', {

	// <div class="col-md-2" style="text-align: start;">
	// 	<label class="control-label text-center">Lokasi Keberangkatan</label>
	// </div>
	// <div class="col-md-2">
	// 	<v-select style="text-transform: capitalize" label="kd_off" :options="officeCode" v-model="v_office" :reduce="officeCode=>officeCode.kd_off"></v-select>
	// </div>
	// <td v-if="dt.tipe_muat === 'Langsung'">
	// 	<button type="button" class="btn btn-xs btn-primary" @click="saveSO2(dt)">BUAT DO</button>
	// </td>
	// <td v-else>
	// 	<button type="button" class="btn btn-xs btn-primary" @click="saveSO3(dt)">BUAT DO</button>
	// </td>
	// <div class="col-md-2" style="text-align: start;">
	// 	<label class="control-label text-center">Tanggal Kirim</label>
	// </div>
	// <div class="col-md-2">
	// 	<vuejs-datepicker
	// 		v-model="v_tgl_perintah_kirim"
	// 		:format="DatePickerFormatb"
	// 		:disabledDates="disabledDates"
	// 		:bootstrap-styling="true"
	// 		:placeholder="holderb"
	// 		required
	// 		style="width: 290px;">
	// 	</vuejs-datepicker>
	// </div>

	// <div class="row">
	// 	<div class="col-sm-1" v-for="date in date_range">
	// 		<button type="button" class="btn btn-xm btn-success" @click="loadDate(date)">{{date}}</button>
	// 	</div>
	// 	<div class="col-sm-3" style="text-align: end;">
	// 	</div>
	// 	<div class="col-sm-1" style="text-align: end;">
	// 		<label>Cari</label>
	// 	</div>
	// 	<div class="col-sm-2">
	// 		<input type="text" class="form-control" placeholder="No. SO" v-model="v_search_so">
	// 	</div>
	// </div>
	// </br>

	template: `
	<div>
		<div class="row">
			<!-- <div class="form-group col-md-2">
				<label class="control-label text-center">Lokasi Keberangkatan</label>
				<v-select style="text-transform: capitalize" label="kd_off" :options="officeCode" v-model="v_office" :reduce="officeCode=>officeCode.kd_off"></v-select>
			</div>
			<div class="form-group col-md-1">
				<label class="control-label text-center" style="color: white;">.</label>
				<button type="button" class="btn btn-md btn-primary" @click="add_SO">Simpan</button>
			</div>
			<div class="form-group col-md-1">
				<label class="control-label text-center" style="color: white;">.</label>
				<button type="button" class="btn btn-md btn-success" @click="loadNew"><i class="fa fa-refresh"></i></button>
			</div> -->
			<div class="form-group col-sm-2 pull-right">
				<label class="control-label text-center" style="color: white;">.</label>
				<input type="text" class="form-control" placeholder="Nomor SO" v-model="v_search_so">
			</div>
		</div>
		</br>
		<div class="row">
			<div class="col-md-12" style="text-transform: capitalize;">
				<div style="overflow-x: auto;">
					<table class="table table-hover table-responsive table-striped" style="font-size: 11px;">
						<thead>
							<tr>
								<th class="active">Comp</th>
								<th v-for="column in new_columns" @click="activeColumn = column" class="active" :style="widthHead(column.name)">
									{{ colTitles[column.name] }} 
									<span @click="column.order = column.order * (-1), sortBy(column.name)" class="arrow" :class="column.order > 0 ? 'asc' : 'dsc'" ></span>
								</th>
								<!-- <th class="active">Pilih</th> -->
							</tr>
						</thead>
						<tbody>
							<tr v-for="(dt,i) in filterListST" :key="i"  v-if="i >= pagestart && i < pageend" style="text-transform: uppercase;" 
								:style="color(dt.tanggal)">
								<td>{{ dt.code_comp }}</td>
								<td>{{ dt.tipe_muat }}</td>
								<td>{{ dt.code }}</td>
								<td>{{ dt.ledgername }}</td>
								<td>{{ dt.tanggal }} {{ dt.jaminput }}</td>
								<td>{{ dt.delivery_time }}</td>
								<td>{{ dt.sm_no }}</td>
								<td>{{ dt.so_no }}</td>
								<td>{{ dt.item_code }}</td>
								<td>{{ dt.alamat_toko }}</td>
								<td>{{ dt.keterangan }}</td>
								<td>{{ dt.sales_code }}</td> 
								<td>{{ dt.total_ton }}</td> 
								<!--<td>
									 <label class="form-checkbox">
										<input type="checkbox" :value="dt" v-model="checkedSO">
										<i class="form-icon"></i>
									</label> 
								</td>-->
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			</br></br></br>
			<div class="col-md-12" style="text-transform: capitalize; text-align: center;">
				<paginate 
					:pagecount="totalpages"
					:containerclass="'pagination pagination-sm'"
					:clickhandler="pagination"
					:prevtext="prevButtonIcon"
					:nexttext="nextButtonIcon">
				</paginate>
			</div>
		</div>
	</div>
	`,
	components: {
		vuejsDatepicker
	},
	props: {
		username: {
			required: true
		},
		loc: {
			required: true
		},
		data: {
			type: Array,
			required: true
		}
	},
	data() {
		return {
			v_tgl_perintah_kirim: moment(this.v_tgl_kirim).format("YYYY-MM-DD"),
			officeCode: [],
			current_kdOff: [],
			checkedSO: [],
			v_office: this.loc,
			v_office_current: this.loc,

			/* pagination setting */
			pagestart: 0,
			pagecurrent: 1,
			itemsperpage: 50,
			pageend: 50,
			prevButtonIcon: '<i class="fa fa-chevron-left"></i>',
			nextButtonIcon: '<i class="fa fa-chevron-right"></i>',
			/* sort setting */
			currentSort: 'name',
			currentSortDir: 'asc',
			/* table column setting */
			activeColumn: {},
			new_columns: [
				// { name: 'type', order: 1 },
				{ name: 'tipe_muat', order: 1 },
				{ name: 'code', order: 1 },
				{ name: 'ledgername', order: 1 },
				{ name: 'tanggal', order: 1 },
				{ name: 'delivery', order: 1 },
				{ name: 'sm_no', order: 1 },
				{ name: 'so_no', order: 1 },
				{ name: 'item_code', order: 1 },
				{ name: 'alamat_toko', order: 1 },
				{ name: 'keterangan', order: 1 },
				{ name: 'sales_code', order: 1 },
				{ name: 'total_ton', order: 1 },
				// { name: 'jaminput', order: 1 },
				// { name: 'code_comp', order: 1 },
				// { name: 'dept_code', order: 1 }
			],
			colTitles: {
				// 'type': 'Comp',
				'tipe_muat': 'Type',
				'code': 'Ledger',
				'ledgername': 'Customer',
				'tanggal': 'Tgl SO',
				'delivery': 'Tgl Kirim',
				'sm_no': 'No. SM',
				'so_no': 'No. SO',
				'item_code': 'Item & QTY',
				'alamat_toko': 'Alamat',
				'keterangan': 'Keterangan',
				'sales_code': 'Sales Code',
				'total_ton': 'Total Ton',
				// 'jaminput': 'Jam Buat',
				// 'code_comp': 'Comp',
				// 'dept_code': 'Dept Code'
			},
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 8640000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
			v_search_so: '',
			styleObject: {
				background: 'none',
			},
			date_range: []
		}
	},
	methods: {
		widthHead(name) {
			if (name == 'code') {
				return 'width: 6%';
			}
			if (name == 'tanggal') {
				return 'width: 9%';
			}
			if (name == 'delivery') {
				return 'width: 7%';
			}
			if (name == 'item_code') {
				return 'width: 8%';
			}
			if (name == 'sales_code') {
				return 'width: 8%';
			}
		},
		color(tgl) {
			var dateNow = new Date().getTime();
			var dateSo = new Date(tgl).getTime();
			var diffDay = parseInt((dateNow - dateSo) / (24 * 3600 * 1000));

			if (diffDay >= 3 && diffDay < 5) {
				return 'color: orange';
			} else if (diffDay >= 5) {
				return 'color: red';
			} else {
				return 'color: black';
			}
		},
		getOfficeCode() {
			axios.post(this.action, {
				case: 'getOfficeCode',
				username: this.username
			}).then(response => {
				this.officeCode = response.data.dt_kd_offifce[0];
			});
		},
		add_SO() {
			if (this.checkedSO.length > 0) {
				// if(this.v_office != this.checkedSO[0].cabang && this.v_office != this.v_office_current) {
				// 	console.log(
				// 		"cabang asal :"+this.v_office_current+","+
				// 		"cabang pilih :"+this.v_office+","+
				// 		"cabang SO :"+this.checkedSO[0].cabang+","+
				// 		"hasil : "+"pengalihan");
				// 	if(this.checkedSO.length > 1) {
				// 		swal.fire({
				// 			title: "Gagal Simpan SO",
				// 			text: "Asal SO dan Asal Gudang Berbeda Silahkan Pilih Hanya Satu SO",
				// 			icon: "error",
				// 		}).then(function (isConfirm) {
				// 			if (isConfirm) {
				// 				console.log('gagal boss');
				// 			}
				// 		});
				// 	} else {
				// 		this.saveSOpengalihan();
				// 	}
				// } else if(this.v_office != this.checkedSO[0].cabang && this.v_office == this.v_office_current) {
				// 	console.log(
				// 		"cabang asal :"+this.v_office_current+","+
				// 		"cabang pilih :"+this.v_office+","+
				// 		"cabang SO :"+this.checkedSO[0].cabang+","+
				// 		"hasil : "+"simpan DP 1");
				// 	this.saveSO();
				// } else if(this.v_office == this.checkedSO[0].cabang) {
				// 	console.log(
				// 		"cabang asal :"+this.v_office_current+","+
				// 		"cabang pilih :"+this.v_office+","+
				// 		"cabang SO :"+this.checkedSO[0].cabang+","+
				// 		"hasil : "+"simpan DP 2");
				// 	this.saveSO();
				// }
				this.saveSO();
			} else {
				swal.fire({
					title: "Gagal Simpan SO",
					text: "Silahkan Pilih SO Terlebih Dahulu",
					icon: "error",
				}).then(function (isConfirm) {
					if (isConfirm) {
						console.log('gagal boss');
					}
				});
			}
		},
		saveSO() {
			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
			}).then(response => {
				// console.log(response.data);
				// console.log("satu cabang");
				EventBus.$emit('confirmPrice_kirim', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				this.checkedSO = [];
			});
		},
		saveSO2(data) {
			let arr_so = [];
			arr_so.push(data);

			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: arr_so,
			}).then(response => {
				// console.log(response.data);
				EventBus.$emit('confirmPrice_kirim', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, arr_so);
				this.checkedSO = [];
			});
		},
		saveSO3(data) {

			// console.log(data);

			let arr_so = [];
			arr_so.push(data);

			axios.post(this.action, {
				case: 'saveDistributionPlanKirimTemp',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: arr_so,
			}).then(response => {
				// console.log(response.data[0]);
				EventBus.$emit('confirmPrice_kirim', response.data[0].data_dt, this.v_tgl_perintah_kirim, this.username, this.v_office, response.data[0].data_so);
				// EventBus.$emit('confirmPrice_kirim', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, arr_so);
				this.checkedSO = [];
			});
		},
		saveSOpengalihan() {
			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
			}).then(response => {
				// console.log(response.data);
				// console.log("pengalihan");
				EventBus.$emit('confirmPrice_kirim_pengalihan', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				this.checkedSO = [];
			});
		},
		pagination(index) {
			if (index >= 0 && index <= this.totalpages) {
				this.pagecurrent = index;
				this.pagestart = (this.itemsperpage * index) - this.itemsperpage;
				this.pageend = (this.itemsperpage * index);
			}
		},
		sortBy(column) {
			if (column === this.currentSort) {
				this.currentSortDir = this.currentSortDir === 'asc' ? 'desc' : 'asc';
			}
			this.currentSort = column;
		},
		getNow() {
			const today = new Date();
			const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
			const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			const dateTime = date + ' ' + time;
			this.v_tgl_perintah_kirim = dateTime;
		},
		getDateRange() {
			axios.post(this.action, {
				case: 'getDatesFromRange'
			}).then(response => {
				this.date_range = response.data;
			});
		},
		loadDate(date) {
			EventBus.$emit('refresh_req_date', date);
		},
		loadNew() {
			location.reload();
		}
	},
	created() {
		this.getOfficeCode();
		this.getNow();
		this.getDateRange();
	},
	computed: {
		filterListST() {
			let filterST = this.v_search_so

			let result = this.data.filter(function (e) {
				let filtered = true
				if (filterST && filterST.length > 0) {
					filtered = e.so_no.includes(filterST)
				}
				return filtered;

			}).sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});

			return result;
		},
		totalpages() {
			//This calculates the amount of pages
			return Math.ceil(this.filterListST.length / this.itemsperpage);
		},
		sortedData: function () {
			return this.data.sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});
		}
	},
	watch: {
		data: function () {
			this.pagecurrent = 1;
			this.pagestart = 0;
			this.pageend = this.itemsperpage;
		},
	}

});

Vue.component('daf_so_kirim_new', {

	props: ['data_less', 'data_more', 'username', 'loc'],
	template: `<div>
		<div class="row">
			<div class="col-md-12">
				<tabs_distribution_plan_component>
					<tab_distribution_plan_component v-for="(dt,i) in tabs" :key="i" :name="dt.name" :icon="dt.icon" :selected="i === 0 ? 'true' : ''">
						<div v-if="i == 0">
							<div class="lds-facebook" v-if="loading"><div></div><div></div><div></div></div>
							<!-- <v-client-table style="font-size: 11px;" :data="data_more" :options="optionsmore" :columns="coloumn_more"></v-client-table> -->
							<daf_so_kirim_new_more :data="data_more" :username="username" :loc="loc"></daf_so_kirim_new_more>
						</div>
						<div v-if="i == 1">
							<div class="lds-facebook" v-if="loading"><div></div><div></div><div></div></div>
							<!-- <v-client-table style="font-size: 11px;" :data="data_less" :options="optionsless" :columns="coloumn_less"></v-client-table> -->
							<daf_so_kirim_new_less :data="data_less" :username="username" :loc="loc"></daf_so_kirim_new_less>
						</div>
					</tab_distribution_plan_component>
				</tabs_distribution_plan_component>
			</div>
		</div>
	</div>
	`,
	data() {
		return {
			loading: false,
			tabs: [
				{
					name: 'DAFTAR SO > 10 TON',
					icon: '<i class="fa fa-print"></i>',
					counter: 0,
				},
				{
					name: 'DAFTAR SO < 10 TON',
					icon: '<i class="fa fa-print"></i>',
					counter: 0,
				},
			],
			coloumn_more: [
				'tipe_muat',
				'code',
				'ledgername',
				'tanggal',
				'delivery',
				'sm_no',
				'so_no',
				'item_code',
				'alamat_toko',
				'keterangan',
				'sales_code',
				'ton',
			],
			optionsmore: {
				perPage: 100,
				headings: {
					ledgername: 'Nama',
					sm_no: 'SM',
					so_no: 'SO',
				},
				sortable: [],
			},
			coloumn_less: [
				'tipe_muat',
				'code',
				'ledgername',
				'tanggal',
				'delivery',
				'sm_no',
				'so_no',
				'item_code',
				'alamat_toko',
				'keterangan',
				'sales_code',
				'ton',
			],
			optionsless: {
				perPage: 100,
				sortable: [],
				headings: {
					ledgername: 'Nama',
					sm_no: 'SM',
					so_no: 'SO',
				},
			}
		}
	}

});

Vue.component('daf_so_kirim_new_more', {

	template:
		`
	<div>
		<!-- <div class="row">
			<div class="col-md-2" style="text-align: start;">
				<label class="control-label text-center">Lokasi Ambil</label>
			</div>
		</div> -->
		<div class="row">
			<!-- <div class="col-md-2">
				<v-select style="text-transform: capitalize" label="kd_off" :options="officeCode" v-model="v_office" :reduce="officeCode=>officeCode.kd_off"></v-select>
			</div>
			<div class="col-md-2">
				<vuejs-datepicker
					v-model="v_tgl_perintah_kirim"
					:format="DatePickerFormatb"
					:disabledDates="disabledDates"
					:bootstrap-styling="true"
					:placeholder="holderb"
					required
					style="width: 290px;">
				</vuejs-datepicker>
			</div>
			<div class="col-md-1">
				<button type="button" class="btn btn-xm btn-primary" @click="add_SO">Simpan</button>
			</div> -->
			<!-- <div class="col-md-1">
				<button type="button" class="btn btn-xm btn-success" @click="load2"><i class="fa fa-refresh"></i></button>
			</div> -->
		</div>
		</br></br>
		<div class="row">
			<div class="col-md-1">
				<button type="button" class="btn btn-xm btn-success" @click="load2"><i class="fa fa-refresh"></i></button>
			</div>
			<div class="col-sm-9" style="text-align: end;">
				<label>Cari</label>
			</div>
			<div class="col-sm-2">
				<input type="text" class="form-control" placeholder="No. SO" v-model="v_search_so">
			</div>
		</div>
		</br>
		<div class="row">
			<div class="col-md-12" style="text-transform: capitalize;">
				<table class="table table-hover table-responsive table-striped" style="font-size: 12px;">
					<thead>
						<tr>
							<th v-for="column in new_columns" @click="activeColumn = column" class="active">
								{{ colTitles[column.name] }} 
								<span @click="column.order = column.order * (-1), sortBy(column.name)" class="arrow" :class="column.order > 0 ? 'asc' : 'dsc'" ></span>
							</th>
							<th class="active">Ton</th>
							<!-- <th class="active">Pilih</th> -->
						</tr>
					</thead>
					<tbody>
						<tr v-for="(dt,i) in filterListST" :key="i"  v-if="i >= pagestart && i < pageend" style="text-transform: uppercase;"
							:style="color(dt.tanggal)">
							<td>{{ dt.code_comp }}</td>
							<td>{{ dt.code }}</td>
							<td style="width: 10%;">{{ dt.ledgername }}</td>
							<td style="width: 10%;">{{ dt.tanggal }} {{ dt.jaminput }}</td>
							<td style="width: 10%;">{{ dt.delivery_time }}</td>
							<td style="width: 5%;">{{ dt.sm_no }}</td>
							<td>{{ dt.so_no }}</td>
							<td style="width: 13%;">{{ dt.item_code }}</td>
							<td>{{ dt.alamat_toko }}</td>
							<td>{{ dt.keterangan }}</td>
							<td>{{ dt.sales_code }}</td>
							<td>{{ dt.ton }}</td>
							<!-- <td>
								<label class="form-checkbox">
									<input type="checkbox" :value="dt" v-model="checkedSO">
									<i class="form-icon"></i>
								</label>
							</td> -->
						</tr>
					</tbody>
				</table>
			</div>
			</br></br></br>
			<div class="col-md-12" style="text-transform: capitalize; text-align: center;">
			<paginate 
				:pagecount="totalpages"
				:containerclass="'pagination pagination-sm'"
				:clickhandler="pagination"
				:prevtext="prevButtonIcon"
				:nexttext="nextButtonIcon">
			</paginate>
			</div>
		</div>
	</div>
	`,
	components: {
		vuejsDatepicker
	},
	props: {
		username: {
			required: true
		},
		loc: {
			required: true
		},
		data: {
			type: Array,
			required: true
		},
	},
	data() {
		return {
			v_tgl_perintah_kirim: moment(this.v_tgl_kirim).format("YYYY-MM-DD"),
			officeCode: [],
			current_kdOff: [],
			checkedSO: [],
			v_office: this.loc,
			v_office_current: this.loc,
			columns: ['type', 'code', 'ledgername', 'tanggal', 'sm_no', 'so_no',
				'item_code', 'alamat_toko', 'keterangan', 'jaminput', 'code_comp', 'sales_code', 'status_so', 'dept_code', 'pilih'],
			pagestart: 0,
			pagecurrent: 1,
			itemsperpage: 100,
			pageend: 100,
			prevButtonIcon: '<i class="fa fa-chevron-left"></i>',
			nextButtonIcon: '<i class="fa fa-chevron-right"></i>',
			currentSort: 'name',
			currentSortDir: 'asc',
			activeColumn: {},
			new_columns: [
				{ name: 'type', order: 1 },
				{ name: 'code', order: 1 },
				{ name: 'ledgername', order: 1 },
				{ name: 'tanggal', order: 1 },
				{ name: 'delivery', order: 1 },
				{ name: 'sm_no', order: 1 },
				{ name: 'so_no', order: 1 },
				{ name: 'item_code', order: 1 },
				{ name: 'alamat_toko', order: 1 },
				{ name: 'keterangan', order: 1 },
				{ name: 'sales_code', order: 1 },
			],
			colTitles: {
				'type': 'Type',
				'code': 'Ledger',
				'ledgername': 'Customer',
				'tanggal': 'Tgl. SO / SM',
				'delivery': 'Tgl. Kirim',
				'sm_no': 'No. SM',
				'so_no': 'No. SO',
				'item_code': 'Item & QTY',
				'alamat_toko': 'Alamat',
				'keterangan': 'Keterangan',
				'sales_code': 'Sales Code',
			},
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 8640000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
			v_search_so: '',
			styleObject: {
				background: 'none',
			}
		}
	},
	methods: {
		getOfficeCode() {
			axios.post(this.action, {
				case: 'getOfficeCode',
				username: 'rachmadi'
			}).then(response => {
				this.officeCode = response.data.dt_kd_offifce[0];
			});
		},
		add_SO() {
			//gantung
			var total_ton = 0;

			if (this.checkedSO.length > 0) {
				this.saveSO();
			} else {
				swal.fire({
					title: "Gagal Simpan SO",
					text: "Silahkan Pilih SO Terlebih Dahulu",
					icon: "error",
				}).then(function (isConfirm) {
					if (isConfirm) {
						console.log('gagal boss');
					}
				});
			}
		},
		saveSO2() {
			axios.post(this.action, {
				case: 'saveDistributionPlanTemp',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO
			}).then(response => {
				console.log(response.data);
				if (response.data.success == 1) {
					this.$toasted.success(response.data.response, { duration: 5000 });
				} else {
					this.$toasted.error(response.data.response, { duration: 5000 });
				}
				// console.log("satu cabang");
				// EventBus.$emit('confirmPrice_kirim', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				EventBus.$emit('refresh_so_sc');
				this.checkedSO = [];
			});
		},
		saveSO() {
			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
			}).then(response => {
				// console.log(response.data);
				EventBus.$emit('confirmPrice_kirim', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				this.checkedSO = [];
			});
		},
		saveSOpengalihan() {
			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
			}).then(response => {
				// console.log(response.data);
				// console.log("pengalihan");
				EventBus.$emit('confirmPrice_kirim_pengalihan', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				this.checkedSO = [];
			});
		},
		pagination(index) {
			if (index >= 0 && index <= this.totalpages) {
				this.pagecurrent = index;
				this.pagestart = (this.itemsperpage * index) - this.itemsperpage;
				this.pageend = (this.itemsperpage * index);
			}
		},
		sortBy(column) {
			if (column === this.currentSort) {
				this.currentSortDir = this.currentSortDir === 'asc' ? 'desc' : 'asc';
			}
			this.currentSort = column;
		},
		diffDay(dateSo) {
			var dateNow = new Date().getTime();
			var dateSo = new Date(dateSo).getTime();
			var diffDay = parseInt((dateNow - dateSo) / (24 * 3600 * 1000));
			return diffDay;
		},
		getNow() {
			const today = new Date();
			const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
			const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			const dateTime = date + ' ' + time;
			this.v_tgl_perintah_kirim = dateTime;
		},
		send_mail_approve(data) {
			// console.log(data);
			this.loading = true;
			axios.post(this.action, {
				case: 'send_mail_approve',
				dt_so: data,
			}).then(response => {
				console.log(response.data);
				this.loading = false;
				this.$toasted.success(response.data, { duration: 5000 });
				// console.log("satu cabang");
				this.checkedSO = [];
			});
		},
		load2() {
			EventBus.$emit('refresh_so_sc');
		},
		load() {
			// console.log(this.data);
			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.data,
			}).then(response => {
				// console.log(response.data);
				EventBus.$emit('confirmPrice_kirim_new', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.data);
				this.checkedSO = [];
			});
		},
		color(tgl) {
			var dateNow = new Date().getTime();
			var dateSo = new Date(tgl).getTime();
			var diffDay = parseInt((dateNow - dateSo) / (24 * 3600 * 1000));

			if (diffDay >= 3 && diffDay < 5) {
				return 'color: orange';
			} else if (diffDay >= 5) {
				return 'color: red';
			} else {
				return 'color: black';
			}
		},
	},
	created() {
		this.getOfficeCode();
		this.getNow();
	},
	computed: {
		filterListST() {
			let filterST = this.v_search_so

			let result = this.data.filter(function (e) {
				let filtered = true
				if (filterST && filterST.length > 0) {
					filtered = e.so_no.includes(filterST)
				}
				return filtered;

			}).sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});

			return result;
		},
		totalpages() {
			//This calculates the amount of pages
			return Math.ceil(this.filterListST.length / this.itemsperpage);
		},
		sortedData: function () {
			return this.data.sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});
		}
	},
	watch: {
		data: function () {
			this.pagecurrent = 1;
			this.pagestart = 0;
			this.pageend = this.itemsperpage;
		},
	}

});

Vue.component('daf_so_kirim_new_less', {

	template:
		`
	<div>
		<!-- <div class="row">
			<div class="col-md-2" style="text-align: start;">
				<label class="control-label text-center">Lokasi Ambil</label>
			</div>
			<div class="col-md-2" style="text-align: start;">
				<label class="control-label text-center">Tanggal Kirim</label>
			</div>
		</div> -->
		<div class="row">
			<!-- <div class="col-md-2">
				<v-select style="text-transform: capitalize" label="kd_off" :options="officeCode" v-model="v_office" :reduce="officeCode=>officeCode.kd_off"></v-select>
			</div>
			<div class="col-md-2">
				<vuejs-datepicker
					v-model="v_tgl_perintah_kirim"
					:format="DatePickerFormatb"
					:disabledDates="disabledDates"
					:bootstrap-styling="true"
					:placeholder="holderb"
					required
					style="width: 290px;">
				</vuejs-datepicker>
			</div>
			<div class="col-md-1">
				<button type="button" class="btn btn-xm btn-primary" @click="add_SO">Simpan</button>
			</div> -->
			<!-- <div class="col-md-1">
				<button type="button" class="btn btn-xm btn-success" @click="load"><i class="fa fa-refresh"></i></button>
			</div> -->
		</div>
		</br></br>
		<div class="row">
			<div class="col-sm-1">
				<button type="button" class="btn btn-xm btn-success" @click="load"><i class="fa fa-refresh"></i></button>
			</div>
			<div class="col-sm-9" style="text-align: end;">
				<label>Cari</label>
			</div>
			<div class="col-sm-2">
				<input type="text" class="form-control" placeholder="No. SO" v-model="v_search_so">
			</div>
		</div>
		</br>
		<div class="row">
			<div class="col-md-12" style="text-transform: capitalize;">
				<table class="table table-hover table-responsive table-striped" style="font-size: 12px;">
					<thead>
						<tr>
							<th v-for="column in new_columns" @click="activeColumn = column" class="active">
								{{ colTitles[column.name] }} 
								<span @click="column.order = column.order * (-1), sortBy(column.name)" class="arrow" :class="column.order > 0 ? 'asc' : 'dsc'" ></span>
							</th>
							<th class="active">Ton</th>
							<!-- <th class="active">Pilih</th> -->
						</tr>
					</thead>
					<tbody>
						<tr v-for="(dt,i) in filterListST" :key="i"  v-if="i >= pagestart && i < pageend" style="text-transform: uppercase;"
							:style="color(dt.tanggal)">
							<td>{{ dt.code_comp }}</td>
							<td>{{ dt.code }}</td>
							<td style="width: 10%;">{{ dt.ledgername }}</td>
							<td style="width: 10%;">{{ dt.tanggal }} {{ dt.jaminput }}</td>
							<td style="width: 10%;">{{ dt.delivery_time }}</td>
							<td style="width: 5%;">{{ dt.sm_no }}</td>
							<td>{{ dt.so_no }}</td>
							<td style="width: 13%;">{{ dt.item_code }}</td>
							<td>{{ dt.alamat_toko }}</td>
							<td>{{ dt.keterangan }}</td>
							<td>{{ dt.sales_code }}</td>
							<td>{{ dt.ton }}</td>
							<!-- <td>
								<label class="form-checkbox">
									<input type="checkbox" :value="dt" v-model="checkedSO">
									<i class="form-icon"></i>
								</label>
							</td> -->
						</tr>
					</tbody>
				</table>
			</div>
			</br></br></br>
			<div class="col-md-12" style="text-transform: capitalize; text-align: center;">
			<paginate 
				:pagecount="totalpages"
				:containerclass="'pagination pagination-sm'"
				:clickhandler="pagination"
				:prevtext="prevButtonIcon"
				:nexttext="nextButtonIcon">
			</paginate>
			</div>
		</div>
	</div>
	`,
	components: {
		vuejsDatepicker
	},
	props: {
		username: {
			required: true
		},
		loc: {
			required: true
		},
		data: {
			type: Array,
			required: true
		},
	},
	data() {
		return {
			v_tgl_perintah_kirim: moment(this.v_tgl_kirim).format("YYYY-MM-DD"),
			officeCode: [],
			current_kdOff: [],
			checkedSO: [],
			v_office: this.loc,
			v_office_current: this.loc,
			columns: ['type', 'code', 'ledgername', 'tanggal', 'sm_no', 'so_no',
				'item_code', 'alamat_toko', 'keterangan', 'jaminput', 'code_comp', 'sales_code', 'status_so', 'dept_code', 'pilih'],
			pagestart: 0,
			pagecurrent: 1,
			itemsperpage: 100,
			pageend: 100,
			prevButtonIcon: '<i class="fa fa-chevron-left"></i>',
			nextButtonIcon: '<i class="fa fa-chevron-right"></i>',
			currentSort: 'name',
			currentSortDir: 'asc',
			activeColumn: {},
			new_columns: [
				{ name: 'type', order: 1 },
				{ name: 'code', order: 1 },
				{ name: 'ledgername', order: 1 },
				{ name: 'tanggal', order: 1 },
				{ name: 'delivery', order: 1 },
				{ name: 'sm_no', order: 1 },
				{ name: 'so_no', order: 1 },
				{ name: 'item_code', order: 1 },
				{ name: 'alamat_toko', order: 1 },
				{ name: 'keterangan', order: 1 },
				{ name: 'sales_code', order: 1 },
			],
			colTitles: {
				'type': 'Type',
				'code': 'Ledger',
				'ledgername': 'Customer',
				'tanggal': 'Tgl. SO / SM',
				'delivery': 'Tgl. Kirim',
				'sm_no': 'No. SM',
				'so_no': 'No. SO',
				'item_code': 'Item & QTY',
				'alamat_toko': 'Alamat',
				'keterangan': 'Keterangan',
				'sales_code': 'Sales Code',
			},
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 8640000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
			v_search_so: '',
			styleObject: {
				background: 'none',
			}
		}
	},
	methods: {
		getOfficeCode() {
			axios.post(this.action, {
				case: 'getOfficeCode',
				username: 'rachmadi'
			}).then(response => {
				this.officeCode = response.data.dt_kd_offifce[0];
			});
		},
		add_SO() {
			//gantung
			var total_ton = 0;

			if (this.checkedSO.length > 0) {
				this.saveSO();
			} else {
				swal.fire({
					title: "Gagal Simpan SO",
					text: "Silahkan Pilih SO Terlebih Dahulu",
					icon: "error",
				}).then(function (isConfirm) {
					if (isConfirm) {
						console.log('gagal boss');
					}
				});
			}
		},
		saveSO2() {
			axios.post(this.action, {
				case: 'saveDistributionPlanTemp',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO
			}).then(response => {
				console.log(response.data);
				if (response.data.success == 1) {
					this.$toasted.success(response.data.response, { duration: 5000 });
				} else {
					this.$toasted.error(response.data.response, { duration: 5000 });
				}
				// console.log("satu cabang");
				// EventBus.$emit('confirmPrice_kirim', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				EventBus.$emit('refresh_so_sc');
				this.checkedSO = [];
			});
		},
		saveSO() {
			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
			}).then(response => {
				// console.log(response.data);
				EventBus.$emit('confirmPrice_kirim', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				this.checkedSO = [];
			});
		},
		saveSOpengalihan() {
			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
			}).then(response => {
				// console.log(response.data);
				// console.log("pengalihan");
				EventBus.$emit('confirmPrice_kirim_pengalihan', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				this.checkedSO = [];
			});
		},
		pagination(index) {
			if (index >= 0 && index <= this.totalpages) {
				this.pagecurrent = index;
				this.pagestart = (this.itemsperpage * index) - this.itemsperpage;
				this.pageend = (this.itemsperpage * index);
			}
		},
		sortBy(column) {
			if (column === this.currentSort) {
				this.currentSortDir = this.currentSortDir === 'asc' ? 'desc' : 'asc';
			}
			this.currentSort = column;
		},
		diffDay(dateSo) {
			var dateNow = new Date().getTime();
			var dateSo = new Date(dateSo).getTime();
			var diffDay = parseInt((dateNow - dateSo) / (24 * 3600 * 1000));
			return diffDay;
		},
		getNow() {
			const today = new Date();
			const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
			const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			const dateTime = date + ' ' + time;
			this.v_tgl_perintah_kirim = dateTime;
		},
		send_mail_approve(data) {
			// console.log(data);
			this.loading = true;
			axios.post(this.action, {
				case: 'send_mail_approve',
				dt_so: data,
			}).then(response => {
				console.log(response.data);
				this.loading = false;
				this.$toasted.success(response.data, { duration: 5000 });
				// console.log("satu cabang");
				this.checkedSO = [];
			});
		},
		load() {
			EventBus.$emit('refresh_so_sc');
		},
		color(tgl) {
			var dateNow = new Date().getTime();
			var dateSo = new Date(tgl).getTime();
			var diffDay = parseInt((dateNow - dateSo) / (24 * 3600 * 1000));

			if (diffDay >= 3 && diffDay < 5) {
				return 'color: orange';
			} else if (diffDay >= 5) {
				return 'color: red';
			} else {
				return 'color: black';
			}
		},
	},
	created() {
		this.getOfficeCode();
		this.getNow();
	},
	computed: {
		filterListST() {
			let filterST = this.v_search_so

			let result = this.data.filter(function (e) {
				let filtered = true
				if (filterST && filterST.length > 0) {
					filtered = e.so_no.includes(filterST)
				}
				return filtered;

			}).sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});

			return result;
		},
		totalpages() {
			//This calculates the amount of pages
			return Math.ceil(this.filterListST.length / this.itemsperpage);
		},
		sortedData: function () {
			return this.data.sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});
		}
	},
	watch: {
		data: function () {
			this.pagecurrent = 1;
			this.pagestart = 0;
			this.pageend = this.itemsperpage;
		},
	}

});

Vue.component('daf_so_ambil_component', {

	// <div class="row">
	// 	<div class="col-md-2" style="text-align: start;">
	// 		<label class="control-label text-center">Lokasi Ambil</label>
	// 	</div>
	// 	<div class="col-md-2" style="text-align: start;">
	// 		<label class="control-label text-center">Tanggal Ambil</label>
	// 	</div>
	// </div>

	// <div class="row">
	// 	<div class="col-md-2">
	// 		<v-select style="text-transform: capitalize" label="kd_off" :options="officeCode" v-model="v_office" :reduce="officeCode=>officeCode.kd_off"></v-select>
	// 	</div>
	// 	<div class="col-md-2">
	// 		<vuejs-datepicker
	// 			v-model="v_tgl_perintah_kirim"
	// 			:format="DatePickerFormatb"
	// 			:disabledDates="disabledDates"
	// 			:bootstrap-styling="true"
	// 			:placeholder="holderb"
	// 			required
	// 			style="width: 290px;">
	// 		</vuejs-datepicker>
	// 	</div>
	// 	<div class="col-md-1">
	// 		<button type="button" class="btn btn-xm btn-primary" @click="add_SO">Simpan</button>
	// 	</div>
	// 	<div class="col-md-1">
	// 		<button type="button" class="btn btn-xm btn-success" @click="loadNew"><i class="fa fa-refresh"></i></button>
	// 	</div>
	// </div>
	// </br></br></br></br>

	// <label class="form-checkbox">
	// 	<input type="checkbox" :value="dt" v-model="checkedSO">
	// 	<i class="form-icon"></i>
	// </label>
	template: `
	<div>
		<!-- <div class="row">
			<div class="form-group col-md-2" style="margin-top: 25px;">
				<button type="button" class="btn btn-xm btn-success" @click="loadNew"><i class="fa fa-refresh"></i></button>
			</div>
			<div class="form-group col-sm-2 pull-right">
				<label>Cari</label>
				<input type="text" class="form-control" placeholder="No. SO" v-model="v_search_so">
			</div>
		</div> -->
		</br>
		<div class="row">
			<div class="col-md-12" style="text-transform: capitalize;">
				<table class="table table-hover table-responsive table-striped" style="font-size: 12px;">
					<thead>
						<tr>
							<th class="active">Comp</th>
							<th v-for="column in new_columns" @click="activeColumn = column" class="active" :style="widthHead(column.name)">
								{{ colTitles[column.name] }} 
								<span @click="column.order = column.order * (-1), sortBy(column.name)" class="arrow" :class="column.order > 0 ? 'asc' : 'dsc'" ></span>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr v-for="(dt,i) in filterListST" :key="i"  v-if="i >= pagestart && i < pageend" style="text-transform: uppercase;">
							<td>{{ dt.code_comp }}</td>
							<td>{{ dt.code }}</td>
							<td style="width: 10%;">{{ dt.ledgername }}</td> 
							<td style="width: 10%;">{{ dt.tanggal }} {{ dt.jaminput }}</td> 
							<td style="width: 10%;">{{ dt.delivery_time }}</td> 
							<td style="width: 5%;">{{ dt.sm_no }}</td>
							<td>{{ dt.so_no }}</td>
							<td style="width: 13%;">{{ dt.item_code }}</td>
							<td>{{ dt.alamat_toko }}</td>
							<td>{{ dt.keterangan }}</td>
							<!--<td>
								 <button type="button" class="btn btn-xs btn-primary" @click="saveSO2(dt)">Buat DO</button> 
							</td>-->
						</tr>
					</tbody>
				</table>
			</div>
			</br></br></br>
			<div class="col-md-12" style="text-transform: capitalize; text-align: center;">
				<paginate 
					:pagecount="totalpages"
					:containerclass="'pagination pagination-sm'"
					:clickhandler="pagination"
					:prevtext="prevButtonIcon"
					:nexttext="nextButtonIcon">
				</paginate>
			</div>
		</div>
	</div>
	`,
	components: {
		vuejsDatepicker
	},
	props: {
		username: {
			required: true
		},
		loc: {
			required: true
		},
		data: {
			type: Array,
			required: true,
		}
	},
	data() {
		return {
			v_tgl_perintah_kirim: '',
			checkedSO: [],
			officeCode: [],
			v_office: this.loc,
			// v_office: "",
			v_office_current: this.loc,
			columns: ['type', 'code', 'ledgername', 'tanggal', 'sm_no', 'so_no',
				'item_code', 'alamat_toko', 'keterangan', 'jaminput', 'pilih'],
			// options: {
			// 	perPage: 50,
			// 	sortable: ['type', 'code', 'ledgername'],
			// 	headings: {
			// 		type: 'Type',
			// 		code: 'Ledger',
			// 		ledgername: 'Customer',
			// 		tanggal: 'Tgl. SO',
			// 		sm_no: 'No. SM',
			// 		so_no: 'No. SO',
			// 		item_code: 'Item & Qty',
			// 		alamat_toko: 'Alamat',
			// 		keterangan: 'Keterangan',
			// 		jaminput: 'Jam Buat'
			// 	},
			// 	templates: {
			// 		pilih: 'pilih-so-ambil-component',
			// 	},
			// 	sortIcon: {
			// 		base : 'fa',
			// 		is: 'fa-sort',
			// 		up: 'fa-sort-asc',
			// 		down: 'fa-sort-desc'
			// 	}
			// },
			/* pagination setting */
			pagestart: 0,
			pagecurrent: 1,
			itemsperpage: 50,
			pageend: 50,
			prevButtonIcon: '<i class="fa fa-chevron-left"></i>',
			nextButtonIcon: '<i class="fa fa-chevron-right"></i>',
			/* sort setting */
			currentSort: 'name',
			currentSortDir: 'asc',
			/* table column setting */
			activeColumn: {},
			new_columns: [
				// { name: 'type', order: 1 },
				{ name: 'code', order: 1 },
				{ name: 'ledgername', order: 1 },
				{ name: 'tanggal', order: 1 },
				{ name: 'delivery_time', order: 1 },
				{ name: 'sm_no', order: 1 },
				{ name: 'so_no', order: 1 },
				{ name: 'item_code', order: 1 },
				{ name: 'alamat_toko', order: 1 },
				{ name: 'keterangan', order: 1 },
				// { name: 'pilih', order: 1 },
				// { name: 'debt_code', order: 1 },
			],
			colTitles: {
				// 'type': 'Type',
				'code': 'Ledger',
				'ledgername': 'Customer',
				'tanggal': 'Tanggal',
				'delivery_time': 'Tgl. Ambil',
				'sm_no': 'No. SM',
				'so_no': 'No. SO',
				'item_code': 'Item & QTY',
				'alamat_toko': 'Alamat',
				'keterangan': 'Keterangan',
				// 'pilih': 'Pilih',
				// 'debt_code': 'Dect Code'
			},
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 8640000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
			v_search_so: ''
		}
	},
	methods: {
		widthHead(name) {
			if (name == 'code') {
				return 'width: 7%';
			}
		},
		color(tgl) {
			var dateNow = new Date().getTime();
			var dateSo = new Date(tgl).getTime();
			var diffDay = parseInt((dateNow - dateSo) / (24 * 3600 * 1000));

			if (diffDay >= 3 && diffDay < 5) {
				return 'color: orange';
			} else if (diffDay >= 5) {
				return 'color: red';
			} else {
				return 'color: black';
			}
		},
		getOfficeCode() {
			axios.post(this.action, {
				case: 'getOfficeCode',
				username: this.username
			}).then(response => {
				this.officeCode = response.data.dt_kd_offifce[0];
			});
		},
		add_SO() {
			if (this.checkedSO.length > 0) {
				// if(this.v_office != this.checkedSO[0].cabang && this.v_office != this.v_office_current) {
				// 	console.log(
				// 		"cabang asal :"+this.v_office_current+","+
				// 		"cabang pilih :"+this.v_office+","+
				// 		"cabang SO :"+this.checkedSO[0].cabang+","+
				// 		"hasil : "+"pengalihan");
				// 	if(this.checkedSO.length > 1) {
				// 		swal.fire({
				// 			title: "Gagal Simpan SO",
				// 			text: "Asal SO dan Asal Gudang Berbeda Silahkan Pilih Hanya Satu SO",
				// 			icon: "error",
				// 		}).then(function (isConfirm) {
				// 			if (isConfirm) {
				// 				console.log('gagal boss');
				// 			}
				// 		});
				// 	} else {
				// 		this.saveSOpengalihan();
				// 	}
				// } else if(this.v_office != this.checkedSO[0].cabang && this.v_office == this.v_office_current) {
				// 	console.log(
				// 		"cabang asal :"+this.v_office_current+","+
				// 		"cabang pilih :"+this.v_office+","+
				// 		"cabang SO :"+this.checkedSO[0].cabang+","+
				// 		"hasil : "+"simpan DP 1");
				// 	this.saveSO();
				// } else if(this.v_office == this.checkedSO[0].cabang) {
				// 	console.log(
				// 		"cabang asal :"+this.v_office_current+","+
				// 		"cabang pilih :"+this.v_office+","+
				// 		"cabang SO :"+this.checkedSO[0].cabang+","+
				// 		"hasil : "+"simpan DP 2");
				// 	this.saveSO();
				// }
				this.saveSO();
			} else {
				swal.fire({
					title: "Gagal Simpan SO",
					text: "Silahkan Pilih SO Terlebih Dahulu",
					icon: "error",
				}).then(function (isConfirm) {
					if (isConfirm) {
						console.log('gagal boss');
					}
				});
			}
		},
		saveSO() {
			axios.post(this.action, {
				case: 'saveDistributionPlanAmbil',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
			}).then(response => {
				// console.log(response.data);
				EventBus.$emit('confirmPrice_ambil', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				this.checkedSO = [];
			});
		},
		saveSO2(data) {

			let arr_so = [];
			arr_so.push(data);

			axios.post(this.action, {
				case: 'saveDistributionPlanAmbil',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: arr_so,
			}).then(response => {
				// console.log(response.data);
				EventBus.$emit('confirmPrice_ambil', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, arr_so);
				this.checkedSO = [];
			});
		},
		saveSOpengalihan() {
			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
			}).then(response => {
				// console.log(response.data);
				// console.log("pengalihan");
				EventBus.$emit('confirmPrice_ambil_pengalihan', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				this.checkedSO = [];
			});
		},
		pagination(index) {
			//This calculates the range of data to show depending on the selected page
			if (index >= 0 && index <= this.totalpages) {
				this.pagecurrent = index;
				this.pagestart = (this.itemsperpage * index) - this.itemsperpage;
				this.pageend = (this.itemsperpage * index);
			}
		},
		getNow() {
			const today = new Date();
			const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
			const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			const dateTime = date + ' ' + time;
			this.v_tgl_perintah_kirim = dateTime;
		},
		loadNew() {
			// location.reload();
			EventBus.$emit('refreshSoAmbil');
		}
	},
	created() {
		this.getOfficeCode();
		EventBus.$on('addItemSOambil', (data) => {
			this.checkedSO = data;
		});
		this.getNow();
		EventBus.$on('loadSOAmbil', (tanggal) => {
			// console.log(tanggal);
			let tgl = tanggal;
			EventBus.$emit('loadSOByTanggal', tgl);
		});
	},
	computed: {
		filterListST() {
			let filterST = this.v_search_so

			let result = this.data.filter(function (e) {
				let filtered = true
				if (filterST && filterST.length > 0) {
					filtered = e.so_no.includes(filterST)
				}
				return filtered;

			}).sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});

			return result;
		},
		totalpages() {
			//This calculates the amount of pages
			return Math.ceil(this.data.length / this.itemsperpage);
		},
		sortedData: function () {
			return this.data.sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});
		}
	},
	watch: {
		data: function () {
			this.pagecurrent = 1;
			this.pagestart = 0;
			this.pageend = this.itemsperpage;
		},
	}

});

Vue.component('daf_so_ambil_component_new', {

	template: `
	<div>
		<div class="row">
			<!-- <div class="col-md-2 form-group">
				<label>Tgl SO</label>
				<vuejs-datepicker
					v-model="v_tgl_sort_ambil"
					:format="DatePickerFormat"
					:bootstrap-styling="true"
					@input="sortData(v_tgl_sort_ambil)">
				</vuejs-datepicker>
			</div> -->
			<div class="form-group col-md-2" style="margin-top: 25px;">
				<button type="button" class="btn btn-xm btn-success" @click="loadNew"><i class="fa fa-refresh"></i></button>
			</div>
			<div class="form-group col-sm-2 pull-right">
				<label>Cari</label>
				<input type="text" class="form-control" placeholder="No. SO" v-model="v_search_so">
			</div>
		</div>
		</br>
		<div class="row">
			<div class="col-md-12" style="text-transform: capitalize;">
				<table class="table table-hover table-responsive table-striped" style="font-size: 12px;">
					<thead>
						<tr>
							<th class="active">Comp</th>
							<th v-for="column in new_columns" @click="activeColumn = column" class="active" :style="widthHead(column.name)">
								{{ colTitles[column.name] }} 
								<span @click="column.order = column.order * (-1), sortBy(column.name)" class="arrow" :class="column.order > 0 ? 'asc' : 'dsc'" ></span>
							</th>
							<th class="active">Pilih</th>
						</tr>
					</thead>
					<tbody>
						<tr v-for="(dt,i) in filterListST" :key="i"  v-if="i >= pagestart && i < pageend" style="text-transform: uppercase;">
							<td>{{ dt.code_comp }}</td>
							<td>{{ dt.code }}</td>
							<td style="width: 10%;">{{ dt.ledgername }}</td> 
							<td style="width: 10%;">{{ dt.tanggal }} {{ dt.jaminput }}</td> 
							<td style="width: 10%;">{{ dt.delivery_time }}</td> 
							<td style="width: 5%;">{{ dt.sm_no }}</td>
							<td>{{ dt.so_no }}</td>
							<td style="width: 13%;">{{ dt.item_code }}</td>
							<td>{{ dt.alamat_toko }}</td>
							<td>{{ dt.keterangan }}</td>
							<td>
								<button type="button" class="btn btn-xs btn-primary" @click="saveSO2(dt)">Buat DO</button> 
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			</br></br></br>
			<div class="col-md-12" style="text-transform: capitalize; text-align: center;">
				<paginate 
					:pagecount="totalpages"
					:containerclass="'pagination pagination-sm'"
					:clickhandler="pagination"
					:prevtext="prevButtonIcon"
					:nexttext="nextButtonIcon">
				</paginate>
			</div>
		</div>
		<div class="modal fade" id="modal_rencana_ambil" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<div class="row">
							<div class="col-sm-6">
								<h4 class="modal-title" id="exampleModalLabel">Rencana Ambil</h4>
							</div>
							<div class="col-sm-6">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
						</div>
					</div>
					<form @submit.prevent="updateDPambil">
						<div class="modal-body">
							<!-- <div class="row">
								<div class="col-md-12" style="text-transform: capitalize;">
									<table class="table table-hover table-responsive table-striped">
										<thead>
											<tr>
												<th class="active">NO. LEDGER</th>
												<th class="active">NAMA TOKO</th>
												<th class="active">NO. SM</th>
												<th class="active">NO. SO</th>
												<th class="active">ITEM</th>
												<th class="active">TAMBAH/KURANG QTY</th>
											</tr>
										</thead>
										<tbody>
											<tr v-for="(dt,i) in data_item_ambil" :key="i">
												<td>{{ dt.code }}</td>
												<td>{{ dt.name }}</td>
												<td v-if="dt.type === 'SM'">{{ dt.no }}</td>
												<td v-else></td>
												<td>{{ dt.so_no }}</td>
												<td>{{ dt.item_code }}</td>
												<td>{{ dt.item_qty2 }}</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div> -->
							</br>
							<div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-4">
											<label>
												Langsung &nbsp;&nbsp;
												<input type="radio" id="langsung" v-model="v_pilih" value="langsung">
											</label>
										</div>
										<div class="col-sm-4">
											<label>
												Pengalihan &nbsp;&nbsp;
												<input type="radio" id="pengalihan" v-model="v_pilih" value="pengalihan">
											</label>
										</div> 
									</div>
								</div>
								<div class="form-group">
									<label for="recipient-name" class="col-form-label">Tanggal Ambil</label>
									<vuejs-datepicker
										v-model="v_tgl_perintah_kirim"
										:format="DatePickerFormatb"
										:disabledDates="disabledDates"
										:bootstrap-styling="true"
										:placeholder="holderb"
										required
										style="width: 290px;">
									</vuejs-datepicker>
								</div>
								<div class="form-group" v-if="v_pilih == 'langsung'">
									<label for="recipient-name" class="col-form-label">No. Plat Truck</label>
									<input class="form-control" type="text" v-model="v_nopol_truck"/>
								</div>
								<div class="form-group" v-if="v_pilih == 'langsung'">
									<label for="recipient-name" class="col-form-label">Nama Supir</label>
									<input class="form-control" type="text" v-model="v_sopir_name"/>
								</div>
								<div class="form-group" v-if="v_pilih == 'langsung'">
									<label for="recipient-name" class="col-form-label">Kuantiti</label>
									<input class="form-control" type="text" v-model="v_kuantiti" disabled/> 
								</div>
								<div class="form-group" v-if="v_pilih == 'pengalihan'">
									<label for="recipient-name" class="col-form-label">Lokasi Keberangkatan</label>
									<v-select style="text-transform: capitalize" label="kd_off" :options="officeCode" v-model="v_office" :reduce="officeCode=>officeCode.kd_off"></v-select>
								</div>
								<div class="form-group" v-if="v_pilih == 'langsung'">
									<label for="recipient-name" class="col-form-label">Pilih Gudang</label>
									<v-select style="text-transform: capitalize" label="wh_code" :options="dt_gudang" v-model="v_gudang" :key="v_gudang" :reduce="dt_gudang=>dt_gudang.wh_code"></v-select>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">Simpan & Cetak DO</button>
							<button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	`,
	components: {
		vuejsDatepicker
	},
	props: {
		username: {
			required: true
		},
		loc: {
			required: true
		},
		where: {
			required: true
		},
		data: {
			type: Array,
			required: true,
		},
	},
	data() {
		return {
			v_tgl_perintah_kirim: '',
			checkedSO: [],
			officeCode: [],
			v_office: this.loc,
			v_office_current: this.loc,
			columns: ['type', 'code', 'ledgername', 'tanggal', 'sm_no', 'so_no',
				'item_code', 'alamat_toko', 'keterangan', 'jaminput', 'pilih'],
			/* pagination setting */
			pagestart: 0,
			pagecurrent: 1,
			itemsperpage: 50,
			pageend: 50,
			prevButtonIcon: '<i class="fa fa-chevron-left"></i>',
			nextButtonIcon: '<i class="fa fa-chevron-right"></i>',
			/* sort setting */
			currentSort: 'name',
			currentSortDir: 'asc',
			/* table column setting */
			activeColumn: {},
			new_columns: [
				{ name: 'code', order: 1 },
				{ name: 'ledgername', order: 1 },
				{ name: 'tanggal', order: 1 },
				{ name: 'delivery_time', order: 1 },
				{ name: 'sm_no', order: 1 },
				{ name: 'so_no', order: 1 },
				{ name: 'item_code', order: 1 },
				{ name: 'alamat_toko', order: 1 },
				{ name: 'keterangan', order: 1 },
			],
			colTitles: {
				'code': 'Ledger',
				'ledgername': 'Customer',
				'tanggal': 'Tanggal',
				'delivery_time': 'Tgl. Ambil',
				'sm_no': 'No. SM',
				'so_no': 'No. SO',
				'item_code': 'Item & QTY',
				'alamat_toko': 'Alamat',
				'keterangan': 'Keterangan',
			},
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 259200000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
			v_search_so: '',
			dt_truck: [],
			dt_supir: [],
			dt_gudang: [],
			v_nopol_truck: "",
			v_sopir_name: "",
			v_gudang: "",
			v_kuantiti: "",
			all_ambil: [],
			v_pilih: '',
			v_tgl_sort_ambil: new Date().toISOString(),
			DatePickerFormat: "yyyy-MM-dd",
		}
	},
	methods: {
		sortBy(column) {
			if (column === this.currentSort) {
				this.currentSortDir = this.currentSortDir === 'asc' ? 'desc' : 'asc';
			}
			this.currentSort = column;
		},
		widthHead(name) {
			if (name == 'code') {
				return 'width: 7%';
			}
		},
		sortData(tgl) {
			let tanggal = moment(tgl).format("YYYY-MM-DD");
			// console.log(tanggal);
			EventBus.$emit('loadSOAmbil', tanggal);
		},
		getTruck() {
			axios.post(this.action, {
				case: "getTruck",
				where: this.where
			}).then(response => {
				this.dt_truck = response.data;
			})
		},
		getSopir() {
			axios.post(this.action, {
				case: "getSopir",
				where: this.where
			}).then(response => {
				this.dt_supir = response.data;
			})
		},
		getGudang() {
			axios.post(this.action, {
				case: "getGudang",
				username: 'rachmadi',
			}).then(response => {
				this.dt_gudang = response.data;
			})
		},
		color(tgl) {
			var dateNow = new Date().getTime();
			var dateSo = new Date(tgl).getTime();
			var diffDay = parseInt((dateNow - dateSo) / (24 * 3600 * 1000));

			if (diffDay >= 3 && diffDay < 5) {
				return 'color: orange';
			} else if (diffDay >= 5) {
				return 'color: red';
			} else {
				return 'color: black';
			}
		},
		getOfficeCode() {
			axios.post(this.action, {
				case: 'getOfficeCode',
				username: 'rachmadi'
			}).then(response => {
				this.officeCode = response.data.dt_kd_offifce[0];
			});
		},
		add_SO() {
			if (this.checkedSO.length > 0) {
				this.saveSO();
			} else {
				swal.fire({
					title: "Gagal Simpan SO",
					text: "Silahkan Pilih SO Terlebih Dahulu",
					icon: "error",
				}).then(function (isConfirm) {
					if (isConfirm) {
						console.log('gagal boss');
					}
				});
			}
		},
		saveSO() {
			axios.post(this.action, {
				case: 'saveDistributionPlanAmbil',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
			}).then(response => {
				// console.log(response.data);
				EventBus.$emit('confirmPrice_ambil', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				this.checkedSO = [];
			});
		},
		saveSO2(data) {
			let arr_so = [];
			arr_so.push(data);

			axios.post(this.action, {
				case: 'saveDistributionPlanAmbil',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: arr_so,
			}).then(response => {
				//passambil
				// console.log(response.data);
				this.checkedSO = arr_so;
				this.all_ambil = response.data;
				this.v_kuantiti = this.all_ambil[0].data_so.item_qty2;

				this.getTruck();
				this.getSopir();
				this.getGudang();

				$('#modal_rencana_ambil').modal('show');
			});
		},
		saveSOpengalihan() {
			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
			}).then(response => {
				// console.log(response.data);
				// console.log("pengalihan");
				EventBus.$emit('confirmPrice_ambil_pengalihan', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				this.checkedSO = [];
			});
		},
		pagination(index) {
			//This calculates the range of data to show depending on the selected page
			if (index >= 0 && index <= this.totalpages) {
				this.pagecurrent = index;
				this.pagestart = (this.itemsperpage * index) - this.itemsperpage;
				this.pageend = (this.itemsperpage * index);
			}
		},
		getNow() {
			const today = new Date();
			const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
			const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			const dateTime = date + ' ' + time;
			this.v_tgl_perintah_kirim = dateTime;
		},
		loadNew() {
			// location.reload();
			EventBus.$emit('load_so_kirim_new');
		},
		updateDPambil() {
			// console.log(this.all_ambil);

			axios.post(this.action, {
				case: 'simpanDistributionPlanAmbilNew',
				gudang: this.v_gudang,
				no_pol: this.v_nopol_truck,
				sopir: this.v_sopir_name,
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
				dt_price: this.all_ambil
			}).then(response => {
				// location.reload();
				console.log(response.data);

				this.checkedSO = [];
				this.v_gudang = "";
				this.v_nopol_truck = "";
				this.v_sopir_name = "";

				if (this.v_pilih == 'langsung') {
					function submit_hidden_form(url, params) {
						var f = $("<form method='POST' target='_blank' style='display:none;'></form>").attr({
							action: url
						}).appendTo(document.body);
						for (var i in params) {
							if (params.hasOwnProperty(i)) {
								$('<input type="hidden" />').attr({
									name: i,
									value: params[i]
								}).appendTo(f);
							}
						}
						f.submit();
						f.remove();
					}

					submit_hidden_form(this.doPage3, {
						dp_id: response.data.id,
					})
				} else {
					this.$toasted.success('Data Berhasil Disimpan', { duration: 3000 });
				}



				$('#modal_rencana_ambil').modal('hide');
			});
		}
	},
	created() {
		this.getOfficeCode();
		EventBus.$on('addItemSOambil', (data) => {
			this.checkedSO = data;
		});

		this.getNow();
	},
	computed: {
		filterListST() {
			let filterST = this.v_search_so

			let result = this.data.filter(function (e) {
				let filtered = true
				if (filterST && filterST.length > 0) {
					filtered = e.so_no.includes(filterST)
				}
				return filtered;

			}).sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});

			if (result.length == 0) {
				let res = this.data.filter(function (e) {
					let filtered = true
					if (filterST && filterST.length > 0) {
						filtered = e.ledgername.includes(filterST)
					}
					return filtered;

				}).sort((a, b) => {
					let modifier = 1;
					if (this.currentSortDir === 'desc') modifier = -1;
					if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
					if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
					return 0;
				});
				return res;
			} else {
				return result;
			}

			// return result;
		},
		totalpages() {
			//This calculates the amount of pages
			return Math.ceil(this.data.length / this.itemsperpage);
		},
		sortedData: function () {
			return this.data.sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});
		}
	},
	watch: {
		data: function () {
			this.pagecurrent = 1;
			this.pagestart = 0;
			this.pageend = this.itemsperpage;
		},
	}

});

Vue.component('daf_so_ambil_component_tab', {

	props: ['data', 'data_plan', 'username', 'loc', 'where'],
	template: `<div>
		<div class="row">
			<div class="col-md-12">
				<div class="lds-facebook" v-if="loading"><div></div><div></div><div></div></div>
				<tabs_distribution_plan_component>
					<tab_distribution_plan_component v-for="(dt,i) in tabs" :key="i" :name="dt.name" :icon="dt.icon" :selected="i === 0 ? 'true' : ''">
						<div v-if="i == 0">
							<daf_so_ambil_component_new :data="data" :username="username" :loc="loc" :where="where"></daf_so_ambil_component_new>
						</div>
						<div v-if="i == 1">
							<dis-plan-ambil-component :data="data_plan" :username="username" :loc="loc"></dis-plan-ambil-component>
						</div>
					</tab_distribution_plan_component>
				</tabs_distribution_plan_component>
			</div>
		</div>
	</div>
	`,
	data() {
		return {
			loading: false,
			tabs: [
				{
					name: 'DAFTAR SO AMBIL',
					icon: '<i class="fa fa-print"></i>',
					counter: 0,
				},
				{
					name: 'DAFTAR DP AMBIL',
					icon: '<i class="fa fa-print"></i>',
					counter: 0,
				},
			]
		}
	},
	created() {
		EventBus.$on('loadSOAmbil', (tanggal) => {
			// console.log(tanggal);
			let tgl = tanggal;
			EventBus.$emit('loadSOByTanggal', tgl);
		});
		// EventBus.$on
	}

});

Vue.component('daf_dp_kirim_component_tab', {

	props: ['data', 'data_plan', 'data_more', 'username', 'loc', 'where'],
	template: `<div>
		<div class="row">
			<div class="col-md-12">
				<div class="lds-facebook" v-if="loading"><div></div><div></div><div></div></div>
				<div v-if="username == 'rachmadi' || username == 'edpaja'">
					<tabs_distribution_plan_component>
						<tab_distribution_plan_component v-for="(dt,i) in tabs2" :key="i" :name="dt.name" :icon="dt.icon" :selected="i === 3 ? 'true' : ''">
							<div v-if="i == 0">
								<daf_so_kirim_new_more2 :data="data_more" :username="username" :loc="loc"></daf_so_kirim_new_more2>
							</div>
							<div v-if="i == 1">
								<dis-plan-kirim-list-component :data="data" :username="username" :loc="loc"></dis-plan-kirim-list-component>
							</div>
							<div v-if="i == 2">
								<selected-displan :username="username" :loc="loc" :where="where"></selected-displan>
							</div>
							<div v-if="i == 3">
								<dis-plan-kirim-component :data="data" :username="username" :loc="loc"></dis-plan-kirim-component>
							</div>
						</tab_distribution_plan_component>
					</tabs_distribution_plan_component>
				</div>
				<div v-else>
					<tabs_distribution_plan_component>
						<tab_distribution_plan_component v-for="(dt,i) in tabs" :key="i" :name="dt.name" :icon="dt.icon" :selected="i === 2 ? 'true' : ''">
							<div v-if="i == 0">
								<dis-plan-kirim-list-component :data="data" :username="username" :loc="loc"></dis-plan-kirim-list-component>
							</div>
							<div v-if="i == 1">
								<selected-displan :username="username" :loc="loc" :where="where"></selected-displan>
							</div>
							<div v-if="i == 2">
								<dis-plan-kirim-component :data="data" :username="username" :loc="loc"></dis-plan-kirim-component>
							</div>
						</tab_distribution_plan_component>
					</tabs_distribution_plan_component>
				</div>
			</div>
		</div>
	</div>
	`,
	data() {
		return {
			loading: false,
			tabs: [
				{
					name: 'DP KIRIM',
					icon: '<i class="fa fa-print"></i>',
					counter: 0,
				},
				{
					name: 'DP GANDENGAN',
					icon: '<i class="fa fa-print"></i>',
					counter: 1,
				},
				{
					name: 'RENCANA KIRIM',
					icon: '<i class="fa fa-print"></i>',
					counter: 2,
				},
			],
			tabs2: [
				{
					name: 'SO KIRIM',
					icon: '<i class="fa fa-print"></i>',
					counter: 0,
				},
				{
					name: 'DP KIRIM',
					icon: '<i class="fa fa-print"></i>',
					counter: 1,
				},
				{
					name: 'DP GANDENGAN',
					icon: '<i class="fa fa-print"></i>',
					counter: 2,
				},
				{
					name: 'RENCANA KIRIM',
					icon: '<i class="fa fa-print"></i>',
					counter: 3,
				},
			]
		}
	},

});

Vue.component('pilih-so-component', {

	props: ["data", "index", "column"],
	// template: `<input type="checkbox" :value="data" id="data.so_no" v-model="checkedSO" @change="check($event)">`,
	template: `<input type="checkbox" v-bind:id="data.so_no" v-bind:value="data" v-model="checkedSO" @change="check($event)">`,
	data() {
		return {
			all_data: [],
			checkedSO: []
		}
	},
	methods: {
		check: function (data) {
			this.checkedSO.push(data);
			EventBus.$emit('addItemSO', this.checkedSO);
		}
	},
	created() {

	},
	computed: {

	},
	watch: {

	}

});

Vue.component('pilih-so-ambil-component', {

	props: ["data", "index", "column"],
	template: `<input type="checkbox" :value="data" id="data.so_no" v-model="checkedSO" @change="check($event)">`,
	data() {
		return {
			checkedSO: []
		}
	},
	methods: {
		check: function (e) {
			EventBus.$emit('addItemSOambil', this.checkedSO);
		}
	},
	created() {
	},
	computed: {

	},

});

//displanpass
Vue.component('distribution_plan_component', {

	// <div v-if="i == 3">
	// 	<daf_force_component :username="username" :usergroup="usergroup"></daf_force_component>
	// </div>
	// <dis-plan-kirim-component :data="dis_plan_list_kirim" :username="username" :loc="loc"></dis-plan-kirim-component>
	template: `
		<div>
			<div class="row" v-if="view_list_plan" style="">
				<div class="col-md-12">
					<div class="lds-facebook" v-if="loading"><div></div><div></div><div></div></div>
					<tabs_distribution_plan_component>
						<tab_distribution_plan_component v-for="(dt,i) in tabs2" :key="i" :name="dt.name" :icon="dt.icon" :selected="i === 0 ? 'true' : ''">
							<div v-if="i == 0">
								<daf_dp_kirim_component_tab :data="dis_plan_list_kirim" :data_more="listSO_kirim_more" :username="username" :loc="loc" :where="where"></daf_dp_kirim_component_tab>								
							</div>
							<div v-if="i == 1">
								<daf_so_ambil_component_tab :data="listSO_ambil" :data_plan="dis_plan_list_ambil" :username="username" :loc="loc" :where="where"></daf_so_ambil_component_tab>
							</div>
							<div v-if="i == 2">
								<dis-plan-transfer-component :data="dis_plan_list_transfer" :username="username" :loc="loc"></dis-plan-transfer-component>
							</div>
							<div v-if="i == 3">
								<daf_outstanding_component :username="username"></daf_outstanding_component>
							</div>
						</tab_distribution_plan_component>
					</tabs_distribution_plan_component>
					<modal-proses-dp-comp :username="username"></modal-proses-dp-comp>
				</div>
			</div>
			<div v-if="view_dp_plan">
				<div class="row">
					<div class="col-md-10">
						<h4>Rencana Kirim</h4>
					</div>
					<div class="col-md-2" style="text-align: end;">
						<button type="button" class="btn btn-xm btn-danger" @click="btn_list_plan">Kembali</button>
					</div>
				</div>
				</br></br>
				<div>
					<div class="row">
						<div class="col-md-10" style="margin-left: 80px; text-transform: capitalize;">
							<table class="table table-hover table-responsive table-striped">
								<thead>
									<tr>
										<th class="active">TANGGAL</th>
										<th class="active">NO. LEDGER</th>
										<th class="active">NAMA TOKO</th>
										<th class="active">NO. SM</th>
										<th class="active">NO. SO</th>
										<th class="active">ITEM</th>
										<th class="active">TAMBAH/KURANG QTY</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="(dt,i) in data.detail" :key="i">
										<td>{{ dt.f4 | moment }}</td>
										<td>{{ dt.f12 }}</td>
										<td>{{ dt.f16 }}</td>
										<td v-if="dt.f2 === 'SM'">{{ dt.f5 }}</td>
										<td v-else></td>
										<td>{{ dt.f6 }}</td>
										<td>{{ dt.f8 }}</td>
										<td>{{ dt.f10 }}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					</br>
					<div class="row">
						<div class="col-md-10" style="margin-left: 80px;">
							<!-- <div class="form-group">
								<label class="col-form-label">Pilih Truck</label>
								<div class="row">
									<div class="col-sm-4">
										<label>
											Truck Operasional &nbsp;&nbsp;
											<input type="radio" id="truck_dalam" v-model="v_pilih_truck" value="truck_dalam">
										</label>
									</div>
									<div class="col-sm-4">
										<label>
											Truck Luar &nbsp;&nbsp;
											<input type="radio" id="truck_luar" v-model="v_pilih_truck" value="truck_luar">
										</label>
									</div> 
								</div>
							</div> -->
							<div class="form-group">
								<label for="recipient-name" class="col-form-label">Tanggal Kirim</label>
								<vuejs-datepicker
									v-model="v_tgl_kirim"
									:format="DatePickerFormatb"
									:bootstrap-styling="true"
									:disabledDates="disabledDates"
									:placeholder="holderb"
									required
									style="width: 290px;"
									@input="countDate"
									required>
								</vuejs-datepicker>
							</div>
							<div class="form-group">
								<label for="recipient-name" class="col-form-label">No. Plat Truck</label>
								<v-select style="text-transform: capitalize" label="nopol" :options="dt_truck" v-model="v_nopol_truck" :reduce="dt_truck=>dt_truck.nopol" :taggable="true" @input="getSupirByNopol"></v-select>
							</div>
							<div class="form-group">
								<label for="recipient-name" class="col-form-label">Nama Supir</label>
								<v-select style="text-transform: capitalize" label="sopir" :options="dt_supir" v-model="v_sopir_name" @input="getSupirBySupir" :taggable="true"></v-select>
							</div>
							<div class="form-group">
								<label for="recipient-name" class="col-form-label">Pilih Gudang</label>
								<v-select style="text-transform: capitalize" label="wh_code" :options="dt_gudang" v-model="v_gudang" :reduce="dt_gudang=>dt_gudang.wh_code" required @input="cekNoTransfer"></v-select> 
							</div>
							<div class="form-group" v-if="dt_notrans.length > 0">
								<label for="recipient-name" class="col-form-label">Pilih No. Transfer</label>
								<v-select style="text-transform: capitalize" label="ref_no" :options="dt_notrans" v-model="v_noTrans" :reduce="dt_notrans=>dt_notrans.ref_no" @input="getSopirByNoto(v_noTrans)"></v-select> 
							</div>
							<div class="form-group" v-if="v_alsan_terlambat.length > 0">
								<label for="recipient-name" class="col-form-label">Alasan Keterlambatan</label>
								<v-select style="text-transform: capitalize" label="alasan" :options="dataAlasan" v-model="v_alasan" :reduce="dataAlasan=>dataAlasan.alasan" required></v-select>
							</div>
							<!-- <div class="form-group" v-else>
								
							</div> -->
							<div class="form-group">
								<textarea class="form-control" rows="6">{{ data.catatan }}</textarea>
								<span style="font-size: 10px;font-style: italic">* Diisi, jika ada instruksi tambahan dalam pengiriman</span>
							</div>
							<div class="form-group">
								<label for="recipient-name" class="col-form-label">Pilih Ritase</label>
								<select class="form-control" id="ritaseplan" name="ritaseplan" v-model="v_ritase">
									<option v-for="option in options_ritase" v-bind:value="option.value">
										{{ option.text }}
									</option>
								</select>
							</div>
							<!-- <div class="form-group" v-else>
								
							</div> -->
							</br></br></br>
							<div class="row">
								<div class="col-md-11" style="text-align: end; margin-right: -20px;">
									<button type="submit" class="btn btn-primary" @click="updateDP">Simpan & Cetak DO</button>
								</div>
								<div class="col-md-1" style="text-align: end;">
									<button type="button" class="btn btn-danger" @click="btn_list_plan">Batalkan</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div v-if="view_dp_plan_ambil">
				<div class="row">
					<div class="col-md-10">
						<h4>Rencana Ambil</h4>
					</div>
					<div class="col-md-2" style="text-align: end;">
						<button type="button" class="btn btn-xm btn-danger" @click="btn_list_plan">Kembali</button>
					</div>
				</div>
				</br></br>
				<div>
					<div class="row">
						<div class="col-md-10" style="margin-left: 80px; text-transform: capitalize;">
							<table class="table table-hover table-responsive table-striped">
								<thead>
									<tr>
										<th class="active">TANGGAL</th>
										<th class="active">NO. LEDGER</th>
										<th class="active">NAMA TOKO</th>
										<th class="active">NO. SM</th>
										<th class="active">NO. SO</th>
										<th class="active">ITEM</th>
										<th class="active">TAMBAH/KURANG QTY</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="(dt,i) in data_ambil.detail" :key="i">
										<td>{{ dt.f4 | moment }}</td>
										<td>{{ dt.f12 }}</td>
										<td>{{ dt.f16 }}</td>
										<td v-if="dt.f2 === 'SM'">{{ dt.f5 }}</td>
										<td v-else></td>
										<td>{{ dt.f6 }}</td>
										<td>{{ dt.f8 }}</td>
										<td>{{ dt.f10 }}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				</br>
				<div class="row">
					<div class="col-md-10" style="margin-left: 80px; text-transform: capitalize;">
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">No. Plat Truck</label>
							<input class="form-control" type="text" v-model="v_nopol_truck" required="required"/>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Nama Supir</label>
							<input class="form-control" type="text" v-model="v_sopir_name" required="required"/>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Kuantiti</label>
							<input class="form-control" type="text" v-model="v_kuantiti" disabled/>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Pilih Gudang</label>
							<v-select style="text-transform: capitalize" label="wh_code" :options="dt_gudang" v-model="v_gudang" :reduce="dt_gudang=>dt_gudang.wh_code" required="required"></v-select>
						</div>
						</br></br></br>
						<div class="row">
							<div class="col-md-11" style="text-align: end; margin-right: -20px;">
								<button type="submit" class="btn btn-primary" @click="updateDPAmbil2">Simpan & Cetak DO</button>
							</div>
							<div class="col-md-1" style="text-align: end;">
								<button type="button" class="btn btn-danger" @click="btn_list_plan">Batalkan</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div v-if="view_dp_plan_edit_kirim">
				<div class="row">
					<div class="col-md-10">
						<h4>Transfer DO Kirim</h4>
					</div>
					<div class="col-md-2" style="text-align: end;">
						<button type="button" class="btn btn-xm btn-danger" @click="btn_list_plan">Kembali</button>
					</div>
				</div>
				</br></br>
				<div>
					<div class="row">
						<div class="col-md-10" style="margin-left: 80px; text-transform: capitalize;">
							<table class="table table-hover table-responsive table-striped">
								<thead>
									<tr>
										<th class="active">TANGGAL</th>
										<th class="active">NO. LEDGER</th>
										<th class="active">NAMA TOKO</th>
										<th class="active">NO. SM</th>
										<th class="active">NO. SO</th>
										<th class="active">ITEM</th>
										<th class="active">TAMBAH/KURANG QTY</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="(dt,i) in detail_dp_kirim.detail" :key="i">
										<td>{{ dt.f4 | moment }}</td>
										<td>{{ dt.f12 }}</td>
										<td>{{ dt.f16 }}</td>
										<td v-if="dt.f2 === 'SM'">{{ dt.f5 }}</td>
										<td v-else></td>
										<td>{{ dt.f6 }}</td>
										<td>{{ dt.f8 }}</td>
										<td>{{ dt.f10 }}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					</br>
					<div class="row">
						<div class="col-md-10" style="margin-left: 80px;">
							<div class="form-group">
								<label class="col-form-label">Pilih Truck</label>
								<div class="row">
									<div class="col-sm-4">
										<label>
											Truck Operasional &nbsp;&nbsp;
											<input type="radio" id="truck_dalam" v-model="v_pilih_truck" value="truck_dalam">
										</label>
									</div>
									<div class="col-sm-4">
										<label>
											Truck Luar &nbsp;&nbsp;
											<input type="radio" id="truck_luar" v-model="v_pilih_truck" value="truck_luar">
										</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="recipient-name" class="col-form-label">Tanggal Kirim</label>
								<vuejs-datepicker
									v-model="v_tgl_kirim"
									:format="DatePickerFormatb"
									:bootstrap-styling="true"
									:placeholder="holderb"
									style="width: 290px;"
									@input="countDate"
									required>
								</vuejs-datepicker>
							</div>
							<div class="form-group">
								<label for="recipient-name" class="col-form-label">Pilih Gudang</label>
								<v-select style="text-transform: capitalize" label="wh_code" :options="dt_gudang" v-model="v_gudang" :key="v_gudang" :reduce="dt_gudang=>dt_gudang.wh_code" required="required" @input="getNoTransfer(v_gudang)"></v-select>
							</div>
							<div class="form-group">
								<label for="recipient-name" class="col-form-label">Pilih No. Transfer</label>
								<v-select style="text-transform: capitalize" label="ref_no" :options="no_transfer" v-model="v_notransfer" :key="v_notransfer" :reduce="no_transfer=>no_transfer.ref_no" @input="getSopirByNoto(v_notransfer)"></v-select>
							</div>
							<div class="form-group">
								<label for="recipient-name" class="col-form-label">No. Plat Truck</label>
								<v-select style="text-transform: capitalize" label="nopol" :options="dt_truck" v-model="v_nopol_truck" :key="v_nopol_truck" :reduce="dt_truck=>dt_truck.nopol" v-if="v_pilih_truck === 'truck_dalam'" :disabled="v_notransfer"></v-select>
								<input class="form-control" type="text" v-model="v_nopol_truck" v-else required="required"/>
							</div>
							<div class="form-group">
								<label for="recipient-name" class="col-form-label">Nama Supir</label>
								<v-select style="text-transform: capitalize" label="sopir" :options="dt_supir" v-model="v_sopir_name" :key="v_sopir_name" :reduce="dt_sopir=>dt_sopir.sopir" v-if="v_pilih_truck === 'truck_dalam'" :disabled="v_notransfer"></v-select>
								<input class="form-control" type="text" v-model="v_sopir_name" v-else required="required"/>
							</div>
							<div class="form-group">
								<label for="recipient-name" class="col-form-label">Tanggal Terima Transfer</label>
								<vuejs-datepicker
									v-model="v_tgl_terima"
									:format="DatePickerFormatb"
									:bootstrap-styling="true"
									:placeholder="holderb"
									style="width: 290px;"
									required>
								</vuejs-datepicker>
							</div>
							<div class="form-group" v-if="v_totalPengiriman > 3">
								<label for="recipient-name" class="col-form-label">Alasan</label>
								<textarea class="form-control" id="exampleFormControlTextarea1" rows="3" v-model="v_alasan" style="resize: none;" required="required"></textarea>
							</div>
							<!-- <div class="form-group" v-else>
								
							</div> -->
							<div class="form-group">
								<label for="recipient-name" class="col-form-label">Catatan Tambahan (Jika Ada)</label>
								<textarea class="form-control" id="exampleFormControlTextarea1" rows="3" v-model="v_catatan_to" style="resize: none;"></textarea>
							</div>
							</br></br></br>
							<div class="row">
								<div class="col-md-11" style="text-align: end; margin-right: -20px;">
									<button type="submit" class="btn btn-primary" @click="updateDetailDPKirim">Simpan & Cetak DO</button>
								</div>
								<div class="col-md-1" style="text-align: end;">
									<button type="button" class="btn btn-danger" @click="btn_list_plan">Batalkan</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div v-if="view_dp_plan_edit_ambil">
				<div class="row">
					<div class="col-md-10">
						<h4>Edit DO Ambil</h4>
					</div>
					<div class="col-md-2" style="text-align: end;">
						<button type="button" class="btn btn-xm btn-danger" @click="btn_list_plan">Kembali</button>
					</div>
				</div>
				</br></br>
				<div>
					<form style="margin-left: 80px; margin-right: 100px;" @submit.prevent="updateDetailDPAmbil">
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">No. Plat Truck</label>
							<input class="form-control" type="text" v-model="v_nopol_truck" required="required"/>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Nama Supir</label>
							<input class="form-control" type="text" v-model="v_sopir_name" required="required"/>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Kuantiti</label>
							<input class="form-control" type="text" v-model="v_kuantiti" disabled/>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Pilih Gudang</label>
							<v-select style="text-transform: capitalize" label="wh_code" :options="dt_gudang" v-model="v_gudang" :key="v_gudang" :reduce="dt_gudang=>dt_gudang.wh_code" required="required"></v-select>
						</div>
						</br></br></br>
						<div class="row">
							<div class="col-md-11" style="text-align: end; margin-right: -20px;">
								<button type="submit" class="btn btn-primary">Simpan & Cetak DO</button>
							</div>
							<div class="col-md-1" style="text-align: end;">
								<button type="button" class="btn btn-danger" @click="btn_list_plan">Batalkan</button>
							</div>
						</div>
					</form>	
				</div>
			</div>
			<div v-if="view_dp_plan_transfer_kirim">
				<div class="row">
					<div class="col-md-10">
						<h4>Transfer DO Kirim</h4>
					</div>
					<div class="col-md-2" style="text-align: end;">
						<button type="button" class="btn btn-xm btn-danger" @click="btn_list_plan">Kembali</button>
					</div>
				</div>
				</br></br>
				<div>
					<div class="row">
						<div class="col-md-10" style="margin-left: 80px; text-transform: capitalize;">
							<table class="table table-hover table-responsive table-striped">
								<thead>
									<tr>
										<th class="active">NO. LEDGER</th>
										<th class="active">NAMA TOKO</th>
										<th class="active">NO. SM</th>
										<th class="active">NO. SO</th>
										<th class="active">ITEM</th>
										<th class="active">TAMBAH/KURANG QTY</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="(dt,i) in detail_dp_kirim.item" :key="i">
										<td>{{ dt.code }}</td>
										<td>{{ dt.name }}</td>
										<td v-if="dt.type === 'SM'">{{ dt.no }}</td>
										<td v-else></td>
										<td>{{ dt.so_no }}</td>
										<td>{{ dt.item_code }}</td>
										<td>{{ dt.item_qty2 }}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					</br>
					<form style="margin-left: 80px; margin-right: 100px;" @submit.prevent="updateProsesTransferDPKirim">
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">No. Plat Truck</label>
							<input class="form-control" type="text" v-model="v_nopol_truck" disabled />
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Nama Supir</label>
							<input class="form-control" type="text" v-model="v_sopir_name" disabled />
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Pilih Gudang</label>
							<v-select style="text-transform: capitalize" label="wh_code" :options="dt_gudang" v-model="v_gudang" :key="v_gudang" :reduce="dt_gudang=>dt_gudang.wh_code" required="required"></v-select>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Pilih No. Transfer</label>
							<v-select style="text-transform: capitalize" label="ref_no" :options="no_transfer" v-model="v_notransfer" :key="v_notransfer" :reduce="no_transfer=>no_transfer.ref_no" required="required"></v-select>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Tanggal Kirim</label>
							<vuejs-datepicker
								v-model="v_tgl_kirim"
								:format="DatePickerFormatb"
								:disabledDates="disabledDates"
								:bootstrap-styling="true"
								:placeholder="holderb"
								style="width: 290px;"
								@input="countDate"
								required>
							</vuejs-datepicker>
						</div>
						<div class="form-group" v-if="v_totalPengiriman > 3">
							<label for="recipient-name" class="col-form-label">Alasan</label>
							<textarea class="form-control" id="exampleFormControlTextarea1" rows="3" v-model="v_alasan" style="resize: none;" required="required"></textarea>
						</div>
						<!-- <div class="form-group" v-else>
							
						</div> -->
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Pilih Ritase</label>
							<select class="form-control" id="ritaseplan" name="ritaseplan" v-model="v_ritase" required="required">
								<option v-for="option in options_ritase" v-bind:value="option.value">
									{{ option.text }}
								</option>
							</select>
						</div>
						</br></br></br>
						<div class="row">
							<div class="col-md-11" style="text-align: end; margin-right: -20px;">
								<button type="submit" class="btn btn-primary">Simpan & Cetak DO</button>
							</div>
							<div class="col-md-1" style="text-align: end;">
								<button type="button" class="btn btn-danger" @click="btn_list_plan">Batalkan</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div v-if="view_dp_plan_transfer_terima">
				<div class="row">
					<div class="col-md-10">
						<h4>Buat DO Pengalihan & Input Terima Konsumen</h4>
					</div>
					<div class="col-md-2" style="text-align: end;">
						<button type="button" class="btn btn-xm btn-danger" @click="btn_list_plan">Kembali</button>
					</div>
				</div>
				</br></br>
				<div>
					<div class="row">
						<div class="col-md-10" style="margin-left: 80px; text-transform: capitalize;">
							<table class="table table-hover table-responsive table-striped">
								<thead>
									<tr>
										<th class="active">NO. LEDGER</th>
										<th class="active">NAMA TOKO</th>
										<th class="active">NO. SM</th>
										<th class="active">NO. SO</th>
										<th class="active">ITEM</th>
										<th class="active">TAMBAH/KURANG QTY</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="(dt,i) in detail_dp_transfer.item" :key="i">
										<td>{{ dt.code }}</td>
										<td>{{ dt.name }}</td>
										<td v-if="dt.type === 'SM'">{{ dt.no }}</td>
										<td v-else></td>
										<td>{{ dt.so_no }}</td>
										<td>{{ dt.item_code }}</td>
										<td>{{ dt.item_qty2 }}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					</br>
					<form style="margin-left: 80px; margin-right: 100px;" @submit.prevent="updateProsesTransferDPKirimx">
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">No. Plat Truck</label>
							<input class="form-control" type="text" v-model="v_nopol_truck" disabled />
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Nama Supir</label>
							<input class="form-control" type="text" v-model="v_sopir_name" disabled />
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Tanggal Terima</label>
							<vuejs-datepicker
								v-model="v_tgl_terima"
								:format="DatePickerFormatb"
								:disabledDates="disabledDates"
								:bootstrap-styling="true"
								:placeholder="holderb"
								style="width: 290px;"
								@input="countDate"
								required>
							</vuejs-datepicker>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Catatan Tambahan (Jika Ada)</label>
							<textarea class="form-control" id="exampleFormControlTextarea1" rows="3" v-model="v_catatan_to" style="resize: none;"></textarea>
						</div>
						</br></br></br>
						<div class="row">
							<div class="col-md-11" style="text-align: end; margin-right: -20px;">
								<button type="submit" class="btn btn-primary">Simpan & Cetak DO</button>
							</div>
							<div class="col-md-1" style="text-align: end;">
								<button type="button" class="btn btn-danger" @click="btn_list_plan">Batalkan</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div v-if="view_pengiriman_kirim">
				<div class="row">
					<div class="col-md-10">
						<h4>Buat DO Kirim</h4>
					</div>
					<div class="col-md-2" style="text-align: end;">
						<button type="button" class="btn btn-xm btn-danger" @click="btn_list_plan">Kembali</button>
					</div>
				</div>
				</br></br>
				<div class="row">
					<div class="col-md-12">
						<table class="table table-hover table-responsive table-striped">
							<thead>
								<tr>
									<th class="active">NO. DP</th>
									<th class="active">TGL. KIRIM</th>
									<th class="active">GUDANG ASAL</th>
									<th class="active">TRUCK</th>
									<th class="active">SUPIR</th>
								</tr>
							</thead>
							<tbody>
								<tr v-for="(dt,i) in detail_kirim_byid">
									<td>{{ dt.no_dp }}</td>
									<td>{{ dt.tgl_kirim2 }}</td>
									<td>{{ dt.wh_code }}</td>
									<td>{{ dt.no_pol }}</td>
									<td>{{ dt.sopir }}</td>
								</tr>
							</tbody>
						</table>
					</div>
					</br></br></br></br>
					<div class="col-md-12">
						<table class="table table-hover table-responsive table-striped">
							<thead>
								<tr>
									<th class="active">No</th>
									<th class="active">No SO</th>
									<th class="active">No Ledger</th>
									<th class="active">Nama Toko</th>
									<th class="active">Item</th>
									<th class="active">Qty</th>
									<th class="active">Tgl. Kirim</th>
									<th class="active">&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								{{ oldSO = '' }}
								<tr v-for="(dta,i) in detail_kirim_byid_item">
									<td>{{ i+1 }}</td>
									<td>{{ dta.so_no }}</td>
									<td>{{ dta.code }}</td>
									<td>{{ dta.name }}</td>
									<td>{{ dta.item_code }}</td>
									<td>{{ dta.item_qty2 }}</td>
									<td>{{ dta.tanggal }}</td>
									<td v-if="oldSO != dta.so_no">
										<button name="btn-load" class="btn btn-info btn-flat btn-xs" @click="cetak_do(dta)"> <i class="fa fa-print"></i> CETAK DO</button>
									</td>
									<td v-else>
									</td>
									<div style="display: none;">{{ oldSO = dta.so_no }}</div>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div v-if="view_pengiriman_ambil">
				<div class="row">
					<div class="col-md-10">
						<h4>Buat DO Ambil</h4>
					</div>
					<div class="col-md-2" style="text-align: end;">
						<button type="button" class="btn btn-xm btn-danger" @click="btn_list_plan">Kembali</button>
					</div>
				</div>
				</br></br>
				<div class="row">
					<div class="col-md-12">
						<table class="table table-hover table-responsive table-striped">
							<thead>
								<tr>
									<th class="active">NO. DP</th>
									<th class="active">TGL. KIRIM</th>
									<th class="active">GUDANG ASAL</th>
									<th class="active">TRUCK</th>
									<th class="active">SUPIR</th>
								</tr>
							</thead>
							<tbody>
								<tr v-for="(dt,i) in detail_ambil_byid">
									<td>{{ dt.no_dp }}</td>
									<td>{{ dt.tgl_perintah2 }}</td>
									<td>{{ dt.wh_code }}</td>
									<td>{{ dt.no_pol }}</td>
									<td>{{ dt.sopir }}</td>
								</tr>
							</tbody>
						</table>
					</div>
					</br></br></br></br>
					<div class="col-md-12">
						<table class="table table-hover table-responsive table-striped">
							<thead>
								<tr>
									<th class="active">No</th>
									<th class="active">No SO</th>
									<th class="active">No Ledger</th>
									<th class="active">Nama Toko</th>
									<th class="active">Item</th>
									<th class="active">Qty</th>
									<th class="active">Tgl. Kirim</th>
									<th class="active">&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								{{ oldSO_ambil = '' }}								
								<tr v-for="(dtaa,i) in detail_ambil_byid_item">
									<td>{{ i+1 }}</td>
									<td>{{ dtaa.so_no }}</td>
									<td>{{ dtaa.code }}</td>
									<td>{{ dtaa.name }}</td>
									<td>{{ dtaa.item_code }}</td>
									<td>{{ dtaa.item_qty2 }}</td>
									<td>{{ dtaa.tanggal }}</td>
									<td v-if="oldSO_ambil != dtaa.so_no">
										<button name="btn-load" class="btn btn-info btn-flat btn-xs" @click="cetak_do(dtaa)"> <i class="fa fa-print"></i> CETAK DO</button>
									</td>
									<td v-else>
									</td>
									<div style="display: none;">{{ oldSO_ambil = dtaa.so_no }}</div>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div v-if="view_pengiriman_transfer">
				<div class="row">
					<div class="col-md-10">
						<h4>Buat DO PENGALIHAN</h4>
					</div>
					<div class="col-md-2" style="text-align: end;">
						<button type="button" class="btn btn-xm btn-danger" @click="btn_list_plan">Kembali</button>
					</div>
				</div>
				</br></br>
				<div class="row">
					<div class="col-md-12">
						<table class="table table-hover table-responsive table-striped">
							<thead>
								<tr>
									<th class="active">NO. DP</th>
									<th class="active">TGL. KIRIM</th>
									<th class="active">GUDANG ASAL</th>
									<th class="active">TRUCK</th>
									<th class="active">SUPIR</th>
									<th class="active">NO. TO</th>
									<th class="active">CATATAN TO</th>
									<th class="active">TGL. TERIMA</th>
								</tr>
							</thead>
							<tbody>
								<tr v-for="(dt,i) in detail_transfer_byid">
									<td>{{ dt.no_dp }}</td>
									<td>{{ dt.tgl_kirim2 }}</td>
									<td>{{ dt.wh_code }}</td>
									<td>{{ dt.no_pol }}</td>
									<td>{{ dt.sopir }}</td>
									<td>{{ dt.no_to }}</td>
									<td>{{ dt.catatan_to }}</td>
									<td>{{ dt.tgl_terima }}</td>
								</tr>
							</tbody>
						</table>
					</div>
					</br></br></br></br>
					<div class="col-md-12">
						<table class="table table-hover table-responsive table-striped">
							<thead>
								<tr>
									<th class="active">No</th>
									<th class="active">No SO</th>
									<th class="active">No Ledger</th>
									<th class="active">Nama Toko</th>
									<th class="active">Item</th>
									<th class="active">Qty</th>
									<th class="active">Tgl. Kirim</th>
									<th class="active">&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								{{ xxxno_dp = '' }}
								<tr v-for="(data,i) in detail_transfer_byid_item">
									<td>{{ i+1 }}</td>
									<td>{{ data.so_no }}</td>
									<td>{{ data.code }}</td>
									<td>{{ data.name }}</td>
									<td>{{ data.item_code }}</td>
									<td>{{ data.item_qty2 }}</td>
									<td>{{ data.tanggal }}</td>
									<td v-if="xxxno_dp != data.so_no">
										<button name="btn-load" class="btn btn-info btn-flat btn-xs" @click="cetak_do(data)"> <i class="fa fa-print"></i> CETAK DO</button>
									</td>
									<td v-else>
									</td>
									<span style="display: none;">{{ xxxno_dp = data.so_no }}</span>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="modal fade" data-backdrop="static" id="modal_proses_transfer" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content modal-md">
						<div class="modal-header">
							<div class="row">
								<div class="col-sm-6">
									<h5 class="modal-title" id="exampleModalLabel">Buat DO Pengalihan & Input Terima Konsumen</h5>
								</div>
								<div class="col-sm-6">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							</div>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="form-group col-sm-12">
									<label>No Truck</label>
									<input class="form-control" v-model="new_data_pengalihan.no_pol" disabled />
								</div>
								<div class="form-group col-sm-12">
									<label>Nama Supir</label>
									<input class="form-control" v-model="new_data_pengalihan.sopir" disabled />
								</div>
								<div class="form-group col-sm-12">
									<label>Tanggal Terima</label>
									<vuejs-datepicker
										v-model="v_tgl_terima"
										:format="DatePickerFormatb"
										:bootstrap-styling="true"
										:placeholder="holderb"
										style="width: 290px;"
										@input="countDate"
										required>
									</vuejs-datepicker>
								</div>
								<div class="form-group col-sm-12">
									<label>Keterangan (Jika Ada)</label>
									<textarea class="form-control" rows="3" style="resize: none;" v-model="new_catatan_to"></textarea>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" data-dismiss="modal" @click="cetakDOTransfer(new_data_pengalihan.id)">Simpan & Cetak DO</button>
							<button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade" data-backdrop="static" id="modal_proses_refuse" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content modal-md">
						<div class="modal-header">
							<div class="row">
								<div class="col-sm-6">
									<h5 class="modal-title" id="exampleModalLabel">Tolak SO</h5>
								</div>
								<div class="col-sm-6">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							</div>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-10" style="text-transform: capitalize;">
									<table class="table table-hover table-responsive table-striped">
										<thead>
											<tr>
												<th class="active">TANGGAL</th>
												<th class="active">NO. LEDGER</th>
												<th class="active">NAMA TOKO</th>
												<th class="active">NO. SM</th>
												<th class="active">NO. SO</th>
												<th class="active">ITEM</th>
												<th class="active">QTY</th>
											</tr>
										</thead>
										<tbody>
											<tr v-for="(dt,i) in refuse_dp.detail" :key="i">
												<td>{{ dt.f18 | moment }}</td>
												<td>{{ dt.f12 }}</td>
												<td>{{ dt.f16 }}</td>
												<td v-if="dt.f2 === 'SM'">{{ dt.f5 }}</td>
												<td v-else></td>
												<td>{{ dt.f6 }}</td>
												<td>{{ dt.f8 }}</td>
												<td>{{ dt.f10 }}</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-12">
									<label>Alasan</label>
									<textarea class="form-control" rows="3" style="resize: none;" v-model="v_alasan_refuse"></textarea>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" data-dismiss="modal" @click="refuse_tolak(refuse_dp)">Simpan & Tolak</button>
							<button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	`,
	components: {
		vuejsDatepicker
	},
	data() {
		return {
			loading: false,
			tabs2: [
				{
					name: 'KIRIM',
					icon: '<i class="fa fa-print"></i>',
					counter: 0,
				},
				{
					name: 'AMBIL',
					icon: '<i class="fa fa-print"></i>',
					counter: 1,
				},
				{
					name: 'PENGALIHAN',
					icon: '<i class="fa fa-print"></i>',
					counter: 2,
				},
				{
					name: 'OUTSTANDING',
					icon: '<i class="fa fa-print"></i>',
					counter: 3,
				}
			],
			options_ritase: [
				{ text: '...', value: '0' },
				{ text: '1', value: '1' },
				{ text: '2', value: '2' },
				{ text: '3', value: '3' },
				{ text: '4', value: '4' },
				{ text: '5', value: '5' },
			],
			view_list_plan: true,
			view_dp_plan: false,
			view_dp_plan_ambil: false,
			view_dp_plan_edit_kirim: false,
			view_dp_plan_edit_ambil: false,
			view_dp_plan_transfer_kirim: false,
			view_dp_plan_transfer_terima: false,
			view_pengiriman_kirim: false,
			view_pengiriman_ambil: false,
			view_pengiriman_transfer: false,
			dis_plan_list_kirim: [],
			dis_plan_list_ambil: [],
			dis_plan_list_transfer: [],
			data: [],
			show: false,
			id: 0,
			data: [],
			data_ambil: [],
			v_gd_start: '',
			v_nama_supir: '',
			v_no_plat: '',
			tgl_kirim: '',
			v_alasan: '',
			v_qty: '',
			v_kuantiti: '',
			dt_truck: [],
			dt_supir: [],
			dt_gudang: [],
			v_nopol_truck: "",
			v_sopir_name: "",
			v_totalPengiriman: '',
			v_alsan_terlambat: [],
			v_tgl_perintah_kirim: '',
			v_tgl_kirim: '',
			v_pilih_truck: '',
			v_gudang: '',
			v_catatan: '',
			v_catatan_to: '',
			v_ritase: '0',
			v_notransfer: '',
			v_tgl_terima: '',
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 259200000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
			detail_dp_kirim: [],
			detail_dp_ambil: [],
			no_transfer: [],
			detail_dp_transfer: [],
			detail_kirim_byid: [],
			detail_kirim_byid_item: [],
			detail_ambil_byid: [],
			detail_ambil_byid_item: [],
			detail_transfer_byid: [],
			detail_transfer_byid_item: [],
			xno_dp: "0",
			xxno_dp: "00",
			xxxno_dp: "",
			terms: true,
			dataAlasan: [
				// { alasan: 'Hari Minggu', },
				// { alasan: 'Gandengan', },
				{ alasan: 'Hari Libur', },
				{ alasan: 'Truk Rusak', },
				{ alasan: 'Toko Tutup', },
				{ alasan: 'System Error', },
				{ alasan: 'Retur', },
				{ alasan: 'Tidak Ada Truck', },
				{ alasan: 'Semen Kosong', }
			],
			oldSO: '',
			oldSO_ambil: '',
			dt_notrans: [],
			v_noTrans: null,
			new_data_pengalihan: [],
			new_catatan_to: '',
			refuse_dp: [],
			v_alasan_refuse: '',
			listSO_ambil: [],
			listSO_kirim_less: [],
			listSO_kirim_more: []
		}
	},
	// provide: {
	// 	location: this.loc
	// },
	props: ["username", "where", "usergroup", "loc", "where2"],
	created() {
		// let tgl = tanggal;
		EventBus.$on('loadSOByTanggal', (tgl) => {
			// console.log(tgl);
			this.getDataSOAmbil(tgl);
		});
		this.getDisPlanListKirimByDate(moment(new Date().toISOString()).format('YYYY-MM-DD'))
		this.getDisPlanListAmbil();
		this.getDisPlanListTransfer();

		EventBus.$on('refresh_so_sc', () => {
			// this.getDataSOKirim();
			this.getDataSOKirimNew();
		});
		
		this.getDataSOKirimNew();

		this.getDataSOAmbil(moment(new Date().toISOString()).format('YYYY-MM-DD'));

		EventBus.$on("open_modal_proses_kirim", (data) => {
			this.v_nopol_truck = "";
			this.v_sopir_name = "";
			this.v_gudang = "";
			// this.v_tgl_kirim = "";
			this.v_alasan = "";
			this.v_ritase = '0';
			// this.data = [];

			this.data = data;
			this.getNow();
			this.getTruck();
			this.getSopir();
			this.getGudang();
			this.countDate();

			this.view_dp_plan = true;
			this.view_dp_plan_ambil = false;
			this.view_dp_plan_edit_kirim = false;
			this.view_dp_plan_edit_ambil = false;
			this.view_dp_plan_transfer_kirim = false;
			this.view_dp_plan_transfer_terima = false;
			this.view_list_plan = false;
			this.view_pengiriman_kirim = false;
			this.view_pengiriman_ambil = false;
			this.view_pengiriman_transfer = false;
		});

		EventBus.$on("open_modal_edit_kirim", (data) => {
			this.getGudang();
			this.getTruck();
			this.getSopir();

			this.getDisPlanKirimByDP(data.no_dp);

			this.view_dp_plan = false;
			this.view_dp_plan_ambil = false;
			this.view_dp_plan_edit_kirim = true;
			this.view_dp_plan_edit_ambil = false;
			this.view_dp_plan_transfer_kirim = false;
			this.view_dp_plan_transfer_terima = false;
			this.view_list_plan = false;
			this.view_pengiriman_kirim = false;
			this.view_pengiriman_ambil = false;
			this.view_pengiriman_transfer = false;

		});

		EventBus.$on('open_modal_proses_ambil', (data) => {
			this.v_nopol_truck = "";
			this.v_sopir_name = "";
			this.v_gudang = "";
			// this.v_tgl_kirim = "";
			this.v_alasan = "";
			this.v_ritase = '0';
			this.data = "";

			this.data_ambil = data;
			this.getTruck();
			this.getSopir();
			this.getGudang();

			console.log(this.data_ambil);

			this.view_dp_plan = false;
			this.view_dp_plan_ambil = true;
			this.view_dp_plan_edit_kirim = false;
			this.view_dp_plan_edit_ambil = false;
			this.view_dp_plan_transfer_kirim = false;
			this.view_dp_plan_transfer_terima = false;
			this.view_list_plan = false;
			this.view_pengiriman_kirim = false;
			this.view_pengiriman_ambil = false;
			this.view_pengiriman_transfer = false;
			this.v_kuantiti = this.data_ambil.detail[0].f10;
		});

		EventBus.$on("open_modal_edit_ambil", (data) => {
			this.getGudang();

			this.getDisPlanAmbilByDP(data.no_dp);

			this.view_dp_plan = false;
			this.view_dp_plan_ambil = false;
			this.view_dp_plan_edit_kirim = false;
			this.view_dp_plan_edit_ambil = true;
			this.view_dp_plan_transfer_kirim = false;
			this.view_dp_plan_transfer_terima = false;
			this.view_list_plan = false;
			this.view_pengiriman_kirim = false;
			this.view_pengiriman_ambil = false;
			this.view_pengiriman_transfer = false;

		});

		EventBus.$on("open_modal_transfer_kirim", (data) => {
			this.getGudang();
			this.getNoTransfer(data.wh_code);
			this.getDisPlanKirimByDP(data.no_dp);

			this.view_dp_plan = false;
			this.view_dp_plan_ambil = false;
			this.view_dp_plan_edit_kirim = false;
			this.view_dp_plan_edit_ambil = false;
			this.view_dp_plan_transfer_kirim = true;
			this.view_dp_plan_transfer_terima = false;
			this.view_list_plan = false;
			this.view_pengiriman_kirim = false;
			this.view_pengiriman_ambil = false;
			this.view_pengiriman_transfer = false;

		});

		EventBus.$on("open_modal_proses_transfer", (data) => {
			// this.getDisPlanTransferByDP(data.no_dp);

			$("#modal_proses_transfer").modal("show");

			this.new_data_pengalihan = data;
			console.log(data);

		});

		EventBus.$on("open_modal_proses_refuse", (data) => {

			this.refuse_dp = data;
			$("#modal_proses_refuse").modal('show');

		});

		EventBus.$on("load_distribution_kirim", () => {
			this.getDisPlanListTransfer();
			this.getDisPlanListKirim();
			this.getDisPlanListAmbil();
		});

		EventBus.$on("load_distribution_transfer", () => {
			this.getDisPlanListTransfer();
			this.getDisPlanListKirim();
			this.getDisPlanListAmbil();
		});

		EventBus.$on('load_distribution_kirim_by_date', (date) => {
			this.getDisPlanListTransfer();
			this.getDisPlanListKirimByDate(date);
			this.getDisPlanListAmbil();
		});

		EventBus.$on('load_distribution_kirim_by_date_up', (date) => {
			this.getDisPlanListTransfer();
			this.getDisPlanListKirimByDateUp(date);
			this.getDisPlanListAmbil();
		});

		EventBus.$on("load_distribution_ambil", () => {
			this.getDisPlanListTransfer();
			this.getDisPlanListKirim();
			this.getDisPlanListAmbil();
		});

		EventBus.$on("load_so_kirim_new", () => {
			this.getDataSOAmbil(moment(new Date().toISOString()).format('YYYY-MM-DD'));
		});

		this.getNow();
	},
	methods: {
		getDataSOKirimNew() {
			this.loading = true;

			axios.post(this.action, {
				case: 'getDataSOKirimNew2',
				username: this.username,
				where: this.where
			}).then(response => {
				this.loading = false;
				this.listSO_kirim_less = response.data.less;
				this.listSO_kirim_more = response.data.more;
				console.log(this.listSO_kirim_less)
				console.log(this.listSO_kirim_more)
			});
		},
		cetakDOTransfer(id) {

			axios.post(this.action, {
				case: 'updateCatatanTo',
				id: id,
				catatan_to: this.new_catatan_to,
				tgl_terima: this.v_tgl_terima
			}).then(response => {

				this.new_catatan_to = "";
				//catatanto
				function submit_hidden_form(url, params) {
					var f = $("<form method='POST' target='_blank' style='display:none;'></form>").attr({
						action: url
					}).appendTo(document.body);
					for (var i in params) {
						if (params.hasOwnProperty(i)) {
							$('<input type="hidden" />').attr({
								name: i,
								value: params[i]
							}).appendTo(f);
						}
					}
					f.submit();
					f.remove();
				}

				submit_hidden_form(this.doPage3, {
					dp_id: id,
				})
			});

		},
		btn_list_plan() {
			this.view_dp_plan = false;
			this.view_dp_plan_ambil = false;
			this.view_dp_plan_edit_kirim = false;
			this.view_dp_plan_edit_ambil = false;
			this.view_dp_plan_transfer_kirim = false;
			this.view_dp_plan_transfer_terima = false;
			this.view_list_plan = true;
			this.view_pengiriman_kirim = false;
			this.view_pengiriman_ambil = false;
			this.view_pengiriman_transfer = false;

			this.getDisPlanListKirimByDate(moment(new Date().toISOString()).format('YYYY-MM-DD'))
			this.getDisPlanListAmbil();

			this.xno_dp = "";
		},
		getDisPlanListKirim() {
			this.loading = true;
			axios.post(this.action, {
				case: 'getDisPlanListKirim',
				loc: this.loc,
				where: this.where
			}).then(response => {
				console.log(response.data);
				if (response.data != null) {
					this.dis_plan_list_kirim = response.data;
				} else {
					this.dis_plan_list_kirim = [];
				}
				this.loading = false;
				EventBus.$emit('datainvoice', this.printed_invoice);
			});
		},
		getDisPlanListKirimByDate(date) {
			this.loading = true;
			axios.post(this.action, {
				case: 'getDisPlanListKirimByDate',
				loc: this.loc,
				where: this.where,
				date: date
			}).then(response => {
				console.log(response.data);
				if (response.data != null) {
					this.dis_plan_list_kirim = response.data;
				} else {
					this.dis_plan_list_kirim = [];
				}
				this.loading = false;
				EventBus.$emit('dataDP', this.dis_plan_list_kirim);
			});
		},
		getDisPlanListKirimByDateUp(date) {
			this.loading = true;
			axios.post(this.action, {
				case: 'getDisPlanListKirimByDateUp',
				loc: this.loc,
				where: this.where,
				date: date
			}).then(response => {
				console.log(response.data);
				if (response.data != null) {
					this.dis_plan_list_kirim = response.data;
				} else {
					this.dis_plan_list_kirim = [];
				}
				this.loading = false;
				EventBus.$emit('dataDP', this.dis_plan_list_kirim);
			});
		},
		getDisPlanListAmbil() {
			this.loading = true;
			axios.post(this.action, {
				case: 'getDisPlanListAmbil',
				loc: this.loc,
				where: this.where
			}).then(response => {
				if (response.data != null) {
					this.dis_plan_list_ambil = response.data;
				} else {
					this.dis_plan_list_ambil = [];
				}
				this.loading = false;
				EventBus.$emit('dataDP', this.dis_plan_list_kirim);
			});
		},
		getDisPlanListTransfer() {
			axios.post(this.action, {
				case: 'getDisPlanListTransfer',
				loc: this.loc,
				where: this.where
			}).then(response => {
				if (response.data != null) {
					this.dis_plan_list_transfer = response.data;
				} else {
					this.dis_plan_list_transfer = [];
				}
				this.loading = false;
			});
		},
		getNoTransfer(wh_code) {
			axios.post(this.action, {
				case: "getNoTransfer",
				wh_code: wh_code
			}).then(response => {
				this.no_transfer = response.data;
			})
		},
		getSopirByNoto(no_to) {
			console.log(no_to);
			axios.post(this.action, {
				case: "getSopirByNoto",
				no_to: no_to
			}).then(response => {
				this.v_nopol_truck = response.data.car_no.trim();
				this.v_sopir_name = response.data.driver.trim();

				console.log(this.v_nopol_truck);
			})
		},
		countDate2() {
			// console.log(this.data);

			this.v_alsan_terlambat = [];

			var tgl_kirim = this.data.tgl_perintah2;

			for (let i = 0; i < this.data.item.length; i++) {

				var tgl_perintah = moment(this.data.item[i].tanggal).format("YYYY-MM-DD");

				dt1 = new Date(tgl_perintah);
				dt2 = new Date(tgl_kirim);

				this.v_totalPengiriman = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));

				// console.log(this.v_totalPengiriman);

				if (this.v_totalPengiriman > 2) {
					this.v_alsan_terlambat.push(this.data.item[i]);
				}

			}
		},
		countDate() {
			// console.log(this.data);

			this.v_alsan_terlambat = [];

			var tgl_kirim = this.v_tgl_kirim;

			for (let i = 0; i < this.data.detail.length; i++) {

				var tgl_perintah = '';

				if (this.data.detail[i].f21 != null) {
					tgl_perintah = moment(this.data.detail[i].f21).format("YYYY-MM-DD");
				} else {
					tgl_perintah = moment(this.data.detail[i].f4).format("YYYY-MM-DD");
				}

				dt1 = new Date(tgl_perintah);
				dt2 = new Date(tgl_kirim);

				this.v_totalPengiriman = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));

				// console.log(this.v_totalPengiriman);

				if (this.v_totalPengiriman > 1) {
					this.v_alsan_terlambat.push(this.data.detail[i]);
				}

			}

			console.log(this.v_alsan_terlambat);
		},
		getTruck() {
			axios.post(this.action, {
				case: "getTruck",
				where: this.where
			}).then(response => {
				this.dt_truck = response.data;
			})
		},
		getSopir() {
			axios.post(this.action, {
				case: "getSopir",
				where: this.where
			}).then(response => {
				this.dt_supir = response.data;
			})
		},
		getGudang() {
			axios.post(this.action, {
				case: "getGudang",
				username: this.username,
			}).then(response => {
				this.dt_gudang = response.data;
			})
		},
		cekNoTransfer() {
			this.dt_notrans = [];
			axios.post(this.action, {
				case: 'getNoTransferByGudang',
				wh_code: this.v_gudang
			}).then(response => {
				let rspn = response.data;
				if (rspn != null) {
					this.dt_notrans = response.data;
				} else {
					this.dt_notrans = [];
				}
			});
		},
		async updateDP() {
			// let v_xritase = '';

			// if (this.v_pilih_truck == 'truck_dalam') {
			// 	this.v_ritase = this.v_ritase
			// } else {
			// 	this.v_ritase = 0
			// }

			if (this.v_totalPengiriman > 1) {
				await axios.post(this.action, {
					case: 'createTerlambat',
					username: this.username,
					terlambat: this.v_totalPengiriman,
					alasan: this.v_alasan,
					dt_late: this.v_alsan_terlambat
				}).then(response => {
					console.log(response.data);
				});
			}

			await axios.post(this.action, {
				case: 'updateDistributionPlan',
				id: this.data.id,
				gudang: this.v_gudang,
				tglkirim: this.v_tgl_kirim,
				ritaseplan: this.v_ritase,
				catatan: this.data.catatan,
				sopir: this.v_sopir_name,
				no_pol: this.v_nopol_truck,
				dt_so: this.data,
				gudangtransfer: this.v_noTrans
			}).then(response => {

				console.log(response.data);

				this.v_nopol_truck = "";
				this.v_sopir_name = "";
				this.v_gudang = "";
				this.v_tgl_kirim = "";
				this.v_alasan = "";
				this.v_ritase = "";
				this.detail_kirim_byid = []

				if (response.data.message == true) {
					function submit_hidden_form(url, params) {
						var f = $("<form method='POST' target='_blank' style='display:none;'></form>").attr({
							action: url
						}).appendTo(document.body);
						for (var i in params) {
							if (params.hasOwnProperty(i)) {
								$('<input type="hidden" />').attr({
									name: i,
									value: params[i]
								}).appendTo(f);
							}
						}
						f.submit();
						f.remove();
					}

					let page = '';

					if (this.username == 'rachmadi') {
						// page = '../../inventory/distributionplan_cetak_do_bak';
						page = this.doPage3;
					} else {
						page = this.doPage3;
					}

					submit_hidden_form(page, {
						dp_id: this.data.id,
					})
				} else {
					alert(response.data.response);
				}



				// this.data = "";

				// axios.post(this.action, {
				// 	case: 'getDaftarPengirimanKirimByID',
				// 	id_dt: this.data.id,
				// }).then(response => {
				// 	this.data = "";
				// 	this.detail_kirim_byid = response.data
				// 	this.detail_kirim_byid_item = response.data[0].item

				// 	this.view_dp_plan = false;
				// 	this.view_dp_plan_ambil = false;
				// 	this.view_dp_plan_edit_kirim = false;
				// 	this.view_dp_plan_edit_ambil = false;
				// 	this.view_dp_plan_transfer_kirim = false;
				// 	this.view_dp_plan_transfer_terima = false;
				// 	this.view_list_plan = false;
				// 	this.view_pengiriman_kirim = true;
				// 	this.view_pengiriman_ambil = false;
				// 	this.view_pengiriman_transfer = false;

				// 	this.getDisPlanListKirim();
				// 	this.getDisPlanListAmbil();

				// 	console.log(this.detail_kirim_byid);
				// 	console.log(this.detail_kirim_byid_item);
				// });
			});
		},
		updateDPAmbil() {
			axios.post(this.action, {
				case: 'updateDistributionPlanAmbil',
				nopol_truck: this.v_nopol_truck,
				sopir_name: this.v_sopir_name,
				gudang: this.v_gudang,
				dt_so: this.data_ambil,
			}).then(response => {

				this.v_nopol_truck = "";
				this.v_sopir_name = "";
				this.v_gudang = "";
				// this.data_ambil = "";

				axios.post(this.action, {
					case: 'getDaftarPengirimanAmbilByID',
					id_dt: this.data_ambil.id
				}).then(response => {
					this.data = "";
					this.detail_ambil_byid = response.data
					this.detail_ambil_byid_item = response.data[0].item

					this.view_dp_plan = false;
					this.view_dp_plan_ambil = false;
					this.view_dp_plan_edit_kirim = false;
					this.view_dp_plan_edit_ambil = false;
					this.view_dp_plan_transfer_kirim = false;
					this.view_dp_plan_transfer_terima = false;
					this.view_list_plan = false;
					this.view_pengiriman_kirim = false;
					this.view_pengiriman_ambil = true;
					this.view_pengiriman_transfer = false;

					this.getDisPlanListKirim();
					this.getDisPlanListAmbil();

					console.log(this.detail_ambil_byid);
					console.log(this.detail_ambil_byid_item);
				});
			});
		},
		updateDPAmbil2() {
			axios.post(this.action, {
				case: 'updateDistributionPlanAmbil2',
				nopol_truck: this.v_nopol_truck,
				sopir_name: this.v_sopir_name,
				gudang: this.v_gudang,
				dt_so: this.data_ambil.id,
			}).then(response => {

				let rspn = response.data;

				this.v_nopol_truck = "";
				this.v_sopir_name = "";
				this.v_gudang = "";
				// this.data_ambil = "";

				this.$toasted.success(rspn.message, { duration: 3000 });
				$('#modal_rencana_ambil').modal('hide');

				function submit_hidden_form(url, params) {
					var f = $("<form method='POST' target='_blank' style='display:none;'></form>").attr({
						action: url
					}).appendTo(document.body);
					for (var i in params) {
						if (params.hasOwnProperty(i)) {
							$('<input type="hidden" />').attr({
								name: i,
								value: params[i]
							}).appendTo(f);
						}
					}
					f.submit();
					f.remove();
				}

				submit_hidden_form(this.doPage3, {
					dp_id: this.data_ambil.id,
				})
				// location.reload();
			});
		},
		getDisPlanKirimByDP(no_dp) {
			axios.post(this.action, {
				case: "getDisPlanKirimByDP",
				where: this.where,
				loc: this.loc,
				no_dp: no_dp,
			}).then(response => {

				this.detail_dp_kirim = response.data[0];
				console.log(this.detail_dp_kirim);

				this.v_nopol_truck = this.detail_dp_kirim.no_pol;
				this.v_sopir_name = this.detail_dp_kirim.sopir;
				this.v_gudang = this.detail_dp_kirim.wh_code;
				this.v_tgl_kirim = this.detail_dp_kirim.tgl_kirim;
				this.v_ritase = this.detail_dp_kirim.ritase;
				this.v_alasan = this.detail_dp_kirim.catatan;
				this.data = this.detail_dp_kirim;
			})
		},
		getDisPlanAmbilByDP(no_dp) {
			axios.post(this.action, {
				case: "getDisPlanAmbilByDP",
				no_dp: no_dp,
			}).then(response => {
				this.detail_dp_ambil = response.data[0];

				this.v_nopol_truck = this.detail_dp_ambil.no_pol;
				this.v_sopir_name = this.detail_dp_ambil.sopir;
				this.v_gudang = this.detail_dp_ambil.wh_code;
				this.v_kuantiti = this.detail_dp_ambil.item[0].item_qty;
				this.data = this.detail_dp_ambil;
			})
		},
		getDisPlanTransferByDP(no_dp) {
			axios.post(this.action, {
				case: "getDisPlanTransferByDP",
				no_dp: no_dp,
			}).then(response => {
				this.detail_dp_transfer = response.data[0];

				this.v_nopol_truck = this.detail_dp_transfer.no_pol;
				this.v_sopir_name = this.detail_dp_transfer.sopir;
				this.v_gudang = this.detail_dp_transfer.wh_code;
				this.v_ritase = this.detail_dp_transfer.ritase;
				this.v_alasan = this.detail_dp_transfer.catatan_so;
				this.data = this.detail_dp_transfer;
			})
		},
		updateDetailDPKirim() {
			axios.post(this.action, {
				case: 'updateDistributionPlanDetail',
				nopol_truck: this.v_nopol_truck,
				sopir_name: this.v_sopir_name,
				gudang: this.v_gudang,
				no_to: this.v_notransfer,
				tgl_terima: this.v_tgl_terima,
				tgl_kirim: this.v_tgl_kirim,
				alasan: this.v_alasan,
				ritase: '0',
				catatan_to: this.v_catatan_to,
				dt_so: this.detail_dp_kirim.id,
			}).then(response => {
				console.log(response.data);

				this.v_nopol_truck = "";
				this.v_sopir_name = "";
				this.v_tgl_terima = "";
				this.v_catatan_to = "";
				// this.detail_dp_kirim = "";

				if (response.data.message == true) {
					function submit_hidden_form(url, params) {
						var f = $("<form method='POST' target='_blank' style='display:none;'></form>").attr({
							action: url
						}).appendTo(document.body);
						for (var i in params) {
							if (params.hasOwnProperty(i)) {
								$('<input type="hidden" />').attr({
									name: i,
									value: params[i]
								}).appendTo(f);
							}
						}
						f.submit();
						f.remove();
					}

					submit_hidden_form(this.doPage3, {
						dp_id: this.detail_dp_kirim.id,
					})
				} else {
					alert(response.data.response);
				}

				// axios.post(this.action, {
				// 	case: 'getDaftarPengirimanTransferByID',
				// 	id_dt: this.data.id,
				// }).then(response => {
				// 	this.data = "";
				// 	this.detail_transfer_byid = response.data
				// 	this.detail_transfer_byid_item = response.data[0].item

				// 	this.view_dp_plan = false;
				// 	this.view_dp_plan_ambil = false;
				// 	this.view_dp_plan_edit_kirim = false;
				// 	this.view_dp_plan_edit_ambil = false;
				// 	this.view_dp_plan_transfer_kirim = false;
				// 	this.view_dp_plan_transfer_terima = false;
				// 	this.view_list_plan = false;
				// 	this.view_pengiriman_kirim = false;
				// 	this.view_pengiriman_ambil = false;
				// 	this.view_pengiriman_transfer = true;

				// 	this.getDisPlanListKirim();
				// 	this.getDisPlanListAmbil();

				// 	console.log(this.detail_transfer_byid);
				// 	console.log(this.detail_transfer_byid_item);
				// });
			});
		},
		updateDetailDPAmbil() {
			axios.post(this.action, {
				case: 'updateDistributionPlanAmbil',
				nopol_truck: this.v_nopol_truck,
				sopir_name: this.v_sopir_name,
				gudang: this.v_gudang,
				tgl_kirim: this.v_tgl_kirim,
				alasan: this.v_alasan,
				ritase: this.v_ritase,
				dt_so: this.data,
			}).then(response => {

				this.v_nopol_truck = "";
				this.v_sopir_name = "";
				this.v_gudang = "";
				this.v_tgl_kirim = "";
				this.v_alasan = "";
				this.v_ritase = "";
				this.data = "";

				this.view_dp_plan = false;
				this.view_dp_plan_ambil = false;
				this.view_dp_plan_edit_kirim = false;
				this.view_dp_plan_edit_ambil = false;
				this.view_dp_plan_transfer_kirim = false;
				this.view_dp_plan_transfer_terima = false;
				this.view_list_plan = true;
				this.view_pengiriman_kirim = false;
				this.view_pengiriman_ambil = false;
				this.view_pengiriman_transfer = false;


				this.getDisPlanListKirim();
				this.getDisPlanListAmbil();
			});
		},
		updateTransferDPKirim() {
			axios.post(this.action, {
				case: 'updateTransferDPKirim',
				nopol_truck: this.v_nopol_truck,
				sopir_name: this.v_sopir_name,
				gudang: this.v_gudang,
				tgl_kirim: this.v_tgl_kirim,
				ritase: this.v_ritase,
				no_to: this.v_notransfer,
				alasan: this.v_alasan,
				dt_so: this.data,
			}).then(response => {
				console.log(response.data);

				this.v_nopol_truck = "";
				this.v_sopir_name = "";
				this.v_gudang = "";
				this.v_notransfer = "";
				this.v_tgl_kirim = "";
				this.v_alasan = "";
				this.v_ritase = "";
				this.data = "";

				this.view_dp_plan = false;
				this.view_dp_plan_ambil = false;
				this.view_dp_plan_edit_kirim = false;
				this.view_dp_plan_edit_ambil = false;
				this.view_dp_plan_transfer_kirim = false;
				this.view_dp_plan_transfer_terima = false;
				this.view_list_plan = true;
				this.view_pengiriman_kirim = false;
				this.view_pengiriman_ambil = false;
				this.view_pengiriman_transfer = false;

				this.getDisPlanListKirim();
				this.getDisPlanListAmbil();
			});
		},
		updateProsesTransferDPKirim() {
			axios.post(this.action, {
				case: 'updateProsesTransferDPKirim',
				nopol_truck: this.v_nopol_truck,
				sopir_name: this.v_sopir_name,
				tgl_terima: this.v_tgl_terima,
				catatan_to: this.v_catatan_to,
				dt_so: this.data,
			}).then(response => {
				console.log(response.data);

				this.v_nopol_truck = "";
				this.v_sopir_name = "";
				this.v_tgl_terima = "";
				this.v_catatan_to = "";
				// this.data = "";

				axios.post(this.action, {
					case: 'getDaftarPengirimanTransferByID',
					id_dt: this.data.id,
				}).then(response => {
					this.data = "";
					this.detail_transfer_byid = response.data
					this.detail_transfer_byid_item = response.data[0].item

					this.view_dp_plan = false;
					this.view_dp_plan_ambil = false;
					this.view_dp_plan_edit_kirim = false;
					this.view_dp_plan_edit_ambil = false;
					this.view_dp_plan_transfer_kirim = false;
					this.view_dp_plan_transfer_terima = false;
					this.view_list_plan = false;
					this.view_pengiriman_kirim = false;
					this.view_pengiriman_ambil = false;
					this.view_pengiriman_transfer = true;

					this.getDisPlanListKirim();
					this.getDisPlanListAmbil();

					console.log(this.detail_transfer_byid);
					console.log(this.detail_transfer_byid_item);
				});
			});
		},
		cetak_do(data) {
			function submit_hidden_form(url, params) {
				var f = $("<form method='POST' target='_blank' style='display:none;'></form>").attr({
					action: url
				}).appendTo(document.body);
				for (var i in params) {
					if (params.hasOwnProperty(i)) {
						$('<input type="hidden" />').attr({
							name: i,
							value: params[i]
						}).appendTo(f);
					}
				}
				f.submit();
				f.remove();
			}

			submit_hidden_form(
				this.doPage, {
				id: data.id,
			}
			)
		},
		getSupirByNopol() {
			axios.post(this.action, {
				case: "getSupirByNopol",
				no_pol: this.v_nopol_truck,
			}).then(response => {
				// console.log(response.data.data.length);
				if (response.data.data != "") {
					this.v_sopir_name = response.data.data.sopir;
				} else {
					this.v_sopir_name = "";
				}
			})

			// axios.post(this.action, {
			// 	case: "getRitaseByNopol",
			// 	no_pol: this.v_nopol_truck,
			// }).then(response => {
			// 	// console.log(response.data.data[0].sopir);
			// 	this.v_ritase = response.data.data[0].standar_rit;
			// })
		},
		getSupirBySupir() {
			this.v_sopir_name = this.v_sopir_name.sopir;
			console.log(this.v_sopir_name);
		},
		getRitaseByRitase() {
			console.log(this.v_ritase);
		},
		getNow() {
			const today = new Date();
			const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
			const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			const dateTime = date + ' ' + time;
			this.v_tgl_perintah_kirim = dateTime;
			this.v_tgl_kirim = dateTime;
			this.v_tgl_terima = dateTime;
		},
		async refuse_tolak(data) {
			await axios.post(this.action, {
				case: 'deleteDistributionPlan',
				id_dp: data.id,
				item: data.item
			}).then(response => {
				console.log(response.data);
			});

			await axios.post(this.action, {
				case: 'updateAlasanRefuse',
				sc_alasan: this.v_alasan_refuse,
				so_no: data.detail[0].f6,
				ledger: data.detail[0].f7,
				name: data.detail[0].f16,
				username: this.username
			}).then(response => {
				this.getDisPlanListKirimByDate(moment(new Date().toISOString()).format('YYYY-MM-DD'))
				this.getDisPlanListAmbil();
				this.getDisPlanListTransfer();
			});
		},
		getDataSOAmbil(tgl) {
			this.loading = true;

			axios.post(this.action, {
				case: 'getDataSOAmbil',
				username: this.username,
				where: this.where,
				tgl: tgl
			}).then(response => {
				this.loading = false;
				if (response.data != null) {
					this.listSO_ambil = response.data;
				} else {
					this.listSO_ambil = [];
				}
			});
		},
	},
	computed: {
		isDisabled: function () {
			if (this.v_nopol_truck == '' || this.v_sopir_name == '' || this.v_gudang == '') {
				this.terms = true;
			} else {
				this.terms = false;
			}
			return this.terms;
		},
		truk_luar: function () {
			if (this.v_pilih_truck == 'truck_dalam') {
				this.v_ritase = this.v_ritase
			} else {
				this.v_ritase = 0
			}
			return this.v_ritase;
		}
	},
	filters: {
		moment: function (date) {
			return moment(date).format('YYYY-MM-DD');
		}
	}

});

Vue.component('tabs_distribution_plan_component', {

	template: `
	<div>
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li v-for="tab in tabs" :class="{ 'active': tab.isActive }">
					<a :href="tab.href" @click="selectTab(tab)"><span v-html="tab.icon"></span> {{tab.name}}</a>
				</li>
			</ul>
		</div>
		<div class="tab-content">
			<slot></slot>
		</div>
	</div>
	`,
	data() {
		return {
			tabs: []
		};
	},
	created() {
		this.tabs = this.$children;
	},
	methods: {
		selectTab(selectedTab) {
			this.tabs.forEach(tab => {
				tab.isActive = (tab.name == selectedTab.name);
			});
		}
	}

});

Vue.component('tab_distribution_plan_component', {

	template: `
	<div v-show="isActive">
		<slot></slot>
	</div>
	`,
	props: {
		name: { required: true },
		selected: { default: false },
		icon: { required: true }
	},

	data() {
		return {
			isActive: false
		};
	},
	computed: {
		href() {
			return '#' + this.name.toLowerCase().replace(/ /g, '-');
		}
	},

	mounted() {
		this.isActive = this.selected;
	}

});

Vue.component('dis-plan-kirim-component2', {

	template: `
	<div>
		<div class="row">
			<div class="col-md-12" style="text-align: end;">
				<button type="button" class="btn btn-xm btn-success" @click="load"><i class="fa fa-refresh"></i></button>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<v-client-table :data="data" :columns="col_kirim" :options="opt_kirim"></v-client-table>
			</div>	
		</div>
	</div>
	`,
	props: {
		username: {
			required: true
		},
		loc: {
			required: true
		},
		data: {
			type: Array,
			required: true,
		}
	},
	data() {
		return {
			col_kirim: ['no_dp', 'name', 'asal_gudang', 'wh_code', 'tgl_perintah2', 'action'],
			opt_kirim: {
				headings: {
					no_dp: "NO. DP",
					name: "Nama",
					asal_gudang: "LOKASI KEBERANGKATAN",
					wh_code: "SO. ASAL",
					tgl_perintah2: "TGL. PERINTAH",
					action: "ACTION"
				},
				sortable: ['no_dp', 'wh_code'],
				texts: {
					count: "",
					filter: "Cari",
					filterPlaceholder: "Cari No. DP",
				},
				templates: {
					asal_gudang: "temp-dis-plan-gudang-asal",
					wh_code: "temp-dis-plan-so-asal",
					action: "action-dis-plan-kirim-comp"
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				filterable: false,
				childRow: "detail-displan-kirim-component",
				showChildRowToggler: true,
				footerHeadings: true,
				highlightMatches: true,
				columnsDropdown: false,
				uniqueKey: "id",
				childRowTogglerFirst: false,
			}
		}
	},
	methods: {
		load() {
			EventBus.$emit('load_distribution_kirim');
		}
	}

});

Vue.component('new_detail', {
	props: {
		detail: {
			type: Array,
			required: true,
		},
	},
	template: `
		<div>
			<table class="table">
				<thead>
					<tr>
						<th style="font-weight: bold; width:15%; text-align: left;" class="active">KEBERANGKATAN</th>
						<th style="font-weight: bold; width:15%; text-align: left;" class="active">TOKO</th>
						<th style="font-weight: bold; width:15%; text-align: left;" class="active">NO. SM</th>
						<th style="font-weight: bold; width:15%; text-align: left;" class="active">NO. SO</th>
						<th style="font-weight: bold; width:15%; text-align: left;" class="active">ITEM &amp; QTY</th>
						<th style="font-weight: bold; width:40%; text-align: left;" class="active">KETERANGAN</th>
					</tr>
				</thead>
				<tbody>
					<template v-for="dt in detail">
						<tr>
							<td>{{ dt['f3'] }}</td>
							<td>{{ dt['f16'] }}</td>
							<td>{{ dt['f5'] }}</td>
							<td>{{ dt['f6'] }}</td>
							<td>{{ dt['f8'] }} - {{ dt['f10'] }}</td>
							<td>{{ dt['f11'] }}</td>
						</tr>
					</template>
				</tbody>
			</table>
		</div>
	`,
	data() {
		return {
			checkedItem: [],
		}
	},
	methods: {
		addItem() {
			if (this.checkedItem.length > 0) {
				if (this.detail.length <= 1) {
					this.saveNewSO();
				} else {
					this.saveNewSO2();
				}
			} else {
				swal.fire({
					title: "Gagal Simpan DP",
					text: "Silahkan Pilih Item Terlebih Dahulu",
					icon: "error",
				}).then(function (isConfirm) {
					if (isConfirm) {
						console.log('gagal boss');
					}
				});
			}
		},
		saveNewSO() {
			console.log("Only One");
			console.log(this.data_all);
			console.log(this.checkedItem);
		},
		saveNewSO2() {
			console.log("More One");
			console.log(this.data_all);
			console.log(this.checkedItem);
		}
	}
});

Vue.component('new_detail_kirim', {
	props: {
		detail: {
			type: Array,
			required: true,
		},
		data_all: {
			type: Object,
			required: true,
		},
		username: {
			required: true
		},
		loc: {
			required: true
		},
	},
	template: `
		<div>
			<table class="table">
				<thead>
					<tr>
						<th style="font-weight: bold; width:15%; text-align: left;" class="active">KEBERANGKATAN</th>
						<th style="font-weight: bold; width:15%; text-align: left;" class="active">TOKO</th>
						<th style="font-weight: bold; width:15%; text-align: left;" class="active">NO. SM</th>
						<th style="font-weight: bold; width:15%; text-align: left;" class="active">NO. SO</th>
						<th style="font-weight: bold; width:15%; text-align: left;" class="active">ITEM &amp; QTY</th>
						<th style="font-weight: bold; width:40%; text-align: left;" class="active">KETERANGAN</th>
					</tr>
				</thead>
				<tbody>
					<template v-for="dt in detail">
						<tr>
							<td>{{ dt['f3'] }}</td>
							<td>{{ dt['f16'] }}</td>
							<td>{{ dt['f5'] }}</td>
							<td>{{ dt['f6'] }}</td>
							<td>{{ dt['f8'] }} - {{ dt['f10'] }}</td>
							<td>{{ dt['f11'] }}</td>
						</tr>
					</template>
				</tbody>
			</table>
			<div style="background: lightgray;padding: 20px;">
				<div class="row">
					<div class="col-md-3" style="text-align: start;">
						<label class="control-label text-center">Lokasi Keberangkatan</label>
					</div>
					<div class="col-md-3" style="text-align: start;">
						<label class="control-label text-center">Tanggal Kirim</label>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<v-select style="text-transform: capitalize" label="kd_off" :options="officeCode" v-model="v_office" :reduce="officeCode=>officeCode.kd_off"></v-select>
					</div>
					<div class="col-md-4">
						<vuejs-datepicker
							v-model="v_tgl_perintah_kirim"
							:format="DatePickerFormatb"
							:bootstrap-styling="true"
							:placeholder="holderb"
							required
							style="width: 290px;">
						</vuejs-datepicker>
					</div>
					<div class="col-md-3">
						<button name="btn-load" class="btn btn-info btn-flat btn-xs" style="width:100%;" @click="addItem">PINDAHKAN</button>
					</div>
				</div>
			</div>
		</div>
	`,
	components: {
		vuejsDatepicker
	},
	data() {
		return {
			checkedItem: [],
			v_tgl_perintah_kirim: moment(new Date().toISOString()).format('YYYY-MM-DD'),
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 259200000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
			v_office: this.loc,
			officeCode: [],
		}
	},
	methods: {
		getOfficeCode() {
			axios.post(this.action, {
				case: 'getOfficeCode',
				username: 'rachmadi'
			}).then(response => {
				this.officeCode = response.data.dt_kd_offifce[0];
			});
		},
		addItem2() {
			if (this.checkedItem.length > 0) {
				if (this.detail.length <= 1) {
					swal.fire({
						title: "Gagal Simpan DP",
						text: "Item Barang Hanya Satu",
						icon: "error",
					}).then(function (isConfirm) {
						if (isConfirm) {
							console.log('gagal boss');
						}
					});
				} else {
					this.saveNewSO();
				}
			} else {
				swal.fire({
					title: "Gagal Simpan DP",
					text: "Silahkan Pilih Item Terlebih Dahulu",
					icon: "error",
				}).then(function (isConfirm) {
					if (isConfirm) {
						console.log('gagal boss');
					}
				});
			}
		},
		addItem() {
			// console.log(this.data_all);
			// console.log(this.detail);
			this.saveNewSO();
		},
		saveNewSO: function () {
			axios.post(this.action, {
				case: 'savePisahDisplan',
				data_all: this.data_all,
				checked_dp: this.detail,
				username: this.username,
				office: this.v_office,
				tgl_kirim: this.v_tgl_perintah_kirim
			}).then(response => {
				console.log(response.data);
				swal.fire({
					title: "Pindahkan DP Berhasil",
					icon: "success",
				});
				EventBus.$emit('load_distribution_kirim_by_date', moment(new Date().toISOString()).format('YYYY-MM-DD'));
			});
		},
	},
	created() {
		this.getOfficeCode();
	}
});

Vue.component('dis-plan-kirim-component', {

	template: `
	<div>
		<div class="row" style="margin-top: 4%;">
			<div class="col-md-1">
				<button type="button" class="btn btn-xm btn-success" @click="load"><i class="fa fa-refresh"></i> Kemarin</button>
			</div>
			<div class="col-sm-1" style="margin-right: 36px;" v-for="date in date_range">
				<button type="button" class="btn btn-xm btn-success" @click="loadDate(date.date)">{{date.date_format}}</button>
			</div>
			<div class="col-md-1">
				<button type="button" class="btn btn-xm btn-success" @click="loadup"><i class="glyphicon glyphicon-menu-right"></i> {{v_next_week}}</button>
			</div>
			<div class="col-sm-3" style="text-align: end;">
			</div>
		</div>
		</br>
		<div class="row">
			<!-- <div class="col-md-12">
				<v-client-table style="font-size:12px;" :data="data" :columns="col_kirim" :options="opt_kirim"></v-client-table>
			</div> -->
			<div class="col-md-12">
				<table class="table table-bordered" style="font-size: 12px;">
					<thead>
						<tr>
							<th class="active" style="width:5%;">No. Rencana Kirim</th>
							<th class="active" style="width:10%;">Tgl Kirim</th>
							<th class="active" style="width:5%;">Total Berat</th>
							<th class="active" style="width:5%;">Biaya Tambahan</th>
							<th class="active">Detail</th>
							<th class="active" style="width:16%;">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<template v-for="(dt, i) in data">
							<tr>
								<td>
									<div>{{ dt.no_dp }}</div>
									<div>
										<span v-if="dt.detail[0]['f24'] !== null">
											<label class='label label-primary lb-sm'>{{ dt.detail[0]['f24'] }} => {{ dt.detail[0]['f3'] }}</label>
										</span>
										<span v-else><label class='label label-primary lb-sm'>{{ dt.detail[0]['f3'] }}</label></span>
									</div>
								</td>
								<td v-if="dt.tgl_kirim != null">{{ dt.tgl_kirim }}</td>
								<td v-else>{{ dt.created_at }} </td>
								<td>{{ dt.total_muatan }} </td>
								<td>{{ dt.total_biaya }} </td>
								<td><new_detail :detail="dt.detail"></new_detail></td>
								<td v-if="dt.sc_status == 0">
									<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
										<div class="btn-group mr-2" role="group" aria-label="First group" v-if="coce(dt.detail[0]['f3']) == true || loc == 'HO'">
											<button name="btn-load" class="btn btn-info btn-flat btn-xs" style="width:100%;" @click="accept(dt.id, dt.tgl_kirim)">TERIMA</button>
										</div>
										<div class="btn-group mr-2" role="group" aria-label="First group" v-if="coce(dt.detail[0]['f3']) == true || loc == 'HO'">
											<button name="btn-load" class="btn btn-warning btn-flat btn-xs" style="width:100%;" @click="refuse(dt)">TOLAK</button>
										</div>
										<div class="btn-group mr-2" role="group" aria-label="Second group">
											<button name="btn-load" class="btn btn-danger btn-flat btn-xs" style="width:100%;" @click="batal(dt)">BATAL</button>
										</div>
									</div>
								</td>
								<td style="width: 17%;" v-else>
									<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
										<div class="btn-group mr-2" role="group" aria-label="First group" v-if="coce(dt.detail[0]['f3']) == true || loc == 'HO'">
											<button name="btn-load" class="btn btn-info btn-flat btn-xs" style="width:100%;" @click="proses(dt)">BUAT DO</button>
										</div>
										<div class="btn-group mr-2" role="group" aria-label="First group" v-if="coce(dt.detail[0]['f3']) == true || loc == 'HO'">
											<button name="btn-load" class="btn btn-warning btn-flat btn-xs" style="width:100%;" @click="edit(dt)">EDIT DATA</button>
										</div>
										<div class="btn-group mr-2" role="group" aria-label="Second group">
											<button name="btn-load" class="btn btn-danger btn-flat btn-xs" style="width:100%;" @click="batal(dt)">BATAL</button>
										</div>
										<!-- <div class="btn-group mr-2" role="group" aria-label="Second group" v-if="coce(dt.detail[0]['f3']) == true && dt.no_pol != '' && dt.sopir != '' || loc == 'HO'">
											<button name="btn-load" class="btn btn-danger btn-flat btn-xs" style="width:100%;" @click="cetak(dt.id)">CETAK DO</button>
										</div> -->
									</div>
								</td>
							</tr>
						</template>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	`,
	props: {
		username: {
			required: true
		},
		loc: {
			required: true
		},
		data: {
			type: Array,
			required: true,
		}
	},
	inject: ['location'],
	data() {
		return {
			col_kirim: ['no_dp', 'asal', 'ritase', 'tgl_kirim', 'dtl_ledger', 'dtl_nama', 'dtl_alamat', 'dtl_keterangan', 'action'],
			opt_kirim: {
				perPage: 1000000,
				headings: {
					no_dp: "NO. RENCANA KIRIM",
					ritase: "NOMOR RITASE",
					tgl_kirim: "TGL KIRIM",
					asal: "CABANG ASAL",
					dtl_ledger: "LEDGER",
					dtl_nama: "NAMA",
					dtl_alamat: "ALAMAT",
					dtl_keterangan: "KET.",
					action: "ACTION"
				},
				sortable: ['no_dp', 'wh_code'],
				texts: {
					count: "",
					filter: "Cari",
					filterPlaceholder: "Cari No. DP",
				},
				templates: {
					action: "action-dis-plan-kirim-comp",
					asal: "temp-dis-plan-gudang-asal-new",
					ritase: function (h, row, index) {
						if (row.ritase == null) {
							return `Ritase`
						} else {
							return `Ritase ${row.ritase}`
						}
					}
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				childRow: "detail-displan-kirim-component",
				showChildRowToggler: true,
				footerHeadings: true,
				highlightMatches: true,
				columnsDropdown: false,
				uniqueKey: "id",
				childRowTogglerFirst: false,
			},
			loc_cab: this.loc,
			date_range: [],
			dt_next_week: '',
			v_next_week: '',
			v_alsan_terlambat: ''
		}
	},
	methods: {
		load() {
			EventBus.$emit('load_distribution_kirim');
		},
		proses(data) {
			EventBus.$emit('open_modal_proses_kirim', data);
		},
		edit(data) {
			EventBus.$emit('open_modal_edit_kirim', data);
		},
		transfer(data) {
			EventBus.$emit('open_modal_transfer_kirim', data);
		},
		batal(data) {
			const swalWithBootstrapButtons = Swal.mixin({
				customClass: {
					confirmButton: 'btn btn-success',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})

			swalWithBootstrapButtons.fire({
				title: 'Are you sure?',
				text: "You want to delete this data?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					axios.post(this.action, {
						case: 'deleteDistributionPlan',
						id_dp: data.id,
						item: data.item
					}).then(response => {
						console.log(response.data);
						location.reload();
					});
				} else if (
					result.dismiss === Swal.DismissReason.cancel
				) {
					swalWithBootstrapButtons.fire(
						'Cancelled',
						'Your data is safe :)',
						'error'
					)
				}
			})

			// axios.post(this.action, { 
			// 	case: 'deleteDistributionPlan',
			// 	id_dp: data.id
			// }).then(response => {
			// 	console.log(response.data);
			// });
		},
		cetak(id) {
			function submit_hidden_form(url, params) {
				var f = $("<form method='POST' target='_blank' style='display:none;'></form>").attr({
					action: url
				}).appendTo(document.body);
				for (var i in params) {
					if (params.hasOwnProperty(i)) {
						$('<input type="hidden" />').attr({
							name: i,
							value: params[i]
						}).appendTo(f);
					}
				}
				f.submit();
				f.remove();
			}

			submit_hidden_form(this.doPage3, {
				dp_id: id,
			})
		},
		coce(cabang) {
			let n = this.location.includes(cabang);
			return n;
		},
		getDateRange() {
			var date = new Date();
			var week = date.setDate(date.getDate() + 7);

			this.dt_next_week = moment(week).format('YYYY-MM-DD');
			this.v_next_week = moment(week).format('DD MMM YYYY');

			axios.post(this.action, {
				case: 'getDatesFromRange'
			}).then(response => {
				this.date_range = response.data;
			});
		},
		loadDate(date) {
			EventBus.$emit('load_distribution_kirim_by_date', date);
		},
		loadup() {
			EventBus.$emit('load_distribution_kirim_by_date_up', this.dt_next_week);
		},
		accept(id, data) {
			axios.post(this.action, {
				case: 'updateSCStatus',
				id: id
			}).then(response => {
				EventBus.$emit('load_distribution_kirim_by_date', data);
			});
		},
		refuse(data) {
			EventBus.$emit('open_modal_proses_refuse', data);
		}
	},
	computed: {
		
	},
	created() {
		this.coce();
		this.getDateRange();
	}

});

Vue.component('daf_so_kirim_new_more2', {

	template: `
	<div>
		<div class="row">
			<!-- <div class="col-md-2" style="text-align: start;">
				<label class="control-label text-center">Lokasi Berangkat</label>
			</div> -->
			<!-- <div class="col-md-2" style="text-align: start;">
				<label class="control-label text-center">Tanggal Kirim</label>
			</div> -->
		</div>
		<div class="row">
			<!-- <div class="col-md-2">
				<v-select style="text-transform: capitalize" label="kd_off" :options="officeCode" v-model="v_office" :reduce="officeCode=>officeCode.kd_off"></v-select>
			</div> -->
			<!-- <div class="col-md-2">
				<vuejs-datepicker
					v-model="v_tgl_perintah_kirim"
					:format="DatePickerFormatb"
					:disabledDates="disabledDates"
					:bootstrap-styling="true"
					:placeholder="holderb"
					required
					style="width: 290px;">
				</vuejs-datepicker>
			</div> -->
			<!-- <div class="col-md-1">
				<button type="button" class="btn btn-xm btn-primary" @click="add_SO">Simpan</button>
			</div> -->
			<!-- <div class="col-md-1">
				<button type="button" class="btn btn-xm btn-success" @click="load"><i class="fa fa-refresh"></i></button>
			</div> -->
		</div>
		<div class="row">
			<div class="col-sm-10" style="margin-top:25px;">
				<button type="button" class="btn btn-xm btn-success" @click="load"><i class="fa fa-refresh"></i></button>
			</div>
			<div class="col-sm-2">
				<label>Cari</label>
				<input type="text" class="form-control" placeholder="No. SO" v-model="v_search_so">
			</div>
		</div>
		</br>
		<div class="row">
			<div class="col-md-12" style="text-transform: capitalize;">
				<table class="table table-hover table-responsive table-striped" style="font-size: 12px;">
					<thead>
						<tr>
							<th class="active">Type</th>						
							<th class="active">Ledger</th>						
							<th class="active">Customer</th>						
							<th v-for="column in new_columns" @click="activeColumn = column" class="active">
								{{ colTitles[column.name] }} 
								<span @click="column.order = column.order * (-1), sortBy(column.name)" class="arrow" :class="column.order > 0 ? 'asc' : 'dsc'" ></span>
							</th>
							<th class="active">Alamat</th>
							<th class="active">Keterangan</th>
							<th class="active">Sales</th>
							<th class="active">Ton</th>
							<th class="active">Lokasi Berangkat</th>
							<th class="active">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<tr v-for="(dt,i) in filterListST" :key="i"  v-if="i >= pagestart && i < pageend" style="text-transform: uppercase;"
							:style="color(dt.tanggal)">
							<td>{{ dt.code_comp }}</td>
							<td>{{ dt.code }}</td>
							<td style="width: 10%;">{{ dt.ledgername }}</td>
							<td style="width: 10%;">{{ dt.tanggal }} {{ dt.jaminput }}</td>
							<td style="width: 10%;">{{ dt.delivery_time }}</td>
							<td style="width: 5%;">{{ dt.sm_no }}</td>
							<td>{{ dt.so_no }}</td>
							<td style="width: 13%;">{{ dt.item_code }}</td>
							<td>{{ dt.alamat_toko }}</td>
							<td>{{ dt.keterangan }}</td>
							<td>{{ dt.sales_code }}</td>
							<td>{{ dt.ton }}</td>
							<td><v-select style="text-transform: capitalize" label="kd_off" :options="officeCode" v-model="v_office" :reduce="officeCode=>officeCode.kd_off"></v-select></td>
							<!-- <td>
								<label class="form-checkbox">
									<input type="checkbox" :value="dt" v-model="checkedSO">
									<i class="form-icon"></i>
								</label>
							</td> --> 
							<td>
								<button type="button" class="btn btn-xs btn-primary" @click="saveSO3(dt)">Simpan</button>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			</br></br></br>
			<div class="col-md-12" style="text-transform: capitalize; text-align: center;">
			<paginate 
				:pagecount="totalpages"
				:containerclass="'pagination pagination-sm'"
				:clickhandler="pagination"
				:prevtext="prevButtonIcon"
				:nexttext="nextButtonIcon">
			</paginate>
			</div>
		</div>
	</div>
	`,
	components: {
		vuejsDatepicker
	},
	props: {
		username: {
			required: true
		},
		loc: {
			required: true
		},
		data: {
			type: Array,
			required: true
		},
	},
	data() {
		return {
			v_tgl_perintah_kirim: moment(this.v_tgl_kirim).format("YYYY-MM-DD"),
			officeCode: [],
			current_kdOff: [],
			checkedSO: [],
			v_office: this.loc,
			v_office_current: this.loc,
			columns: ['type', 'code', 'ledgername', 'tanggal', 'sm_no', 'so_no',
				'item_code', 'alamat_toko', 'keterangan', 'jaminput', 'code_comp', 'sales_code', 'status_so', 'dept_code', 'pilih'],
			pagestart: 0,
			pagecurrent: 1,
			itemsperpage: 100,
			pageend: 100,
			prevButtonIcon: '<i class="fa fa-chevron-left"></i>',
			nextButtonIcon: '<i class="fa fa-chevron-right"></i>',
			currentSort: 'name',
			currentSortDir: 'asc',
			activeColumn: {},
			new_columns: [
				// { name: 'type', order: 1 },
				// { name: 'code', order: 1 },
				// { name: 'ledgername', order: 1 },
				{ name: 'tanggal', order: 1 },
				{ name: 'delivery', order: 1 },
				{ name: 'sm_no', order: 1 },
				{ name: 'so_no', order: 1 },
				{ name: 'item_code', order: 1 },
				// { name: 'alamat_toko', order: 1 },
				// { name: 'keterangan', order: 1 },
				// { name: 'sales_code', order: 1 },
			],
			colTitles: {
				// 'type': 'Type',
				// 'code': 'Ledger',
				// 'ledgername': 'Customer',
				'tanggal': 'Tgl. SO / SM',
				'delivery': 'Tgl. Kirim',
				'sm_no': 'No. SM',
				'so_no': 'No. SO',
				'item_code': 'Item & QTY',
				// 'alamat_toko': 'Alamat',
				// 'keterangan': 'Keterangan',
				// 'sales_code': 'Sales Code',
			},
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 8640000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
			v_search_so: '',
			styleObject: {
				background: 'none',
			},
			checkedSO: [],
			totalMuat: 0,
			confirmPrice_kirim: [],
			new_arr_price_kirim: [{
				data_so: []
			}],
		}
	},
	methods: {
		getOfficeCode() {
			axios.post(this.action, {
				case: 'getOfficeCode',
				username: 'rachmadi'
			}).then(response => {
				this.officeCode = response.data.dt_kd_offifce[0];
			});
		},
		add_SO() {
			//gantung
			var total_ton = 0;

			if (this.checkedSO.length > 0) {
				this.saveSO();
			} else {
				swal.fire({
					title: "Gagal Simpan SO",
					text: "Silahkan Pilih SO Terlebih Dahulu",
					icon: "error",
				}).then(function (isConfirm) {
					if (isConfirm) {
						console.log('gagal boss');
					}
				});
			}
		},
		saveSO2() {
			axios.post(this.action, {
				case: 'saveDistributionPlanTemp',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO
			}).then(response => {
				console.log(response.data);
				if (response.data.success == 1) {
					this.$toasted.success(response.data.response, { duration: 5000 });
				} else {
					this.$toasted.error(response.data.response, { duration: 5000 });
				}
				// console.log("satu cabang");
				// EventBus.$emit('confirmPrice_kirim', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				EventBus.$emit('refresh_so_sc');
				this.checkedSO = [];
			});
		},
		saveSO() {
			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
			}).then(response => {
				console.log(response.data);
				// EventBus.$emit('confirmPrice_kirim', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				this.checkedSO = [];
			});
		},
		saveSO3(data) {
			// console.log(data); //savegantung
			let arr_so = [];
			arr_so.push(data);

			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: arr_so,
			}).then(response => {
				// console.log(response.data);
				EventBus.$emit('confirmPrice_kirim_newx', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, arr_so, data.ton);
				this.checkedSO = [];
			});
		},
		simpanHargaKirimNew() {
			for (let i = 0; i < this.confirmPrice_kirim.length; i++) {
				this.new_arr_price_kirim[0].data_so.push(this.confirmPrice_kirim[i].data_so);
			}
			// console.log(this.new_arr_price_kirim);

			axios.post(this.action, {
				case: 'simpanDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
				dt_price: this.new_arr_price_kirim,
				loc: this.loc,
				totalMuat: this.totalMuat
			}).then(response => {

				// console.log(response.data);
				// location.reload();

				this.new_arr_price_kirim[0].data_so = [];
				// this.arr_price_kirim[0].price = [];
				this.checkedSO = [];

				let rspn = response.data;
				if (rspn.success === 1) {
					console.log(response.data);
					this.load();
					this.$toasted.success(rspn.message, { duration: 5000 });
				} else {
					console.log(response.data);
					this.load();
					this.$toasted.error(rspn.message, { duration: 5000 });
				}
			});
		},
		saveSOpengalihan() {
			axios.post(this.action, {
				case: 'saveDistributionPlanKirim',
				tgl_perintah: this.v_tgl_perintah_kirim,
				username: this.username,
				office: this.v_office,
				dt_so: this.checkedSO,
			}).then(response => {
				// console.log(response.data);
				// console.log("pengalihan");
				EventBus.$emit('confirmPrice_kirim_pengalihan', response.data, this.v_tgl_perintah_kirim, this.username, this.v_office, this.checkedSO);
				this.checkedSO = [];
			});
		},
		pagination(index) {
			if (index >= 0 && index <= this.totalpages) {
				this.pagecurrent = index;
				this.pagestart = (this.itemsperpage * index) - this.itemsperpage;
				this.pageend = (this.itemsperpage * index);
			}
		},
		sortBy(column) {
			if (column === this.currentSort) {
				this.currentSortDir = this.currentSortDir === 'asc' ? 'desc' : 'asc';
			}
			this.currentSort = column;
		},
		diffDay(dateSo) {
			var dateNow = new Date().getTime();
			var dateSo = new Date(dateSo).getTime();
			var diffDay = parseInt((dateNow - dateSo) / (24 * 3600 * 1000));
			return diffDay;
		},
		getNow() {
			const today = new Date();
			const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
			const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			const dateTime = date + ' ' + time;
			this.v_tgl_perintah_kirim = dateTime;
		},
		send_mail_approve(data) {
			// console.log(data);
			this.loading = true;
			axios.post(this.action, {
				case: 'send_mail_approve',
				dt_so: data,
			}).then(response => {
				console.log(response.data);
				this.loading = false;
				this.$toasted.success(response.data, { duration: 5000 });
				// console.log("satu cabang");
				this.checkedSO = [];
			});
		},
		load() {
			EventBus.$emit('refresh_so_sc');
		},
		recordRefresh(source) {
			axios.post(this.action, {
				case: 'recordRefresh',
				username: this.username,
				source: source
			}).then(response => {
				console.log(response.data);
			});
		},
		color(tgl) {
			var dateNow = new Date().getTime();
			var dateSo = new Date(tgl).getTime();
			var diffDay = parseInt((dateNow - dateSo) / (24 * 3600 * 1000));

			if (diffDay >= 3 && diffDay < 5) {
				return 'color: orange';
			} else if (diffDay >= 5) {
				return 'color: red';
			} else {
				return 'color: black';
			}
		},
	},
	created() {
		this.getOfficeCode();
		this.getNow();
		this.recordRefresh('page');

		EventBus.$on('confirmPrice_kirim_newx', (price_data, tgl_perintah, username, v_office, checkedSO, totalMuat) => {

			this.confirmPrice_kirim = price_data;
			this.v_tgl_perintah_kirim = tgl_perintah;
			this.username = username;
			this.v_office = v_office;
			this.checkedSO = checkedSO;
			this.totalMuat = totalMuat;

			this.simpanHargaKirimNew();
		});
	},
	computed: {
		filterListST() {
			let filterST = this.v_search_so.toUpperCase();

			let result = this.data.filter(function (e) {
				let filtered = true
				if (filterST && filterST.length > 0) {
					filtered = e.so_no.includes(filterST)
				}
				return filtered;

			}).sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});

			if (result.length == 0) {
				let res = this.data.filter(function (e) {
					let filtered = true
					if (filterST && filterST.length > 0) {
						filtered = e.ledgername.includes(filterST)
					}
					return filtered;

				}).sort((a, b) => {
					let modifier = 1;
					if (this.currentSortDir === 'desc') modifier = -1;
					if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
					if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
					return 0;
				});
				return res;
			} else {
				return result;
			}
		},
		totalpages() {
			//This calculates the amount of pages
			return Math.ceil(this.filterListST.length / this.itemsperpage);
		},
		sortedData: function () {
			return this.data.sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});
		},
	},
	watch: {
		data: function () {
			this.pagecurrent = 1;
			this.pagestart = 0;
			this.pageend = this.itemsperpage;
		},
	}

});

Vue.component('dis-plan-kirim-list-component', {

	template: `
	<div>
		<div class="row" style="margin-top: 4%;">
			<div class="col-md-1">
				<button type="button" class="btn btn-xm btn-success" @click="load"><i class="fa fa-refresh"></i> Kemarin</button>
			</div>
			<div class="col-sm-1" v-for="date in date_range">
				<button type="button" class="btn btn-xm btn-success" @click="loadDate(date.date)">{{date.date_format}}</button>
			</div>
			<div class="col-md-1">
				<button type="button" class="btn btn-xm btn-success" @click="loadup"><i class="glyphicon glyphicon-menu-right"></i> {{v_next_week}}</button>
			</div>
			<div class="col-sm-3" style="text-align: end;">
			</div>
		</div>
		</br>
		<div class="row">
			<!-- <div class="col-md-12">
				<v-client-table style="font-size:12px;" :data="data" :columns="col_kirim" :options="opt_kirim"></v-client-table>
			</div> -->
			<div class="col-md-12" style="margin-bottom: 150px;">
				<table class="table table-bordered" style="font-size: 12px;">
					<thead>
						<tr>
							<th class="active" style="width:5%;">No. Rencana Kirim</th>
							<th class="active" style="width:10%;">Tgl Kirim</th>
							<th class="active" style="width:5%;">Total Berat</th>
							<th class="active" style="width:5%;">Biaya Tambahan</th>
							<th class="active">Detail</th>
							<th class="active" style="width:16%;">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<template v-for="(dt, i) in arr_data">
							<tr>
								<td>
									<div>{{ dt.no_dp }}</div>
									<div>
										<span v-if="dt.detail[0]['f24'] !== null">
											<label class='label label-primary lb-sm'>{{ dt.detail[0]['f24'] }} => {{ dt.detail[0]['f3'] }}</label>
										</span>
										<span v-else><label class='label label-primary lb-sm'>{{ dt.detail[0]['f3'] }}</label></span>
									</div>
								</td>
								<td v-if="dt.tgl_kirim != null">{{ dt.tgl_kirim }}</td>
								<td v-else>{{ dt.created_at }} </td>
								<td>{{ dt.total_muatan }} </td>
								<td>{{ dt.total_biaya }} </td>
								<td><new_detail_kirim :detail="dt.detail" :data_all="dt" :username="username" :loc="loc"></new_detail_kirim></td>
								<td v-if="dt.sc_status == 0">
									<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
										<div class="btn-group mr-2" role="group" aria-label="First group" v-if="coce(dt.detail[0]['f3']) == true || loc == 'HO'">
											<button name="btn-load" class="btn btn-info btn-flat btn-xs" @click="addItem(dt)"><i class="fa fa-cloud-upload"></i> GANDENGKAN</button>
										</div>
										<div class="btn-group mr-2" role="group" aria-label="First group" v-if="coce(dt.detail[0]['f3']) == true || loc == 'HO'">
											<button name="btn-load" class="btn btn-warning btn-flat btn-xs" style="width:100%;" @click="refuse(dt)">TOLAK</button>
										</div>
									</div>
								</td>
								<td style="width: 17%;" v-else>
									<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
										<div class="btn-group mr-2" role="group" aria-label="First group" v-if="coce(dt.detail[0]['f3']) == true || loc == 'HO'">
											<button name="btn-load" class="btn btn-info btn-flat btn-xs" @click="addItem(dt)"><i class="fa fa-cloud-upload"></i> GANDENGKAN</button>
										</div>
									</div>
								</td>
							</tr>
						</template>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	`,
	props: {
		username: {
			required: true
		},
		loc: {
			required: true
		},
		data: {
			type: Array,
			required: true,
		}
	},
	inject: ['location'],
	data() {
		return {
			col_kirim: ['no_dp', 'asal', 'ritase', 'tgl_kirim', 'dtl_ledger', 'dtl_nama', 'dtl_alamat', 'dtl_keterangan', 'action'],
			opt_kirim: {
				perPage: 1000000,
				headings: {
					no_dp: "NO. RENCANA KIRIM",
					ritase: "NOMOR RITASE",
					tgl_kirim: "TGL KIRIM",
					asal: "CABANG ASAL",
					dtl_ledger: "LEDGER",
					dtl_nama: "NAMA",
					dtl_alamat: "ALAMAT",
					dtl_keterangan: "KET.",
					action: "ACTION"
				},
				sortable: ['no_dp', 'wh_code'],
				texts: {
					count: "",
					filter: "Cari",
					filterPlaceholder: "Cari No. DP",
				},
				templates: {
					action: "action-dis-plan-kirim-comp",
					asal: "temp-dis-plan-gudang-asal-new",
					ritase: function (h, row, index) {
						if (row.ritase == null) {
							return `Ritase`
						} else {
							return `Ritase ${row.ritase}`
						}
					}
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				childRow: "detail-displan-kirim-component",
				showChildRowToggler: true,
				footerHeadings: true,
				highlightMatches: true,
				columnsDropdown: false,
				uniqueKey: "id",
				childRowTogglerFirst: false,
			},
			loc_cab: this.loc,
			date_range: [],
			dt_next_week: '',
			v_next_week: '',
			printed_dp: this.data,
			selected: [],
			arr_data: []
		}
	},
	methods: {
		load() {
			EventBus.$emit('load_distribution_kirim');
		},
		proses(data) {
			EventBus.$emit('open_modal_proses_kirim', data);
		},
		edit(data) {
			EventBus.$emit('open_modal_edit_kirim', data);
		},
		transfer(data) {
			EventBus.$emit('open_modal_transfer_kirim', data);
		},
		batal(data) {
			const swalWithBootstrapButtons = Swal.mixin({
				customClass: {
					confirmButton: 'btn btn-success',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})

			swalWithBootstrapButtons.fire({
				title: 'Are you sure?',
				text: "You want to delete this data?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					axios.post(this.action, {
						case: 'deleteDistributionPlan',
						id_dp: data.id,
						item: data.item
					}).then(response => {
						console.log(response.data);
						location.reload();
					});
				} else if (
					result.dismiss === Swal.DismissReason.cancel
				) {
					swalWithBootstrapButtons.fire(
						'Cancelled',
						'Your data is safe :)',
						'error'
					)
				}
			})

			// axios.post(this.action, { 
			// 	case: 'deleteDistributionPlan',
			// 	id_dp: data.id
			// }).then(response => {
			// 	console.log(response.data);
			// });
		},
		cetak(id) {
			function submit_hidden_form(url, params) {
				var f = $("<form method='POST' target='_blank' style='display:none;'></form>").attr({
					action: url
				}).appendTo(document.body);
				for (var i in params) {
					if (params.hasOwnProperty(i)) {
						$('<input type="hidden" />').attr({
							name: i,
							value: params[i]
						}).appendTo(f);
					}
				}
				f.submit();
				f.remove();
			}

			submit_hidden_form(this.doPage3, {
				dp_id: id,
			})
		},
		coce(cabang) {
			let n = this.location.includes(cabang);
			return n;
		},
		getDateRange() {
			var date = new Date();
			var week = date.setDate(date.getDate() + 7);

			this.dt_next_week = moment(week).format('YYYY-MM-DD');
			this.v_next_week = moment(week).format('DD MMM YYYY');

			axios.post(this.action, {
				case: 'getDatesFromRange'
			}).then(response => {
				this.date_range = response.data;
			});
		},
		loadDate(date) {
			EventBus.$emit('load_distribution_kirim_by_date', date);
		},
		loadup() {
			EventBus.$emit('load_distribution_kirim_by_date_up', this.dt_next_week);
		},
		accept(id, data) {
			axios.post(this.action, {
				case: 'updateSCStatus',
				id: id
			}).then(response => {
				EventBus.$emit('load_distribution_kirim_by_date', data);
			});
		},
		refuse(data) {
			EventBus.$emit('open_modal_proses_refuse', data);
		},
		addItem: function (i) {
			console.log(i);
			let item = i;
			let arr = this.arr_data;
			arr = arr.filter((n) => { return n != i });
			this.arr_data = arr;
			this.selected.push(item);
			EventBus.$emit('addItem', item);
		},
	},
	computed: {
		
	},
	mounted() {
		
	},
	created() {
		this.coce();
		this.getDateRange();

		EventBus.$on('removeSelected', (i) => {
			this.arr_data.unshift(i);
			this.selected.splice(i, 1);
			// console.log(this.selected);
		});

		EventBus.$on('dataDP', (data) => {
			this.arr_data = data
		});
	}

});

Vue.component('selected-displan', {

	template: `
	<div>
		<!-- <div class="row" style="margin-top: 4%;">
			<div class="col-md-1">
				<button type="button" class="btn btn-xm btn-success" @click="load"><i class="fa fa-refresh"></i> Kemarin</button>
			</div>
			<div class="col-sm-1" v-for="date in date_range">
				<button type="button" class="btn btn-xm btn-success" @click="loadDate(date.date)">{{date.date_format}}</button>
			</div>
			<div class="col-md-1">
				<button type="button" class="btn btn-xm btn-success" @click="loadup"><i class="glyphicon glyphicon-menu-right"></i> {{v_next_week}}</button>
			</div>
			<div class="col-sm-3" style="text-align: end;">
			</div>
		</div> -->
		</br>
		<div class="row">
			<!-- <div class="col-md-12">
				<v-client-table style="font-size:12px;" :data="data" :columns="col_kirim" :options="opt_kirim"></v-client-table>
			</div> -->
			<div class="col-md-12">
				<table class="table table-bordered" style="font-size: 12px;">
					<thead>
						<tr>
							<th class="active" style="width:5%;">No. Rencana Kirim</th>
							<th class="active" style="width:10%;">Tgl Kirim</th>
							<th class="active" style="width:5%;">Total Berat</th>
							<th class="active" style="width:5%;">Biaya Tambahan</th>
							<th class="active">Detail</th>
							<th class="active" style="width:16%;">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<template>
							<tr v-for="(dt, index) in fields.items" :key="index">
								<td>
									<div>{{ dt.no_dp }}</div>
									<div>
										<span v-if="dt.detail[0]['f24'] !== null">
											<label class='label label-primary lb-sm'>{{ dt.detail[0]['f24'] }} => {{ dt.detail[0]['f3'] }}</label>
										</span>
										<span v-else><label class='label label-primary lb-sm'>{{ dt.detail[0]['f3'] }}</label></span>
									</div>
								</td>
								<td v-if="dt.tgl_kirim != null">{{ dt.tgl_kirim }}</td>
								<td v-else>{{ dt.created_at }} </td>
								<td>{{ dt.total_muatan }} </td>
								<td>{{ dt.total_biaya }} </td>
								<td><new_detail :detail="dt.detail" :data_all="dt"></new_detail></td>
								<td v-if="dt.sc_status == 0">
									<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
										<div class="btn-group mr-2" role="group" aria-label="First group" v-if="coce(dt.detail[0]['f3']) == true || loc == 'HO'">
											<button type="button" class="btn btn-xs btn-danger" @click="delItem(index,dt)"><i class="fa fa-trash"></i></button>
										</div>
									</div>
								</td>
								<td style="width: 17%;" v-else>
									<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
										<div class="btn-group mr-2" role="group" aria-label="First group" v-if="coce(dt.detail[0]['f3']) == true || loc == 'HO'">
											<button type="button" class="btn btn-xs btn-danger" @click="delItem(index,dt)"><i class="fa fa-trash"></i></button>
										</div>
									</div>
								</td>
							</tr>
						</template>
					</tbody>
				</table>
			</div>
		</div>
		<div style="background: lightgray;padding: 20px; margin-bottom: 10%;" v-if="fields.items.length > 0">
			<div class="row">
				<div class="col-md-2" style="text-align: start;">
					<label class="control-label text-center">Lokasi Keberangkatan</label>
				</div>
				<div class="col-md-2" style="text-align: start;">
					<label class="control-label text-center">Tanggal Kirim</label>
				</div>
				<!-- <div class="col-md-2" style="text-align: start;">
					<label class="control-label text-center">Gudang</label>
				</div>
				<div class="col-md-2" style="text-align: start;">
					<label class="control-label text-center">NO. Truck</label>
				</div>
				<div class="col-md-2" style="text-align: start;">
					<label class="control-label text-center">Sopir</label>
				</div> -->
				<div class="col-md-2" style="text-align: start;">
					<label class="control-label text-center">Total Muat</label>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2">
					<v-select style="text-transform: capitalize" label="kd_off" :options="officeCode" v-model="v_office" :reduce="officeCode=>officeCode.kd_off"></v-select>
				</div>
				<div class="col-md-2">
					<vuejs-datepicker
						v-model="v_tgl_perintah_kirim"
						:format="DatePickerFormatb"
						:disabledDates="disabledDates"
						:bootstrap-styling="true"
						:placeholder="holderb"
						required
						style="width: 290px;">
					</vuejs-datepicker>
				</div>
				<div class="col-md-2" style="display: none;">
					<v-select style="text-transform: capitalize" label="wh_code" :options="dt_gudang" v-model="v_gudang" :reduce="dt_gudang=>dt_gudang.wh_code" required @input="cekNoTransfer"></v-select> 
				</div>
				<div class="col-md-2" style="display: none;">
					<v-select style="text-transform: capitalize" label="nopol" :options="dt_truck" v-model="v_nopol_truck" :reduce="dt_truck=>dt_truck.nopol" :taggable="true" @input="getSupirByNopol"></v-select>
				</div>
				<div class="col-md-2" style="display: none;">
					<v-select style="text-transform: capitalize" label="sopir" :options="dt_supir" v-model="v_sopir_name" @input="getSupirBySupir" :taggable="true"></v-select>
				</div>
				<div class="col-md-2">
					<input class="form-control" type="number" v-model="total_muatan" disabled />
				</div>
			</div>
			</br>
			</br>
			<div class="row">
				<div class="col-md-12">
					<button class="btn btn-danger" @click="delAllItem"> Batal / Hapus Semua</button >
					<button class="btn btn-primary" @click="saveData">Simpan</button>
				</div>
			</div>
		</div>
	</div>
	`,
	components: {
		vuejsDatepicker
	},
	props: {
		username: {
			required: true
		},
		loc: {
			required: true
		},
		where: {
			required: true
		},
		// data: {
		// 	type: Array,
		// 	required: true,
		// }
	},
	inject: ['location'],
	data() {
		return {
			v_tgl_perintah_kirim: moment(new Date().toISOString()).format('YYYY-MM-DD'),
			col_kirim: ['no_dp', 'asal', 'ritase', 'tgl_kirim', 'dtl_ledger', 'dtl_nama', 'dtl_alamat', 'dtl_keterangan', 'action'],
			opt_kirim: {
				perPage: 1000000,
				headings: {
					no_dp: "NO. RENCANA KIRIM",
					ritase: "NOMOR RITASE",
					tgl_kirim: "TGL KIRIM",
					asal: "CABANG ASAL",
					dtl_ledger: "LEDGER",
					dtl_nama: "NAMA",
					dtl_alamat: "ALAMAT",
					dtl_keterangan: "KET.",
					action: "ACTION"
				},
				sortable: ['no_dp', 'wh_code'],
				texts: {
					count: "",
					filter: "Cari",
					filterPlaceholder: "Cari No. DP",
				},
				templates: {
					action: "action-dis-plan-kirim-comp",
					asal: "temp-dis-plan-gudang-asal-new",
					ritase: function (h, row, index) {
						if (row.ritase == null) {
							return `Ritase`
						} else {
							return `Ritase ${row.ritase}`
						}
					}
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				childRow: "detail-displan-kirim-component",
				showChildRowToggler: true,
				footerHeadings: true,
				highlightMatches: true,
				columnsDropdown: false,
				uniqueKey: "id",
				childRowTogglerFirst: false,
			},
			loc_cab: this.loc,
			date_range: [],
			dt_next_week: '',
			v_next_week: '',
			printed_dp: [],
			selected: [],
			fields: {
				items: [],
			},
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 259200000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
			v_office: this.loc,
			officeCode: [],
			dt_truck: [],
			dt_supir: [],
			dt_gudang: [],
			v_nopol_truck: "",
			v_sopir_name: "",
			v_gudang: "",
			dt_notrans: [],
			v_noTrans: '',
			total_muat: 0
		}
	},
	methods: {
		getSupirBySupir() {
			this.v_sopir_name = this.v_sopir_name.sopir;
			console.log(this.v_sopir_name);
		},
		getSopirByNoto(no_to) {
			console.log(no_to);
			axios.post(this.action, {
				case: "getSopirByNoto",
				no_to: no_to
			}).then(response => {
				console.log(response.data);
				this.v_nopol_truck = response.data.car_no.trim();
				this.v_sopir_name = response.data.driver.trim();

				console.log(this.v_nopol_truck);
			})
		},
		getSupirByNopol() {
			axios.post(this.action, {
				case: "getSupirByNopol",
				no_pol: this.v_nopol_truck,
			}).then(response => {
				// console.log(response.data.data.length);
				if (response.data.data != "") {
					this.v_sopir_name = response.data.data.sopir;
				} else {
					this.v_sopir_name = "";
				}
			})
		},
		cekNoTransfer() {
			this.dt_notrans = [];
			axios.post(this.action, {
				case: 'getNoTransferByGudang',
				wh_code: this.v_gudang
			}).then(response => {
				let rspn = response.data;
				if (rspn != null) {
					this.dt_notrans = response.data;
				} else {
					this.dt_notrans = [];
				}
			});
		},
		getTruck() {
			axios.post(this.action, {
				case: "getTruck",
				where: this.where
			}).then(response => {
				this.dt_truck = response.data;
			})
		},
		getSopir() {
			axios.post(this.action, {
				case: "getSopir",
				where: this.where
			}).then(response => {
				this.dt_supir = response.data;
			})
		},
		getGudang() {
			axios.post(this.action, {
				case: "getGudang",
				username: this.username,
			}).then(response => {
				this.dt_gudang = response.data;
			})
		},
		getOfficeCode() {
			axios.post(this.action, {
				case: 'getOfficeCode',
				username: 'rachmadi'
			}).then(response => {
				this.officeCode = response.data.dt_kd_offifce[0];
			});
		},
		load() {
			EventBus.$emit('load_distribution_kirim');
		},
		proses(data) {
			EventBus.$emit('open_modal_proses_kirim', data);
		},
		edit(data) {
			EventBus.$emit('open_modal_edit_kirim', data);
		},
		transfer(data) {
			EventBus.$emit('open_modal_transfer_kirim', data);
		},
		batal(data) {
			const swalWithBootstrapButtons = Swal.mixin({
				customClass: {
					confirmButton: 'btn btn-success',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})

			swalWithBootstrapButtons.fire({
				title: 'Are you sure?',
				text: "You want to delete this data?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					axios.post(this.action, {
						case: 'deleteDistributionPlan',
						id_dp: data.id,
						item: data.item
					}).then(response => {
						console.log(response.data);
						location.reload();
					});
				} else if (
					result.dismiss === Swal.DismissReason.cancel
				) {
					swalWithBootstrapButtons.fire(
						'Cancelled',
						'Your data is safe :)',
						'error'
					)
				}
			})

			// axios.post(this.action, { 
			// 	case: 'deleteDistributionPlan',
			// 	id_dp: data.id
			// }).then(response => {
			// 	console.log(response.data);
			// });
		},
		cetak(id) {
			function submit_hidden_form(url, params) {
				var f = $("<form method='POST' target='_blank' style='display:none;'></form>").attr({
					action: url
				}).appendTo(document.body);
				for (var i in params) {
					if (params.hasOwnProperty(i)) {
						$('<input type="hidden" />').attr({
							name: i,
							value: params[i]
						}).appendTo(f);
					}
				}
				f.submit();
				f.remove();
			}

			submit_hidden_form(this.doPage3, {
				dp_id: id,
			})
		},
		coce(cabang) {
			let n = this.location.includes(cabang);
			return n;
		},
		getDateRange() {
			var date = new Date();
			var week = date.setDate(date.getDate() + 7);

			this.dt_next_week = moment(week).format('YYYY-MM-DD');
			this.v_next_week = moment(week).format('DD MMM YYYY');

			axios.post(this.action, {
				case: 'getDatesFromRange'
			}).then(response => {
				this.date_range = response.data;
			});
		},
		loadDate(date) {
			EventBus.$emit('load_distribution_kirim_by_date', date);
		},
		loadup() {
			EventBus.$emit('load_distribution_kirim_by_date_up', this.dt_next_week);
		},
		accept(id, data) {
			axios.post(this.action, {
				case: 'updateSCStatus',
				id: id
			}).then(response => {
				EventBus.$emit('load_distribution_kirim_by_date', data);
			});
		},
		refuse(data) {
			EventBus.$emit('open_modal_proses_refuse', data);
		},
		delItem: function (index, item) {
			var idx = this.fields.items.indexOf(item);
			if (idx > -1) {
				this.fields.items.splice(idx, 1);
			}
			EventBus.$emit('removeSelected', item);
		},
		delAllItem: function () {
			// this.$emit('delinv', this.fields.items.length);

			for (let i = 0; i < this.fields.items.length; i++) {
				let item = { ...this.fields.items[i] };
				EventBus.$emit('removeSelected', item);
			}

			//this.$refs.emp.clearSelection();
			this.fields.items = [];
		},
		saveData: function () {
			// console.log(this.fields);

			axios.post(this.action, {
				case: 'saveMergerDisplan',
				tgl_perintah: this.v_tgl_perintah_kirim,
				gudang: this.v_gudang,
				username: this.username,
				office: this.v_office,
				loc: this.loc,
				dt_dp: this.fields,
				no_pol: this.v_nopol_truck,
				sopir: this.v_sopir_name,
			}).then(response => {
				console.log(response.data);

				// function submit_hidden_form(url, params) {
				// 	var f = $("<form method='POST' target='_blank' style='display:none;'></form>").attr({
				// 		action: url
				// 	}).appendTo(document.body);
				// 	for (var i in params) {
				// 		if (params.hasOwnProperty(i)) {
				// 			$('<input type="hidden" />').attr({
				// 				name: i,
				// 				value: params[i]
				// 			}).appendTo(f);
				// 		}
				// 	}
				// 	f.submit();
				// 	f.remove();
				// }
	
				// submit_hidden_form(this.doPage3, {
				// 	dp_id: response.data.id,
				// })

				this.v_nopol_truck = "";
				this.v_sopir_name = "";
				this.v_gudang = "";

				this.delAllItem();

				this.loadDate(this.v_tgl_perintah_kirim);
			});
		},
	},
	computed: {
		total_muatan() {
			let total = 0;
			this.fields.items.forEach(element => {
				total = total + Number(element.total_muatan);
			});
			this.total_muat = total;
			return total;
		}
	},
	created() {
		this.coce();
		this.getDateRange();
		this.getTruck();
		this.getSopir();
		this.getGudang();
		this.getOfficeCode();

		EventBus.$on('addItem', (item) => {
			this.fields.items.push(item);
			console.log(this.fields);
		});
	}

});

Vue.component('dis-plan-transfer-component', {

	template: `
	<div>
		<div class="row">
			<div class="col-md-12" style="text-align: end;">
				<button type="button" class="btn btn-xm btn-success" @click="load"><i class="fa fa-refresh"></i></button>
			</div>
		</div>
		</br>
		<div class="row">
			<!-- <div class="col-md-12">
				<v-client-table style="font-size:12px;" :data="data" :columns="col_kirim" :options="opt_kirim"></v-client-table>
			</div> -->
			<div class="col-md-12">
				<table class="table table-bordered" style="font-size: 12px;">
					<thead>
						<tr>
							<th class="active" style="width:3%;">No. Rencana Kirim</th>
							<th class="active" style="width:3%;">Tgl Pengalihan</th>
							<th class="active" style="width:3%;">Transfer Ke</th>
							<th class="active" style="width:3%;">No. TO</th>
							<th class="active">Detail</th>
							<th class="active"></th>
						</tr>
					</thead>
					<tbody>
						<template v-for="(dt, i) in data">
							<tr>
								<td>
									<div>{{ dt.no_dp }}</div>
									<div>
										<span v-if="dt.detail[0]['f24'] !== null">
											<label class='label label-primary lb-sm'>{{ dt.detail[0]['f24'] }} => {{ dt.detail[0]['f3'] }}</label>
										</span>
										<span v-else><label class='label label-primary lb-sm'>{{ dt.detail[0]['f3'] }}</label></span>
									</div>
								</td>
								<td v-if="dt.tgl_kirim != null">{{ dt.tgl_kirim }}</td>
								<td v-else>{{ dt.created_at }}</td>
								<td>{{ dt.wh_code }}</td>
								<td>{{ dt.no_to }}</td>
								<td><new_detail :detail="dt.detail" :data_all="dt"></new_detail></td>
								<td style="width: 17%;">
									<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
										<div class="btn-group mr-2" role="group" aria-label="First group" v-if="coce(dt.detail[0]['f3']) == true || loc == 'HO'">
											<button name="btn-load" class="btn btn-info btn-flat btn-xs" style="width:100%;" @click="proses(dt)">BUAT DO & TERIMA</button>
										</div>
										<div class="btn-group mr-2" role="group" aria-label="Second group">
											<button name="btn-load" class="btn btn-danger btn-flat btn-xs" style="width:100%;" @click="batal(dt)">BATAL</button>
										</div>
									</div>
								</td>
							</tr>
						</template>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	`,
	props: {
		username: {
			required: true
		},
		loc: {
			required: true
		},
		data: {
			type: Array,
			required: true,
		}
	},
	inject: ['location'],
	data() {
		return {
			col_kirim: ['no_dp', 'asal', 'ritase', 'tgl_kirim', 'dtl_ledger', 'dtl_nama', 'dtl_alamat', 'dtl_keterangan', 'action'],
			opt_kirim: {
				perPage: 1000000,
				headings: {
					no_dp: "NO. RENCANA KIRIM",
					ritase: "NOMOR RITASE",
					tgl_kirim: "TGL KIRIM",
					asal: "CABANG ASAL",
					dtl_ledger: "LEDGER",
					dtl_nama: "NAMA",
					dtl_alamat: "ALAMAT",
					dtl_keterangan: "KET.",
					action: "ACTION"
				},
				sortable: ['no_dp', 'wh_code'],
				texts: {
					count: "",
					filter: "Cari",
					filterPlaceholder: "Cari No. DP",
				},
				templates: {
					action: "action-dis-plan-kirim-comp",
					asal: "temp-dis-plan-gudang-asal-new",
					ritase: function (h, row, index) {
						if (row.ritase == null) {
							return `Ritase`
						} else {
							return `Ritase ${row.ritase}`
						}
					}
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				childRow: "detail-displan-kirim-component",
				showChildRowToggler: true,
				footerHeadings: true,
				highlightMatches: true,
				columnsDropdown: false,
				uniqueKey: "id",
				childRowTogglerFirst: false,
			},
			loc_cab: this.loc,
			date_range: []
		}
	},
	methods: {
		load() {
			EventBus.$emit('load_distribution_transfer');
		},
		proses(data) {
			EventBus.$emit('open_modal_proses_transfer', data);
		},
		batal(data) {
			const swalWithBootstrapButtons = Swal.mixin({
				customClass: {
					confirmButton: 'btn btn-success',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})

			swalWithBootstrapButtons.fire({
				title: 'Are you sure?',
				text: "You want to delete this data?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					axios.post(this.action, {
						case: 'deleteDistributionPlan',
						id_dp: data.id,
						item: data.item
					}).then(response => {
						console.log(response.data);
						location.reload();
					});
				} else if (
					result.dismiss === Swal.DismissReason.cancel
				) {
					swalWithBootstrapButtons.fire(
						'Cancelled',
						'Your data is safe :)',
						'error'
					)
				}
			})

			// axios.post(this.action, { 
			// 	case: 'deleteDistributionPlan',
			// 	id_dp: data.id
			// }).then(response => {
			// 	console.log(response.data);
			// });
		},
		cetak(id) {
			function submit_hidden_form(url, params) {
				var f = $("<form method='POST' target='_blank' style='display:none;'></form>").attr({
					action: url
				}).appendTo(document.body);
				for (var i in params) {
					if (params.hasOwnProperty(i)) {
						$('<input type="hidden" />').attr({
							name: i,
							value: params[i]
						}).appendTo(f);
					}
				}
				f.submit();
				f.remove();
			}

			submit_hidden_form(this.doPage3, {
				dp_id: id,
			})
		},
		coce(cabang) {
			let n = this.location.includes(cabang);
			return n;
		},
		getDateRange() {
			axios.post(this.action, {
				case: 'getDatesFromRange'
			}).then(response => {
				this.date_range = response.data;
			});
		},
		loadDate(date) {
			EventBus.$emit('load_distribution_kirim_by_date', date);
		},
	},
	computed: {

	},
	created() {
		this.coce();
		this.getDateRange();
	}

});

Vue.component('detail-displan-kirim-component2', {

	props: ["data", "index", "columns"],
	template: `
	<div>
		<div class="row">
			<div class="col-sm-12" style="text-transform: capitalize;">
				<v-client-table :data="data.item" :columns="col_kirim" :options="opt_kirim"></v-client-table>
			</div>
		</div>
	</div>
	`,
	data() {
		return {
			col_kirim: ['sm_no', 'so_no', 'code', 'name', 'code_comp', 'item_code', 'tanggal', 'item_qty2', 'keterangan', 'sales_from_no'],
			opt_kirim: {
				headings: {
					name: "NAMA",
					code: "LEDGER",
					code_comp: 'CODE COMP',
					item_code: 'ITEM CODE',
					tanggal: 'TANGGAL SO',
					item_qty2: 'QTY',
					price_amount: "HARGA",
					keterangan: "KET.",
					sm_no: "NO. SM",
					so_no: "NO. SO",
					sales_from_no: "SALES CODE"
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				templates: {

				},
				filterable: false
			}
		}
	},
	methods: {

	},
	created() {

	}

});

Vue.component('detail-displan-kirim-component', {

	props: ["data", "index", "columns"],
	template: `
	<div>
		<div class="row">
			<div class="col-sm-12" style="text-transform: capitalize;">
				<v-client-table :data="data.detail" :columns="col_kirim" :options="opt_kirim"></v-client-table>
			</div>
		</div>
	</div>
	`,
	data() {
		return {
			col_kirim: ['f25', 'f5', 'f6', 'f8', 'f10', 'f11'],
			opt_kirim: {
				headings: {
					f25: "NAMA TOKO",
					f5: "NO SM",
					f6: "NO SO",
					f8: "ITEM",
					f10: "QTY",
					f11: "KETERANGAN"
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				templates: {

				},
				filterable: false
			}
		}
	},
	methods: {

	},
	created() {

	}

});

Vue.component('new_detail_ambil', {
	props: ['detail'],

	template: `
		<div>
			<table class="table">
				<thead>
					<tr>
						<th style="font-weight: bold; width:15%; text-align: left;" class="active">TUJUAN AMBIL</th>	
						<th style="font-weight: bold; width:15%; text-align: left;" class="active">TOKO</th>
						<th style="font-weight: bold; width:15%; text-align: left;" class="active">NO. SM</th>
						<th style="font-weight: bold; width:15%; text-align: left;" class="active">NO. SO</th>
						<th style="font-weight: bold; width:15%; text-align: left;" class="active">ITEM &amp; QTY</th>
						<th style="font-weight: bold; width:40%; text-align: left;" class="active">KETERANGAN</th>
					</tr>
				</thead>
				<tbody>
					<template v-for="dt in detail">
						<tr>
							<td>{{ dt['f3'] }}</td>
							<td>{{ dt['f16'] }}</td>
							<td>{{ dt['f5'] }}</td>
							<td>{{ dt['f6'] }}</td>
							<td>{{ dt['f8'] }} - {{ dt['f10'] }}</td>
							<td>{{ dt['f11'] }}</td>
						</tr>
					</template>
				</tbody>
			</table>
		</div>
	`,
});

Vue.component('dis-plan-ambil-component', {

	template: `
	<div>
		<div class="row" style="margin-top: 4%;">
			<div class="col-md-12" style="text-align: end;">
				<button type="button" class="btn btn-xm btn-success" @click="load"><i class="fa fa-refresh"></i></button>
			</div>	
		</div>
		</br>
		<div class="row">
			<!-- <div class="col-md-12">
				<v-client-table style="font-size: 12px;" :data="data" :columns="col_ambil" :options="opt_ambil"></v-client-table>
			</div> -->
			<div class="col-md-12">
				<table class="table table-bordered" style="font-size: 12px;">
					<thead>
						<tr>
							<th class="active" style="width:10%;">No. Rencana Kirim</th>
							<th class="active">Tgl Ambil</th>
							<th class="active">Detail</th>
							<th class="active" style="width:10%;">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<template v-for="(dt, i) in data">
							<tr>
								<td>{{ dt.no_dp }}</td>
								<td v-if="dt.tgl_kirim != null">{{ dt.tgl_kirim }}</td>
								<td v-else>{{ dt.created_at }}</td>
								<td><new_detail_ambil :detail="dt.detail"></new_detail_ambil></td>
								<td>
									<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
										<div class="btn-group mr-2" role="group" aria-label="First group" v-if="coce(dt.detail[0]['f3']) == true || loc == 'HO'">
											<button name="btn-load" class="btn btn-info btn-flat btn-xs" style="width:100%;" @click="proses(dt)">BUAT DO</button>
										</div>
										<div class="btn-group mr-2" role="group" aria-label="Second group">
											<button name="btn-load" class="btn btn-danger btn-flat btn-xs" style="width:100%;" @click="batal(dt)">BATAL</button>
										</div>
									</div>
								</td>
							</tr>
						</template>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	`,
	props: {
		loc: {
			required: true
		},
		username: {
			required: true
		},
		data: {
			type: Array,
			required: true,
		}
	},
	inject: ['location'],
	data() {
		return {
			col_ambil: ['no_dp', 'tgl_kirim', 'wh_code', 'dtl_ledger', 'dtl_nama', 'dtl_alamat', 'dtl_keterangan', 'action'],
			opt_ambil: {
				headings: {
					no_dp: "NO. DP",
					tgl_kirim: "TGL. KIRIM",
					wh_code: "GUDANG",
					dtl_ledger: "LEDGER",
					dtl_nama: "NAMA",
					dtl_alamat: "ALAMAT",
					dtl_keterangan: "KET.",
					action: "ACTION"
				},
				sortable: ['no_dp'],
				texts: {
					count: "",
					filter: "Cari",
					filterPlaceholder: "Cari No. DP",
				},
				templates: {
					// asal_gudang: "temp-dis-plan-gudang-asal",
					// wh_code: "temp-dis-plan-so-asal",
					action: "action-dis-plan-ambil-comp"
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				childRow: "detail-displan-ambil-component",
				showChildRowToggler: true,
				footerHeadings: true,
				highlightMatches: true,
				columnsDropdown: false,
				uniqueKey: "id",
				childRowTogglerFirst: false,
			}
		}
	},
	methods: {
		load() {
			EventBus.$emit('load_distribution_ambil');
		},
		proses(dt) {
			EventBus.$emit('open_modal_proses_ambil', dt);
			// function submit_hidden_form(url, params) {
			// 	var f = $("<form method='POST' target='_blank' style='display:none;'></form>").attr({
			// 		action: url
			// 	}).appendTo(document.body);
			// 	for (var i in params) {
			// 		if (params.hasOwnProperty(i)) {
			// 			$('<input type="hidden" />').attr({
			// 				name: i,
			// 				value: params[i]
			// 			}).appendTo(f);
			// 		}
			// 	}
			// 	f.submit();
			// 	f.remove();
			// }

			// submit_hidden_form(this.doPage3, {
			// 	dp_id: dt.id,
			// })
		},
		edit(dt) {
			EventBus.$emit('open_modal_edit_ambil', dt);
		},
		batal(dt) {
			const swalWithBootstrapButtons = Swal.mixin({
				customClass: {
					confirmButton: 'btn btn-success',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})

			swalWithBootstrapButtons.fire({
				title: 'Are you sure?',
				text: "You want to delete this data?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					axios.post(this.action, {
						case: 'deleteDistributionPlan',
						id_dp: dt.id
					}).then(response => {
						console.log(response.data);
						location.reload();
					});
				} else if (
					result.dismiss === Swal.DismissReason.cancel
				) {
					swalWithBootstrapButtons.fire(
						'Cancelled',
						'Your data is safe :)',
						'error'
					)
				}
			})

			// axios.post(this.action, { 
			// 	case: 'deleteDistributionPlan',
			// 	id_dp: data.id
			// }).then(response => {
			// 	console.log(response.data);
			// });
		},
		coce(cabang) {
			let n = this.location.includes(cabang);
			return n;
		}
	}

});

Vue.component('detail-displan-ambil-component', {

	props: ["data", "index", "columns"],
	template: `
	<div>
		<div class="row">
			<div class="col-sm-12" style="text-transform: capitalize;">
				<v-client-table :data="data.detail" :columns="col_kirim" :options="opt_kirim"></v-client-table>
			</div>
		</div>
	</div>
	`,
	data() {
		return {
			col_kirim: ['f5', 'f6', 'f7', 'f16', 'f12', 'f8', 'f4', 'f10', 'f11'],
			opt_kirim: {
				headings: {
					f5: "No. SM",
					f6: "No. SO",
					f7: 'LEDGER',
					f16: 'NAMA',
					f12: 'CODE COMP',
					f8: 'ITEM CODE',
					f4: "TANGGAL",
					f10: "QTY",
					f11: "KETERANGAN",
					so_no: "NO. SO"
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				templates: {

				},
				filterable: false
			}
		}
	},
	methods: {

	},
	created() {

	}

});

Vue.component('dis-plan-transfer-component2', {
	template: `
	<div>
		<div class="row">
			<div class="col-md-12" style="text-align: end;">
				<button type="button" class="btn btn-xm btn-success" @click="load"><i class="fa fa-refresh"></i></button>
			</div>	
		</div>
		</br>
		<div class="row">
			<div class="col-md-12">
				<v-client-table :data="data" :columns="col_transfer" :options="opt_transfer"></v-client-table>
			</div>	
		</div>
	</div>
	`,
	props: {
		username: {
			required: true
		},
		data: {
			type: Array,
			required: true,
		}
	},
	data() {
		return {
			col_transfer: ['no_dp', 'name', 'asal_gudang', 'wh_code', 'tgl_kirim2', 'action'],
			opt_transfer: {
				headings: {
					no_dp: "NO. DP",
					name: "Nama",
					asal_gudang: "GUDANG ASAL",
					wh_code: "SO. ASAL",
					tgl_kirim2: "TGL. AMBIL",
					action: "ACTION"
				},
				sortable: ['no_dp'],
				texts: {
					count: "",
					filter: "Cari",
					filterPlaceholder: "Cari No. DP",
				},
				templates: {
					asal_gudang: "temp-dis-plan-gudang-asal",
					wh_code: "temp-dis-plan-so-asal",
					action: "action-dis-plan-transfer-comp"
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				filterable: false,
				childRow: "detail-displan-transfer-component",
				showChildRowToggler: true,
				footerHeadings: true,
				highlightMatches: true,
				columnsDropdown: false,
				uniqueKey: "id",
				childRowTogglerFirst: false,
			}
		}
	},
	methods: {
		load() {
			EventBus.$emit('load_distribution_transfer');
		}
	}
});

Vue.component('detail-displan-transfer-component', {

	props: ["data", "index", "columns"],
	template: `
	<div>
		<div class="row">
			<div class="col-sm-12" style="text-transform: capitalize;">
				<v-client-table :data="data.item" :columns="col_kirim" :options="opt_kirim"></v-client-table>
			</div>
		</div>
	</div>
	`,
	data() {
		return {
			col_kirim: ['sm_no', 'so_no', 'code', 'name', 'code_comp', 'item_code', 'tanggal', 'SO', 'keterangan', 'sales_from_no'],
			opt_kirim: {
				headings: {
					name: "NAMA",
					code: "LEDGER",
					code_comp: 'CODE COMP',
					item_code: 'ITEM CODE',
					tanggal: 'TANGGAL SO',
					price_amount: "HARGA",
					keterangan: "KET.",
					sm_no: "NO. SM",
					so_no: "NO. SO",
					sales_from_no: "SALES CODE"
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				templates: {

				},
				filterable: false
			}
		}
	},
	methods: {

	},
	created() {

	}

});

Vue.component('daf_outstanding_component', {
	template:
		`
		<div>
			<div class="row">
				<div class="col-md-1">
					<label class="control-label text-center">Produk</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Cabang</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Gudang</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Ledger</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Periode</label>
				</div>
				<div class="col-md-1">
					<label class="control-label text-center">Report</label>
				</div>
			</div>
			<div class="row">
				<form @submit.prevent="getOutstanding">
					<div class="col-md-1">
						<v-select style="text-transform: capitalize" label="category" :options="dt_produk" v-model="v_produk" :reduce="dt_produk=>dt_produk.category" @input="changeKategory"></v-select>
					</div>
					<div class="col-md-2">
						<v-select style="text-transform: capitalize" label="coce_code" :options="dt_cabang" v-model="v_cabang" :reduce="dt_cabang=>dt_cabang.coce_code"></v-select>
					</div>
					<div class="col-md-2">
						<v-select style="text-transform: capitalize" label="wh_code" :options="dt_gudang" v-model="v_gudang" :reduce="dt_gudang=>dt_gudang.wh_code"></v-select>
					</div>
					<div class="col-md-2">
						<input type="text" class="form-control" v-model="v_ledger">
					</div>
					<div class="col-md-2">
						<vuejs-datepicker
							v-model="v_periode"
							:format="DatePickerFormatb"
							:bootstrap-styling="true"
							:placeholder="holderb"
							required
							style="width: 290px;">
						</vuejs-datepicker>
					</div>
					<div class="col-md-2">
						<v-select style="text-transform: capitalize" label="report" :options="dt_repot" v-model="v_report" :reduce="dt_repot=>dt_repot.report"></v-select>
					</div>
					<div class="col-md-1">
						<button type="submit" class="btn btn-xm btn-primary">Load</button>
					</div>
				</form>
			</div>
			</br></br>
			<div class="row">
				<div class="col-md-12">
					<div class="lds-facebook" v-if="loading"><div></div><div></div><div></div></div>
					<table class="table table-hover table-responsive table-striped">
						<thead>
							<tr>
								<th class="active">#</th>
								<th class="active" v-if="ar_cabang === 'ALL'">Cabang</th>
								<th class="active">Ledger</th>
								<th class="active">Customer</th>
								<th class="active">Tgl SO</th>
								<th class="active">Nomor SO</th>
								<th class="active">WH</th>
								<th class="active">Item</th>
								<th class="active">QTY SO</th>
								<th class="active" v-if="ar_report === 'detail'">TGL SO</th>
								<th class="active" v-if="ar_report === 'detail'">NOMOR DO</th>
								<th class="active" v-if="ar_report === 'detail'">NO PLAT</th>
								<th class="active" v-if="ar_report === 'detail'">QTY DO</th>
								<th class="active" v-else>QTY DO</th>
								<th class="active">Outs</th>
								<th class="active" v-if="ar_report === 'detail'"></th>
								<th class="active" v-else>Description</th>
								<th class="active"></th>
							</tr>
						</thead>
						<tbody>
							<tr v-for="(dt,i) in dt_outstanding">
								<td>{{ dt.no }}</td>
								<td v-if="ar_cabang === 'ALL'">{{ dt.txt_coce_code }}</td>
								<td>{{ dt.vend_code }}</td>
								<td>{{ dt.name }}</td>
								<td>{{ dt.pr_date }}</td>
								<td>{{ dt.txt_pr_no }}</td>
								<td>{{ dt.kp_code }}</td>
								<td>{{ dt.txt_item_code }}</td>
								<td>{{ dt.item_qty }}</td>
								<td v-if="ar_report === 'detail'">{{ dt.do_date }}</td>
								<td v-if="ar_report === 'detail'">{{ dt.ref_no }}</td>
								<td v-if="ar_report === 'detail'">{{ dt.car_no }}</td>
								<td v-if="ar_report === 'detail'">{{ dt.do_qty }}</td>
								<td v-else>{{ dt.do_sum }}</td>
								<td style="color: red;">{{ dt.outs_do }}</td>
								<td v-if="ar_report === 'detail'"></td>
								<td v-else>{{ dt.pr_desc }}</td>
								<td v-if="dt.txt_pr_no === xso_no"></td>
								<td v-else><button name="btn-load" class="btn btn-info btn-flat btn-xs" style="width:100%;" @click="do_outstanding(dt)">BUAT DO</button></td>
							</tr>
						</tbody>
						<tfoot>
							<tr v-for="(dta,i) in total_outstanding" style="font-weight:bold; background-color:#F8F8F8;">
								<th></th>
								<th v-if="ar_cabang === 'ALL'"></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<td>{{dta.item_code}}</td>
								<td>{{dta.item_qty}}</td>
								<th v-if="ar_report === 'detail'"></th>
								<th v-if="ar_report === 'detail'"></th>
								<th v-if="ar_report === 'detail'"></th>
								<td v-if="ar_report === 'detail'">{{dta.do_sum}}</td>
								<td v-else>{{dta.do_sum}}</td>
								<td>{{dta.outs_do}}</td>
								<th></th>
							</tr>
							<tr style="font-weight:bold; background-color:#F8F8F8;">
								<th></th>
								<th v-if="ar_cabang === 'ALL'"></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<td style="text-align:right;">{{ grand_total_outstanding.sum_so_qty }}</td>
								<th v-if="ar_report === 'detail'"></th>
								<th v-if="ar_report === 'detail'"></th>
								<th v-if="ar_report === 'detail'"></th>
								<td style="text-align:right;" v-if="ar_report === 'detail'">{{ grand_total_outstanding.sum_do_qty }}</td>
								<td style="text-align:right;" v-else>{{ grand_total_outstanding.sum_do_sum }}</td>
								<td style="text-align:right; color:red;">{{ grand_total_outstanding.sum_outs_do }}</td>
								<th></th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
	`,
	components: {
		vuejsDatepicker
	},
	props: ["username"],
	data() {
		return {
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 8640000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
			loading: false,
			v_produk: "SAA",
			v_cabang: "ALL",
			v_gudang: "ALL",
			v_report: "Summary",
			v_periode: moment(this.v_periode).format("YYYY-MM-DD"),
			v_ledger: "",
			dt_produk: [],
			dt_cabang: [],
			dt_gudang: [],
			dt_gudang_all: [
				{
					"wh_code": "ALL"
				}
			],
			dt_repot: [
				{
					'report': 'summary'
				},
				{
					'report': 'detail'
				}
			],
			dt_outstanding: [],
			ar_report: "summary",
			ar_cabang: "ALL",
			total_outstanding: [],
			grand_total_outstanding: [],
			xso_no: ""
		}
	},
	methods: {
		getKategori() {
			axios.post(this.action, {
				case: 'getKategori',
				username: this.username
			}).then(response => {
				this.dt_produk = response.data.data;
				this.getCabang(this.dt_produk[0].category, this.username);
			});
		},
		getCabang(category) {
			axios.post(this.action, {
				case: 'getCabang',
				category: category,
				username: this.username,
			}).then(response => {
				this.dt_cabang = response.data.data;
				this.getGudang(this.dt_cabang[0].coce_code);
			});
		},
		getGudang(coce_code) {
			axios.post(this.action, {
				case: 'getGudangOutstanding',
				coce_code: coce_code,
				username: this.username,
			}).then(response => {
				this.dt_gudang = response.data[0];
				this.dt_gudang.push(this.dt_gudang_all[0]);
			});
		},
		changeKategory() {
			this.getCabang(this.v_produk);
		},
		getOutstanding() {
			this.loading = true;
			axios.post(this.action, {
				case: 'getOutstanding',
				category: this.v_produk,
				cabang: this.v_cabang,
				gudang: this.v_gudang,
				ledger: this.v_ledger,
				periode: this.v_periode,
				report: this.v_report
			}).then(response => {
				this.loading = false;
				this.dt_outstanding = response.data.outstanding;
				this.total_outstanding = response.data.total_outstanding;
				this.grand_total_outstanding = response.data.grand_total_outstanding[0];

				this.ar_report = this.dt_outstanding[0].report;
				this.ar_cabang = this.dt_outstanding[0].kode_cab;
			});
		},
		do_outstanding(data) {
			console.log(data);
			function submit_hidden_form(url, params) {
				var f = $("<form method='POST' target='_blank' style='display:none;'></form>").attr({
					action: url
				}).appendTo(document.body);
				for (var i in params) {
					if (params.hasOwnProperty(i)) {
						$('<input type="hidden" />').attr({
							name: i,
							value: params[i]
						}).appendTo(f);
					}
				}
				f.submit();
				f.remove();
			}

			submit_hidden_form(
				this.doPage2, {
				shift: this.v_produk,
				whCode: data.kp_code + '-' + data.kode_cab,
			}
			)
		},
		getNow() {
			const today = new Date();
			const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
			const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			const dateTime = date + ' ' + time;
			this.v_periode = dateTime;
		}
	},
	created() {
		this.getKategori();
	}
});

Vue.component('daf_force_close_comp', {

	template:
		`
	<div>
		<div class="row">
			<div class="col-md-12">
				<tabs_distribution_plan_component>
					<tab_distribution_plan_component v-for="(dt,i) in tabs" :key="i" :name="dt.name" :icon="dt.icon" :selected="i === 0 ? 'true' : ''">
						<div v-if="i == 0">
							<daf_force_component :username="username" :usergroup="usergroup"></daf_force_component>
						</div>
						<div v-if="i == 1">
							<daf_force_approve_component :username="username" :usergroup="usergroup"></daf_force_approve_component>
						</div>
					</tab_distribution_plan_component>
				</tabs_distribution_plan_component>
			</div>
		</div>
	</div>
	`,
	props: ["username", "usergroup"],
	data() {
		return {
			loading: false,
			tabs: [
				{
					name: 'REQUEST FORCE CLOSE',
					icon: '<i class="fa fa-print"></i>',
					counter: 0,
				},
				{
					name: 'APPROVE',
					icon: '<i class="fa fa-print"></i>',
					counter: 1,
				},
			],
		}
	},
	methods: {

	},
	created() {

	},
	watch: {

	}

});

Vue.component('daf_force_component2', {
	template:
		`
		<div>
			<div class="row">
				<div class="col-md-2">
					<label class="control-label text-center">Produk</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Dari Tanggal</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Sampai Tanggal</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Search</label>
				</div>
			</div>
			<div class="row">
				<form @submit.prevent="getForceClose">
					<div class="col-md-2">
						<v-select style="text-transform: capitalize" label="category" :options="dt_produk" v-model="v_produk" :reduce="dt_produk=>dt_produk.category"></v-select>
					</div>
					<div class="col-md-2">
						<vuejs-datepicker
							v-model="v_date_start"
							:format="DatePickerFormatb"
							:bootstrap-styling="true"
							:placeholder="holderb"
							required
							style="width: 290px;">
						</vuejs-datepicker>
					</div>
					<div class="col-md-2">
						<vuejs-datepicker
							v-model="v_date_end"
							:format="DatePickerFormatb"
							:bootstrap-styling="true"
							:placeholder="holderb"
							required
							style="width: 290px;">
						</vuejs-datepicker>
					</div>
					<div class="col-md-2">
						<input type="text" class="form-control" v-model="v_search">
					</div>
					<div class="col-md-2">
						<button type="submit" class="btn btn-xm btn-primary">Load</button>
					</div>
				</form>
			</div>
			</br></br>
			<div class="row">
				<div class="col-md-12">
					<div class="lds-facebook" v-if="loading"><div></div><div></div><div></div></div>
					<v-client-table :data="dt_requestClose" :columns="col_requestclose" :options="opt_requestclose"></v-client-table>
				</div>
			</div>
		</div>
	`,
	components: {
		vuejsDatepicker
	},
	props: ["username"],
	data() {
		return {
			loading: false,
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 8640000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
			v_search: "",
			v_date_start: "",
			v_date_end: "",
			v_produk: "",
			dt_produk: [],
			dt_requestClose: [],
			col_requestclose: ['event_date', 'user_id', 'event_desc', 'reason', 'qty_so', 'qty_do', 'qty_outs', 'action'],
			opt_requestclose: {
				headings: {
					event_date: "DATETIME",
					user_id: "USER",
					event_desc: 'DESCRIPTION',
					reason: 'REASON',
					qty_so: "QTY SO",
					qty_do: "QTY DO",
					qty_outs: "QTY OUTS"
				},
				templates: {
					action: 'action-force-close-comp'
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				filterable: true,
				customFilters: ['event_date', 'user_id', 'event_desc', 'reason']
			}
		}
	},
	methods: {
		getKategori() {
			axios.post(this.action, {
				case: 'getKategori',
				username: this.username
			}).then(response => {
				this.dt_produk = response.data.data;
			});
		},
		getForceClose() {
			this.loading = true;
			axios.post(this.action, {
				case: 'getForceClose',
				shift: this.v_produk,
				status: 'REQUEST CLOSED',
				cari: this.v_search,
				date1: moment(this.v_date_start).format("YYYY-MM-DD"),
				date2: moment(this.v_date_end).format("YYYY-MM-DD"),
			}).then(response => {
				this.loading = false;
				this.dt_requestClose = response.data;
				console.log(this.dt_requestClose);
			});
		}
	},
	created() {
		this.getKategori();
	}
});

Vue.component('daf_force_component', {
	// <v-client-table :data="dt_requestClose" :columns="col_requestclose" :options="opt_requestclose"></v-client-table>

	// <td v-if="dt.sum_in > 10">
	// 	<button name="btn-load" class="btn btn-info btn-flat btn-xs" style="width:100%;" @click="email(dt)">KIRIM EMAIL</button>
	// </td>
	// <td v-else>
	// 	<button name="btn-load" class="btn btn-warning btn-flat btn-xs" style="width:100%;" @click="proses(dt)">FORCE CLOSE</button>
	// </td>
	template:
		`
		<div>
			<div class="row">
				<div class="col-md-1">
					<label class="control-label text-center">Produk</label>
				</div>
				<div class="col-md-1">
					<label class="control-label text-center">Cabang</label>
				</div>
				<div class="col-md-1">
					<label class="control-label text-center">Gudang</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Sales</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Ledger</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Periode</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Report</label>
				</div>
			</div>
			<div class="row">
				<form @submit.prevent="getForceClose">
					<div class="col-md-1">
						<v-select style="text-transform: capitalize" label="category" :options="dt_produk" v-model="v_produk" :reduce="dt_produk=>dt_produk.category" @input="changeKategory"></v-select>
					</div>
					<div class="col-md-1">
						<v-select style="text-transform: capitalize" label="coce_code" :options="dt_cabang" v-model="v_cabang" :reduce="dt_cabang=>dt_cabang.coce_code" @input="getSales"></v-select>
					</div>
					<div class="col-md-1">
						<v-select style="text-transform: capitalize" label="wh_code" :options="dt_gudang" v-model="v_gudang" :reduce="dt_gudang=>dt_gudang.wh_code"></v-select>
					</div>
					<div class="col-md-2">
						<v-select style="text-transform: capitalize" label="sales_code" :options="dt_sales" v-model="v_sales_code" :reduce="dt_sales=>dt_sales.sales_code"></v-select>
					</div>
					<div class="col-md-2">
						<input type="text" class="form-control" v-model="v_search">
					</div>
					<div class="col-md-2">
						<vuejs-datepicker
							v-model="v_date_start"
							:format="DatePickerFormatb"
							:bootstrap-styling="true"
							:placeholder="holderb"
							required
							style="width: 290px;">
						</vuejs-datepicker>
					</div>
					<div class="col-md-2">
						<v-select style="text-transform: capitalize" label="label_report" :options="dt_report" v-model="v_report" :reduce="dt_report=>dt_report.report"></v-select>
					</div>
					<div class="col-md-1">
						<button type="submit" class="btn btn-xm btn-primary">Load</button>
					</div>
					<div class="col-md-1" style="display: none;">
						<button type="submit" class="btn btn-xm btn-primary" @click="send_mail">Send</button>
					</div>
				</form>
			</div>
			</br></br>
			<div class="row">
				<div class="col-sm-10">

				</div>
				<div class="col-sm-2" style="margin-bottom: 20px;">
					<input type="text" class="form-control" placeholder="No. SO" v-model="v_search_so">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="lds-facebook" v-if="loading"><div></div><div></div><div></div></div>
					<div style="overflow-x: auto;">
						<table class="table table-hover table-responsive table-striped" style="font-size: 11px;">
							<thead>
								<tr>
									<th v-for="column in new_columns" @click="activeColumn = column" class="active">
										{{ colTitles[column.name] }} 
										<span @click="column.order = column.order * (-1), sortBy(column.name)" class="arrow" :class="column.order > 0 ? 'asc' : 'dsc'" ></span>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr v-for="(dt,i) in filterListST" :key="i"  v-if="i >= pagestart && i < pageend" style="text-transform: uppercase;">
									<td>{{ i+1 }}</td>
									<td>{{ dt.spk_no }}</td>
									<td>{{ dt.vend_code }}</td>
									<td>{{ dt.name }}</td>
									<td>{{ dt.pr_no }}</td>
									<td>{{ dt.item_code }}</td>
									<td>{{ dt.item_qty }}</td>
									<td>{{ dt.ref_no_out }}</td>
									<td>{{ dt.sum_out }}</td>
									<td>{{ dt.sum_in }}</td>
									<td>{{ dt.pr_desc }}</td>
									<td>
										<button name="btn-load" class="btn btn-warning btn-flat btn-xs" style="width:100%;" @click="proses(dt)">FORCE CLOSE</button>
									</td>
								</tr>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td style="font-weight: bold;">Grand Total</td>
									<td style="font-weight: bold;">{{ sum_qty_so }}</td>
									<td></td>
									<td style="font-weight: bold;">{{ sum_qty_do }}</td>
									<td style="font-weight: bold;">{{ sum_out }}</td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			</br></br></br>
			<div class="row">
				<div class="col-md-12" style="text-transform: capitalize; text-align: center;">
					<paginate 
						:pagecount="totalpages"
						:containerclass="'pagination pagination-sm'"
						:clickhandler="pagination"
						:prevtext="prevButtonIcon"
						:nexttext="nextButtonIcon">
					</paginate>
				</div>
			</div>
			<div class="modal fade" id="proses_force_close" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<div class="row">
								<div class="col-sm-11">
									<h3 class="modal-title">FORCE CLOSE SO #{{dt_force.pr_no}}</h3>
								</div>
								<div class="col-sm-1" style="text-align: end;">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							</div>
						</div>
						<div class="modal-body" style="background: white; padding-top: 1px;">
							<div class="form-group">
								<div class="row">
									<div class="col-sm-4">
										<label for="recipient-name" class="col-form-label">NAMA PELANGGAN</label>
									</div>
									<div class="col-sm-1">
										<p>:</p>
									</div>
									<div class="col-sm-6">
										<label for="recipient-name" class="col-form-label">{{dt_force.name}} [{{dt_force.vend_code}}]</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-4">
										<label for="recipient-name" class="col-form-label">PERMINTAAN DARI</label>
									</div>
									<div class="col-sm-1">
										<p>:</p>
									</div>
									<div class="col-sm-6">
										<label for="recipient-name" class="col-form-label">{{dt_reason.user_id}}</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="recipient-name" class="col-form-label">ALASAN KARENA</label>
								<textarea class="form-control" rows="3" style="resize: none;" readonly>{{dt_reason.reason}}</textarea>
							</div>
							<div class="form-group">
								<select class="form-control" v-model="v_force_aksi">
									<option selected value="1">Menyetujui</option>
									<option value="0">Menolak</option>
								</select>
							</div>
							<div class="form-group">
								<label for="recipient-name" class="col-form-label">ALASANNYA</label>
								<textarea class="form-control" rows="3" style="resize: none;" v-model="v_force_alasan_hoo"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger btn-sm" @click="closeModalForce()">Batal</button>
							<button type="button" class="btn btn-primary btn-sm" @click="actionForceClose">Simpan</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	`,
	components: {
		vuejsDatepicker
	},
	props: ["username", "usergroup"],
	data() {
		return {
			loading: false,
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 8640000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
			v_search: "",
			v_date_start: moment(this.v_date_start).format("YYYY-MM-DD"),
			v_date_end: "",
			v_produk: "saa",
			v_cabang: "ALL",
			v_gudang: "ALL",
			v_report: "Summary",
			v_periode: "",
			v_ledger: "",
			v_sales_code: "ALL",
			dt_produk: [],
			dt_requestClose: [],
			dt_cabang: [],
			dt_gudang: [],
			dt_sales: [],
			dt_report: [
				{
					"report": "summary",
					"label_report": "summary",
				},
				{
					"report": "detail",
					"label_report": "Detail",
				}
			],
			dt_gudang_all: [
				{
					"wh_code": "ALL"
				}
			],
			dt_sales_all: [
				{
					"sales_code": "ALL"
				}
			],
			col_requestclose: ['spk_no', 'vend_code', 'name',
				'pr_no', 'item_code', 'item_qty', 'ref_no_out',
				'sum_out', 'sum_in', 'pr_desc', 'action'],
			// gantung
			// opt_requestclose: {
			// 	headings: {
			// 		spk_no: "CABANG",
			// 		vend_code: "LEDGER",
			// 		name: 'NAME',
			// 		pr_no: 'NO. SO',
			// 		item_code: "ITEM CODE",
			// 		item_qty: "QTY SO",
			// 		ref_no_out: "REF NO OUT",
			// 		sum_out: "QTY DO",
			// 		sum_in: "OUTS",
			// 		pr_desc: "DESCRIPTION"
			// 	},
			// 	templates: {
			// 		action: 'action-force-close-comp'
			// 	},
			// 	sortIcon: {
			// 		base : 'fa',
			// 		is: 'fa-sort',
			// 		up: 'fa-sort-asc',
			// 		down: 'fa-sort-desc'
			// 	},
			// 	filterable: true,
			// 	childRow: "detail-force-close-component",
			// 	showChildRowToggler: true,
			//     footerHeadings: true,
			//     highlightMatches: true,
			//     columnsDropdown: false,
			//     uniqueKey: "pr_no",
			//     childRowTogglerFirst: false,
			// },
			/* pagination setting */
			pagestart: 0,
			pagecurrent: 1,
			itemsperpage: 50,
			pageend: 50,
			prevButtonIcon: '<i class="fa fa-chevron-left"></i>',
			nextButtonIcon: '<i class="fa fa-chevron-right"></i>',
			/* sort setting */
			currentSort: 'name',
			currentSortDir: 'asc',
			/* table column setting */
			activeColumn: {},
			new_columns: [
				{ name: 'number', order: 1 },
				{ name: 'spk_no', order: 1 },
				{ name: 'vend_code', order: 1 },
				{ name: 'name', order: 1 },
				{ name: 'pr_no', order: 1 },
				{ name: 'item_code', order: 1 },
				{ name: 'item_qty', order: 1 },
				{ name: 'ref_no_out', order: 1 },
				{ name: 'sum_out', order: 1 },
				{ name: 'sum_in', order: 1 },
				{ name: 'pr_desc', order: 1 },
				{ name: 'action', order: 1 }
			],
			colTitles: {
				'number': 'NO.',
				'spk_no': 'Cabang',
				'vend_code': 'Ledger',
				'name': 'Name',
				'pr_no': 'No. SO',
				'item_code': 'Item Code',
				'item_qty': 'Qty SO',
				'ref_no_out': 'Ref No Out',
				'sum_out': 'Qty DO',
				'sum_in': 'Outs',
				'pr_desc': 'Description',
				'action': '#'
			},
			v_search_so: "",
			dt_force: [],
			dt_reason: [],
			v_force_aksi: "1",
			v_force_alasan_hoo: "",
			sum_qty_so: 0,
			sum_qty_do: 0,
			sum_out: 0,
		}
	},
	methods: {
		openModalForce() {
			$("#proses_force_close").modal("show");
		},
		closeModalForce() {
			$("#proses_force_close").modal("hide");
		},
		proses(data) {
			EventBus.$emit('open_modal_proses_force_close', data);
		},
		email(data) {
			EventBus.$emit('open_email_force_close', data);
		},
		getKategori() {
			axios.post(this.action, {
				case: 'getKategori',
				username: this.username
			}).then(response => {
				this.dt_produk = response.data.data;
				this.getCabang(this.dt_produk[0].category, this.username);
			});
		},
		getCabang(category) {
			axios.post(this.action, {
				case: 'getCabang',
				category: category,
				username: this.username,
			}).then(response => {
				this.dt_cabang = response.data.data;
				this.getGudang(this.dt_cabang[0].coce_code);
			});
		},
		getGudang(coce_code) {
			axios.post(this.action, {
				case: 'getGudangOutstanding',
				coce_code: coce_code,
				username: this.username,
			}).then(response => {
				this.dt_gudang = response.data[0];
				this.dt_gudang.push(this.dt_gudang_all[0]);
			});
		},
		getSales() {
			axios.post(this.action, {
				case: 'getSales',
				coce_code: this.v_cabang,
				username: this.username,
				usergroup: this.usergroup
			}).then(response => {
				this.dt_sales = response.data.data;
				this.dt_sales.push(this.dt_sales_all[0]);
			});
		},
		changeKategory() {
			this.getCabang(this.v_produk);
		},
		getForceClose() {
			this.sum_qty_so = 0;
			this.sum_qty_do = 0;
			this.sum_out = 0;

			this.loading = true;
			this.dt_requestClose = [];

			axios.post(this.action, {
				case: 'getForceClose',
				shift: this.v_produk,
				coce_code: this.v_cabang,
				sales_code: this.v_sales_code,
				wh_code: this.v_gudang,
				ledger: this.v_search,
				report: this.v_report,
				date2: moment(this.v_date_start).format("YYYY-MM-DD"),
			}).then(response => {
				this.loading = false;
				if (response.data[0].data_fc == null) {
					this.dt_requestClose = [];
				} else {
					this.dt_requestClose = response.data[0].data_fc;
					this.sum_qty_so = response.data[0].sum_so_qty;
					this.sum_qty_do = response.data[0].sum_do_qty;
					this.sum_out = response.data[0].sum_outs;
				}
				console.log(response.data);
			});
		},
		getForceCloseAprove() {
			this.sum_qty_so = 0;
			this.sum_qty_do = 0;
			this.sum_out = 0;

			this.loading = true;
			this.dt_requestClose = [];

			axios.post(this.action, {
				case: 'getForceCloseAprove',
				shift: this.v_produk,
				coce_code: this.v_cabang,
				sales_code: this.v_sales_code,
				wh_code: this.v_gudang,
				ledger: this.v_search,
				report: this.v_report,
				date2: moment(this.v_date_start).format("YYYY-MM-DD"),
			}).then(response => {
				this.loading = false;
				// if (response.data[0].data_fc == null) {
				// 	this.dt_requestClose = [];
				// } else {
				// 	this.dt_requestClose = response.data[0].data_fc;
				// 	this.sum_qty_so = response.data[0].sum_so_qty;
				// 	this.sum_qty_do = response.data[0].sum_do_qty;
				// 	this.sum_out = response.data[0].sum_outs;
				// }
				console.log(response.data);
			});
		},
		pagination(index) {
			if (index >= 0 && index <= this.totalpages) {
				this.pagecurrent = index;
				this.pagestart = (this.itemsperpage * index) - this.itemsperpage;
				this.pageend = (this.itemsperpage * index);
			}
		},
		sortBy(column) {
			if (column === this.currentSort) {
				this.currentSortDir = this.currentSortDir === 'asc' ? 'desc' : 'asc';
			}
			this.currentSort = column;
		},
		getNow() {
			const today = new Date();
			const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
			const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			const dateTime = date + ' ' + time;
			this.v_periode = dateTime;
		},
		actionForceClose() {

			this.closeModalForce();
			this.loading = true;

			//gantung
			var data = {
				"action": 'approvedCloseSO',
				"aksi": 'F',
				"coce_code": this.v_cabang,
				"status": 'W',
				"shift": this.dt_force.code_comp.toLowerCase(),
				"date2": moment(this.dt_force.pr_date).format("YYYY-MM-DD"),
				"pr_no": this.dt_force.pr_no,
				"usergroup": 'EDP',
				"reason": '',
				"user_id": this.dt_reason.user_id,
				"reason_desc2": this.dt_reason.reason,
				"aksi_hoo": this.v_force_aksi,
				"reason_hoo": this.v_force_alasan_hoo,
				"reason_desc": ''
			};

			$.post("../../sales/actionForceClose_bak.php",
				data,
				function (data, status) {
					data = JSON.parse(data);
					console.log(data);
					console.log(status);
					this.loading = false;
					this.getForceClose();
				}
			);

		},
		send_mail() {
			// console.log(this.dt_force);
			axios.post(this.action, {
				case: 'sendEmail',
				action: 'approvedCloseSO',
				aksi: 'F',
				coce_code: this.v_cabang,
				status: 'W',
				shift: this.dt_force.code_comp.toLowerCase(),
				date2: moment(this.dt_force.pr_date).format("YYYY-MM-DD"),
				pr_no: this.dt_force.pr_no,
				usergroup: 'EDP',
				reason: '',
				user_id: this.dt_reason.user_id,
				reason_desc2: this.dt_reason.reason,
				aksi_hoo: this.v_force_aksi,
				reason_hoo: this.v_force_alasan_hoo,
				reason_desc: '',
				data_all: this.dt_force
			}).then(response => {
				console.log(response.data);
			});
		}
	},
	created() {
		this.getKategori();
		this.getNow();
		EventBus.$on("open_modal_proses_force_close", (data) => {
			this.dt_force = data;
			this.dt_reason = data.reason[0]
			this.openModalForce();
		});

		EventBus.$on("open_email_force_close", (data) => {
			this.dt_force = data;
			this.dt_reason = data.reason[0];
			this.send_mail();
		});
	},
	computed: {
		filterListST() {
			let filterST = this.v_search_so

			let result = this.dt_requestClose.filter(function (e) {
				let filtered = true
				if (filterST && filterST.length > 0) {
					filtered = e.so_no.includes(filterST)
				}
				return filtered;

			}).sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});

			return result;
		},
		totalpages() {
			//This calculates the amount of pages
			return Math.ceil(this.filterListST.length / this.itemsperpage);
		},
		sortedData: function () {
			return this.dt_requestClose.sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});
		}
	},
	watch: {
		data: function () {
			this.pagecurrent = 1;
			this.pagestart = 0;
			this.pageend = this.itemsperpage;
		},
	}
});

Vue.component('daf_force_approve_component', {
	// <v-client-table :data="dt_requestClose" :columns="col_requestclose" :options="opt_requestclose"></v-client-table>
	template:
		`
		<div>
			<div class="row">
				<div class="col-md-1">
					<label class="control-label text-center">Produk</label>
				</div>
				<div class="col-md-1">
					<label class="control-label text-center">Cabang</label>
				</div>
				<div class="col-md-1">
					<label class="control-label text-center">Gudang</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Sales</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Ledger</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Periode</label>
				</div>
				<div class="col-md-2">
					<label class="control-label text-center">Report</label>
				</div>
			</div>
			<div class="row">
				<form @submit.prevent="getForceCloseAprove">
					<div class="col-md-1">
						<v-select style="text-transform: capitalize" label="category" :options="dt_produk" v-model="v_produk" :reduce="dt_produk=>dt_produk.category" @input="changeKategory"></v-select>
					</div>
					<div class="col-md-1">
						<v-select style="text-transform: capitalize" label="coce_code" :options="dt_cabang" v-model="v_cabang" :reduce="dt_cabang=>dt_cabang.coce_code" @input="getSales"></v-select>
					</div>
					<div class="col-md-1">
						<v-select style="text-transform: capitalize" label="wh_code" :options="dt_gudang" v-model="v_gudang" :reduce="dt_gudang=>dt_gudang.wh_code"></v-select>
					</div>
					<div class="col-md-2">
						<v-select style="text-transform: capitalize" label="sales_code" :options="dt_sales" v-model="v_sales_code" :reduce="dt_sales=>dt_sales.sales_code"></v-select>
					</div>
					<div class="col-md-2">
						<input type="text" class="form-control" v-model="v_search">
					</div>
					<div class="col-md-2">
						<vuejs-datepicker
							v-model="v_date_start"
							:format="DatePickerFormatb"
							:bootstrap-styling="true"
							:placeholder="holderb"
							required
							style="width: 290px;">
						</vuejs-datepicker>
					</div>
					<div class="col-md-2">
						<v-select style="text-transform: capitalize" label="label_report" :options="dt_report" v-model="v_report" :reduce="dt_report=>dt_report.report"></v-select>
					</div>
					<div class="col-md-1">
						<button type="submit" class="btn btn-xm btn-primary">Load</button>
					</div>
					<div class="col-md-1" style="display: none;">
						<button type="submit" class="btn btn-xm btn-primary" @click="send_mail">Send</button>
					</div>
				</form>
			</div>
			</br></br>
			<div class="row">
				<div class="col-sm-10">

				</div>
				<div class="col-sm-2" style="margin-bottom: 20px;">
					<input type="text" class="form-control" placeholder="No. SO" v-model="v_search_so">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="lds-facebook" v-if="loading"><div></div><div></div><div></div></div>
					<div style="overflow-x: auto;">
						<table class="table table-hover table-responsive table-striped" style="font-size: 11px;">
							<thead>
								<tr>
									<th v-for="column in new_columns" @click="activeColumn = column" class="active">
										{{ colTitles[column.name] }} 
										<span @click="column.order = column.order * (-1), sortBy(column.name)" class="arrow" :class="column.order > 0 ? 'asc' : 'dsc'" ></span>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr v-for="(dt,i) in filterListST" :key="i"  v-if="i >= pagestart && i < pageend" style="text-transform: uppercase;">
									<td>{{ i+1 }}</td>
									<td>{{ dt.spk_no }}</td>
									<td>{{ dt.vend_code }}</td>
									<td>{{ dt.name }}</td>
									<td>{{ dt.pr_no }}</td>
									<td>{{ dt.item_code }}</td>
									<td>{{ dt.item_qty }}</td>
									<td>{{ dt.sum_out }}</td>
									<td>{{ dt.sum_in }}</td>
									<td>{{ dt.pr_desc }}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			</br></br></br>
			<div class="row">
				<div class="col-md-12" style="text-transform: capitalize; text-align: center;">
					<paginate 
						:pagecount="totalpages"
						:containerclass="'pagination pagination-sm'"
						:clickhandler="pagination"
						:prevtext="prevButtonIcon"
						:nexttext="nextButtonIcon">
					</paginate>
				</div>
			</div>
			<div class="modal fade" id="proses_force_close" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<div class="row">
								<div class="col-sm-11">
									<h3 class="modal-title">FORCE CLOSE SO #{{dt_force.pr_no}}</h3>
								</div>
								<div class="col-sm-1" style="text-align: end;">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
							</div>
						</div>
						<div class="modal-body" style="background: white; padding-top: 1px;">
							<div class="form-group">
								<div class="row">
									<div class="col-sm-4">
										<label for="recipient-name" class="col-form-label">NAMA PELANGGAN</label>
									</div>
									<div class="col-sm-1">
										<p>:</p>
									</div>
									<div class="col-sm-6">
										<label for="recipient-name" class="col-form-label">{{dt_force.name}} [{{dt_force.vend_code}}]</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-sm-4">
										<label for="recipient-name" class="col-form-label">PERMINTAAN DARI</label>
									</div>
									<div class="col-sm-1">
										<p>:</p>
									</div>
									<div class="col-sm-6">
										<label for="recipient-name" class="col-form-label">{{dt_reason.user_id}}</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="recipient-name" class="col-form-label">ALASAN KARENA</label>
								<textarea class="form-control" rows="3" style="resize: none;" readonly>{{dt_reason.reason}}</textarea>
							</div>
							<div class="form-group">
								<select class="form-control" v-model="v_force_aksi">
									<option selected value="1">Menyetujui</option>
									<option value="0">Menolak</option>
								</select>
							</div>
							<div class="form-group">
								<label for="recipient-name" class="col-form-label">ALASANNYA</label>
								<textarea class="form-control" rows="3" style="resize: none;" v-model="v_force_alasan_hoo"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger btn-sm" @click="closeModalForce()">Batal</button>
							<button type="button" class="btn btn-primary btn-sm" @click="actionForceClose">Simpan</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	`,
	components: {
		vuejsDatepicker
	},
	props: ["username", "usergroup"],
	data() {
		return {
			loading: false,
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 8640000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
			v_search: "",
			v_date_start: moment(this.v_date_start).format("YYYY-MM-DD"),
			v_date_end: "",
			v_produk: "SAA",
			v_cabang: "ALL",
			v_gudang: "ALL",
			v_report: "Summary",
			v_periode: "",
			v_ledger: "",
			v_sales_code: "ALL",
			dt_produk: [],
			dt_requestClose: [],
			dt_cabang: [],
			dt_gudang: [],
			dt_sales: [],
			dt_report: [
				{
					"report": "summary",
					"label_report": "summary",
				},
				{
					"report": "detail",
					"label_report": "Detail",
				}
			],
			dt_gudang_all: [
				{
					"wh_code": "ALL"
				}
			],
			dt_sales_all: [
				{
					"sales_code": "ALL"
				}
			],
			col_requestclose: ['spk_no', 'vend_code', 'name',
				'pr_no', 'item_code', 'item_qty', 'ref_no_out',
				'sum_out', 'sum_in', 'pr_desc', 'action'],
			// gantung
			// opt_requestclose: {
			// 	headings: {
			// 		spk_no: "CABANG",
			// 		vend_code: "LEDGER",
			// 		name: 'NAME',
			// 		pr_no: 'NO. SO',
			// 		item_code: "ITEM CODE",
			// 		item_qty: "QTY SO",
			// 		ref_no_out: "REF NO OUT",
			// 		sum_out: "QTY DO",
			// 		sum_in: "OUTS",
			// 		pr_desc: "DESCRIPTION"
			// 	},
			// 	templates: {
			// 		action: 'action-force-close-comp'
			// 	},
			// 	sortIcon: {
			// 		base : 'fa',
			// 		is: 'fa-sort',
			// 		up: 'fa-sort-asc',
			// 		down: 'fa-sort-desc'
			// 	},
			// 	filterable: true,
			// 	childRow: "detail-force-close-component",
			// 	showChildRowToggler: true,
			//     footerHeadings: true,
			//     highlightMatches: true,
			//     columnsDropdown: false,
			//     uniqueKey: "pr_no",
			//     childRowTogglerFirst: false,
			// },
			/* pagination setting */
			pagestart: 0,
			pagecurrent: 1,
			itemsperpage: 50,
			pageend: 50,
			prevButtonIcon: '<i class="fa fa-chevron-left"></i>',
			nextButtonIcon: '<i class="fa fa-chevron-right"></i>',
			/* sort setting */
			currentSort: 'name',
			currentSortDir: 'asc',
			/* table column setting */
			activeColumn: {},
			new_columns: [
				{ name: 'number', order: 1 },
				{ name: 'spk_no', order: 1 },
				{ name: 'vend_code', order: 1 },
				{ name: 'name', order: 1 },
				{ name: 'pr_no', order: 1 },
				{ name: 'item_code', order: 1 },
				{ name: 'item_qty', order: 1 },
				{ name: 'sum_out', order: 1 },
				{ name: 'sum_in', order: 1 },
				{ name: 'pr_desc', order: 1 },
			],
			colTitles: {
				'number': 'NO.',
				'spk_no': 'Cabang',
				'vend_code': 'Ledger',
				'name': 'Name',
				'pr_no': 'No. SO',
				'item_code': 'Item Code',
				'item_qty': 'Qty SO',
				'ref_no_out': 'Ref No Out',
				'sum_out': 'Qty DO',
				'sum_in': 'Outs',
				'pr_desc': 'Description'
			},
			v_search_so: "",
			dt_force: [],
			dt_reason: [],
			v_force_aksi: "1",
			v_force_alasan_hoo: "",
			sum_qty_so: 0,
			sum_qty_do: 0,
			sum_out: 0,
		}
	},
	methods: {
		openModalForce() {
			$("#proses_force_close").modal("show");
		},
		closeModalForce() {
			$("#proses_force_close").modal("hide");
		},
		proses(data) {
			EventBus.$emit('open_modal_proses_force_close', data);
		},
		email(data) {
			EventBus.$emit('open_email_force_close', data);
		},
		getKategori() {
			axios.post(this.action, {
				case: 'getKategori',
				username: this.username
			}).then(response => {
				this.dt_produk = response.data.data;
				this.getCabang(this.dt_produk[0].category, this.username);
			});
		},
		getCabang(category) {
			axios.post(this.action, {
				case: 'getCabang',
				category: category,
				username: this.username,
			}).then(response => {
				this.dt_cabang = response.data.data;
				this.getGudang(this.dt_cabang[0].coce_code);
			});
		},
		getGudang(coce_code) {
			axios.post(this.action, {
				case: 'getGudangOutstanding',
				coce_code: coce_code,
				username: this.username,
			}).then(response => {
				this.dt_gudang = response.data[0];
				this.dt_gudang.push(this.dt_gudang_all[0]);
			});
		},
		getSales() {
			axios.post(this.action, {
				case: 'getSales',
				coce_code: this.v_cabang,
				username: this.username,
				usergroup: this.usergroup
			}).then(response => {
				this.dt_sales = response.data.data;
				this.dt_sales.push(this.dt_sales_all[0]);
			});
		},
		changeKategory() {
			this.getCabang(this.v_produk);
		},
		getForceCloseAprove() {
			this.sum_qty_so = 0;
			this.sum_qty_do = 0;
			this.sum_out = 0;

			this.loading = true;
			this.dt_requestClose = [];

			axios.post(this.action, {
				case: 'getForceCloseAprove',
				shift: this.v_produk,
				coce_code: this.v_cabang,
				sales_code: this.v_sales_code,
				wh_code: this.v_gudang,
				ledger: this.v_search,
				report: this.v_report,
				date2: moment(this.v_date_start).format("YYYY-MM-DD"),
			}).then(response => {
				this.loading = false;
				if (response.data == null) {
					this.dt_requestClose = [];
				} else {
					this.dt_requestClose = response.data;
				}
				console.log(this.dt_requestClose);
			});
		},
		pagination(index) {
			if (index >= 0 && index <= this.totalpages) {
				this.pagecurrent = index;
				this.pagestart = (this.itemsperpage * index) - this.itemsperpage;
				this.pageend = (this.itemsperpage * index);
			}
		},
		sortBy(column) {
			if (column === this.currentSort) {
				this.currentSortDir = this.currentSortDir === 'asc' ? 'desc' : 'asc';
			}
			this.currentSort = column;
		},
		getNow() {
			const today = new Date();
			const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
			const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			const dateTime = date + ' ' + time;
			this.v_periode = dateTime;
		},
		actionForceClose() {

			this.closeModalForce();
			this.loading = true;

			//gantung
			var data = {
				"action": 'approvedCloseSO',
				"aksi": 'F',
				"coce_code": this.v_cabang,
				"status": 'W',
				"shift": this.dt_force.code_comp.toLowerCase(),
				"date2": moment(this.dt_force.pr_date).format("YYYY-MM-DD"),
				"pr_no": this.dt_force.pr_no,
				"usergroup": 'EDP',
				"reason": '',
				"user_id": this.dt_reason.user_id,
				"reason_desc2": this.dt_reason.reason,
				"aksi_hoo": this.v_force_aksi,
				"reason_hoo": this.v_force_alasan_hoo,
				"reason_desc": ''
			};

			$.post("../../sales/actionForceClose_bak.php",
				data,
				function (data, status) {
					data = JSON.parse(data);
					console.log(data);
					console.log(status);
					this.loading = false;
					this.getForceClose();
				}
			);

		},
		send_mail() {
			// console.log(this.dt_force);
			axios.post(this.action, {
				case: 'sendEmail',
				action: 'approvedCloseSO',
				aksi: 'F',
				coce_code: this.v_cabang,
				status: 'W',
				shift: this.dt_force.code_comp.toLowerCase(),
				date2: moment(this.dt_force.pr_date).format("YYYY-MM-DD"),
				pr_no: this.dt_force.pr_no,
				usergroup: 'EDP',
				reason: '',
				user_id: this.dt_reason.user_id,
				reason_desc2: this.dt_reason.reason,
				aksi_hoo: this.v_force_aksi,
				reason_hoo: this.v_force_alasan_hoo,
				reason_desc: '',
				data_all: this.dt_force
			}).then(response => {
				console.log(response.data);
			});
		}
	},
	created() {
		this.getKategori();
		this.getNow();
		EventBus.$on("open_modal_proses_force_close", (data) => {
			this.dt_force = data;
			this.dt_reason = data.reason[0]
			this.openModalForce();
		});

		EventBus.$on("open_email_force_close", (data) => {
			this.dt_force = data;
			this.dt_reason = data.reason[0];
			this.send_mail();
		});
	},
	computed: {
		filterListST() {
			let filterST = this.v_search_so

			let result = this.dt_requestClose.filter(function (e) {
				let filtered = true
				if (filterST && filterST.length > 0) {
					filtered = e.so_no.includes(filterST)
				}
				return filtered;

			}).sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});

			return result;
		},
		totalpages() {
			//This calculates the amount of pages
			return Math.ceil(this.filterListST.length / this.itemsperpage);
		},
		sortedData: function () {
			return this.dt_requestClose.sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});
		}
	},
	watch: {
		data: function () {
			this.pagecurrent = 1;
			this.pagestart = 0;
			this.pageend = this.itemsperpage;
		},
	}
});

Vue.component('temp-dis-plan-gudang-asal-new', {
	props: ["data", "index", "column"],
	template: `
		<div v-if="data.detail[0]['f24'] != null">
			<label class='label label-primary lb-sm'>{{ data.detail[0]['f24'] }} => {{ data.detail[0]['f3'] }}</label>
		</div>
		<div v-else>
			
		</div>`,
});

Vue.component('temp-dis-plan-gudang-asal', {
	props: ["data", "index", "column"],
	template: `<label class='label label-primary lb-sm'>{{data.asal_gudang}}</label>`,
});

Vue.component('temp-dis-plan-so-asal', {
	props: ["data", "index", "column"],
	template: `<label class='label label-primary lb-sm'>{{data.item[0].cabang}}</label>`,
});

Vue.component('action-dis-plan-kirim-comp', {

	// data.asal_gudang === ( [0] => HO [1] => CLB )

	props: ['data', 'index', 'columns'],
	inject: ['location'],
	template: `
	<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
		<div class="btn-group mr-2" role="group" aria-label="First group">
			<button name="btn-load" class="btn btn-info btn-flat btn-xs" style="width:100%;" @click="proses(data)">BUAT DO</button>
		</div>
		<div class="btn-group mr-2" role="group" aria-label="Second group">
			<button name="btn-load" class="btn btn-danger btn-flat btn-xs" style="width:100%;" @click="batal(data)">BATAL</button>
		</div>
	</div>
	`,
	data() {
		return {
			arr_loc: []
		}
	},
	methods: {
		filter(cabang) {
			return this.location.filter(n => n === cabang).length === 0 ? false : true;
		},
		proses(data) {
			EventBus.$emit('open_modal_proses_kirim', data);
		},
		edit(data) {
			EventBus.$emit('open_modal_edit_kirim', data);
		},
		transfer(data) {
			EventBus.$emit('open_modal_transfer_kirim', data);
		},
		batal(data) {
			const swalWithBootstrapButtons = Swal.mixin({
				customClass: {
					confirmButton: 'btn btn-success',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})

			swalWithBootstrapButtons.fire({
				title: 'Are you sure?',
				text: "You want to delete this data?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					axios.post(this.action, {
						case: 'deleteDistributionPlan',
						id_dp: data.id,
						item: data.item
					}).then(response => {
						console.log(response.data);
						location.reload();
					});
				} else if (
					result.dismiss === Swal.DismissReason.cancel
				) {
					swalWithBootstrapButtons.fire(
						'Cancelled',
						'Your data is safe :)',
						'error'
					)
				}
			})

			// axios.post(this.action, { 
			// 	case: 'deleteDistributionPlan',
			// 	id_dp: data.id
			// }).then(response => {
			// 	console.log(response.data);
			// });
		}
	},
	created() {
		this.arr_loc = this.location;
	},
	computed: {
		loc_true() {

		}
	}

})

Vue.component('action-dis-plan-ambil-comp', {

	props: ['data', 'index', 'columns'],
	template: `
	<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
		<div class="btn-group mr-2" role="group" aria-label="First group">
			<button name="btn-load" class="btn btn-info btn-flat btn-xs" style="width:100%;" @click="proses(data)">BUAT DO</button>
		</div>
		<div class="btn-group mr-2" role="group" aria-label="Second group">
			<button name="btn-load" class="btn btn-danger btn-flat btn-xs" style="width:100%;" @click="batal(data)">BATAL</button>
		</div>
	</div>
	`,
	methods: {
		proses(data) {
			// EventBus.$emit('open_modal_proses_ambil', data);
			function submit_hidden_form(url, params) {
				var f = $("<form method='POST' target='_blank' style='display:none;'></form>").attr({
					action: url
				}).appendTo(document.body);
				for (var i in params) {
					if (params.hasOwnProperty(i)) {
						$('<input type="hidden" />').attr({
							name: i,
							value: params[i]
						}).appendTo(f);
					}
				}
				f.submit();
				f.remove();
			}

			submit_hidden_form(this.doPage3, {
				dp_id: data.id,
			})
		},
		edit(data) {
			EventBus.$emit('open_modal_edit_ambil', data);
		},
		batal(data) {
			const swalWithBootstrapButtons = Swal.mixin({
				customClass: {
					confirmButton: 'btn btn-success',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})

			swalWithBootstrapButtons.fire({
				title: 'Are you sure?',
				text: "You want to delete this data?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					axios.post(this.action, {
						case: 'deleteDistributionPlan',
						id_dp: data.id
					}).then(response => {
						console.log(response.data);
						location.reload();
					});
				} else if (
					result.dismiss === Swal.DismissReason.cancel
				) {
					swalWithBootstrapButtons.fire(
						'Cancelled',
						'Your data is safe :)',
						'error'
					)
				}
			})

			// axios.post(this.action, { 
			// 	case: 'deleteDistributionPlan',
			// 	id_dp: data.id
			// }).then(response => {
			// 	console.log(response.data);
			// });
		}
	}

})

Vue.component('action-dis-plan-transfer-comp', {

	props: ['data', 'index', 'columns'],
	template: `
	<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
		<div class="btn-group mr-2" role="group" aria-label="First group">
			<button name="btn-load" class="btn btn-info btn-flat btn-xs" style="width:100%;" @click="proses(data)">BUAT DO & TERIMA</button>
		</div>
		<div class="btn-group mr-2" role="group" aria-label="Second group">
			<button name="btn-load" class="btn btn-danger btn-flat btn-xs" style="width:100%;" @click="batal(data)">BATAL</button>
		</div>
	</div>
	`,
	methods: {
		proses(data) {
			EventBus.$emit('open_modal_proses_transfer', data);
		},
		batal(data) {
			const swalWithBootstrapButtons = Swal.mixin({
				customClass: {
					confirmButton: 'btn btn-success',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})

			swalWithBootstrapButtons.fire({
				title: 'Are you sure?',
				text: "You want to delete this data?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					axios.post(this.action, {
						case: 'deleteDistributionPlan',
						id_dp: data.id
					}).then(response => {
						console.log(response.data);
						location.reload();
					});
				} else if (
					result.dismiss === Swal.DismissReason.cancel
				) {
					swalWithBootstrapButtons.fire(
						'Cancelled',
						'Your data is safe :)',
						'error'
					)
				}
			})

			// axios.post(this.action, { 
			// 	case: 'deleteDistributionPlan',
			// 	id_dp: data.id
			// }).then(response => {
			// 	console.log(response.data);
			// });
		}
	}

})

Vue.component('action-force-close-comp', {
	// <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
	// 	<div class="btn-group mr-2" role="group" aria-label="First group" v-if="data.sum_in > 10">
	// 		<button name="btn-load" class="btn btn-info btn-flat btn-xs" style="width:100%;" @click="email(data)">KIRIM EMAIL</button>
	// 	</div>
	// 	<div class="btn-group mr-2" role="group" aria-label="First group" v-else>
	// 		<button name="btn-load" class="btn btn-warning btn-flat btn-xs" style="width:100%;" @click="proses(data)">FORCE CLOSE</button>
	// 	</div>
	// </div>
	props: ['data', 'index', 'columns'],
	template:
		`
	<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
		<div class="btn-group mr-2" role="group" aria-label="First group">
			<button name="btn-load" class="btn btn-warning btn-flat btn-xs" style="width:100%;" @click="proses(data)">FORCE CLOSE</button>
		</div>
	</div>
	`,
	methods: {
		proses(data) {
			EventBus.$emit('open_modal_proses_force_close', data);
		},
		email(data) {
			EventBus.$emit('open_email_force_close', data);
		},
	}
});

Vue.component('modal-proses-dp-comp', {

	template: `
	<div class="modal fade" id="proses_dp_so" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<div class="row">
						<div class="col-sm-8">
							<h3 class="modal-title">Buat DO & Kirim</h3>
						</div>
					</div>
				</div>
				</br>
				<div class="modal-body" style="background: white; padding-top: 1px;">
					<div class="form-group">
						<label class="col-form-label">Pilih Truck</label>
						<div class="row">
							<div class="col-sm-4">
								<label>
									Truck Operasional &nbsp;&nbsp;
									<input type="radio" id="truck_dalam" v-model="v_pilih_truck" value="truck_dalam">
								</label>
							</div>
							<div class="col-sm-4">
								<label>
									Truck Luar &nbsp;&nbsp;
									<input type="radio" id="truck_luar" v-model="v_pilih_truck" value="truck_luar">
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="col-form-label">No. Plat Truck</label>
						<v-select style="text-transform: capitalize" label="nopol" :options="dt_truck" v-model="v_nopol_truck" :reduce="dt_truck=>dt_truck.nopol" v-if="v_pilih_truck === 'truck_dalam'"></v-select>
						<input class="form-control" type="text" v-model="v_nopol_truck" v-else/>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="col-form-label">Nama Supir</label>
						<v-select style="text-transform: capitalize" label="sopir" :options="dt_supir" v-model="v_sopir_name" :reduce="dt_sopir=>dt_sopir.sopir" v-if="v_pilih_truck === 'truck_dalam'"></v-select>
						<input class="form-control" type="text" v-model="v_sopir_name" v-else/>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="col-form-label">Pilih Gudang</label>
						<v-select style="text-transform: capitalize" label="wh_code" :options="dt_gudang" v-model="v_gudang" :reduce="dt_gudang=>dt_gudang.wh_code"></v-select>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="col-form-label">Tanggal Kirim</label>
						<vuejs-datepicker
							v-model="v_tgl_kirim"
							:format="DatePickerFormatb"
							:disabledDates="disabledDates"
							:bootstrap-styling="true"
							:placeholder="holderb"
							required
							style="width: 290px;"
							@input="countDate">
						</vuejs-datepicker>
					</div>
					<div class="form-group" v-if="v_totalPengiriman > 3">
						<label for="recipient-name" class="col-form-label">Alasan</label>
						<textarea class="form-control" id="exampleFormControlTextarea1" rows="3" v-model="v_alasan" style="resize: none;"></textarea>
					</div>
					<!-- <div class="form-group" v-else>
						
					</div> -->
					<div class="form-group">
						<label for="recipient-name" class="col-form-label">Pilih Ritase</label>
						<select class="form-control" id="ritaseplan" name="ritaseplan" v-model="v_ritase">
							<option value="">...</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
						</select>
					</div>
				</div>
				<div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" @click="closeModal()">Batal</button>
                    <button type="button" class="btn btn-primary btn-sm" @click="saveProsesPengiriman">Simpan & Cetak DO</button>
				</div>
			</div>
		</div>
	</div>
	`,
	components: {
		vuejsDatepicker
	},
	username: {
		required: true
	},
	data() {
		return {
			show: false,
			id: 0,
			data: [],
			v_gd_start: '',
			v_nama_supir: '',
			v_no_plat: '',
			tgl_kirim: '',
			v_alasan: '',
			v_qty: '',
			dt_truck: [],
			dt_supir: [],
			dt_gudang: [],
			v_nopol_truck: "",
			v_sopir_name: "",
			username: 'rachmadi',
			v_totalPengiriman: '',
			v_tgl_kirim: '',
			v_pilih_truck: '',
			v_gudang: '',
			v_ritase: '',
			holdera: "-- Pilih Bulan --",
			holderb: "-- Pilih Tanggal --",
			DatePickerFormata: 'MMMM yyyy',
			DatePickerFormatb: 'dd MMMM yyyy',
			disabledDates: {
				to: new Date(Date.now() - 8640000)
			},
			startmonth: '',
			enddate: '',
			minv: 'month',
			monthNames: [
				{ value: "1", text: "January" },
				{ value: "2", text: "February" },
				{ value: "3", text: "March" },
				{ value: "4", text: "April" },
				{ value: "5", text: "May" },
				{ value: "6", text: "June" },
				{ value: "7", text: "July" },
				{ value: "8", text: "August" },
				{ value: "9", text: "September" },
				{ value: "10", text: "October" },
				{ value: "11", text: "November" },
				{ value: "12", text: "December" }
			],
		};
	},
	created() {
		EventBus.$on("open_modal_proses_kirim2", (data) => {
			this.data = data;
			this.getTruck();
			this.getSopir();
			this.getGudang();
			this.openModal();
		});
	},
	methods: {
		openModal() {
			$("#proses_dp_so").modal("show");
		},
		closeModal() {
			$("#proses_dp_so").modal("hide");
		},
		saveProsesPengiriman() {
			axios.post(this.action, {
				case: "saveProsesPengiriman",
				nama_supir: this.v_sopir_name,
				no_plat: this.v_nopol_truck,
				tgl_kirim: this.tgl_kirim,
				alasan: this.v_alasan,
				detail: this.data
			}).then(response => {
				console.log(response.data);
				this.closeModal();
			})
		},
		countDate() {
			var tgl_perintah = this.data.tgl_kirim;
			var tgl_kirim = moment(this.v_tgl_kirim).format("YYYY-MM-DD");

			dt1 = new Date(tgl_perintah);
			dt2 = new Date(tgl_kirim);

			this.v_totalPengiriman = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
		},
		getTruck() {
			axios.post(this.action, {
				case: "getTruck",
				username: "rachmadi",
			}).then(response => {
				this.dt_truck = response.data;
			})
		},
		getSopir() {
			axios.post(this.action, {
				case: "getSopir",
				username: "rachmadi",
			}).then(response => {
				this.dt_supir = response.data;
			})
		},
		getGudang() {
			axios.post(this.action, {
				case: "getGudang",
				username: "rachmadi",
			}).then(response => {
				this.dt_gudang = response.data;
				console.log(this.dt_gudang);
			})
		}
	},
	watch: {
		v_pilih_truck(value) {
			console.log(value);
		}
	}

});

Vue.component('daftar-pengiriman-comp', {

	template: `
	<div>
		<div class="row">
			<div class="col-md-12" style="text-transform: capitalize;">
				<tabs_distribution_plan_component>
					<tab_distribution_plan_component v-for="(dt,i) in tabs" :key="i" :name="dt.name" :icon="dt.icon" :selected="i === 0 ? 'true' : ''">
						<div v-if="i == 0">
							<daf_pengiriman_kirim_component :data="pengiriman_kirim"></daf_pengiriman_kirim_component>
						</div>
						<div v-if="i == 1">
							<daf_pengiriman_ambil_component :data="pengiriman_ambil"></daf_pengiriman_ambil_component>
						</div>
						<div v-if="i == 2">
							<daf_pengiriman_transfer_component :data="pengiriman_transfer"></daf_pengiriman_transfer_component>
						</div>
					</tab_distribution_plan_component>
				</tabs_distribution_plan_component>
			</div>
		</div>
	</div>
	`,
	props: ['loc', 'where', 'where2'],
	components: {
		vuejsDatepicker
	},
	data() {
		return {
			tabs: [
				{
					name: 'DO KIRIM',
					icon: '<i class="fa fa-print"></i>',
					counter: 0,
				},
				{
					name: 'DO AMBIL',
					icon: '<i class="fa fa-print"></i>',
					counter: 1,
				},
				{
					name: 'DO PENGALIHAN',
					icon: '<i class="fa fa-print"></i>',
					counter: 2,
				},
			],
			v_tgl_perintah_kirim: '',
			pengiriman_kirim: [],
			pengiriman_ambil: [],
			pengiriman_transfer: []
		}
	},
	methods: {
		getDaftarPengirimanKirim() {
			axios.post(this.action, {
				case: 'getDaftarPengirimanKirim',
				loc: this.loc,
				where: this.where
			}).then(response => {
				this.pengiriman_kirim = response.data;
				console.log(this.pengiriman_kirim);
			});
		},
		getDaftarPengirimanAmbil() {
			axios.post(this.action, {
				case: 'getDaftarPengirimanAmbil',
				loc: this.loc,
				where: this.where
			}).then(response => {
				this.pengiriman_ambil = response.data;
			});
		},
		getDaftarPengirimanTransfer() {
			axios.post(this.action, {
				case: 'getDaftarPengirimanTransfer',
				loc: this.loc,
				where: this.where
			}).then(response => {
				this.pengiriman_transfer = response.data;
			});
		}
	},
	created() {
		this.getDaftarPengirimanKirim();
		this.getDaftarPengirimanAmbil();
		this.getDaftarPengirimanTransfer();
	}

});

Vue.component('daf_pengiriman_kirim_component', {

	template: `
	<div>
		<div class="row">
			<div class="col-md-12" style="text-transform: capitalize;">
				<v-client-table :data="data" :columns="columns" :options="options"></v-client-table>
			</div>
		</div>
	</div>
	`,
	props: {
		data: {
			type: Array,
			required: true
		},
	},
	data() {
		return {
			columns: ['no_dp', 'name', 'tgl_kirim2', 'wh_code', 'no_pol', 'sopir', 'action'],
			options: {
				perPage: 50,
				sortable: ['type', 'code', 'ledgername'],
				headings: {
					no_dp: 'NO. DP',
					name: 'NAMA',
					tgl_kirim2: 'TGL. KIRIM',
					wh_code: 'GUDANG ASAL',
					no_pol: 'No. Pol',
					sopir: 'Sopir'
				},
				sortable: ['no_dp', 'wh_code'],
				texts: {
					count: "",
					filter: "Cari",
					filterPlaceholder: "Cari No. DP",
				},
				templates: {
					action: 'action-daf-dist-kirim-comp'
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				filterable: false,
				childRow: "detail-pengiriman-kirim-component",
				showChildRowToggler: true,
				footerHeadings: true,
				highlightMatches: true,
				columnsDropdown: false,
				uniqueKey: "id",
				childRowTogglerFirst: false,
			}
		}
	}

});

Vue.component('daf_pengiriman_ambil_component', {

	template: `
	<div>
		<div class="row">
			<div class="col-md-12" style="text-transform: capitalize;">
				<v-client-table :data="data" :columns="columns" :options="options"></v-client-table>
			</div>
		</div>
	</div>
	`,
	props: {
		data: {
			type: Array,
			required: true
		},
	},
	data() {
		return {
			columns: ['no_dp', 'tgl_perintah2', 'wh_code', 'no_pol', 'sopir', 'action'],
			options: {
				perPage: 50,
				sortable: ['type', 'code', 'ledgername'],
				headings: {
					no_dp: 'NO. DP',
					tgl_perintah2: 'TGL. AMBIL',
					wh_code: 'GUDANG ASAL',
					no_pol: 'NO. POL',
					sopir: 'SOPIR'
				},
				sortable: ['no_dp', 'wh_code'],
				texts: {
					count: "",
					filter: "Cari",
					filterPlaceholder: "Cari No. DP",
				},
				templates: {
					action: 'action-daf-dist-kirim-comp'
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				filterable: false,
				childRow: "detail-pengiriman-kirim-component",
				showChildRowToggler: true,
				footerHeadings: true,
				highlightMatches: true,
				columnsDropdown: false,
				uniqueKey: "id",
				childRowTogglerFirst: false,
			}
		}
	}

});

Vue.component('daf_pengiriman_transfer_component', {

	template: `
	<div>
		<div class="row">
			<div class="col-md-12" style="text-transform: capitalize;">
				<v-client-table :data="data" :columns="columns" :options="options"></v-client-table>
			</div>
		</div>
	</div>
	`,
	props: {
		data: {
			type: Array,
			required: true
		},
	},
	data() {
		return {
			columns: ['no_dp', 'tgl_kirim2', 'wh_code', 'no_pol', 'sopir', 'action'],
			options: {
				perPage: 50,
				sortable: ['type', 'code', 'ledgername'],
				headings: {
					no_dp: 'NO. DP',
					tgl_kirim2: 'TGL. KIRIM',
					wh_code: 'GUDANG ASAL',
					no_pol: 'NO. POL',
					sopir: 'SOPIR'
				},
				sortable: ['no_dp', 'wh_code'],
				texts: {
					count: "",
					filter: "Cari",
					filterPlaceholder: "Cari No. DP",
				},
				templates: {
					action: 'action-daf-dist-kirim-comp'
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				filterable: false,
				childRow: "detail-pengiriman-kirim-component",
				showChildRowToggler: true,
				footerHeadings: true,
				highlightMatches: true,
				columnsDropdown: false,
				uniqueKey: "id",
				childRowTogglerFirst: false,
			}
		}
	}

});

Vue.component('detail-pengiriman-kirim-component', {

	props: ["data", "index", "columns"],
	template: `
	<div>
		<div class="row">
			<div class="col-sm-12" style="text-transform: capitalize;">
				<v-client-table :data="data.item" :columns="col_kirim" :options="opt_kirim"></v-client-table>
			</div>
		</div>
	</div>
	`,
	data() {
		return {
			col_kirim: ['code', 'name', 'item_code', 'item_qty2', 'action'],
			opt_kirim: {
				headings: {
					code: "NAMA",
					name: "LEDGER",
					item_code: 'CODE COMP',
					item_qty2: 'ITEM CODE'
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc',
				},
				templates: {
					action: 'action-pengiriman-kirim-comp'
				},
				filterable: false
			}
		}
	},
	methods: {

	},
	created() {

	}

});

Vue.component('detail-force-close-component', {

	props: ["data", "index", "columns"],
	template: `
	<div>
		<div class="row">
			<div class="col-sm-12" style="text-transform: capitalize;">
				<v-client-table :data="data.item" :columns="col_kirim" :options="opt_kirim"></v-client-table>
			</div>
		</div>
	</div>
	`,
	data() {
		return {
			col_kirim: ['ref_no', 'voucher_desc', 'car_no', 'driver'],
			opt_kirim: {
				headings: {
					ref_no: "REF NO IN",
					voucher_desc: "REASON",
					car_no: 'PLAT',
					driver: 'SOPIR'
				},
				sortIcon: {
					base: 'fa',
					is: 'fa-sort',
					up: 'fa-sort-asc',
					down: 'fa-sort-desc'
				},
				templates: {

				},
				filterable: false
			}
		}
	},
	methods: {

	},
	created() {

	}

});

Vue.component('action-pengiriman-kirim-comp', {

	props: ['data', 'index', 'columns'],
	template: `
	<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
		<div class="btn-group mr-2" role="group" aria-label="First group" v-if="oldSO == data.so_no">
			<button name="btn-load" class="btn btn-info btn-flat btn-xs" style="width:100%;" @click="cetak(data)"><i class="fa fa-print"></i> CETAK</button>
		</div>
		<div class="btn-group mr-2" role="group" aria-label="First group" v-else>
		</div>
		<div style="display: none;">{{ oldSO = data.so_no }}</div>
	</div>
	`,
	data() {
		return {
			oldSO: ''
		}
	},
	methods: {
		cetak(data) {
			// console.log(data.id);
			// console.log(this.doPage);

			function submit_hidden_form(url, params) {
				var f = $("<form method='POST' target='_blank' style='display:none;'></form>").attr({
					action: url
				}).appendTo(document.body);
				for (var i in params) {
					if (params.hasOwnProperty(i)) {
						$('<input type="hidden" />').attr({
							name: i,
							value: params[i]
						}).appendTo(f);
					}
				}
				f.submit();
				f.remove();
			}

			submit_hidden_form(
				this.doPage, {
				id: data.id,
			}
			)
		}
	}

});

Vue.component('action-daf-dist-kirim-comp', {

	props: ['data', 'index', 'columns'],
	template: `
	<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
		<div class="btn-group mr-2" role="group" aria-label="Second group">
			<button name="btn-load" class="btn btn-danger btn-flat btn-xs" style="width:100%;" @click="batal(data)">BATAL</button>
		</div>
	</div>
	`,
	methods: {
		batal(data) {
			// console.log(data);
			const swalWithBootstrapButtons = Swal.mixin({
				customClass: {
					confirmButton: 'btn btn-success',
					cancelButton: 'btn btn-danger'
				},
				buttonsStyling: false
			})

			swalWithBootstrapButtons.fire({
				title: 'Are you sure?',
				text: "You want to delete this data?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes, delete it!',
				cancelButtonText: 'No, cancel!',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					axios.post(this.action, {
						case: 'deleteDistributionPlan',
						id_dp: data.id
					}).then(response => {
						console.log(response.data);
						location.reload();
					});
				} else if (
					result.dismiss === Swal.DismissReason.cancel
				) {
					swalWithBootstrapButtons.fire(
						'Cancelled',
						'Your data is safe :)',
						'error'
					)
				}
			})

			axios.post(this.action, {
				case: 'deleteDistributionPlan',
				id_dp: data.id
			}).then(response => {
				console.log(response.data);
			});
		}
	}

});

Vue.component('report_displan_comp', {
	template:
		`
	<div>
		<div class="row">
			<div class="col-md-2">
				<label class="control-label text-center">Pilih Cabang</label>
			</div>
			<div class="col-md-2">
				<label class="control-label text-center">Pilih Bulan</label>
			</div>
			<div class="col-md-2">
				<label class="control-label text-center">Pilih Tahun</label>
			</div>
			<div class="col-md-2">
				<label class="control-label text-center">Tipe</label>
			</div>
		</div>
		<div class="row">
			<form @submit.prevent="getReportDisplan">
				<div class="col-md-2">
					<v-select style="text-transform: capitalize" label="name" :options="listcabang" v-model="v_cabang" :reduce="listcabang=>listcabang.code"></v-select>
				</div>
				<div class="col-md-2">
					<v-select style="text-transform: capitalize" label="nama" :options="dataBulan" v-model="v_bulan" :reduce="dataBulan=>dataBulan.code"></v-select>
				</div>
				<div class="col-md-2">
					<v-select style="text-transform: capitalize" label="nama" :options="dataTahun" v-model="v_tahun" :reduce="dataTahun=>dataTahun.code"></v-select>
				</div>
				<div class="col-md-2">
					<v-select style="text-transform: capitalize" label="name" :options="dataPromosi" v-model="v_tipe" :reduce="dataPromosi=>dataPromosi.tipe"></v-select>
				</div>
				<div class="col-md-1">
					<button type="button" class="btn btn-info" @click="showData"><i class="fa fa-search"></i></button>
				</div>
				<div class="col-md-1">
					<button type="button" class="btn btn-danger" @click="downoladExcel"><i class="fa fa-download"></i></button>
				</div>
			</form>	
		</div>
		</br></br></br>
		<div class="row">
			<div class="col-sm-10">

			</div>
			<div class="col-sm-2">
				<input type="text" class="form-control" placeholder="No. SO" v-model="v_search_so">
			</div>
		</div>
		</br>
		<div class="row">
			<div class="col-sm-12">
				<div class="lds-facebook" v-if="loading"><div></div><div></div><div></div></div>
				<div style="overflow-x: auto;">
					<table class="table table-hover table-responsive table-striped" style="font-size: 12px;">
						<thead>
							<tr>
								<th v-for="column in new_columns" @click="activeColumn = column" class="active">
									{{ colTitles[column.name] }} 
									<span @click="column.order = column.order * (-1), sortBy(column.name)" class="arrow" :class="column.order > 0 ? 'asc' : 'dsc'" ></span>
								</th>
								<th class="active"></th>
							</tr>
						</thead>
						<tbody>
							<tr v-for="(dt,i) in filterListST" :key="i"  v-if="i >= pagestart && i < pageend" style="text-transform: uppercase;">
								<td>{{dt.comp}}</td>
								<td>{{dt.tgl_kirim }}</td>
								<td>{{dt.no_dp}}</td>
								<td>{{dt.tgl_so }}</td>
								<td>{{dt.tipe}}</td>
								<td>{{dt.no_so}}</td>
								<td>{{dt.code}}</td>
								<td>{{dt.name}}</td>
								<td>{{dt.item_code}}</td>
								<td>{{dt.qty_so}}</td>
								<td>{{dt.qty_dp}}</td>
								<td>
									<span class="label label-success" v-if="dt.waktu_kirim < 0">Hari Sebelumnya</span>
									<span class="label label-success" v-if="dt.waktu_kirim == 0">Hari yg sama</span>
									<span class="label label-info" v-if="dt.waktu_kirim > 0 && dt.waktu_kirim <=2">{{dt.waktu_kirim}} hari</span>
									<span class="label label-warning" v-if="dt.waktu_kirim > 2 && dt.waktu_kirim <= 5">{{dt.waktu_kirim}} hari</span>
									<span class="label label-danger" v-if="dt.waktu_kirim > 5">{{dt.waktu_kirim}} hari</span>
								</td>
								<td style="background-color: #f9f9f9" v-if="dt.waktu_kirim > 1"><button class="btn btn-danger btn-sm" @click="showReason(dt.no_dp,dt.no_so)"><i class="fa fa-commenting-o"></i></button></td>
								<td style="background-color: #f9f9f9" v-else>&nbsp;</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		</br>
		<div class="row">
			<div class="col-md-12" style="text-transform: capitalize; text-align: center;">
				<paginate 
					:pagecount="totalpages"
					:containerclass="'pagination pagination-sm'"
					:clickhandler="pagination"
					:prevtext="prevButtonIcon"
					:nexttext="nextButtonIcon">
				</paginate>
			</div>
		</div>
		<div class="modal fade" id="modalDetail">
			<div class="modal-dialog">
				<div class="modal-content" style="border-radius: 6px;">
					<div class="modal-header" style="background-color: #428bca;color: #fff;border-radius: 6px 6px 0px 0px;">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title"><i class="fa fa-exclamation-circle"></i> Alasan Keterlambatan </h4>
					</div>
					<div class="modal-body">
						<div v-if="dt_alasan.length > 0">
							<dl class="dl-horizontal" v-for="(inf,i) in dt_alasan" :key="i">
								<dt>PRODUK</dt>
									<dd><span :class="inf.code_comp == 'SAA' ? 'label label-primary':'label label-warning'">{{inf.code_comp}}</span></dd>
								<dt>No DP</dt>
									<dd>{{ nodp }}</dd>
								<dt>Konsumen</dt>
									<dd>{{ inf.code }} {{ inf.name }}</dd>
									<dd>{{ inf.alamat_toko }}</dd>
								<dt>No SM/SO</dt>
									<dd>{{ noso }}  <span class="label label-info">Kode Barang: {{ inf.item_code }} Qty: {{ inf.item_qty2 }} [tonase: {{ inf.item_ton }} Ton]</span></dd>
								<dt>Keterangan</dt>
									<dd>{{inf.keterangan}}</dd>
								<dt>Alasan Terlambat</dt>
									<dd>{{inf.alasan}}</dd>
								<dt>Dibuat Oleh</dt>
									<dd>{{inf.insert_by}} [{{inf.insert_time}}]</dd>
							</dl>
						</div>
						<div v-else>

						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">TUTUP</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	`,
	data() {
		return {
			loading: false,
			v_cabang: '',
			v_bulan: '',
			v_tahun: '',
			v_tipe: '',
			listcabang: [],
			dataRpt: [],
			dataBulan: [
				{ nama: 'Januari', code: '01' },
				{ nama: 'Februari', code: '02' },
				{ nama: 'Maret', code: '03' },
				{ nama: 'April', code: '04' },
				{ nama: 'Mei', code: '05' },
				{ nama: 'Juni', code: '06' },
				{ nama: 'Juli', code: '07' },
				{ nama: 'Agustus', code: '08' },
				{ nama: 'September', code: '09' },
				{ nama: 'Oktober', code: '10' },
				{ nama: 'November', code: '11' },
				{ nama: 'Desember', code: '12' }
			],
			dataTahun: [
				{ nama: '2019', code: '2019' },
				{ nama: '2020', code: '2020' },
				{ nama: '2021', code: '2021' },
			],
			dataPromosi: [
				{ name: 'Semua', tipe: 'semua' },
				{ name: 'Tanpa Promosi', tipe: 'tanpa' },
			],
			v_search_so: '',
			/* pagination setting */
			pagestart: 0,
			pagecurrent: 1,
			itemsperpage: 50,
			pageend: 50,
			prevButtonIcon: '<i class="fa fa-chevron-left"></i>',
			nextButtonIcon: '<i class="fa fa-chevron-right"></i>',
			/* sort setting */
			currentSort: 'name',
			currentSortDir: 'asc',
			/* table column setting */
			activeColumn: {},
			new_columns: [
				{ name: 'comp', order: 1 },
				{ name: 'tgl_kirim', order: 1 },
				{ name: 'no_dp', order: 1 },
				{ name: 'tgl_so', order: 1 },
				{ name: 'tipe', order: 1 },
				{ name: 'so_no', order: 1 },
				{ name: 'code', order: 1 },
				{ name: 'ledgername', order: 1 },
				{ name: 'item_code', order: 1 },
				{ name: 'qty_so', order: 1 },
				{ name: 'qty_dp', order: 1 },
				{ name: 'waktu_kirim', order: 1 }
			],
			colTitles: {
				'comp': 'Comp',
				'tgl_kirim': 'Tgl. Kirim',
				'no_dp': 'DP NO',
				'tgl_so': 'Tgl. SO',
				'tipe': 'Tipe',
				'so_no': 'NO SO',
				'code': 'Kode',
				'ledgername': 'Nama Toko',
				'item_code': 'Item Code',
				'qty_so': 'Qty SO',
				'qty_dp': 'Qty DP',
				'waktu_kirim': 'Waktu Kirim'
			},
			dt_alasan: [],
			nodp: '',
			noso: ''

		}
	},
	methods: {
		getDataCabang() {
			axios.post(this.action, {
				case: 'getCabang2'
			}).then(response => {
				this.listcabang = response.data;
			}).catch(error => {
				console.log(error);
			});
		},
		pagination(index) {
			if (index >= 0 && index <= this.totalpages) {
				this.pagecurrent = index;
				this.pagestart = (this.itemsperpage * index) - this.itemsperpage;
				this.pageend = (this.itemsperpage * index);
			}
		},
		sortBy(column) {
			if (column === this.currentSort) {
				this.currentSortDir = this.currentSortDir === 'asc' ? 'desc' : 'asc';
			}
			this.currentSort = column;
		},
		showData() {
			this.loading = true;
			axios.post(this.action, {
				case: 'getReportDisplan',
				cabang: this.v_cabang,
				bulan: this.v_bulan,
				tahun: this.v_tahun,
				promo: this.v_tipe
			}).then(response => {
				this.dataRpt = response.data;
				this.loading = false;
			}).catch(error => {
				console.log(error);
			});
		},
		downoladExcel() {

			let cabang = this.v_cabang;
			let bulan = this.v_bulan;
			let tahun = this.v_tahun;
			let promo = this.v_tipe;

			// this.loadingDownload = true;

			var data = {
				cabang,
				bulan,
				tahun,
				promo
			};

			$.post("report_distribution_plan_detail2.php",
				JSON.stringify(data),
				function (response) {
					window.location.href = JSON.parse(response)
					console.log(response.data)
				}
			)

		},
		showReason(nodp, nososm) {
			$('#modalDetail').modal('show');

			axios.post(this.action, {
				case: 'getReason',
				nodp: nodp, 
				noso: nososm
			}).then(response => {
				this.dt_alasan = response.data;
				this.nodp = nodp;
				this.noso = nososm;
				console.log(response.data);
			}).catch(error => {
				console.log(error);
			});
		}
	},
	created() {
		this.getDataCabang();
	},
	computed: {
		filterListST() {
			let filterST = this.v_search_so

			let result = this.dataRpt.filter(function (e) {
				let filtered = true
				if (filterST && filterST.length > 0) {
					filtered = e.so_no.includes(filterST)
				}
				return filtered;

			}).sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});

			return result;
		},
		totalpages() {
			//This calculates the amount of pages
			return Math.ceil(this.filterListST.length / this.itemsperpage);
		},
		sortedData: function () {
			return this.dataRpt.sort((a, b) => {
				let modifier = 1;
				if (this.currentSortDir === 'desc') modifier = -1;
				if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
				if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
				return 0;
			});
		}
	},
	watch: {
		data: function () {
			this.pagecurrent = 1;
			this.pagestart = 0;
			this.pageend = this.itemsperpage;
		},
	}
});

