<?
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(1);

    // include_once '../invconfig.php';
    include_once '../../../web-bin/function.php';
    include_once '../../function/function_sendmail.php';

    if (!isset($dbgl)) {
		try {
			$dbgl = new PDO($pggl, $pguser, $pgpass);
			$dbgl->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$dbgl->query("SET application_name = '".realpath(dirname(__FILE__))."/".basename(__FILE__)."/".$username."'");
		} catch (PDOException $e) {
			$errorMsg = 'DBGL '.$username.' '.$e->getMessage()."\nError on line ".$e->getLine()." in ".$e->getFile();
			sendMessageBot($groupWebChatId, $errorMsg, $webSupportBot);
			// throw new pdoDbException($e);
		}
	}

    // ini_set('display_errors', 1);
    // ini_set('display_startup_errors', 1);
    // error_reporting(1);

    $GLOBALS['groupWebChatId']  = $groupWebChatId;
    $GLOBALS['botInventory']    = $botInventory;
    $GLOBALS['botWebSupport']   = $webSupportBot;
    $username   = $_SESSION['username'];

    // $location   = $_SESSION['location'];
    $menu_desc  = $accGroup['menu_desc'];
	$data = json_decode(file_get_contents("php://input"));
	
    // $data->location = $location;
    
    $filename = $_FILES['file']['name'];

    $case = $data->case? $data->case : $_POST['case'];
	
    switch ($case) {
        case 'getDataSOKirim': 
            // $result = getDataSOKirim($dbgl,$data);
            $result = getDataSOKirimNew($dbgl,$data);
            echo json_encode($result);
            break; 
        case 'getDataSOKirimNew2': 
            $result = getDataSOKirimNew2($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getDataSOAmbil': 
            $result = getDataSOAmbil($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getOfficeCode':
            $result = getOfficeCode($dbh,$data);
            echo json_encode($result);
            break;
        case 'saveDistributionPlanKirim': 
            $result = saveDistributionPlanKirim($dbgl,$data);
            echo json_encode($result);
            break;
        case 'saveDistributionPlanAmbil': 
            $result = saveDistributionPlanAmbil($dbgl,$data);
            echo json_encode($result);
            break;
        case 'simpanDistributionPlanKirim': 
            $result = actionSaveDistributionPlanKirim($dbgl,$data);
            echo json_encode($result);
            break;
        case 'simpanDistributionPlanKirimNew':
            $result = actionSaveDistributionPlanKirimNew($dbgl,$data);
            echo json_encode($result);
            break; 
        case 'simpanDistributionPlanAmbil':
            $result = actionSaveDistributionPlanAmbil($dbgl,$data);
            echo json_encode($result); 
            break;
        case 'simpanDistributionPlanAmbilNew':
            $result = actionSaveDistributionPlanAmbilNew($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getDisPlanListKirim': 
            $result = getDisPlanListKirim($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getDisPlanListAmbil': 
            $result = getDisPlanListAmbil($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getDisPlanListTransfer': 
            $result = getDisPlanListTransfer($dbgl,$data);
            echo json_encode($result);
            break;
        case 'saveProsesPengiriman': 
            $result = saveProsesPengiriman($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getDaftarPengirimanKirim': 
            $result = getDaftarPengirimanKirim($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getDaftarPengirimanKirimByID': 
            $result = getDaftarPengirimanKirimByID($dbgl,$data);
            echo json_encode($result); 
            break;
        case 'getDaftarPengirimanAmbil':
            $result = getDaftarPengirimanAmbil($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getDaftarPengirimanAmbilByID':
            $result = getDaftarPengirimanAmbilByID($dbgl,$data);
            echo json_encode($result);
            break; 
        case 'deleteDistributionPlan': 
            $result = deleteDistributionPlan($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getTruck': 
            $result = getTruck($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getSopir':
            $result = getSopir($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getGudang': 
            $result = getGudang($dbgl,$data);
            echo json_encode($result);
            break;
        case 'updateDistributionPlan': 
            $result = updateDistributionPlan($dbgl,$data);
            echo json_encode($result);
            break;
        case 'updateDistributionPlanAmbil': 
            $result = updateDistributionPlanAmbil($dbgl,$data);
            echo json_encode($result);
            break; 
        case 'updateDistributionPlanAmbil2': 
            $result = updateDistributionPlanAmbil2($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getDisPlanKirimByDP': 
            $result = getDisPlanKirimByDP($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getDisPlanTransferByDP': 
            $result = getDisPlanTransferByDP($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getDisPlanAmbilByDP': 
            $result = getDisPlanAmbilByDP($dbgl,$data);
            echo json_encode($result);
            break;
        case 'updateDistributionPlanDetail': 
            $result = updateDistributionPlanDetail($dbgl,$data);
            echo json_encode($result);
            break; 
        case 'getNoTransfer': 
            $result = getNoTransfer($dbgl,$data);
            echo json_encode($result);
            break; 
        case 'getNoTransferByGudang': 
            $result = getNoTransferByGudang($dbgl,$data);
            echo json_encode($result);
            break; 
        case 'updateTransferDPKirim': 
            $result = updateTransferDPKirim($dbgl,$data);
            echo json_encode($result); 
            break;
        case 'updateProsesTransferDPKirim': 
            $result = updateProsesTransferDPKirim($dbgl,$data);
            echo json_encode($result);
            break; 
        case 'getDaftarPengirimanTransfer': 
            $result = getDaftarPengirimanTransfer($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getDaftarPengirimanTransferByID': 
            $result = getDaftarPengirimanTransferByID($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getKategori': 
            $result = getKategori($dbgl,$data);
            echo json_encode($result);
            break; 
        case 'getCabang': 
            $result = getCabang($dbgl,$data);
            echo json_encode($result);
            break;
        case 'getGudangOutstanding': 
            $result = getGudangOutstanding($dbgl,$data,$dbh);
            echo json_encode($result);
            break;
        case 'getOutstanding': 
            $result = getOutstanding($dbgl,$data,$dbh);
            echo json_encode($result); 
            break;
        case 'getForceClose': 
            $result = getForceClose($dbgl,$data,$dbh);
            echo json_encode($result);
            break; 
        case 'getForceCloseAprove': 
            $result = getForceCloseAprove($dbgl,$data,$dbh);
            echo json_encode($result);
            break; 
        case 'getSales': 
            $result = getSales($dbgl,$data,$dbh);
            echo json_encode($result);
            break;
        case 'sendEmail':
            $result = sendEmail($dbgl,$data,$dbh);
            echo json_encode($result); 
            break;
        case 'getSupirByNopol':
            $result = getSupirByNopol($dbgl,$data,$dbh);
            echo json_encode($result); 
            break;
        case 'getRitaseByNopol':
            $result = getRitaseByNopol($dbgl,$data,$dbh);
            echo json_encode($result); 
            break;  
        case 'updateStatusSO':
            $result = updateStatusSO($dbgl,$data,$dbh);
            echo json_encode($result); 
            break; 
        case 'saveDistributionPlanKirimTemp':
            $result = saveDistributionPlanKirimTemp($dbgl,$data,$dbh);
            echo json_encode($result); 
            break;  
        case 'getDataSOKirimSCReq':
            $result = getDataSOKirimSCReq($dbgl,$data,$dbh);
            echo json_encode($result); 
            break; 
        case 'updateApproveReqSales':
            $result = updateApproveReqSales($dbgl,$data,$dbh);
            echo json_encode($result); 
            break; 
        case 'updateDeleteReqSales':
            $result = updateDeleteReqSales($dbgl,$data,$dbh);
            echo json_encode($result); 
            break; 
        case 'getDatesFromRange':
            $start_date = date('Y-m-d');

            $today = new DateTime('today');
            $end_day = $today->modify('+6 day');
            $end_date = $end_day->format('Y-m-d');

            $result = getDatesFromRange($start_date, $end_date);
            echo json_encode($result); 
            break; 
        case 'getCabang2':
            $result = getCabang2($dbgl,$data,$dbh);
            echo json_encode($result); 
            break;  
        case 'getReportDisplan':
            $result = getReportDisplan($dbgl,$data,$dbh);
            echo json_encode($result); 
            break;  
        case 'getReason':
            $result = getReason($dbgl,$data,$dbh);
            echo json_encode($result);  
            break;
        case 'getDisPlanByID':
            $result = getDisPlanByID($dbgl,$data,$dbh);
            echo json_encode($result); 
            break;
        case 'getDisPlanListKirimByDate':
            $result = getDisPlanListKirimByDate($dbgl,$data);
            echo json_encode($result);  
            break;
        case 'getDisPlanListKirimByDateUp':
            $result = getDisPlanListKirimByDateUp($dbgl,$data);
            echo json_encode($result);  
            break;
        case 'getSopirByNoto':
            $result = getSopirByNoto($dbgl,$data);
            echo json_encode($result); 
            break;
        case 'updateCatatanTo':
            $result = updateCatatanTo($dbgl,$data);
            echo json_encode($result);
            break;
        case 'updateSCStatus':
            $result = updateSCStatus($dbgl,$data);
            echo json_encode($result);
            break;
        case 'updateAlasanRefuse':
            $result = updateAlasanRefuse($dbgl,$data);
            echo json_encode($result); 
            break;
        case 'saveMergerDisplan':
            $result = saveMergerDisplan($dbgl,$data);
            echo json_encode($result); 
            break;
        case 'savePisahDisplan':
            $result = savePisahDisplan($dbgl,$data);
            echo json_encode($result); 
            break; 
        case 'createTerlambat':
            $result = createTerlambat($dbgl,$data);
            echo json_encode($result); 
            break;
        default:
            # code...
            break;
    }

    function createTerlambat($dbgl,$data)
    {
        $username = $data->username;
        $terlambat = $data->terlambat;
        $alasan = $data->alasan;
        $now = date('Y-m-d H:i:s');

        $sql = "INSERT INTO t_dp_rejected_so (
                    type, cabang, tanggal, no, so_no, code, item_code, item_qty, item_qty2, keterangan, 
                    code_comp, item_ton, desa, sales_form_no, name, alamat_toko, 
                    dept_code, terlambat, alasan, insert_by, insert_time
                ) VALUES (
                    :type, :cabang, :tanggal, :no, :so_no, :code, :item_code, :item_qty, :item_qty2, :keterangan, 
                    :code_comp, :item_ton, :desa, :sales_form_no, :name, :alamat_toko, 
                    :dept_code, :terlambat, :alasan, :insert_by, :insert_time
                )";

        $stmt = $dbgl->prepare($sql);

        try {

            for($i=0; $i < count($data->dt_late); $i++) {
                $stmt->bindValue(':type', $data->dt_late[$i]->f2);
                $stmt->bindValue(':cabang', $data->dt_late[$i]->f3);
                $stmt->bindValue(':tanggal', $data->dt_late[$i]->f4);
                $stmt->bindValue(':no', $data->dt_late[$i]->f5);
                $stmt->bindValue(':so_no', $data->dt_late[$i]->f6);
                $stmt->bindValue(':code', $data->dt_late[$i]->f7);
                $stmt->bindValue(':item_code', $data->dt_late[$i]->f8);
                $stmt->bindValue(':item_qty', $data->dt_late[$i]->f9);
                $stmt->bindValue(':item_qty2', $data->dt_late[$i]->f10);
                $stmt->bindValue(':keterangan', $data->dt_late[$i]->f11);
                $stmt->bindValue(':code_comp', $data->dt_late[$i]->f12);
                $stmt->bindValue(':item_ton', $data->dt_late[$i]->f13);
                $stmt->bindValue(':desa', $data->dt_late[$i]->f14);
                $stmt->bindValue(':sales_form_no', $data->dt_late[$i]->f15);
                $stmt->bindValue(':name', $data->dt_late[$i]->f16);
                $stmt->bindValue(':alamat_toko', $data->dt_late[$i]->f17);
                $stmt->bindValue(':dept_code', $data->dt_late[$i]->f22);
                $stmt->bindValue(':terlambat', $terlambat); 
                $stmt->bindValue(':alasan', $alasan); 
                $stmt->bindValue(':insert_by', $username);
                $stmt->bindValue(':insert_time', $now); 
                $stmt->execute();
            }

            $response = [
                "success" => 1,
                "message" => "Data Terlambat Berhasil Disimpan"
            ];

        } catch (PDOException $err) {
            $response = [
                "success" => 0,
                "id" => $id,
                "message" => $err
            ];
        }

        return $response;
    }


    function getDataSOKirimNew2($dbgl,$data)
    {   
        $q = "SELECT a.code_comp, a.type, a.cabang, 
        a.tanggal, a.jaminput, a.code, 
        a.name, a.no, a.so_no, a.item_code, 
        a.item_qty, a.item_qty2, a.keterangan, 
        a.item_ton, a.desa, a.alamat_toko, 
        a.car_no, a.wh_code, d.address1 as alamat_promosi , 0 AS total_do, 
        a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type, a.sales_code,
        (SELECT wh_office FROM in_warehouse WHERE wh_code = a.kp_code) AS kp_office,
        CONCAT(d.name,'-',a.name) AS ledgername, a.pr_stat, to_char(a.delivery_time, 'YYYY-MM-DD ') AS delivery_time,
        CASE
            WHEN a.item_qty2 < a.item_qty THEN 'Outstanding'
            ELSE 'Langsung'
        END AS tipe_muat
        FROM v_displan a 
        LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp 
        WHERE a.cabang IN (".$data->where.")
        AND a.freight_type = 'kirim'
        AND a.code_comp = upper('saa')";

        $res = $dbgl->query($q)->fetchAll(PDO::FETCH_ASSOC);

        $arrayItem = [];
        foreach ($res as $key => $row) {

            $so = $row['so_no'].$row['no'].$row['code_comp'];
            // if ($row['type'] == 'SM') {
            //     $so = $row['no'];
            // }
            if (isset($arrayItem[$so])) {
                $arrayItem[$so]['item_code'] .= '['.$row['item_code'].' - '.$row['item_qty2'].']';
                $arrayItem[$so]['total_ton'] .= $row['item_ton'].',';
                unset($res[$key]);
            } else {
                $arrayItem[$so] = [
                    'item_code' => '['.$row['item_code'].' - '.$row['item_qty2'].']',
                    'total_ton' => $row['item_ton'].',',
                ];
            }
        }

        foreach ($res as $row) {
            $so = $row['so_no'].$row['no'].$row['code_comp'];
            // if ($row['type'] == 'SM') {
            //     $so = $row['no'];
            // }
            $ledgerName = $row['name'];
            if ($row['dept_code'] <> NULL || $row['dept_code'] <> '') {
                $ledgerName = $row['dept_name'].' - '.$ledgerName;
                $row['alamat_toko'] = $row['alamat_promosi'];
            }

            if($row['type'] == 'SO' && $row['ot_flag'] == 1) {
                continue;
            }

            if ($row['freight_type'] == 'ambil') {
                continue;
            }

            $hasil[] = array(
                'type' => $row['type'], 
                'code' => $row['code'],
                'ledgername' => $ledgerName, 
                'tanggal' => $row['tanggal'],
                'sm_no' => $row['type'] == 'SM' ? $row['no'] : '',
                'so_no' => $row['so_no'], 
                'item_code' => $arrayItem[$so]['item_code'], 
                'alamat_toko' => $row['alamat_toko'], 
                'keterangan' => $row['keterangan'], 
                'jaminput' => $row['jaminput'], 
                'code_comp' => $row['code_comp'],
                'sales_code' => $row['sales_code'],
                'cabang' => $row['cabang'],
                'freight_type' => $row['freight_type'],
                'dept_code' => $row['dept_code'],
                'type_so' => 'cabang_int',
                'kp_office' => $row['kp_office'],
                'pr_stat' => $row['pr_stat'], 
                'delivery_time' => $row['delivery_time'],
                'tipe_muat' => $row['tipe_muat'],
                // 'total_ton' => $arrayItem[$so]['total_ton']
            );
        }
        // echo count($hasil);

    
        $newHasil = [];
        foreach ($hasil as $value) {
            # code...
            $so_no = $value['so_no'];
            $qry = "SELECT a.pr_no, TRIM(a.item_code) as item_code, a.item_qty::integer, a.item_qty2::integer, b.item_weight, a.price_amt 
                    FROM po_req a 
                    -- LEFT JOIN t_dp_detail e ON a.code_comp::text = e.code_comp AND a.pr_no = e.no AND a.item_code = e.item_code::bpchar
                    JOIN t_card d ON a.vend_code = d.code AND a.code_comp = d.code_comp
                    JOIN t_item b ON a.item_code = b.item_code AND a.code_comp = b.code_comp
                    WHERE date(a.pr_date) >= date(now() - '120 days'::interval) 
                    AND a.pr_stat = 'A' 
                    AND btrim(a.pending_stat::text) <> 'F'
                    AND a.pr_type = 'S'::bpchar AND a.item_qty2 <> 0::numeric 
                    -- AND (e.id IS NULL OR e.status = '1'::bpchar) 
                    AND a.pr_no = '$so_no'";
            
            $rq = $dbgl->query($qry)->fetchAll(PDO::FETCH_ASSOC);

            $newHasil[] = array(
                'type' => $value['type'], 
                'code' => $value['code'],
                'ledgername' => $value['ledgername'], 
                'tanggal' => $value['tanggal'],
                'sm_no' => $value['sm_no'],
                'so_no' => $value['so_no'], 
                'item_code' => $value['item_code'], 
                'alamat_toko' => $value['alamat_toko'], 
                'keterangan' => $value['keterangan'], 
                'jaminput' => $value['jaminput'], 
                'code_comp' => $value['code_comp'],
                'sales_code' => $value['sales_code'],
                'cabang' => $value['cabang'],
                'freight_type' => $value['freight_type'],
                'dept_code' => $value['dept_code'],
                'type_so' => 'cabang_int',
                'kp_office' => $value['kp_office'],
                'pr_stat' => $value['pr_stat'], 
                'delivery_time' => $value['delivery_time'],
                'tipe_muat' => $value['tipe_muat'],
                'detail' => $rq,
            );
            $rq = [];
        }


        $resmore = [];
        $resless = [];
        foreach($newHasil as $val) {
            if($val['code_comp'] == 'SAA') {
                $tonase = getTonase($val['detail']);
            } else {
                $tonase = 0;
            }
            // $itemqty = getItemQty($val['detail']);

            if($tonase >= 10) {
                $resmore[] = array(
                    "type" => $val['type'],
                    "code" => $val['code'],
                    "ledgername" => $val['ledgername'],
                    "tanggal" => $val['tanggal'],
                    "sm_no" => $val['sm_no'],
                    "so_no" => $val['so_no'],
                    "alamat_toko" => $val['alamat_toko'],
                    "keterangan" => $val['keterangan'],
                    "jaminput" => $val['jaminput'],
                    "code_comp" => $val['code_comp'],
                    "sales_code" => $val['sales_code'],
                    "cabang" => $val['cabang'],
                    "freight_type" => $val['freight_type'],
                    "dept_code" => $val['dept_code'],
                    "type_so" => $val['type_so'],
                    "kp_office" => $val['kp_office'],
                    "pr_stat" => $val['pr_stat'],
                    "delivery_time" => $val['delivery_time'],
                    "tipe_muat" => $val['tipe_muat'],
                    "ton" => $tonase,
                    // "item_code" => $itemqty,
                    "item_code" => $val['item_code'],
                    "detail" => $val['detail']
                );
                $tonase = [];
            } else {
                $resless[] = array(
                    "type" => $val['type'],
                    "code" => $val['code'],
                    "ledgername" => $val['ledgername'],
                    "tanggal" => $val['tanggal'],
                    "sm_no" => $val['sm_no'],
                    "so_no" => $val['so_no'],
                    "alamat_toko" => $val['alamat_toko'],
                    "keterangan" => $val['keterangan'],
                    "jaminput" => $val['jaminput'],
                    "code_comp" => $val['code_comp'],
                    "sales_code" => $val['sales_code'],
                    "cabang" => $val['cabang'],
                    "freight_type" => $val['freight_type'],
                    "dept_code" => $val['dept_code'],
                    "type_so" => $val['type_so'],
                    "kp_office" => $val['kp_office'],
                    "pr_stat" => $val['pr_stat'],
                    "delivery_time" => $val['delivery_time'],
                    "tipe_muat" => $val['tipe_muat'],
                    "ton" => $tonase,
                    // "item_code" => $itemqty,
                    "item_code" => $val['item_code'],
                    "detail" => $new['detail']
                );
                $tonase = [];
            }
            // $itemqty = [];
        }

        $result = array(
            "more" => $resmore,
            "less" => $resless,
        );

        // print_r($result);

        return $result;
    }

    function saveMergerDisplan($dbgl,$data)
    {

        $noDp = generateNoDp($dbgl,$data);
        $total_muatan = 0;
        $total_biaya = 0;

        foreach ($data->dt_dp->items as $arr) {
            $total_muatan += $arr->total_muatan;
            $total_biaya += $arr->total_biaya;
        }

        if ($total_muatan > 10) {
            $xtotal_biaya = 0;
        } else {
            $xtotal_biaya = $total_biaya;
        }

        $id             = "";
        $office         = $data->office;
        $gudang         = $data->gudang;
        $tgl_perintah   = $data->tgl_perintah;
        $tgl_perintah_kirim = date('Y-m-d H:i:s', strtotime($tgl_perintah));
        $username       = $data->username;
        $now            = date('Y-m-d H:i:s');
        $loc            = $data->loc;
        $no_pol         = $data->no_pol;
        $sopir          = $data->sopir;

        $query = "INSERT INTO t_distribution_plan(no_dp, created_at, insert_by, freight_type, tgl_kirim, sc_status, total_muatan, total_biaya) 
                    VALUES (:no_dp, :created_at, :insert_by, :freight_type, :tgl_kirim, :sc_status, :total_muatan, :total_biaya) RETURNING id";
        $stmt = $dbgl->prepare($query);
        $dataheader = [
            ':no_dp' => $noDp,
            ':created_at' => $now,
            ':insert_by' => $username,
            ':freight_type' => "kirim",
            ':tgl_kirim' => $data->tgl_perintah, 
            // ':wh_code' => $gudang, 
            // ':no_pol' => $no_pol, 
            // ':sopir' => $sopir, 
            ':sc_status' => 1,
            ':total_muatan' => $total_muatan,
            ':total_biaya' => $xtotal_biaya
        ];

        try {
            $stmt->execute($dataheader);
            $stmt->bindColumn('id', $id);
            $stmt->fetch();
            $response = [
                'success' => 1,
                'response' => 'sukses',
                'id' => $id
            ];
        } catch (PDOException $e) {
            $response = [
                'success' => 0, 
                'response' => 'silahkan coba kembali!',
                'error' => $e
            ];
        }
        
        foreach ($data->dt_dp->items as $value) {
            foreach ($value->detail as $detail) {

                $lokasi_berangkat = "";
                $pengalihan = null;

                if ($data->office == $detail->f3) {
                    $lokasi_berangkat = $data->office;
                    $pengalihan = null;
                } else {
                    $lokasi_berangkat = $data->office;
                    $pengalihan = $detail->f3;
                }


                $sql = "INSERT INTO 
                            t_dp_detail(type,
                            cabang,
                            tanggal,
                            no,
                            so_no,
                            code,
                            item_code,
                            item_qty,
                            item_qty2,
                            keterangan,
                            code_comp,
                            item_ton,
                            desa,
                            sales_form_no,
                            name,
                            alamat_toko,
                            insert_time,
                            insert_by,
                            delivery_date,
                            status,
                            dept_code,
                            dp_id,
                            pengalihan) 
                        VALUES 
                            (:type,
                            :cabang,
                            :tanggal,
                            :no,
                            :so_no,
                            :code,
                            :item_code,
                            :item_qty,
                            :item_qty2,
                            :keterangan,
                            :code_comp,
                            :item_ton,
                            :desa,
                            :sales_form_no,
                            :name,
                            :alamat_toko,
                            :insert_time,
                            :insert_by,
                            :delivery_date,
                            :status,
                            :dept_code,
                            :dp_id,
                            :pengalihan)";

                $stmt = $dbgl->prepare($sql);

                $dataheader2 = [
                    ':type' => $detail->f2,
                    ':cabang' => $lokasi_berangkat,
                    ':tanggal' => $detail->f4,
                    ':no' => $detail->f5,
                    ':so_no' => $detail->f6,
                    ':code' => $detail->f7,
                    ':item_code' => trim($detail->f8,"[]"),
                    ':item_qty' => $detail->f9,
                    ':item_qty2' => $detail->f10,
                    ':keterangan' => $detail->f11,
                    ':code_comp' => $detail->f12,
                    ':item_ton' => $detail->f13,
                    ':desa' => $detail->f14,
                    ':sales_form_no' => $detail->f15,
                    ':name' => $detail->f16,
                    ':alamat_toko' => $detail->f17,
                    ':insert_time' => $now,
                    ':insert_by' => $username,
                    ':delivery_date' => $tgl_perintah_kirim,
                    ':status' => '0',
                    ':dept_code' => $detail->f22,
                    ':dp_id' => $id,
                    ':pengalihan' => $pengalihan,
                ];

                try {
                    $stmt->execute($dataheader2);
                    $dataheader2 = [];
                    $response = [
                        'success' => 1,
                        'response' => 'sukses detail',
                        'id' => $id
                    ];
                } catch (PDOException $e) {
                    $response = [
                        'success' => 0, 
                        'response' => 'silahkan coba kembali! (detail)',
                        'error' => $e
                    ];
                }
            }

            $now = date('Y-m-d H:i:s');
            $id_dp = $value->id;

            try {

                $sql2 = "DELETE FROM t_distribution_plan WHERE id = '$id_dp'";
            
                $stmt2 = $dbgl->prepare($sql2);
                $stmt2->execute();
    
            } catch (PDOException $e) {
                $e->getMessage();
            }
        }

        return $response;
    }

    function savePisahDisplan($dbgl,$data)
    {

        $noDp = generateNoDp($dbgl,$data);
        $total_muatan = 0;
        $total_biaya = 0;

        $id             = "";
        $tgl_perintah   = $data->tgl_kirim;
        $username       = $data->username;
        $now            = date('Y-m-d H:i:s');
        $office         = $data->office;
        $username       = $data->username;


        $query = "INSERT INTO t_distribution_plan(no_dp, created_at, insert_by, freight_type, tgl_kirim, sc_status) 
                    VALUES (:no_dp, :created_at, :insert_by, :freight_type, :tgl_kirim, :sc_status) RETURNING id";
        $stmt = $dbgl->prepare($query);
        $dataheader = [
            ':no_dp' => $noDp,
            ':created_at' => $now,
            ':insert_by' => $username,
            ':freight_type' => "kirim",
            ':tgl_kirim' => $tgl_perintah, 
            ':sc_status' => 1
        ];

        try {
            $stmt->execute($dataheader);
            $stmt->bindColumn('id', $id);
            $stmt->fetch();
            $response = [
                'success' => 1,
                'response' => 'sukses',
                'id' => $id
            ];
        } catch (PDOException $e) {
            $response = [
                'success' => 0, 
                'response' => 'silahkan coba kembali!',
                'error' => $e
            ];
        }
        
        foreach ($data->checked_dp as $detail) {

            $lokasi_berangkat = "";
            $pengalihan = null;

            if ($detail->f24 == null) {
                if ($data->office == $detail->f3) {
                    $lokasi_berangkat = $data->office;
                    $pengalihan = null;
                } else {
                    $lokasi_berangkat = $data->office;
                    $pengalihan = $detail->f3;
                }
            } else {
                if ($data->office == $detail->f24) {
                    $lokasi_berangkat = $data->office;
                    $pengalihan = null;
                } else {
                    $lokasi_berangkat = $data->office;
                    $pengalihan = $detail->f24;
                }
            }

            $sql = "INSERT INTO 
                        t_dp_detail(type,
                        cabang,
                        tanggal,
                        no,
                        so_no,
                        code,
                        item_code,
                        item_qty,
                        item_qty2,
                        keterangan,
                        code_comp,
                        item_ton,
                        desa,
                        sales_form_no,
                        name,
                        alamat_toko,
                        insert_time,
                        insert_by,
                        status,
                        dept_code,
                        dp_id,
                        pengalihan) 
                    VALUES 
                        (:type,
                        :cabang,
                        :tanggal,
                        :no,
                        :so_no,
                        :code,
                        :item_code,
                        :item_qty,
                        :item_qty2,
                        :keterangan,
                        :code_comp,
                        :item_ton,
                        :desa,
                        :sales_form_no,
                        :name,
                        :alamat_toko,
                        :insert_time,
                        :insert_by,
                        :status,
                        :dept_code,
                        :dp_id,
                        :pengalihan)";

            $stmt = $dbgl->prepare($sql);

            $dataheader2 = [
                ':type' => $detail->f2,
                ':cabang' => $lokasi_berangkat,
                ':tanggal' => $detail->f4,
                ':no' => $detail->f5,
                ':so_no' => $detail->f6,
                ':code' => $detail->f7,
                ':item_code' => $detail->f8,
                ':item_qty' => $detail->f9,
                ':item_qty2' => $detail->f10,
                ':keterangan' => $detail->f11,
                ':code_comp' => $detail->f12,
                ':item_ton' => $detail->f13,
                ':desa' => $detail->f14,
                ':sales_form_no' => $detail->f15,
                ':name' => $detail->f16,
                ':alamat_toko' => $detail->f17,
                ':insert_time' => $now,
                ':insert_by' => $username,
                ':status' => '0',
                ':dept_code' => $detail->f22,
                ':dp_id' => $id,
                ':pengalihan' => $pengalihan,
            ];

            try {
                $stmt->execute($dataheader2);
                $dataheader2 = [];
                $response = [
                    'success' => 1,
                    'response' => 'sukses detail',
                    'id' => $id
                ];
            } catch (PDOException $e) {
                $response = [
                    'success' => 0, 
                    'response' => 'silahkan coba kembali! (detail)',
                    'error' => $e
                ];
            }
        }

        $id = $data->data_all->id;

        try {

            $sql2 = "DELETE FROM t_distribution_plan WHERE id = '$id'";
        
            $stmt2 = $dbgl->prepare($sql2);
            $stmt2->execute();

        } catch (PDOException $e) {
            $e->getMessage();
        }

        return $response;
    }

    function updateAlasanRefuse($dbgl,$data)
    {
        $now = date('Y-m-d');

        try {

            $sql = "INSERT INTO 
                        t_dp_sc_detail(sc_status, sc_alasan, so_no, ledger, name, rejected_by, rejected_date) 
                    VALUES 
                        (0, '$data->sc_alasan', '$data->so_no', '$data->ledger', '$data->name', '$data->username', '$now')";
        
            $stmt = $dbgl->prepare($sql);
            $stmt->execute();

            $respon = [
                "success" => 1,
                "message" => true,
                "pesan" => "Data Berhasil Diinsert"
            ];

        } catch (PDOException $e) {
            $e->getMessage();
            $respon = [
                "success" => 0,
                "message" => false,
                "pesan" => $e
            ];
        }

        return $respon;
    }

    function updateSCStatus($dbgl,$data)
    {
        try {

            $sql = "UPDATE t_distribution_plan SET sc_status = 1
                    WHERE id = '$data->id'";
        
            $stmt = $dbgl->prepare($sql);
            $stmt->execute();

            $respon = [
                "success" => 1,
                "message" => true,
                "pesan" => "Data DP Berhasil Diupdate"
            ];

        } catch (PDOException $e) {
            $e->getMessage();
            $respon = [
                "success" => 0,
                "message" => false,
                "pesan" => $e
            ];
        }

        return $respon;
    }

    function updateCatatanTo($dbgl,$data)
    {
        try {

            $sql = "UPDATE t_distribution_plan SET 
                    catatan_to = '$data->catatan_to' 
                    -- tgl_terima = '$data->tgl_terima'
                    WHERE id = '$data->id'";
        
            $stmt = $dbgl->prepare($sql);
            $stmt->execute();

            $respon = [
                "success" => 1,
                "message" => true,
                "pesan" => "Data DP Berhasil Diupdate"
            ];

        } catch (PDOException $e) {
            $e->getMessage();
            $respon = [
                "success" => 0,
                "message" => false,
                "pesan" => $e
            ];
        }

        return $respon;
    }

    function getSopirByNoto($dbgl,$data)
    {

        $sql = "SELECT * FROM in_trans WHERE ref_no IN ('$data->no_to') ORDER BY voucher_date DESC LIMIT 1";

        try {
            $result = $dbgl->query($sql)->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            $result = $e;    
        }

        return $result;
    }
    
    function generateNoDp($dbgl,$data) 
    {
        $dpPeriode = "DP/".date('ym');
        $sql = "SELECT no_dp FROM t_distribution_plan WHERE no_dp LIKE '".$dpPeriode."%' ORDER BY id DESC LIMIT 1";
        $noDp = '';
        try {
            $stmt = $dbgl->query($sql);
            $result = $stmt->fetchColumn();
            if ($result === false) {
                $noDp = $dpPeriode.'/000001';
            } else {
                $noDp = (int) substr($result, 8);
                $noDp = $noDp + 1;
                $noDp = $dpPeriode."/".sprintf("%06s", $noDp);
            }

        } catch (PDOException $e) {
            $noDp = $e;    
        }

        return $noDp;
    }

    function getSOKirim2($dbgl,$data) {
        global $username;
        $rows = [];
        $list_coce = listCostCenter($dbgl, $username);
        $list_coce = "'".implode("', '", $list_coce)."'";
        $where = " a.cabang IN (".$list_coce.")";
        // $where = " a.cabang IN ('GYR')";
        if ($location == 'TBN') {
            $where.= " AND a.tanggal >= '2020-01-06'";
        }
        
        if ($location == 'LTM') {
            $where .= " AND a.tanggal >= '2020-11-24'";
        }
        
        if ($location == 'BPN') {
            $where.= " AND a.tanggal >= '2020-02-24'";
        }
        $where.= " AND a.ot_flag NOT IN ('1') AND a.freight_type NOT IN ('ambil')";
        
        $query = "SELECT 
                a.code_comp, a.type, a.cabang, a.tanggal, a.jaminput, a.code, a.name, a.no, a.so_no, a.item_code, a.item_qty, a.item_qty2, 
                a.keterangan, a.item_ton, a.desa, a.alamat_toko, a.car_no, a.wh_code, d.address1 as alamat_promosi
                , 0 AS total_do, a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type
            FROM v_displan2 a
            LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp
            WHERE ".$where." ORDER BY a.code, a.tanggal, a.no 
        ";
        try {
            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $newItem = [];
            $arrayItem = [];
            foreach ($rows as $key => $row) {
                $so = $row['so_no'].$row['no'].$row['code_comp'];
                // if ($row['type'] == 'SM') {
                //     $so = $row['no'];
                // }
                if (isset($arrayItem[$so])) {
                    $arrayItem[$so]['item_code'] .= '<br> ['.$row['item_code'].' - '.$row['item_qty2'].']';
                    unset($rows[$key]);
                } else {
                    $arrayItem[$so] = [
                        'item_code' => '['.$row['item_code'].' - '.$row['item_qty2'].']',
                    ];
                }
            }
            foreach ($rows as $rslt)
            {
                $so = $rslt['so_no'].$rslt['no'].$rslt['code_comp'];
                // if ($rslt['type'] == 'SM') {
                //     $so = $rslt['no'];
                // }
                $ledgerName = $rslt['name'];
                if ($rslt['dept_code'] <> NULL || $rslt['dept_code'] <> '') {
                    $ledgerName = $rslt['dept_name'].' - '.$ledgerName;
                    $rslt['alamat_toko'] = $rslt['alamat_promosi'];
                }

                // if($rslt['type'] == 'SO' && $rslt['ot_flag'] !== '1') {
                //     // continue;
                // }
                $newArr[] = array(
                    'type' => $rslt['type'], 
                    'code' => $rslt['code'],
                    'ledgername' => $ledgerName, 
                    'tanggal' => $rslt['tanggal'],
                    'sm_no' => $rslt['type'] == 'SM' ? $rslt['no'] : '',
                    'so_no' => $rslt['so_no'], 
                    'item_code' => $arrayItem[$so]['item_code'], 
                    'alamat_toko' => $rslt['alamat_toko'], 
                    'keterangan' => $rslt['keterangan'], 
                    'jaminput' => $rslt['jaminput'], 
                    'code_comp' => $rslt['code_comp'],
                );

                // if ($rslt['freight_type'] == 'ambil') {
                //     continue;
                // }

            }
        } catch(PDOException $e) {
            echo $e;
        }
        return $newArr;
    }

    function getSOKirim($dbgl,$data) {

        $all_kirim = [];
        $hasil_all = [];

        try {

            // $query = "SELECT a.code_comp, a.type, a.cabang, 
            //                 a.tanggal, a.jaminput, a.code, 
            //                 a.name, a.no, a.so_no, a.item_code, 
            //                 a.item_qty, a.item_qty2, a.keterangan, 
            //                 a.item_ton, a.desa, a.alamat_toko, 
            //                 a.car_no, a.wh_code, d.address1 as alamat_promosi , 0 AS total_do, 
            //                 a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type, a.sales_code,
            //                 (SELECT wh_office FROM in_warehouse WHERE wh_code = a.kp_code) AS kp_office,
            //                 CONCAT(d.name,'-',a.name) AS ledgername, a.pr_stat, to_char(a.delivery_time, 'YYYY-MM-DD ') AS delivery_time
            //         FROM v_displan a 
            //         LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp 
            //         WHERE a.cabang IN (".$data->where.") 
            //         AND a.freight_type = 'kirim'
            //         AND a.so_no NOT IN 
            //             (SELECT so_no FROM t_distribution_plan_list a 
            //             LEFT JOIN t_distribution_plan_item b ON b.id = a.dp_id
            //             WHERE status_so = '1' OR status_so = '0')
            //         AND a.so_no NOT IN 
            //             (SELECT so_no FROM t_distribution_plan_pengalihan a 
            //             LEFT JOIN t_distribution_plan_item b ON b.id = a.id_pengalihan
            //             WHERE status_so = '0' OR status_so = '1')
            //         AND a.item_ton >= 8
            //         ORDER BY a.code, a.tanggal, a.no";

            // AND a.no NOT IN
            // (SELECT C.no
            // FROM PO_REQ A
            // INNER JOIN T_CARD B ON A.vend_code = B.code AND A.code_comp = B.code_comp
            // JOIN sales_memo C ON C.so_no = A.pr_no AND C.item_code::bpchar = A.item_code AND upper(C.kategori::text) = A.code_comp::text
            // WHERE A.comp_code = 'SAKA00' 
            // AND A.code_comp = UPPER('saa')
            // AND A.spk_no IN (".$data->where.")
            // AND A.pr_type = 'S' 
            // AND (A.pr_stat = 'A' OR A.pr_stat = 'F'))

            // AND a.so_no NOT IN 
            // (SELECT so_no FROM t_distribution_plan_list a 
            // LEFT JOIN t_distribution_plan_item b ON b.id = a.dp_id
            // WHERE a.status = '1' OR a.status = '2' )

            $query1 = "SELECT a.code_comp, a.type, a.cabang, 
                            cast(a.tanggal as varchar), a.jaminput, a.code, 
                            a.name, a.no, a.so_no, a.item_code, 
                            cast(a.item_qty as varchar), cast(a.item_qty2 as varchar), a.keterangan, 
                            cast(a.item_ton as varchar), a.desa, a.alamat_toko, 
                            a.car_no, a.wh_code, d.address1 as alamat_promosi , cast(0 as varchar) AS total_do, 
                            a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type, a.sales_code,
                            (SELECT wh_office FROM in_warehouse WHERE wh_code = a.kp_code) AS kp_office,
                            CONCAT(d.name,'-',a.name) AS ledgername, a.pr_stat, to_char(a.delivery_time, 'YYYY-MM-DD ') AS delivery_time,
                            CASE
                                WHEN a.item_qty2 < a.item_qty THEN 'Outstanding'
                                ELSE 'Langsung'
                            END AS tipe_muat,
                            SUM(A.item_ton) OVER (ORDER BY A.so_no ASC) AS total_ton
                        FROM v_displan a 
                        LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp 
                        WHERE a.cabang IN (".$data->where.")
                        AND a.freight_type = 'kirim'";

            $query2 = "SELECT * FROM (SELECT cast(a.code_comp as varchar), cast(a.type as varchar), cast(a.cabang as varchar) , 
                                        cast(a.tanggal as varchar), cast(a.jaminput as text), cast(a.code as varchar), 
                                        cast(a.name as varchar), cast(a.no as varchar), cast(a.so_no as varchar), cast(a.item_code as varchar), 
                                        cast(a.item_qty as varchar), cast(a.item_qty2 as varchar), cast(a.keterangan as varchar), 
                                        cast(a.item_ton as varchar), cast(a.desa as varchar), cast(a.alamat_toko as varchar) , 
                                        cast(a.car_no as varchar) , cast(a.wh_code as varchar) , cast(a.alamat_promosi as varchar), cast(a.total_do as varchar), 
                                        cast(a.dept_code as varchar), cast(a.dept_name as varchar), cast(a.ot_flag as varchar) , cast(a.freight_type as varchar) , cast(a.sales_code as varchar),
                                        cast(a.kp_office as varchar) , 
                                        cast(a.ledgername as varchar) , cast(a.pr_stat as varchar), cast(a.delivery_time as varchar),
                                        'Gandengan' AS tipe_muat FROM t_distribution_plan_temp2 a
                                        WHERE a.sc_status = '1'
                                        AND a.asal_gudang IN (".$data->where.") 
                                        AND a.so_no NOT IN 
                                            (SELECT a.so_no FROM t_distribution_plan_list a 
                                            LEFT JOIN t_distribution_plan_item b ON b.id = a.dp_id
                                            WHERE b.status_so = '1'
                                            OR b.status_so = '0')
                                        AND a.so_no NOT IN 
                                            (SELECT so_no FROM t_distribution_plan_pengalihan a 
                                            LEFT JOIN t_distribution_plan_item b ON b.id = a.id_pengalihan
                                            WHERE status_so = '0' OR status_so = '1')) V_temp2";
            
            // echo $query;
            // AND a.so_no NOT IN (SELECT so_no FROM t_distribution_plan_list)
            // AND a.so_no NOT IN (SELECT so_no FROM t_distribution_plan_pengalihan)

            // $rows1 = $dbgl->query($query1)->fetchAll(PDO::FETCH_ASSOC);
            // $rows2 = $dbgl->query($query2)->fetchAll(PDO::FETCH_ASSOC);

            $rows = $dbgl->query($query1)->fetchAll(PDO::FETCH_ASSOC);

            $arrayItem = [];
            foreach ($rows as $key => $row) {

                $so = $row['so_no'].$row['no'].$row['code_comp'];
                // if ($row['type'] == 'SM') {
                //     $so = $row['no'];
                // }
                if (isset($arrayItem[$so])) {
                    $arrayItem[$so]['item_code'] .= '['.$row['item_code'].' - '.$row['item_qty2'].']';
                    unset($rows[$key]);
                } else {
                    $arrayItem[$so] = [
                        'item_code' => '['.$row['item_code'].' - '.$row['item_qty2'].']',
                    ];
                }
            }

            foreach ($rows as $row) {
                $so = $row['so_no'].$row['no'].$row['code_comp'];
                // if ($row['type'] == 'SM') {
                //     $so = $row['no'];
                // }
                $ledgerName = $row['name'];
                if ($row['dept_code'] <> NULL || $row['dept_code'] <> '') {
                    $ledgerName = $row['dept_name'].' - '.$ledgerName;
                    $row['alamat_toko'] = $row['alamat_promosi'];
                }

                if($row['type'] == 'SO' && $row['ot_flag'] == 1) {
                    continue;
                }

                if ($row['freight_type'] == 'ambil') {
                    continue;
                }

                $hasil[] = array(
                    'type' => $row['type'], 
                    'code' => $row['code'],
                    'ledgername' => $ledgerName, 
                    'tanggal' => $row['tanggal'],
                    'sm_no' => $row['type'] == 'SM' ? $row['no'] : '',
                    'so_no' => $row['so_no'], 
                    'item_code' => $arrayItem[$so]['item_code'], 
                    'alamat_toko' => $row['alamat_toko'], 
                    'keterangan' => $row['keterangan'], 
                    'jaminput' => $row['jaminput'], 
                    'code_comp' => $row['code_comp'],
                    'sales_code' => $row['sales_code'],
                    'cabang' => $row['cabang'],
                    'freight_type' => $row['freight_type'],
                    'dept_code' => $row['dept_code'],
                    'type_so' => 'cabang_int',
                    'kp_office' => $row['kp_office'],
                    'pr_stat' => $row['pr_stat'], 
                    'delivery_time' => $row['delivery_time'],
                    'tipe_muat' => $row['tipe_muat']
                );
            }

        } catch (PDOException $e) {
            $e->getMessage();
            $hasil = $e; 
        }

        return $hasil;
    }

    function getDataSOKirim($dbgl,$data)
    {   
        $all_kirim = [];
        $hasil_all = [];

        $end_date = date('Y-m-d');
        $start_date = date('Y-m-d', strtotime('-7 days'));

        $rows = [];
        $rows1 = [];
        $rows2 = [];
        
        try {

            // $query = "SELECT a.code_comp, a.type, a.cabang, 
            //                 a.tanggal, a.jaminput, a.code, 
            //                 a.name, a.no, a.so_no, a.item_code, 
            //                 a.item_qty, a.item_qty2, a.keterangan, 
            //                 a.item_ton, a.desa, a.alamat_toko, 
            //                 a.car_no, a.wh_code, d.address1 as alamat_promosi , 0 AS total_do, 
            //                 a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type, a.sales_code,
            //                 (SELECT wh_office FROM in_warehouse WHERE wh_code = a.kp_code) AS kp_office,
            //                 CONCAT(d.name,'-',a.name) AS ledgername, a.pr_stat, to_char(a.delivery_time, 'YYYY-MM-DD ') AS delivery_time
            //         FROM v_displan a 
            //         LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp 
            //         WHERE a.cabang IN (".$data->where.") 
            //         AND a.freight_type = 'kirim'
            //         AND a.so_no NOT IN 
            //             (SELECT so_no FROM t_distribution_plan_list a 
            //             LEFT JOIN t_distribution_plan_item b ON b.id = a.dp_id
            //             WHERE status_so = '1' OR status_so = '0')
            //         AND a.so_no NOT IN 
            //             (SELECT so_no FROM t_distribution_plan_pengalihan a 
            //             LEFT JOIN t_distribution_plan_item b ON b.id = a.id_pengalihan
            //             WHERE status_so = '0' OR status_so = '1')
            //         AND a.item_ton >= 8
            //         ORDER BY a.code, a.tanggal, a.no";

            // AND a.no NOT IN
            // (SELECT C.no
            // FROM PO_REQ A
            // INNER JOIN T_CARD B ON A.vend_code = B.code AND A.code_comp = B.code_comp
            // JOIN sales_memo C ON C.so_no = A.pr_no AND C.item_code::bpchar = A.item_code AND upper(C.kategori::text) = A.code_comp::text
            // WHERE A.comp_code = 'SAKA00' 
            // AND A.code_comp = UPPER('saa')
            // AND A.spk_no IN (".$data->where.")
            // AND A.pr_type = 'S' 
            // AND (A.pr_stat = 'A' OR A.pr_stat = 'F'))

            // AND a.so_no NOT IN 
            // (SELECT so_no FROM t_distribution_plan_list a 
            // LEFT JOIN t_distribution_plan_item b ON b.id = a.dp_id
            // WHERE a.status = '1' OR a.status = '2' )

            $query1 = "SELECT a.code_comp, a.type, a.cabang, 
                            a.tanggal, a.jaminput, a.code, 
                            a.name, a.no, a.so_no, a.item_code, 
                            a.item_qty, a.item_qty2, a.keterangan, 
                            a.item_ton, a.desa, a.alamat_toko, 
                            a.car_no, a.wh_code, d.address1 as alamat_promosi , 0 AS total_do, 
                            a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type, a.sales_code,
                            (SELECT wh_office FROM in_warehouse WHERE wh_code = a.kp_code) AS kp_office,
                            CONCAT(d.name,'-',a.name) AS ledgername, a.pr_stat, to_char(a.delivery_time, 'YYYY-MM-DD ') AS delivery_time,
                            CASE
                                WHEN a.item_qty2 < a.item_qty THEN 'Outstanding'
                                ELSE 'Langsung'
                            END AS tipe_muat
                        FROM v_displan a 
                        LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp 
                        WHERE a.cabang IN (".$data->where.")
                        AND a.freight_type = 'kirim'
                        AND a.code_comp = upper('saa')";

            // $query2 = "SELECT * FROM (SELECT cast(a.code_comp as varchar), cast(a.type as varchar), cast(a.cabang as varchar) , 
            //                             cast(a.tanggal as varchar), cast(a.jaminput as text), cast(a.code as varchar), 
            //                             cast(a.name as varchar), cast(a.no as varchar), cast(a.so_no as varchar), cast(a.item_code as varchar), 
            //                             cast(a.item_qty as varchar), cast(a.item_qty2 as varchar), cast(a.keterangan as varchar), 
            //                             cast(a.item_ton as varchar), cast(a.desa as varchar), cast(a.alamat_toko as varchar) , 
            //                             cast(a.car_no as varchar) , cast(a.wh_code as varchar) , cast(a.alamat_promosi as varchar), cast(a.total_do as varchar), 
            //                             cast(a.dept_code as varchar), cast(a.dept_name as varchar), cast(a.ot_flag as varchar) , cast(a.freight_type as varchar) , cast(a.sales_code as varchar),
            //                             cast(a.kp_office as varchar) , 
            //                             cast(a.ledgername as varchar) , cast(a.pr_stat as varchar), cast(a.delivery_time as varchar),
            //                             'Gandengan' AS tipe_muat FROM t_distribution_plan_temp2 a
            //                             WHERE a.sc_status = '1'
            //                             AND a.asal_gudang IN (".$data->where.")) V_temp2";
            
            // echo $query;
            // AND a.so_no NOT IN (SELECT so_no FROM t_distribution_plan_list)
            // AND a.so_no NOT IN (SELECT so_no FROM t_distribution_plan_pengalihan)

            // $rows1 = $dbgl->query($query1)->fetchAll(PDO::FETCH_ASSOC);
            // $rows2 = $dbgl->query($query2)->fetchAll(PDO::FETCH_ASSOC);

            // $rows = array_merge($rows1, $rows2);
            $rows = $dbgl->query($query1)->fetchAll(PDO::FETCH_ASSOC);

            $arrayItem = [];
            foreach ($rows as $key => $row) {

                $so = $row['so_no'].$row['no'].$row['code_comp'];
                // if ($row['type'] == 'SM') {
                //     $so = $row['no'];
                // }
                if (isset($arrayItem[$so])) {
                    $arrayItem[$so]['item_code'] .= '['.$row['item_code'].' - '.$row['item_qty2'].']';
                    $arrayItem[$so]['total_ton'] .= $row['item_ton'].',';
                    unset($rows[$key]);
                } else {
                    $arrayItem[$so] = [
                        'item_code' => '['.$row['item_code'].' - '.$row['item_qty2'].']',
                        'total_ton' => $row['item_ton'].',',
                    ];
                }
            }

            foreach ($rows as $row) {
                $so = $row['so_no'].$row['no'].$row['code_comp'];
                // if ($row['type'] == 'SM') {
                //     $so = $row['no'];
                // }
                $ledgerName = $row['name'];
                if ($row['dept_code'] <> NULL || $row['dept_code'] <> '') {
                    $ledgerName = $row['dept_name'].' - '.$ledgerName;
                    $row['alamat_toko'] = $row['alamat_promosi'];
                }

                if($row['type'] == 'SO' && $row['ot_flag'] == 1) {
                    continue;
                }

                if ($row['freight_type'] == 'ambil') {
                    continue;
                }

                $hasil[] = array(
                    'type' => $row['type'], 
                    'code' => $row['code'],
                    'ledgername' => $ledgerName, 
                    'tanggal' => $row['tanggal'],
                    'sm_no' => $row['type'] == 'SM' ? $row['no'] : '',
                    'so_no' => $row['so_no'], 
                    'item_code' => $arrayItem[$so]['item_code'], 
                    'alamat_toko' => $row['alamat_toko'], 
                    'keterangan' => $row['keterangan'], 
                    'jaminput' => $row['jaminput'], 
                    'code_comp' => $row['code_comp'],
                    'sales_code' => $row['sales_code'],
                    'cabang' => $row['cabang'],
                    'freight_type' => $row['freight_type'],
                    'dept_code' => $row['dept_code'],
                    'type_so' => 'cabang_int',
                    'kp_office' => $row['kp_office'],
                    'pr_stat' => $row['pr_stat'], 
                    'delivery_time' => $row['delivery_time'],
                    'tipe_muat' => $row['tipe_muat'],
                    'total_ton' => $arrayItem[$so]['total_ton']
                );
            }

        } catch (PDOException $e) {
            $e->getMessage();
            $hasil = $e; 
        }

        // $hasilKirim2 = getDataSOKirimPengalihan($dbgl,$data);

        // $all_kirim = array_merge($hasilKirim2,$hasil);

        return $hasil;
    }

    function getDataSOKirimNew($dbgl,$data)
    {   
        $q = "SELECT a.code_comp, a.type, a.cabang, 
        a.tanggal, a.jaminput, a.code, 
        a.name, a.no, a.so_no, a.item_code, 
        a.item_qty, a.item_qty2, a.keterangan, 
        a.item_ton, a.desa, a.alamat_toko, 
        a.car_no, a.wh_code, d.address1 as alamat_promosi , 0 AS total_do, 
        a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type, a.sales_code,
        (SELECT wh_office FROM in_warehouse WHERE wh_code = a.kp_code) AS kp_office,
        CONCAT(d.name,'-',a.name) AS ledgername, a.pr_stat, to_char(a.delivery_time, 'YYYY-MM-DD ') AS delivery_time,
        CASE
            WHEN a.item_qty2 < a.item_qty THEN 'Outstanding'
            ELSE 'Langsung'
        END AS tipe_muat
        FROM v_displan a 
        LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp 
        WHERE a.cabang IN (".$data->where.")
        AND a.freight_type = 'kirim'";

        $res = $dbgl->query($q)->fetchAll(PDO::FETCH_ASSOC);

        $arrayItem = [];
        foreach ($res as $key => $row) {

            $so = $row['so_no'].$row['no'].$row['code_comp'];
            // if ($row['type'] == 'SM') {
            //     $so = $row['no'];
            // }
            if (isset($arrayItem[$so])) {
                $arrayItem[$so]['item_code'] .= '['.$row['item_code'].' - '.$row['item_qty2'].']';
                $arrayItem[$so]['total_ton'] .= $row['item_ton'].',';
                unset($res[$key]);
            } else {
                $arrayItem[$so] = [
                    'item_code' => '['.$row['item_code'].' - '.$row['item_qty2'].']',
                    'total_ton' => $row['item_ton'].',',
                ];
            }
        }

        foreach ($res as $row) {
            $so = $row['so_no'].$row['no'].$row['code_comp'];
            // if ($row['type'] == 'SM') {
            //     $so = $row['no'];
            // }
            $ledgerName = $row['name'];
            if ($row['dept_code'] <> NULL || $row['dept_code'] <> '') {
                $ledgerName = $row['dept_name'].' - '.$ledgerName;
                $row['alamat_toko'] = $row['alamat_promosi'];
            }

            if($row['type'] == 'SO' && $row['ot_flag'] == 1) {
                continue;
            }

            if ($row['freight_type'] == 'ambil') {
                continue;
            }

            $hasil[] = array(
                'type' => $row['type'], 
                'code' => $row['code'],
                'ledgername' => $ledgerName, 
                'tanggal' => $row['tanggal'],
                'sm_no' => $row['type'] == 'SM' ? $row['no'] : '',
                'so_no' => $row['so_no'], 
                'item_code' => $arrayItem[$so]['item_code'], 
                'alamat_toko' => $row['alamat_toko'], 
                'keterangan' => $row['keterangan'], 
                'jaminput' => $row['jaminput'], 
                'code_comp' => $row['code_comp'],
                'sales_code' => $row['sales_code'],
                'cabang' => $row['cabang'],
                'freight_type' => $row['freight_type'],
                'dept_code' => $row['dept_code'],
                'type_so' => 'cabang_int',
                'kp_office' => $row['kp_office'],
                'pr_stat' => $row['pr_stat'], 
                'delivery_time' => $row['delivery_time'],
                'tipe_muat' => $row['tipe_muat'],
                // 'total_ton' => $arrayItem[$so]['total_ton']
            );
        }
        // echo count($hasil);

    
        $newHasil = [];
        foreach ($hasil as $value) {
            # code...
            $so_no = $value['so_no'];
            $sm_no = $value['sm_no'];

            if ($sm_no != '') {
                $qry = "SELECT DISTINCT 'SM'::text AS type,
                            a.cabang,
                            c.kp_code,
                            a.order_date AS tanggal,
                            to_char(a.insert_date, 'HH24:MI:SS'::text) AS jaminput,
                            a.no,
                            a.so_no,
                            a.ledger_code AS code,
                            a.item_code,
                            c.item_qty::double precision AS item_qty,
                            a.item_qty::double precision AS item_qty2,
                            upper(a.keterangan::text) AS keterangan,
                            upper(a.kategori::text) AS code_comp,
                            (a.item_qty * b.item_weight / 1000000::numeric)::double precision AS item_ton,
                            a.desa,
                            c.ref_no AS sales_form_no,
                            d.name,
                            d.address1 AS alamat_toko,
                            d.sales_code,
                            ''::text AS car_no,
                            ''::text AS wh_code,
                            ''::text AS dept_code,
                            a.ot_flag,
                            a.freight_type,
                            c.pr_stat,
                            c.delivery_time,
                            c.close_by,
                            c.close_date,
                            c.item_qty_ton,
                            b.item_weight
                            FROM sales_memo a
                                LEFT JOIN t_dp_detail e ON upper(a.kategori::text) = e.code_comp AND a.no = e.no AND a.so_no = e.so_no
                                JOIN po_req c ON a.so_no = c.pr_no AND a.item_code::bpchar = c.item_code AND upper(a.kategori::text) = c.code_comp::text
                                JOIN t_card d ON a.ledger_code::bpchar = d.code AND upper(a.kategori::text) = d.code_comp::text
                                JOIN t_item b ON a.item_code::bpchar = b.item_code AND upper(a.kategori::text) = b.code_comp::text
                            WHERE a.so_no IS NOT NULL AND a.status = '1'::bpchar AND e.id IS NULL AND date(c.pr_date) >= date(now() - '1000 days'::interval) AND c.pr_stat = 'A'::bpchar AND btrim(c.pending_stat::text) <> 'F'::text AND a.item_qty <= c.item_qty2 AND c.item_qty2 <> 0::numeric AND a.do_no IS NULL
                            AND a.no = '$sm_no'";
            } else {
                $qry = "SELECT DISTINCT 'SO'::text AS type,
                            a.spk_no AS cabang,
                            a.kp_code,
                            a.pr_date::date AS tanggal,
                            to_char(a.entry_date, 'HH24:MI:SS'::text) AS jaminput,
                            a.pr_no AS no,
                            a.pr_no AS so_no,
                            a.vend_code AS code,
                            a.item_code,
                            a.item_qty,
                            a.item_qty2,
                            (upper(a.pr_desc::text) || ' '::text) || to_char(a.delivery_time, 'YYYY-MM-DD'::text) AS keterangan,
                            a.code_comp,
                            (a.item_qty2 * b.item_weight / 1000000::numeric)::double precision AS item_ton,
                            c.desa,
                            c.no AS sales_form_no,
                            d.name,
                            d.address1 AS alamat_toko,
                            d.sales_code,
                            ''::text AS car_no,
                            ''::text AS wh_code,
                            a.dept_code,
                            c.ot_flag,
                            lower(a.car_no::text) AS freight_type,
                            a.pr_stat,
                            a.delivery_time,
                            a.close_by,
                            a.close_date,
                            a.item_qty_ton,
                            b.item_weight
                        FROM po_req a
                            LEFT JOIN t_dp_detail e ON a.code_comp::text = e.code_comp AND a.pr_no = e.no AND a.item_code = e.item_code::bpchar
                            JOIN t_card d ON a.vend_code = d.code AND a.code_comp = d.code_comp
                            JOIN t_item b ON a.item_code = b.item_code AND a.code_comp = b.code_comp
                            LEFT JOIN sales_form c ON a.ref_no = c.no AND a.code_comp::text = upper(c.kategori::text) AND a.spk_no::bpchar = c.cabang
                        WHERE (e.id IS NULL OR e.status = '1'::bpchar) AND date(a.pr_date) >= date(now() - '120 days'::interval) AND a.pr_stat = 'A'::bpchar AND btrim(a.pending_stat::text) <> 'F'::text AND a.pr_type = 'S'::bpchar AND a.item_qty2 <> 0::numeric
                        AND a.pr_no = '$so_no'";

            }


            // $qry = "SELECT a.pr_no, TRIM(a.item_code) as item_code, a.item_qty::integer, a.item_qty2::integer, b.item_weight, a.price_amt 
            //         FROM po_req a 
            //         -- LEFT JOIN t_dp_detail e ON a.code_comp::text = e.code_comp AND a.pr_no = e.no AND a.item_code = e.item_code::bpchar
            //         JOIN t_card d ON a.vend_code = d.code AND a.code_comp = d.code_comp
            //         JOIN t_item b ON a.item_code = b.item_code AND a.code_comp = b.code_comp
            //         WHERE date(a.pr_date) >= date(now() - '120 days'::interval) 
            //         AND a.pr_stat = 'A' 
            //         AND btrim(a.pending_stat::text) <> 'F'
            //         AND a.pr_type = 'S'::bpchar AND a.item_qty2 <> 0::numeric 
            //         -- AND (e.id IS NULL OR e.status = '1'::bpchar) 
            //         AND a.pr_no = '$so_no'";
            
            $rq = $dbgl->query($qry)->fetchAll(PDO::FETCH_ASSOC);

            // print_r($rq);

            $newHasil[] = array(
                'type' => $value['type'], 
                'code' => $value['code'],
                'ledgername' => $value['ledgername'], 
                'tanggal' => $value['tanggal'],
                'sm_no' => $value['sm_no'],
                'so_no' => $value['so_no'], 
                'item_code' => $value['item_code'], 
                'alamat_toko' => $value['alamat_toko'], 
                'keterangan' => $value['keterangan'], 
                'jaminput' => $value['jaminput'], 
                'code_comp' => $value['code_comp'],
                'sales_code' => $value['sales_code'],
                'cabang' => $value['cabang'],
                'freight_type' => $value['freight_type'],
                'dept_code' => $value['dept_code'],
                'type_so' => 'cabang_int',
                'kp_office' => $value['kp_office'],
                'pr_stat' => $value['pr_stat'], 
                'delivery_time' => $value['delivery_time'],
                'tipe_muat' => $value['tipe_muat'],
                'detail' => $rq,
            );
            $rq = [];
        }


        $resmore = [];
        $resless = [];
        foreach($newHasil as $val) {
            if($val['code_comp'] == 'SAA') {
                $tonase = getTonase($val['detail']);
            } else {
                $tonase = 0;
            }
            // $itemqty = getItemQty($val['detail']);

            if($tonase >= 10) {
                $resmore[] = array(
                    "type" => $val['type'],
                    "code" => $val['code'],
                    "ledgername" => $val['ledgername'],
                    "tanggal" => $val['tanggal'],
                    "sm_no" => $val['sm_no'],
                    "so_no" => $val['so_no'],
                    "alamat_toko" => $val['alamat_toko'],
                    "keterangan" => $val['keterangan'],
                    "jaminput" => $val['jaminput'],
                    "code_comp" => $val['code_comp'],
                    "sales_code" => $val['sales_code'],
                    "cabang" => $val['cabang'],
                    "freight_type" => $val['freight_type'],
                    "dept_code" => $val['dept_code'],
                    "type_so" => $val['type_so'],
                    "kp_office" => $val['kp_office'],
                    "pr_stat" => $val['pr_stat'],
                    "delivery_time" => $val['delivery_time'],
                    "tipe_muat" => $val['tipe_muat'],
                    "ton" => $tonase,
                    // "item_code" => $itemqty,
                    "item_code" => $val['item_code'],
                    "detail" => $val['detail']
                );
                $tonase = [];
            } else {
                $resless[] = array(
                    "type" => $val['type'],
                    "code" => $val['code'],
                    "ledgername" => $val['ledgername'],
                    "tanggal" => $val['tanggal'],
                    "sm_no" => $val['sm_no'],
                    "so_no" => $val['so_no'],
                    "alamat_toko" => $val['alamat_toko'],
                    "keterangan" => $val['keterangan'],
                    "jaminput" => $val['jaminput'],
                    "code_comp" => $val['code_comp'],
                    "sales_code" => $val['sales_code'],
                    "cabang" => $val['cabang'],
                    "freight_type" => $val['freight_type'],
                    "dept_code" => $val['dept_code'],
                    "type_so" => $val['type_so'],
                    "kp_office" => $val['kp_office'],
                    "pr_stat" => $val['pr_stat'],
                    "delivery_time" => $val['delivery_time'],
                    "tipe_muat" => $val['tipe_muat'],
                    "ton" => $tonase,
                    // "item_code" => $itemqty,
                    "item_code" => $val['item_code'],
                    "detail" => $new['detail']
                );
                $tonase = [];
            }
            // $itemqty = [];
        }

        $result = array(
            "more" => $resmore,
            "less" => $resless,
        );

        // print_r($result);

        return $result;
    }

    function getTonase($new) {
        $jumlah_ton = 0;
        $jml_qty = 0;
        $ton = 0;
        foreach($new as $dt) {
            $mystring = $dt['item_code'];
            $findme   = 'B';
            $pos = strpos($mystring, $findme);
            if($pos == false) {
                $itemweight = $dt['item_weight'];
                if($dt['item_qty'] == $dt['item_qty2']) {
                    $jml_qty += $dt['item_qty'];
                } else {
                    $jml_qty += $dt['item_qty2'];
                }
                $ton = ($jml_qty * $itemweight) / 1000000; 
                $jumlah_ton += $ton;
            }
        }
        $jml_qty = 0;
        $ton = 0;
        return $jumlah_ton;
    }

    function getItemQty($new) {
        $string = "";
        foreach($new as $dt) {
            if($dt['item_qty'] == $dt['item_qty2']) {
                $string .= "[".$dt['item_code']."-".$dt['item_qty']."]";
            } else {
                $string .= "[".$dt['item_code']."-".$dt['item_qty2']."]"; 
            }
        }
        return $string;

    }

    function getDataSOKirimPengalihan($dbgl,$data)
    {
        $responseData = [];

        try {

            $query = "SELECT a.type, a.cabang, 
                             a.tanggal, a.jaminput, a.code, a.so_no, a.item_code, a.keterangan, 
                             a.alamat_toko, a.code_comp,
                             a.dept_code, a.freight_type, a.sales_code,
                             a.ledgername, 'cabang_eks' AS type_so
                        FROM t_distribution_plan_pengalihan a
                        WHERE freight_type = 'kirim'
                        AND a.so_no NOT IN (SELECT so_no FROM t_distribution_plan_list)
                        AND a.cabang_pengalihan IN (".$data->where.")";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }
    
        return $responseData;
    }

    function getDataSOAmbil($dbgl,$data)
    {   

        $all_ambil = [];

        // SELECT a.code_comp, a.type, a.cabang, 
        //                         a.tanggal, a.jaminput, a.code, 
        //                         a.name, a.no, a.so_no, a.item_code, 
        //                         a.item_qty, a.item_qty2, a.keterangan, 
        //                         a.item_ton, a.desa, a.alamat_toko, a.car_no, a.wh_code, 
        //                         0 AS total_do, a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type, to_char(a.delivery_time, 'YYYY-MM-DD ') AS delivery_time,
        //                         (SELECT wh_office FROM in_warehouse WHERE wh_code = a.kp_code) AS kp_office 
        //                 FROM v_displan a 
        //                 LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp 
        //                 WHERE a.cabang IN (".$data->where.")
        //                 AND a.freight_type = 'ambil'
        //                 AND a.so_no NOT IN 
        //                     (SELECT a.so_no FROM t_distribution_plan_list a 
        //                     LEFT JOIN t_distribution_plan_item b ON b.id = a.dp_id
        //                     WHERE a.status = '1'
        //                     AND b.status_so = '1'
        //                     OR b.status_so = '0'
        //                     OR b.status_so = '3')
        //                 AND a.so_no NOT IN 
        //                     (SELECT so_no FROM t_distribution_plan_pengalihan a 
        //                     LEFT JOIN t_distribution_plan_item b ON b.id = a.id_pengalihan
        //                     WHERE status_so = '0' OR status_so = '1')

        // $end_date = date('Y-m-d');
        // $start_date = date('Y-m-d', strtotime('-7 days'));

        try {
            $query = "SELECT a.code_comp, a.type, a.cabang, 
                                a.tanggal, a.jaminput, a.code, 
                                a.name, a.no, a.so_no, a.item_code, 
                                a.item_qty, a.item_qty2, a.keterangan, 
                                a.item_ton, a.desa, a.alamat_toko, a.car_no, a.wh_code, 
                                0 AS total_do, a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type, to_char(a.delivery_time, 'YYYY-MM-DD ') AS delivery_time,
                                (SELECT wh_office FROM in_warehouse WHERE wh_code = a.kp_code) AS kp_office 
                        FROM v_displan a 
                        LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp 
                        WHERE a.cabang IN (".$data->where.")
                        AND a.freight_type = 'ambil'";

                        // echo $query;
            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);

            $arrayItem = [];

            foreach ($rows as $key => $row) {
                $so = $row['so_no'].$row['no'];
                // if ($row['type'] == 'SM') {
                //     $so = $row['no'];
                // }
                if (isset($arrayItem[$so])) {
                    $arrayItem[$so]['item_code'] .= '['.$row['item_code'].' - '.$row['item_qty2'].']';
                    unset($rows[$key]);
                } else {
                    $arrayItem[$so] = [
                        'item_code' => '['.$row['item_code'].' - '.$row['item_qty2'].']',
                    ];
                }
            }

            $hasil = [];

            foreach ($rows as $val) {
                // print_r($val);

                $so = $val['so_no'].$val['no'];
                // if ($val['type'] == 'SM') {
                //     $so = $val['no'];
                // }
                $ledgerName = $val['name'];
                if ($val['dept_code'] <> NULL || $val['dept_code'] <> '') {
                    $ledgerName = '['.$val['dept_name'].']'.$ledgerName;
                }

                // DITUTUP KARENA TERHALANGI OT FLAG TGL 04 05 2021
                if($val['type'] == 'SO' && $val['ot_flag'] == 1) {
                    continue;
                }
                
                if ($val['freight_type'] == 'kirim') {
                    continue;
                }

                $hasil[] = array(

                    'type' => $val['type'], 
                    'code' => $val['code'],
                    'ledgername' => $ledgerName, 
                    'tanggal' => $val['tanggal'],
                    'sm_no' => $val['type'] == 'SM' ? $val['no'] : '',
                    'so_no' => $val['so_no'], 
                    'item_code' => $arrayItem[$so]['item_code'], 
                    'alamat_toko' => $val['alamat_toko'], 
                    'keterangan' => $val['keterangan'], 
                    'jaminput' => $val['jaminput'], 
                    'code_comp' => $val['code_comp'],
                    'sales_code' => $val['sales_code'],
                    'alamat_toko' => $val['alamat_toko'],
                    'cabang' => $val['cabang'],
                    'freight_type' => $val['freight_type'],
                    'dept_code' => $val['dept_code'],
                    'type_so' => 'cabang_int',
                    'cabang' => $val['cabang'],
                    'kp_office' => $val['kp_office'], 
                    'delivery_time' => $val['delivery_time']

                );

            }
        } catch (PDOException $e) {
            $e->getMessage();
            $arr_hasil = $e; 
        }

        // $hasilAmbil2 = getDataSOAmbilPengalihan($dbgl,$data);

        // $all_ambil = array_merge($hasilAmbil2,$arr_hasil);

        return $hasil;
    }

    function getDataSOAmbilPengalihan($dbgl,$data)
    {
        $responseData = [];

        try {

            $query = "SELECT a.type, a.cabang, 
                                a.tanggal, a.jaminput, a.code, a.so_no, a.item_code, a.keterangan, 
                                a.alamat_toko, a.code_comp,
                                a.dept_code, a.freight_type, a.sales_code,
                                a.ledgername, 'cabang_eks' AS type_so
                        FROM t_distribution_plan_pengalihan a
                        WHERE freight_type = 'ambil'
                        AND a.so_no NOT IN (SELECT so_no FROM t_distribution_plan_list)
                        AND a.cabang_pengalihan IN (".$data->where.")";
            
            // echo $query;

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }
    
        return $responseData;
    }

    function getOfficeCode($dbh,$data)
    {
        $responseData = [];

        try {

            $username = strtolower($data->username);
            $query = "SELECT kd_off FROM hrm_officeacc WHERE username = '{$username}' AND kd_off NOT IN ('KTW', 'HO', 'ALL', 'DPS')";

            $rows = $dbh->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData['dt_kd_offifce'][] = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }
    
        return $responseData;
    }

    function getPriceAmtBySO($dbgl,$data)
    {
        for($i=0; $i<count($data->dt_so);$i++) {

            if ($data->dt_so[$i]->sm_no != "") {
                $ket = "SM";
                $no  = $data->dt_so[$i]->sm_no;
                $kol = "AND a.no";
            } else {
                $ket = "SO";
                $no = $data->dt_so[$i]->so_no;
                $kol = "AND a.so_no";
            }

            // $so_no = $data->dt_so[$i]->so_no;
            $tipe_muat = $data->dt_so[$i]->tipe_muat;

            try {
                $query = "SELECT a.code_comp, a.type, a.cabang,
                                    a.tanggal, a.jaminput, a.code, 
                                    a.name, a.no, a.so_no, a.item_code, 
                                    a.item_qty, a.item_qty2, a.keterangan, 
                                    a.item_ton, a.desa, a.alamat_toko, a.sales_form_no,
                                    a.car_no, a.wh_code, d.address1 as alamat_promosi , 0 AS total_do, 
                                    a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type, a.sales_code,
                                    CONCAT(d.name,'-',a.name) AS ledgername,
                                    (SELECT lob_code FROM t_card WHERE code = a.code AND code_comp = a.code_comp) AS lob_code,
                                    '$tipe_muat' AS tipe_muat, to_char(delivery_time, 'YYYY-MM-DD') AS delivery_time
                            FROM v_displan a 
                            LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp
                            WHERE a.type = '$ket'
                            $kol = '$no'
                            ORDER BY a.code, a.tanggal, a.no";

                $stmt = $dbgl->prepare($query);
                $stmt->execute();
            
                while($rows_so = $stmt->fetch(PDO::FETCH_ASSOC)) {
                  $arr_so[] = $rows_so;
                }

            } catch (PDOException $e) {
                $xarr_data = $e;
            }
        }

        for ($j=0; $j < count($arr_so); $j++) { 

            $item_code = $arr_so[$j]['item_code'];
            $so_no =  $arr_so[$j]['so_no'];

            $lob_code =  $arr_so[$j]['lob_code'];
            $code_comp =  $arr_so[$j]['code_comp'];

            try {
                $query = "SELECT 
                        a.vend_code, a.pr_no, a.item_code, 
                        a.spk_no, a.item_qty, a.price_amt,
                        a.car_no, a.so_code, '$lob_code' AS lob_code, 
                        '$code_comp' AS code_comp
                    FROM po_req a
                    WHERE pr_no = '$so_no'
                    AND item_code = '$item_code'";

                $stmt = $dbgl->prepare($query);
                $stmt->execute();

                while($rows_so = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $xarr_data[] = $rows_so;
                }

            } catch (PDOException $e) {
                $xarr_data = $e;
            }

            $arr_data['data_so'][] = array(
                'code_comp' => $arr_so[$j]['code_comp'],
                'type' => $arr_so[$j]['type'],   
                'cabang' => $arr_so[$j]['cabang'],   
                'tanggal' => $arr_so[$j]['tanggal'],   
                'jaminput' => $arr_so[$j]['jaminput'],   
                'code' => $arr_so[$j]['code'],   
                'name' => $arr_so[$j]['name'],   
                'no' => $arr_so[$j]['no'],   
                'so_no' => $arr_so[$j]['so_no'],   
                'item_code' => $arr_so[$j]['item_code'],   
                'item_qty' => $arr_so[$j]['item_qty'],   
                'item_qty2' => $arr_so[$j]['item_qty2'],   
                'keterangan' => $arr_so[$j]['keterangan'],   
                'item_ton' => $arr_so[$j]['item_ton'],   
                'desa' => $arr_so[$j]['desa'],   
                'alamat_toko' => $arr_so[$j]['alamat_toko'],   
                'car_no' => $arr_so[$j]['car_no'],   
                'wh_code' => $arr_so[$j]['wh_code'],   
                'alamat_promosi' => $arr_so[$j]['alamat_promosi'],   
                'total_do' => $arr_so[$j]['total_do'],   
                'dept_code' => $arr_so[$j]['dept_code'],   
                'dept_name' => $arr_so[$j]['dept_name'],   
                'ot_flag' => $arr_so[$j]['ot_flag'],   
                'freight_type' => $arr_so[$j]['freight_type'],   
                'sales_code' => $arr_so[$j]['sales_code'],
                'sales_form_no' => $arr_so[$j]['sales_form_no'],
                'ledgername' => $arr_so[$j]['ledgername'],
                'status_so' => $arr_so[$j]['status_so'],
                'lob_code' => $arr_so[$j]['lob_code'],
                'dept_code' => $arr_so[$j]['dept_code'],
                'tipe_muat' => $arr_so[$j]['tipe_muat'],
                'delivery_time' => $arr_so[$j]['delivery_time'],
                'item' => $xarr_data
            );
            $xarr_data = [];
        }

        return $arr_data;
    }

    function getPriceAmtBySales($dbgl,$data,$price_so)
    {
        $result = [];
        $cabang = $data->office;

        for($i=0; $i<count($price_so['data_so']); $i++) {

            $code_comp = $price_so['data_so'][$i]['code_comp'];
            $freight_type = $price_so['data_so'][$i]['freight_type'];

            $item_code = trim($price_so['data_so'][$i]['item'][0]['item_code']);
            $lob_code = trim($price_so['data_so'][$i]['item'][0]['lob_code']);

            try {
                $query = "SELECT a.*, b.inv_uom, b.item_group, upper(b.item_name) AS item_name
                            FROM t_item_price a INNER JOIN t_item b 
                            ON a.item_code = b.item_code AND a.code_comp = b.code_comp
                            WHERE lower(a.code_comp) = lower('$code_comp') 
                            AND a.delete_time IS NULL
                            AND a.coce_code = '$cabang'
                            AND a.item_code = '$item_code'
                            AND a.cash_flag = '1'
                            AND (a.lob_code = '$lob_code' OR a.lob_code = 'ALL')
                            ORDER BY price_date DESC LIMIT 1";

                $stmt = $dbgl->prepare($query);
                $stmt->execute();

                while($rows_pricelist = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $arr_pricelist[] = $rows_pricelist;
                }

            } catch (PDOException $e) {
                $arr_pricelist = $e;
            }

        }

        return $arr_pricelist;
    }

    function saveDistributionPlanKirim($dbgl,$data)
    {
        $now = date('Y-m-d');

        $xprice_so = getPriceAmtBySO($dbgl,$data);
        $price_so = $xprice_so['data_so'];

        $price_sales = getPriceAmtBySales($dbgl,$data,$xprice_so);

        // $arr_all[] = array (
        //     'price_so' => $xprice_so,
        //     'price_sales' => $price_sales
        // );

        // return $arr_all;

        for ($i=0; $i < count($price_so) ; $i++) {
            
            if (trim($price_so[$i]['item'][0]['item_code']) == $price_sales[$i]['item_code']) {
            
                if ($price_so[$i]['item'][0]['price_amt'] == $price_sales[$i]['kirim']) {
                    $arr_all[] = array(
                        'hasil' => 1,
                        'data_so' => $price_so[$i],
                        'price_so' => array(
                            'so_no' => $price_so[$i]['item'][0]['pr_no'],
                            'cabang' => $price_so[$i]['item'][0]['spk_no'],
                            'item_code' => trim($price_so[$i]['item'][0]['item_code']),
                            'price_amount' => $price_so[$i]['item'][0]['price_amt'],
                        ),
                        'price_sales' => array(
                            'cabang' => $price_sales[$i]['coce_code'],
                            'item_code' => $price_sales[$i]['item_code'],
                            'price_amount' => $price_sales[$i]['kirim']
                        )
                    );
                } else {
                    $arr_all[] = array(
                        'hasil' => 2,
                        'data_so' => $price_so[$i],
                        'price_so' => array(
                            'so_no' => $price_so[$i]['item'][0]['pr_no'],
                            'cabang' => $price_so[$i]['item'][0]['spk_no'],
                            'item_code' => trim($price_so[$i]['item'][0]['item_code']),
                            'price_amount' => $price_so[$i]['item'][0]['price_amt'],
                        ),
                        'price_sales' => array(
                            'cabang' => $price_sales[$i]['coce_code'],
                            'item_code' => $price_sales[$i]['item_code'],
                            'price_amount' => $price_sales[$i]['kirim']
                        )
                    );
                }
            } else {
                $arr_all[] = array(
                    'hasil' => 0,
                    'data_so' => $price_so[$i],
                    'price_so' => array(
                        'so_no' => $price_so[$i]['item'][0]['pr_no'],
                        'cabang' => $price_so[$i]['item'][0]['spk_no'],
                        'item_code' => trim($price_so[$i]['item'][0]['item_code']),
                        'price_amount' => $price_so[$i]['item'][0]['price_amt'],
                    ),
                    'price_sales' => array(
                        'cabang' => $price_sales[$i]['coce_code'],
                        'item_code' => $price_sales[$i]['item_code'],
                        'price_amount' => $price_sales[$i]['kirim']
                    )
                );
            }
        }

        return $arr_all;
    }

    function actionSaveDistributionPlanKirim2($dbgl,$data)
    {
        $noDp = generateNoDp($dbgl,$data);

        // return $data;

        $response = [];
        if($data->office == '' || $data->tgl_perintah == '' || $data->username == '' || count($data->dt_so) <= 0){
            $response = [
                'success' => 0,
                'response' => 'Validasi error, silahkan isi data yang diperlukan'
            ];
        } else {
            $id             = "";
            $office         = $data->office;
            $tgl_perintah   = $data->tgl_perintah;
            $username       = $data->username;
            $now            = date('Y-m-d H:i:s');
            $loc            = $data->loc;

            $query = "INSERT INTO t_distribution_plan_item(tgl_perintah, no_dp, created_at, updated_at, insert_by, freight_type, asal_gudang, status_so, asal_so) VALUES (:tgl_perintah, :no_dp, :created_at, :updated_at, :insert_by, :freight_type, :asal_gudang, :status_so, :asal_so) RETURNING id";
            $stmt = $dbgl->prepare($query);
            $dataheader = [
                ':tgl_perintah' => $tgl_perintah,
                ':no_dp' => $noDp,
                ':created_at' => $now,
                ':updated_at' => $now,
                ':insert_by' => $username,
                ':freight_type' => "kirim",
                ':asal_gudang' => $office,
                ':status_so' => '0',
                ':asal_so' => $loc
            ];

            try {
                $stmt->execute($dataheader);
                $stmt->bindColumn('id', $id);
                $stmt->fetch();
                $response = [
                    'success' => 1,
                    'response' => 'sukses',
                ];
            } catch (PDOException $e) {
                $response = [
                    'success' => 0, 
                    'response' => 'silahkan coba kembali!',
                    'error' => $e
                ];
            }

            $sql = "INSERT INTO 
                        t_distribution_plan_list(type,cabang,
                        tanggal,
                        no,
                        so_no,
                        code,
                        item_code,
                        item_qty,
                        item_qty2,
                        keterangan,
                        code_comp,
                        item_ton,
                        desa,
                        sales_from_no,
                        name,
                        alamat_toko,
                        insert_time,
                        insert_by,
                        status,
                        dept_code,
                        dp_id,
                        price_amt,
                        tipe_muat) 
                    VALUES 
                        (:type,:cabang,
                        :tanggal,
                        :no,
                        :so_no,
                        :code,
                        :item_code,
                        :item_qty,
                        :item_qty2,
                        :keterangan,
                        :code_comp,
                        :item_ton,
                        :desa,
                        :sales_from_no,
                        :name,
                        :alamat_toko,
                        :insert_time,
                        :insert_by,
                        :status,
                        :dept_code,
                        :dp_id,
                        :price_amt,
                        :tipe_muat)";

            $stmt = $dbgl->prepare($sql);

            try {

                for($i=0; $i < count($data->dt_price[0]->data_so); $i++) {
                    $stmt->bindValue(':type', $data->dt_price[0]->data_so[$i]->type);
                    $stmt->bindValue(':cabang', $data->dt_price[0]->data_so[$i]->cabang);
                    $stmt->bindValue(':tanggal', $data->dt_price[0]->data_so[$i]->tanggal);
                    $stmt->bindValue(':no', $data->dt_price[0]->data_so[$i]->no);
                    $stmt->bindValue(':so_no', $data->dt_price[0]->data_so[$i]->so_no);
                    $stmt->bindValue(':code', $data->dt_price[0]->data_so[$i]->code);
                    $stmt->bindValue(':item_code', $data->dt_price[0]->data_so[$i]->item_code);
                    $stmt->bindValue(':item_qty', $data->dt_price[0]->data_so[$i]->item_qty);
                    $stmt->bindValue(':item_qty2', $data->dt_price[0]->data_so[$i]->item_qty2);
                    $stmt->bindValue(':keterangan', $data->dt_price[0]->data_so[$i]->keterangan);
                    $stmt->bindValue(':code_comp', $data->dt_price[0]->data_so[$i]->code_comp);
                    $stmt->bindValue(':item_ton', $data->dt_price[0]->data_so[$i]->item_ton);
                    $stmt->bindValue(':desa', $data->dt_price[0]->data_so[$i]->desa);
                    $stmt->bindValue(':sales_from_no', $data->dt_price[0]->data_so[$i]->sales_code);
                    $stmt->bindValue(':name', $data->dt_price[0]->data_so[$i]->name);
                    $stmt->bindValue(':alamat_toko', $data->dt_price[0]->data_so[$i]->alamat_toko);
                    $stmt->bindValue(':insert_time', $now);
                    $stmt->bindValue(':insert_by', $username);
                    $stmt->bindValue(':status', '0');
                    $stmt->bindValue(':dept_code', $data->dt_price[0]->data_so[$i]->dept_code);
                    $stmt->bindValue(':dp_id', $id);
                    $stmt->bindValue(':price_amt', $data->dt_price[0]->price[$i]->price_amount);
                    $stmt->bindValue(':tipe_muat', $data->dt_price[0]->data_so[$i]->tipe_muat);
                    $stmt->execute();
                }

                $response = [
                    "success" => 1,
                    "message" => "Data DP Berhasil Disimpan"
                ];

            } catch (PDOException $err) {
                $response = [
                    "success" => 0,
                    "id" => $id,
                    "message" => $err
                ];
            }
            
            return $response; 
        }
    }

    function actionSaveDistributionPlanKirim($dbgl,$data)
    {
        $noDp = generateNoDp($dbgl,$data);

        // return $data;

        $response = [];
        if($data->office == '' || $data->tgl_perintah == '' || $data->username == '' || count($data->dt_so) <= 0){
            $response = [
                'success' => 0,
                'response' => 'Validasi error, silahkan isi data yang diperlukan'
            ];
        } else {
            $id             = "";
            $office         = $data->office;
            $tgl_perintah   = $data->tgl_perintah;
            $username       = $data->username;
            $now            = date('Y-m-d H:i:s');
            $loc            = $data->loc;
            $totalMuatan            = $data->totalMuatan;

            $query = "INSERT INTO t_distribution_plan(no_dp, created_at, insert_by, freight_type, tgl_kirim, sc_status, total_muatan) 
            VALUES (:no_dp, :created_at, :insert_by, :freight_type, :tgl_kirim, :sc_status, :total_muatan) RETURNING id";
            $stmt = $dbgl->prepare($query);
            $dataheader = [
                ':no_dp' => $noDp,
                ':created_at' => $now,
                ':insert_by' => $username,
                ':freight_type' => "kirim",
                ':tgl_kirim' => $data->tgl_perintah,
                ':sc_status' => 1,
                ':total_muatan' =>$totalMuatan,
            ];

            try {
                $stmt->execute($dataheader);
                $stmt->bindColumn('id', $id);
                $stmt->fetch();
                $response = [
                    'success' => 1,
                    'response' => 'sukses',
                ];
            } catch (PDOException $e) {
                $response = [
                    'success' => 0, 
                    'response' => 'silahkan coba kembali!',
                    'error' => $e
                ];
            }

            $sql = "INSERT INTO 
                        t_dp_detail(type,cabang,
                        tanggal,
                        no,
                        so_no,
                        code,
                        item_code,
                        item_qty,
                        item_qty2,
                        keterangan,
                        code_comp,
                        item_ton,
                        desa,
                        sales_form_no,
                        name,
                        alamat_toko,
                        insert_time,
                        insert_by,
                        status,
                        delivery_date,
                        dept_code,
                        dp_id,
                        pengalihan) 
                    VALUES 
                        (:type,:cabang,
                        :tanggal,
                        :no,
                        :so_no,
                        :code,
                        :item_code,
                        :item_qty,
                        :item_qty2,
                        :keterangan,
                        :code_comp,
                        :item_ton,
                        :desa,
                        :sales_form_no,
                        :name,
                        :alamat_toko,
                        :insert_time,
                        :insert_by,
                        :status,
                        :delivery_date,
                        :dept_code,
                        :dp_id,
                        :pengalihan)";

            $stmt = $dbgl->prepare($sql);

            try {

                for($i=0; $i < count($data->dt_price[0]->data_so); $i++) {

                    $lokasi_berangkat = "";
                    $pengalihan = null;

                    if ($data->office == $data->dt_price[0]->data_so[$i]->cabang) {
                        $lokasi_berangkat = $data->office;
                        $pengalihan = null;
                    } else {
                        $lokasi_berangkat = $data->office;
                        $pengalihan = $data->dt_price[0]->data_so[$i]->cabang;
                    }

                    $xtanggal = $data->dt_price[0]->data_so[$i]->delivery_time;
                    $timestamp = date('Y-m-d H:i:s', strtotime($xtanggal)); 

                    $stmt->bindValue(':type', $data->dt_price[0]->data_so[$i]->type);
                    $stmt->bindValue(':cabang', $lokasi_berangkat);
                    $stmt->bindValue(':tanggal', $data->dt_price[0]->data_so[$i]->tanggal);
                    $stmt->bindValue(':no', $data->dt_price[0]->data_so[$i]->no);
                    $stmt->bindValue(':so_no', $data->dt_price[0]->data_so[$i]->so_no);
                    $stmt->bindValue(':code', $data->dt_price[0]->data_so[$i]->code);
                    $stmt->bindValue(':item_code', $data->dt_price[0]->data_so[$i]->item_code);
                    $stmt->bindValue(':item_qty', $data->dt_price[0]->data_so[$i]->item_qty);
                    $stmt->bindValue(':item_qty2', $data->dt_price[0]->data_so[$i]->item_qty2);
                    $stmt->bindValue(':keterangan', $data->dt_price[0]->data_so[$i]->keterangan);
                    $stmt->bindValue(':code_comp', $data->dt_price[0]->data_so[$i]->code_comp);
                    $stmt->bindValue(':item_ton', $data->dt_price[0]->data_so[$i]->item_ton);
                    $stmt->bindValue(':desa', $data->dt_price[0]->data_so[$i]->desa);
                    $stmt->bindValue(':sales_form_no', $data->dt_price[0]->data_so[$i]->sales_code);
                    $stmt->bindValue(':name', $data->dt_price[0]->data_so[$i]->name);
                    $stmt->bindValue(':alamat_toko', $data->dt_price[0]->data_so[$i]->alamat_toko);
                    $stmt->bindValue(':insert_time', $now);
                    $stmt->bindValue(':insert_by', $username);
                    $stmt->bindValue(':status', '0');
                    $stmt->bindValue(':delivery_date', $timestamp);
                    $stmt->bindValue(':dept_code', $data->dt_price[0]->data_so[$i]->dept_code);
                    $stmt->bindValue(':dp_id', $id); 
                    $stmt->bindValue(':pengalihan', $pengalihan); 
                    $stmt->execute();
                }

                $response = [
                    "success" => 1,
                    "message" => "Data DP Berhasil Disimpan"
                ];

            } catch (PDOException $err) {
                $response = [
                    "success" => 0,
                    "id" => $id,
                    "message" => $err
                ];
            }

            // $query2 = "INSERT INTO t_dp_sc_detail(dp_id, sc_status) VALUES (:dp_id, :sc_status)";
            // $stmt2 = $dbgl->prepare($query2);
            // $dataheader2 = [
            //     ':dp_id' => $id,
            //     ':sc_status' => 0
            // ];

            // try {
            //     $stmt2->execute($dataheader2);
            // } catch (PDOException $ee) {
            //     $err = $ee;
            // }
            
            return $response; 
        }
    }

    function actionSaveDistributionPlanKirimNew($dbgl,$data)
    {

        $now            = date('Y-m-d H:i:s');

        foreach ($data->dt_so as $value) {

            $noDp = generateNoDp($dbgl,$data);

            $id             = "";
            $office         = $data->office;
            $tgl_perintah   = $data->tgl_perintah;
            $username       = $data->username;
            $now            = date('Y-m-d H:i:s');
            $loc            = $data->loc;

            $query = "INSERT INTO t_distribution_plan(no_dp, created_at, insert_by, freight_type, tgl_kirim, sc_status) VALUES (:no_dp, :created_at, :insert_by, :freight_type, :tgl_kirim, :sc_status) RETURNING id";
            $stmt = $dbgl->prepare($query);
            $dataheader = [
                ':no_dp' => $noDp,
                ':created_at' => $now,
                ':insert_by' => $username,
                ':freight_type' => "kirim",
                ':tgl_kirim' => $data->tgl_perintah,
                ':sc_status' => 0,
            ];

            try {
                $stmt->execute($dataheader);
                $stmt->bindColumn('id', $id);
                $stmt->fetch();
                $response = [
                    'success' => 1,
                    'response' => 'sukses',
                ];
            } catch (PDOException $e) {
                $response = [
                    'success' => 0, 
                    'response' => 'silahkan coba kembali!',
                    'error' => $e
                ];
            }

            foreach ($value->detail as $detail) {

                $lokasi_berangkat = "";
                $pengalihan = null;

                if ($data->office == $detail->cabang) {
                    $lokasi_berangkat = $data->office;
                    $pengalihan = null;
                } else {
                    $lokasi_berangkat = $data->office;
                    $pengalihan = $detail->cabang;
                }
                
                $sql = "INSERT INTO 
                            t_dp_detail(type,
                            cabang,
                            tanggal,
                            no,
                            so_no,
                            code,
                            item_code,
                            item_qty,
                            item_qty2,
                            keterangan,
                            code_comp,
                            item_ton,
                            desa,
                            sales_form_no,
                            name,
                            alamat_toko,
                            insert_time,
                            insert_by,
                            status,
                            dept_code,
                            dp_id,
                            pengalihan) 
                        VALUES 
                            (:type,
                            :cabang,
                            :tanggal,
                            :no,
                            :so_no,
                            :code,
                            :item_code,
                            :item_qty,
                            :item_qty2,
                            :keterangan,
                            :code_comp,
                            :item_ton,
                            :desa,
                            :sales_form_no,
                            :name,
                            :alamat_toko,
                            :insert_time,
                            :insert_by,
                            :status,
                            :dept_code,
                            :dp_id,
                            :pengalihan)";

                $stmt = $dbgl->prepare($sql);
                $dataheader2 = [
                    ':type' => $detail->type,
                    ':cabang' => $detail->cabang,
                    ':tanggal' => $detail->tanggal,
                    ':no' => $detail->no,
                    ':so_no' => $detail->so_no,
                    ':code' => $detail->code,
                    ':item_code' => $detail->item_code,
                    ':item_qty' => $detail->item_qty,
                    ':item_qty2' => $detail->item_qty2,
                    ':keterangan' => $detail->keterangan,
                    ':code_comp' => $detail->code_comp,
                    ':item_ton' => $detail->item_ton,
                    ':desa' => $detail->desa,
                    ':sales_form_no' => $detail->sales_form_no,
                    ':name' => $detail->name,
                    ':alamat_toko' => $detail->alamat,
                    ':insert_time' => $now,
                    ':insert_by' => $username,
                    ':status' => '0',
                    ':dept_code' => $detail->dept_code,
                    ':dp_id' => $id,
                    ':pengalihan' => $pengalihan,
                ];

                try {
                    $stmt->execute($dataheader2);
                    $dataheader2 = [];
                    $response = [
                        'success' => 1,
                        'response' => 'sukses detail',
                    ];
                } catch (PDOException $e) {
                    $response = [
                        'success' => 0, 
                        'response' => 'silahkan coba kembali! (detail)',
                        'error' => $e
                    ];
                }
            }
        }

        return $response;
    }

    function saveDistributionPlanAmbil($dbgl,$data)
    {

        $now = date('Y-m-d');

        $xprice_so = getPriceAmtBySO($dbgl,$data);
        $price_so = $xprice_so['data_so'];

        $price_sales = getPriceAmtBySales($dbgl,$data,$xprice_so);

        // $arr_all[] = array (
        //     'price_so' => $data_price_so,
        //     'price_sales' => $price_sales
        // );

        // return $arr_all;

        for ($i=0; $i < count($price_so) ; $i++) {
            
            if (trim($price_so[$i]['item'][0]['item_code']) == $price_sales[$i]['item_code']) {
            
                if ($price_so[$i]['item'][0]['price_amt'] == $price_sales[$i]['kirim']) {
                    $arr_all[] = array(
                        'hasil' => 1,
                        'data_so' => $price_so[$i],
                        'price_so' => array(
                            'so_no' => $price_so[$i]['item'][0]['pr_no'],
                            'cabang' => $price_so[$i]['item'][0]['spk_no'],
                            'item_code' => trim($price_so[$i]['item'][0]['item_code']),
                            'price_amount' => $price_so[$i]['item'][0]['price_amt'],
                        ),
                        'price_sales' => array(
                            'cabang' => $price_sales[$i]['coce_code'],
                            'item_code' => $price_sales[$i]['item_code'],
                            'price_amount' => $price_sales[$i]['kirim']
                        )
                    );
                } else {
                    $arr_all[] = array(
                        'hasil' => 2,
                        'data_so' => $price_so[$i],
                        'price_so' => array(
                            'so_no' => $price_so[$i]['item'][0]['pr_no'],
                            'cabang' => $price_so[$i]['item'][0]['spk_no'],
                            'item_code' => trim($price_so[$i]['item'][0]['item_code']),
                            'price_amount' => $price_so[$i]['item'][0]['price_amt'],
                        ),
                        'price_sales' => array(
                            'cabang' => $price_sales[$i]['coce_code'],
                            'item_code' => $price_sales[$i]['item_code'],
                            'price_amount' => $price_sales[$i]['kirim']
                        )
                    );
                }
            } else {
                $arr_all[] = array(
                    'hasil' => 0,
                    'data_so' => $price_so[$i],
                    'price_so' => array(
                        'so_no' => $price_so[$i]['item'][0]['pr_no'],
                        'cabang' => $price_so[$i]['item'][0]['spk_no'],
                        'item_code' => trim($price_so[$i]['item'][0]['item_code']),
                        'price_amount' => $price_so[$i]['item'][0]['price_amt'],
                    ),
                    'price_sales' => array(
                        'cabang' => $price_sales[$i]['coce_code'],
                        'item_code' => $price_sales[$i]['item_code'],
                        'price_amount' => $price_sales[$i]['kirim']
                    )
                );
            }
        }

        return $arr_all;
        
    }

    function actionSaveDistributionPlanAmbil2($dbgl,$data)
    {
        $now = date('Y-m-d');

        $noDp = generateNoDp($dbgl,$data);

        $response = [];
        if($data->office == '' || $data->tgl_perintah == '' || $data->username == '' || count($data->dt_so) <= 0){
            $response = [
                'success' => 0,
                'response' => 'Validasi error, silahkan isi data yang diperlukan'
            ];
        } else {
            $id             = "";
            $tgl_perintah   = $data->tgl_perintah;
            $office         = $data->office;
            $username       = $data->username;
            $now            = date('Y-m-d H:i:s');

            $query = "INSERT INTO t_distribution_plan_item(tgl_kirim,tgl_perintah, no_dp, created_at, updated_at, insert_by, freight_type, asal_gudang, status_so) VALUES (:tgl_kirim, :tgl_perintah, :no_dp, :created_at, :updated_at, :insert_by, :freight_type, :asal_gudang, :status_so) RETURNING id";
            $stmt = $dbgl->prepare($query);
            $dataheader = [
                ':tgl_kirim' => $tgl_perintah,
                ':tgl_perintah' => $tgl_perintah,
                ':no_dp' => $noDp,
                ':created_at' => $now,
                ':updated_at' => $now,
                ':insert_by' => $username,
                ':freight_type' => "ambil",
                ':asal_gudang' => $office,
                ':status_so' => '0'
            ];

            try {
                $stmt->execute($dataheader);
                $stmt->bindColumn('id', $id);
                $stmt->fetch();
                $response = [
                    'success' => 1,
                    'response' => 'sukses',
                ];
            } catch (PDOException $e) {
                $response = [
                    'success' => 0,
                    'response' => 'silahkan coba kembali!',
                    'error' => $e
                ];
            }

            $sql = "INSERT INTO 
                        t_distribution_plan_list(type,cabang,
                        tanggal,
                        no,
                        so_no,
                        code,
                        item_code,
                        item_qty,
                        item_qty2,
                        keterangan,
                        code_comp,
                        item_ton,
                        desa,
                        sales_from_no,
                        name,
                        alamat_toko,
                        insert_time,
                        insert_by,
                        status,
                        dept_code,
                        dp_id) 
                    VALUES 
                        (:type,:cabang,
                        :tanggal,
                        :no,
                        :so_no,
                        :code,
                        :item_code,
                        :item_qty,
                        :item_qty2,
                        :keterangan,
                        :code_comp,
                        :item_ton,
                        :desa,
                        :sales_from_no,
                        :name,
                        :alamat_toko,
                        :insert_time,
                        :insert_by,
                        :status,
                        :dept_code,
                        :dp_id)";

            $stmt = $dbgl->prepare($sql);

            try {

                for($i=0; $i < count($data->dt_price[0]->data_so); $i++) {
                    $stmt->bindValue(':type', $data->dt_price[0]->data_so[$i]->type);
                    $stmt->bindValue(':cabang', $data->dt_price[0]->data_so[$i]->cabang);
                    $stmt->bindValue(':tanggal', $data->dt_price[0]->data_so[$i]->tanggal);
                    $stmt->bindValue(':no', $data->dt_price[0]->data_so[$i]->no);
                    $stmt->bindValue(':so_no', $data->dt_price[0]->data_so[$i]->so_no);
                    $stmt->bindValue(':code', $data->dt_price[0]->data_so[$i]->code);
                    $stmt->bindValue(':item_code', $data->dt_price[0]->data_so[$i]->item_code);
                    $stmt->bindValue(':item_qty', $data->dt_price[0]->data_so[$i]->item_qty);
                    $stmt->bindValue(':item_qty2', $data->dt_price[0]->data_so[$i]->item_qty2);
                    $stmt->bindValue(':keterangan', $data->dt_price[0]->data_so[$i]->keterangan);
                    $stmt->bindValue(':code_comp', $data->dt_price[0]->data_so[$i]->code_comp);
                    $stmt->bindValue(':item_ton', $data->dt_price[0]->data_so[$i]->item_ton);
                    $stmt->bindValue(':desa', $data->dt_price[0]->data_so[$i]->desa);
                    $stmt->bindValue(':sales_from_no', $data->dt_price[0]->data_so[$i]->sales_code);
                    $stmt->bindValue(':name', $data->dt_price[0]->data_so[$i]->name);
                    $stmt->bindValue(':alamat_toko', $data->dt_price[0]->data_so[$i]->alamat_toko);
                    $stmt->bindValue(':insert_time', $now);
                    $stmt->bindValue(':insert_by', $username);
                    $stmt->bindValue(':status', '0');
                    $stmt->bindValue(':dept_code', $data->dt_price[0]->data_so[$i]->dept_code);
                    $stmt->bindValue(':dp_id', $id);
                    $stmt->execute();
                }

                $response = [
                    "success" => 1,
                    "id" => $id,
                    "message" => "Data DP Berhasil Disimpan"
                ];

            } catch (PDOException $err) {
                $response = [
                    "success" => 0,
                    "id" => $id,
                    "message" => $err
                ];
            }
        }

        return $response;

    }

    function actionSaveDistributionPlanAmbil($dbgl,$data)
    {
        $now = date('Y-m-d');

        $noDp = generateNoDp($dbgl,$data);

        $response = [];
        if($data->office == '' || $data->tgl_perintah == '' || $data->username == '' || count($data->dt_so) <= 0){
            $response = [
                'success' => 0,
                'response' => 'Validasi error, silahkan isi data yang diperlukan'
            ];
        } else {
            $id             = "";
            $tgl_perintah   = $data->tgl_perintah;
            $office         = $data->office;
            $username       = $data->username;
            $now            = date('Y-m-d H:i:s');

            $query = "INSERT INTO t_distribution_plan(no_dp, created_at, insert_by, freight_type) VALUES (:no_dp, :created_at, :insert_by, :freight_type) RETURNING id";
            $stmt = $dbgl->prepare($query);
            $dataheader = [
                ':no_dp' => $noDp,
                ':created_at' => $now,
                ':insert_by' => $username,
                ':freight_type' => "ambil"
            ];

            try {
                $stmt->execute($dataheader);
                $stmt->bindColumn('id', $id);
                $stmt->fetch();
                $response = [
                    'success' => 1,
                    'response' => 'sukses',
                ];
            } catch (PDOException $e) {
                $response = [
                    'success' => 0,
                    'response' => 'silahkan coba kembali!',
                    'error' => $e
                ];
            }

            $sql = "INSERT INTO 
                        t_dp_detail(type,cabang,
                        tanggal,
                        no,
                        so_no,
                        code,
                        item_code,
                        item_qty,
                        item_qty2,
                        keterangan,
                        code_comp,
                        item_ton,
                        desa,
                        sales_form_no,
                        name,
                        alamat_toko,
                        insert_time,
                        insert_by,
                        status,
                        dept_code,
                        dp_id) 
                    VALUES 
                        (:type,:cabang,
                        :tanggal,
                        :no,
                        :so_no,
                        :code,
                        :item_code,
                        :item_qty,
                        :item_qty2,
                        :keterangan,
                        :code_comp,
                        :item_ton,
                        :desa,
                        :sales_form_no,
                        :name,
                        :alamat_toko,
                        :insert_time,
                        :insert_by,
                        :status,
                        :dept_code,
                        :dp_id)";

            $stmt = $dbgl->prepare($sql);

            try {

                for($i=0; $i < count($data->dt_price[0]->data_so); $i++) {
                    $stmt->bindValue(':type', $data->dt_price[0]->data_so[$i]->type);
                    $stmt->bindValue(':cabang', $data->dt_price[0]->data_so[$i]->cabang);
                    $stmt->bindValue(':tanggal', $data->dt_price[0]->data_so[$i]->tanggal);
                    $stmt->bindValue(':no', $data->dt_price[0]->data_so[$i]->no);
                    $stmt->bindValue(':so_no', $data->dt_price[0]->data_so[$i]->so_no);
                    $stmt->bindValue(':code', $data->dt_price[0]->data_so[$i]->code);
                    $stmt->bindValue(':item_code', $data->dt_price[0]->data_so[$i]->item_code);
                    $stmt->bindValue(':item_qty', $data->dt_price[0]->data_so[$i]->item_qty);
                    $stmt->bindValue(':item_qty2', $data->dt_price[0]->data_so[$i]->item_qty2);
                    $stmt->bindValue(':keterangan', $data->dt_price[0]->data_so[$i]->keterangan);
                    $stmt->bindValue(':code_comp', $data->dt_price[0]->data_so[$i]->code_comp);
                    $stmt->bindValue(':item_ton', $data->dt_price[0]->data_so[$i]->item_ton);
                    $stmt->bindValue(':desa', $data->dt_price[0]->data_so[$i]->desa);
                    $stmt->bindValue(':sales_form_no', $data->dt_price[0]->data_so[$i]->sales_code);
                    $stmt->bindValue(':name', $data->dt_price[0]->data_so[$i]->name);
                    $stmt->bindValue(':alamat_toko', $data->dt_price[0]->data_so[$i]->alamat_toko);
                    $stmt->bindValue(':insert_time', $now);
                    $stmt->bindValue(':insert_by', $username);
                    $stmt->bindValue(':status', '0');
                    $stmt->bindValue(':dept_code', $data->dt_price[0]->data_so[$i]->dept_code);
                    $stmt->bindValue(':dp_id', $id);
                    $stmt->execute();
                }

                $response = [
                    "success" => 1,
                    "id" => $id,
                    "message" => "Data DP Berhasil Disimpan"
                ];

            } catch (PDOException $err) {
                $response = [
                    "success" => 0,
                    "id" => $id,
                    "message" => $err
                ];
            }
        }

        return $response;

    }

    function actionSaveDistributionPlanAmbilNew($dbgl,$data)
    {
        $now = date('Y-m-d');

        $noDp = generateNoDp($dbgl,$data);

        $response = [];
        if($data->office == '' || $data->tgl_perintah == '' || $data->username == '' || count($data->dt_so) <= 0){
            $response = [
                'success' => 0,
                'response' => 'Validasi error, silahkan isi data yang diperlukan'
            ];
        } else {
            $id             = "";
            $tgl_perintah   = $data->tgl_perintah;
            $office         = $data->office;
            $username       = $data->username;
            $sopir          = $data->sopir;
            $no_pol         = $data->no_pol;
            $gudang         = $data->gudang;
            $now            = date('Y-m-d H:i:s');

            $query = "INSERT INTO t_distribution_plan(no_dp, created_at, insert_by, freight_type, tgl_kirim, wh_code, no_pol, sopir) VALUES (:no_dp, :created_at, :insert_by, :freight_type, :tgl_kirim, :wh_code, :no_pol, :sopir) RETURNING id";
            $stmt = $dbgl->prepare($query);
            $dataheader = [
                ':no_dp' => $noDp,
                ':created_at' => $now,
                ':insert_by' => $username,
                ':freight_type' => "ambil",
                ':tgl_kirim' => $data->tgl_perintah,
                ':wh_code' => $gudang,
                ':no_pol' => $no_pol,
                ':sopir' => $sopir
            ];

            try {
                $stmt->execute($dataheader);
                $stmt->bindColumn('id', $id);
                $stmt->fetch();
                $response = [
                    'success' => 1,
                    'response' => 'sukses',
                ];
            } catch (PDOException $e) {
                $response = [
                    'success' => 0,
                    'response' => 'silahkan coba kembali!',
                    'error' => $e
                ];
            }

            $sql = "INSERT INTO 
                        t_dp_detail(type,cabang,
                        tanggal,
                        no,
                        so_no,
                        code,
                        item_code,
                        item_qty,
                        item_qty2,
                        keterangan,
                        code_comp,
                        item_ton,
                        desa,
                        sales_form_no,
                        name,
                        alamat_toko,
                        insert_time,
                        insert_by,
                        status,
                        delivery_date,
                        dept_code,
                        dp_id,
                        pengalihan) 
                    VALUES 
                        (:type,:cabang,
                        :tanggal,
                        :no,
                        :so_no,
                        :code,
                        :item_code,
                        :item_qty,
                        :item_qty2,
                        :keterangan,
                        :code_comp,
                        :item_ton,
                        :desa,
                        :sales_form_no,
                        :name,
                        :alamat_toko,
                        :insert_time,
                        :insert_by,
                        :status,
                        :delivery_date,
                        :dept_code,
                        :dp_id,
                        :pengalihan)";

            $stmt = $dbgl->prepare($sql);

            try {

                for($i=0; $i < count($data->dt_price); $i++) {

                    $lokasi_berangkat = "";
                    $pengalihan = null;

                    if ($data->office == $data->dt_price[$i]->data_so->cabang) {
                        $lokasi_berangkat = $data->office;
                        $pengalihan = null;
                    } else {
                        $lokasi_berangkat = $data->office;
                        $pengalihan = $data->dt_price[$i]->data_so->cabang;
                    }


                    $xtanggal = $data->dt_price[$i]->data_so->delivery_time;
                    $timestamp = date('Y-m-d H:i:s', strtotime($xtanggal)); 

                    $stmt->bindValue(':type', $data->dt_price[$i]->data_so->type);
                    $stmt->bindValue(':cabang', $lokasi_berangkat);
                    $stmt->bindValue(':tanggal', $data->dt_price[$i]->data_so->tanggal);
                    $stmt->bindValue(':no', $data->dt_price[$i]->data_so->no);
                    $stmt->bindValue(':so_no', $data->dt_price[$i]->data_so->so_no);
                    $stmt->bindValue(':code', $data->dt_price[$i]->data_so->code);
                    $stmt->bindValue(':item_code', $data->dt_price[$i]->data_so->item_code);
                    $stmt->bindValue(':item_qty', $data->dt_price[$i]->data_so->item_qty);
                    $stmt->bindValue(':item_qty2', $data->dt_price[$i]->data_so->item_qty2);
                    $stmt->bindValue(':keterangan', $data->dt_price[$i]->data_so->keterangan);
                    $stmt->bindValue(':code_comp', $data->dt_price[$i]->data_so->code_comp);
                    $stmt->bindValue(':item_ton', $data->dt_price[$i]->data_so->item_ton);
                    $stmt->bindValue(':desa', $data->dt_price[$i]->data_so->desa);
                    $stmt->bindValue(':sales_form_no', $data->dt_price[$i]->data_so->sales_form_no);
                    $stmt->bindValue(':name', $data->dt_price[$i]->data_so->name);
                    $stmt->bindValue(':alamat_toko', $data->dt_price[$i]->data_so->alamat_toko);
                    $stmt->bindValue(':insert_time', $now);
                    $stmt->bindValue(':insert_by', $username);
                    $stmt->bindValue(':status', '0');
                    $stmt->bindValue(':delivery_date', $xtanggal);
                    $stmt->bindValue(':dept_code', $data->dt_price[$i]->data_so->dept_code);
                    $stmt->bindValue(':dp_id', $id);
                    $stmt->bindValue(':pengalihan', $pengalihan);
                    $stmt->execute();
                }

                $response = [
                    "success" => 1,
                    "id" => $id,
                    "message" => "Data DP Berhasil Disimpan"
                ];

            } catch (PDOException $err) {
                $response = [
                    "success" => 0,
                    "id" => $id,
                    "message" => $err
                ];
            }
        }

        return $response;
    }

    function getDisPlanListKirim2($dbgl,$data)
    {
        $responseData = [];

        // SELECT string_agg(name::text, ',') FROM t_distribution_plan_list WHERE dp_id = a.id

        try {

            $query = "SELECT a.*, to_char(a.tgl_kirim, 'DD Mon YYYY') AS tgl_kirim2, to_char(a.tgl_perintah, 'DD Mon YYYY') AS tgl_perintah2,
                        (SELECT name FROM t_distribution_plan_list WHERE dp_id = a.id LIMIT 1) AS name
                        FROM t_distribution_plan_item a 
                        WHERE a.no_to IS NULL AND a.freight_type = 'kirim'
                        AND a.no_pol IS NULL AND a.sopir IS NULL
                        AND a.asal_gudang IN (".$data->where.")
                        UNION ALL
                        SELECT a.*, to_char(a.tgl_kirim, 'DD Mon YYYY') AS tgl_kirim2, to_char(a.tgl_perintah, 'DD Mon YYYY') AS tgl_perintah2,
                        (SELECT name FROM t_distribution_plan_list WHERE dp_id = a.id LIMIT 1) AS name
                        FROM t_distribution_plan_item a 
                        WHERE a.no_to IS NULL AND a.freight_type = 'kirim'
                        AND a.no_pol IS NULL AND a.sopir IS NULL
                        AND a.asal_gudang NOT IN (".$data->where.")
                        AND a.asal_so IN (".$data->where.")";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $response = $rows;

        } catch (PDOException $e) {
            $response = $e;
        }

        for($i=0;$i<count($response);$i++) {

            $id_item = $response[$i]['id'];

            try {
                $sql = "SELECT b.*
                            FROM t_distribution_plan_list b
                            INNER JOIN t_card c on b.code = c.code and b.code_comp = c.code_comp
                            WHERE dp_id = '$id_item' AND b.status = '0'";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            $hasil[] = array(
                "id" => $response[$i]['id'],
                "no_dp" => $response[$i]['no_dp'],
                "asal_gudang" => $response[$i]['asal_gudang'],
                "asal_so" => $response[$i]['asal_so'],
                "wh_code" => $response[$i]['wh_code'],
                "freight_type" => $response[$i]['freight_type'],
                "tgl_kirim" => $response[$i]['tgl_kirim'],
                "tgl_kirim2" => $response[$i]['tgl_kirim2'],
                "tgl_perintah2" => $response[$i]['tgl_perintah2'],
                "price_amount" => $response[$i]['price_amount'],
                "status_so" => $response[$i]['status_so'],
                "name" => $response[$i]['name'],
                "item" => $arr
            );
            $arr = [];

        } 
    
        return $hasil;
    }

    function getDisPlanListKirim($dbgl,$data)
    {
        $responseData = [];

        $start_date = date('Y-m-d');

        // SELECT string_agg(name::text, ',') FROM t_distribution_plan_list WHERE dp_id = a.id

        $where = " (b.cabang IN (".$data->where.") OR b.pengalihan IN('{$data->loc}'))";

        try {

            $query = "SELECT a.*, JSON_AGG((b.*, c.name)) as detail FROM t_distribution_plan a
                            INNER JOIN t_dp_detail b ON a.id = b.dp_id
                            INNER JOIN t_card c on b.code = c.code and b.code_comp = c.code_comp
                        WHERE {$where} AND b.status = '0' AND a.no_to IS NULL AND a.freight_type = 'kirim'
                        AND a.tgl_kirim < '$start_date' 
                        AND a.deleted_at IS NULL
                        GROUP BY a.id";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            
            foreach($rows as $key => $dt) {
                $detail = json_decode($dt['detail'], true);

                $hasil[] = array(
                    'id' => $dt['id'],
                    'no_dp' => $dt['no_dp'],
                    'tgl_kirim' => $dt['tgl_kirim'],
                    'ritase' => $dt['ritase'],
                    'wh_code' => $dt['wh_code'],
                    'no_pol' => $dt['no_pol'],
                    'sopir' => $dt['sopir'],
                    'insert_by' => $dt['insert_by'],
                    'created_at' => $dt['created_at'],
                    'catatan' => $dt['catatan'],
                    'no_to' => $dt['no_to'],
                    'catatan_to' => $dt['catatan_to'],
                    'tgl_terima' => $dt['tgl_terima'],
                    'freight_type' => $dt['freight_type'],
                    'sc_status' => $dt['sc_status'],
                    'dtl_ledger' => $detail[0]['f7'],
                    'dtl_nama' => $detail[0]['f16'],
                    'dtl_alamat' => $detail[0]['f17'], 
                    'dtl_keterangan' => $detail[0]['f11'], 
                    'total_muatan' => $dt['total_muatan'], 
                    'total_biaya' => $dt['total_biaya'], 
                    'detail' => $detail
                );
                $detail = [];
            }



        } catch (PDOException $e) {
            $hasil = $e;
        }
    
        return $hasil;
    }

    function getDisPlanListKirimByDate($dbgl,$data)
    {
        $responseData = [];

        // SELECT string_agg(name::text, ',') FROM t_distribution_plan_list WHERE dp_id = a.id

        $where = " (b.cabang IN (".$data->where.") OR b.pengalihan IN('{$data->loc}'))";

        try {

            $query = "SELECT a.*, JSON_AGG((b.*, c.name)) as detail FROM t_distribution_plan a
                            INNER JOIN t_dp_detail b ON a.id = b.dp_id
                            INNER JOIN t_card c on b.code = c.code and b.code_comp = c.code_comp
                        WHERE {$where} AND b.status = '0' AND a.no_to IS NULL AND a.freight_type = 'kirim'
                        AND a.tgl_kirim = '$data->date'
                        AND a.deleted_at IS NULL
                        GROUP BY a.id";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            
            foreach($rows as $key => $dt) {
                $detail = json_decode($dt['detail'], true);

                $hasil[] = array(
                    'id' => $dt['id'],
                    'no_dp' => $dt['no_dp'],
                    'tgl_kirim' => $dt['tgl_kirim'],
                    'ritase' => $dt['ritase'],
                    'wh_code' => $dt['wh_code'],
                    'no_pol' => $dt['no_pol'],
                    'sopir' => $dt['sopir'],
                    'insert_by' => $dt['insert_by'],
                    'created_at' => $dt['created_at'],
                    'catatan' => $dt['catatan'],
                    'no_to' => $dt['no_to'],
                    'catatan_to' => $dt['catatan_to'],
                    'tgl_terima' => $dt['tgl_terima'],
                    'freight_type' => $dt['freight_type'],
                    'sc_status' => $dt['sc_status'],
                    'dtl_ledger' => $detail[0]['f7'],
                    'dtl_nama' => $detail[0]['f16'],
                    'dtl_alamat' => $detail[0]['f17'], 
                    'dtl_keterangan' => $detail[0]['f11'], 
                    'total_muatan' => $dt['total_muatan'], 
                    'total_biaya' => $dt['total_biaya'], 
                    'detail' => $detail
                );
                $detail = [];
            }



        } catch (PDOException $e) {
            $hasil = $e;
        }
    
        return $hasil;
    } 

    function getDisPlanListKirimByDateUp($dbgl,$data)
    {
        $responseData = [];

        // SELECT string_agg(name::text, ',') FROM t_distribution_plan_list WHERE dp_id = a.id

        $where = " (b.cabang IN (".$data->where.") OR b.pengalihan IN('{$data->loc}'))";

        try {

            $query = "SELECT a.*, JSON_AGG((b.*, c.name)) as detail FROM t_distribution_plan a
                            INNER JOIN t_dp_detail b ON a.id = b.dp_id
                            INNER JOIN t_card c on b.code = c.code and b.code_comp = c.code_comp
                        WHERE {$where} AND b.status = '0' AND a.no_to IS NULL AND a.freight_type = 'kirim'
                        AND a.tgl_kirim > '$data->date'
                        AND a.deleted_at IS NULL
                        GROUP BY a.id";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            
            foreach($rows as $key => $dt) {
                $detail = json_decode($dt['detail'], true);

                $hasil[] = array(
                    'id' => $dt['id'],
                    'no_dp' => $dt['no_dp'],
                    'tgl_kirim' => $dt['tgl_kirim'],
                    'ritase' => $dt['ritase'],
                    'wh_code' => $dt['wh_code'],
                    'no_pol' => $dt['no_pol'],
                    'sopir' => $dt['sopir'],
                    'insert_by' => $dt['insert_by'],
                    'created_at' => $dt['created_at'],
                    'catatan' => $dt['catatan'],
                    'no_to' => $dt['no_to'],
                    'catatan_to' => $dt['catatan_to'],
                    'tgl_terima' => $dt['tgl_terima'],
                    'freight_type' => $dt['freight_type'],
                    'sc_status' => $dt['sc_status'],
                    'dtl_ledger' => $detail[0]['f7'],
                    'dtl_nama' => $detail[0]['f16'],
                    'dtl_alamat' => $detail[0]['f17'], 
                    'dtl_keterangan' => $detail[0]['f11'], 
                    'total_muatan' => $dt['total_muatan'], 
                    'total_biaya' => $dt['total_biaya'], 
                    'detail' => $detail
                );
                $detail = [];
            }



        } catch (PDOException $e) {
            $hasil = $e;
        }
    
        return $hasil;
    }

    function getDisPlanKirimByDP2($dbgl,$data)
    {
        $response = [];

        try {

            $query = "SELECT a.*, to_char(a.tgl_kirim, 'DD Mon YYYY') AS tgl_kirim2 
                        FROM t_distribution_plan_item a 
                        WHERE a.no_to IS NULL AND a.freight_type = 'kirim' AND no_dp = '$data->no_dp'";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $response = $rows;

        } catch (PDOException $e) {
            $response = $e;
        }

        for($i=0;$i<count($response);$i++) {

            $id_item = $response[$i]['id'];

            try {
                $sql = "SELECT b.*
                            FROM t_distribution_plan_list b
                            INNER JOIN t_card c on b.code = c.code and b.code_comp = c.code_comp
                            WHERE dp_id = '$id_item' AND b.status = '0'";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            $hasil[] = array(
                "id" => $response[$i]['id'],
                "no_dp" => $response[$i]['no_dp'],
                "asal_gudang" => $response[$i]['asal_gudang'],
                "wh_code" => $response[$i]['wh_code'],
                "freight_type" => $response[$i]['freight_type'],
                "tgl_kirim" => $response[$i]['tgl_kirim'],
                "tgl_kirim2" => $response[$i]['tgl_kirim2'],
                "tgl_perintah" => $response[$i]['tgl_perintah'],
                "price_amount" => $response[$i]['price_amount'],
                "status_so" => $response[$i]['status_so'],
                "no_pol" => $response[$i]['no_pol'],
                "sopir" => $response[$i]['sopir'],
                "ritase" => $response[$i]['ritase'],
                "catatan" => $response[$i]['catatan'],
                "catatan_to" => $response[$i]['catatan_to'],
                "item" => $arr
            );
            $arr = [];

        } 
    
        return $hasil;
    }

    function getDisPlanKirimByDP($dbgl,$data)
    {
        $response = [];

        $where = " (b.cabang IN (".$data->where.") OR b.pengalihan IN('{$data->loc}'))";

        try {

            $query = "SELECT a.*, JSON_AGG((b.*, c.name)) as detail FROM t_distribution_plan a
                            INNER JOIN t_dp_detail b ON a.id = b.dp_id
                            INNER JOIN t_card c on b.code = c.code and b.code_comp = c.code_comp
                        WHERE {$where} AND b.status = '0' AND a.no_to IS NULL AND a.freight_type = 'kirim'
                        AND no_dp = '$data->no_dp'
                        GROUP BY a.id";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            
            foreach($rows as $key => $dt) {
                $detail = json_decode($dt['detail'], true);

                $hasil[] = array(
                    'id' => $dt['id'],
                    'no_dp' => $dt['no_dp'],
                    'tgl_kirim' => $dt['tgl_kirim'],
                    'ritase' => $dt['ritase'],
                    'wh_code' => $dt['wh_code'],
                    'no_pol' => $dt['no_pol'],
                    'sopir' => $dt['sopir'],
                    'insert_by' => $dt['insert_by'],
                    'created_at' => $dt['created_at'],
                    'catatan' => $dt['catatan'],
                    'no_to' => $dt['no_to'],
                    'catatan_to' => $dt['catatan_to'],
                    'tgl_terima' => $dt['tgl_terima'],
                    'freight_type' => $dt['freight_type'],
                    'dtl_ledger' => $detail[0]['f7'],
                    'dtl_nama' => $detail[0]['f16'],
                    'dtl_alamat' => $detail[0]['f17'], 
                    'dtl_keterangan' => $detail[0]['f11'], 
                    'detail' => $detail
                );
                $detail = [];
            }



        } catch (PDOException $e) {
            $hasil = $e;
        }
    
        return $hasil;
    
        return $hasil;
    }

    function getDisPlanAmbilByDP($dbgl,$data)
    {
        $response = [];

        try {

            $query = "SELECT a.*, to_char(a.tgl_kirim, 'DD Mon YYYY') AS tgl_kirim2 
                        FROM t_distribution_plan_item a 
                        WHERE a.no_to IS NULL AND a.freight_type = 'ambil' AND no_dp = '$data->no_dp'";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $response = $rows;

        } catch (PDOException $e) {
            $response = $e;
        }

        for($i=0;$i<count($response);$i++) {

            $id_item = $response[$i]['id'];

            try {
                $sql = "SELECT b.*
                            FROM t_distribution_plan_list b
                            INNER JOIN t_card c on b.code = c.code and b.code_comp = c.code_comp
                            WHERE dp_id = '$id_item' AND b.status = '0'";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            $hasil[] = array(
                "id" => $response[$i]['id'],
                "no_dp" => $response[$i]['no_dp'],
                "asal_gudang" => $response[$i]['asal_gudang'],
                "wh_code" => $response[$i]['wh_code'],
                "freight_type" => $response[$i]['freight_type'],
                "tgl_kirim" => $response[$i]['tgl_kirim'],
                "tgl_kirim2" => $response[$i]['tgl_kirim2'],
                "price_amount" => $response[$i]['price_amount'],
                "status_so" => $response[$i]['status_so'],
                "no_pol" => $response[$i]['no_pol'],
                "sopir" => $response[$i]['sopir'],
                "ritase" => $response[$i]['ritase'],
                "catatan_to" => $response[$i]['catatan_to'],
                "item" => $arr
            );
            $arr = [];

        } 
    
        return $hasil;
    }

    function getDisPlanTransferByDP($dbgl,$data)
    {
        $response = [];

        try {

            $query = "SELECT a.*, to_char(a.tgl_kirim, 'DD Mon YYYY') AS tgl_kirim2 
                        FROM t_distribution_plan_item a 
                        WHERE a.no_to IS NOT NULL AND a.freight_type = 'kirim' AND no_dp = '$data->no_dp'";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $response = $rows;

        } catch (PDOException $e) {
            $response = $e;
        }

        for($i=0;$i<count($response);$i++) {

            $id_item = $response[$i]['id'];

            try {
                $sql = "SELECT b.*
                            FROM t_distribution_plan_list b
                            INNER JOIN t_card c on b.code = c.code and b.code_comp = c.code_comp
                            WHERE dp_id = '$id_item' AND b.status = '0'";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            $hasil[] = array(
                "id" => $response[$i]['id'],
                "no_dp" => $response[$i]['no_dp'],
                "asal_gudang" => $response[$i]['asal_gudang'],
                "wh_code" => $response[$i]['wh_code'],
                "freight_type" => $response[$i]['freight_type'],
                "tgl_kirim" => $response[$i]['tgl_kirim'],
                "tgl_kirim2" => $response[$i]['tgl_kirim2'],
                "price_amount" => $response[$i]['price_amount'],
                "status_so" => $response[$i]['status_so'],
                "no_pol" => $response[$i]['no_pol'],
                "sopir" => $response[$i]['sopir'],
                "ritase" => $response[$i]['ritase'],
                "catatan_to" => $response[$i]['catatan_to'],
                "item" => $arr
            );
            $arr = [];

        } 
    
        return $hasil;
    }

    function getDisPlanListAmbil2($dbgl,$data)
    {
        $responseData = [];

        // SELECT string_agg(name::text, ',') FROM t_distribution_plan_list WHERE dp_id = a.id

        try {

            $query = "SELECT a.*, to_char(a.tgl_kirim, 'DD Mon YYYY') AS tgl_kirim2, to_char(a.tgl_perintah, 'DD Mon YYYY') AS tgl_perintah2,
                        (SELECT name FROM t_distribution_plan_list WHERE dp_id = a.id LIMIT 1) AS name 
                        FROM t_distribution_plan_item a 
                        WHERE a.no_to IS NULL AND a.freight_type = 'ambil'
                        AND a.no_pol IS NULL AND a.sopir IS NULL
                        AND a.asal_gudang IN (".$data->where.")";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $response = $rows;

        } catch (PDOException $e) {
            $response = $e;
        }

        for($i=0;$i<count($response);$i++) {

            $id_item = $response[$i]['id'];

            try {
                $sql = "SELECT b.* 
                        FROM t_distribution_plan_list b
                        INNER JOIN t_card c on b.code = c.code and b.code_comp = c.code_comp
                        WHERE dp_id = '$id_item'";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            $hasil[] = array(
                "id" => $response[$i]['id'],
                "no_dp" => $response[$i]['no_dp'],
                "asal_gudang" => $response[$i]['asal_gudang'],
                "wh_code" => $response[$i]['wh_code'],
                "freight_type" => $response[$i]['freight_type'],
                "tgl_kirim" => $response[$i]['tgl_kirim'],
                "tgl_kirim2" => $response[$i]['tgl_kirim2'],
                "tgl_perintah2" => $response[$i]['tgl_perintah2'],
                "price_amount" => $response[$i]['price_amount'],
                "status_so" => $response[$i]['status_so'],
                "name" => $response[$i]['name'],
                "item" => $arr
            );
            $arr = [];

        } 
    
        return $hasil;
    }

    function getDisPlanListAmbil($dbgl,$data)
    {
        $responseData = [];

        // SELECT string_agg(name::text, ',') FROM t_distribution_plan_list WHERE dp_id = a.id

        $where = " (b.cabang IN (".$data->where.") OR b.pengalihan IN('{$data->loc}'))";

        try {

            $query = "SELECT a.*, JSON_AGG((b.*, c.name)) as detail FROM t_distribution_plan a
                            INNER JOIN t_dp_detail b ON a.id = b.dp_id
                            INNER JOIN t_card c on b.code = c.code and b.code_comp = c.code_comp
                        WHERE {$where} AND b.status = '0' AND a.no_to IS NULL AND a.freight_type = 'ambil'
                        GROUP BY a.id";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);

            foreach($rows as $key => $dt) {
                $detail = json_decode($dt['detail'], true);

                $hasil[] = array(
                    'id' => $dt['id'],
                    'no_dp' => $dt['no_dp'],
                    'tgl_kirim' => $dt['tgl_kirim'],
                    'ritase' => $dt['ritase'],
                    'wh_code' => $dt['wh_code'],
                    'no_pol' => $dt['no_pol'],
                    'sopir' => $dt['sopir'],
                    'insert_by' => $dt['insert_by'],
                    'created_at' => $dt['created_at'],
                    'catatan' => $dt['catatan'],
                    'no_to' => $dt['no_to'],
                    'catatan_to' => $dt['catatan_to'],
                    'tgl_terima' => $dt['tgl_terima'],
                    'freight_type' => $dt['freight_type'],
                    'dtl_ledger' => $detail[0]['f7'],
                    'dtl_nama' => $detail[0]['f16'],
                    'dtl_alamat' => $detail[0]['f17'], 
                    'dtl_keterangan' => $detail[0]['f11'], 
                    'detail' => $detail
                );
                $detail = [];
            }

        } catch (PDOException $e) {
            $hasil = $e;
        }
    
        return $hasil;
    }

    function saveProsesPengiriman($dbgl,$data)
    {
        return $data;
        // try {

        //     $sql = "INSERT INTO 
        //               t_distribution_plan_pengiriman(id_dp,
        //               kd_off_start,
        //               kd_off_end,
        //               tgl_kirim,
        //               alasan,
        //               code,
        //               ledgername,
        //               alamat,
        //               qty_barang) 
        //             VALUES 
        //               (:id_dp,
        //                 :kd_off_start,
        //                 :kd_off_end,
        //                 :tgl_kirim,
        //                 :alasan,
        //                 :code,
        //                 :ledgername,
        //                 :alamat,
        //                 :qty_barang)";
        
        //     $stmt = $dbgl->prepare($sql);
        
        //     $stmt->bindValue(':id_dp', $data->detail->id_dp);
        //     $stmt->bindValue(':kd_off_start', $data->gd_start);
        //     $stmt->bindValue(':kd_off_end', $data->detail->cabang);
        //     $stmt->bindValue(':tgl_kirim', $data->tgl_kirim);
        //     $stmt->bindValue(':alasan', $data->alasan);
        //     $stmt->bindValue(':code', $data->detail->code);
        //     $stmt->bindValue(':ledgername', $data->detail->ledgername);
        //     $stmt->bindValue(':alamat', $data->detail->alamat_toko);
        //     $stmt->bindValue(':qty_barang', $data->qty);
        //     $stmt->execute();

        //     $respon = [
        //         "success" => 1,
        //         "message" => "Data Proses Pengiriman Berhasil Disimpan"
        //     ];

        // } catch (PDOException $e) {
        //     $e->getMessage();
        //     $respon = [
        //         "success" => 0,
        //         "message" => $e
        //     ];
        // }

        // return $respon;
    }

    function getDisPlanListTransfer2($dbgl,$data)
    {
        $responseData = [];

        try {

            $query = "SELECT a.*, to_char(a.tgl_kirim, 'DD Mon YYYY') AS tgl_kirim2,
                        (SELECT string_agg(name::text, ',') FROM t_distribution_plan_list WHERE dp_id = a.id) AS name 
                        FROM t_distribution_plan_item a 
                        WHERE a.no_to IS NOT NULL AND a.freight_type = 'kirim'
                        AND a.asal_gudang IN (".$data->where.")";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $response = $rows;

        } catch (PDOException $e) {
            $response = $e;
        }

        for($i=0;$i<count($response);$i++) {

            $id_item = $response[$i]['id'];

            try {
                $sql = "SELECT b.*
                            FROM t_distribution_plan_list b
                            INNER JOIN t_card c on b.code = c.code and b.code_comp = c.code_comp
                            WHERE dp_id = '$id_item' AND b.status = '0'";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            $hasil[] = array(
                "id" => $response[$i]['id'],
                "no_dp" => $response[$i]['no_dp'],
                "asal_gudang" => $response[$i]['asal_gudang'],
                "wh_code" => $response[$i]['wh_code'],
                "freight_type" => $response[$i]['freight_type'],
                "tgl_kirim" => $response[$i]['tgl_kirim'],
                "tgl_kirim2" => $response[$i]['tgl_kirim2'],
                "price_amount" => $response[$i]['price_amount'],
                "status_so" => $response[$i]['status_so'],
                "name" => $response[$i]['name'],
                "item" => $arr
            );
            $arr = [];

        } 
    
        return $hasil;
    }

    function getDisPlanListTransfer($dbgl,$data)
    {
        $responseData = [];

        $start_date = date('Y-m-d');

        // SELECT string_agg(name::text, ',') FROM t_distribution_plan_list WHERE dp_id = a.id

        $where = " (b.cabang IN (".$data->where.") OR b.pengalihan IN('{$data->loc}'))";

        try {

            $query = "SELECT a.*, JSON_AGG((b.*, c.name)) as detail FROM t_distribution_plan a
                            INNER JOIN t_dp_detail b ON a.id = b.dp_id
                            INNER JOIN t_card c on b.code = c.code and b.code_comp = c.code_comp
                        WHERE {$where} AND b.status = '0' AND a.no_to IS NOT NULL AND a.freight_type = 'kirim'
                        GROUP BY a.id";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            
            foreach($rows as $key => $dt) {
                $detail = json_decode($dt['detail'], true);

                $hasil[] = array(
                    'id' => $dt['id'],
                    'no_dp' => $dt['no_dp'],
                    'tgl_kirim' => $dt['tgl_kirim'],
                    'ritase' => $dt['ritase'],
                    'wh_code' => $dt['wh_code'],
                    'no_pol' => $dt['no_pol'],
                    'sopir' => $dt['sopir'],
                    'insert_by' => $dt['insert_by'],
                    'created_at' => $dt['created_at'],
                    'catatan' => $dt['catatan'],
                    'no_to' => $dt['no_to'],
                    'catatan_to' => $dt['catatan_to'],
                    'tgl_terima' => $dt['tgl_terima'],
                    'freight_type' => $dt['freight_type'],
                    'dtl_ledger' => $detail[0]['f7'],
                    'dtl_nama' => $detail[0]['f16'],
                    'dtl_alamat' => $detail[0]['f17'], 
                    'dtl_keterangan' => $detail[0]['f11'], 
                    'detail' => $detail
                );
                $detail = [];
            }



        } catch (PDOException $e) {
            $hasil = $e;
        }
    
        return $hasil;
    }
    
    function getDaftarPengirimanKirim($dbgl,$data)
    {
        $responseData = [];

        try {

            $query = "SELECT a.*, to_char(a.tgl_kirim, 'DD Mon YYYY') AS tgl_kirim2, 
                      (SELECT string_agg(name::text, ',') FROM t_distribution_plan_list WHERE dp_id = a.id) AS name
                      FROM t_distribution_plan_item a WHERE status_so = '1' 
                      AND freight_type = 'kirim' AND no_to IS NULL
                      AND a.asal_gudang IN (".$data->where.")";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }

        for($i=0;$i<count($responseData);$i++) {

            $dpId = $responseData[$i]['id'];

            try {
                $sql = "SELECT 
                            id, a.code_comp, a.type, a.cabang, a.tanggal, a.code, a.name, a.no, a.so_no, a.item_code, a.item_qty, a.item_qty2, 
                            a.keterangan, a.item_ton, a.desa, a.alamat_toko, 0 AS total_do, a.status, a.dept_code, d.name as dept_name
                        FROM t_distribution_plan_list a
                        LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp
                        WHERE a.status = '0' AND a.dp_id = {$dpId}
                        ORDER BY a.so_no";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            $hasil[] = array(
                "id" => $responseData[$i]['id'],
                "no_dp" => $responseData[$i]['no_dp'],
                "name" => $responseData[$i]['name'],
                "tgl_kirim" => $responseData[$i]['tgl_kirim'],
                "tgl_kirim2" => $responseData[$i]['tgl_kirim2'],
                "wh_code" => $responseData[$i]['wh_code'],
                "no_pol" => $responseData[$i]['no_pol'],
                "sopir" => $responseData[$i]['sopir'],
                "item" => $arr
            );
            $arr = [];

        }
    
        return $hasil;
    }

    function getDaftarPengirimanKirimByID2($dbgl,$data)
    {
        $responseData = [];

        // return $data;

        try {

            $query = "SELECT a.*, to_char(a.tgl_kirim, 'DD Mon YYYY') AS tgl_kirim2 
                        FROM t_distribution_plan_item a 
                        WHERE status_so = '1' 
                        AND freight_type = 'kirim' 
                        AND no_to IS NULL
                        AND id = $data->id_dt";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }

        for($i=0;$i<count($responseData);$i++) {

            try {
                $sql = "SELECT 
                            id, a.code_comp, a.type, a.cabang, a.tanggal, a.code, a.name, a.no, a.so_no, a.item_code, a.item_qty, a.item_qty2, 
                            a.keterangan, a.item_ton, a.desa, a.alamat_toko, 0 AS total_do, a.status, a.dept_code, d.name as dept_name
                        FROM t_distribution_plan_list a
                        LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp
                        WHERE a.status = '0' AND a.dp_id = {$data->id_dt}
                        ORDER BY a.so_no";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            $hasil[] = array(
                "id" => $responseData[$i]['id'],
                "no_dp" => $responseData[$i]['no_dp'],
                "tgl_kirim" => $responseData[$i]['tgl_kirim'],
                "tgl_kirim2" => $responseData[$i]['tgl_kirim2'],
                "wh_code" => $responseData[$i]['wh_code'],
                "no_pol" => $responseData[$i]['no_pol'],
                "sopir" => $responseData[$i]['sopir'],
                "item" => $arr
            );
            $arr = [];

        }
    
        return $hasil;
    }

    function getDaftarPengirimanKirimByID($dbgl,$data)
    {
        $responseData = [];

        // return $data;

        try {

            $query = "SELECT a.*, to_char(a.tgl_kirim, 'DD Mon YYYY') AS tgl_kirim2 
                        FROM t_distribution_plan_item a 
                        WHERE status_so = '1' 
                        AND freight_type = 'kirim' 
                        AND no_to IS NULL
                        AND id = $data->id_dt";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }

        for($i=0;$i<count($responseData);$i++) {

            try {
                $sql = "SELECT 
                            id, a.code_comp, a.type, a.cabang, a.tanggal, a.code, a.name, a.no, a.so_no, a.item_code, a.item_qty, a.item_qty2, 
                            a.keterangan, a.item_ton, a.desa, a.alamat_toko, 0 AS total_do, a.status, a.dept_code, d.name as dept_name
                        FROM t_distribution_plan_list a
                        LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp
                        WHERE a.status = '0' AND a.dp_id = {$data->id_dt}
                        ORDER BY a.so_no";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            $hasil[] = array(
                "id" => $responseData[$i]['id'],
                "no_dp" => $responseData[$i]['no_dp'],
                "tgl_kirim" => $responseData[$i]['tgl_kirim'],
                "tgl_kirim2" => $responseData[$i]['tgl_kirim2'],
                "wh_code" => $responseData[$i]['wh_code'],
                "no_pol" => $responseData[$i]['no_pol'],
                "sopir" => $responseData[$i]['sopir'],
                "item" => $arr
            );
            $arr = [];

        }
    
        return $hasil;
    }

    function getDaftarPengirimanAmbil($dbgl,$data)
    {
        $responseData = [];

        try {
            $query = "SELECT a.*, to_char(a.tgl_perintah, 'DD Mon YYYY') AS tgl_perintah2 FROM t_distribution_plan_item a WHERE status_so = '1' 
                      AND freight_type = 'ambil' AND no_to IS NULL
                      AND a.asal_gudang IN (".$data->where.")";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }

        for($i=0;$i<count($responseData);$i++) {

            $dpId = $responseData[$i]['id'];

            try {
                $sql = "SELECT 
                            id, a.code_comp, a.type, a.cabang, a.tanggal, a.code, a.name, a.no, a.so_no, a.item_code, a.item_qty, a.item_qty2, 
                            a.keterangan, a.item_ton, a.desa, a.alamat_toko, 0 AS total_do, a.status, a.dept_code, d.name as dept_name
                        FROM t_distribution_plan_list a
                        LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp
                        WHERE a.status = '0' AND a.dp_id = {$dpId}
                        ORDER BY a.so_no";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            $hasil[] = array(
                "id" => $responseData[$i]['id'],
                "no_dp" => $responseData[$i]['no_dp'],
                "tgl_perintah" => $responseData[$i]['tgl_perintah'],
                "tgl_perintah2" => $responseData[$i]['tgl_perintah2'],
                "wh_code" => $responseData[$i]['wh_code'],
                "no_pol" => $responseData[$i]['no_pol'],
                "sopir" => $responseData[$i]['sopir'],
                "item" => $arr
            );
            $arr = [];

        }
    
        return $hasil;
    }

    function getDaftarPengirimanAmbilByID($dbgl,$data)
    {
        $responseData = [];

        try {
            $query = "SELECT a.*, to_char(a.tgl_perintah, 'DD Mon YYYY') AS tgl_perintah2 
                        FROM t_distribution_plan_item a 
                        WHERE status_so = '1' 
                        AND freight_type = 'ambil'
                        AND id = $data->id_dt";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }

        for($i=0;$i<count($responseData);$i++) {

            $dpId = $responseData[$i]['id'];

            try {
                $sql = "SELECT 
                            id, a.code_comp, a.type, a.cabang, a.tanggal, a.code, a.name, a.no, a.so_no, a.item_code, a.item_qty, a.item_qty2, 
                            a.keterangan, a.item_ton, a.desa, a.alamat_toko, 0 AS total_do, a.status, a.dept_code, d.name as dept_name
                        FROM t_distribution_plan_list a
                        LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp
                        WHERE a.status = '0' AND a.dp_id = {$data->id_dt}
                        ORDER BY a.so_no";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            $hasil[] = array(
                "id" => $responseData[$i]['id'],
                "no_dp" => $responseData[$i]['no_dp'],
                "tgl_perintah" => $responseData[$i]['tgl_perintah'],
                "tgl_perintah2" => $responseData[$i]['tgl_perintah2'],
                "wh_code" => $responseData[$i]['wh_code'],
                "no_pol" => $responseData[$i]['no_pol'],
                "sopir" => $responseData[$i]['sopir'],
                "item" => $arr
            );
            $arr = [];

        }
    
        return $hasil;
    }

    function getDaftarPengirimanTransfer($dbgl,$data)
    {
        $responseData = [];

        try {

            $query = "SELECT a.*, to_char(a.tgl_kirim, 'DD Mon YYYY') AS tgl_kirim2 FROM t_distribution_plan_item a 
                      WHERE status_so = '1' AND freight_type = 'kirim' AND no_to IS NOT NULL
                      AND a.asal_gudang IN (".$data->where.")";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }

        for($i=0;$i<count($responseData);$i++) {

            $dpId = $responseData[$i]['id'];

            try {
                $sql = "SELECT 
                            id, a.code_comp, a.type, a.cabang, a.tanggal, a.code, a.name, a.no, a.so_no, a.item_code, a.item_qty, a.item_qty2, 
                            a.keterangan, a.item_ton, a.desa, a.alamat_toko, 0 AS total_do, a.status, a.dept_code, d.name as dept_name
                        FROM t_distribution_plan_list a
                        LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp
                        WHERE a.status = '0' AND a.dp_id = {$dpId}
                        ORDER BY a.so_no";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            $hasil[] = array(
                "id" => $responseData[$i]['id'],
                "no_dp" => $responseData[$i]['no_dp'],
                "tgl_kirim" => $responseData[$i]['tgl_kirim'],
                "tgl_kirim2" => $responseData[$i]['tgl_kirim2'],
                "wh_code" => $responseData[$i]['wh_code'],
                "no_pol" => $responseData[$i]['no_pol'],
                "sopir" => $responseData[$i]['sopir'],
                "item" => $arr
            );
            $arr = [];

        }
    
        return $hasil;
    }

    function getDaftarPengirimanTransferByID($dbgl,$data)
    {
        $responseData = [];

        try {

            $query = "SELECT a.*, to_char(a.tgl_kirim, 'DD Mon YYYY') AS tgl_kirim2 
                        FROM t_distribution_plan_item a 
                        WHERE status_so = '1' 
                        AND freight_type = 'kirim' 
                        AND no_to IS NOT NULL
                        AND id = $data->id_dt";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }

        for($i=0;$i<count($responseData);$i++) {

            $dpId = $responseData[$i]['id'];

            try {
                $sql = "SELECT 
                            id, a.code_comp, a.type, a.cabang, a.tanggal, a.code, a.name, a.no, a.so_no, a.item_code, a.item_qty, a.item_qty2, 
                            a.keterangan, a.item_ton, a.desa, a.alamat_toko, 0 AS total_do, a.status, a.dept_code, d.name as dept_name
                        FROM t_distribution_plan_list a
                        LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp
                        WHERE a.status = '0' AND a.dp_id = {$data->id_dt}
                        ORDER BY a.so_no";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            $hasil[] = array(
                "id" => $responseData[$i]['id'],
                "no_dp" => $responseData[$i]['no_dp'],
                "tgl_kirim" => $responseData[$i]['tgl_kirim'],
                "tgl_kirim2" => $responseData[$i]['tgl_kirim2'],
                "wh_code" => $responseData[$i]['wh_code'],
                "no_pol" => $responseData[$i]['no_pol'],
                "sopir" => $responseData[$i]['sopir'],
                "no_to" => $responseData[$i]['no_to'],
                "catatan_to" => $responseData[$i]['catatan_to'],
                "tgl_terima" => $responseData[$i]['tgl_terima'],
                "item" => $arr
            );
            $arr = [];

        }
    
        return $hasil;
    }

    function deleteDistributionPlan2($dbgl,$data)
    {
        $responseData = [];

        for ($i=0; $i < count($data->item); $i++) { 

            $so_no = $data->item[$i]->so_no;

            try {

                $sql = "UPDATE t_distribution_plan_temp2 SET sc_status = '1'
                        WHERE so_no = '$so_no' AND sc_status = '2'";
            
                $stmt = $dbgl->prepare($sql);
                $stmt->execute();
    
                $respon = [
                    "success" => 1,
                    "message" => "Data DP Berhasil Diupdate"
                ];
    
            } catch (PDOException $e) {
                $e->getMessage();
                $respon = [
                    "success" => 0,
                    "message" => "Data DP Gagal Diupdate",
                    "error" => $e
                ];
            }
        }

        try {

            $query = "DELETE FROM t_distribution_plan_item WHERE id = $data->id_dp";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $response1[] = array(
                'result' => 1,
                "message" => "Data Berhasil Di Hapus"
            );

        } catch (PDOException $e) {
            $response1[] = array(
                'result' => 0,
                "message" => $e
            );
        }

        try {

            $query = "DELETE FROM t_distribution_plan_list WHERE dp_id = $data->id_dp";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $response2[] = array(
                'result' => 1,
                "message" => "Data Berhasil Di Hapus"
            );

        } catch (PDOException $e) {
            $response2[] = array(
                'result' => 0,
                "message" => $e
            );
        }

        $arr[] = array(
            'response1' => $response1,
            'response2' => $response2,
        );
    
        return $arr;
    }

    function deleteDistributionPlan($dbgl,$data)
    {
        $responseData = [];

        try {

            $query = "DELETE FROM t_distribution_plan WHERE id = $data->id_dp";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $response1[] = array(
                'result' => 1,
                "message" => "Data Berhasil Di Hapus"
            );

        } catch (PDOException $e) {
            $response1[] = array(
                'result' => 0,
                "message" => $e
            );
        }

        // try {

        //     $query = "DELETE FROM t_distribution_plan_list WHERE dp_id = $data->id_dp";

        //     $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
        //     $response2[] = array(
        //         'result' => 1,
        //         "message" => "Data Berhasil Di Hapus"
        //     );

        // } catch (PDOException $e) {
        //     $response2[] = array(
        //         'result' => 0,
        //         "message" => $e
        //     );
        // }

        $arr[] = array(
            'response1' => $response1,
            'response2' => $response2,
        );
    
        return $arr;
    }

    function getTruck($dbgl,$data)
    {
        $responseData = [];

        try {
            
            $query = "SELECT nopol FROM t_truck WHERE truck_stat='Y' AND cabang IN (".$data->where.")";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }
    
        return $responseData;
    }

    function getSopir($dbgl,$data)
    {
        $responseData = [];

        try {
        
            $query = "SELECT sopir_utama as sopir FROM t_truck WHERE trim(cabang) IN (".$data->where.") GROUP BY sopir_utama";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }
    
        return $responseData;
    }

    function getGudang($dbgl,$data)
    {
        $responseData = [];

        try {

            $query = "SELECT TRIM(a.wh_code) AS wh_code, a.wh_office FROM in_warehouse a 
                        INNER JOIN in_warehouseuser b ON a.wh_code = b.wh_code AND b.userid = '".strtoupper($data->username)."'
                        WHERE a.wh_status = '1'";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }
    
        return $responseData;
    }

    function updateDistributionPlan2($dbgl,$data)
    {
        $respon = [];
        $id_dp = $data->dt_so->id;

        $tgl_perintah = "";

        try {

            $sql = "SELECT tgl_perintah FROM t_distribution_plan_item
                    WHERE id = '$id_dp'";
        
            $rows = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);

            $tgl_perintah = $rows[0]['tgl_perintah'];

        } catch (PDOException $e) {
            $e->getMessage();
            $tgl_perintah = [
                "success" => 0,
                "message" => $e
            ];
        }

        try {

            $sql = "UPDATE t_distribution_plan_item SET
                        no_pol = '$data->nopol_truck',
                        sopir = '$data->sopir_name',
                        wh_code = '$data->gudang',
                        tgl_kirim = '$data->tgl_kirim',
                        catatan = '$data->alasan',
                        ritase = '$data->ritase',
                        status_so = '1'
                    WHERE id = '$id_dp'";
        
            $stmt = $dbgl->prepare($sql);
            $stmt->execute();

            $respon = [
                "success" => 1,
                "message" => "Data DP Berhasil Diupdate"
            ];

        } catch (PDOException $e) {
            $e->getMessage();
            $respon = [
                "success" => 0,
                "message" => $e
            ];
        }

        return $respon;
    }

    function updateDistributionPlan($dbgl,$data)
    {

        $gdgTransfer    = isset($data->gudangtransfer) ? $data->gudangtransfer : null;
        $gudang         = $data->gudang;
        $tglKirim       = date('Y-m-d', strtotime($data->tglkirim));
        $ritasePlan     = ($data->ritaseplan == "") ? null : $data->ritaseplan;
        $catatan        = $data->catatan;
        $id             = $data->id;
        $sopir          = $data->sopir;
        $noPol          =  preg_replace('/[^A-Za-z0-9\-]/', '', $data->no_pol);
        $response       = [];
    
        $query = "UPDATE t_distribution_plan SET tgl_kirim = ?, wh_code = ?, no_pol = ?, sopir = ?, ritase = ?, catatan = ?, no_to = ? WHERE id = ?";
        $stmt = $dbgl->prepare($query);
        $updateData = [$tglKirim, $gudang, $noPol, $sopir, $ritasePlan, $catatan, $gdgTransfer, $id];
    
        try {
            $stmt->execute($updateData);
            $response = [
                'id' => $id,
                'message' => true,
                'response' => 'Update data berhasil'
            ];
        } catch (PDOException $e) {
            $response = [
                'id' => $id,
                'message' => false,
                'response' => 'Gagal menyimpan data, silahkan coba kembali'
            ];
        }

        // try {
        //     $query1 = "UPDATE t_dp_plan SET delivery_time = '$tglKirim' WHERE dp_id = '$id'";
        //     $stmt2 = $dbgl->prepare($query1);
        //     $stmt2->execute();
        // } catch (PDOException $e) {
        //     $err = $e;
        // }
    
        return $response;
    }

    function updateDistributionPlanDetail($dbgl,$data)
    {
        $respon = [];
        $id_dp = $data->dt_so;

        try {

            $sql = "UPDATE t_distribution_plan SET
                        no_pol = '$data->nopol_truck',
                        sopir = '$data->sopir_name',
                        wh_code = '$data->gudang',
                        tgl_terima = '$data->tgl_terima',
                        -- tgl_kirim = '$data->tgl_kirim',
                        catatan = '$data->alasan',
                        no_to = '$data->no_to',
                        catatan_to = '$data->catatan_to',
                        ritase = '$data->ritase'
                    WHERE id = '$id_dp'";
        
            $stmt = $dbgl->prepare($sql);
            $stmt->execute();

            $respon = [
                "success" => 1,
                "message" => true,
                "pesan" => "Data DP Berhasil Diupdate"
            ];

        } catch (PDOException $e) {
            $e->getMessage();
            $respon = [
                "success" => 0,
                "message" => false,
                "pesan" => $e
            ];
        }

        return $respon;
    }

    function updateDistributionPlanAmbil($dbgl,$data)
    {
        $respon = [];
        $id_dp = $data->dt_so->id;

        $tgl_perintah = "";

        try {

            $sql = "SELECT tgl_perintah FROM t_distribution_plan_item
                    WHERE id = '$id_dp'";
        
            $rows = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);

            $tgl_perintah = $rows[0]['tgl_perintah'];

        } catch (PDOException $e) {
            $e->getMessage();
            $tgl_perintah = [
                "success" => 0,
                "message" => $e
            ];
        }

        try {

            $sql = "UPDATE t_distribution_plan_item SET
                        no_pol = '$data->nopol_truck',
                        sopir = '$data->sopir_name',
                        wh_code = '$data->gudang',
                        tgl_kirim = '$tgl_perintah',
                        status_so = '1'
                    WHERE id = '$id_dp'";
        
            $stmt = $dbgl->prepare($sql);
            $stmt->execute();

            $respon = [
                "success" => 1,
                "message" => "Data DP Berhasil Diupdate"
            ];

        } catch (PDOException $e) {
            $e->getMessage();
            $respon = [
                "success" => 0,
                "message" => $e
            ];
        }

        return $respon;
    }

    function updateDistributionPlanAmbil2($dbgl,$data)
    {
        $respon = [];
        $id_dp = $data->dt_so;

        $tglKirim = date('Y-m-d');

        // try {

        //     $sql = "SELECT tgl_kirim FROM t_distribution_plan
        //             WHERE id = '$id_dp'";
        
        //     $rows = $dbgl->query($sql)->fetch(PDO::FETCH_ASSOC);

        //     $tgl_perintah = $rows['tgl_kirim'];

        // } catch (PDOException $e) {
        //     $e->getMessage();
        //     $tgl_perintah = [
        //         "success" => 0,
        //         "message" => $e
        //     ];
        // }

        try {

            $sql = "UPDATE t_distribution_plan SET
                        no_pol = '$data->nopol_truck',
                        sopir = '$data->sopir_name',
                        wh_code = '$data->gudang',
                        tgl_kirim = '$tglKirim'
                    WHERE id = '$id_dp'";
        
            $stmt = $dbgl->prepare($sql);
            $stmt->execute();

            $respon = [
                "success" => 1,
                "message" => "Data DP Berhasil Diupdate"
            ];

        } catch (PDOException $e) {
            $e->getMessage();
            $respon = [
                "success" => 0,
                "message" => $e
            ];
        }

        return $respon;
    }

    function getNoTransfer($dbgl,$data)
    {
        $prd = date("Ym");
        $whCode = trim($data->wh_code);
        $now = date('Y-m-d');
        $before = date('Y-m-d', strtotime("-7 days {$now}"));
        $responseData = [];

        // if (isGudangTransfer($dbgl,$data) > 0) {
        //     $query = "SELECT ref_no, item_code, item_qty FROM in_trans WHERE wh_code = '{$whCode}' AND record_id = 'F' AND in_code IN ('TI','RTR') AND (DATE(voucher_date) BETWEEN '{$before}' AND '{$now}')";
        //     try {
        //         $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
        //         $responseData = $rows;
        //     } catch(PDOException $e) {
        //         $responseData = $e;
        //     }
        // }

        $query = "SELECT ref_no, item_code, item_qty 
                  FROM in_trans 
                  WHERE wh_code = '{$whCode}' 
                  AND record_id = 'F' AND in_code IN ('TI','TO','RTR') 
                  AND (DATE(voucher_date) BETWEEN '{$before}' AND '{$now}')
                  ORDER BY ref_no";
        try {
            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;
        } catch(PDOException $e) {
            $responseData = $e;
        }
        
        return $responseData;
        // return $query;
    }

    function getNoTransferByGudang($dbgl,$data)
    {
        $whCode = trim($data->wh_code);
        //get data transfer -7 days
        $now = date('Y-m-d');
        $before = date('Y-m-d', strtotime("-7 days {$now}"));

        if (isGudangTransfer($dbgl,$data) > 0) {
            $query = "SELECT ref_no, item_code, item_qty FROM in_trans WHERE wh_code = '{$whCode}' AND record_id = 'F' AND in_code IN ('TI','RTR') AND (DATE(voucher_date) BETWEEN '{$before}' AND '{$now}')";
            try {
                $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
                $responseData = $rows;
            } catch(PDOException $e) {
                $responseData = $e;
            }
        }

        return $responseData;
    }

    function isGudangTransfer($dbgl,$data)
    {
        $whCode = trim($data->wh_code);
        $query = "SELECT COUNT(*) FROM in_warehouse WHERE wh_code = '{$whCode}' AND capacity = -2";
        $result = $dbgl->query($query)->fetchColumn();
        return $result;
    }

    function updateTransferDPKirim($dbgl,$data)
    {
        $respon = [];
        $id_dp = $data->dt_so->id;

        try {

            $sql = "UPDATE t_distribution_plan_item SET
                        no_pol = '$data->nopol_truck',
                        sopir = '$data->sopir_name',
                        wh_code = '$data->gudang',
                        no_to = '$data->no_to',
                        catatan = '$data->alasan',
                        tgl_kirim = '$data->tgl_kirim',
                        ritase = '$data->ritase',
                        status_so = '1'
                    WHERE id = '$id_dp'";
        
            $stmt = $dbgl->prepare($sql);
            $stmt->execute();

            $respon = [
                "success" => 1,
                "message" => "Data DP Berhasil Diupdate"
            ];

        } catch (PDOException $e) {
            $e->getMessage();
            $respon = [
                "success" => 0,
                "message" => $e
            ];
        }

        return $respon;
    }

    function updateProsesTransferDPKirim($dbgl,$data)
    {
        $respon = [];
        $id_dp = $data->dt_so->id;

        try {

            $sql = "UPDATE t_distribution_plan_item SET
                        catatan_to = '$data->catatan_to',
                        tgl_terima = '$data->tgl_terima'
                    WHERE id = '$id_dp'";
        
            $stmt = $dbgl->prepare($sql);
            $stmt->execute();

            $respon = [
                "success" => 1,
                "message" => "Data DP Berhasil Diupdate"
            ];

        } catch (PDOException $e) {
            $e->getMessage();
            $respon = [
                "success" => 0,
                "message" => $e
            ];
        }

        return $respon;
    }

    function getKategori($dbgl,$data)
    {
        $responseData = [];
        $respon = [];

        try {
        
            $query = "SELECT category FROM v_username_category WHERE username = '$data->username' ORDER BY category";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

            $respon = [
                "success" => 1,
                "message" => "Data Berhasil Ditampilkan",
                "data" => $responseData
            ];

        } catch (PDOException $e) {
            $respon = [
                "success" => 0,
                "message" => "Data Gagal Ditampilkan",
                "data" => $e
            ];
        }
    
        return $respon;
    }

    function getCabang($dbgl,$data)
    {
        $responseData = [];
        $respon = [];

        try {
        
            $query = "SELECT trim(coce_code) AS coce_code FROM coce_codeuser WHERE username = '$data->username' AND code_comp = '$data->category'
                        AND trim(coce_code) <> 'PLR' 
                        GROUP BY trim(coce_code) ORDER BY trim(coce_code)";

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

            $respon = [
                "success" => 1,
                "message" => "Data Berhasil Ditampilkan",
                "data" => $responseData
            ];

        } catch (PDOException $e) {
            $respon = [
                "success" => 0,
                "message" => "Data Gagal Ditampilkan",
                "data" => $e
            ];
        }
    
        return $respon;
    }

    function getGudangOutstanding($dbgl,$data,$dbh)
    {
        $datax = [];
        $respon = [];
        $rows = [];

        if (isset($data->username)) {

            $username = $data->username;
            $modEntry = isset($data->entry) ? $data->entry : 0;
            $modFisik = isset($data->fisik) ? $data->fisik : 0;
            $region = isset($data->region) ? $data->region : 0;
            $coce_code = trim($data->coce_code);
            $menu_code = isset($data->menu_code) ? $data->menu_code : null;
            $acc_approved = get_approved_acc('SAA', $menu_code, $dbh);

            if ($acc_approved && $modFisik) $modFisik = 0;

            $where = " A.wh_status = '1' ";
            if ($modFisik) $where .= " AND capacity > 0 ";
            if ($region) $where .= " AND wh_loc = '$region' ";
            if ($menu_code=='invdispatch'||$menu_code=='invmttruck') $where .= " AND dispatch_status = '1' ";
            if ($coce_code != '') {
                if ($coce_code == 'ALL') {
                    $where .= " AND 1=1 ";
                } elseif ($coce_code == 'SMD') {
                    $where .= " AND (A.wh_coce = 'SMD' OR A.wh_coce = 'PLR') ";
                } elseif ($coce_code == 'BWI') {
                    // $where = " (A.wh_coce = 'BWI' OR A.wh_coce = 'DPS' OR A.wh_coce = 'KLK' OR A.wh_coce = 'NGR' OR A.wh_coce = 'SIN' OR A.wh_coce = 'TBN') ";
                    $where = " (A.wh_coce = 'BWI' OR A.wh_coce = 'GYR' OR A.wh_coce = 'KLK' OR A.wh_coce = 'NGR' OR A.wh_coce = 'SIN' OR A.wh_coce = 'TBN') ";
                } else {
                    $where .= " AND A.wh_coce = '$coce_code' ";
                }
            }

            try {
                $sql = "
                    SELECT trim(A.wh_code) AS wh_code FROM in_warehouse A INNER JOIN in_warehouseuser B ON trim(A.wh_code) = trim(B.wh_code)
                    WHERE ".$where." AND B.userid = upper('$username') ORDER BY trim(A.wh_code)
                ";
                                
                $stmt = $dbh->prepare($sql);
                $stmt->execute();
                $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $stmt->closeCursor();

                $datax[] = $rows;
                // if (!$modEntry) $datax[0][count($rows)+1]['wh_code'] = 'ALL';
                
                // if (in_array($coce_code, ['SMD','TGT'])) {
                //     $data['KTITR'] = ['KTITR'];
                // } elseif (in_array($coce_code, ['BMA','SBW'])) {
                //     $data['NTBTR'] = ['NTBTR'];
                // }

                // asort($data);

                $respon = $datax;
            
            } catch (PDOException $e) {
                $respon = $e;
            }

        }

        return $respon;
    }

    function getOutstanding($dbgl,$data,$dbh)
    {
        $row = [];
        $shift = isset($data->category) ? $data->category : ''; $desc = '';
        $coce_code = isset($data->cabang) ? $data->cabang : ''; $kode_cab = $coce_code;
        $wh_code = isset($data->gudang) ? $data->gudang : ''; $kode_wh = $wh_code;
        $ledger = isset($data->ledger) ? $data->ledger : NULL;
        $report = isset($data->report) ? $data->report : NULL;
        $date2 = isset($data->periode) ? $data->periode : date("Y-m-d");

        if ($kode_cab == 'ALL') {
            ini_set("memory_limit", "5120M");
        } else {
            ini_set("memory_limit", "2048M"); 
        } 

        if (empty($ledger)) {
            $ledger = 'ALL';
        }
        $kode_ledger = $ledger;
        $list_coce = list_coce($dbgl,$username);

        // userlogs
        // $datalogs = new stdClass();
        // $datalogs->dbh = $dbh;
        // $datalogs->menu_code = $menu_code;
        // $datalogs->statuslogs = strtoupper($shift." - ".$kode_cab." - ".$kode_wh." - ".$report." - ".$date2);
        // userlogs($datalogs);

        $dbgl->query("DROP TABLE IF EXISTS tmp_po_req_group");
        $dbgl->query("DROP TABLE IF EXISTS tmp_po_req,tmp_po_req2,tmp_po_req3");
        $dbgl->query("DROP TABLE IF EXISTS tmp_t_card");
        $dbgl->query("DROP TABLE IF EXISTS tmp_in_trans");
        $dbgl->query("DROP TABLE IF EXISTS tmp_po_req_sum");
        $dbgl->query("DROP TABLE IF EXISTS salesoutstanding");

        $whereTCard = " WHERE code_comp = upper('{$shift}')";
        if ($kode_cab == 'ALL') {
            $whereTCard .= " AND coce_code = ANY('{" . implode(',', $list_coce) . "}'::text[]) ";
        } else {
            if ($kode_cab == 'SMD') {
                $whereTCard .= " AND (coce_code = 'SMD' OR coce_code = 'PLR') ";
            } elseif ($kode_cab == 'GYR') {
                // if ($area_code == 'ALL') {
                    $whereTCard .= " AND (coce_code = 'DPS' OR coce_code = 'GYR') ";
                // } else {
                //     $whereTCard .= " AND (coce_code = 'DPS' OR coce_code = 'GYR') AND area_code = '$area_code' ";
                // }
            } else {
                $whereTCard .= " AND coce_code = '{$kode_cab}' ";
            }
        }

        $query = "SELECT code, name, coce_code, area_code, group_code INTO TEMPORARY tmp_t_card FROM t_card {$whereTCard}";
        $dbgl->query($query);
        $query = "
        SELECT 
            A.voucher_prd, A.voucher_doc, A.voucher_no, A.wh_code, TRIM(A.remark) as remark, A.ref_no, 
            UPPER(A.item_code) as item_code, A.item_qty, A.in_type, A.update_date, A.voucher_date, 
            A.car_no, A.driver, A.received_time 
            INTO TEMPORARY tmp_in_trans 
        FROM in_trans A
        INNER JOIN tmp_t_card B ON A.code = B.code
        WHERE A.code_comp = upper('{$shift}') AND A.voucher_date <= '{$date2}' AND A.in_stat != 'C'";
        $dbgl->query($query);

        //total qty item per SO
        $wherePoReq = " WHERE A.comp_code = 'SAKA00' AND A.code_comp = upper('{$shift}') AND A.pr_type = 'S' AND A.pr_date <= '{$date2}' AND A.pr_stat = 'A'";
        if ($kode_wh != 'ALL') {
            $wherePoReq .= " AND A.kp_code = '{$kode_wh}' ";
        }

        if ($ledger != '' && $ledger != 'ALL') {
            $wherePoReq .= " AND (vend_code = '{$ledger}' OR dept_code = '{$ledger}') ";
        }
        $query = "
            SELECT 
                A.pr_no, A.item_code, SUM(A.item_qty) AS item_qty 
                INTO TEMPORARY tmp_po_req
            FROM po_req A
            INNER JOIN tmp_t_card B ON A.vend_code = B.code
            {$wherePoReq}
            GROUP BY A.pr_no, A.item_code
        ";
        $dbgl->query($query);

        //total qty realisasi DO per SO
        $query = "
            SELECT
                A.remark, A.item_code,
                (SUM(CASE WHEN A.in_type = 'O' THEN A.item_qty ELSE 0 END) - SUM(CASE WHEN A.in_type = 'I' THEN A.item_qty ELSE 0 END)) AS do_sum
                INTO TEMPORARY tmp_in_trans_sum
            FROM tmp_in_trans A
            GROUP BY A.remark, A.item_code
        ";
        $dbgl->query($query);

        //SO masih Outstanding
        $query = "
            SELECT
                A.*, (A.item_qty - coalesce(B.do_sum, 0)) AS sisa, B.do_sum 
                INTO TEMPORARY tmp_so_outs
            FROM tmp_po_req A
            LEFT JOIN tmp_in_trans_sum B
                ON (A.pr_no = B.remark) AND (A.item_code = B.item_code)
        ";
        $dbgl->query($query);

        //detail realisasi DO
        $query = "
            SELECT
                A.voucher_prd, A.voucher_doc, A.voucher_no, A.wh_code, A.ref_no, A.item_code AS item_code_in, 
                A.item_qty AS do_qty, A.in_type, A.update_date,
                TO_CHAR(A.voucher_date, 'YYYY-MM-DD') AS do_date, A.car_no, A.driver, 
                TO_CHAR(A.received_time, 'YYYY-MM-DD HH24:MI:SS') AS received_time, B.pr_no AS remark
                INTO TEMPORARY tmp_realisasi_so_outs
            FROM tmp_in_trans A
            INNER JOIN tmp_so_outs B
                ON (A.remark = B.pr_no) AND (A.item_code = B.item_code)
            WHERE B.sisa <> 0
        ";
        $dbgl->query($query);

        $query = "
            SELECT 
                A.pr_no, (A.pr_date)::date AS pr_date, A.vend_code, A.dept_code, A.sales_code, A.pr_stat, 
                    A.kp_code, upper(A.pr_desc) AS pr_desc, A.price_amt, A.pending_stat, A.code_comp,
                    C.item_code, C.item_qty, C.do_sum
                    INTO TEMPORARY tmp_po_req2
                FROM po_req A INNER JOIN tmp_so_outs C 
                    ON A.code_comp = upper('{$shift}') AND A.pr_no = C.pr_no AND A.item_code = C.item_code
        ";
        $dbgl->query($query);

        $query = "
            SELECT A.*, B.name, B.coce_code INTO TEMPORARY tmp_po_req3
                FROM tmp_po_req2 A INNER JOIN t_card B 
                    ON A.vend_code = B.code AND A.code_comp = B.code_comp
        ";
        $dbgl->query($query);

        $query = "
            SELECT 
                A.*, D.* INTO TEMPORARY salesoutstanding
                FROM tmp_po_req3 A LEFT JOIN tmp_realisasi_so_outs D 
                    ON A.pr_no = D.remark AND A.item_code = D.item_code_in 
        ";
        $query .= " ORDER BY A.vend_code, A.pr_no, do_date";
        $dbgl->query($query);

        $sql = "SELECT * FROM salesoutstanding WHERE (item_qty - COALESCE(do_sum, 0)) <> 0 ";
        $stmt = $dbgl->query($sql);
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ( $stmt->rowCount() ) {
            $stmt = $dbh->prepare("DELETE FROM data_json WHERE lower(menu_code)=lower(?) AND lower(username)=lower(?)");
            $stmt->execute([$menu_code,$username]);
            $stmt = $dbh->prepare("INSERT INTO data_json (menu_code,data,username) VALUES(?,?,?) RETURNING id");
            try {
                $stmt->execute([$menu_code,json_encode($rows),$username]);
                $stmt->bindColumn('id', $id);
                $stmt->fetch();
            } catch (PDOException $e) {
                // echo $e;
            }
        }

        $query = "SELECT code, name FROM v_autoledger ORDER BY code";
        $stmt = $dbh->prepare($query);
        $stmt->execute();
        $rowx = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $arrayName = array();
        foreach ($rowx as $row) {
            $arrayName[$row['code']] = $row['name'];
        }

        $no = 0;
        $sum_do_qty = 0;
        $sum_do_sum = 0;
        $sum_so_qty = 0;
        $sum_outs_do = 0;
        $temp_pr_no = '';

        foreach ($rows as $row) {
            if (($row["item_qty"]-$row["do_sum"]) == 0)
            {
                continue;
            }
            $txt_wh_code = $row["wh_code"];
            $voucher_prd = $row["voucher_prd"];
            $voucher_doc = $row["voucher_doc"];
            $voucher_no  = $row["voucher_no"];
            $pr_no       = $row["pr_no"];
            $item_code   = $row["item_code"];

            if (isset($temp_pr_no) && $temp_pr_no==$pr_no)
            {
                if ($report != 'detail')
                {                       
                    continue;
                }
                $vend_code  = "";
                $name       = "";
                $sales_code = "";
                $pr_date    = "";
                $pr_stat    = "";
                $kp_code    = "";
                $txt_pr_no  = "";
                if ($shift == "saa")
                {
                    $txt_item_code  = "";
                    $item_qty       = 0;
                } else {
                    $txt_item_code  = $row["item_code"];
                    $item_qty       = $row["item_qty"];
                    $outs_do       = 0;
                }
            } else {
                $no++;
                $temp_pr_no    = $pr_no;
                $vend_code     = $row["vend_code"];
                $dept_code     = trim($row["dept_code"]);
                $name          = $arrayName[$vend_code];
                if (strpos($name,'PROMOSI') !== false) {
                    if ($dept_code!='') { 
                        $dept_name = $arrayName[$dept_code];
                    } else {
                        $dept_name = '';
                    }
                    $name = $dept_name.' - PROMOSI'; 
                }
                $sales_code    = $row["sales_code"];
                $pr_date       = $row["pr_date"];
                $txt_pr_no     = $row["pr_no"];
                // $car_no        = $row["car_no"];
                $txt_item_code = $row["item_code"];
                $item_qty      = $row["item_qty"];
                $do_sum        = $row["do_sum"];
                $outs_do       = $row["item_qty"]-$row["do_sum"];
                $pr_stat       = $row['pr_stat'];
                $kp_code       = $row['kp_code'];
                $pending_stat  = $row['pending_stat'];
            }
            $car_no        = $row["car_no"];
            $txt_coce_code = $row["coce_code"];  // echo $txt_coce_code;
            $pr_desc   = $row["pr_desc"];
            $do_date   = $row["do_date"];
            $ref_no    = $row["ref_no"];
            $item_code_in = $row["item_code_in"];
            $do_qty    = $row["do_qty"];
            $price_amt = $row["price_amt"];         //20150812
            $action = 'invtranout';
            if ($row["in_type"] == "I")
            {
                $do_qty = -1 * $do_qty;
                $action = 'invtranin';
            }

            $array_footer[] = array(
                'item_code'   => $txt_item_code,
                'item_qty'    => $item_qty,
                'do_sum'      => $do_sum,
                'outs_do'     => $outs_do
            );

            if ($item_qty == 0) {
                $xitem_qty = "";
            } else {
                $xitem_qty = number_format($item_qty,2);
            }

            if ($do_qty == 0) {
                $xdo_qty = "";
            } else {
                $xdo_qty = number_format($do_qty,2);
            }

            $xdo_sum = number_format($do_sum,2);
            $xouts_do = number_format($outs_do,2);

            //SUSUN ULANG 1
            $hasil['outstanding'][] = array(
                "no" => $no,
                "kode_cab" => $kode_cab,
                "vend_code" => $vend_code,
                "name" => $name,
                "pr_date" => $pr_date,
                "txt_pr_no" => $txt_pr_no,
                "kp_code" => $kp_code,
                "txt_item_code" => $txt_item_code,
                "item_qty" => $xitem_qty,
                "report" => $report,
                "do_date" => $do_date,
                "ref_no" => $ref_no,
                "car_no" => $car_no,
                "do_qty" => $xdo_qty,
                "do_sum" => $xdo_sum,
                "outs_do" => $xouts_do,
                "pr_desc" => $pr_desc
            );

            $sum_do_qty += $do_qty;
            $sum_do_sum += $do_sum;
            $sum_so_qty += $item_qty;
            $sum_outs_do += $outs_do;
        }

        if (isset($array_footer)) {
            $sum_array_footer = array(); $i = 0;
            foreach ($array_footer as $data) {
                $i = $data['item_code'];
                if (!array_key_exists($i, $sum_array_footer)) {
                    $sum_array_footer[$i] = array(
                        'item_code' => $data['item_code'],
                        'item_qty'  => $data['item_qty'],
                        'do_sum'    => $data['do_sum'],
                        'outs_do'   => $data['outs_do']
                    );
                } else {
                    $sum_array_footer[$i]['item_qty'] = $sum_array_footer[$i]['item_qty'] + $data['item_qty'];
                    $sum_array_footer[$i]['do_sum'] = $sum_array_footer[$i]['do_sum'] + $data['do_sum'];
                    $sum_array_footer[$i]['outs_do'] = $sum_array_footer[$i]['outs_do'] + $data['outs_do'];
                }
                $i++;
            }
            sort($sum_array_footer);

            foreach ($sum_array_footer as $x=>$v)
            {
                if ($sum_array_footer[$x]['item_code']==''){continue;}
                //SUSUN ULANG TOTAL
                $hasil['total_outstanding'][] = array(
                    "item_code" => $sum_array_footer[$x]['item_code'],
                    "item_qty" => number_format($sum_array_footer[$x]['item_qty'],2),
                    "do_sum" => number_format($sum_array_footer[$x]['do_sum'],2),
                    "outs_do" => number_format($sum_array_footer[$x]['outs_do'],2)
                );
            }
        }

        $hasil['grand_total_outstanding'][] = array(
            "sum_so_qty" => number_format($sum_so_qty,2),
            "sum_do_qty" => number_format($sum_do_qty,2),
            "sum_do_sum" => number_format($sum_do_sum,2),
            "sum_outs_do" => number_format($sum_outs_do,2)

        );
        
        return $hasil;
    }

    function getForceClose2($dbgl,$data,$dbh)
    {

        $shift = $data->shift;
        $status = $data->status;
        $cari = strtoupper(trim($data->cari));
        $date1 = $data->date1;
        $date2 = $data->date2;

            // userlogs
            $datalogs = new stdClass();
            $datalogs->dbh = $dbh;
            $datalogs->menu_code = $menu_code;
            $datalogs->statuslogs = strtoupper($shift." - ".$status." - ".$cari." - ".$date1." - ".$date2);
            userlogs($datalogs);

        if ($shift == 'saa')
        {
            $desc = "Semen";
            $company = "PT. Saka Agung Abadi";
        } else {
            $desc = "Non Semen";
            $company = "PT. Saka NiagaSukses Abadi";
        }

        $sql = "SELECT A. menuprg, A.user_id, A.event_code, A.event_desc, A.reason,
                TO_CHAR(A.event_date, 'YYYY-MM-DD HH24:MI:SS') AS event_date, C.name
                FROM s_audit_gab A
                LEFT JOIN t_card C
                    ON RIGHT(A.event_desc, 10) = C.code AND A.code_comp = C.code_comp
                WHERE A.code_comp = upper('$shift') AND (A.event_date::date BETWEEN '$date1' AND '$date2') 
                    AND (A.menuprg = 'SO111' OR A.menuprg = 'SO141') ";
                if ($status != 'ALL') {
                    $sql .= " AND A.event_code = '$status' ";
                }
                if ($cari != '') {
                    $sql .= " AND (A.reason LIKE '%$cari%' OR A.event_desc LIKE '%$cari%' OR A.user_id LIKE '%$cari%') ";
                    // $sql .= " AND ('$cari' LIKE '%' || A.event_desc || '%' OR '$cari' LIKE '%' || A.user_id || '%') ";
                    // $sql .= " AND ('$cari' LIKE '%' || A.event_desc || '%' OR A.user_id LIKE '%$cari%') ";
                }
        $sql .= " ORDER BY A.event_date, A.menuprg, A.event_desc, A.event_code ";

        $stmt = $dbh->prepare($sql); $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        $dataJson = array();

        if (isset($rows))
        {
            $no = 1;
            foreach ($rows as $row)
            {
                $menuprg    = TRIM($row['menuprg']);
                if($menuprg=="SO111"){
                    $menuprgText = "Sales Order";
                }else if($menuprg=="SO141"){
                    $menuprgText = "Sales Invoice";
                }
                $user_id    = $row['user_id'];
                $event_code = $row['event_code'];
                $event_date = $row['event_date'];
                $event_desc = $row['event_desc'];
                // if ($cari != '') {
                //     if (strpos($event_desc,$cari) === false) { 
                //         continue; 
                //     }
                // }
                $reason     = trim($row['reason']);

                $dataJson[] = array(
                    'no' => $no++,
                    'event_date' => $event_date,
                    'menuprg' => $menuprgText,
                    'user_id' => $user_id,
                    'event_code' => $event_code,
                    'event_desc' => $event_desc,
                    'reason' => $reason
                );
            }
        }

        return $dataJson;
    }

    function getForceClose($dbgl,$data,$dbh)
    {

        $sum_so_qty = 0;
        $sum_do_qty = 0;
        $sum_outs = 0;

        $shift = isset($data->shift) ? $data->shift : NULL;
        $coce_code = isset($data->coce_code) ? $data->coce_code : NULL; $kode_cab = $coce_code;
        $sales_code = isset($data->sales_code) ? $data->sales_code : NULL;
        $wh_code = isset($data->wh_code) ? $data->wh_code : NULL; $kode_wh = $wh_code;
        $tanggal1 = isset($data->date1) ? $data->date1 : date("Y-m-d",strtotime("first day of this month")); $date1 = $tanggal1;
        $tanggal2 = isset($data->date2) ? $data->date2 : date("Y-m-d"); $date2 = $tanggal2; $today = date("Y-m-d");
        $toDate = date("Y-m-d", strtotime("last day of next month", strtotime($date2)));
        $ledger = isset($data->ledger) ? $data->ledger : NULL; if (isset($ledger)&&!empty($ledger)) $accUpdate = true;
        $report = isset($data->report) ? $data->report : NULL;
        $status = "W";

        if ($coce_code != 'ALL') {
            $and_cabang = "AND A.spk_no = '$coce_code'";
        } else {
            $and_cabang = "";
        }

        if ($sales_code != 'ALL') {
            $and_sales_code = "AND trim(a.sales_code) = '$sales_code'";
        } else {
            $and_sales_code = "";
        }

        if ($ledger != null) {
            $and_ledger = "AND A.vend_code = '$ledger'";
        } else {
            $and_ledger = "";
        }

        if ($shift == 'saa') {
            // $total_qty = " ROUND(SUM(A.item_qty,2)) AS item_qty,";
            $total_qty = " SUM(A.item_qty) AS item_qty,";
        } else {
            $total_qty = " ROUND(A.item_qty,2) AS item_qty,";
        }
        
        

        $sql = "SELECT 
                    A.spk_no, 
                    A.vend_code,  
                    (SELECT b.name FROM in_trans b WHERE b.remark = A.pr_no AND b.in_type = 'O' AND b.item_code = A.item_code) AS name,
                    A.pr_no, 
                    A.item_code, 
                    $total_qty
                    (SELECT b.ref_no FROM in_trans b WHERE b.remark = A.pr_no AND b.in_type = 'O' AND b.item_code = A.item_code) AS ref_no_out,
                    ((SELECT ROUND(SUM(b.item_qty),2) FROM in_trans b WHERE b.remark = A.pr_no AND b.in_type = 'O' AND b.item_code = A.item_code) - (SELECT ROUND(SUM(b.item_qty),2) FROM in_trans b WHERE b.remark = A.pr_no AND b.in_type = 'I' AND b.item_code = A.item_code)) AS sum_out,
                    (SELECT ROUND(SUM(b.item_qty),2) FROM in_trans b WHERE b.remark = A.pr_no AND b.in_type = 'I' AND b.item_code = A.item_code) AS sum_in,
                    A.pr_desc,
                    A.code_comp,
                    A.entry_by,
                    A.pr_date
                FROM po_req A
                INNER JOIN t_card B ON A.vend_code = B.code
                WHERE A.comp_code = 'SAKA00' 
                AND A.pr_type = 'S' 
                AND A.item_qty2 > 0
                AND A.code_comp = UPPER('$shift')
                AND A.pr_date <= '$date2'
                AND 1=1 
                AND A.pr_stat = 'A' 
                AND trim(A.pending_stat) = 'F'
                $and_cabang
                $and_sales_code
                $and_ledger
                GROUP BY A.spk_no, A.vend_code, A.pr_no, A.item_code, A.item_qty, A.pr_desc,A.entry_by,A.code_comp,A.pr_date";

        try {

            $rows = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }

        for($i=0;$i<count($responseData);$i++) {

            $pr_no = $responseData[$i]['pr_no'];
            $item_code = $responseData[$i]['item_code'];
            $listPrNo = implode(',', array_map('array_pop', $pr_no));

            try {
                $sql = "SELECT b.ref_no, b.voucher_desc, b.car_no, b.driver 
                        FROM in_trans b 
                        WHERE b.remark = '$pr_no'
                        AND b.in_type = 'I' 
                        AND b.item_code = '$item_code'";
                $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                $e->getMessage();
                $arr = $e; 
            }

            try {
                $sql2 = "SELECT trim(event_desc) AS pr_no,trim(user_id) AS user_id,reason
                            FROM s_audit_gab 
                            WHERE menuprg = 'SO111' 
                            AND event_code = 'REQUEST CLOSED' 
                            AND code_comp = upper('$shift') 
                            AND trim(event_desc) = '$pr_no' 
                            ORDER BY event_date DESC";
                $rowx = $dbh->query($sql2)->fetchAll(PDO::FETCH_ASSOC);
            } catch (PDOException $th) {
                $th->getMessage();
                $rowx = $th;
            }

            $hasil[] = array(
                "spk_no" => $responseData[$i]['spk_no'],
                "vend_code" => $responseData[$i]['vend_code'],
                "name" => $responseData[$i]['name'],
                "pr_no" => $responseData[$i]['pr_no'],
                "item_code" => $responseData[$i]['item_code'],
                "item_qty" => $responseData[$i]['item_qty'],
                "ref_no_out" => $responseData[$i]['ref_no_out'],
                "sum_out" => $responseData[$i]['sum_out'],
                "sum_in" => $responseData[$i]['sum_in'],
                "pr_desc" => $responseData[$i]['pr_desc'],
                "pr_date" => $responseData[$i]['pr_date'],
                "entry_by" => $responseData[$i]['entry_by'], 
                "code_comp" => $responseData[$i]['code_comp'],
                "item" => $arr,
                "reason" => $rowx
            );
            $arr = [];

            $sum_so_qty += $responseData[$i]['item_qty'];
            $sum_do_qty += $responseData[$i]['sum_out'];
            $sum_outs += $responseData[$i]['sum_in'];

        }

        $dt_force[] = array(
            "data_fc" => $hasil,
            "sum_so_qty" => $sum_so_qty,
            "sum_do_qty" => $sum_do_qty,
            "sum_outs" => $sum_outs,
            "sql" => $sql
        );

        return $dt_force;
        // return $sql;
        
    }

    function getForceCloseAprove($dbgl,$data,$dbh)
    {
        $shift = isset($data->shift) ? $data->shift : NULL;
        $coce_code = isset($data->coce_code) ? $data->coce_code : NULL; $kode_cab = $coce_code;
        $sales_code = isset($data->sales_code) ? $data->sales_code : NULL;
        $wh_code = isset($data->wh_code) ? $data->wh_code : NULL; $kode_wh = $wh_code;
        $tanggal1 = isset($data->date1) ? $data->date1 : date("Y-m-d",strtotime("first day of this month")); $date1 = $tanggal1;
        $tanggal2 = isset($data->date2) ? $data->date2 : date("Y-m-d"); $date2 = $tanggal2; $today = date("Y-m-d");
        $toDate = date("Y-m-d", strtotime("last day of next month", strtotime($date2)));
        $ledger = isset($data->ledger) ? $data->ledger : NULL; if (isset($ledger)&&!empty($ledger)) $accUpdate = true;
        $report = isset($data->report) ? $data->report : NULL;
        $status = "W";

        if ($coce_code != 'ALL') {
            $and_cabang = "AND A.spk_no = '$coce_code'";
        } else {
            $and_cabang = "";
        }

        if ($sales_code != 'ALL') {
            $and_sales_code = "AND trim(a.sales_code) = '$sales_code'";
        } else {
            $and_sales_code = "";
        }

        if ($ledger != null) {
            $and_ledger = "AND A.vend_code = '$ledger'";
        } else {
            $and_ledger = "";
        }
        

        // $sql = "SELECT 
        //             A.spk_no, 
        //             A.vend_code,  
        //             (SELECT b.name FROM in_trans b WHERE b.remark = A.pr_no AND b.in_type = 'O' AND b.item_code = A.item_code) AS name,
        //             A.pr_no, 
        //             A.item_code, 
        //             ROUND(SUM(A.item_qty),2) AS 
        //             (SELECT b.ref_no FROM in_trans b WHERE b.remark = A.pr_no AND b.in_type = 'O' AND b.item_code = A.item_code) AS ref_no_out,
        //             ((SELECT ROUND(SUM(b.item_qty),2) FROM in_trans b WHERE b.remark = A.pr_no AND b.in_type = 'O' AND b.item_code = A.item_code) - (SELECT ROUND(SUM(b.item_qty),2) FROM in_trans b WHERE b.remark = A.pr_no AND b.in_type = 'I' AND b.item_code = A.item_code)) AS sum_out,
        //             (SELECT ROUND(SUM(b.item_qty),2) FROM in_trans b WHERE b.remark = A.pr_no AND b.in_type = 'I' AND b.item_code = A.item_code) AS sum_in,
        //             A.pr_desc,
        //             A.code_comp,
        //             A.entry_by,
        //             A.pr_date
        //         FROM po_req A
        //         INNER JOIN t_card B ON A.vend_code = B.code
        //         WHERE A.comp_code = 'SAKA00' 
        //         AND A.pr_type = 'S' 
        //         AND A.item_qty2 > 0
        //         AND A.code_comp = UPPER('$shift')
        //         AND A.pr_date <= '$date2'
        //         AND 1=1 
        //         AND A.pr_stat = 'A' 
        //         AND trim(A.pending_stat) = 'F'
        //         $and_cabang
        //         $and_sales_code
        //         $and_ledger
        //         GROUP BY A.spk_no, A.vend_code, A.pr_no, A.item_code, A.item_qty, A.pr_desc,A.entry_by,A.code_comp,A.pr_date";

        // ((SELECT ROUND(SUM(b.item_qty),2) FROM in_trans b WHERE b.remark = A.pr_no AND b.in_type = 'O' AND b.item_code = A.item_code) - (SELECT ROUND(SUM(b.item_qty),2) FROM in_trans b WHERE b.remark = A.pr_no AND b.in_type = 'I' AND b.item_code = A.item_code)) AS sum_out,
        // (SELECT ROUND(SUM(b.item_qty),2) FROM in_trans b WHERE b.remark = A.pr_no AND b.in_type = 'I' AND b.item_code = A.item_code) AS sum_in
        
        $sql = "SELECT 
                    A.spk_no,
                    A.vend_code,
                    B.name,
                    A.pr_date,
                    A.pr_no,
                    A.item_code,
                    ROUND(A.item_qty,2) AS item_qty
                FROM po_req A
                INNER JOIN t_card B ON A.vend_code = B.code
                WHERE A.comp_code = 'SAKA00' 
                AND A.pr_type = 'S' 
                AND A.item_qty2 > 0
                AND A.code_comp = UPPER('$shift')
                AND A.pr_date <= '$date2'
                AND 1=1 
                AND A.pr_stat = 'A'
                $and_cabang
                $and_sales_code
                $and_ledger
                GROUP BY A.spk_no,A.vend_code,B.name,A.pr_date,A.pr_no,A.item_code,A.item_qty";

        try {

            $rows = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

        } catch (PDOException $e) {
            $responseData = $e;
        }

        for($i=0;$i<count($responseData);$i++) {

            $pr_no = $responseData[$i]['pr_no'];
            $item_code = $responseData[$i]['item_code'];
            $listPrNo = implode(',', array_map('array_pop', $pr_no));

            // try {
            //     $sql = "SELECT b.ref_no, b.voucher_desc, b.car_no, b.driver 
            //             FROM in_trans b 
            //             WHERE b.remark = '$pr_no'
            //             AND b.in_type = 'I' 
            //             AND b.item_code = '$item_code'";
            //     $arr = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            // } catch (PDOException $e) {
            //     $e->getMessage();
            //     $arr = $e; 
            // }

            // try {
            //     $sql2 = "SELECT trim(event_desc) AS pr_no,trim(user_id) AS user_id,reason
            //                 FROM s_audit_gab 
            //                 WHERE menuprg = 'SO111' 
            //                 AND event_code = 'REQUEST CLOSED' 
            //                 AND code_comp = upper('$shift') 
            //                 AND trim(event_desc) = '$pr_no' 
            //                 ORDER BY event_date DESC";
            //     $rowx = $dbh->query($sql2)->fetchAll(PDO::FETCH_ASSOC);
            // } catch (PDOException $th) {
            //     $th->getMessage();
            //     $rowx = $th;
            // }

            $hasil[] = array(
                "spk_no" => $responseData[$i]['spk_no'],
                "vend_code" => $responseData[$i]['vend_code'],
                "name" => $responseData[$i]['name'],
                "pr_date" => $responseData[$i]['pr_date'],
                "pr_no" => $responseData[$i]['pr_no'],
                "item_code" => $responseData[$i]['item_code'],
                "item_qty" => $responseData[$i]['item_qty'],
                "sum_out" => $responseData[$i]['sum_out'],
                "sum_in" => $responseData[$i]['sum_in']
                // "item" => $arr,
                // "reason" => $rowx
            );
            $arr = [];

        }

        return $hasil;
        
    }

    function getSales($dbgl,$data,$dbh)
    {
        $responseData = [];
        $respon = [];

        $kode_cab = $data->coce_code;
        $usergroup = $data->usergroup;
        $username = $data->username;

        if ($usergroup == 'SR') {
            $sql = "SELECT sales_code FROM sales_codeuser WHERE username = '".$username."' ORDER BY sales_code";            
        } else {
            if ($kode_cab == 'ALL') {
                $sql = "SELECT trim(nik) AS sales_code FROM t_person WHERE active_flag = 'Y' AND record_id = 'S' ORDER BY nik";
            } else {
                $sql = "SELECT trim(nik) AS sales_code FROM t_person WHERE active_flag = 'Y' AND record_id = 'S' AND position = '$kode_cab' ORDER BY nik";
            }
        }

        try {

            $rows = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

            $respon = [
                "success" => 1,
                "message" => "Data Berhasil Ditampilkan",
                "data" => $responseData
            ];

        } catch (PDOException $e) {
            $respon = [
                "success" => 0,
                "message" => "Data Gagal Ditampilkan",
                "data" => $e
            ];
        }
    
        return $respon;
    }

    function sendEmail($dbgl,$data,$dbh)
    {
        $dataEmail              = headingTemplate("PENGAJUAN FORCE CLOSE");
        $dataEmail              .= bodyTemplateEmail($data);
        $dataEmail              .= footerTemplateEmail();

        $a                     = new Mail();
        $email                 = (object) array();
        $email->senderEmail    = 'rachmadi@ho.pt-saa.com';
        $email->senderUsername = 'rachmadi';
        $email->to             = 'ady.appkey@gmail.com';
        $email->cc             = ['brandalsanggulan@gmail.com'];
        // $email->attachment     = '';
        $token                 = $a->getToken();
        $email->token          = $token;
        $email->subject        = "Test Email Rachmadi";
        $email->modul          = "Modul Email Ady";
        $email->body           = $dataEmail;
        $email->tokenExpired   = 7;

        $a->sendMail($email);

        return "Email Send";
        // return $data;
    }

    function headingTemplate($titleNotification)
	{
		$htmlHeader = '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
        <head>
            <!--[if gte mso 9]>
            <xml>
                <o:OfficeDocumentSettings>
                <o:AllowPNG/>
                <o:PixelsPerInch>96</o:PixelsPerInch>
                </o:OfficeDocumentSettings>
            </xml>
            <![endif]-->
            <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
            <meta name="format-detection" content="date=no" />
            <meta name="format-detection" content="address=no" />
            <meta name="format-detection" content="telephone=no" />
            <meta name="x-apple-disable-message-reformatting" />
            <!--[if !mso]><!-->
            <link href="https://fonts.googleapis.com/css?family=Kreon:400,700|Playfair+Display:400,400i,700,700i|Raleway:400,400i,700,700i|Roboto:400,400i,700,700i" rel="stylesheet" />
            <!--<![endif]-->
            <title>Email Template</title>
            <!--[if gte mso 9]>
            <style type="text/css" media="all">
                sup { font-size: 100% !important; }
            </style>
            <![endif]-->


            <style type="text/css" media="screen">
                /* Linked Styles */
                body { padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#1e52bd; -webkit-text-size-adjust:none }
                a { color:#000001; text-decoration:none }
                p { padding:0 !important; margin:0 !important }
                img { -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }
                .mcnPreviewText { display: none !important; }
                .text-footer2 a { color: #ffffff; }

                /* Mobile styles */
                @media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
                    .mobile-shell { width: 100% !important; min-width: 100% !important; }

                    .m-center { text-align: center !important; }
                    .m-left { text-align: left !important; margin-right: auto !important; }

                    .center { margin: 0 auto !important; }
                    .content2 { padding: 8px 15px 12px !important; }
                    .t-left { float: left !important; margin-right: 30px !important; }
                    .t-left-2  { float: left !important; }

                    .td { width: 100% !important; min-width: 100% !important; }

                    .content { padding: 30px 15px !important; }
                    .section { padding: 30px 15px 0px !important; }

                    .m-br-15 { height: 15px !important; }
                    .mpb5 { padding-bottom: 5px !important; }
                    .mpb15 { padding-bottom: 15px !important; }
                    .mpb20 { padding-bottom: 20px !important; }
                    .mpb30 { padding-bottom: 30px !important; }
                    .mp30 { padding-bottom: 30px !important; }
                    .m-padder { padding: 0px 15px !important; }
                    .m-padder2 { padding-left: 15px !important; padding-right: 15px !important; }
                    .p70 { padding: 30px 0px !important; }
                    .pt70 { padding-top: 30px !important; }
                    .p0-15 { padding: 0px 15px !important; }
                    .p30-15 { padding: 30px 15px !important; }
                    .p30-15-0 { padding: 30px 15px 0px 15px !important; }
                    .p0-15-30 { padding: 0px 15px 30px 15px !important; }
                    .text-footer { text-align: center !important; }
                    .m-td,
                    .m-hide { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }
                    .m-block { display: block !important; }
                    .fluid-img img { width: 100% !important; max-width: 100% !important; height: auto !important; }
                    .column,
                    .column-dir,
                    .column-top,
                    .column-empty,
                    .column-top-30,
                    .column-top-60,
                    .column-empty2,
                    .column-bottom { float: left !important; width: 100% !important; display: block !important; }
                    .column-empty { padding-bottom: 15px !important; }
                    .column-empty2 { padding-bottom: 30px !important; }

                    .content-spacing { width: 15px !important; }
                    .text-button-info :hover{
                        background: #8ec9e5;
                        box-shadow: #ffffff;
                        transition: all 1s ease-in-out 0s;
                        -moz-transition: all 1s ease-in-out 0s;
                        -webkit-transition: all 1s ease-in-out 0s;
                        -o-transition: all 1s ease-in-out 0s;
                    }

                }
            </style>
        </head>
        <body class="body"style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#1e52bd; -webkit-text-size-adjust:none;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#1e52bd">
                <tr>
                    <td align="center" valign="top">
                        <!-- Main -->
                        <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
                            <tr>
                                <td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
                                    <!-- Header -->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="p30-15" style="padding: 40px 0px 20px 0px;">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <th class="column-top" width="200"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="text-top m-center mpb5"style="color:#9babdb; font-family:\'Times New Roman\', Georgia, serif; font-size:11px; line-height:22px; text-align:left; text-transform:uppercase;"><multiline></multiline></td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                        <th class="column-top"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td align="right">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <!-- END Top bar -->
                                        <!-- Logo saka agung abadi -->


                                        <tr>
                                            <td>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#1e52bd">
                                                    <tr>
                                                        <td class="p0-15" style="padding: 0px 20px;border-bottom:solid 2px #ffffff">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <!--header section  -->
                                                                    <td class="pb40"style="padding-bottom:20px;">
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <th class="column-top" width="60"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                        <tr>
                                                                                            <td class="day"style="color:#1e52bd; font-family:\'Raleway\', Arial,sans-serif; font-size:40px; line-height:44px; text-align:left; font-weight:bold;">
                                                                                                <multiline>
                                                                                                    <img style="margin-bottom: -30px;" src="https://drive.google.com/uc?id=1UWpvU7a8P3tPWy03Nh_UNApbo1DDEUw-&export=download" width="90" height="70" editable="true" border="0" alt="logo saka" />
                                                                                                </multiline>
                                                                                            </td>

                                                                                        </tr>
                                                                                    </table>
                                                                                </th>
                                                                                <th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
                                                                                <th class="column-top"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:bold; vertical-align:top;color:#ffffff">
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                        <tr style="padding-bottom: 0px;">
                                                                                            <td class="h5-black black"style="font-family:\'Raleway\', Arial,sans-serif; font-size:18px; line-height:18px; text-align:left; padding-bottom:0px;padding-top:15px; font-weight:bold; color:#ffffff;">
                                                                                                <multiline>SAKA Integrated Information System </multiline>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td class="h5-black black"style="font-family:\'Raleway\', Arial,sans-serif; font-size:12px; line-height:22px; text-align:left;font-weight:bold; color:#ffffff !important; text-decoration: none !important"><multiline>`https://siis.pt-saa.com`</multiline></td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </th>
                                                                                <th class="column-empty" width="10"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; direction:ltr;"></th>
                                                                                <th class="column-top" width="156"style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
                                                                                    <td class="event-separator" style="padding-bottom: 2px;border-bottom:solid 1px #ffffff; "></td>
                                                                                </th>
                                                                            </tr>
                                                                        </table>
                                                                    </td>

                                                                    <!--end of header section  -->
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <!-- logo dan header table -->
                                        <!-- Nav -->
                                        <!-- END Nav -->
                                    </table>
                                    <!-- END Header -->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ebebeb">
                                        <tr>
                                            <td class="p30-15-0" style="padding: 10px 10px 0px;" bgcolor="#1e52bd">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="center">
                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td class="text-button-orange"style="background:#e85711; color:#ffffff; font-family:\'Raleway\', Arial,sans-serif; font-size:14px; font-weight:bold; line-height:18px; text-align:center; padding:10px 30px;  border-radius:0px;">
                                                                        <a href="#" target="_blank" class="link-white"style="color:#ffffff; text-decoration:none;">
                                                                            <span class="link-white"style="color:#ffffff; text-decoration:none; ">
                                                                                ' . $titleNotification . '
                                                                            </span>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>

                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ebebeb">
                                        <tr>
                                            <td class="p30-15-0" style="padding: 20px 20px 0px;" bgcolor="#1e52bd">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="center">
                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!--end of header template-->
                                    <!--content table is here  --> ';
		return $htmlHeader;
	}

    function bodyTemplateEmail($data) {
        $bodyTemplate = '
            <div style="overflow: auto;min-height: 200px;">
                <table width="100%" border="1" cellspacing="0" cellpadding="0">
                    <tr style="border: solid 1px #ffffff;">
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">CABANG</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">LEDGER</h4>
                        </td>
                        <td style="width:5%; padding: 2px !important; color:#ffffff; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">NAME</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">NO. SO</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">ITEM CODE</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">QTY SO</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">REF NO OUT</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">QTY DO</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">OUT</h4>
                        </td>
                        <td style="width:5%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">DESCRIPTION</h4>
                        </td>
                    </tr>
                    <tr style="border: solid 1px #ffffff;">
                        <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">'.$data->data_all->spk_no.'</h4>
                        </td>
                        <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">'.$data->data_all->vend_code.'</h4>
                        </td>
                        <td style="width:5%; height:10%; padding: 2px !important; color:#ffffff; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">'.$data->data_all->name.'</h4>
                        </td>
                        <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">'.$data->data_all->pr_no.'</h4>
                        </td>
                        <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">'.$data->data_all->item_code.'</h4>
                        </td>
                        <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">'.$data->data_all->item_qty.'</h4>
                        </td>
                        <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;"></h4>
                        </td>
                        <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">'.$data->data_all->sum_out.'</h4>
                        </td>
                        <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;">'.$data->data_all->sum_in.'</h4>
                        </td>
                        <td style="width:5%; height:10%; font-family:\'Raleway\', Arial,sans-serif; font-size:10px; line-height:10px; text-align:center;">
                            <h4 style="color: #ffffff;margin: 8px;"></h4>
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top: 80px;">
                    <tr>
                        <td bgcolor="#e63d13f5" class="text-button-info" style="width: 30%; background-image: linear-gradient(-225deg, #f08b06 0%, #ee7715 48%, #ee6a12 100%);background: linear-gradient(-225deg, #e63d13f5 0%, #e2390f 48%, #e98708 100%); border-color:solid 1px #ffffff; color:#ffffff; font-family:\'Raleway\', Arial,sans-serif; font-size:16px; font-weight:bold; line-height:18px; text-align:center; padding:10px 20px; border-radius:20px;border:solid 2px #ffffff">
                            <a href="#"  class="link-white" style="color:#ffffff; text-decoration:none;">
                                <span class="link-white"style="color:#ffffff; text-decoration:none;">
                                    MENYETUJUI
                                </span>
                            </a>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td bgcolor="#e63d13f5" class="text-button-info" style="width: 30%; background-image: linear-gradient(-225deg, #f08b06 0%, #ee7715 48%, #ee6a12 100%);background: linear-gradient(-225deg, #e63d13f5 0%, #e2390f 48%, #e98708 100%); border-color:solid 1px #ffffff; color:#ffffff; font-family:\'Raleway\', Arial,sans-serif; font-size:16px; font-weight:bold; line-height:18px; text-align:center; padding:10px 20px; border-radius:20px;border:solid 2px #ffffff">
                            <a href="#"  class="link-white" style="color:#ffffff; text-decoration:none;">
                                <span class="link-white"style="color:#ffffff; text-decoration:none;">
                                    MENOLAK
                                </span>
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
        ';
        return $bodyTemplate;
    }

    function footerTemplateEmail()
	{
		$footerTempate = '
                                <!--end of content table is here  -->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="text-footer2 p30-15" style="padding: 30px 15px 50px 15px; color:#ffffff; font-family:\'Raleway\', Arial,sans-serif; font-size:12px; line-height:14px; text-align:left;">
                                                <multiline>
                                                    <h4 style="color: #ffffff;">PT SAKA AGUNG ABADI - KERAHASIAAN DATA</h4>
                                                </multiline>
                                                <multiline>Data dan informasi yang disajikan melalui
                                                SIIS (SAKA Integrated Information System) adalah
                                                milik perusahaan dan setiap karyawan wajib menjaga
                                                kerahasiaan data transaksi maupun data-data
                                                lainnya. Melakukan pelanggaran dalam menjaga
                                                kerahasiaan data perusahaan akan dikenakan sanksi
                                                yang berlaku di perusahaan.</multiline>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- END Footer -->
                                </td>
                            </tr>
                        </table>
                        <!-- END Main -->
                    </td>
                </tr>
            </table>
        </body>
        </html>
        ';
		return $footerTempate;
	}

    function getSupirByNopol($dbgl,$data,$dbh)
    {

        $sql = "SELECT sopir, standar_rit FROM t_truck WHERE nopol='{$data->no_pol}'";

        try {

            $rows = $dbgl->query($sql)->fetch(PDO::FETCH_ASSOC);
            $responseData = $rows;

            $respon = [
                "success" => 1,
                "message" => "Data Berhasil Ditampilkan",
                "data" => $responseData
            ];

        } catch (PDOException $e) {
            $respon = [
                "success" => 0,
                "message" => "Data Gagal Ditampilkan",
                "data" => $e
            ];
        }
    
        return $respon;
    }

    function getRitaseByNopol($dbgl,$data,$dbh)
    {
        $sql = "SELECT standar_rit FROM t_truck WHERE nopol='{$data->no_pol}'";

        try {

            $rows = $dbgl->query($sql)->fetchAll(PDO::FETCH_ASSOC);
            $responseData = $rows;

            $respon = [
                "success" => 1,
                "message" => "Data Berhasil Ditampilkan",
                "data" => $responseData
            ];

        } catch (PDOException $e) {
            $respon = [
                "success" => 0,
                "message" => "Data Gagal Ditampilkan",
                "data" => $e
            ];
        }
    
        return $respon;
    }

    function updateStatusSO($dbgl,$data,$dbh)
    {
        try {

            $sql = "UPDATE t_distribution_plan_item SET status_so = '3'
                    WHERE id = '$data->dpId'";
        
            $stmt = $dbgl->prepare($sql);
            $stmt->execute();

            $respon = [
                "success" => 1,
                "message" => "Data DP Berhasil Diupdate"
            ];

        } catch (PDOException $e) {
            $e->getMessage();
            $respon = [
                "success" => 0,
                "message" => $e
            ];
        }


        try {

            $sql = "UPDATE t_distribution_plan_list SET status = '1'
                    WHERE dp_id = '$data->dpId'";
        
            $stmt = $dbgl->prepare($sql);
            $stmt->execute();

            $respon = [
                "success" => 1,
                "message" => "Data DP Berhasil Diupdate"
            ];

        } catch (PDOException $e) {
            $e->getMessage();
            $respon = [
                "success" => 0,
                "message" => $e
            ];
        }

        return $respon;
    }

    function saveDistributionPlanKirimTemp($dbgl,$data,$dbh)
    {

        $arr_so = [];

        $dt = $data->dt_so[0]->so_no;
        $arr = str_replace("{","",$dt);
        $arr2 = str_replace("}","",$arr);
        $arr3 = explode(",",$arr2);

        for ($i=0; $i < count($arr3); $i++) { 

            $so_no = $arr3[$i];

            try {
                $query = "SELECT a.code_comp, a.type, a.cabang,
                                    a.tanggal, a.jaminput, a.code, 
                                    a.name, a.no, a.so_no, a.item_code, 
                                    a.item_qty, a.item_qty2, a.keterangan, 
                                    a.item_ton, a.desa, a.alamat_toko, 
                                    a.car_no, a.wh_code, d.address1 as alamat_promosi , 0 AS total_do, 
                                    a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type, a.sales_code,
                                    CONCAT(d.name,'-',a.name) AS ledgername,
                                    (SELECT lob_code FROM t_card WHERE code = a.code AND code_comp = a.code_comp) AS lob_code
                            FROM v_displan a 
                            LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp
                            WHERE a.cabang IN ('ALL', 'BMA', 'BPN', 'BWI', 'DISL', 'DPS', 'GYR', 'HO', 'KPG', 'KRS', 'MTR', 'NGR', 'PLR', 'SBW', 'SIN', 'SMD', 'TBN', 'TGT')
                            AND a.so_no = '$so_no'
                            ORDER BY a.code, a.tanggal, a.no";

                $stmt = $dbgl->prepare($query);
                $stmt->execute();
            
                while($rows_so = $stmt->fetch(PDO::FETCH_ASSOC)) {
                  $arr_so[] = $rows_so;
                }

            } catch (PDOException $e) {
                $arr_so = $e;
            }

        }

        for ($j=0; $j < count($arr_so); $j++) { 
            $item_code = $arr_so[$j]['item_code'];
            $so_no =  $arr_so[$j]['so_no'];

            $lob_code =  $arr_so[$j]['lob_code'];
            $code_comp =  $arr_so[$j]['code_comp'];

            try {
                $query = "SELECT 
                        a.vend_code, a.pr_no, a.item_code, 
                        a.spk_no, a.item_qty, a.price_amt,
                        a.car_no, a.so_code, '$lob_code' AS lob_code, 
                        '$code_comp' AS code_comp
                    FROM po_req a
                    WHERE pr_no = '$so_no'
                    AND item_code = '$item_code'";

                $stmt = $dbgl->prepare($query);
                $stmt->execute();

                while($rows_so = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $xarr_data[] = $rows_so;
                }

            } catch (PDOException $e) {
                $xarr_data = $e;
            }

            $arr_data['data_so'][] = array(
                'code_comp' => $arr_so[$j]['code_comp'],
                'type' => $arr_so[$j]['type'],   
                'cabang' => $arr_so[$j]['cabang'],   
                'tanggal' => $arr_so[$j]['tanggal'],   
                'jaminput' => $arr_so[$j]['jaminput'],   
                'code' => $arr_so[$j]['code'],   
                'name' => $arr_so[$j]['name'],   
                'no' => $arr_so[$j]['no'],   
                'so_no' => $arr_so[$j]['so_no'],   
                'item_code' => $arr_so[$j]['item_code'],   
                'item_qty' => $arr_so[$j]['item_qty'],   
                'item_qty2' => $arr_so[$j]['item_qty2'],   
                'keterangan' => $arr_so[$j]['keterangan'],   
                'item_ton' => $arr_so[$j]['item_ton'],   
                'desa' => $arr_so[$j]['desa'],   
                'alamat_toko' => $arr_so[$j]['alamat_toko'],   
                'car_no' => $arr_so[$j]['car_no'],   
                'wh_code' => $arr_so[$j]['wh_code'],   
                'alamat_promosi' => $arr_so[$j]['alamat_promosi'],   
                'total_do' => $arr_so[$j]['total_do'],   
                'dept_code' => $arr_so[$j]['dept_code'],   
                'dept_name' => $arr_so[$j]['dept_name'],   
                'ot_flag' => $arr_so[$j]['ot_flag'],   
                'freight_type' => $arr_so[$j]['freight_type'],   
                'sales_code' => $arr_so[$j]['sales_code'],
                'ledgername' => $arr_so[$j]['ledgername'],
                'status_so' => $arr_so[$j]['status_so'],
                'lob_code' => $arr_so[$j]['lob_code'],
                'dept_code' => $arr_so[$j]['dept_code'],
                'item' => $xarr_data
            );
            $xarr_data = [];
        }

        $price_so = $arr_data['data_so'];

        for ($k=0; $k < count($price_so); $k++) { 

            $arr_all[] = array(
                'hasil' => 1,
                'data_so' => $price_so[$k],
                'price_so' => array(
                    'so_no' => $price_so[$k]['item'][0]['pr_no'],
                    'cabang' => $price_so[$k]['item'][0]['spk_no'],
                    'item_code' => trim($price_so[$k]['item'][0]['item_code']),
                    'price_amount' => $price_so[$k]['item'][0]['price_amt'],
                ),
                'price_sales' => array(
                    'cabang' => $price_sales[$k]['coce_code'],
                    'item_code' => $price_sales[$k]['item_code'],
                    'price_amount' => $price_sales[$k]['kirim']
                )
            );

        }

        $all[] = array(
            'data_so' => $arr_so,
            'data_dt' => $arr_all
        );

        return $all;
    }

    function getDataSOKirimSCReq($dbgl,$data,$dbh)
    {

        $all_kirim = [];

        try {
            // $query = "SELECT a.code_comp, a.type, a.cabang, 
            //                 a.tanggal, a.jaminput, a.code, 
            //                 a.name, a.no, a.so_no, a.item_code, 
            //                 a.item_qty, a.item_qty2, a.keterangan, 
            //                 a.item_ton, a.desa, a.alamat_toko, 
            //                 a.car_no, a.wh_code, d.address1 as alamat_promosi , 0 AS total_do, 
            //                 a.dept_code, d.name as dept_name, a.ot_flag, a.freight_type, a.sales_code,
            //                 (SELECT wh_office FROM in_warehouse WHERE wh_code = a.kp_code) AS kp_office,
            //                 CONCAT(d.name,'-',a.name) AS ledgername, a.pr_stat, to_char(a.delivery_time, 'YYYY-MM-DD ') AS delivery_time
            //         FROM v_displan a 
            //         LEFT JOIN t_card d on trim(a.dept_code) = d.code and a.code_comp = d.code_comp 
            //         WHERE a.cabang IN (".$data->where.") 
            //         AND a.freight_type = 'kirim'
            //         AND a.so_no NOT IN 
            //             (SELECT so_no FROM t_distribution_plan_list a 
            //             LEFT JOIN t_distribution_plan_item b ON b.id = a.dp_id
            //             WHERE status_so = '1' OR status_so = '0')
            //         AND a.so_no NOT IN 
            //             (SELECT so_no FROM t_distribution_plan_pengalihan a 
            //             LEFT JOIN t_distribution_plan_item b ON b.id = a.id_pengalihan
            //             WHERE status_so = '0' OR status_so = '1')
            //         AND a.item_ton >= 8
            //         ORDER BY a.code, a.tanggal, a.no";

            $query = "SELECT a.*, 'Gandengan' AS tipe_muat FROM t_distribution_plan_temp2 a
                        WHERE a.sc_status = '0'
                        AND a.asal_gudang IN (".$data->where.")
                        AND  a.so_no NOT IN 
                            (SELECT so_no FROM t_distribution_plan_list a 
                            LEFT JOIN t_distribution_plan_item b ON b.id = a.dp_id
                            WHERE status_so = '1' OR status_so = '0')
                        AND a.so_no NOT IN 
                            (SELECT so_no FROM t_distribution_plan_pengalihan a 
                            LEFT JOIN t_distribution_plan_item b ON b.id = a.id_pengalihan
                            WHERE status_so = '0' OR status_so = '1')";
            
            // echo $query;
            // AND a.so_no NOT IN (SELECT so_no FROM t_distribution_plan_list)
            // AND a.so_no NOT IN (SELECT so_no FROM t_distribution_plan_pengalihan)

            $rows = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);

            // $arrayItem = [];
            // foreach ($rows as $key => $row) {

            //     $so = $row['so_no'].$row['no'].$row['code_comp'];
            //     // if ($row['type'] == 'SM') {
            //     //     $so = $row['no'];
            //     // }
            //     if (isset($arrayItem[$so])) {
            //         $arrayItem[$so]['item_code'] .= '['.$row['item_code'].' - '.$row['item_qty2'].']';
            //         unset($rows[$key]);
            //     } else {
            //         $arrayItem[$so] = [
            //             'item_code' => '['.$row['item_code'].' - '.$row['item_qty2'].']',
            //         ];
            //     }
            // }

            // foreach ($rows as $row) {
            //     $so = $row['so_no'].$row['no'].$row['code_comp'];
            //     // if ($row['type'] == 'SM') {
            //     //     $so = $row['no'];
            //     // }
            //     $ledgerName = $row['name'];
            //     if ($row['dept_code'] <> NULL || $row['dept_code'] <> '') {
            //         $ledgerName = $row['dept_name'].' - '.$ledgerName;
            //         $row['alamat_toko'] = $row['alamat_promosi'];
            //     }

            //     if($row['type'] == 'SO' && $row['ot_flag'] == 1) {
            //         continue;
            //     }

            //     if ($row['freight_type'] == 'ambil') {
            //         continue;
            //     }

            //     $hasil[] = array(
            //         'id_temp' => $row['id_temp'],
            //         'type' => $row['type'], 
            //         'code' => $row['code'],
            //         'ledgername' => $ledgerName, 
            //         'tanggal' => $row['tanggal'],
            //         'sm_no' => $row['type'] == 'SM' ? $row['no'] : '',
            //         'so_no' => $row['so_no'], 
            //         'item_code' => $arrayItem[$so]['item_code'], 
            //         'alamat_toko' => $row['alamat_toko'], 
            //         'keterangan' => $row['keterangan'], 
            //         'jaminput' => $row['jaminput'], 
            //         'code_comp' => $row['code_comp'],
            //         'sales_code' => $row['sales_code'],
            //         'cabang' => $row['cabang'],
            //         'freight_type' => $row['freight_type'],
            //         'dept_code' => $row['dept_code'],
            //         'type_so' => 'cabang_int',
            //         'kp_office' => $row['kp_office'],
            //         'pr_stat' => $row['pr_stat'], 
            //         'delivery_time' => $row['delivery_time'],
            //         'tipe_muat' => $row['tipe_muat']
            //     );
            // }

        } catch (PDOException $e) {
            $e->getMessage();
            $hasil = $e; 
        }

        // return $hasil; $rows
        return $rows; 
    }

    function updateApproveReqSales($dbgl,$data,$dbh)
    {
        try {

            $sql = "UPDATE t_distribution_plan_temp2 SET sc_status = '1'
                    WHERE id_temp = '$data->id_temp'";
        
            $stmt = $dbgl->prepare($sql);
            $stmt->execute();

            $respon = [
                "success" => 1,
                "message" => "Data DP Berhasil Disetujui"
            ];

        } catch (PDOException $e) {
            $e->getMessage();
            $respon = [
                "success" => 0,
                "message" => "Data DP Gagal Disetujui",
                "error" => $e
            ];
        }

        return $respon;
    }

    function updateDeleteReqSales($dbgl,$data,$dbh)
    {
        try {

            $sql = "DELETE FROM t_distribution_plan_temp2
                    WHERE id_temp = '$data->id_temp'";
        
            $stmt = $dbgl->prepare($sql);
            $stmt->execute();

            $respon = [
                "success" => 1,
                "message" => "Data DP Berhasil Ditolak"
            ];

        } catch (PDOException $e) {
            $e->getMessage();
            $respon = [
                "success" => 0,
                "message" => "Data DP Gagal Ditolak",
                "error" => $e
            ];
        }

        return $respon;
    }

    function getDatesFromRange($start, $end, $format = 'Y-m-d') {
      
        $array = array();
          
        $interval = new DateInterval('P1D');
      
        $realEnd = new DateTime($end);
        $realEnd->add($interval);
      
        $period = new DatePeriod(new DateTime($start), $interval, $realEnd);
      
        foreach($period as $date) {                 
            $array[] = array(
                'date' => $date->format($format),
                'date_format' => $date->format('j F Y')
            );
        }
      
        return $array;
    }

    function getCabang2($dbgl,$data,$dbh)
    {
        $response = [];
        $querya = "SELECT 
                        TRIM(coce_code) AS code,
                        coce_name AS name
                    FROM 
                        gl_coce 
                    WHERE 
                        coce_code NOT IN ('KLK','PLR','CLB','CBW','BWI','DISL','KP','HO','DPS','BALI')";
    
        try {
    
            $response = $dbgl->query($querya)->fetchAll(PDO::FETCH_ASSOC);
            $object = array("code" => "ALL", "name" => "ALL");
            array_unshift($response, $object);

        } catch (PDOException $e) {
            $response = $e;
        }
    
        return $response;
    }

    function getReportDisplan($dbgl,$data,$dbh)
    {
        $cabang = $data->cabang;
        $bulan = $data->bulan;
        $tahun = $data->tahun;
        $promo = $data->promo;

        $response = [];
        $hasil = [];

        if($promo == "tanpa") {
            
            $kodepromo = $cabang.'P';

            $query = "SELECT
                        a.code_comp as comp,
                        a.cabang as cabang,
                        TO_CHAR(a.tanggal::timestamp,'Month') AS bulan,
                        TO_CHAR(a.tanggal::timestamp,'YYYY') AS periode,
                        a.type as tipe,
                        b.no_dp as no_dp,
                        CASE WHEN a.delivery_date IS NOT NULL 
                            THEN to_char(a.delivery_date, 'YYYY-MM-DD')
                            ELSE a.tanggal
                        END AS tgl_so,
                        b.tgl_kirim as tgl_kirim,
                        a.no as no_so,
                        a.code as code,
                        a.name as name,
                        b.no_pol as no_truck,
                        a.item_code as item_code,
                        a.item_qty as qty_so,
                        a.item_qty2 as qty_dp,
                        CASE WHEN a.delivery_date IS NOT NULL 
                            THEN DATE_PART('day', b.tgl_kirim::timestamp - a.delivery_date::timestamp) - (SELECT COUNT(*)
                                FROM generate_series(a.delivery_date::timestamp, b.tgl_kirim::timestamp, '1 day'::interval) s
                                WHERE extract(DOW from s) in (0))
                            ELSE DATE_PART('day', b.tgl_kirim::timestamp - a.tanggal::timestamp) - (SELECT COUNT(*)
                                FROM generate_series(a.tanggal::timestamp, b.tgl_kirim::timestamp, '1 day'::interval) s
                                WHERE extract(DOW from s) in (0))
                        END AS waktu_kirim,
                        a.pengalihan as pengalihan,
                        CONCAT(a.code_comp,' ',a.cabang,' ',a.type,' ',b.tgl_kirim,' ',b.no_dp,' ',a.tanggal,' ',
                        a.no,' ',a.code,' ',a.item_code,' ',a.item_qty,' ',a.item_qty2,' ',DATE_PART('day', b.tgl_kirim::timestamp - a.tanggal::timestamp),' hari') as searchqry
                    FROM
                        t_dp_detail a
                    LEFT JOIN
                        t_distribution_plan b ON a.dp_id = b.id
                    WHERE
                        b.tgl_kirim IS NOT NULL 
                        AND 
                        EXTRACT(year FROM b.tgl_kirim::TIMESTAMP) = '{$tahun}'
                        AND
                        EXTRACT(month from b.tgl_kirim::TIMESTAMP) = '{$bulan}'
                        AND 
                        LEFT(a.so_no, 4) <> '{$kodepromo}'
                        AND
                        a.cabang = '{$cabang}'
                        AND
                        b.freight_type = 'kirim'
                    ORDER BY 
                        a.tanggal, b.no_dp";
        
        } else {
        
            $query = "SELECT
                            a.code_comp as comp,
                            a.cabang as cabang,
                            TO_CHAR(a.tanggal::timestamp,'Month') AS bulan,
                            TO_CHAR(a.tanggal::timestamp,'YYYY') AS periode,
                            a.type as tipe,
                            b.no_dp as no_dp,
                            CASE WHEN a.delivery_date IS NOT NULL 
                                THEN to_char(a.delivery_date, 'YYYY-MM-DD')
                                ELSE a.tanggal
                            END AS tgl_so,
                            b.tgl_kirim as tgl_kirim,
                            a.no as no_so,
                            a.code as code,
                            a.name as name,
                            b.no_pol as no_truck,
                            a.item_code as item_code,
                            a.item_qty as qty_so,
                            a.item_qty2 as qty_dp,
                            CASE WHEN a.delivery_date IS NOT NULL 
                                THEN DATE_PART('day', b.tgl_kirim::timestamp - a.delivery_date::timestamp) - (SELECT COUNT(*)
                                    FROM generate_series(a.delivery_date::timestamp, b.tgl_kirim::timestamp, '1 day'::interval) s
                                    WHERE extract(DOW from s) in (0))
                                ELSE DATE_PART('day', b.tgl_kirim::timestamp - a.tanggal::timestamp) - (SELECT COUNT(*)
                                    FROM generate_series(a.tanggal::timestamp, b.tgl_kirim::timestamp, '1 day'::interval) s
                                    WHERE extract(DOW from s) in (0))
                            END AS waktu_kirim,
                            a.pengalihan as pengalihan,
                            $tahun AS periode,
                            CONCAT(a.code_comp,' ',a.cabang,' ',a.type,' ',b.tgl_kirim,' ',b.no_dp,' ',a.tanggal,' ',
                            a.no,' ',a.code,' ',a.item_code,' ',a.item_qty,' ',a.item_qty2,' ',DATE_PART('day', b.tgl_kirim::timestamp - a.tanggal::timestamp),' hari') as searchqry
                        FROM
                            t_dp_detail a
                        LEFT JOIN
                            t_distribution_plan b ON a.dp_id = b.id
                        WHERE
                            b.tgl_kirim IS NOT NULL 
                            AND 
                            EXTRACT(year FROM b.tgl_kirim::TIMESTAMP) = '{$tahun}'
                            AND
                            EXTRACT(month from b.tgl_kirim::TIMESTAMP) = '{$bulan}'
                            AND 
                            a.cabang = '{$cabang}'
                            AND 
                            b.freight_type = 'kirim'
                        ORDER BY 
                            a.tanggal, b.no_dp";
        }

		try {

			$response = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);

            foreach ($response as $value) {
                $tgl_so = $value['tgl_so'];
                $tgl_kirim = $value['tgl_kirim'];
                $periode = $value['periode'];
                $cabang = $value['cabang'];

                $qry = "SELECT COUNT(*)
                        FROM hrm_hari_libur a 
                        INNER JOIN hrm_office b ON a.kd_cab = b.kd_off 
                        WHERE a.periode = '$periode' AND a.kd_cab = '$cabang' AND a.id > 0
                        AND a.tanggal BETWEEN '$tgl_so' AND '$tgl_kirim'";

                $rq = $dbh->query($qry)->fetchAll(PDO::FETCH_ASSOC);

                $total_libur = $rq[0]['count'];

                $hasil[] = array(
                    'comp' => $value['comp'],
                    'cabang' => $value['cabang'],
                    'bulan' => $value['bulan'],
                    'tipe' => $value['tipe'],
                    'no_dp' => $value['no_dp'],
                    'tgl_kirim' => $value['tgl_kirim'],
                    'tgl_so' => $value['tgl_so'],
                    'no_so' => $value['no_so'],
                    'code' => $value['code'],
                    'name' => $value['name'],
                    'no_truck' => $value['no_truck'],
                    'item_code' => $value['item_code'],
                    'qty_so' => $value['qty_so'],
                    'qty_dp' => $value['qty_dp'],
                    'waktu_kirim' => $value['waktu_kirim'] - $total_libur,
                    'pengalihan' => $value['pengalihan'],
                    'searchqry' => $value['searchqry']
                );

            }

		} catch (PDOException $e) {
            
			$hasil = $e;

		}

		return $hasil;
    }

    function getReason($dbgl,$data,$dbh)
    {

        $nodp = $data->nodp;
        $noso = $data->noso;

        $query = "SELECT 
                        DISTINCT ON (a.no) a.no, a.so_no, a.cabang, a.code, a.name, a.alamat_toko, a.item_code, a.item_qty2, a.item_ton, a.code_comp, a.insert_by, a.insert_time, a.alasan, a.keterangan
                    FROM 
                        t_dp_rejected_so a
                    WHERE
                        a.no = '{$noso}'";

        try {

			$response = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);

		} catch (PDOException $e) {
            
			$response = $e;

		}

        return $response;
    }

    function getDisPlanByID($dbgl,$data,$dbh)
    {

        $query = "SELECT
                        a.*
                    FROM
                        t_dp_detail a
                    WHERE
                        a.dp_id = '$data->id'";

        try {

            $response = $dbgl->query($query)->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {

            $response = $e;

        }

        return $response;
    }

?>