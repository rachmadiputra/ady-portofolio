package baledigital.foodcourt.seller;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.HashMap;

import baledigital.foodcourt.seller.Utility.CustomVolleyRequestQueue;
import baledigital.foodcourt.seller.Utility.JSONParser;
import baledigital.foodcourt.seller.Utility.Mainlink;

public class History_Transaksi_Detail_Activity extends AppCompatActivity {

    public static HashMap<String, String> request;

    JSONParser jparser = new JSONParser();

    String trans_detail_id = "", id_seller = "", qtyAwal = "", qtyAkhir = "", img = "", status = "";
    Button bt_update, bt_edit, bt_batal;
    TextView transId, transMeja, transStatus, transMenu, transQTY, transNama, transHarga, transTgl;

    NetworkImageView mNetworkImageView;
    ImageLoader mImageLoader;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_transaksi_detail_layout);
        initComponent();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        id_seller = request.get("seller_id");
        trans_detail_id = request.get("trans_detail_id");
        img = request.get("img");
        status = request.get("status_order");

        transId.setText(request.get("trans_id"));
        transMeja.setText("Meja ("+request.get("no_table")+")");
        if(request.get("no_table").equals(""))
        {
            transMeja.setText("Meja tidak tersedia");
        }
        if (request.get("status_order").equals("0")) {
            transStatus.setText("Pesan");
            transStatus.setTextColor(Color.parseColor("#ff0505"));
        } else if (request.get("status_order").equals("1")) {
            transStatus.setText("Diproses");
            transStatus.setTextColor(Color.parseColor("#3fcffe"));
        } else if (request.get("status_order").equals("2")) {
            transStatus.setText("Selesai");
            transStatus.setTextColor(Color.parseColor("#36d62a"));
        } else {
            transStatus.setText("Batal");
            transStatus.setTextColor(Color.parseColor("#ff0505"));
        }
        transMenu.setText(request.get("nama_menu"));
        transQTY.setText("Qty ("+request.get("qty")+")");
        transNama.setText(request.get("nama_lengkap"));
        transHarga.setText("Rp "+request.get("harga"));
        transTgl.setText(request.get("date_trans"));

        mImageLoader = CustomVolleyRequestQueue.getInstance(this.getApplicationContext()).getImageLoader();
        final String url = Mainlink.locationPhotoMenu+img;
        mImageLoader.get(url, ImageLoader.getImageListener(mNetworkImageView, R.mipmap.ic_launcher, android.R.id.icon1));
        mNetworkImageView.setImageUrl(url, mImageLoader);

    }

    public void initComponent() {
        mNetworkImageView = findViewById(R.id.ivTransaksi);
        bt_update = findViewById(R.id.detTrans_bt_update);
        bt_edit = findViewById(R.id.detTrans_bt_edit);
        bt_batal = findViewById(R.id.detTrans_bt_cancel);
        transId = findViewById(R.id.detTrans_id);
        transMeja = findViewById(R.id.detTrans_noMeja);
        transStatus = findViewById(R.id.detTrans_status);
        transMenu = findViewById(R.id.detTrans_namaMenu);
        transQTY = findViewById(R.id.detTrans_qty);
        transNama = findViewById(R.id.detTrans_namaPanggilan);
        transHarga = findViewById(R.id.detTrans_total);
        transTgl = findViewById(R.id.detTrans_tgl);
    }

}
