package baledigital.foodcourt.seller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import baledigital.foodcourt.seller.Utility.CommonUtils;
import baledigital.foodcourt.seller.Utility.JSONParser;
import baledigital.foodcourt.seller.Utility.Mainlink;
import baledigital.foodcourt.seller.Utility.SharedPref;
import baledigital.foodcourt.seller.Utility.Util;

public class Login_Activity extends AppCompatActivity {

    Button bt_signIn;
    EditText edit_email, edit_password;

    public static final String loginSellerUrl = Mainlink.MainLinkIP+"login_seller.php";

    JSONParser jparser = new JSONParser();

    String refreshedToken = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);
        initComponent();

        bt_signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new LoginSeller().execute();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( Login_Activity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                refreshedToken = instanceIdResult.getToken();
                Log.e("TOKEN", refreshedToken);
                SharedPref.setSharePreferencesString(getApplicationContext(),"pushToken",refreshedToken);
            }
        });

        String id_pedagang = SharedPref.getSharedPreference(getApplicationContext(), "id_seller");

        if (id_pedagang != null) {
            Intent i = new Intent(Login_Activity.this, MainActivity.class);
            startActivity(i);
        } else {
            Log.d("status", "Not Logged In");
        }


    }

    class LoginSeller extends AsyncTask<String, String, String> {
        ProgressDialog pDialog;
        String s="";
        int success=-1;
        int error=0;
        String email = "", password = "", seller_id = "", shaPassword = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog=new ProgressDialog(Login_Activity.this);
            pDialog.setMessage("Login Process. Please Wait..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            email = edit_email.getText().toString();
            password = edit_password.getText().toString();
            try {
                shaPassword = URLEncoder.encode(CommonUtils.getSha1Hex(password), Mainlink.CHARSET);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("email", email));
            params.add(new BasicNameValuePair("password", shaPassword));
            params.add(new BasicNameValuePair("token", refreshedToken));

            JSONObject json = jparser.makeHttpRequest(loginSellerUrl, "POST", params);

            try {
                success = json.getInt("success");
                s = json.getString("message");
                seller_id = json.getString("seller_id");
            } catch (Exception e) {
                error = 1;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String ss) {
            super.onPostExecute(ss);
            pDialog.dismiss();

            if (success == 1) {
                Toast.makeText(getApplicationContext(), "Login Berhasil", Toast.LENGTH_SHORT).show();

                SharedPref.setSharePreferencesString(getApplicationContext(),"id_seller",seller_id);

                Intent i = new Intent(Login_Activity.this, MainActivity.class);
                startActivity(i);
            } else {
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
            }

            if(error==1){
                if(Util.isConnectingToInternet(Login_Activity.this)){
                    Toast.makeText(Login_Activity.this, "Login Gagal, Silahkan Coba Lagi", Toast.LENGTH_SHORT).show();
                } else {
                    Util.showNoInternetDialog(Login_Activity.this);
                }
                return;
            }
        }
    }

    public void initComponent() {
        bt_signIn = findViewById(R.id.login_bt_login);
        edit_email = findViewById(R.id.login_username);
        edit_password = findViewById(R.id.login_password);
    }

}
