package baledigital.foodcourt.seller;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.SyncStateContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import baledigital.foodcourt.seller.Utility.JSONParser;
import baledigital.foodcourt.seller.Utility.Mainlink;
import baledigital.foodcourt.seller.Utility.SharedPref;
import baledigital.foodcourt.seller.Utility.Util;

public class Menu_Add_Activity extends AppCompatActivity {

    EditText editName, editDeskripsi, editPhoto, editHarga;
    Spinner spinCategoryID, spinStatus;
    Button buttonSave, buttonCancel, buttoChoose;

    ImageView imageView;
    Bitmap bitmap, decoded;
    int PICK_IMAGE_REQUEST = 1;
    int bitmap_size = 60;

    public static final String addMenuUrl = Mainlink.MainLinkIP+"add_menu_beta.php";

    JSONParser jparser = new JSONParser();

    String menu_name = "", menu_deskrisi = "", menu_photo = "", menu_harga = "";
    String sellerID = "", menu_catergoryID = "", menu_status = "";
    String id_category = "", id_status = "";
    String kat[] = {"Makanan","Minuman","Apparel"};
    String stat[] = {"Tersedia","Habis"};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_add_layout);

        initComponent();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        spinCategoryID.setAdapter(new ArrayAdapter<String>(this	, android.R.layout.simple_dropdown_item_1line, kat));
        if (spinCategoryID.getSelectedItemPosition()==0) {
            id_category = "1";
        } else if (spinCategoryID.getSelectedItemPosition()==1) {
            id_category = "2";
        } else {
            id_category = "3";
        }

        spinStatus.setAdapter(new ArrayAdapter<String>(this	, android.R.layout.simple_dropdown_item_1line, stat));
        if (spinStatus.getSelectedItemPosition()==0) {
            id_status = "1";
        } else if (spinStatus.getSelectedItemPosition()==1) {
            id_status = "0";
        }

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!TextUtils.isEmpty(editName.getText().toString())){
                    if(!TextUtils.isEmpty(editDeskripsi.getText().toString())) {
                        if(!TextUtils.isEmpty(editHarga.getText().toString())) {
                            if(!TextUtils.isEmpty(id_category)) {
                                if(!TextUtils.isEmpty(id_status)) {
                                    if (imageView.getDrawable() != null) {
                                        new SaveMenu().execute();
                                    } else Toast.makeText(getApplicationContext(), "Silahkan Masukkan Gambar", Toast.LENGTH_SHORT).show();
                                } else Toast.makeText(getApplicationContext(), "Silahkan Masukkan Status", Toast.LENGTH_SHORT).show();
                            } else Toast.makeText(getApplicationContext(), "Silahkan Masukkan Kategori", Toast.LENGTH_SHORT).show();
                        } else Toast.makeText(getApplicationContext(), "Silahkan Masukkan Harga", Toast.LENGTH_SHORT).show();
                    } else Toast.makeText(getApplicationContext(), "Silahkan Masukkan Deskripsi", Toast.LENGTH_SHORT).show();
                } else Toast.makeText(getApplicationContext(), "Silahkan Masukkan Nama Menu", Toast.LENGTH_SHORT).show();
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Menu_Add_Activity.this, MainActivity.class);
                startActivity(i);
            }
        });

        buttoChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooser();
            }
        });
    }

    class SaveMenu extends AsyncTask<String, String, String> {
        ProgressDialog pDialog;
        String s="";
        int success=-1;
        int error=0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog=new ProgressDialog(Menu_Add_Activity.this);
            pDialog.setMessage("Load data. Please Wait..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            sellerID = SharedPref.getSharedPreference(Menu_Add_Activity.this, "id_seller");
            menu_catergoryID = id_category;
            menu_status = id_status;
            menu_name = editName.getText().toString();
            menu_deskrisi = editDeskripsi.getText().toString();
            menu_photo = editPhoto.getText().toString();
            menu_harga = editHarga.getText().toString();

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("seller_id", sellerID));
            params.add(new BasicNameValuePair("category_id", menu_catergoryID));
            params.add(new BasicNameValuePair("nama_menu", menu_name));
            params.add(new BasicNameValuePair("deskripsi", menu_deskrisi));
            params.add(new BasicNameValuePair("img", menu_photo));
            params.add(new BasicNameValuePair("harga", menu_harga));
            params.add(new BasicNameValuePair("status", menu_status));
            params.add(new BasicNameValuePair("img", getStringImage(decoded)));

            JSONObject json = jparser.makeHttpRequest(addMenuUrl, "POST", params);

            try {
                success = json.getInt("success");
                s = json.getString("message");
            } catch (Exception e) {
                error = 1;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String ss) {
            super.onPostExecute(ss);
            pDialog.dismiss();

            if (success == 1) {
                Toast.makeText(getApplicationContext(), "Data Berhasil Disimpan", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(Menu_Add_Activity.this, MainActivity.class);
                startActivity(i);
                Log.d("message", s);
            } else {
                //Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
                Log.d("message", s);
            }

            if(error==1){
                if(Util.isConnectingToInternet(Menu_Add_Activity.this)){
                    Toast.makeText(Menu_Add_Activity.this, "Server Down, Please Try Again", Toast.LENGTH_SHORT).show();
                } else {
                    Util.showNoInternetDialog(Menu_Add_Activity.this);
                }
                return;
            }
        }
    }

    private void setToImageView(Bitmap bmp) {
        //compress image
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, bitmap_size, bytes);
        decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(bytes.toByteArray()));

        //menampilkan gambar yang dipilih dari camera/gallery ke ImageView
        imageView.setImageBitmap(decoded);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //mengambil fambar dari Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                // 512 adalah resolusi tertinggi setelah image di resize, bisa di ganti.
                setToImageView(getResizedBitmap(bitmap, 512));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, bitmap_size, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public void initComponent() {
        editName = findViewById(R.id.menu_edit_name);
        editDeskripsi = findViewById(R.id.menu_edit_deskripsi);
        editPhoto = findViewById(R.id.menu_edit_img);
        editHarga = findViewById(R.id.menu_edit_harga);
        spinCategoryID = findViewById(R.id.menu_edit_category);
        spinStatus = findViewById(R.id.menu_edit_status);
        buttonSave = findViewById(R.id.menu_bt_add);
        buttonCancel = findViewById(R.id.menu_bt_cancel);
        buttoChoose = findViewById(R.id.menu_pilihGambar);
        imageView = findViewById(R.id.imageView);
    }
}
