package baledigital.foodcourt.seller;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import baledigital.foodcourt.seller.Utility.CustomVolleyRequestQueue;
import baledigital.foodcourt.seller.Utility.JSONParser;
import baledigital.foodcourt.seller.Utility.Mainlink;
import baledigital.foodcourt.seller.Utility.SharedPref;
import baledigital.foodcourt.seller.Utility.Util;

public class Menu_Edit_Activity extends AppCompatActivity {

    public static HashMap<String, String> request;
    AutoCompleteTextView txtName, txtDeskripsi, txtHarga;
    Spinner spinCategori, spinStatus;
    Button bt_edit, bt_delete, pilihGambar;

    NetworkImageView mNetworkImageView;
    ImageLoader mImageLoader;
    String menu_id = "", id_seller = "", img = "";
    String menu_name = "", menu_deskrisi = "", menu_photo = "", menu_harga = "";
    String menuID = "", sellerID = "", menu_catergoryID = "", menu_status = "", imgBaru = "";
    String kat[] = {"Makanan", "Minuman", "Apparel"};
    String stat[] = {"Habis","Tersedia"};
    String id_category = "", id_status = "";

    JSONParser jparser = new JSONParser();

    ImageView imageView;
    Bitmap bitmap, decoded;
    int PICK_IMAGE_REQUEST = 1;
    int bitmap_size = 60;
    int setCategory, setStatus;

    private static final String TAG = Menu_Edit_Activity.class.getSimpleName();
    Context con;

    public static final String updateMenuUrl = Mainlink.MainLinkIP + "edit_menu_beta.php";
    public static final String deleteMenuUrl = Mainlink.MainLinkIP + "delete_menu.php";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_edit_layout);
        initComponent();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        id_seller = SharedPref.getSharedPreference(getApplicationContext(), "id_seller");
        menu_id = request.get("menu_id");
        img = request.get("img");

        txtName.setText(request.get("nama_menu"));
        txtDeskripsi.setText(request.get("deskripsi"));
        txtHarga.setText(request.get("harga"));

        con = Menu_Edit_Activity.this;

        spinCategori.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, kat));
        spinStatus.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, stat));

        setCategory = Integer.parseInt(request.get("category_id")) - 1;
        setStatus = Integer.parseInt(request.get("status"));

        spinCategori.setSelection(setCategory);
        spinStatus.setSelection(setStatus);

        bt_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imageView.getDrawable() != null) {
                    imgBaru = getStringImage(decoded);
                    new UpdateMenu().execute();
                } else {
                    new UpdateMenu().execute();
                }
            }
        });

        bt_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder dialogDelete = new AlertDialog.Builder(Menu_Edit_Activity.this);

                dialogDelete.setTitle("Warning!!");
                dialogDelete.setMessage("Are you sure you want to this delete?");
                dialogDelete.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            new DeleteMenu().execute();
                        } catch (Exception e) {
                            Log.e("error", e.getMessage());
                        }
                    }
                });

                dialogDelete.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialogDelete.show();
            }
        });

        pilihGambar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });

        mImageLoader = CustomVolleyRequestQueue.getInstance(this.getApplicationContext()).getImageLoader();
        final String url = Mainlink.locationPhotoMenu + img;
        mImageLoader.get(url, ImageLoader.getImageListener(mNetworkImageView, R.mipmap.ic_launcher, android.R.id.icon1));
        mNetworkImageView.setImageUrl(url, mImageLoader);
    }

    class UpdateMenu extends AsyncTask<String, String, String> {
        ProgressDialog pDialog;
        String s = "";
        int success = -1;
        int error = 0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Menu_Edit_Activity.this);
            pDialog.setMessage("Load data. Please Wait..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            if (spinCategori.getSelectedItemPosition() == 0) {
                id_category = "1";
            } else if (spinCategori.getSelectedItemPosition() == 1) {
                id_category = "2";
            } else {
                id_category = "3";
            }

            if (spinStatus.getSelectedItemPosition() == 0) {
                id_status = "1";
            } else if (spinStatus.getSelectedItemPosition() == 1) {
                id_status = "0";
            } else if (spinStatus.getSelectedItemPosition() == 2) {
                id_status = "2";
            }

            sellerID = id_seller;
            menuID = menu_id;
            menu_catergoryID = id_category;
            menu_name = txtName.getText().toString();
            menu_deskrisi = txtDeskripsi.getText().toString();
            menu_harga = txtHarga.getText().toString();
            menu_status = id_status;

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("menu_id", menuID));
            params.add(new BasicNameValuePair("seller_id", sellerID));
            params.add(new BasicNameValuePair("category_id", menu_catergoryID));
            params.add(new BasicNameValuePair("nama_menu", menu_name));
            params.add(new BasicNameValuePair("deskripsi", menu_deskrisi));
            params.add(new BasicNameValuePair("img", imgBaru));
            params.add(new BasicNameValuePair("harga", menu_harga));
            params.add(new BasicNameValuePair("status", menu_status));

            JSONObject json = jparser.makeHttpRequest(updateMenuUrl, "POST", params);

            try {
                success = json.getInt("success");
                s = json.getString("message");
            } catch (Exception e) {
                error = 1;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String ss) {
            super.onPostExecute(ss);
            pDialog.dismiss();

            if (success == 1) {
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
                Intent i = new Intent(Menu_Edit_Activity.this, MainActivity.class);
                startActivity(i);
            }

            if (error == 1) {
                if (Util.isConnectingToInternet(Menu_Edit_Activity.this)) {
                    Toast.makeText(Menu_Edit_Activity.this, "Server Down, Please Try Again", Toast.LENGTH_SHORT).show();
                } else {
                    Util.showNoInternetDialog(Menu_Edit_Activity.this);
                }
                return;
            }
        }
    }

    class DeleteMenu extends AsyncTask<String, String, String> {
        ProgressDialog pDialog;
        String s = "";
        int success = -1;
        int error = 0;
        String menuID = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Menu_Edit_Activity.this);
            pDialog.setMessage("Load data. Please Wait..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            menuID = menu_id;

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("menu_id", menuID));

            JSONObject json = jparser.makeHttpRequest(deleteMenuUrl, "POST", params);

            try {
                success = json.getInt("success");
                s = json.getString("message");
            } catch (Exception e) {
                error = 1;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();

            if (success == 1) {
                Toast.makeText(getApplicationContext(), "Data Berhasil Dihapus", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(Menu_Edit_Activity.this, MainActivity.class);
                startActivity(i);
            }

            if (error == 1) {
                if (Util.isConnectingToInternet(Menu_Edit_Activity.this)) {
                    Toast.makeText(Menu_Edit_Activity.this, "Server Down, Please Try Again", Toast.LENGTH_SHORT).show();
                } else {
                    Util.showNoInternetDialog(Menu_Edit_Activity.this);
                }
                return;
            }
        }
    }

    private void setToImageView(Bitmap bmp) {
        //compress image
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, bitmap_size, bytes);
        decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(bytes.toByteArray()));

        //menampilkan gambar yang dipilih dari camera/gallery ke ImageView
        imageView.setImageBitmap(decoded);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //mengambil fambar dari Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                // 512 adalah resolusi tertinggi setelah image di resize, bisa di ganti.
                setToImageView(getResizedBitmap(bitmap, 512));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, bitmap_size, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void initComponent() {
        txtName = findViewById(R.id.detail_textNameMenu);
        spinCategori = findViewById(R.id.detail_textCategory);
        spinStatus = findViewById(R.id.detail_textStatus);
        txtDeskripsi = findViewById(R.id.detail_textDescription);
        txtHarga = findViewById(R.id.detail_textHarga);
        bt_edit = findViewById(R.id.menu_bt_add);
        bt_delete = findViewById(R.id.menu_bt_delete);
        mNetworkImageView = findViewById(R.id.ivMenu);
        imageView = findViewById(R.id.imageView);
        pilihGambar = findViewById(R.id.menu_pilihGambar);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
