package baledigital.foodcourt.seller;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import baledigital.foodcourt.seller.Utility.CustomVolleyRequestQueue;
import baledigital.foodcourt.seller.Utility.JSONParser;
import baledigital.foodcourt.seller.Utility.Mainlink;
import baledigital.foodcourt.seller.Utility.SharedPref;
import baledigital.foodcourt.seller.Utility.Util;

public class Menu_List_Activity extends AppCompatActivity {

    Button bt_add_menu;
    GridView requestList;
    Spinner spinCategoryMenu;

    JSONParser jparser = new JSONParser();
    public static final String getMenu = Mainlink.MainLinkIP+"menu.php";
    public static final String getCategoryMenu = Mainlink.MainLinkIP+"category_menu.php";
    ArrayList<HashMap<String, String>> rides;
    String seller_id = "", id_category = "1";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_list);

        seller_id = SharedPref.getSharedPreference(getApplicationContext(), "id_seller");

        new GetDataMenu().execute();
        new GetDataCategoryMenu(spinCategoryMenu).execute();

        spinCategoryMenu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                new GetDataMenu().execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        bt_add_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Menu_Add_Activity.class);
                startActivity(i);
            }
        });

        requestList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Menu_Edit_Activity.request=rides.get(i);
                startActivityForResult(new Intent(getApplicationContext(), Menu_Edit_Activity.class), 101);
            }
        });
    }

    class GetDataMenu extends AsyncTask<String, String, String> {
        ProgressDialog pDialog;
        String s="";
        int success=-1;
        int error=0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog=new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Load data. Please Wait..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("id_seller", seller_id));
            params.add(new BasicNameValuePair("id_category", spinCategoryMenu.getSelectedItemPosition()+""));

            try {
                JSONObject json = jparser.makeHttpRequest(getMenu, "POST", params);
                success=json.getInt("success");
                s=json.getString("message");

                if(success==1) {
                    rides = new ArrayList<HashMap<String, String>>();
                    JSONArray jAr = json.getJSONArray("ridelist");
                    for (int i = 0; i < jAr.length(); i++) {
                        JSONObject job=jAr.getJSONObject(i);
                        HashMap<String , String> map=new HashMap<String, String>();
                        map.put("menu_id", job.getString("menu_id"));
                        map.put("seller_id", job.getString("seller_id"));
                        map.put("category_id", job.getString("category_id"));
                        map.put("nama_menu", job.getString("nama_menu"));
                        map.put("deskripsi", job.getString("deskripsi"));
                        map.put("img", job.getString("img"));
                        map.put("harga", job.getString("harga"));
                        map.put("status", job.getString("status"));
                        map.put("category", job.getString("category"));

                        if(spinCategoryMenu.getSelectedItemPosition()==0) {
                            rides.add(map);
                        } else if (spinCategoryMenu.getSelectedItemPosition()==1) {
                            if(job.getString("category_id").equals("1"))
                                rides.add(map);
                        } else if (spinCategoryMenu.getSelectedItemPosition()==2) {
                            if(job.getString("category_id").equals("2"))
                                rides.add(map);
                        } else if (spinCategoryMenu.getSelectedItemPosition()==3) {
                            if(job.getString("category_id").equals("3"))
                                rides.add(map);
                        }
                        rides.add(map);
                    }
                }

            } catch (Exception e){
                error = 1;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String ss) {
            super.onPostExecute(ss);
            pDialog.dismiss();

            if(error==1){
                if(Util.isConnectingToInternet(getApplicationContext())){
                    Toast.makeText(getApplicationContext(), "Server down, Please Try Again", Toast.LENGTH_SHORT).show();
                } else {
                    Util.showNoInternetDialog(getApplicationContext());
                }
                return;
            }

            if(success==1){
                ListAdapter adapter = new ListAdapter(getApplicationContext());
                requestList.setAdapter(adapter);
            }else{
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
            }
        }

        class ListAdapter extends BaseAdapter {

            private static final String TAG = "ManateeAdapter";
            private int convertViewCounter = 0;
            private Context mContext;
            private LayoutInflater mInflater;

            public class ViewHolder {
                NetworkImageView image;
                TextView nameMenu;
                TextView hargaMenu;
                TextView statusMenu;
                TextView categoryMenu;
            }

            private int[] manatees = {R.drawable.logo, R.drawable.logo};

            private Bitmap[] manateeImages = new Bitmap[manatees.length];
            private Bitmap[] manateeThumbs = new Bitmap[manatees.length];

            public ListAdapter(Context context) {
                Log.v(TAG, "Constructing ManateeAdapter");
                this.mContext = context;
                mInflater = LayoutInflater.from(context);

//                for(int i=0; i<manatees.length; i++) {
//                    manateeImages[i] = BitmapFactory.decodeResource(
//                            context.getResources(), manatees[i]);
//                    manateeThumbs[i] = Bitmap.createScaledBitmap(manateeImages[i],
//                            100, 100, false);
//                }
            }

            @Override
            public int getCount() {
                return rides.size();
            }

            @Override
            public Object getItem(int i) {
                return rides.get(i);
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public View getView(int i, View view, ViewGroup parent) {
                ViewHolder holder;

                if (view == null) {
                    view = mInflater.inflate(R.layout.item_menu_layout, null);
                    convertViewCounter++;
                    Log.v(TAG, convertViewCounter + " convertViews have been created");

                    holder = new ViewHolder();
                    holder.image = view.findViewById(R.id.itemImg);
                    holder.nameMenu = view.findViewById(R.id.itemNamaMenu);
                    holder.hargaMenu = view.findViewById(R.id.itemHarga);
                    holder.statusMenu = view.findViewById(R.id.itemStatus);
                    holder.categoryMenu = view.findViewById(R.id.itemCategory);

                    ImageLoader mImageLoader;

                    holder.nameMenu.setText(rides.get(i).get("nama_menu"));
                    holder.hargaMenu.setText("Rp : "+rides.get(i).get("harga"));
                    holder.categoryMenu.setText(rides.get(i).get("category"));

                    if (rides.get(i).get("status").equals("0")) {
                        holder.statusMenu.setText("Habis");
                        holder.statusMenu.setTextColor(Color.parseColor("#bc2327"));
                    } else {
                        holder.statusMenu.setText("Tersedia");
                        holder.statusMenu.setTextColor(Color.parseColor("#41b409"));
                    }

                    mImageLoader = CustomVolleyRequestQueue.getInstance(getApplicationContext()).getImageLoader();
                    final String url = Mainlink.locationPhotoMenuList+rides.get(i).get("img");
                    mImageLoader.get(url, ImageLoader.getImageListener(holder.image, R.mipmap.ic_launcher, android.R.id.icon1));
                    holder.image.setImageUrl(url, mImageLoader);

                    view.setTag(holder);
                } else {
                    holder = (ViewHolder) view.getTag();
                }

                return view;
            }
        }
    }

    class GetDataCategoryMenu extends AsyncTask<String, String, String> {
        ProgressDialog pDialog;
        String s="";
        int success=-1;
        int error=0;

        private Spinner spinCategoryMenu;

        public GetDataCategoryMenu(Spinner spinCategoryMenu) {
            this.spinCategoryMenu = spinCategoryMenu;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog=new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Load data. Please Wait..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            String excategory="Semua kategori,Makanan,Minuman,Apparel";
            try {
                JSONObject json = jparser.makeHttpRequest(getCategoryMenu, "POST", params);
                success=json.getInt("success");
                s=json.getString("message");

                if(success==1) {
                    JSONArray jAr = json.getJSONArray("ridelist");
                    excategory = "Semua kategori,";
                    for (int i = 0; i < jAr.length(); i++) {
                        JSONObject job=jAr.getJSONObject(i);
                        excategory += job.getString("category")+",";
                    }
                }

            } catch (Exception e){
                error = 1;
            }
            return excategory;
        }

        @Override
        protected void onPostExecute(String ss) {
            super.onPostExecute(ss);
            pDialog.dismiss();
            ss = ss.substring(0, ss.length() - 1);
            String[] category=ss.split(",");
            spinCategoryMenu.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_dropdown_item_1line, category));
            if(error==1){
                if(Util.isConnectingToInternet(getApplicationContext())){
                    Toast.makeText(getApplicationContext(), "Server down, Please Try Again", Toast.LENGTH_SHORT).show();
                } else {
                    Util.showNoInternetDialog(getApplicationContext());
                }
                return;
            }

            if(success==1){
                Log.d("hasil", s);
            }else{
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==101 && resultCode==RESULT_OK)
            new GetDataMenu().execute();
    }
}
