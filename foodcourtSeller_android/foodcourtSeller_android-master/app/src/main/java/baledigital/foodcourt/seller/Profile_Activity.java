package baledigital.foodcourt.seller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import baledigital.foodcourt.seller.Utility.CommonUtils;
import baledigital.foodcourt.seller.Utility.JSONParser;
import baledigital.foodcourt.seller.Utility.Mainlink;
import baledigital.foodcourt.seller.Utility.SharedPref;
import baledigital.foodcourt.seller.Utility.Util;

public class Profile_Activity extends AppCompatActivity {

    JSONParser jparser = new JSONParser();
    public static final String getAccount = Mainlink.MainLinkIP+"account.php";
    public static final String updateAccount = Mainlink.MainLinkIP+"edit_account.php";
    ArrayList<HashMap<String, String>> rides;

    EditText edit_namaDagang, edit_namaPemilik, edit_email, edit_telp, edit_alamat, edit_password;
    Button bt_save;
    Spinner spinnerStatus;
    ScrollView scroll;

    String seller_id = "";
    String xnamaDagang = "", xnamaPemilik = "", xemail = "", xtelp = "", xalamat = "", shaPassword = "", password = "", xStatus="";
    String statusSeller[] = {"Tutup Lapak", "Buka Lapak"};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_layout);

        initComponent();

        seller_id = SharedPref.getSharedPreference(getApplicationContext(), "id_seller");

        new GetAccount().execute();

        spinnerStatus.setAdapter(new ArrayAdapter<String>(getApplicationContext()	, android.R.layout.simple_dropdown_item_1line, statusSeller));

        //Toast.makeText(getContext(), xStatus, Toast.LENGTH_SHORT).show();

        try {
            shaPassword = URLEncoder.encode(CommonUtils.getSha1Hex(edit_password.getText().toString()), Mainlink.CHARSET);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        bt_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getContext(), radioBuka.getText(), Toast.LENGTH_SHORT).show();
                //new UpdateAccount().execute();
            }
        });

    }

    class GetAccount extends AsyncTask<String, String, String> {
        ProgressDialog pDialog;
        String s="";
        int success=-1;
        int error=0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog=new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Load data. Please Wait..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("id_seller", seller_id));

            try {
                JSONObject json = jparser.makeHttpRequest(getAccount, "POST", params);

                success=json.getInt("success");
                s=json.getString("message");

                if(success==1) {
                    rides = new ArrayList<HashMap<String, String>>();
                    JSONArray jAr = json.getJSONArray("ridelist");
                    for (int i = 0; i < jAr.length(); i++) {
                        JSONObject job=jAr.getJSONObject(i);
                        HashMap<String , String> map=new HashMap<String, String>();
                        map.put("seller_id", job.getString("seller_id"));
                        map.put("nama_dagang", job.getString("nama_dagang"));
                        map.put("nama_pemilik", job.getString("nama_pemilik"));
                        map.put("email", job.getString("email"));
                        map.put("telp", job.getString("telp"));
                        map.put("alamat", job.getString("alamat"));
                        map.put("status", job.getString("status"));

                        rides.add(map);
                    }
                }

            } catch (Exception e){
                error = 1;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();

            if(error==1){
                if(Util.isConnectingToInternet(getApplicationContext())){
                    Toast.makeText(getApplicationContext(), "Server down, Please Try Again", Toast.LENGTH_SHORT).show();
                } else {
                    Util.showNoInternetDialog(getApplicationContext());
                }
                return;
            }

            if(success==1){

                for (int i = 0; i < rides.size(); i++) {
                    xnamaDagang = rides.get(i).get("nama_dagang");
                    xnamaPemilik = rides.get(i).get("nama_pemilik");
                    xemail = rides.get(i).get("email");
                    xtelp = rides.get(i).get("telp");
                    xalamat = rides.get(i).get("alamat");
                    xStatus = rides.get(i).get("status");

                    edit_namaDagang.setText(xnamaDagang);
                    edit_namaPemilik.setText(xnamaPemilik);
                    edit_email.setText(xemail);
                    edit_telp.setText(xtelp);
                    edit_alamat.setText(xalamat);

                    spinnerStatus.setSelection(Integer.parseInt(xStatus));
                }
            }else{
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
            }
        }
    }

    class UpdateAccount extends AsyncTask<String, String, String> {
        ProgressDialog pDialog;
        String s="";
        int success=-1;
        int error=0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog=new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Load data. Please Wait..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            if (spinnerStatus.getSelectedItemPosition()==0) {
                xStatus = "0";
            } else if (spinnerStatus.getSelectedItemPosition()==1) {
                xStatus = "1";
            }


            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("seller_id", seller_id));
            params.add(new BasicNameValuePair("nama_dagang", edit_namaDagang.getText().toString()));
            params.add(new BasicNameValuePair("nama_pemilik", edit_namaPemilik.getText().toString()));
            params.add(new BasicNameValuePair("email", edit_email.getText().toString()));
            params.add(new BasicNameValuePair("telp", edit_telp.getText().toString()));
            params.add(new BasicNameValuePair("alamat", edit_alamat.getText().toString()));
            params.add(new BasicNameValuePair("password", edit_password.getText().toString()));
            params.add(new BasicNameValuePair("status", xStatus));

            JSONObject json = jparser.makeHttpRequest(updateAccount, "POST", params);

            try {
                success = json.getInt("success");
                s = json.getString("message");
            } catch (Exception e) {
                error = 1;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();

            if (success == 1) {
                Toast.makeText(getApplicationContext(), "Data Berhasil Diupdate", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
            }

            if(error==1){
                if(Util.isConnectingToInternet(getApplicationContext())){
                    Toast.makeText(getApplicationContext(), "Server Down, Please Try Again", Toast.LENGTH_SHORT).show();
                } else {
                    Util.showNoInternetDialog(getApplicationContext());
                }
                return;
            }
        }
    }

    public void initComponent() {
        edit_namaDagang = findViewById(R.id.account_namaDagang);
        edit_namaPemilik = findViewById(R.id.account_namaPemilik);
        edit_email = findViewById(R.id.account_email);
        edit_telp = findViewById(R.id.account_telp);
        edit_alamat = findViewById(R.id.account_alamat);
        edit_password = findViewById(R.id.account_password);
        bt_save = findViewById(R.id.account_bt_save);
        spinnerStatus = findViewById(R.id.spin_status);
    }
}
