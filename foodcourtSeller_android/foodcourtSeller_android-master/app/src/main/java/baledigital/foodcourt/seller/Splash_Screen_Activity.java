package baledigital.foodcourt.seller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import baledigital.foodcourt.seller.R;

public class Splash_Screen_Activity extends Activity{

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen_layout);

        new Handler().postDelayed(new Thread() {
            @Override
            public void run() {
                Intent toLanguage = new Intent(getApplicationContext(), Login_Activity.class);
                startActivity(toLanguage);
                finish();
            }
        }, 1000);
    }

}
