package baledigital.foodcourt.seller;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import baledigital.foodcourt.seller.Utility.CustomVolleyRequestQueue;
import baledigital.foodcourt.seller.Utility.JSONParser;
import baledigital.foodcourt.seller.Utility.Mainlink;
import baledigital.foodcourt.seller.Utility.SharedPref;
import baledigital.foodcourt.seller.Utility.Util;

public class Transaksi_Detail_Activity extends AppCompatActivity {

    public static HashMap<String, String> request;

    JSONParser jparser = new JSONParser();

    String transID = "", trans_detail_id = "", id_seller = "", qtyAwal = "", qtyAkhir = "", img = "", status = "";
    Button bt_update, bt_edit, bt_batal;
    TextView transId, transMeja, transStatus, transMenu, transQTY, transNama, transHarga, transTgl,transNote;
    String nama_menu = "", qty = "", nama_lengkap = "", harga = "", date_trans = "", no_meja = "", note = "";

    ArrayList<HashMap<String, String>> rides;

    NetworkImageView mNetworkImageView;
    ImageLoader mImageLoader;

    public static final String getTransaksi = Mainlink.MainLinkIP+"transaksi_detail.php";
    public static final String updateDetailTrans = Mainlink.MainLinkIP+"edit_detail_transaksi.php";
    public static final String cancelOrder = Mainlink.MainLinkIP+"cancel_transaksi.php";
    public static final String updateProsesUrl = Mainlink.MainLinkIP+"edit_status_transaksi_proses.php";
    public static final String updateSudahUrl = Mainlink.MainLinkIP+"edit_status_transaksi_sudah.php";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transaksi_detail_layout);
        initComponent();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        id_seller = SharedPref.getSharedPreference(getApplicationContext(), "id_seller");

        if(getIntent().getExtras() == null) {
            trans_detail_id = request.get("trans_detail_id");
            new GetDetailTransaksi().execute();
            //Toast.makeText(getApplicationContext(), "No Notif"+trans_detail_id, Toast.LENGTH_LONG).show();

        } else {
            trans_detail_id = getIntent().getExtras().getString("kirim_Id_transDetail");
            new GetDetailTransaksi().execute();
            //Toast.makeText(getApplicationContext(), "With Notif"+trans_detail_id, Toast.LENGTH_LONG).show();
        }


        bt_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Transaksi_Detail_Activity.this, Transaksi_Detail_Edit_Activity.class);
                i.putExtra("trans_detail_id", trans_detail_id);
                startActivity(i);
            }
        });

        bt_batal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder dialogDelete = new AlertDialog.Builder(Transaksi_Detail_Activity.this);

                dialogDelete.setTitle("Peringatan!");
                dialogDelete.setMessage("Apakah Anda Yakin Membataklkan Order?");
                dialogDelete.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            new CancelOrder().execute();
                        } catch (Exception e){
                            Log.e("error", e.getMessage());
                        }
                    }
                });

                dialogDelete.setNegativeButton("Batalkan", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialogDelete.show();
            }
        });

        bt_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder dialogDelete = new AlertDialog.Builder(Transaksi_Detail_Activity.this);

                dialogDelete.setMessage("Pilih Status");
                dialogDelete.setPositiveButton("Diproses", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final AlertDialog.Builder dialogDelete = new AlertDialog.Builder(Transaksi_Detail_Activity.this);

                        dialogDelete.setMessage("Apakah Anda Yakin Status Dirubah 'Proses'?");
                        dialogDelete.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    new UpdateProses().execute();
                                } catch (Exception e){
                                    Log.e("error", e.getMessage());
                                }
                            }
                        });

                        dialogDelete.setNegativeButton("Kembali", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        dialogDelete.show();
                    }
                });

                dialogDelete.setNegativeButton("Sudah Jadi", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final AlertDialog.Builder dialogDelete = new AlertDialog.Builder(Transaksi_Detail_Activity.this);

                        dialogDelete.setMessage("Apakah Anda Yakin Status Dirubah 'Sudah Jadi'?");
                        dialogDelete.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    new UpdateSelesai().execute();
                                } catch (Exception e){
                                    Log.e("error", e.getMessage());
                                }
                            }
                        });

                        dialogDelete.setNegativeButton("Kembali", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        dialogDelete.show();
                    }
                });
                dialogDelete.show();
            }
        });
    }

    class GetDetailTransaksi extends AsyncTask<String, String, String> {
        ProgressDialog pDialog;
        String s="";
        int success=-1;
        int error=0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog=new ProgressDialog(Transaksi_Detail_Activity.this);
            pDialog.setMessage("Load data. Please Wait..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("id_seller", id_seller));
            params.add(new BasicNameValuePair("trans_detail_id", trans_detail_id));

            try {
                JSONObject json = jparser.makeHttpRequest(getTransaksi, "POST", params);
                success=json.getInt("success");
                s=json.getString("message");

                if(success==1) {
                    rides = new ArrayList<HashMap<String, String>>();
                    JSONArray jAr = json.getJSONArray("ridelist");
                    for (int i = 0; i < jAr.length(); i++) {
                        JSONObject job=jAr.getJSONObject(i);
                        HashMap<String , String> map=new HashMap<String, String>();
                        map.put("trans_detail_id", job.getString("trans_detail_id"));
                        map.put("trans_id", job.getString("trans_id"));
                        map.put("menu_id", job.getString("menu_id"));
                        map.put("qty", job.getString("qty"));
                        map.put("harga", job.getString("harga"));
                        map.put("status_order", job.getString("status_order"));
                        map.put("customer_id", job.getString("customer_id"));
                        map.put("no_table", job.getString("no_table"));
                        map.put("date_trans", job.getString("date_trans"));
                        map.put("nama_lengkap", job.getString("nama_lengkap"));
                        map.put("nama_menu", job.getString("nama_menu"));
                        map.put("img", job.getString("img"));
                        map.put("note", job.getString("note"));

                        rides.add(map);
                    }
                }

            } catch (Exception e){
                error = 1;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String ss) {
            super.onPostExecute(ss);
            pDialog.dismiss();

            if(error==1){
                if(Util.isConnectingToInternet(Transaksi_Detail_Activity.this)){
                    Toast.makeText(Transaksi_Detail_Activity.this, "Server down, Please Try Again", Toast.LENGTH_SHORT).show();
                } else {
                    Util.showNoInternetDialog(Transaksi_Detail_Activity.this);
                }
                return;
            }

            if(success==1){
                for (int i = 0; i < rides.size(); i++) {
                    transId.setText(rides.get(i).get("trans_id"));
                    transMeja.setText("Meja ("+rides.get(i).get("no_table")+")");
                    if(rides.get(i).get("no_table").equals(""))
                    {
                        transMeja.setText("Meja tidak tersedia");
                    }
                    transStatus.setText(rides.get(i).get("status_order"));

                    if (rides.get(i).get("status_order").equals("0")) {
                        transStatus.setText("Pesan");
                        transStatus.setTextColor(Color.parseColor("#ff0505"));
                    } else if (rides.get(i).get("status_order").equals("1")) {
                        transStatus.setText("Diproses");
                        transStatus.setTextColor(Color.parseColor("#3fcffe"));
                    } else if (rides.get(i).get("status_order").equals("2")) {
                        transStatus.setText("Selesai");
                        transStatus.setTextColor(Color.parseColor("#36d62a"));
                    } else {
                        transStatus.setText("Batal");
                        transStatus.setTextColor(Color.parseColor("#ff0505"));
                    }

                    if(rides.get(i).get("status_order").equals("1")) {
                        bt_edit.setVisibility(View.GONE);
                    } else if (rides.get(i).get("status_order").equals("2")) {
                        bt_edit.setVisibility(View.GONE);
                        bt_update.setVisibility(View.GONE);
                        bt_batal.setVisibility(View.GONE);

                    }

                    transMenu.setText(rides.get(i).get("nama_menu"));
                    transQTY.setText("QTY ("+rides.get(i).get("qty")+")");
                    transNama.setText(rides.get(i).get("nama_lengkap"));
                    transHarga.setText("Rp "+rides.get(i).get("harga"));
                    transTgl.setText(rides.get(i).get("date_trans"));
                    img = rides.get(i).get("img");
                }

                mImageLoader = CustomVolleyRequestQueue.getInstance(Transaksi_Detail_Activity.this).getImageLoader();
                final String url = Mainlink.locationPhotoMenu+img;
                mImageLoader.get(url, ImageLoader.getImageListener(mNetworkImageView, R.mipmap.ic_launcher, android.R.id.icon1));
                mNetworkImageView.setImageUrl(url, mImageLoader);

            }else{
                Toast.makeText(Transaksi_Detail_Activity.this, s, Toast.LENGTH_SHORT).show();
            }
        }
    }

    class GetDetailTransaksiBeta extends AsyncTask<String, String, String> {
        ProgressDialog pDialog;
        String s="";
        int success=-1;
        int error=0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog=new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Load data. Please Wait..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("id_seller", id_seller));
            params.add(new BasicNameValuePair("trans_detail_id", trans_detail_id));

            try {
                JSONObject json = jparser.makeHttpRequest(getTransaksi, "POST", params);
                success = json.getInt("success");
                s = json.getString("message");

                trans_detail_id = json.getString("trans_detail_id");
                transID = json.getString("trans_id");
                img = json.getString("img");
                status = json.getString("status_order");
                qty = "Qty ("+json.getString("qty")+")";
                nama_menu = json.getString("nama_menu");
                nama_lengkap = json.getString("nama_lengkap");
                harga = "Rp "+json.getString("harga");
                date_trans = json.getString("date_trans");
                no_meja = "Meja ("+json.getString("no_table")+")";
                note = json.getString("note");


                if(json.getString("no_table").equals(""))
                {
                    no_meja="Meja tidak tersedia";
                }

                Log.d("transID",trans_detail_id);

            } catch (Exception e){
                error = 1;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();

            if(error==1){
                if(Util.isConnectingToInternet(getApplicationContext())){
                    Toast.makeText(getApplicationContext(), "Server down, Please Try Again", Toast.LENGTH_SHORT).show();
                } else {
                    Util.showNoInternetDialog(getApplicationContext());
                }
                return;
            }

            if(success==1){
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
            }
        }
    }

    class UpdateQTY extends AsyncTask<String, String, String> {
        ProgressDialog pDialog;
        String s="";
        int success=-1;
        int error=0;
        String menuID = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog=new ProgressDialog(Transaksi_Detail_Activity.this);
            pDialog.setMessage("Load data. Please Wait..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("id_seller", id_seller));
            params.add(new BasicNameValuePair("trans_detail_id", trans_detail_id));
            params.add(new BasicNameValuePair("qty", qtyAkhir));

            JSONObject json = jparser.makeHttpRequest(updateDetailTrans, "POST", params);

            try {
                success = json.getInt("success");
                s = json.getString("message");
            } catch (Exception e) {
                error = 1;
            }

            return null;
        }

        @Override
        protected void onPostExecute(String ss) {
            super.onPostExecute(ss);
            pDialog.dismiss();

            if (success == 1) {
                Toast.makeText(getApplicationContext(), "Data Berhasil Diupdate", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(Transaksi_Detail_Activity.this, MainActivity.class);
                startActivity(i);
            } else {
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
            }

            if(error==1){
                if(Util.isConnectingToInternet(Transaksi_Detail_Activity.this)){
                } else {
                    Util.showNoInternetDialog(Transaksi_Detail_Activity.this);
                }
                return;
            }
        }
    }

    class CancelOrder extends AsyncTask<String, String, String> {
        ProgressDialog pDialog;
        String s="";
        int success=-1;
        int error=0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog=new ProgressDialog(Transaksi_Detail_Activity.this);
            pDialog.setMessage("Load data. Please Wait..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("id_seller", id_seller));
            params.add(new BasicNameValuePair("trans_detail_id", request.get("trans_detail_id")));

            JSONObject json = jparser.makeHttpRequest(cancelOrder, "POST", params);

            try {
                success = json.getInt("success");
                s = json.getString("message");
            } catch (Exception e) {
                error = 1;
            }

            return null;
        }

        @Override
        protected void onPostExecute(String ss) {
            super.onPostExecute(ss);
            pDialog.dismiss();

            if (success == 1) {
                Toast.makeText(getApplicationContext(), "Data Berhasil Dibatalkan", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(Transaksi_Detail_Activity.this, MainActivity.class);
                startActivity(i);
            } else {
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
            }

            if(error==1){
                if(Util.isConnectingToInternet(Transaksi_Detail_Activity.this)){
                } else {
                    Util.showNoInternetDialog(Transaksi_Detail_Activity.this);
                }
                return;
            }
        }
    }

    class UpdateProses extends AsyncTask<String, String, String> {
        ProgressDialog pDialog;
        String s="";
        int success=-1;
        int error=0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog=new ProgressDialog(Transaksi_Detail_Activity.this);
            pDialog.setMessage("Load data. Please Wait..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("id_seller", id_seller));
            params.add(new BasicNameValuePair("trans_detail_id", request.get("trans_detail_id")));

            JSONObject json = jparser.makeHttpRequest(updateProsesUrl, "POST", params);

            try {
                success = json.getInt("success");
                s = json.getString("message");
            } catch (Exception e) {
                error = 1;
            }

            return null;
        }

        @Override
        protected void onPostExecute(String ss) {
            super.onPostExecute(ss);
            pDialog.dismiss();

            if (success == 1) {
                Toast.makeText(getApplicationContext(), "Status Transaksi Proses", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(Transaksi_Detail_Activity.this, MainActivity.class);
                startActivity(i);
            } else {
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
            }

            if(error==1){
                if(Util.isConnectingToInternet(Transaksi_Detail_Activity.this)){
                } else {
                    Util.showNoInternetDialog(Transaksi_Detail_Activity.this);
                }
                return;
            }
        }
    }

    class UpdateSelesai extends AsyncTask<String, String, String> {
        ProgressDialog pDialog;
        String s="";
        int success=-1;
        int error=0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog=new ProgressDialog(Transaksi_Detail_Activity.this);
            pDialog.setMessage("Load data. Please Wait..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("id_seller", id_seller));
            params.add(new BasicNameValuePair("trans_detail_id", request.get("trans_detail_id")));

            JSONObject json = jparser.makeHttpRequest(updateSudahUrl, "POST", params);

            try {
                success = json.getInt("success");
                s = json.getString("message");
            } catch (Exception e) {
                error = 1;
            }

            return null;
        }

        @Override
        protected void onPostExecute(String ss) {
            super.onPostExecute(ss);
            pDialog.dismiss();

            if (success == 1) {
                Toast.makeText(getApplicationContext(), "Status Transaksi Sudah Jadi", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(Transaksi_Detail_Activity.this, MainActivity.class);
                startActivity(i);
            } else {
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
            }

            if(error==1){
                if(Util.isConnectingToInternet(Transaksi_Detail_Activity.this)){
                } else {
                    Util.showNoInternetDialog(Transaksi_Detail_Activity.this);
                }
                return;
            }
        }
    }

    public void initComponent() {
        mNetworkImageView = findViewById(R.id.ivTransaksi);
        bt_update = findViewById(R.id.detTrans_bt_update);
        bt_edit = findViewById(R.id.detTrans_bt_edit);
        bt_batal = findViewById(R.id.detTrans_bt_cancel);
        transId = findViewById(R.id.detTrans_id);
        transMeja = findViewById(R.id.detTrans_noMeja);
        transStatus = findViewById(R.id.detTrans_status);
        transMenu = findViewById(R.id.detTrans_namaMenu);
        transQTY = findViewById(R.id.detTrans_qty);
        transNama = findViewById(R.id.detTrans_namaPanggilan);
        transHarga = findViewById(R.id.detTrans_total);
        transTgl = findViewById(R.id.detTrans_tgl);
        transNote = findViewById(R.id.detTrans_note);
    }

}
