package baledigital.foodcourt.seller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import baledigital.foodcourt.seller.Utility.CustomVolleyRequestQueue;
import baledigital.foodcourt.seller.Utility.JSONParser;
import baledigital.foodcourt.seller.Utility.Mainlink;
import baledigital.foodcourt.seller.Utility.SharedPref;
import baledigital.foodcourt.seller.Utility.Util;

public class Transaksi_Detail_Edit_Activity extends AppCompatActivity {

    public static HashMap<String, String> request;
    Button bt_simpan, bt_batal;
    JSONParser jparser = new JSONParser();
    TextView transId, transMeja, transStatus, transMenu, transQTY, transNama, transHarga, transTgl;
    ArrayList<HashMap<String, String>> rides;

    String seller_id = "", trans_detailID = "", qty = "", img = "";
    EditText editQTY;
    SharedPreferences sh;

    NetworkImageView mNetworkImageView;
    ImageLoader mImageLoader;

    public static final String getTransaksi = Mainlink.MainLinkIP+"transaksi_hari_ini.php";
    public static final String editQTYUrl = Mainlink.MainLinkIP+"edit_detail_transaksi.php";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transaksi_detail_edit_layout);

        seller_id = SharedPref.getSharedPreference(Transaksi_Detail_Edit_Activity.this, "id_seller");
        trans_detailID = getIntent().getExtras().getString("trans_detail_id");
        qty = sh.getString("qty", null);

        initComponent();
        new GetDetailTransaksi().execute();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        bt_simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new UpdateQTY().execute();
                //Toast.makeText(getApplicationContext(), seller_id+","+trans_detailID+","+editQTY.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    class GetDetailTransaksi extends AsyncTask<String, String, String> {
        ProgressDialog pDialog;
        String s="";
        int success=-1;
        int error=0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog=new ProgressDialog(Transaksi_Detail_Edit_Activity.this);
            pDialog.setMessage("Load data. Please Wait..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("id_seller", seller_id));
            params.add(new BasicNameValuePair("trans_detail_id", trans_detailID));

            try {
                JSONObject json = jparser.makeHttpRequest(getTransaksi, "POST", params);
                success=json.getInt("success");
                s=json.getString("message");

                if(success==1) {
                    rides = new ArrayList<HashMap<String, String>>();
                    JSONArray jAr = json.getJSONArray("ridelist");
                    for (int i = 0; i < jAr.length(); i++) {
                        JSONObject job=jAr.getJSONObject(i);
                        HashMap<String , String> map=new HashMap<String, String>();
                        map.put("trans_detail_id", job.getString("trans_detail_id"));
                        map.put("trans_id", job.getString("trans_id"));
                        map.put("menu_id", job.getString("menu_id"));
                        map.put("qty", job.getString("qty"));
                        map.put("harga", job.getString("harga"));
                        map.put("status_order", job.getString("status_order"));
                        map.put("customer_id", job.getString("customer_id"));
                        map.put("no_table", job.getString("no_table"));
                        map.put("date_trans", job.getString("date_trans"));
                        map.put("nama_lengkap", job.getString("nama_lengkap"));
                        map.put("nama_menu", job.getString("nama_menu"));
                        map.put("img", job.getString("img"));

                        rides.add(map);
                    }
                }

            } catch (Exception e){
                error = 1;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String ss) {
            super.onPostExecute(ss);
            pDialog.dismiss();

            if(error==1){
                if(Util.isConnectingToInternet(Transaksi_Detail_Edit_Activity.this)){
                    Toast.makeText(Transaksi_Detail_Edit_Activity.this, "Server down, Please Try Again", Toast.LENGTH_SHORT).show();
                } else {
                    Util.showNoInternetDialog(Transaksi_Detail_Edit_Activity.this);
                }
                return;
            }

            if(success==1){
                for (int i = 0; i < rides.size(); i++) {
                    transId.setText(rides.get(i).get("trans_id"));
                    transMeja.setText("Meja ("+rides.get(i).get("no_table")+")");
                    if(rides.get(i).get("no_table").equals(""))
                    {
                        transMeja.setText("Meja tidak tersedia");
                    }
                    transStatus.setText(rides.get(i).get("status_order"));

                    if (rides.get(i).get("status_order").equals("0")) {
                        transStatus.setText("Pesan");
                        transStatus.setTextColor(Color.parseColor("#ff0505"));
                    } else if (rides.get(i).get("status_order").equals("1")) {
                        transStatus.setText("Diproses");
                        transStatus.setTextColor(Color.parseColor("#3fcffe"));
                    } else if (rides.get(i).get("status_order").equals("2")) {
                        transStatus.setText("Selesai");
                        transStatus.setTextColor(Color.parseColor("#36d62a"));
                    } else {
                        transStatus.setText("Batal");
                        transStatus.setTextColor(Color.parseColor("#ff0505"));
                    }

                    transMenu.setText(rides.get(i).get("nama_menu"));
                    editQTY.setText(rides.get(i).get("qty"));
                    transNama.setText(rides.get(i).get("nama_lengkap"));
                    transHarga.setText("Rp "+rides.get(i).get("harga"));
                    transTgl.setText(rides.get(i).get("date_trans"));
                    img = rides.get(i).get("img");
                }

                mImageLoader = CustomVolleyRequestQueue.getInstance(Transaksi_Detail_Edit_Activity.this).getImageLoader();
                final String url = Mainlink.locationPhotoMenu+img;
                mImageLoader.get(url, ImageLoader.getImageListener(mNetworkImageView, R.mipmap.ic_launcher, android.R.id.icon1));
                mNetworkImageView.setImageUrl(url, mImageLoader);

            }else{
                Toast.makeText(Transaksi_Detail_Edit_Activity.this, s, Toast.LENGTH_SHORT).show();
            }
        }
    }

    class UpdateQTY extends AsyncTask<String, String, String> {
        ProgressDialog pDialog;
        String s="";
        int success=-1;
        int error=0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog=new ProgressDialog(Transaksi_Detail_Edit_Activity.this);
            pDialog.setMessage("Load data. Please Wait..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("id_seller", seller_id));
            params.add(new BasicNameValuePair("trans_detail_id", trans_detailID));
            params.add(new BasicNameValuePair("qty", editQTY.getText().toString()));

            JSONObject json = jparser.makeHttpRequest(editQTYUrl, "POST", params);

            try {
                success = json.getInt("success");
                s = json.getString("message");
            } catch (Exception e) {
                error = 1;
            }

            return null;
        }

        @Override
        protected void onPostExecute(String ss) {
            super.onPostExecute(ss);
            pDialog.dismiss();

            if (success == 1) {
                Toast.makeText(getApplicationContext(), "Data Berhasil Diupdate", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(Transaksi_Detail_Edit_Activity.this, MainActivity.class);
                startActivity(i);
            } else {
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
            }

            if(error==1){
                if(Util.isConnectingToInternet(Transaksi_Detail_Edit_Activity.this)){
                } else {
                    Util.showNoInternetDialog(Transaksi_Detail_Edit_Activity.this);
                }
                return;
            }
        }
    }

    public void initComponent() {
        mNetworkImageView = findViewById(R.id.ivTransaksi);
        bt_simpan = findViewById(R.id.detTrans_bt_simpan);
        bt_batal = findViewById(R.id.detTrans_bt_batal);
        transId = findViewById(R.id.detTrans_id);
        transMeja = findViewById(R.id.detTrans_noMeja);
        transStatus = findViewById(R.id.detTrans_status);
        transMenu = findViewById(R.id.detTrans_namaMenu);
        editQTY = findViewById(R.id.edit_detTrans_qty);
        transNama = findViewById(R.id.detTrans_namaPanggilan);
        transHarga = findViewById(R.id.detTrans_total);
        transTgl = findViewById(R.id.detTrans_tgl);
    }
}
