package baledigital.foodcourt.seller;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import baledigital.foodcourt.seller.Utility.CustomVolleyRequestQueue;
import baledigital.foodcourt.seller.Utility.JSONParser;
import baledigital.foodcourt.seller.Utility.Mainlink;
import baledigital.foodcourt.seller.Utility.SharedPref;
import baledigital.foodcourt.seller.Utility.Util;

public class Transaksi_List_Activity extends AppCompatActivity {

    ListView transaksiList;
    TextView jml_trans, total_trans;
    JSONParser jparser = new JSONParser();
    public static final String getTransaksi = Mainlink.MainLinkIP+"transaksi_hari_ini.php";
    ArrayList<HashMap<String, String>> rides;

    String seller_id = "",jml_transaksi = "", total_transaksi = "", trans_det_id = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.transaksi_list_layout);

        seller_id = SharedPref.getSharedPreference(getApplicationContext(), "id_seller");

        new GetDetailTransaksi().execute();

        transaksiList = findViewById(R.id.transactionList);
        jml_trans = findViewById(R.id.jml_trans);
        total_trans = findViewById(R.id.total_trans);
    }

    class GetDetailTransaksi extends AsyncTask<String, String, String> {
        ProgressDialog pDialog;
        String s="";
        int success=-1;
        int error=0;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog=new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Load data. Please Wait..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("id_seller", seller_id));

            try {
                JSONObject json = jparser.makeHttpRequest(getTransaksi, "POST", params);
                success=json.getInt("success");
                s=json.getString("message");
                jml_transaksi=json.getString("jumlah_transaksi");
                total_transaksi=json.getString("total_transaksi");

                if(success==1) {
                    rides = new ArrayList<HashMap<String, String>>();
                    JSONArray jAr = json.getJSONArray("ridelist");
                    for (int i = 0; i < jAr.length(); i++) {
                        JSONObject job=jAr.getJSONObject(i);
                        HashMap<String , String> map=new HashMap<String, String>();
                        map.put("trans_detail_id", job.getString("trans_detail_id"));
                        map.put("trans_id", job.getString("trans_id"));
                        map.put("menu_id", job.getString("menu_id"));
                        map.put("qty", job.getString("qty"));
                        map.put("harga", job.getString("harga"));
                        map.put("status_order", job.getString("status_order"));
                        map.put("customer_id", job.getString("customer_id"));
                        map.put("no_table", job.getString("no_table"));
                        map.put("date_trans", job.getString("date_trans"));
                        map.put("nama_lengkap", job.getString("nama_lengkap"));
                        map.put("nama_menu", job.getString("nama_menu"));
                        map.put("img", job.getString("img"));

                        rides.add(map);
                        Log.v("Trans ID", job.getString("trans_id"));
                    }
                }

            } catch (Exception e){
                error = 1;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();

            if(error==1){
                if(Util.isConnectingToInternet(getApplicationContext())){
                    Toast.makeText(getApplicationContext(), "Server down, Please Try Again", Toast.LENGTH_SHORT).show();
                } else {
                    Util.showNoInternetDialog(getApplicationContext());
                }
                return;
            }

            if(success==1){
                jml_trans.setText("Transaksi : "+jml_transaksi);
                total_trans.setText("Total : Rp "+total_transaksi);
                ListAdapter adapter = new ListAdapter(getApplicationContext());
                transaksiList.setAdapter(adapter);
            }else{
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
            }
        }

        class ListAdapter extends BaseAdapter {

            private static final String TAG = "ManateeAdapter";
            private int convertViewCounter = 0;
            private Context mContext;
            private LayoutInflater mInflater;

            public class ViewHolder {
                NetworkImageView image;
                TextView transaksiID;
                TextView namaMenu;
                TextView qty;
                TextView harga;
                TextView status;
                TextView tgl;
                TextView noMeja;
                TextView namaPanggilan;
            }

            private int[] manatees = {R.drawable.logo, R.drawable.logo};

            private Bitmap[] manateeImages = new Bitmap[manatees.length];
            private Bitmap[] manateeThumbs = new Bitmap[manatees.length];

            public ListAdapter(Context context) {
                Log.v(TAG, "Constructing ManateeAdapter");
                this.mContext = context;
                mInflater = LayoutInflater.from(context);

//                for(int i=0; i<manatees.length; i++) {
//                    manateeImages[i] = BitmapFactory.decodeResource(
//                            context.getResources(), manatees[i]);
//                    manateeThumbs[i] = Bitmap.createScaledBitmap(manateeImages[i],
//                            100, 100, false);
//                }
            }

            @Override
            public int getCount() {
                return rides.size();
            }

            @Override
            public Object getItem(int i) {
                return rides.get(i);
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public View getView(int i, View view, ViewGroup parent) {
                ViewHolder holder;

                if (view == null) {
                    view = mInflater.inflate(R.layout.item_transaksi_layout, null);
                    convertViewCounter++;
                    Log.v(TAG, convertViewCounter + " convertViews have been created");

                    holder = new ViewHolder();
                    holder.image = view.findViewById(R.id.transImg);
                    holder.transaksiID = view.findViewById(R.id.transaksi_id);
                    holder.namaMenu = view.findViewById(R.id.transaksi_namaMenu);
                    holder.qty = view.findViewById(R.id.transaksi_qty);
                    holder.harga = view.findViewById(R.id.transaksi_totalHarga);
                    holder.status = view.findViewById(R.id.transaksi_status);
                    holder.tgl = view.findViewById(R.id.transaksi_tgl);
                    holder.noMeja = view.findViewById(R.id.transaksi_noMeja);
                    holder.namaPanggilan = view.findViewById(R.id.transaksi_namaPanggilan);

                    ImageLoader mImageLoader;

                    trans_det_id = rides.get(i).get("trans_detail_id");

                    holder.transaksiID.setText(rides.get(i).get("trans_id"));
                    holder.namaMenu.setText(rides.get(i).get("nama_menu"));
                    holder.qty.setText("Qty ("+rides.get(i).get("qty")+")");
                    holder.harga.setText("Rp "+rides.get(i).get("harga"));

                    if (rides.get(i).get("status_order").equals("0")) {
                        holder.status.setText("Pesan");
                        holder.status.setTextColor(Color.parseColor("#ff0505"));
                    } else if (rides.get(i).get("status_order").equals("1")) {
                        holder.status.setText("Diproses");
                        holder.status.setTextColor(Color.parseColor("#3fcffe"));
                    } else if (rides.get(i).get("status_order").equals("2")) {
                        holder.status.setText("Selesai");
                        holder.status.setTextColor(Color.parseColor("#36d62a"));
                    } else {
                        holder.status.setText("Batal");
                        holder.status.setTextColor(Color.parseColor("#ff0505"));
                    }

                    holder.tgl.setText(rides.get(i).get("date_trans"));
                    holder.noMeja.setText("Meja ("+rides.get(i).get("no_table")+")");
                    if(rides.get(i).get("no_table").equals(""))
                    {
                        holder.noMeja.setText("");
                    }
                    holder.namaPanggilan.setText(rides.get(i).get("nama_lengkap"));

                    mImageLoader = CustomVolleyRequestQueue.getInstance(getApplicationContext()).getImageLoader();
                    final String url = Mainlink.locationPhotoMenuList+rides.get(i).get("img");
                    mImageLoader.get(url, ImageLoader.getImageListener(holder.image, R.mipmap.ic_launcher, android.R.id.icon1));
                    holder.image.setImageUrl(url, mImageLoader);

                }

                return view;
            }
        }
    }
}
