package baledigital.foodcourt.seller.Utility;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPref {

    private static final String PREFERENCE_NAME ="pickme_driver_config";
    public static final String LOGIN="login";

    public static void setSharePreferencesString(Context context, String key, String value){
        SharedPreferences sharedPref=context.getSharedPreferences(PREFERENCE_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key,value);
        editor.apply();
    }

    public static String getSharedPreference(Context context,String key) {
        SharedPreferences settings=context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        String text;
        text = settings.getString(key, null);
        return text;
    }

    public static void clearSharedPreference(Context context) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.clear();
        editor.apply();
    }

}
