<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Home';
$route['404_override'] = '';
// $route['translate_uri_dashes'] = FALSE;

if ($this->uri->segment(1) == 'about') {
	$route['about'] = 'About/index';
}
if ($this->uri->segment(1) == 'tampil_page') {
	$route['tampil_page'] = 'Newpage/index';
}
if ($this->uri->segment(1) == 'adduser') {
	$route['adduser'] = 'Newpage/userbaru';
	$route['adduser/add'] = 'Newpage/Add';
	$route['adduser/delete'] = 'Newpage/DeleteData';
	$route['adduser/datatable'] = 'Newpage/dataTable';
	$route['adduser/update'] = 'Newpage/Update';
	$route['adduser/edit'] = 'Newpage/getEdit';
}
if ($this->uri->segment(1) == 'adminsite') {
	$route['adminsite'] = 'B_Home/index';
	$route['adminsite/actlogin'] = 'B_Login/Actlogin';
	$route['adminsite/actlogout'] = 'B_Login/Actlogout';
}
if ($this->uri->segment(1) == 'adminsite' && $this->uri->segment(2)=='') {
	$route['adminsite'] = 'B_Home/index';
}
if ($this->uri->segment(2) == 'home') {
	$route['adminsite/home'] = 'B_Home/index';
}
if ($this->uri->segment(2) == 'manage_data_add') {
	$route['adminsite/manage_data_add'] = 'B_Manage_data/Manage_data_add';
	$route['adminsite/manage_data_add/showtable'] = 'B_Manage_data/ShowTable';
}
if ($this->uri->segment(2) == 'login') {
	$route['adminsite/login'] = 'B_Login/index';
}

$route['translate_uri_dashes'] = FALSE;