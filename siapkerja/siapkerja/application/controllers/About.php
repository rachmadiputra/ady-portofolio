<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	public function index()
	{	
		$data['head_title']='About';
		$this->load->view('static/head',$data);
		$this->load->view('content/about');
		$this->load->view('static/footer');
	}
}
