<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_Home extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('m_static');
		$this->load->model('mrmodel');
		$this->load->library('Ajax');
		// $this->load->model('mrmodel','',TRUE);
		header('Access-Control-Allow-Origin: *');
		
		// session_start();
		// if(!isset($_SESSION['adminsite_001'])){
		// 	redirect(base_url().'adminsite/login');
		// }
		if(!isset($_SESSION['adminsite_001'])){
			redirect(base_url().'adminsite/login');
		}
	}

	public function index()
	{	
		$data['manage'] = $this->mrmodel->getManageData();
		$data['head_title']='Title Kepala';
		$data['segment3'] = $this->uri->segment(3);
		$data['segment2'] = $this->uri->segment(2);
		$this->load->view('static/backend/head',$data);
		$this->load->view('content/backend/home');
		$this->load->view('static/backend/footer');
	}
}
?>