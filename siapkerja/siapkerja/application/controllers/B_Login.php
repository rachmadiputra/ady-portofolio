<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_Login extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('m_static');
		//$this->load->model('mrmodel','',TRUE);
		$this->load->model('mrmodel');
		$this->load->library('Ajax');
		header('Access-Control-Allow-Origin: *');
		
		// session_start();
	}

	public function index()
	{	
		if(isset($_SESSION['adminsite_001'])){
			redirect(base_url().'adminsite/home');
		}
		$data['head_title']='Login Page';
		$this->load->view('content/backend/login',$data);
		// $this->load->view('content/backend/home');
		// $this->load->view('static/backend/footer');

	}
	function Actlogin()
	{
		$result['url_success']=base_url().'adminsite/home';
		$result['data']="0";
		$result['usern']= $_POST['username'];

		if(isset($_POST['username']) && isset($_POST['password'])){
			$user = $this->m_static->xss($_POST['username']);
			$pass =  $this->m_static->xss($_POST['password']);
			$passSha1 = sha1($pass);

			$getAdmin=$this->mrmodel->getAdmin($user,$passSha1);
			if(count($getAdmin)> 0){
				// $_SESSION['adminsite_001']=$getAdmin[0]->id_admin;
				// $_SESSION['adminsite_name001']=$getAdmin[0]->nama;
				$data_session = array(
                'adminsite_001' => $getAdmin[0]->id_admin,
                'adminsite_name' => $getAdmin[0]->nama,
                'status' => "login"
                );
				// $userdata=$_SESSION['user']=$getAdmin[0]->id_admin;
				// $data_session= $userdata
                $this->session->set_userdata($data_session);
				$result['data']="200";
			}
		}

		echo json_encode($result);
	}

	function Actlogout()
	{
		unset($_SESSION['adminsite_001']);
		// unset($_SESSION['adminsite_name001']);

		redirect(base_url().'adminsite/login');
	}
}
?>