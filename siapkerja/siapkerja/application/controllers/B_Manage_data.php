<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class B_Manage_data extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('m_static');
		//$this->load->model('mrmodel','',TRUE);
		$this->load->model('mrmodel');
		$this->load->library('Ajax');
		header('Access-Control-Allow-Origin: *');
		
		// session_start();
		// if(!isset($_SESSION['adminsite_001'])){
		// 	redirect(base_url().'adminsite/login');
		// }
	}

	public function index()
	{	
		// if(isset($_SESSION['adminsite_001'])){
		// 	redirect(base_url().'adminsite/home');
		// }
		// $data['head_title']='Login Page';
		// $this->load->view('content/backend/login',$data);
		// $this->load->view('content/backend/home');
		// $this->load->view('static/backend/footer');

	}
	function Manage_data_add()
	{
		
		$result['data'] = "0";
		if (isset($_POST['name'])) {
		$nama= $this->m_static->xss($_POST['name']);
		$email = $this->m_static->xss($_POST['email']);
		$number = $this->m_static->xss($_POST['number']);
		$website= $this->m_static->xss($_POST['website']);
		$occupation= $this->m_static->xss($_POST['occupation']);
		$password= $this->m_static->xss($_POST['password']);
		$phone= $this->m_static->xss($_POST['phone']);
		$textarea= $this->m_static->xss($_POST['textarea']);
		$this->mrmodel->add_manage_data($nama,$email,$number,$website,$occupation,$password,$phone,$textarea);
		$result['data']='success';
		}
		echo json_encode($result);
	}

	public function ShowTable()
	{
		$datamanage = $this->mrmodel->getManageData();
			$str='<thead>
			  <th>No</th>
			  <th>Name</th>
			  <th>Email</th>
			  <th>Number</th>
			  <th>Website</th>
			  <th>Occupation</th>
			  <th>Password</th>
			  <th>Phone</th>
			  <th>Text</th>
			  <th>Aksi</th>
			 </thead>
			 <tbody>';
				$no=1;
				foreach($datamanage as $key){
				$str.='<tr>
				<td>'.$no.'</td>
				<td>'.$key->name.'</td>
				<td>'.$key->email.'</td>
				<td>'.$key->number.'</td>
				<td>'.$key->website.'</td>
				<td>'.$key->occupation.'</td>
				<td>'.$key->password.'</td>
				<td>'.$key->phone.'</td>
				<td>'.$key->textarea.'</td>
				<td> <button class="btn btn-warning btn-sm editData" id="btn-edit" data-idname="'.$key->id_data.'">Edit</button> <button class="btn del btn-danger btn-sm" data-id="'.$key->id_data.'" data-toggle="modal" data-target="#modalDelete">Delete</button> </td>
				</tr>';
				
				$no++;
				}

				
		$str.='</tbody>';
		echo $str ;
	}

	
}
?>