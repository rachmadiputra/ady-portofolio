<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{	
		$data['head_title']='Title Frontend';
		$this->load->view('static/frontend/head',$data);
		$this->load->view('content/frontend/home');
		$this->load->view('static/frontend/footer');
	}
}
?>