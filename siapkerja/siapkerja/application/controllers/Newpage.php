<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newpage extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('mrmodel','',TRUE); 
	}

	public function index()
	{	
		$data['head_title']='My Page';
		$data['dataUser'] = $this->mrmodel->dataUser();
		$data['admin'] = $this->mrmodel->getAdmin();
		$this->load->view('static/head',$data);
		$this->load->view('content/tampil_page',$data);
		$this->load->view('static/footer');
	}

	public function userbaru()
	{
		$data['head_title']='Add data user';
		$data['user'] = $this->mrmodel->getUser();
		$data['kontak'] = $this->mrmodel->getKontak();
		$this->load->view('static/head',$data);
		$this->load->view('content/adduser',$data);
		$this->load->view('static/footer');

		if (isset($_POST['nama'])) {
		$nama= $_POST['nama'];
		$alamat = $_POST['alamat'];
		$jk =$_POST['jk'];
		$kota= $_POST['kota'];
		$propinsi= $_POST['propinsi'];
		$this->mrmodel->add_user($nama,$alamat,$jk,$kota,$propinsi);
		//echo '<input type="hidden" id="success" value="1">';
		}
	}
	public function Add()
	{
		$result['data'] = "false";
		if (isset($_POST['nama1'])) {
		$nama= $_POST['nama1'];
		$alamat = $_POST['alamat1'];
		$jk =$_POST['jk1'];
		$kota= $_POST['kota1'];
		$propinsi= $_POST['propinsi1'];
		$this->mrmodel->add_user_new($nama,$alamat,$jk,$kota,$propinsi);
		$result['data']='success';
		}
		echo json_encode($result);
		
	}

	public function Update(){
		// $id=$_POST['getIDp'];
		// $nama=$_POST['nama1'];
		// $alamat=$_POST['alamat1'];
		// $jk=$_POST['jk1'];
		// $kota=$_POST['kota1'];
		// $propinsi=$_POST['propinsi1'];
		// $data['hasil']='false';
		// if (isset($nama)) {
		// $data['hasil']='true';
		// $this->mrmodel->actUpdate($id,$nama,$alamat,$jk,$kota,$propinsi);
		// redirect(base_url('adduser',$data));
		// }

		$result['data'] = "false";
		$id=$_POST['id_user_new'];
		$nama=$_POST['nama1'];
		$alamat=$_POST['alamat1'];
		$jk=$_POST['jk1'];
		$kota=$_POST['kota1'];
		$propinsi=$_POST['propinsi1'];
		if (isset($nama)) {
		$this->mrmodel->actUpdate($id,$nama,$alamat,$jk,$kota,$propinsi);
		//redirect(base_url('adduser',$data));
		$result['data'] = "success";
		}
		echo json_encode($result);
		
	}


	public function DeleteData(){
		$resul['data'] = "false";
		$id=$_POST['id_user_new'];
		if (isset($id)) {
		//$id= $_POST['id_user_new'];
		
		$this->mrmodel->delete_user_new($id);
		$result['data']='success';
		}
		echo json_encode($result);
	}
	public function dataTable()
	{
		$dataUser = $this->mrmodel->getUser();
			$str='<thead>
			  <th>No</th>
			  <th>Nama</th>
			  <th>Alamat</th>
			  <th>JK</th>
			  <th>Kota</th>
			  <th>Propinsi</th>
			  <th>Aksi</th>
			 </thead>
			 <tbody>';
				$no=1;
				foreach($dataUser as $key){
				$str.='<tr>
				<td>'.$no.'</td>
				<td>'.$key->nama1.'</td>
				<td>'.$key->alamat1.'</td>
				<td>'.$key->jk1.'</td>
				<td>'.$key->kota1.'</td>
				<td>'.$key->propinsi1.'</td>
				<td> <button class="btn btn-warning btn-sm editData" id="btn-edit" data-idname="'.$key->id_user_new.'">Edit</button> <button class="btn del btn-danger btn-sm" data-id="'.$key->id_user_new.'" data-toggle="modal" data-target="#modalDelete">Delete</button> </td>
				</tr>';
				
				$no++;
				}

				
		$str.='</tbody>';
		echo $str;
	}

	public function getEdit()
	{
		$id=$_POST['id_user_new'];
		if (isset($id)) {
		$data = $this->mrmodel->getDataUser($id);
		foreach ($data as $key) {
			$nama= $key->nama1;
			$alamat= $key->alamat1;
			$nama= $key->nama1;
			$jk= $key->jk1;
			$kota= $key->kota1;
			$propinsi= $key->propinsi1;
		}
		$result['data']='success';
		$result['nama']=$nama;
		$result['alamat']=$alamat;
		$result['jk']=$jk;
		$result['kota']=$kota;
		$result['propinsi']=$propinsi;
		}
		echo json_encode($result);
	}
	
}