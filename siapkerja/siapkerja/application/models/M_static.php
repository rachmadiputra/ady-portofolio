<?php
	class M_static extends CI_Model 
	{
		function __construct() 
		{
			parent::__construct();
		}

		function xss($data)
		{
			$cek=stripcslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES)));
			return $cek;
		}

		function display_html($data)
		{
			$cek=htmlspecialchars_decode($data);
			return $cek;
		}

		function eng_display_month($data)
        {
            $months=array("Jan" => "January", "Feb" => "February", "Mar" => "March", "Apr" => "April", "May" => "May", "Jun" => "Juny", "Jul" => "July",
            "Aug" => "August", "Sep" => "September", "Oct" => "October", "Nov" => "November", "Dec" => "December");
            return $months[$data];
        }
        function push($title,$message,$category,$cont,$deviceToken){
        	$badge = 1;
			$passphrase = '';


			$ctx = stream_context_create(); //online server
			stream_context_set_option($ctx, 'ssl', 'local_cert', 'CertificatesAppshop.pem');
			stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);


			$fp = stream_socket_client(
			  'ssl://gateway.sandbox.push.apple.com:2195', $err,
			  $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

			if (!$fp)
			  return false;
			  //exit("Failed to connect: $err $errstr" . PHP_EOL);



			// Create the payload body
			$body['aps'] = array(
			  'alert' => "$title",
			  'alert' => "$message",
			  'sound' => "default",
			  'id' => "$cont",
			  'badge' => $badge, 
			  //'link_url' => $url,
			  'category' => "$category"
			  );


			$payload = json_encode($body);
			$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
			$result = fwrite($fp, $msg, strlen($msg));

			if (!$result)
			  return true;
			else
			  return false;

			fclose($fp);
        }
        function segmentMenu($segment,$nsegment,$class,$style){
        	if ($this->uri->segment($segment)==$nsegment){
        		
        	return $class; }
        	echo $class;
        }

		function time_eng_withoutday($date)
        {
            $timestamp = strtotime($date);
            $day = date('D', $timestamp);
            $month = date('M', $timestamp);
            
            $getWaktu=
			$this->m_static->eng_display_month($month).' '.
            substr($date, 8,2).', '.
            substr($date, 0,4);
            $getWaktu=date('Y',$timestamp).'年'.date('m',$timestamp).'月'.date('d',$timestamp).'日';
            return $getWaktu;
        }

        function set_link($content)
	    {
	        $karakter = array ('{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+','-','/','\\',',','.','#',':',';','\'','"','[',']');
	        $hapus_karakter_aneh = strtolower(str_replace($karakter,"",$content));
	        $tambah_strip = strtolower(str_replace(' ', '-', $hapus_karakter_aneh));
	        $link_akhir = $tambah_strip;
	        return $link_akhir;
	    }
	    function trimUrl($text){
	    	$new_text=str_replace("img/content/gallery/",base_url()."img/content/gallery/",$text);
	    	return $new_text;
	    }
        ///////////////////////////////////////////////////////////RESIZE IMAGE////////////////////////////////////////////////////

        function normal_resize_image($source, $destination, $image_type, $max_size, $image_width, $image_height, $quality){
	
			if($image_width <= 0 || $image_height <= 0){return false;} //return false if nothing to resize
			
			//do not resize if image is smaller than max size
			if($image_width <= $max_size && $image_height <= $max_size){
				$new_canvas		= imagecreatetruecolor( $image_width, $image_height );
				if($image_type == "image/gif" or $image_type == "image/png"){
				    imagecolortransparent($new_canvas, imagecolorallocatealpha($new_canvas, 0, 0, 0, 127));
				    imagealphablending($new_canvas, false);
				    imagesavealpha($new_canvas, true);
			  	}
			  	if(imagecopyresampled($new_canvas, $source, 0, 0, 0, 0, $image_width, $image_height, $image_width, $image_height)){
					$this->m_static->save_image($new_canvas, $destination, $image_type, $quality); //save resized image
					return true;
				}
			}
			
			//Construct a proportional size of new image
			$image_scale	= min($max_size/$image_width, $max_size/$image_height);
			$new_width		= ceil($image_scale * $image_width);
			$new_height		= ceil($image_scale * $image_height);
			
			$new_canvas		= imagecreatetruecolor( $new_width, $new_height ); //Create a new true color image
			if($image_type == "image/gif" or $image_type == "image/png"){
			    imagecolortransparent($new_canvas, imagecolorallocatealpha($new_canvas, 0, 0, 0, 127));
			    imagealphablending($new_canvas, false);
			    imagesavealpha($new_canvas, true);
			  }
			//Copy and resize part of an image with resampling
			if(imagecopyresampled($new_canvas, $source, 0, 0, 0, 0, $new_width, $new_height, $image_width, $image_height)){
				$this->m_static->save_image($new_canvas, $destination, $image_type, $quality); //save resized image
			}

			return true;
		}

		function crop_image_square($source, $destination, $image_type, $square_size, $image_width, $image_height, $quality){
			if($image_width <= 0 || $image_height <= 0){return false;} //return false if nothing to resize
			
			if( $image_width > $image_height )
			{
				$y_offset = 0;
				$x_offset = ($image_width - $image_height) / 2;
				$s_size 	= $image_width - ($x_offset * 2);
			}else{
				$x_offset = 0;
				$y_offset = ($image_height - $image_width) / 2;
				$s_size = $image_height - ($y_offset * 2);
			}
			$new_canvas	= imagecreatetruecolor( $square_size, $square_size);
			 //Create a new true color image
			
			//Copy and resize part of an image with resampling
			if(imagecopyresampled($new_canvas, $source, 0, 0, $x_offset, $y_offset, $square_size, $square_size, $s_size, $s_size)){
				$this->m_static->save_image($new_canvas, $destination, $image_type, $quality);
			}

			return true;
		}

		function save_image($source, $destination, $image_type, $quality){
			switch(strtolower($image_type)){//determine mime type
				case 'image/png': 
					imagepng($source, $destination); return true; //save png file
					break;
				case 'image/gif': 
					imagegif($source, $destination); return true; //save gif file
					break;          
				case 'image/jpeg': case 'image/pjpeg': 
					imagejpeg($source, $destination, $quality); return true; //save jpeg file
					break;
				default: return false;
			}
		}

		///////////////////////////////////////////////////////////=================////////////////////////////////////////////////////

		function pagination($limit,$adjacents,$rows,$page)
		{	
			$pagination='';
			if ($page == 0) $page = 1;
			$prev = $page - 1;
			$next = $page + 1;
			
			$lastpage = ceil($rows/$limit);	
			
			if($lastpage > 1)
			{	
				$pagination .= "<ul class=\"pagination pagination-sm no-margin pull-left\" style=\"padding:10px 0 10px 0;\">";
				//previous button
				if ($page > 1) 
					$pagination.= "<li><a class='page-numbers' href=\"?page=$prev\">&laquo;</a></li>";
				else{
					$pagination.= "<li><span class=\"disabled\">&laquo;</span></li>";	
					}
				
				//pages	
				if ($lastpage < 5 + ($adjacents * 2))
				{	
					for ($counter = 1; $counter <= $lastpage; $counter++)
					{
						if ($counter == $page)
							$pagination.= "<li class=\"active\"><span>$counter</span></li>";
						else
							$pagination.= "<li><a class='page-numbers' href=\"?page=$counter\">$counter</a></li>";					
					}
				}
				elseif($lastpage > 3 + ($adjacents * 2))
				{
					//close to beginning; only hide later pages
					if($page < 1 + ($adjacents * 2))		
					{
						for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
						{
							if ($counter == $page)
								$pagination.= "<li class=\"active\"><span>$counter</span></li>";
							else
								$pagination.= "<li><a class='page-numbers' href=\"?page=$counter\">$counter</a></li>";					
						}
						
					}
					//in middle; hide some front and some back
					elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
					{
						for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
						{
							if ($counter == $page)
								$pagination.= "<li class=\"active\"><span>$counter</span></li>";
							else
								$pagination.= "<li><a class='page-numbers' href=\"?page=$counter\">$counter</a></li>";					
						}
						
					}
					//close to end; only hide early pages
					else
					{
					
						for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
						{
							if ($counter == $page)
								$pagination.= "<li class=\"active\"><span>$counter</span></li>";
							else
								$pagination.= "<li><a class='page-numbers' href=\"$?page=$counter\">$counter</a></li>";					
						}
					}
				}
				
				//next button
				if ($page < $counter - 1) 
					$pagination.= "<li><a class='page-numbers' href=\"?page=$next\">&raquo;</a></li>";
				else{
					$pagination.= "<li><span class=\"disabled\">&raquo;</span></li>";
					}
				$pagination.= "</ul>";		
			}

			return $pagination;  
		}

		////////////////////////////////////////////////////////LIMIT DESCRIPTION////////////////////////////////////////////////////

		function limit_text($text, $limit, $link) {
	      	if (str_word_count($text, 0) > $limit) {
				$words = str_word_count($text, 2);
				$pos = array_keys($words);
				$text = substr($text, 0, $pos[$limit]) . '. . . <a href="'.$link.'">Read More<a>';
	      	}
	      	return $text;
	    }

	    function limit_text2($text, $limit) {
	      	if (str_word_count($text, 0) > $limit) {
				$words = str_word_count($text, 2);
				$pos = array_keys($words);
				$text = substr($text, 0, $pos[$limit]) . '. . .';
	      	}
	      	return $text;
	    }

	    function japanese_date($data)
		{
			$dateGmt= ($data +(32400));
			$cek = gmdate("Y年m月d日",$dateGmt);
			return $cek;
		}
		function japanese_date_fromdate($data)
		{
			$date=date_create($data);
			$cek = date_format($date,"Y年m月d日");
			return $cek;
		}

		function date_timestamp($data){
			$timestamp = strtotime($data);
			return $timestamp;
		}


		function sendNotif($title,$message,$category,$cont,$deviceToken){
			
			$payload = array();
	        $payload['team'] = 'Appkey';
	        $payload['score'] = '5.6';
			$res['data']['title'] = $title;
	        $res['data']['is_background'] = false;
	        $res['data']['message'] = $message;
	        $res['data']['cate'] = $category;
	        $res['data']['image'] = "";
	        $res['data']['payload'] = $payload;
	        $res['data']['timestamp'] = date('Y-m-d G:i:s');
			 $fields = array(
	            'to' => $deviceToken,
	            'data' => $res,
	        );
			$url = 'https://fcm.googleapis.com/fcm/send';

	        $headers = array(
	            'Authorization: key=' . "AAAASXxTRm8:APA91bHQmyblqK3CWXZchW36hO-g6pOkOcwXmkWHuacqBNn4aa-l3OrDeBlmaTeaoSRsSRR4TqoBL_8NpRaO3A-z-N4Y5NMrvPPsTFbReGfc_8LSTte98MPeNFBrJJaNyjGoCJTFjnqm",
	            'Content-Type: application/json'
	            //old key AAAAOeuYlLg:APA91bErIJYF9NS9jYuL73_sgZIVT-FnRmPQEAJSO4Slf6czds8WIuc_mYx-EohJG6PXl1bshZ1iX6riU4Ytcv1omgJEHMBMkMQjPUcFFrQJ-tfTxXpMjc_GBZoUiZ0Bo-RXoZ-ltrUJ
	        );
	        // Open connection
	        $ch = curl_init();

	        // Set the url, number of POST vars, POST data
	        curl_setopt($ch, CURLOPT_URL, $url);

	        curl_setopt($ch, CURLOPT_POST, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	        // Disabling SSL Certificate support temporarly
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

	        // Execute post
	        $result = curl_exec($ch);
	        if ($result === FALSE) {
	            die('Curl failed: ' . curl_error($ch));
	        }

	        // Close connection
	        curl_close($ch);
	        return $result;
		}


		function sendNotifIOS($title,$message,$category,$cont,$deviceToken){
			
			$ch = curl_init("https://fcm.googleapis.com/fcm/send");

		    //The device token.
		    $token = "$deviceToken"; //token here 

		    //Title of the Notification.
		    $title = "$title";

		    //Body of the Notification.
		    $body = "$message";

		    //Creating the notification array.
		    $payload['team'] = 'Appkey';
			$payload['score'] = '5.6';



		    $notification = array('title' =>$title , 
		    						'body' => $body,
									'cate' => $category,
									'id' => "$cont");


		    //This array contains, the token and the notification. The 'to' attribute stores the token.
		    $arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high');

		    //Generating JSON encoded string form the above array.
		    $json = json_encode($arrayToSend);
		    echo $json;
		    echo "<br>";
		    //Setup headers:
		    $headers = array();
		    $headers[] = 'Content-Type: application/json';
		    $headers[] = 'Authorization: key= AAAASXxTRm8:APA91bHQmyblqK3CWXZchW36hO-g6pOkOcwXmkWHuacqBNn4aa-l3OrDeBlmaTeaoSRsSRR4TqoBL_8NpRaO3A-z-N4Y5NMrvPPsTFbReGfc_8LSTte98MPeNFBrJJaNyjGoCJTFjnqm'; // key here old AAAAOeuYlLg:APA91bErIJYF9NS9jYuL73_sgZIVT-FnRmPQEAJSO4Slf6czds8WIuc_mYx-EohJG6PXl1bshZ1iX6riU4Ytcv1omgJEHMBMkMQjPUcFFrQJ-tfTxXpMjc_GBZoUiZ0Bo-RXoZ-ltrUJ

		    //Setup curl, add headers and post parameters.
		    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		    curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);       

		    //Send the request
		    $response = curl_exec($ch);

		    //Close request
		    curl_close($ch);
		    return $response;
		}
	}
