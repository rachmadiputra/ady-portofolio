<?php
class Mrmodel extends CI_Model 
	{
		function __construct() 
		{
			parent::__construct();
		}
		
		function getAdmin($user,$passSha1)
		{
			$this->db->select('id_admin,username,password,nama');
			$this->db->where('username',$user);
			$this->db->where('password',$passSha1);
			$query=$this->db->get('admin');
			return $query->result();
		}

		function add_manage_data($nama,$email,$number,$website,$occupation,$password,$phone,$textarea)
		{
			$data=array('name'=>$nama, 'email'=>$email, 'number'=>$number, 'website'=>$website, 'occupation'=>$occupation, 'password'=>$password, 'phone'=>$phone, 'textarea'=>$textarea);
			$this->db->set($data);
			return $this->db->insert('manage_data');
		}

		function getUser()
		{
			$this->db->select('*');
			$query=$this->db->get('data_user_new');
			return $query->result();
		}
		function getManageData()
		{
			$this->db->select('*');
			$query=$this->db->get('manage_data');
			return $query->result();
		}
		function getKontak()
		{
			$this->db->select('nama2,alamat2,jk2,kota2');
			$query=$this->db->get('kontak');
			return $query->result();
		}
		function getDataUser($id)
		{
			$this->db->select('*');
			$this->db->where('id_user_new', $id);
			$query=$this->db->get('data_user_new');
			return $query->result();
		}
		function dataUser(){
		$datasaya='asdasdasdasdas';
		return $datasaya;
		}
		function actUpdate($id,$nama,$alamat,$jk,$kota,$propinsi){
		$data=array('nama1'=>$nama,'alamat1'=>$alamat,'jk1'=>$jk,'kota1'=>$kota,'propinsi1'=>$propinsi);
		$this->db->where('id_user_new', $id);
		$this->db->update('data_user_new',$data);
		}

		function add_user($nama,$alamat,$jk,$kota,$propinsi)
		{
			$data=array('nama'=>$nama, 'alamat'=>$alamat, 'jk'=>$jk, 'kota'=>$kota, 'propinsi'=>$propinsi);
			$this->db->set($data);
			return $this->db->insert('data_user');
		}
		function add_user_new($nama,$alamat,$jk,$kota,$propinsi)
		{
			$data=array('nama1'=>$nama, 'alamat1'=>$alamat, 'jk1'=>$jk, 'kota1'=>$kota, 'propinsi1'=>$propinsi);
			$this->db->set($data);
			return $this->db->insert('data_user_new');
		}
		function delete_user_new($id)
		{
			$this->db->where('id_user_new', $id);
			$this->db->delete('data_user_new');
		}

	}


?>