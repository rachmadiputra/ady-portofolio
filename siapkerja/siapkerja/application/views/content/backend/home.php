<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="page-title">
			<div class="title_left">
				<h3>Form Validation</h3>
				<button id="tutup" class="btn btn-danger" style="display: none;">Close</button>
				<button id="add-data" class="btn btn-primary">Add Data</button>
			</div>


		</div>
		<div class="clearfix"></div>
		<div class="clearfix">&nbsp;</div>

		<div class="row" id="form-data" style="display: none;">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Form validation
							<small>sub title</small>
						</h2>
						<ul class="nav navbar-right panel_toolbox no-close">
							<li>
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
									<i class="fa fa-wrench"></i>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li>
										<a href="#">Settings 1</a>
									</li>
									<li>
										<a href="#">Settings 2</a>
									</li>
								</ul>
							</li>
							<li>
								<a class="close-link">
									<i class="fa fa-close"></i>
								</a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">

						<form class="form-horizontal form-label-left" method="POST" id="form-login2" data-toggle="validator" action="<?php echo base_url('adminsite/manage_data_add'); ?>">

							<p>For alternative validation library
								<code>parsleyJS</code> check out in the
								<a href="form.html">form page</a>
							</p>
							<span class="section">Personal Info</span>

							<div class="item form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Name
									<span class="required">*</span>
								</label>
								<div class="col-md-7 col-sm-7 col-xs-12">
									<input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="name"
									placeholder="My name" required data-error="Please fill this colom" type="text" value="My Name">
									<div class="help-block with-errors"></div>
								</div>

							</div>
							<div class="item form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="email">Email
									<span class="required">*</span>
								</label>
								<div class="col-md-7 col-sm-8 col-xs-12">
									<input type="email" id="email" name="email" required data-error="Not email" class="form-control col-md-7 col-xs-12" value="darma.appkey@gmail.com">
									<div class="help-block with-errors"></div>
								</div>
							</div>

							<div class="item form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="number">Number
									<span class="required">*</span>
								</label>
								<div class="col-md-7 col-sm-8 col-xs-12">
									<input type="number" id="number" name="number" required="required" class="form-control col-md-7 col-xs-12" value="123123">
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="item form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="website">Website URL
									<span class="required">*</span>
								</label>
								<div class="col-md-7 col-sm-8 col-xs-12">
									<input type="url" id="website" name="website" required="required" placeholder="www.website.com" class="form-control col-md-7 col-xs-12"
									value="http://darma.com">
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="item form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="occupation">Occupation
									<span class="required">*</span>
								</label>
								<div class="col-md-7 col-sm-8 col-xs-12">
									<input id="occupation" type="text" name="occupation" class="optional form-control col-md-7 col-xs-12" value="darma">
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="item form-group">
								<label for="password" class="control-label col-md-2">Password</label>
								<div class="col-md-7 col-sm-8 col-xs-12">
									<input id="password" type="text" name="password" class="form-control col-md-7 col-xs-12" required="required" value="darma">
									<div class="help-block with-errors"></div>
								</div>
							</div>

							<div class="item form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="telephone">Telephone
									<span class="required">*</span>
								</label>
								<div class="col-md-7 col-sm-8 col-xs-12">
									<input type="tel" id="telephone" name="phone" required="required" class="form-control col-md-7 col-xs-12" value="085737223531">
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="item form-group">
								<label class="control-label col-md-2 col-sm-2 col-xs-12" for="textarea">Textarea
									<span class="required">*</span>
								</label>
								<div class="col-md-7 col-sm-8 col-xs-12">
									<textarea id="textarea" name="textarea" class="form-control col-md-7 col-xs-12">Saya ingin tahu</textarea>
								</div>
							</div>
							<div class="ln_solid"></div>
							<div class="form-group">
								<div class="col-md-6 col-md-offset-4">
									<button type="reset" class="btn btn-primary">Reset</button>
									<button id="btn-send" type="submit" class="btn btn-success">Submit</button>

								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Default Example
							<small>Users</small>
						</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li>
								<a class="collapse-link">
									<i class="fa fa-chevron-up"></i>
								</a>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
									<i class="fa fa-wrench"></i>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li>
										<a href="#">Settings 1</a>
									</li>
									<li>
										<a href="#">Settings 2</a>
									</li>
								</ul>
							</li>
							<li>
								<a class="close-link">
									<i class="fa fa-close"></i>
								</a>
							</li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<p class="text-muted font-13 m-b-30">
							DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction
							function:
							<code>$().DataTable();</code>
						</p>
						<div class="table-responsive">
							<table id="table_tab" class="table table-striped table-bordered">

							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix">&nbsp;</div>

			<!-- <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Default Example <small>Users</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                      DataTables has most features enabled by default, so all you need to do to use it with your own tables is to call the construction function: <code>$().DataTable();</code>
                    </p>
                    <table id="abc" class="table table-striped table-bordered">
                      <thead>
                        <th>No</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Number</th>
                        <th>Website</th>
                        <th>Occupation</th>
                        <th>Password</th>
                        <th>Phone</th>
                        <th>Text</th>
                        <th>Aksi</th>
                       </thead>
                       <tbody>
                        <tr>
                          <td>Tiger Nixon</td>
                          <td>System Architect</td>
                          <td>Edinburgh</td>
                          <td>61</td>
                          <td>2011/04/25</td>
                          <td>$320,800</td>
                          <td>Tiger Nixon</td>
                          <td>System Architect</td>
                          <td>Edinburgh</td>
                          <td>61</td>
                  
                        </tr>
                        <tr>
                          <td>Garrett Winters</td>
                          <td>Accountant</td>
                          <td>Tokyo</td>
                          <td>63</td>
                          <td>2011/07/25</td>
                          <td>$170,750</td>
                          <td>Tiger Nixon</td>
                          <td>System Architect</td>
                          <td>Edinburgh</td>
                          <td>61</td>

                        </tr>
                        <tr>
                          <td>Ashton Cox</td>
                          <td>Junior Technical Author</td>
                          <td>San Francisco</td>
                          <td>66</td>
                          <td>2009/01/12</td>
                          <td>$86,000</td>
                          <td>Tiger Nixon</td>
                          <td>System Architect</td>
                          <td>Edinburgh</td>
                          <td>61</td>
  
                        </tr>
                        <tr>
                          <td>Cedric Kelly</td>
                          <td>Senior Javascript Developer</td>
                          <td>Edinburgh</td>
                          <td>22</td>
                          <td>2012/03/29</td>
                          <td>$433,060</td>
                          <td>Tiger Nixon</td>
                          <td>System Architect</td>
                          <td>Edinburgh</td>
                          <td>61</td>

                        </tr>
                        <tr>
                          <td>Airi Satou</td>
                          <td>Accountant</td>
                          <td>Tokyo</td>
                          <td>33</td>
                          <td>2008/11/28</td>
                          <td>$162,700</td>
                          <td>Tiger Nixon</td>
                          <td>System Architect</td>
                          <td>Edinburgh</td>
                          <td>61</td>
                 
                        </tr>
                        <tr>
                          <td>Brielle Williamson</td>
                          <td>Integration Specialist</td>
                          <td>New York</td>
                          <td>61</td>
                          <td>2012/12/02</td>
                          <td>$372,000</td>
                          <td>Tiger Nixon</td>
                          <td>System Architect</td>
                          <td>Edinburgh</td>
                          <td>61</td>
                         
                        </tr>
                        <tr>
                          <td>Herrod Chandler</td>
                          <td>Sales Assistant</td>
                          <td>San Francisco</td>
                          <td>59</td>
                          <td>2012/08/06</td>
                          <td>$137,500</td>
                          <td>Tiger Nixon</td>
                          <td>System Architect</td>
                          <td>Edinburgh</td>
                          <td>61</td>
                   
                        </tr>
                        <tr>
                          <td>Rhona Davidson</td>
                          <td>Integration Specialist</td>
                          <td>Tokyo</td>
                          <td>55</td>
                          <td>2010/10/14</td>
                          <td>$327,900</td>
                          <td>Tiger Nixon</td>
                          <td>System Architect</td>
                          <td>Edinburgh</td>
                          <td>61</td>
                        
                        </tr>
                        <tr>
                          <td>Colleen Hurst</td>
                          <td>Javascript Developer</td>
                          <td>San Francisco</td>
                          <td>39</td>
                          <td>2009/09/15</td>
                          <td>$205,500</td>
                          <td>Tiger Nixon</td>
                          <td>System Architect</td>
                          <td>Edinburgh</td>
                          <td>61</td>
                          
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div> -->
			<div class="clearfix">&nbsp;</div>

		</div>
	</div>
</div>

<style type="text/css">
	#show-text {
		display: none;
		/*border:1px solid green; 
  border-radius: 5px; 
  background-color: green; 
  color: white;
  padding: 10px;*/
		position: fixed;
		bottom: 0;
		right: 20px;
		width: 17%;
		color: #FFFFFF;
	}

</style>
<div id="show-text" class="alert alert-info alert-dismissible" role="alert">

	<strong>Success!</strong> please Check!.
</div>
<!-- /page content -->


<script type="text/javascript">
	$('#btn-send').click(function () {
		$('#form-login2').ajaxForm({
			beforeSubmit: function () {

			},
			beforeSend: function () {
				// $('#btn-login').attr('disabled', 'disabled');
				// $('#btn-login').html('Loading 。 。 。');

			},
			success: function () {
				// $('#btn-login').removeAttr('disabled');
				// $('#btn-login').html('Signin');

			},
			complete: function (response, textStatus, XMLHttpRequest) {
				var responseText = jQuery.parseJSON(response.responseText);
				if (responseText.data == "success") {
					$('#show-text').fadeIn();
					$('#show-text').delay(3000).fadeOut(1000);
					displayPage();
					$('#data-success').html('Data suksess di add');
					$("#form-login2")[0].reset();
					$("#form-data").slideUp('fast');
					$("#tutup").hide();
					$("#add-data").fadeIn();
				} else {
					$('#data-success').html('Data gagal di add');
				}
			},
			error: function () {
				alert('browser tidak mendukung javascript');
			}
		});
	});

</script>

<script type="text/javascript">
	$(document).ready(function () {
		$("#add-data").click(function () {
			$("#form-data").slideDown('fast');
			$("#tutup").fadeIn();
			$("#add-data").hide();
		});

		$("#tutup").click(function () {
			$("#form-data").slideUp('fast');
			$("#tutup").hide();
			$("#add-data").fadeIn();
		});

		// $("a[data-action]").on("click", function (event) {
		// event.preventDefault();
		// });

	});

</script>

<script type="text/javascript">
	displayPage();

	function displayPage() {
		$.ajax({
			type: "POST",
			url: 'http://localhost/ci-admin/adminsite/manage_data_add/showtable',
			//data: "page="+page,
			cache: false,
			success: function (response) {
				$('#table_tab').html(response);
			}
		});
	}
	$(document).ready(displayPage);

	$(document).ready(function () {
		$('#abc').DataTable();
	});

</script>
