<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <title><?= $head_title ?></title>

    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/nprogress/nprogress.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/build/css/custom.css" rel="stylesheet">
    <script src="<?php echo $this->config->item('jQuery');?>"></script>
    <script src="<?php echo $this->config->item('JQForm');?>"></script>
  </head>

  <body>
    
            
           <style type="text/css">
           	.login-form {
           		position: fixed;
				top: 55%;
				left: 50%;
				  /* bring your own prefixes */
				transform: translate(-25%, -50%);
				width: 100%;
           	}
           	.nav {
			    padding-left: 50px;
			    margin-bottom: 0;
			    list-style: none;
			}
			.x_title2 {
				padding-top: 10px;
			}
			body {
				background-color: #4D8FAC;
			}
			.btn {
				border-radius: 0px;
			}
			#fa-eye {
				color: #000;
				cursor: pointer;
				position: relative;
				top: -23px;
				right: 7px;
			}
			.x_panel2 {
				background-color:#4D8FAC;
				margin-top: 10px;
				color: #fff;
			}
			.x_panel2 h3 small {
				
				color: #ABB7B7;
			}
           </style>

            <div class="login-form">
              <div class="col-md-5 col-xs-12">
              	<div class="panel-sukses" id="notif-login" style="display: none;">
              		<div class="alert alert-success alert-dismissible fade in" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <strong>Login Success!</strong> Redirect to admin homepage.
                  </div>
              	</div>
              	<div class="panel-sukses" id="notif-danger" style="display: none;">
              		<div class="alert alert-danger alert-dismissible fade in" role="alert">
                   <strong>Login Failed!</strong> Your username or password is not correct.
                  </div>
              	</div>
              	<div class="panel-sukses" id="notif-warning" style="display: none;">
              		<div class="alert alert-warning alert-dismissible fade in" role="alert">
                    
                    <p id="textinfo"></p>
                  </div>
              	</div>
                <div class="x_panel">
                	<ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                	<div class="x_panel x_panel2">
                  <div class="x_title2">
                    
                    <!-- <p>Only admin can access this page!</p> -->
                    <!-- <div class="clearfix"></div> -->
                    <h3><span class="fa fa-user"></span> Login Form <small>Complete the username and password</small></h3><small>Enter your username and password in the field below!</small>
                  </div>
                  </div>
                  <div class="x_title">
                    

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left input_mask" method="POST" id="form-login" action="<?php echo base_url('adminsite/actlogin'); ?>">
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" name="username" id="username"  placeholder="Username">
                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                        <input type="password" name="password" class="form-control has-feedback-left" id="password" placeholder="password">
                        <span class="fa fa-unlock-alt form-control-feedback left" aria-hidden="true"></span>
                        <span id="fa-eye" class="fa fa-eye right" title="Show Password"></span>
                        <!-- <input type="checkbox" id="showpassword" name="showpassword" value="check">
    					  <label for="showpassword">Show Password</label> -->
                      </div> 

                      <div class="form-group">
                       
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input class="form-control col-md-7 col-xs-12" required="required" type="hidden">
                          <input type="checkbox" id="check" name="check" value="check">
    					  <label for="check">Remember me!</label>
                          <a href=""><h5>Forgot password?</h5></a>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-8 col-sm-8 col-xs-12 col-md-offset-4" align="right">
						  <button class="btn btn-primary" id="reset" type="reset">Reset</button>
                          <button type="submit" class="btn btn-success" name="btn-login" id="btn-login">Login</button>
                        </div>

                      </div>

                    </form>
                  </div>
                </div>


              </div>
          </div>

              
     <script type="text/javascript">
     	$('#btn-login').click(function() {
		$('#form-login').ajaxForm({
			beforeSubmit:  function() {
				if(!$('#username').val()){
					$('#username').focus();
					$('#notif-danger').hide();
					$('#notif-warning').fadeIn();
					// $('#notif-warning').delay(2000).fadeOut(1000);
					$('#textinfo').html('Anda belum memasukan username!');
					return false;
				} else if(!$('#password').val()){
					$('#password').focus();
					$('#notif-danger').hide();
					$('#notif-warning').fadeIn();
					// $('#notif-warning').delay(2000).fadeOut(1000);
					$('#textinfo').html('Anda belum memasukan Password!');
					return false;
				} else {

				}
			},
			beforeSend: function(){
				// $('#btn-login').attr('disabled', 'disabled');
				// $('#btn-login').html('Loading 。 。 。');
			},
			success: function() {
				// $('#btn-login').removeAttr('disabled');
				// $('#btn-login').html('Signin');
			},
			complete: function(response, textStatus, XMLHttpRequest) {
				var responseText = jQuery.parseJSON(response.responseText);
				if(responseText.data=="200"){
					$('#notif-warning').hide();
					$('#notif-danger').hide();
					$('#notif-login').fadeIn();
					$('#notif-login').delay(1000).fadeOut(1000,function() {
						location.href=responseText.url_success;
					});
				} else {
					$('#notif-warning').hide();
					$('#notif-danger').fadeIn();
					// $('#notif-danger').delay(2000).fadeOut(1000);
					$('#textinfo').html('Username atau passowrd anda salah');
				}
			},
			error: function(){
				$('#btn-login').removeAttr('disabled');
				$('#btn-login').html('Error');
				alert('browser tidak mendukung javascript');
			}
		});
	});


     $(document).ready(function() {
	  $('#reset').click(function() {
	  	$('#notif-warning').hide();
	  	$('#notif-danger').hide();
	  });
	});
     </script>

     <script type="text/javascript">
     $(document).ready(function() {
	  $('#fa-eye').click(function() {
	  	if ($(this).hasClass('fa-eye')) {
	    $(this).removeClass( "fa-eye" ).addClass( "fa-eye-slash" );
	    $('#password').attr('type','text');
	    $(this).prop('title', 'Hide Password');
		}else{
		$(this).removeClass( "fa-eye-slash" ).addClass( "fa-eye" );
	    $('#password').attr('type','password');
	    $(this).prop('title', 'Show Password');	
		}
	  });
	});
      $('span.fa-eye').css( 'cursor', 'pointer' );
     </script>

     <!-- jQuery -->
    
    <!-- <script src="<?php //echo base_url(); ?>assets/jquery/dist/jquery.min.js"></script> -->
    <!-- Bootstrap -->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/nprogress/nprogress.js"></script>
    <script src="<?php echo base_url(); ?>assets/build/js/custom.min.js"></script>
	
  </body>
</html>
