<footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

<script type="text/javascript">
  $(document).ready(function() {
    $('.updown').on('click', function() {
        var $ICON = $(this).find('span');
        $ICON.toggleClass('fa-chevron-down fa-chevron-up');
    });
});
</script>

    <!-- jQuery -->

    
    <!-- <script src="<?php //echo base_url(); ?>assets/jquery/dist/jquery.min.js"></script> -->
    

    <script src="<?php echo $this->config->item('dataTables_JS');?>"></script>
    <script src="<?php echo $this->config->item('dataTables_BS');?>"></script>
    <script src="<?php echo $this->config->item('dataTables_Fix');?>"></script>

    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/validator.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/fastclick/lib/fastclick.js"></script>
    <script src="<?php echo base_url(); ?>assets/nprogress/nprogress.js"></script>
    <!-- <script src="<?php //echo base_url(); ?>assets/validator/validator.js"></script> -->
    <script src="<?php echo base_url(); ?>assets/build/js/custom.min.js"></script>
	
  </body>
</html> 